<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 27.11.2018
 * Time: 15:56
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Series_Invitation_Manager extends \Core\Controller_core
{
    private $seriesInvitationsModel;
    private $seriesOwnersModel;
    private $seriesModel;
    private $postsHandle;
    private $isLoggedIn;
    private $auth;
    private $purchasedModel;

    public function __construct()
    {
        parent::__construct();

        $this->auth = new UserAuthentication();
        if (!$this->auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }

        $this->load->model('api_m/Series_Invitations_m');
        $this->seriesInvitationsModel = new \Models\api\Series_Invitations_m();

        $this->load->model('api_m/Series_Owners_m');
        $this->seriesOwnersModel = new \Models\api\Series_Owners_m();

        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();

        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();

        if ($this->isLoggedIn) {
            $this->purchasedModel = $this->postsHandle->purchasedModel;
        }

    }
    public function accept() {
        $invitationId = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $invitation = $this->seriesInvitationsModel->get($invitationId);
        if (!$invitation) {
            echo 'Sorry, this invitation has been deleted by Administrator<br>';
            echo 'Please wait until someone sends new invitation.';
            die();
        }
        if ($this->isLoggedIn && $_SESSION['guardian']['email'] != $invitation['strInvitation_email']){
            $this->auth->log_out();
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        if ($this->isLoggedIn) {
            $this->seriesInvitationsModel->update($invitationId, ['strSeriesInvitation_status' => 'accept']);
            $sets = [
                'intSeriesOwner_user_ID' => $_SESSION['client_ID'],
                'intSeriesOwner_series_ID' => $invitation['intSeries_ID']
            ];
            $this->seriesOwnersModel->insert($sets);
            $this->purchasedModel->insert(['intPurchased_series_ID' => $sets['intSeriesOwner_series_ID']]);
        }
        $series = $this->seriesModel->get($invitation['intSeries_ID']);
        $series['posts'] = $this->postsHandle->getSerialItems(['intPost_series_ID' => $series['series_ID'], 'intPost_parent' => 0], 'post_ID, intPost_series_ID, strPost_title, strPost_body, strPost_featuredimage, intPost_type, intPost_order, intPost_parent, strPost_nodeType, strPost_duration', ['intPost_order', 'post_ID'], 30);
        foreach ($series['posts'] as &$post) {
            $post['viewDay'] = $this->postsHandle->calcDay($post);
            $body = $post['strPost_body'];
            $post['origin_body'] = $body;
            switch ((int) $post['intPost_type']){
                case 0:
                    $body = \Helpers\isAbsUrl($body) ? $body : BASE_URL . '/' . $body;
                    break;
                case 2:
                    break;
                case 8:
                    $imgSection = substr($body, 0, strpos($body, "</section>"));
                    $body = \Helpers\htmlPurify($imgSection);
                    break;
                case 10:
                    break;
                default:
                    $subBody = trim(substr($body, 0, 480));
                    $fltBody = stripslashes($subBody . '...');
                    $body = \Helpers\htmlPurify($fltBody);
                    break;
            }
            $post['strPost_body'] = $body;
        }
        $series = \Helpers\utf8Encode($series);
        $this->load->view('Series_Invitation_Accepted_v', ['series' => $series, 'isLoggedIn' => $this->isLoggedIn, 'invitedEmail' => $invitation['strInvitation_email'], 'invitationId' => $invitationId]);
    }
    public function decline() {
        $invitationId = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $this->seriesInvitationsModel->update($invitationId, ['strSeriesInvitation_status' => 'decline']);
        $this->load->view('Series_Invitation_Declined_v');
    }
}


$handle = new Series_Invitation_Manager();

$action = isset($_POST['action']) ? $_POST['action'] : (isset($_GET['action']) ? $_GET['action'] : false);
switch ($action){
    case 'accept':
        $handle->accept();
        break;
    case 'decline':
        $handle->decline();
        break;
    default:
        $handle->index();
        break;
}