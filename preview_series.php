<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 25.10.2018
 * Time: 10:56
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Preview_Series extends \Core\Controller_core
{
    private $isLoggedIn;
    private $postsHandle;
    private $model;
    private $seriesHandle;
    private $seriesModel;
    private $purchasedModel;
    private $userHandle;
    private $optionsModel;
    private $categoryModel;
    private $favoriteModel;

    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->load->model('api_m/Posts_m');
        $this->load->model('api_m/Purchased_m');
        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();
        $this->model = $this->postsHandle->model;
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();
        $this->seriesModel = $this->seriesHandle->model;
        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Favorite_m');
        $this->favoriteModel = new \Models\api\Favorite_m();
    }
    public function index() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if (!$id){ echo 'no id'; die(); }

        $series = $this->seriesModel->get($id);

        if (!$this->seriesHandle->isAccessible($series)) {
            echo "you don't have permission for this series.";
            die();
        }

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        $series['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;

        $posts = $this->postsHandle->getSerialItems(['intPost_series_ID' => $id, 'intPost_parent' => 0], '*', ['intPost_order', 'post_ID'], 100);
        $viewUri = rawurlencode(BASE_URL . '/preview_series?id=' . $id . '&affiliate_id=' . $_SESSION['affiliate_id']);
        $uriTitle = rawurlencode($series['strSeries_title']);
        $isLoggedIn = $this->isLoggedIn;

        $owner = $this->userHandle->get($series['intSeries_client_ID']);
        $series['stripe_account_id'] = $owner['stripe_account_id'];
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';

        $postsCount = $this->model->countPosts(['intPost_series_ID' => $id]);

        $category = $this->categoryModel->get($series['intSeries_category']);
        $series = \Helpers\utf8Encode($series);
        foreach ($posts as &$post) {
            $post['viewDay'] = $this->postsHandle->calcDay($post);
            if ($post['strPost_nodeType'] == 'menu') {
                $where = [
                    'intPost_series_ID' => $post['intPost_series_ID'],
                    'intPost_parent' => $post['post_ID']
                ];
                $paths = $this->postsHandle->model->getPosts($where);
                $post['paths'] = $paths;
            }
        }
        $this->seriesHandle->filterViewRecord($series['series_ID']);
        $this->seriesHandle->filterAnyViewRecord($series['series_ID']);
        $series['permission'] = $this->seriesHandle->getRole($series);
        $shareData = [
            'image' => \Helpers\isAbsUrl($series['strSeries_image']) ? $series['strSeries_image'] : BASE_URL . '/' . $series['strSeries_image'],
        ];
        list($width, $height) = getimagesize($shareData['image']);
        if ($width && $height) {
            $width = $width > 200 ? $width : 200;
            $height = $height > 200 ? $height : 200;
            $shareData['image_width'] = $width;
            $shareData['image_height'] = $height;
        }
        $this->load->view('Preview_Series_v', ['posts' => $posts, 'series' => $series, 'permission' => $series['permission'], 'isLoggedIn' => $isLoggedIn, 'viewUri' => $viewUri, 'uriTitle' => $uriTitle, 'stripeApi_pKey' => $stripeApi_pKey, 'postsCount' => $postsCount, 'category' => $category, 'shareData' => $shareData]);
    }
    public function indexV2() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if (!$id){ echo 'no id'; die(); }

        $series = $this->seriesModel->get($id);

        if (!$this->seriesHandle->isAccessible($series)) {
            echo "you don't have permission for this series.";
            die();
        }

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        $series['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;

        $posts = $this->postsHandle->getSerialItems(['intPost_series_ID' => $id, 'intPost_parent' => 0], '*', ['intPost_order', 'post_ID'], 100);
        $viewUri = rawurlencode(BASE_URL . '/preview_series?id=' . $id . '&affiliate_id=' . $_SESSION['affiliate_id']);
        $uriTitle = rawurlencode($series['strSeries_title']);
        $isLoggedIn = $this->isLoggedIn;

        $owner = $this->userHandle->get($series['intSeries_client_ID']);
        $series['stripe_account_id'] = $owner['stripe_account_id'];
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';

        $postsCount = $this->model->countPosts(['intPost_series_ID' => $id]);

        $category = $this->categoryModel->get($series['intSeries_category']);
        $series = \Helpers\utf8Encode($series);
        foreach ($posts as &$post) {
            $post['viewDay'] = $this->postsHandle->calcDay($post);
            if ($post['strPost_nodeType'] == 'menu') {
                $where = [
                    'intPost_series_ID' => $post['intPost_series_ID'],
                    'intPost_parent' => $post['post_ID']
                ];
                $paths = $this->postsHandle->model->getPosts($where);
                $post['paths'] = $paths;
            }
        }
        $this->seriesHandle->filterViewRecord($series['series_ID']);
        $this->seriesHandle->filterAnyViewRecord($series['series_ID']);
        $series['permission'] = $this->seriesHandle->getRole($series);
        $shareData = [
            'image' => \Helpers\isAbsUrl($series['strSeries_image']) ? $series['strSeries_image'] : BASE_URL . '/' . $series['strSeries_image'],
        ];
        list($width, $height) = getimagesize($shareData['image']);
        if ($width && $height) {
            $width = $width > 200 ? $width : 200;
            $height = $height > 200 ? $height : 200;
            $shareData['image_width'] = $width;
            $shareData['image_height'] = $height;
        }
        $creator = $this->userHandle->model->get($series['intSeries_client_ID']);
        $joinCount = $this->purchasedModel->count(['intPurchased_series_ID' => $id]);
        $series['join_count'] = $joinCount;
        $series['favorite_count'] = $this->favoriteModel->count(['intFavorite_series_ID' => $id]);
        $this->load->view('Preview_Series_v2', ['posts' => $posts, 'series' => $series, 'creator' => $creator, 'permission' => $series['permission'], 'isLoggedIn' => $isLoggedIn, 'viewUri' => $viewUri, 'uriTitle' => $uriTitle, 'stripeApi_pKey' => $stripeApi_pKey, 'postsCount' => $postsCount, 'category' => $category, 'shareData' => $shareData, 'joinCount' => $joinCount]);
    }
}

$handle = new Preview_Series();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->indexV2();
            break;
    }
}
else {
    $handle->indexV2();
}
