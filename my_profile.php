<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.11.2018
 * Time: 07:44
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class My_Profile extends \Core\Controller_core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->model = new \Models\api\User_m();
    }
    public function index() {
        $user = $this->model->get($_SESSION['client_ID'], 'id, username, email, f_name, l_name, phone, image, about_me, social_facebook, social_twitter, social_linkedin, social_pinterest');
        $this->load->view('My_Profile_v', ['user' => $user]);
    }
}


$handle = new My_Profile();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}