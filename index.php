<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 15.08.2018
 * Time: 17:48
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Index extends \Core\Controller_core
{
    private $categoryModel;
    private $seriesHandle;
    private $seriesModel;
    private $postsModel;
    private $optionsModel;
    private $userHandle;
    private $purchasedModel;

    public function __construct()
    {
        parent::__construct();

        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }

        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();
        $this->seriesModel = $this->seriesHandle->model;
    }
    public function index() {
        $categories = $this->categoryModel->getRows();
        $series = $this->seriesHandle->getAvailableRows();
        $series = \Helpers\utf8Encode($series);
        foreach ($series as &$sery) {
            $sery['itemsCount'] = $this->postsModel->countPosts(['intPost_series_ID' => $sery['series_ID']]);
            $owner = $this->userHandle->get($sery['intSeries_client_ID']);
            $sery['stripe_account_id'] = $owner['stripe_account_id'];

            $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $sery['series_ID']]);
            $sery['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;
        }
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';
        $this->load->view('home_v', ['categories' => $categories, 'series' => $series, 'stripeApi_pKey' => $stripeApi_pKey]);
    }
}

$handle = new Index();
$handle->index();