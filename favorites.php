<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.02.2018
 * Time: 10:57
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/Favorites_m.php';

use Core\Controller_core;
use Models\Favorites_m;

class Favorites extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Favorites_m();
    }
    public function index(){
        $purchasedSeries = $this->model->getPurchasedSeries();
        foreach ($purchasedSeries as & $purchasedSery){
            $subscriptions = $this->model->getFavoriteSubsByPurchasedId($purchasedSery['purchased_ID']);
            $purchasedSery['subscriptions'] = $subscriptions;
        }
        $this->load->view('Favorites_v', ['purchasedSeries' => $purchasedSeries]);
    }
    public function ajax_makeFavorite(){
        $sets = [
            'intFavorite_subscription_ID' => $_POST['subsId'],
            'intFavorite_series_ID' => $_POST['seriesId'],
        ];

        if (isset($_POST['postId'])){
            $sets['intFavorite_post_ID'] = $_POST['postId'];
        }
        $newId = $this->model->insertFavorite($sets);
        echo json_encode(['status' => true, 'data' => $newId]);
        die();
    }
    public function ajax_makeUnFavorite(){
        $id = $_POST['id'];
        $this->model->deleteFavorite($id);
        echo json_encode(['status' => true]);
        die();
    }
}

$favoritesHandle = new Favorites();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'make_favorite':
            $favoritesHandle->ajax_makeFavorite();
            break;
        case 'make_unFavorite':
            $favoritesHandle->ajax_makeUnFavorite();
            break;
        default:
            $favoritesHandle->index();
            break;
    }
}
else{
    $favoritesHandle->index();
//    $favoritesHandle->test();
}