<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.11.2018
 * Time: 16:02
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Create_Series extends \Core\Controller_core
{
    private $categoryModel;
    private $optionModel;

    public function __construct()
    {
        parent::__construct();

        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }

        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Options_m');
        $this->optionModel = new \Models\api\Options_m();
    }
    public function index() {

        $stripeClientRow = $this->optionModel->get(['strOption_name' => 'stripe_client_id']);
        $stripeClientId = $stripeClientRow ? $stripeClientRow['strOption_value'] : '';
        $categories = $this->categoryModel->getRows();
        $allowUnApprovedUserCanCreatePublicSeries = $this->optionModel->getOption('allow_unApprovedUser_create_publicSeries');
        $allowUnApprovedUserCanCreatePublicSeries = $allowUnApprovedUserCanCreatePublicSeries !== 'undefined' ? (int) $allowUnApprovedUserCanCreatePublicSeries : false;
        $this->load->view('Create_Series_v', ['categories' => $categories, 'stripeClientId' => $stripeClientId, 'allowUnApprovedUserCanCreatePublicSeries' => $allowUnApprovedUserCanCreatePublicSeries]);
    }
}


$handle = new Create_Series();

if (isset($_POST['action']) || isset($_GET['action'])){
    switch ($_GET['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}
