<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 06.04.2018
 * Time: 12:53
 */

namespace Pages;


use Controllers\Auth_c;
use Core\Controller_Core;
use function Helpers\parseInput;

class Auth_p extends Controller_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->controller('Auth');
        $this->ctrl = new Auth_c();
    }
    public function ajax_login(){
        $input = $_POST;
        $rlt = $this->ctrl->login($input['username'], $input['password']);
        echo json_encode($rlt);
        die();
    }
    public function ajax_register(){
        $input = $_POST;
        $rlt = $this->ctrl->register($input);
        echo json_encode($rlt);
        die();
    }
    public function ajax_logout(){
        $this->ctrl->logout();
    }
}