<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 30.08.2018
 * Time: 16:03
 */

namespace Pages;


use Core\Access_Core;
use Firebase\JWT\JWT;
use function Helpers\jwtDecode;
use function Helpers\jwtEncode;
use function Helpers\pushDecode;
use function Helpers\pushEncode;
use Models\Options_m;

class Secretes_p extends Access_Core
{
    private $optionsModel;
    private $jwtKey = 'oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Options');
        $this->optionsModel = new Options_m();
    }
    public function ajax_get() {
        $res = [];
        $stripeApi_pKey_row = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $res['stripeApi_pKey'] = $stripeApi_pKey_row ? $stripeApi_pKey_row['strOption_value'] : '';
        $stripeApi_sKey_row = $this->optionsModel->get(['strOption_name' => 'stripeApi_sKey']);
        $res['stripeApi_sKey'] = $stripeApi_sKey_row ? jwtDecode($stripeApi_sKey_row['strOption_value']) : '';
        $stripe_client_row = $this->optionsModel->get(['strOption_name' => 'stripe_client_id']);
        $res['stripe_client_id'] = $stripe_client_row ? $stripe_client_row['strOption_value'] : '';
        echo json_encode($res);
        die();
    }
    public function ajax_save() {
        $stripeApi_pKey = $_POST['stripeApi_pKey'];
        $stripeApi_sKey = $_POST['stripeApi_sKey'];
        $stripe_client_id = $_POST['stripe_client_id'];
        $this->optionsModel->forceUpdate(['strOption_name' => 'stripeApi_pKey'], ['strOption_value' => $stripeApi_pKey]);
        $this->optionsModel->forceUpdate(['strOption_name' => 'stripeApi_sKey'], ['strOption_value' => jwtEncode($stripeApi_sKey)]);
        $this->optionsModel->forceUpdate(['strOption_name' => 'stripe_client_id'], ['strOption_value' => $stripe_client_id]);
        echo json_encode(['status' => true]);
        die();
    }
}
