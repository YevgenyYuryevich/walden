<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 27.04.2018
 * Time: 00:13
 */

namespace Pages;


use Core\Access_Core;
use Models\UserMeta_m;
use Models\Users_m;

class UserPermissions_p extends Access_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users');
        $this->load->model('UserMeta');
        $this->usersModel = new Users_m();
        $this->userMetaModel = new UserMeta_m();
    }
    public function ajax_index(){
        $users = $this->usersModel->getRows();
        foreach ($users as & $user) {
            $banPublicSeries = $this->userMetaModel->get(['client_id' => $user['id'], 'meta_key' => 'ban_create_public_series']);
            $user['ban_create_public_series'] = $banPublicSeries ? $banPublicSeries['meta_value'] : 0;
        }
        echo json_encode($users);
        die();
    }
}