<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 25.05.2018
 * Time: 12:39
 */

namespace Pages;


use Core\Access_Core;
use Models\Invitations_m;
use Models\Logs_m;
use Models\Purchased_m;
use Models\Series_m;
use Models\Users_m;

class Stats_p extends Access_Core
{
    private $usersModel;
    private $invitationsModel;
    private $logsModel;
    private $seriesModel;
    private $purchasedModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users');
        $this->load->model('Invitations');
        $this->load->model('Logs');
        $this->load->model('Series');
        $this->load->model('Purchased');

        $this->usersModel = new Users_m();
        $this->invitationsModel = new Invitations_m();
        $this->logsModel = new Logs_m();
        $this->seriesModel = new Series_m();
        $this->purchasedModel = new Purchased_m();
    }
    public function ajax_get(){
        $res = [];

        $res['totalUsersCnt'] = $this->usersModel->count();
        $res['waitUsersCnt'] = $this->invitationsModel->count(['intInvitation_status' => 1]);
        $res['avgWaitDate'] = $this->invitationsModel->waitListAvg();
        $activeUsersCnt = 0;
        $totalUsers = $this->usersModel->getRows();
        $res['totalUsers'] = $totalUsers;
        $firstDayLastMonth = date("Y-m-d h:m:s", strtotime("first day of previous month"));
        foreach ($totalUsers as $user) {
            $loggedIn = $this->logsModel->count(['member' => $user['id'], 'success' => 1, 'time >=' => $firstDayLastMonth]);
            if ($loggedIn) {
                $activeUsersCnt++;
            }
        }
        $res['activeUsersCnt'] = $activeUsersCnt;

//      end account stats

        $totalSeries = $this->seriesModel->getRows([], 'series_ID, dtSeries_daterequested, strSeries_title, boolSeries_approved');
        $seriesThisMonth = [];
        $thisMonthSeries = [];
        $res['totalSeriesCnt'] = count($totalSeries);
        $thisMonth = date("Y-m-d", strtotime("first day of this month"));
        $res['totalSeriesCntThisMonth'] = 0;
        $res['totalSeriesJoinedCnt'] = $this->purchasedModel->count();
        $res['totalSeriesJoinedCntThisMonth'] = $this->purchasedModel->count(['dtPurchased_startdate >=' => $thisMonth]);
        foreach ($totalSeries as & $sery) {
            $sery['joinedCnt'] = $this->purchasedModel->count(['intPurchased_series_ID' => $sery['series_ID']]);
            $sery['joinedCntThisMonth'] = $this->purchasedModel->count(['intPurchased_series_ID' => $sery['series_ID'], 'dtPurchased_startdate >=' => $thisMonth]);
            if ($sery['dtSeries_daterequested'] >= $thisMonth) {
                $res['totalSeriesCntThisMonth']++;
                $thisMonthSeries[] = $sery;
                array_push($seriesThisMonth, $sery);
            }
        }
        $res['series'] = $totalSeries;
        $res['seriesThisMonth'] = $seriesThisMonth;
        $res['purchasedRows'] = $this->purchasedModel->getRows();
        echo json_encode($res);
        die();
    }
}