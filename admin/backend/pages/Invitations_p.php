<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.04.2018
 * Time: 06:47
 */

namespace Pages;

require_once __DIR__ . './../models/Invitations_m.php';

use function Helpers\{parseInput, strongEncrypt, htmlMailHeader};
use Models\Invitations_m;

class Invitations_p
{
    public function __construct()
    {
        $this->model = new Invitations_m();
    }
    public function ajax_sendInvitation(){
        $input = $_POST;
        $row = $this->model->get($input['where']);
        $invitedEmail = $row['strInvitedEmail'];
        $token = strongEncrypt($invitedEmail . time());

        $headers = htmlMailHeader();
        $msg = file_get_contents(BASE_URL . '/assets/email-templates/You_are_invited.php?accept_url=' . urlencode(BASE_URL . '/login?register&token=' . $token));
        mail($invitedEmail, 'You are invited to theGreyshirt', $msg, $headers);

        $sets = ['strToken' => $token, 'intInvitation_status' => 2];
        $this->model->update($input['where'], $sets);
        echo json_encode(array_merge($row, $sets));
        die();
    }
}