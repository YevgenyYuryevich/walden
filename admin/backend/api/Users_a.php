<?php


namespace Api;


use Core\Access_Core;
use Models\Users_m;

class Users_a extends Access_Core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users');
        $this->model = new Users_m();
    }
    public function ajax_getMany() {
        $items = $this->model->getRows([], 'id, username, email, f_name, l_name, phone, image, level, level, active, created_date, about_me, social_facebook, social_twitter, social_pinterest, social_linkedin');
        echo json_encode($items);
        die();
    }
    public function ajax_update() {
        $where = $_POST['where'];
        $sets = $_POST['sets'];
        $this->model->update($where, $sets);
        echo json_encode($sets);
        die();
    }
}
