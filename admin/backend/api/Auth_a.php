<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 16:16
 */

namespace Api;

use Core\Access_Core;
use Models\Contractor_m;

class Auth_a extends Access_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Contractor');
        $this->model = new Contractor_m();
    }
    private function forceLogin(){
        if ($_SESSION['client_ID'] == 4){
            $contractor = $this->model->get(4);
        }
        else if($_SESSION['client_ID'] == 6){
            $contractor = $this->model->get(2);
        }
        else if($_SESSION['client_ID'] == 7){
            $contractor = $this->model->get(5);
        }
        $_SESSION['contractor'] = $contractor;
    }
    public function get(){
        echo json_encode($_SESSION['contractor']);
        die();
    }
}