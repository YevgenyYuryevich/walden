<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.04.2018
 * Time: 12:14
 */

namespace Api;


use Controllers\Series_c;
use Core\Access_Core;
use function Helpers\utf8Encode;
use Models\Series_m;

class Series_a extends Access_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->controller('Series');
        $this->load->model('Series');
        $this->ctrl = new Series_c();
        $this->model = new Series_m();
    }
    public function ajax_getItems(){
        $input = $_POST;
        $where = isset($input['where']) ? $input['where'] : [];
        $items = $this->ctrl->getItems($where);
        foreach ($items as & $item){
            $this->ctrl->formatItem($item);
        }
        $items = utf8Encode($items);
        echo json_encode($items);
        die();
    }
    public function ajax_update(){
        $input = $_POST;
        $where = $input['where'];
        $sets = $input['sets'];
        $rlt = $this->model->update($where, $sets);
        echo json_encode($rlt);
        die();
    }
    public function ajax_delete(){
        $input = $_POST;
        $where = $input['where'];
        $rlt = $this->ctrl->delete($where);
        echo json_encode($rlt);
        die();
    }
    public function ajax_get(){
        $input = $_POST;
        $where = $input['where'];
        $item = $this->model->get($where);
        $this->ctrl->formatItem($item);
        echo json_encode($item);
        die();
    }
}