<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 19:16
 */

namespace Api;

use Core\Access_Core;
use Models\Invitations_m;

require_once __DIR__ . './../models/Invitations_m.php';

class Invitations_a extends Access_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Invitations_m();
    }
    public function ajax_get(){
        $input = $_POST;
        $where = $input['where'];
        $rows = $this->model->getRows($where);
        echo json_encode($rows);
        die();
    }
}