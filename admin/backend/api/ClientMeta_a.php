<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 27.04.2018
 * Time: 01:26
 */

namespace Api;


use Core\Access_Core;
use Models\UserMeta_m;

class ClientMeta_a extends Access_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserMeta');
        $this->model = new UserMeta_m();
    }
    public function ajax_update(){
        $input = $_POST;
        $where = $input['where'];
        $sets = $input['sets'];
        $rlt = $this->model->update($where, $sets);
        echo json_encode($rlt);
        die();
    }
}