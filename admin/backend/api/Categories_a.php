<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.10.2018
 * Time: 12:40
 */

namespace Api;


use Controllers\Categories_c;
use Core\Access_Core;

class Categories_a extends Access_Core
{
    private $model;
    private $ctrl;
    public function __construct()
    {
        parent::__construct();
        $this->load->controller('Categories');
        $this->ctrl = new Categories_c();
        $this->model = $this->ctrl->model;
    }
    public function ajax_getItems() {
        $input = $_POST;
        $where = isset($input['where']) ? $input['where'] : [];
        $rows = $this->model->getRows($where);
        foreach ($rows as &$row) {
            $row = $this->ctrl->formatItem($row);
        }
        echo json_encode($rows);
        die();
    }
    public function ajax_merge() {
        $input = $_POST;
        $to = $input['to'];
        $where = $input['where'];
        $this->ctrl->mergeTo($where, $to);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_update() {
        $input = $_POST;
        $where = $input['where'];
        $sets = $input['sets'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true]);
        die();
    }
}