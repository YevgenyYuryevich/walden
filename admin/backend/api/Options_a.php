<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 11.05.2018
 * Time: 05:23
 */

namespace Api;


use Core\Access_Core;
use Models\Options_m;

class Options_a extends Access_Core
{
    public $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Options');
        $this->model = new Options_m();
    }
    public function ajax_get(){
        $input = $_POST;
        $where = $input['where'];
        $row = $this->model->get($where);
        echo json_encode($row);
        die();
    }
    public function ajax_getItems(){
        $input = $_POST;
        $where = $input['where'];
        $rows = $this->model->getRows($where);
        $fRows = [];
        foreach ($rows as $row) {
            $fRows[$row['strOption_name']] = $row['strOption_value'];
        }
        echo json_encode($fRows);
        die();
    }
    public function ajax_update(){
        $input = $_POST;
        $where = $input['where'];
        $sets = $input['sets'];
        $rlt = $this->model->forceUpdate($where, $sets);
        echo json_encode($rlt);
        die();
    }
}