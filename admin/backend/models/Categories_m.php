<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.04.2018
 * Time: 12:25
 */

namespace Models;


use Core\Model_Core;

class Categories_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::CATEGORIES;
        $this->PRIMARY_KEY = 'category_ID';
    }
}