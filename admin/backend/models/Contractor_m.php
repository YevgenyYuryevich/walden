<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 16:10
 */

namespace Models;

use Core\Model_Core;

class Contractor_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::CONTRACTOR;
        $this->PRIMARY_KEY = 'contractor_ID';
    }
}