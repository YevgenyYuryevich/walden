<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 28.05.2018
 * Time: 14:43
 */

namespace Models;


use Core\Model_Core;

class Purchased_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::PURCHASED_TNAME;
        $this->PRIMARY_KEY = 'purchased_ID';
    }
}