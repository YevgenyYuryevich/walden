<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 11.05.2018
 * Time: 05:25
 */

namespace Models;


use Core\Model_Core;

class Options_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::OPTION_TNAME;
        $this->PRIMARY_KEY = 'option_ID';
    }
}