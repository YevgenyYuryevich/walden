<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.04.2018
 * Time: 12:16
 */

namespace Models;


use Core\Model_Core;

class Series_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::SERIES_TNAME;
        $this->PRIMARY_KEY = 'series_ID';
    }
}