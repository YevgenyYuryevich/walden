<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 25.05.2018
 * Time: 15:36
 */

namespace Models;


use Core\Model_Core;

class Logs_m extends Model_Core
{
    public function __construct(string $database = 'guardian')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::LOGS_TNAME;
        $this->PRIMARY_KEY = 'id';
    }
}