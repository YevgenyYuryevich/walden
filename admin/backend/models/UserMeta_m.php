<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 27.04.2018
 * Time: 00:39
 */

namespace Models;


use Core\Model_Core;

class UserMeta_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::CLIENT_META_TNAME;
        $this->PRIMARY_KEY = 'clientMeta_id';
    }
    public function update($where = [], $sets = []){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        if ($this->get($where)){
            $this->db->from($this->TNAME);
            $this->db->set($sets);
            $this->db->where($where);
            return $this->db->update();
        }
        return $this->insert(array_merge($where, $sets));
    }
}