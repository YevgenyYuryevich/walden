<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 06.04.2018
 * Time: 17:41
 */

namespace Models;

use Core\Model_Core;
use function Helpers\pushDecode;

class Users_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct('guardian');
        $this->TNAME = $this->db::USERS_TNAME;
        $this->PRIMARY_KEY = 'id';
    }
    public function getRows($where = [], $selects = '*'){
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $rows =  $this->db->get();
        foreach ($rows as &$row) {
            if (isset($row['stripeApi_pKey'])) {
                $row['stripeApi_pKey'] = pushDecode($row['stripeApi_pKey']);
            }
            if (isset($row['stripeApi_cUrl'])) {
                $row['stripeApi_cUrl'] = pushDecode($row['stripeApi_cUrl']);
            }
        }
        return $rows;
    }
    public function get($where = [], $selects = '*', $orderBy = [], $limit = false){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->orderBy($orderBy);
        if ($limit){
            $this->db->limit($limit);
        }
        $row = $this->db->get(true);
        if ($row) {
            $row['stripeApi_pKey'] = pushDecode($row['stripeApi_pKey']);
            $row['stripeApi_cUrl'] = pushDecode($row['stripeApi_cUrl']);
            return $row;
        }
        else {
            return $row;
        }
    }
}
