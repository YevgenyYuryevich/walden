<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 19:17
 */

namespace Models;

use Core\Model_Core;

class Invitations_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::CLIENTINVITE_TNAME;
        $this->PRIMARY_KEY = 'clientInvitation_ID';
    }
    public function waitListAvg() {
        $rows = $this->getRows(['intInvitation_status' => 1], 'dtClientInvitation_date');
        $totalTime = 0;
        if ($rows) {
            foreach ($rows as $row) {
                $totalTime += strtotime($row['dtClientInvitation_date']);
            }
            $avgTime = $totalTime / count($rows);
            $avg = date('Y-m-d H:i:s', $avgTime);
            return $avg;
        }
        return 'There is no wait list';
    }
}