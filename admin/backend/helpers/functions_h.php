<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 19:21
 */

namespace Helpers;

function parseInput(){
    $input = file_get_contents("php://input");
    return json_decode(json_encode(json_decode($input)), true);
}
function strongEncrypt($password){
    $strong1 = hash_pbkdf2('haval192,4', $password, 'nbNB12_+' . substr($password, 0, 3), 50);
    $strong2 = hash_pbkdf2('gost-crypto', $strong1, 'yvgYVG1@<[~' . substr($password, 3), 100);
    $strong3 = substr($strong2, 1) . $strong2[0];
    return $strong3;
}
function htmlMailHeader($fromName = 'Grey Shirt', $fromEmail = 'nbaca@mtnlegal.com', $replyEmail = 'nbaca@mtnlegal.com'){
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";
    $headers .= "From: " . $fromName . " <" . $fromEmail . ">\r\n";
    $headers .= "Reply-To: <" . strip_tags($replyEmail) . ">\r\n";
    return $headers;
}

function utf8Encode($arr){
    if (!is_array($arr)){
        return utf8_encode($arr);
    }
    foreach ($arr as &$value){
        $value = utf8Encode($value);
    }
    return $arr;
}

function baseUrl(){
    return sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
}
function hostName() {
    $host = array_reverse(explode('.',$_SERVER['HTTP_HOST']));
    return $host[1];
}

function strToAscii($str) {
    $arr = [];
    for ( $pos = 0; $pos < strlen($str); $pos ++ ) {
        $byte = substr($str, $pos);
        $arr[] = ord($byte);
    }
    return $arr;
}
function asciiToStr($arr) {
    $str = '';
    foreach ($arr as $v) {
        $str .= chr((int)$v);
    }
    return $str;
}
function asciiEncode($a) {
    return ($a * 1 + 133) % 255;
}
function asciiDecode($a) {
    return (255 + $a * 1 - 133) % 255;
}
function pushEncode($str) {
    $arr = strToAscii($str);
    $encodeArr = [];
    foreach ($arr as $v) {
        $encodeArr[] = asciiEncode($v);
    }
    $encodeArr = array_reverse($encodeArr);
    return asciiToStr($encodeArr);
}
function pushDecode($str) {
    $arr = strToAscii($str);
    $decodeArr = [];
    foreach ($arr as $v) {
        $decodeArr[] = asciiDecode($v);
    }
    $decodeArr = array_reverse($decodeArr);
    return asciiToStr($decodeArr);
}

define('BASE_URL', baseUrl());
define('ADMIN_ROOT', realpath($_SERVER['DOCUMENT_ROOT']) . '/admin');

function jwtEncode($plainText) {
    return \Firebase\JWT\JWT::encode($plainText, 'wa!den=p@rf$ct');
}

function jwtDecode($hashedText) {
    return \Firebase\JWT\JWT::decode($hashedText, 'wa!den=p@rf$ct', ['HS256']);
}
