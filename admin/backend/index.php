<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 13:11
 */

require_once './../../yevgeny/vendor/autoload.php';
require_once 'helpers/functions_h.php';
require_once 'core/Loader_Core.php';
require_once 'core/Controller_Core.php';
require_once 'core/Access_Core.php';
require_once 'core/Router_Core.php';
require_once 'core/Database_m_Core.php';
require_once 'core/Model_Core.php';

class Index
{
    public function __construct()
    {
        $this->router = new \Core\Router_Core();
    }
    public function index(){
        $this->router->go();
    }
}

$handle = new Index();

$handle->index();
