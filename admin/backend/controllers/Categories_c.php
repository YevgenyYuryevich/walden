<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.10.2018
 * Time: 12:45
 */

namespace Controllers;


use Core\Controller_Core;
use Models\Categories_m;
use Models\Series_m;

class Categories_c extends Controller_Core
{
    public $model;
    private $seriesModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categories');
        $this->model = new Categories_m();
        $this->load->model('Series');
        $this->seriesModel = new Series_m();
    }
    public function formatItem($row) {
        $row['strCategory_image'] = BASE_URL . '/' . $row['strCategory_image'];
        return $row;
    }
    public function mergeTo($where, $to) {
        $rows = $this->model->getRows($where);
        foreach ($rows as $row) {
            $this->seriesModel->update(['intSeries_category' => $row['category_ID']], ['intSeries_category' => $to]);
            $this->model->delete($row['category_ID']);
        }
        return true;
    }
}