<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.04.2018
 * Time: 12:17
 */

namespace Controllers;


use Core\Controller_Core;
use Models\Categories_m;
use Models\Series_m;
use Models\Users_m;

class Series_c extends Controller_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Series');
        $this->load->model('Users');
        $this->load->model('Categories');
        $this->model = new Series_m();
        $this->usersModel = new Users_m();
        $this->categoriesModel = new Categories_m();
    }
    public function getItems($where = []){
        return $this->model->getRows($where);
    }
    public function formatItem(& $item){
        $user = $this->usersModel->get($item['intSeries_client_ID'], ['f_name', 'l_name', 'stripeApi_pKey', 'stripeApi_cUrl']);
        $category = $this->categoriesModel->get($item['intSeries_category'], 'strCategory_name');
        $item['strSeries_image'] = BASE_URL . '/' . $item['strSeries_image'];
        $item['series_client'] = $user;
        $item['series_category'] = $category;
        return $item;
    }
    public function delete($where = []){
        return $this->model->delete($where);
    }
}