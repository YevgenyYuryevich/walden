<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 06.04.2018
 * Time: 07:01
 */

namespace Controllers;

use Core\Controller_Core;
use function Helpers\hostName;
use function Helpers\strongEncrypt;
use Models\Contractor_m;
use Models\Users_m;

class Auth_c extends Controller_Core
{
    public function __construct()
    {
        $this->startSession();
        parent::__construct();

        $this->load->model('Users');
        $this->load->model('Contractor');

        $this->usersModel = new Users_m();
        $this->model = new Contractor_m();
    }

    public function startSession(){
        session_name('contractor');
        session_set_cookie_params(3600*24*7,"/");
        session_save_path(ADMIN_ROOT . '/backend/session');
        if (!@session_start()) throw new Exception("Unable to start a session");
    }
    public function login($username, $password){
        $password = strongEncrypt($password);
        $row = $this->model->get(['strContractor_email' => $username, 'strContractor_password' => $password]);
        $_SESSION['contractor'] = $row;
        setcookie(hostName(), session_id(), time()+365*24*3600, "/");
        return $row ? $row : false;
    }
    public function register($sets){
        $user = $this->usersModel->get(['email' => $sets['strContractor_email'], 'level' => 0]);
        if (!$user){
            return ['status' => false, 'error' => 'This email is not allowed'];
        }
        $existingRow = $this->model->get(['strContractor_email' => $sets['strContractor_email']]);
        if ($existingRow){
            return ['status' => false, 'error' => 'This eamil is already existing'];
        }
        $sets['strContractor_password'] = strongEncrypt($sets['strContractor_password']);
        $id = $this->model->insert($sets);
        return ['status' => true, 'data' => $id];
    }
    public function logout(){
        unset($_SESSION['contractor']);
        setcookie(hostName(), '', time()-3600, "/");
        session_destroy();
    }
    public function checkLoggedIn(){
        if (isset($_SESSION['contractor']) && $_SESSION['contractor']){
            return true;
        }
        return false;
    }
}