<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 06.04.2018
 * Time: 09:56
 */

namespace Core;


class Controller_Core
{
    public function __construct()
    {
        $this->load = new Loader_Core($this);
    }
}