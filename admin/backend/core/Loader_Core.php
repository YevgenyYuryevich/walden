<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 12:52
 */

namespace Core;

class Loader_Core
{
    public function __construct($parent = null)
    {
        $this->RootPath = realpath($_SERVER['DOCUMENT_ROOT']) . '/admin';
        $this->LoadPath = $this->RootPath . '/backend';
        $this->PagePath = $this->LoadPath . '/pages';
        $this->ApiPath = $this->LoadPath . '/api';
        $this->ControllerPath = $this->LoadPath . '/controllers';
        $this->ModelPath = $this->LoadPath . '/models';
        $this->ViewPath = $this->LoadPath . '/views';
        $this->ConfigPath = $this->LoadPath . '/config';
        $this->HelperPath = $this->LoadPath . '/helpers';
        $this->parent = $parent;
    }
    public function view($viewName, $params = []){
        $viewNamePath = $this->ViewPath . '/' . $viewName . '.php';

        foreach ($params as $key => $param){
            ${$key} = $param;
        }
        require_once $viewNamePath;
    }
    public function model($modelName, $asName = 'model'){
        $modelName .= '_m';
        $modelNamePath = $this->ModelPath . '/' . $modelName . '.php';
        require_once $modelNamePath;
        $className = 'Models\\' . $modelName;
//        $this->parent->$asName = new $className();
//        return $this->parent->$asName;
    }
    public function page($pageName, $asName = 'page'){
        $pageName .= '_p';
        $pageNamePath = $this->PagePath . '/' . $pageName . '.php';
        require_once $pageNamePath;
        $className = 'Pages\\' . $pageName;
        $this->parent->$asName = new $className();
    }
    public function api($className, $asName = 'api'){
        $className .= '_a';
        $classNamePath = $this->ApiPath . '/' . $className . '.php';
        require_once $classNamePath;
        $className = 'Api\\' . $className;
        $this->parent->$asName = new $className();
    }
    public function controller($className, $asName = 'controller'){
        $className .= '_c';
        $classNamePath = $this->ControllerPath . '/' . $className . '.php';
        require_once $classNamePath;
//        $className = 'Controllers\\' . $className;
//        $this->parent->$asName = new $className();
    }
}