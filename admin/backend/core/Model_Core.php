<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 19:43
 */

namespace Core;

class Model_Core
{
    public $TNAME;
    public $PRIMARY_KEY;

    public function __construct($database = 'greyshirt')
    {
        $this->db = new Database_m_Core($database);
    }
    public function getRows($where = [], $selects = '*'){

        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }

        $this->db->select($selects);
        $this->db->from($this->TNAME);
        foreach ($where as $key => $value) {
            if (is_array($value)) {
                $this->db->groupStart();
                foreach ($value as $v) {
                    $this->db->orWhere($key, $v);
                }
                $this->db->groupEnd();
            }
            else {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get();
    }
    public function get($where = [], $selects = '*'){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $row = $this->db->get(true);
        return !is_null($row) ? $row : false;
    }
    public function delete($where = []){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function insert($sets){
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function update($where = [], $sets = []){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        $this->db->where($where);
        return $this->db->update();
    }
    public function forceUpdate($where = [], $sets = []) {
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        if ($this->get($where)){
            $this->db->from($this->TNAME);
            $this->db->set($sets);
            $this->db->where($where);
            return $this->db->update();
        }
        return $this->insert(array_merge($where, $sets));
    }
    public function count($where = [], $select = false) {
        $select = $select ? $select : $this->PRIMARY_KEY;
        $select = 'COUNT(' . $select . ') AS cnt';
        $this->db->select($select);
        $this->db->where($where);
        $this->db->from($this->TNAME);
        $row = $this->db->get(true);
        return $row ? (int) $row['cnt'] : 0;
    }
    public function avg($where = [], $select) {
        $select = 'AVG('. $select .') AS avg';
        $this->db->select($select);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $row = $this->db->get(true);
        return $row ? $row['avg'] : false;
    }
}
