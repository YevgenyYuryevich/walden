<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 12:48
 */

namespace Core;

require_once  __DIR__ . '/../config/routes.php';

class Router_Core
{
    public function __construct()
    {
        $this->load = new Loader_Core($this);
        global $routes;
        $this->routes = $routes;
    }
    public function requestUri(){
        $requestUri = $_SERVER['REQUEST_URI'];
        $parsed = explode('/', $requestUri);
        $dir = $parsed[3];
        $className = $parsed[4];
        $methodName = $parsed[5] ? $parsed[5] : 'index';
        return $dir . '/' . $className . '/' . $methodName;
    }
    public function parseRoute($route){
        $parsed = explode('/', $route);
        return [
            'dir' => $parsed[0],
            'class' => $parsed[1],
            'method' => $parsed[2]
        ];
    }
    public function go(){
        $request = $this->requestUri();
        $route = isset($this->routes[$request]) ? $this->routes[$request] : $request;
        $parsed = $this->parseRoute($route);
        switch ($parsed['dir']){
            case 'page':
                $this->load->page($parsed['class']);
                $method = $parsed['method'];
                $this->page->$method();
                break;
            case 'api':
                $this->load->api($parsed['class']);
                $method = $parsed['method'];
                $this->api->$method();
                break;
            case 'controller':
                $this->load->controller($parsed['class']);
                $method = $parsed['method'];
                $this->controller->$method();
                break;
        }
    }
}