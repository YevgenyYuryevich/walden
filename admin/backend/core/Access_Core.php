<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 16:25
 */

namespace Core;

use Models\Contractor_m;

class Access_Core extends Controller_Core
{
    public function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] == 'http://localhost:4200'){
            $this->startSession();
            $this->load->model('Contractor');
            $model = new Contractor_m();
            $_SESSION['contractor'] = $model->get(1);
        }
        else{
            $this->startSession();
            $this->filterLoggedIn();
        }
    }
    private function filterLoggedIn(){
        if (isset($_SESSION['contractor']) && $_SESSION['contractor']){
            return true;
        }
        else {
            http_response_code(403);
            die();
        }
    }
    private function startSession(){
        session_name('contractor');
        session_set_cookie_params(3600*24*7,"/");
        session_save_path(ADMIN_ROOT . '/backend/session');
        if (!@session_start()) throw new Exception("Unable to start a session");
    }
}