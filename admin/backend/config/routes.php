<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.04.2018
 * Time: 12:47
 */

$routes['default'] = 'page/start/index';
$routes['page/waitlist/get'] = 'page/waitlist/get';
$routes['api/auth/get'] = 'api/Auth/get';

$routes['api/invitations/get'] = 'api/Invitations/ajax_get';
$routes['page/invitations/sendInvitation'] = 'page/Invitations/ajax_sendInvitation';

$routes['page/auth/login'] = 'page/Auth/ajax_login';
$routes['page/auth/register'] = 'page/Auth/ajax_register';
$routes['page/auth/logout'] = 'page/Auth/ajax_logout';

$routes['api/series/get_items'] = 'api/Series/ajax_getItems';
$routes['api/series/update'] = 'api/Series/ajax_update';
$routes['api/series/delete'] = 'api/Series/ajax_delete';
$routes['api/series/get'] = 'api/Series/ajax_get';

$routes['page/UserPermissions/index'] = 'page/UserPermissions/ajax_index';
$routes['api/ClientMeta/update'] = 'api/ClientMeta/ajax_update';