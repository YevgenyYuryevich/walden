import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { AuthGuardService } from "./guards/auth-guard.service";
import { StatsComponent } from "./stats/stats.component";
import { StatsResolverService } from "./stats/stats-resolver.service";
import {SecretesComponent} from "./secretes/secretes.component";
import {SecretesResolveService} from "./secretes/secretes-resolve.service";

const Approutes: Routes = [
    {
        path: '',
        component: FullComponent,
        canActivate: [AuthGuardService],
        children: [
            { path: '', redirectTo: '/starter', pathMatch: 'full' },
            { path: 'starter', loadChildren: './starter/starter.module#StarterModule' },
            {
                path: 'stats', component: StatsComponent,
                resolve: {
                    stats: StatsResolverService,
                }
            },
            {
                path: 'secretes', component: SecretesComponent,
                resolve: {
                    secretes: SecretesResolveService,
                }
            },
            {
                path: 'accounts',
                loadChildren: './accounts/accounts.module#AccountsModule',
            },
            {
                path: 'categories',
                loadChildren: './categories/categories.module#CategoriesModule'
            },
            {
                path: 'series',
                loadChildren: './series/series.module#SeriesModule'
            },
            {
                path: 'settings',
                loadChildren: './settings/settings.module#SettingsModule',
            },
            { path: 'component', loadChildren: './component/test.component.module#TestComponentModule' },
        ]
    },
    {
        path: 'authorize',
        loadChildren: './authorize/authorize.module#AuthorizeModule',
    }
];

@NgModule({
    imports: [RouterModule.forRoot(Approutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }