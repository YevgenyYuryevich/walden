import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService } from "../services/alert.service";
import {AjaxApiService} from "../../services/ajax-api.service";

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: ['register.component.scss']
})

export class RegisterComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private ajaxApi: AjaxApiService,
        private alertService: AlertService) { }

    register() {
        this.loading = true;
        this.ajaxApi.pagePost('auth/register', this.model, false)
            .then(
                res => {
                    this.loading = false;
                    if (res.status){
                        this.alertService.success('Registration successful', true);
                        this.router.navigate(['/authorize/login']);
                    }
                    else {
                        this.alertService.error(res.error);
                    }
                },
                (error: any) => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}