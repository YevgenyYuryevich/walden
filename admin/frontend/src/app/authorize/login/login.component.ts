import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {AuthService} from "../../services/auth.service";
import {AlertService} from "../services/alert.service";
import {NgForm} from "@angular/forms";

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss'],
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthService,
        private alertService: AlertService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .then(
                (data) => {
                    this.loading = false;
                    if (data){
                        this.router.navigate([this.returnUrl]);
                    }
                    else {
                        this.alertService.error('Incorrect Username or Password');
                    }
                },
                (error) => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}