import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorizeRoutingModule } from './authorize-routing.module';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './alert/alert.component';
import { FormsModule } from "@angular/forms";
import {AlertService} from "./services/alert.service";
import { RegisterComponent } from './register/register.component';

@NgModule({
    imports: [
        CommonModule,
        AuthorizeRoutingModule,
        FormsModule
    ],
    declarations: [LoginComponent, AlertComponent, RegisterComponent],
    providers: [AlertService]
})
export class AuthorizeModule { }
