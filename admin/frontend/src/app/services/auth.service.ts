import { Injectable } from '@angular/core';
import {AjaxApiService} from "./ajax-api.service";

@Injectable()
export class AuthService {

    auth: any = false;
    isLoggedIn: boolean = false;
    redirectUrl: string;

    constructor(private ajaxApi: AjaxApiService) {
    }

    authorization(){
        if (this.auth){
            return Promise.resolve(this.auth);
        }
        else {
            return this.ajaxApi.apiPost('auth/get', {}, false).then((data) => {
                this.auth = data;
                this.isLoggedIn = true;
                return this.auth;
            }, (error) => { this.isLoggedIn = false; console.log(error); });
        }
    }

    login(username: string, password: string) {
            return this.ajaxApi.pagePost ('auth/login', { username: username, password: password })
                .then((user: any) => {
                    // login successful if there's a jwt token in the response
                    if (user) {
                        this.auth = user;
                        this.isLoggedIn = true;
                        return user;
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                    }
                    else {
                        return false;
                    }
                });
    }
    logout() {
        this.auth = false;
        this.isLoggedIn = false;
        this.ajaxApi.pagePost('auth/logout', {}, false);
    }
}
