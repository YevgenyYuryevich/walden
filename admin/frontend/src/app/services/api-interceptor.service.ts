import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {AjaxApiService} from "./ajax-api.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ApiInterceptorService implements HttpInterceptor{

    rc: number;

    constructor(private ajaxApi: AjaxApiService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.ajaxApi.showSpinner){
            this.rc = this.ajaxApi.reqCnt();
            this.rc = this.ajaxApi.setReqCnt(this.rc + 1);
        }
        let handleObs: Observable<HttpEvent<any>> = next.handle(req);
        handleObs.subscribe(event => {
                if (event instanceof HttpResponse) {
                    if (this.ajaxApi.showSpinner) {
                        this.rc = this.ajaxApi.setReqCnt(this.rc - 1);
                    }
                }
            });

        return handleObs;
    }
}
