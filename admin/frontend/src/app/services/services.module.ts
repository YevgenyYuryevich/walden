import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AjaxApiService} from "./ajax-api.service";
import {AuthService} from "./auth.service";
import {ApiInterceptorService} from "./api-interceptor.service";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [],
    providers: [
        AjaxApiService,
        AuthService
    ]
})
export class ServicesModule { }
