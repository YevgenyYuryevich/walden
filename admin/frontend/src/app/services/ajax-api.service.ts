import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AjaxApiService {

    BASE_URL: string = window.location.protocol + "//" + window.location.host;
    // BASE_URL = 'http://job.walden.com';
    ROOT_URL: string = this.BASE_URL + '/admin/backend';
    PUBLIC_ROOT_URL: string = this.BASE_URL + '/yevgeny';
    API_ROOT_URL: string = this.ROOT_URL + '/api';
    PAGE_ROOT_URL: string = this.ROOT_URL + '/page';
    requestCnt = 0;
    showSpinner = true;
    token = 'noToken';

    constructor(private httpClient: HttpClient) {
        const self = this;
        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader ('Authorization', self.token);
                self.requestCnt ++;
            },
            complete: function () {
                self.requestCnt --;
            }
        });
    }

    post(absUrl, data, showSpinner = true) {
        this.showSpinner = showSpinner;

        return $.ajax({
            url: absUrl,
            data: data,
            type: 'post',
            dataType: 'json'
        });
    }

    apiPost(endUrl, data = {}, showSpinner = true) {
        return this.post(this.API_ROOT_URL + '/' + endUrl, data, showSpinner);
    }
    pagePost(endUrl, data = {}, showSpinner = true) {
        return this.post(this.PAGE_ROOT_URL + '/' + endUrl, data, showSpinner);
    }
    reqCnt() {
        return this.requestCnt;
    }
    setReqCnt(cnt: number) {
        this.requestCnt = cnt >= 0 ? cnt : 0;
        return this.requestCnt;
    }
}
