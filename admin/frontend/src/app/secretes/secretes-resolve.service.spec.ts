import { TestBed, inject } from '@angular/core/testing';

import { SecretesResolveService } from './secretes-resolve.service';

describe('SecretesResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecretesResolveService]
    });
  });

  it('should be created', inject([SecretesResolveService], (service: SecretesResolveService) => {
    expect(service).toBeTruthy();
  }));
});
