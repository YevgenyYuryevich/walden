import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AjaxApiService} from "../services/ajax-api.service";

@Component({
    selector: 'app-secretes',
    templateUrl: './secretes.component.html',
    styleUrls: ['./secretes.component.scss']
})
export class SecretesComponent implements OnInit {

    secretes: any;
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.secretes = {
            stripeApi_pKey: '',
            stripeApi_sKey: '',
            stripe_client_id: '',
        };
        let secretes = this.route.snapshot.data['secretes'];
        Object.assign(this.secretes, secretes);
    }

    ngOnInit() {
    }
    save() {
        this.ajaxApi.pagePost('Secretes/ajax_save', this.secretes).then(() => {
            console.log('saved');
        });
    }
}
