import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretesComponent } from './secretes.component';

describe('SecretesComponent', () => {
  let component: SecretesComponent;
  let fixture: ComponentFixture<SecretesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
