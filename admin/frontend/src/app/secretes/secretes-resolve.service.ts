import { Injectable } from '@angular/core';
import {Resolve} from "@angular/router";
import {AjaxApiService} from "../services/ajax-api.service";

@Injectable()

export class SecretesResolveService implements Resolve<any>{

    constructor(private ajaxApi: AjaxApiService) { }
    resolve() {
        return this.ajaxApi.pagePost('Secretes/ajax_get');
    }
}
