import { Injectable } from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {Resolve} from '@angular/router';

@Injectable()
export class GeneralSettingsResolver implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve() {
      return this.ajaxApi.apiPost('Options/ajax_getItems', {where: {strOption_name: ['users_can_register', 'users_can_edit_series', 'series_application_fee', 'series_auto_approve', 'allow_unApprovedUser_create_publicSeries']}});
  }
}
