import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GeneralSettingsComponent} from "./general-settings/general-settings.component";
import {GeneralSettingsResolver} from "./services/general-settings.resolver";

const routes: Routes = [
    {
        path: 'general',
        component: GeneralSettingsComponent,
        resolve: {
            generalSettings: GeneralSettingsResolver,
        },
        data: {
            title: 'General Settings',
            urls: [{title: 'Settings',url: '/settings/general'},{title: 'general'}]
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
