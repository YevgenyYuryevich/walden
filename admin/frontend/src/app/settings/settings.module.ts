import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import {ComponentModule} from "../component/component.module";
import {UiSwitchModule} from "ngx-toggle-switch";
import {GeneralSettingsResolver} from "./services/general-settings.resolver";

@NgModule({
    imports: [
        CommonModule,
        SettingsRoutingModule,
        UiSwitchModule,
    ],
    declarations: [GeneralSettingsComponent],
    providers: [
        GeneralSettingsResolver
    ]
})
export class SettingsModule { }
