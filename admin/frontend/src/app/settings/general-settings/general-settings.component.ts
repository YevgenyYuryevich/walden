import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../services/ajax-api.service';

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss']
})
export class GeneralSettingsComponent implements OnInit {

  settings: any = {};

  constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
    this.settings = {
      users_can_register: 0,
      users_can_edit_series: 0,
      series_application_fee: 10,
      series_auto_approve: 0,
      allow_unApprovedUser_create_publicSeries: 0,
    };
    const settings = this.route.snapshot.data['generalSettings'];
    Object.assign(this.settings, settings);
    this.settings.users_can_register = parseInt(this.settings.users_can_register);
    this.settings.users_can_edit_series = parseInt(this.settings.users_can_edit_series);
    this.settings.series_auto_approve = parseInt(this.settings.series_auto_approve);
    this.settings.allow_unApprovedUser_create_publicSeries = parseInt(this.settings.allow_unApprovedUser_create_publicSeries);
  }

  ngOnInit() {
  }
  update(key) {
    let v = this.settings[key];
    if (typeof v === 'boolean') {
      v = v ? 1 : 0;
    }
    this.ajaxApi.apiPost('Options/ajax_update', {where: {strOption_name: key}, sets: {strOption_value: v}}).then((res) => {
      console.log(res);
    });
  }
  save() {
    this.ajaxApi.apiPost('Options/ajax_update', {where: {strOption_name: 'series_application_fee'}, sets: {strOption_value: this.settings.series_application_fee}}).then((res) => {
      console.log(res);
    });
  }
}
