import { Injectable } from '@angular/core';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable()
export class AppHelperService {

  constructor(private ajaxApi: AjaxApiService) {}
  absUrl(url: string | null) {
    if (url) {
      return this.isUrlAbs(url) ? url : this.ajaxApi.BASE_URL + '/' + url;
    }
    return false;
  }
  isUrlAbs(url) {
    return /^(?:http(s)?:\/\/)[\w.-]+/gm.test(url);
  }
}
