import { TestBed, inject } from '@angular/core/testing';

import { AppHelperService } from './app-helper.service';

describe('AppHelperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppHelperService]
    });
  });

  it('should be created', inject([AppHelperService], (service: AppHelperService) => {
    expect(service).toBeTruthy();
  }));
});
