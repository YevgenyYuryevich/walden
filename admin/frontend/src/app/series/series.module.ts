import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeriesRoutingModule } from './series-routing.module';
import { SeriesListComponent } from './series-list/series-list.component';
import {SeriesResolverService} from "./services/series-resolver.service";
import {UiSwitchModule} from "ngx-toggle-switch";
import {SweetAlert2Module} from "@toverux/ngx-sweetalert2";
import { EditComponent } from './edit/edit.component';
import {EditResolveService} from "./services/edit-resolve.service";
import {ComponentModule} from "../component/component.module";
import {SeriesApplicationFeeResolverService} from "./services/series-application-fee-resolver.service";

@NgModule({
    imports: [
        CommonModule,
        SeriesRoutingModule,
        UiSwitchModule,
        SweetAlert2Module.forRoot(),
        ComponentModule
    ],
    declarations: [SeriesListComponent, EditComponent],
    providers: [
        SeriesResolverService,
        EditResolveService,
        SeriesApplicationFeeResolverService,
    ]
})
export class SeriesModule { }
