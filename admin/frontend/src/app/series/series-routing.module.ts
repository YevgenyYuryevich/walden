import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SeriesListComponent} from './series-list/series-list.component';
import {SeriesResolverService} from './services/series-resolver.service';
import {EditComponent} from './edit/edit.component';
import {EditResolveService} from './services/edit-resolve.service';
import {SeriesApplicationFeeResolverService} from './services/series-application-fee-resolver.service';

const routes: Routes = [
    {
        path: 'list',
        component: SeriesListComponent,
        resolve: {
            series: SeriesResolverService,
            applicationFee: SeriesApplicationFeeResolverService,
        }
    },
    {
        path: 'edit/:id',
        component: EditComponent,
        resolve: {
            itemData: EditResolveService
        },
        data: {
            title: 'Edit Series',
            urls: [{title: 'Series', url: '/series/list'}, {title: 'edit'}]
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeriesRoutingModule { }
