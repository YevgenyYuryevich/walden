import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    itemData;
    constructor(private route: ActivatedRoute) {
        this.itemData = this.route.snapshot.data['itemData'];
        console.log(this.itemData);
    }

    ngOnInit() {
    }

}
