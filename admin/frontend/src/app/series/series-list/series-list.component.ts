import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TableSettings} from "../../component/table/table.component";

@Component({
    selector: 'app-series-list',
    templateUrl: './series-list.component.html',
    styleUrls: ['./series-list.component.css']
})
export class SeriesListComponent implements OnInit {

    tableRows: any[];
    tableSettings: TableSettings;
    applicationFee: any;

    constructor(private route: ActivatedRoute) {
        this.tableRows = this.route.snapshot.data['series'];
        this.applicationFee = this.route.snapshot.data['applicationFee'];
        this.applicationFee = this.applicationFee ? parseInt(this.applicationFee['strOption_value']) : 10;
        this.tableSettings = {
            primaryKey: 'series_ID',
            columns: [
                {title: 'Image', key: 'strSeries_image', sortable: false, searchable: false, style: {width: '80px'}},
                {title: 'Title', key: 'strSeries_title', sortable: true},
                {title: 'Fee(%)', key: 'intSeries_application_fee', sortable: true, style: {width: '100px'}},
                {title: 'Approve', key: 'boolSeries_approved', sortable: true, style: {width: '100px'}},
                {title: 'S / E', key: 'boolSeries_level', sortable: true, style: {width: '100px'}},
                {title: 'Public', key: 'boolSeries_isPublic', sortable: true, style: {width: '100px'}},
                {title: 'Client', key: 'series_client', sortable: true, val: colV => colV.f_name , style: {width: '100px'}},
                {title: 'Category', key: 'series_category', sortable: true, val: colV => colV.strCategory_name, style: {width: '150px'}},
                {title: 'Actions', key: 'no', sortable: false, searchable: false, style: {width: '100px'}},
            ]
        }
    }
    onItemDeleted(id){
        this.tableRows.forEach((row, i) => {
            if (row.series_ID == id) {
                this.tableRows.splice(i, 1);
                this.tableRows = this.tableRows.slice();
            }
        });
    }
    ngOnInit() {
    }

}
