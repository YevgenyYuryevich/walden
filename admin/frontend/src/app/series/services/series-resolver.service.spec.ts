import { TestBed, inject } from '@angular/core/testing';

import { SeriesResolverService } from './series-resolver.service';

describe('SeriesResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeriesResolverService]
    });
  });

  it('should be created', inject([SeriesResolverService], (service: SeriesResolverService) => {
    expect(service).toBeTruthy();
  }));
});
