import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {AjaxApiService} from "../../services/ajax-api.service";

@Injectable()
export class EditResolveService implements Resolve<any>{

  constructor(private ajaxService: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let id = route.paramMap.get('id');

      return this.ajaxService.apiPost('series/get', { where: id });
  }
}
