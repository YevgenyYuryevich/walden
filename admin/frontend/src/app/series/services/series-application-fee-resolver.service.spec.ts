import { TestBed, inject } from '@angular/core/testing';

import { SeriesApplicationFeeResolverService } from './series-application-fee-resolver.service';

describe('SeriesApplicationFeeResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeriesApplicationFeeResolverService]
    });
  });

  it('should be created', inject([SeriesApplicationFeeResolverService], (service: SeriesApplicationFeeResolverService) => {
    expect(service).toBeTruthy();
  }));
});
