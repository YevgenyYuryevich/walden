import { TestBed, inject } from '@angular/core/testing';

import { EditResolveService } from './edit-resolve.service';

describe('EditResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditResolveService]
    });
  });

  it('should be created', inject([EditResolveService], (service: EditResolveService) => {
    expect(service).toBeTruthy();
  }));
});
