import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TableSettings} from "../component/table/table.component";

@Component({
    selector: 'app-stats',
    templateUrl: './stats.component.html',
    styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

    stats: any;
    startDateUr: any;
    endDateUr: any;
    startDate: any;
    endDate: any;
    userRetentionGraph: any = {
        data: [100, 97.6, 90.5, 91, 89, 86.8, 48.2, 98.1, 101, 120],
        months: ['Jan 2018', 'Feb 2018', 'Mar 2018', 'Apr 2018', 'May 2018', 'Jun 2018', 'Jul 2018', 'Aug 2018', 'Sep 2018', 'Oct 2018'],
    };
    joinRetentionGraph: any = {
        data: [],
        months: [],
    };
    totalJoinedTableSettings: TableSettings;
    joinedTableSettingsThisWeek: TableSettings;
    totalJoinedTableRows: any[];
    joinedTableRowsThisMonth: any[];

    cntUsersRegisterByDate: any;
    cntPurchasedByDate: any;

    constructor(private route: ActivatedRoute) {
        this.stats = this.route.snapshot.data['stats'];

        this.totalJoinedTableSettings = {
            primaryKey: 'series_ID',
            columns: [
                {title: 'Title', key: 'strSeries_title', sortable: false},
                {title: 'count', key: 'joinedCnt', sortable: true},
            ],
            sortStore: {fieldName: 'joinedCnt', dir: false},
        };
        this.joinedTableSettingsThisWeek = {
            primaryKey: 'series_ID',
            columns: [
                {title: 'Title', key: 'strSeries_title', sortable: false},
                {title: 'count', key: 'joinedCnt', sortable: true},
            ],
            sortStore: {fieldName: 'joinedCnt', dir: false},
        };
        this.cntUsersRegisterByDate = {};

        let dDate = new Date();
        this.endDateUr = {
            year: dDate.getFullYear(),
            month: dDate.getMonth() + 1,
            day: dDate.getDate(),
        };
        this.stats.totalUsers.map((user) => {
            let rDate = user.created_date.substr(0, 10);
            let date = new Date(rDate);
            if (dDate.getTime() > date.getTime()) {
                dDate = date;
            }
            if (this.cntUsersRegisterByDate[rDate]) {
                this.cntUsersRegisterByDate[rDate]++;
            }
            else {
                this.cntUsersRegisterByDate[rDate] = 1;
            }
        });
        this.startDateUr = {
            year: dDate.getFullYear(),
            month: dDate.getMonth() + 1,
            day: dDate.getDate(),
        };
        this.totalJoinedTableRows = [];
        this.joinedTableRowsThisMonth = [];
        this.stats.series.map((sery) => {
            this.totalJoinedTableRows.push({strSeries_title: sery.strSeries_title, joinedCnt: sery.joinedCnt});
            this.joinedTableRowsThisMonth.push({strSeries_title: sery.strSeries_title, joinedCnt: sery.joinedCntThisMonth});
        });

        dDate = new Date();
        this.endDate = {
            year: dDate.getFullYear(),
            month: dDate.getMonth() + 1,
            day: dDate.getDate(),
        };
        this.cntPurchasedByDate = [];
        this.stats.purchasedRows.map((purchased) => {
            let rDate = purchased.dtPurchased_startdate.substr(0, 10);
            let date = new Date(rDate);
            if (dDate.getTime() > date.getTime()) {
                dDate = date;
            }
            if (this.cntPurchasedByDate[rDate]) {
                this.cntPurchasedByDate[rDate]++;
            }
            else {
                this.cntPurchasedByDate[rDate] = 1;
            }
        });
        this.startDate = {
            year: dDate.getFullYear(),
            month: dDate.getMonth() + 1,
            day: dDate.getDate(),
        };
    }
    ngOnInit() {
        this.refreshUserRetentionGraph();
        this.refreshJoinRetentionGraph();
    }
    refreshUserRetentionGraph() {
        let sd = new Date(this.startDateUr.year, this.startDateUr.month -1 , this.startDateUr.day);
        let ed = new Date(this.endDateUr.year, this.endDateUr.month - 1, this.endDateUr.day);
        let seekD = sd;
        let rtnArr = {
            data: [],
            months: [],
        };
        let baseV = 0;
        if (sd && ed) {
            while (seekD.getTime() <= ed.getTime()) {
                if (rtnArr.months.indexOf(this.dateToMonth(seekD)) === -1) {
                    rtnArr.months.push(this.dateToMonth(seekD));
                    rtnArr.data.push(0);
                }
                if (this.cntUsersRegisterByDate[this.dateToString(seekD)]) {
                    rtnArr.data[rtnArr.data.length - 1] += this.cntUsersRegisterByDate[this.dateToString(seekD)];
                }
                seekD.setDate(seekD.getDate() + 1);
            }
        }
        rtnArr.data.map((v) => {
            if (!baseV) {
                baseV = v;
            }
        });
        if (baseV) {
            for(let i = 0; i < rtnArr.data.length; i ++) {
                rtnArr.data[i] = Number((rtnArr.data[i] * 100 / baseV).toFixed(2));
            }
        }
        this.userRetentionGraph.months = rtnArr.months;
        this.userRetentionGraph.data = rtnArr.data;
        console.log(rtnArr);
        return rtnArr;
    }

    refreshJoinRetentionGraph() {
        let sd = new Date(this.startDate.year, this.startDate.month -1 , this.startDate.day);
        let ed = new Date(this.endDate.year, this.endDate.month - 1, this.endDate.day);
        let seekD = sd;
        let rtnArr = {
            data: [],
            months: [],
        };
        let baseV = 0;
        if (sd && ed) {
            while (seekD.getTime() <= ed.getTime()) {
                if (rtnArr.months.indexOf(this.dateToMonth(seekD)) === -1) {
                    rtnArr.months.push(this.dateToMonth(seekD));
                    rtnArr.data.push(0);
                }
                if (this.cntPurchasedByDate[this.dateToString(seekD)]) {
                    rtnArr.data[rtnArr.data.length - 1] += this.cntPurchasedByDate[this.dateToString(seekD)];
                }
                seekD.setDate(seekD.getDate() + 1);
            }
        }
        rtnArr.data.map((v) => {
            if (!baseV) {
                baseV = v;
            }
        });
        if (baseV) {
            for(let i = 0; i < rtnArr.data.length; i ++) {
                rtnArr.data[i] = Number((rtnArr.data[i] * 100 / baseV).toFixed(2));
            }
        }
        this.joinRetentionGraph.months = rtnArr.months;
        this.joinRetentionGraph.data = rtnArr.data;
        return rtnArr;
    }
    dateToMonth(d){
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[d.getMonth()] + ' ' + d.getFullYear();
    }
    dateToString(d) {
        let month = '' + (d.getMonth() + 1);
        let day = '' + d.getDate();
        let year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}