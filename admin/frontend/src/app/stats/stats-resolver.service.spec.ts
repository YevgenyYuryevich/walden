import { TestBed, inject } from '@angular/core/testing';

import { StatsResolverService } from './stats-resolver.service';

describe('StatsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StatsResolverService]
    });
  });

  it('should be created', inject([StatsResolverService], (service: StatsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
