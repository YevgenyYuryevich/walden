import { Injectable } from '@angular/core';
import {Resolve} from "@angular/router";
import {AjaxApiService} from "../services/ajax-api.service";

@Injectable()
export class StatsResolverService implements Resolve<any>{

  constructor(private ajaxApi: AjaxApiService) { }
  resolve() {
      return this.ajaxApi.pagePost('Stats/ajax_get');
  }
}
