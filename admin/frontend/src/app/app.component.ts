import { Component } from '@angular/core';
import {AjaxApiService} from "./services/ajax-api.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(public ajaxApi: AjaxApiService){
  }
}
