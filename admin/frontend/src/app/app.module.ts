import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CommonModule, LocationStrategy, HashLocationStrategy} from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";

import { FullComponent } from './layouts/full/full.component';

import { NavigationComponent } from './shared/header-navigation/navigation.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpinnerComponent } from './shared/spinner.component';
import {AuthResolver} from "./services/auth-resolver";
import {ServicesModule} from "./services/services.module";
import {ApiInterceptorService} from "./services/api-interceptor.service";
import { LoadingModule } from "ngx-loading";
import {AuthGuardService} from "./guards/auth-guard.service";
import { StatsComponent } from './stats/stats.component';
import {StatsResolverService} from "./stats/stats-resolver.service";
import 'hammerjs';
import { ChartModule } from "@progress/kendo-angular-charts";
import {ComponentModule} from "./component/component.module";
import { SecretesComponent } from './secretes/secretes.component';
import {SecretesResolveService} from "./secretes/secretes-resolve.service";
import {AppHelperService} from "./helpers/app-helper.service";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true,
};

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    NavigationComponent,
    BreadcrumbComponent,
    SidebarComponent,
    StatsComponent,
    SecretesComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    PerfectScrollbarModule,
    ServicesModule,
    LoadingModule.forRoot({
      primaryColour: '#256a9e',
      secondaryColour: '#256a9e',
      tertiaryColour: '#256a9e'
    }),
    ChartModule,
    ComponentModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    AuthResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptorService,
      multi: true,
    },
    AuthGuardService,
    StatsResolverService,
    SecretesResolveService,
    AppHelperService,
    // {
    //     provide: LocationStrategy,
    //     useClass: HashLocationStrategy
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
