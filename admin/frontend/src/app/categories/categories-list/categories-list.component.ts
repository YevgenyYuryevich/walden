import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TableSettings} from "../../component/table/table.component";

@Component({
    selector: 'app-categories-list',
    templateUrl: './categories-list.component.html',
    styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {
    metaData: any;
    tableRows: any[];
    tableSettings: TableSettings;

    constructor(private route: ActivatedRoute) {
        this.tableRows = this.route.snapshot.data['categories'];
        this.metaData = {
          categories: this.tableRows
        };
        this.tableSettings = {
            primaryKey: 'category_ID',
            columns: [
                {title: 'Image', key: 'strCategory_image', sortable: false, searchable: false, style: {width: '80px'}},
                {title: 'Title', key: 'strCategory_name', sortable: true},
                {title: 'Description', key: 'strCategory_description', sortable: true},
                {title: 'Approved', key: 'intCategory_approved', sortable: true},
                {title: 'Actions', key: 'no', sortable: false, searchable: false, style: {width: '100px'}},
            ]
        }
    }
    onItemDeleted(id){
        this.tableRows.forEach((row, i) => {
            if (row.category_ID == id) {
                this.tableRows.splice(i, 1);
                this.tableRows = this.tableRows.slice();
            }
        });
    }

    ngOnInit() {
    }
}
