import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CategoriesListComponent} from "./categories-list/categories-list.component";
import {CategoriesResolverService} from "./services/categories-resolver.service";

const routes: Routes = [
    {
        path: 'list',
        component: CategoriesListComponent,
        resolve: {
            categories: CategoriesResolverService,
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
