import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import {CategoriesRoutingModule} from "./categories-routing.module";
import {CategoriesResolverService} from "./services/categories-resolver.service";
import {ComponentModule} from "../component/component.module";

@NgModule({
    imports: [
        CommonModule,
        CategoriesRoutingModule,
        ComponentModule
    ],
    declarations: [CategoriesListComponent],
    providers: [
        CategoriesResolverService,
    ]
})
export class CategoriesModule { }
