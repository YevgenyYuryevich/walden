import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        let url: string = state.url;
        if (this.authService.isLoggedIn){
            return true;
        }
        return this.checkLogin( url );
    }

    checkLogin(url: string) {

        return this.authService.authorization().then((res) => {
            if (res){
                return true;
            }
            else {
                this.authService.redirectUrl = url;
                this.router.navigate(['/authorize/login']);
                return false;
            }
        });
    }
}
