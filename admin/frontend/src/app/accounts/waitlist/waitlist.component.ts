import {Component, Input, OnInit} from '@angular/core';
import {AjaxApiService} from "../../services/ajax-api.service";
import {ActivatedRoute, ActivatedRouteSnapshot} from "@angular/router";

@Component({
    selector: 'app-waitlist',
    templateUrl: './waitlist.component.html',
    styleUrls: ['./waitlist.component.css']
})
export class WaitlistComponent implements OnInit {

    tableHeader = [
        {label: 'No'},
        {label: 'Email'},
        {label: 'Send Invitation'},
    ];
    tableRows;

    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute) {
        this.tableRows = this.route.snapshot.data['waitList'];
    }

    ngOnInit() {
    }
}
