import { Injectable } from '@angular/core';
import {AjaxApiService} from "../../services/ajax-api.service";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";

@Injectable()
export class WaitlistResolver implements Resolve<any>{
  constructor(private ajaxApi: AjaxApiService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
      return this.ajaxApi.apiPost('invitations/get', {where: {intInvitation_status: 1}});
  }
}
