import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitlistTrComponent } from './waitlist-tr.component';

describe('WaitlistTrComponent', () => {
  let component: WaitlistTrComponent;
  let fixture: ComponentFixture<WaitlistTrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitlistTrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitlistTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
