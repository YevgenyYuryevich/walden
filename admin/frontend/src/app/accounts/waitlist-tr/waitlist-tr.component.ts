import {Component, Input, OnInit} from '@angular/core';
import {AjaxApiService} from "../../services/ajax-api.service";

@Component({
    selector: '[app-waitlist-tr]',
    templateUrl: './waitlist-tr.component.html',
    styleUrls: ['./waitlist-tr.component.css']
})
export class WaitlistTrComponent implements OnInit {

    @Input() row;
    @Input() i;

    constructor(private ajaxApi: AjaxApiService) {
    }

    ngOnInit() {
    }
    sendInvitation() {
        this.ajaxApi.pagePost('invitations/sendInvitation', {where: this.row.clientInvitation_ID}).then((res) => {
            Object.assign(this.row, res);
        })
    }
}