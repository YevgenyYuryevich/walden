import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WaitlistComponent} from './waitlist/waitlist.component';
import {WaitlistResolver} from './waitlist/waitlist.resolver';
import {UserPermissionsComponent} from './user-permissions/user-permissions.component';
import {UserPermissionsResolverService} from './user-permissions/user-permissions-resolver.service';
import {UsersComponent} from "./users/users.component";
import {UsersResolveService} from "./users/users-resolve.service";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'users',
        component: UsersComponent,
        resolve: {
          users: UsersResolveService,
        },
        data: {
          title: 'Use List',
          urls: [{title: 'Accounts', url: '/accounts'}, {title: 'user list'}]
        }
      },
      {
        path: 'waitlist',
        component: WaitlistComponent,
        resolve: {
          waitList: WaitlistResolver
        },
        data: {
          title: 'Accounts Manage',
          urls: [{title: 'Accounts', url: '/accounts'}, {title: 'wait list'}]
        },
      },
      {
        path: 'user-permissions',
        component: UserPermissionsComponent,
        resolve: {
          userPermissions: UserPermissionsResolverService
        },
        data: {
          title: 'Accounts Manage',
          urls: [{title: 'Accounts', url: '/accounts'}, {title: 'permissions'}]
        },
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
