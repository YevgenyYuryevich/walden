import { TestBed, inject } from '@angular/core/testing';

import { UsersResolveService } from './users-resolve.service';

describe('UsersResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersResolveService]
    });
  });

  it('should be created', inject([UsersResolveService], (service: UsersResolveService) => {
    expect(service).toBeTruthy();
  }));
});
