import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TableSettings} from '../../component/table/table.component';
import {ActivatedRoute} from '@angular/router';
import {AppHelperService} from '../../helpers/app-helper.service';
import {AjaxApiService} from "../../services/ajax-api.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild('imgCellTemplate') imgCellTemplate: TemplateRef<any>;
  @ViewChild('switchCellTemplate') switchCellTemplate: TemplateRef<any>;

  items: any[];
  columns: any[];

  constructor(private route: ActivatedRoute, private appHelper: AppHelperService, private ajaxApi: AjaxApiService) {
    this.items = this.route.snapshot.data['users'].map(item => {
      item.image = appHelper.absUrl(item.image) || appHelper.absUrl('assets/images/global-icons/user-avatar.svg');
      item.active = parseInt(item.active) ? true : false;
      return item;
    });
  }
  update(row) {
    const sets = {
      active: row.active ? 1 : 0
    };
    this.ajaxApi.apiPost('Users/ajax_update', {where: row.id, sets: sets}).then(res => {
      console.log(res);
    });
  }
  ngOnInit() {
    this.columns = [
      {
        name: 'Image',
        cellTemplate: this.imgCellTemplate,
        sortable: false,
        width: 100,
        canAutoResize: false,
        resizeable: false
      },
      {
        name: 'Full Name',
        prop: 'f_name',
      },
      {
        name: 'Email',
      },
      {
        name: 'About Me',
        width: 200,
        prop: 'about_me'
      },
      {
        name: 'Approve',
        prop: 'active',
        width: 100,
        cellTemplate: this.switchCellTemplate
      }
    ];
  }
}
