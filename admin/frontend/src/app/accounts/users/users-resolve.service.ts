import { Injectable } from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable()
export class UsersResolveService implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.ajaxApi.apiPost('Users/ajax_getMany');
  }
}
