import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitlistTableComponent } from './waitlist-table.component';

describe('WaitlistTableComponent', () => {
  let component: WaitlistTableComponent;
  let fixture: ComponentFixture<WaitlistTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitlistTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitlistTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
