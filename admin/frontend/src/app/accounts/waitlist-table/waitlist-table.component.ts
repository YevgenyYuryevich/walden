import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-waitlist-table',
    templateUrl: './waitlist-table.component.html',
    styleUrls: ['./waitlist-table.component.scss']
})
export class WaitlistTableComponent implements OnInit {

    @Input() headers;
    @Input() rows;

    constructor() {
    }
    ngOnInit() {
    }

}
