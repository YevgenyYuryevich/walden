import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AjaxApiService} from '../../services/ajax-api.service';

@Injectable()
export class UserPermissionsResolverService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.ajaxApi.pagePost('UserPermissions/index');
    }
}
