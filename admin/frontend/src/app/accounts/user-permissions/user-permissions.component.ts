import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TableSettings} from "../../component/table/table.component";

@Component({
    selector: 'app-user-permissions',
    templateUrl: './user-permissions.component.html',
    styleUrls: ['./user-permissions.component.css']
})
export class UserPermissionsComponent implements OnInit {

    tableRows: any[];
    tableSettings: TableSettings;

    constructor(private route: ActivatedRoute) {
        this.tableRows = this.route.snapshot.data['userPermissions'];
        this.tableSettings = {
            primaryKey: 'id',
            columns: [
                {title: 'Name', key: 'f_name', sortable: true},
                {title: 'Ban To Create Public Series', key: 'ban_create_public_series', sortable: true},
            ]
        };
    }

    ngOnInit() {
    }

}
