import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { WaitlistComponent } from './waitlist/waitlist.component';
import { WaitlistTrComponent } from './waitlist-tr/waitlist-tr.component';
import { WaitlistTableComponent } from './waitlist-table/waitlist-table.component';
import {WaitlistResolver} from './waitlist/waitlist.resolver';
import { UserPermissionsComponent } from './user-permissions/user-permissions.component';
import {UserPermissionsResolverService} from './user-permissions/user-permissions-resolver.service';
import {ComponentModule} from '../component/component.module';
import { UsersComponent } from './users/users.component';
import {UsersResolveService} from './users/users-resolve.service';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {UiSwitchModule} from 'ngx-toggle-switch';

@NgModule({
  imports: [
    CommonModule,
    AccountsRoutingModule,
    ComponentModule,
    NgxDatatableModule,
    UiSwitchModule
  ],
  declarations: [
    WaitlistComponent,
    WaitlistTrComponent,
    WaitlistTableComponent,
    UserPermissionsComponent,
    UsersComponent,
  ],
  providers: [
    WaitlistResolver,
    UserPermissionsResolverService,
    UsersResolveService,
  ]
})
export class AccountsModule { }
