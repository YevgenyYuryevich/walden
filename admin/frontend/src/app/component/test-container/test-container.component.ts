import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-test-container',
    templateUrl: './test-container.component.html',
    styleUrls: ['./test-container.component.css']
})
export class TestContainerComponent implements OnInit {

    items: any[];
    tableSettings;

    constructor() {
        this.items = [
            {name: 'name1', age: '11'},
            {name: 'name2', age: '12'},
            {name: 'name3', age: '13'},
            {name: 'name5', age: '14'},
            {name: 'name6', age: '15'},
            {name: 'name7', age: '16'},
            {name: 'name8', age: '17'},
            {name: 'name9', age: '18'},
            {name: 'name10', age: '19'},
            {name: 'name11', age: '20'},
            {name: 'name12', age: '21'},
        ];
        this.tableSettings = {
            columns: [
                {
                    key: 'name',
                    title: 'Name',
                    sortable: true,
                },
                {
                    key: 'age',
                    title: 'Age',
                    sortable: true,
                },
            ]
        }
    }

    ngOnInit() {
    }

}
