import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionsTrComponent } from './permissions-tr.component';

describe('PermissionsTrComponent', () => {
  let component: PermissionsTrComponent;
  let fixture: ComponentFixture<PermissionsTrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionsTrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
