import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {AjaxApiService} from "../../../../services/ajax-api.service";

@Component({
    selector: '[app-permissions-tr]',
    templateUrl: './permissions-tr.component.html',
    styleUrls: ['./permissions-tr.component.css']
})
export class PermissionsTrComponent implements OnInit {

    @Input() row;
    @Output() rowChange = new EventEmitter();
    constructor(private ajaxService: AjaxApiService) { }

    ngOnInit() {
        this.formatRow();
    }
    formatRow(){
        this.row.ban_create_public_series = parseInt(this.row.ban_create_public_series);
    }
    updateBanPublicSeries(){
        let sets = {
            meta_value: this.row.ban_create_public_series ? 1 : 0,
        };
        let where = {client_id: this.row.id, meta_key: 'ban_create_public_series'};
        this.ajaxService.apiPost('ClientMeta/update', {sets: sets, where: where}).then(
            (res) => {
                this.rowChange.next({ban_create_public_series: sets.meta_value});
            }
        );
    }
    update(){

    }
}
