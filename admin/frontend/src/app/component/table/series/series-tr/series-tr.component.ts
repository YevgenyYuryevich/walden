import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {AjaxApiService} from "../../../../services/ajax-api.service";

@Component({
    selector: '[app-series-tr]',
    templateUrl: './series-tr.component.html',
    styleUrls: ['./series-tr.component.scss']
})
export class TestSeriesTrComponent implements OnInit {

    @Input() row;
    @Input() metaData;
    @Output() rowChange = new EventEmitter();

    @Input() i;
    @Output() itemDeleted = new EventEmitter<number>();
    constructor(private ajaxService: AjaxApiService) {
    }

    ngOnInit() {
        this.formatRow();
    }
    formatRow(): void{
        this.row.boolSeries_approved = parseInt(this.row.boolSeries_approved);
        this.row.boolSeries_isPublic = parseInt(this.row.boolSeries_isPublic);
        this.row.boolSeries_level = parseInt(this.row.boolSeries_level);
    }
    update(): void{
        let sets = {
            boolSeries_approved: this.row.boolSeries_approved ? 1 : 0,
            boolSeries_isPublic: this.row.boolSeries_isPublic ? 1 : 0,
            boolSeries_level: this.row.boolSeries_level ? 1 : 0,
        };
        let where = {series_ID: this.row.series_ID}
        this.ajaxService.apiPost('series/update', {sets: sets, where: where}).then(
            (res) => {
                this.rowChange.emit(sets);
            }
        );
    }
    updateFee() {
        if (this.row.intSeries_application_fee) {
            let sets = {
                intSeries_application_fee: this.row.intSeries_application_fee,
            };
            let where = {series_ID: this.row.series_ID};
            this.ajaxService.apiPost('series/update', {sets: sets, where: where}).then(
                (res) => {
                    this.rowChange.emit(sets);
                }
            );
        }
        else {

        }
    }
    delete(){
        let where = {series_ID: this.row.series_ID};
        this.ajaxService.apiPost('series/delete', {where: where}).then(
            (res) => { this.itemDeleted.next(this.row.series_ID); }
        );
    }
}
