import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesTrComponent } from './series-tr.component';

describe('SeriesTrComponent', () => {
  let component: SeriesTrComponent;
  let fixture: ComponentFixture<SeriesTrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeriesTrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
