import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: '[appTr]',
    templateUrl: './tr.component.html',
    styleUrls: ['./tr.component.css']
})
export class TrComponent implements OnInit {
    @Input() row;
    values: (number | string)[];
    constructor() {
    }

    ngOnInit() {
        this.values = [];
        for (let col in this.row) {
            this.values.push(this.row[col]);
        }
    }
}
