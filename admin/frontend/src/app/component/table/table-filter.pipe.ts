import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'tableFilter'
})
export class TableFilterPipe implements PipeTransform {

    transform(value, searchStore, sortStore, pagingStore, columnsSetting, changeDetector?): any {
        let searchedItems = this.search(value, searchStore, columnsSetting);
        let sortedItems = this.sort(searchedItems, sortStore, columnsSetting);
        let pagedItems = this.paging(sortedItems, pagingStore);
        return pagedItems;
    }

    search(items: any[], searchStore, columnsSetting){
        return items.filter( (item) => {
            return this.isSearched(item, searchStore, columnsSetting);
        });
    }

    isSearched(item, searchStore, columnsSetting): boolean {
        let totalFlg = false;

        for (let k in columnsSetting) {
            let colSetting = columnsSetting[k];
            if (colSetting.searchable) {
                let v = colSetting.val(item[k]);
                if (colSetting.search(searchStore.totalSearch, v)) {
                    totalFlg = true;
                }
                if (!colSetting.search(colSetting.colSearch, v)) {
                    return false;
                }
            }
        }
        return totalFlg;
    }

    sort(items: any[], sortStore, columnsSetting){
        if (sortStore.fieldName == '') {
            return items;
        }
        return items.sort( (a, b) => {
            let colSetting = columnsSetting[sortStore.fieldName];

            let av = colSetting.val(a[sortStore.fieldName]), bv = colSetting.val(b[sortStore.fieldName]);

            if (av < bv) {
                return sortStore.dir ? -1 : 1;
            } else if (av > bv) {
                return sortStore.dir ? 1 : -1;
            } else {
                return 0;
            }
        })
    }

    paging(items: any[], pagingStore){
        return items.filter((v, i) => {
            return (pagingStore.page - 1) * pagingStore.pageSize <= i && i < pagingStore.page * pagingStore.pageSize;
        });
    }
}