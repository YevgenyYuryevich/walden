import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesTrComponent } from './categories-tr.component';

describe('CategoriesTrComponent', () => {
  let component: CategoriesTrComponent;
  let fixture: ComponentFixture<CategoriesTrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesTrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
