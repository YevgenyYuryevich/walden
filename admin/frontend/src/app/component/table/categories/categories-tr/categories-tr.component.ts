import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AjaxApiService} from "../../../../services/ajax-api.service";

@Component({
    selector: '[app-categories-tr]',
    templateUrl: './categories-tr.component.html',
    styleUrls: ['./categories-tr.component.scss']
})
export class CategoriesTrComponent implements OnInit {
    @Input() row;
    @Input() metaData;
    @Output() rowChange = new EventEmitter();
    @Output() itemDeleted = new EventEmitter<number>();
    editRow: any;
    mergeTo: any;

    constructor(private modalService: NgbModal, private ajaxService: AjaxApiService) {
    }

    ngOnInit() {
        this.mergeTo = this.row.category_ID;
        this.formatRow();
        this.editRow = Object.assign({}, this.row);
    }
    formatRow(): void{
        this.row.intCategory_approved = parseInt(this.row.intCategory_approved);
    }
    delete() {
    }
    updateApproved() {
        let v = this.row.intCategory_approved ? 1 : 0;
        this.ajaxService.apiPost('Categories/ajax_update', {sets: {intCategory_approved: v}, where: this.row.category_ID}).then(
            (res) => {
                this.rowChange.emit({intCategory_approved: v});
            }
        );
    }
    openEditModal(content) {
        this.modalService.open(content, { size: 'lg'}).result.then((result) => {
            this.saveChanges();
        }, (reason) => {
        });
    }
    public saveChanges(){
        if (this.mergeTo !== this.row.category_ID) {
            return this.ajaxService.apiPost('Categories/ajax_merge', {to: this.mergeTo, where: this.row.category_ID}).then(() => {
                this.itemDeleted.emit(this.row.category_ID);
            });
        }
        else {
            let sets = {
                strCategory_name: this.editRow.strCategory_name,
                strCategory_description: this.editRow.strCategory_description
            };
            return this.ajaxService.apiPost('Categories/ajax_update', {sets: sets, where: this.row.category_ID}).then(() => {
                this.rowChange.emit(sets);
            });
        }
    }
}
