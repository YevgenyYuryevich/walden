import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {TableFilterPipe} from "./table-filter.pipe";

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Input() tr;
    @Input() items;
    @Input() settings: TableSettings;
    @Input() metaData: any;
    @Output() itemDeleted = new EventEmitter<number>();

    pagingStore: {pageSize: number, page: number, collectionSize: number} = {pageSize: 10, page: 1, collectionSize: 1};
    searchStore: {totalSearch: string, columns: any} = {totalSearch: '', columns: {}};
    sortStore: {fieldName: string, dir: boolean} = {fieldName: '', dir: true};
    columnsSetting: any = {};
    changeDetector: number = 0;

    constructor(private tablePipe: TableFilterPipe) {
    }
    onItemDeleted(id){
        for (let k in this.items) {
            if (this.items[k][this.settings.primaryKey] == id) {
                this.items.splice(k, 1);
                this.applyChange();
                break;
            }
        }
        this.itemDeleted.next(id);
    }
    onRowChange(id, sets){
        for (let k in this.items) {
            if (this.items[k][this.settings.primaryKey] == id) {
                Object.assign(this.items[k], sets);
                break;
            }
        }
    }
    ngOnInit() {
        this.pagingStore.collectionSize = this.items.length;
        this.settings.columns.forEach((column) => {
            column = Object.assign(new defaultColumn, column);
            this.columnsSetting[column.key] = {val: column.val, colSearch: '', searchable: column.searchable, search: column.search};
        });
        if (this.settings.sortStore) {
            this.sortStore = this.settings.sortStore;
        }
    }
    applyChange() {
        this.pagingStore.collectionSize = this.tablePipe.search(this.items, this.searchStore, this.columnsSetting).length;
        this.changeDetector++;
    }
    thClick(col){
        if (!col.sortable) {
            return;
        }
        if (this.sortStore.fieldName == col.key) {
            this.sortStore.dir = !this.sortStore.dir;
        }
        this.sortStore.fieldName = col.key;
        this.pagingStore.page = 1;
        this.applyChange();
    }
}

export class defaultColumn {
    key: string;
    title: string;
    sortable: boolean;
    searchable: boolean;
    style: {};
    val(colV): any {
        return colV;
    };
    search(s, v): boolean{
        if (s == '') {
            return true;
        }
        if (typeof v == 'number') {
            return parseInt(s) == v;
        }
        else if (typeof v == 'boolean') {
            return s == v;
        }
        else {
            return v.toLowerCase().includes(s.toLowerCase());
        }
    }
    constructor(){
        this.key = 'no';
        this.title = 'no title';
        this.sortable = true;
        this.searchable = true;
    }
}

export interface TableSettingColumn {
    key: string;
    title: string;
    sortable?: boolean;
    searchable?: boolean;
    style?: any;
    val?(colV): any;
    search?(): boolean;
}

export interface TableSettings {
    primaryKey?: string;
    columns: TableSettingColumn[];
    sortStore?: {
        fieldName: string, dir: boolean
    };
}