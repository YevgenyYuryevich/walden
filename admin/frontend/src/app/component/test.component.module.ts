import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {ComponentModule} from "./component.module";
import {ComponentsRoutes} from "./component.routing";

@NgModule({
    imports: [
        RouterModule.forChild(ComponentsRoutes),
        ComponentModule
    ]
})

export class TestComponentModule {}