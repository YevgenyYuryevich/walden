import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbdpregressbarBasic } from './progressbar/progressbar.component';
import { NgbdpaginationBasic } from './pagination/pagination.component';
import { NgbdAccordionBasic } from './accordion/accordion.component';
import { NgbdAlertBasic } from './alert/alert.component';
import { NgbdCarouselBasic } from './carousel/carousel.component';
import { NgbdDatepickerBasic } from './datepicker/datepicker.component';
import { NgbdDropdownBasic } from './dropdown-collapse/dropdown-collapse.component';
import { NgbdModalBasic } from './modal/modal.component';
import { NgbdPopTooltip } from './popover-tooltip/popover-tooltip.component';
import { NgbdratingBasic } from './rating/rating.component';
import { NgbdtabsBasic } from './tabs/tabs.component';
import { NgbdtimepickerBasic } from './timepicker/timepicker.component';
import { NgbdtypeheadBasic } from './typehead/typehead.component';
import { CardsComponent } from './card/card.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TableComponent } from './table/table.component';
import { TrComponent } from './table/tr/tr.component';
import { TestContainerComponent } from './test-container/test-container.component';
import { TableFilterPipe } from './table/table-filter.pipe';
import { TestSeriesTrComponent } from "./table/series/series-tr/series-tr.component";
import {UiSwitchModule} from "ngx-toggle-switch";
import {SweetAlert2Module} from "@toverux/ngx-sweetalert2";
import {RouterModule} from "@angular/router";
import {PermissionsTrComponent} from "./table/accounts/permissions-tr/permissions-tr.component";
import {CategoriesTrComponent} from "./table/categories/categories-tr/categories-tr.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        JsonpModule,
        NgbModule,
        UiSwitchModule,
        SweetAlert2Module.forRoot(),
        RouterModule.forChild([]),
    ],
    exports: [
        NgbdpregressbarBasic,
        NgbdpaginationBasic,
        NgbdAccordionBasic,
        NgbdAlertBasic,
        NgbdCarouselBasic,
        NgbdDatepickerBasic,
        NgbdDropdownBasic,
        NgbdModalBasic,
        NgbdPopTooltip,
        NgbdratingBasic,
        NgbdtabsBasic,
        NgbdtimepickerBasic,
        NgbdtypeheadBasic,
        CardsComponent,
        ButtonsComponent,
        TableComponent,
        TrComponent,
        CategoriesTrComponent,
        TestContainerComponent,
        TableFilterPipe,
        PermissionsTrComponent
    ],
    declarations: [
        NgbdpregressbarBasic,
        NgbdpaginationBasic,
        NgbdAccordionBasic,
        NgbdAlertBasic,
        NgbdCarouselBasic,
        NgbdDatepickerBasic,
        NgbdDropdownBasic,
        NgbdModalBasic,
        NgbdPopTooltip,
        NgbdratingBasic,
        NgbdtabsBasic,
        NgbdtimepickerBasic,
        NgbdtypeheadBasic,
        CardsComponent,
        ButtonsComponent,
        TableComponent,
        TrComponent,
        TestContainerComponent,
        TableFilterPipe,
        TestSeriesTrComponent,
        CategoriesTrComponent,
        PermissionsTrComponent,
    ],
    providers: [
        TableFilterPipe
    ]
})

export class ComponentModule {}