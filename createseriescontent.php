<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.04.2018
 * Time: 06:29
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class CreateSeriesContent extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Series_m');
        $this->load->model('api_m/Purchased_m');
        $this->load->model('api_m/FormField_m');
        $this->load->model('api_m/FormFieldAnswer_m');
        $this->seriesModel = new \Models\api\Series_m();
        $this->purchasedModel = new \Models\api\Purchased_m();
        $this->formFieldModel = new \Models\api\FormField_m();
        $this->formFieldAnswerModel = new \Models\api\FormFieldAnswer_m();
    }

    public function inex(){
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $nodeType = isset($_POST['nodeType']) ? $_POST['nodeType'] : (isset($_GET['nodeType']) ? $_GET['nodeType'] : false);
        $type = isset($_POST['type']) ? $_POST['type'] : (isset($_GET['type']) ? $_GET['type'] : false);
        $tab = isset($_POST['tab']) ? $_POST['tab'] : (isset($_GET['tab']) ? $_GET['tab'] : false);
        if (!$id){
            echo 'no id';
            die();
        }

        $series = $this->seriesModel->get($id);

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $id]);
        $purchased = $purchased ? $purchased : false;

        $isSeriesMine = $series['intSeries_client_ID'] == $_SESSION['client_ID'];
        $formFields = $this->formFieldModel->getRows(['intFormField_series_ID' => $id]);
        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);

        $this->load->view('CreateSeriesContent_v', ['series' => $series, 'purchased' => $purchased, 'isSeriesMine' => $isSeriesMine, 'formFields' => $formFields, 'nodeType' => $nodeType, 'type' => $type, 'formFieldAnswers' => $formFieldAnswers, 'tab' => $tab]);
    }
}

$handle = new CreateSeriesContent();
$handle->inex();