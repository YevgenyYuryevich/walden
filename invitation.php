<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.02.2018
 * Time: 09:28
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/Invitation_m.php';

use Models\Invitation_m;
use Core\Controller_core;
use function Helpers\{strongEncrypt, htmlMailHeader};

class Invitation extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Invitation_m();
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function index(){
        $userInfo = $this->model->getUserInfo();
        $this->load->view('Invitation_v', ['invitationCnt' => (int) $userInfo['invitation_cnt']]);
    }
    public function ajax_inviteFriend(){
        $userInfo = $this->model->getUserInfo();
        $invitationCnt = $userInfo['invitation_cnt'];
        if ((int) $invitationCnt == 0){
            echo json_encode(['status' => false]);
            die();
        }
        $friendEmail = $_POST['friendEmail'];
        $token = strongEncrypt($friendEmail . time());
        $fromName = $userInfo['f_name'] || $userInfo['l_name'] ? $userInfo['f_name'] . ' ' . $userInfo['l_name'] : $userInfo['username'];

//        $headers  = "MIME-Version: 1.0\r\n";
//        $headers .= "Content-type: text/html; charset=utf-8\r\n";
//        $headers .= "From: " . $fromName . " <" . $userInfo['email'] . ">\r\n";
//        $headers .= "Reply-To: <" . strip_tags($userInfo['email']) . ">\r\n";

        $headers = htmlMailHeader();

//        $msg  = '<h3>Hello</h3><div>You are invited to <strong>theGreyshirt</strong> by <strong>'. $fromName .'</strong></div>';
//        $link = sprintf('<div><a href="%s">Please Click here to Join</a></div>', BASE_URL . '/login?register&token=' . $token);
//        $msg .= $link;

        $msg1 = file_get_contents(BASE_URL . '/assets/email-templates/You_are_invited.php?accept_url=' . urlencode(BASE_URL . '/login?register&token=' . $token));
        $msg2 = file_get_contents(BASE_URL . '/assets/email-templates/thanks_for_inviting.php?email=' . $friendEmail);
        $flg1  = mail($friendEmail, 'You are invited to theGreyshirt', $msg1, $headers);
        $flg2  = mail($userInfo['email'], 'Thanks for Invite', $msg2, $headers);
        $flg = $flg1 && $flg2;

//        $flg = true;

        if ($flg){
            $newId = $this->model->insertInvitation(['strInvitedEmail' => $friendEmail, 'strToken' => $token]);
            $this->model->updateUser(['invitation_cnt' => (int) $invitationCnt - 1]);
            echo json_encode(['status' => true, 'data' => $newId, 'link' => BASE_URL . '/login?register&token=' . $token, 'headers' => $headers, 'msg' => $msg2]);
        }
        else{
            echo json_encode(['status' => false, 'data' => 'email is not valid']);
        }
        die();
    }
    public function test(){
        $this->model->test();
    }
}

$invitationHandle = new Invitation();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'invite_friend':
            $invitationHandle->ajax_inviteFriend();
            break;
        default:
            $invitationHandle->index();
            break;
    }
}
else{
    $invitationHandle->index();
//    $invitationHandle->test();
}