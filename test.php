<?php

require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/options/cloud_convert.php';

use function Helpers\pushEncode;
use function Helpers\pushDecode;
use function Helpers\utf8Encode;

class Test extends \Core\Controller_core {
    private $api;
    private $postsModel;
    private $postsHandle;
    private $purchasedModel;
    private $subsHandle;
    private $seriesModel;

    public function __construct()
    {
        parent::__construct();
    }
    public function index() {
        $enc = \Firebase\JWT\JWT::encode('walden', 'key1');
        $dec = \Firebase\JWT\JWT::decode($enc, 'key1', ['HS256']);
        var_dump($enc);
        var_dump($dec);
    }
    public function indexPre(){
        echo '<pre>';
        $purchased = $this->purchasedModel->get(360);
        $seekSubs = $this->subsHandle->seekSubscription($purchased);
        var_dump($seekSubs);
        die();
    }
    public function domInnerHTML($element)
    {
        $innerHTML = "";
        $children  = $element->childNodes;

        foreach ($children as $child)
        {
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        }
        return $innerHTML;
    }
}

$handle = new Test();

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'test':
            break;
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}
