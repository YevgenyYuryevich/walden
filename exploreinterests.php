<?php

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/ExploreInterests_m.php';
require_once 'yevgeny/models/api_m/Purchased_m.php';
require_once 'yevgeny/models/api_m/Series_m.php';

use Core\Controller_core;
use Models\ExploreInterests_m;
use Models\api\Purchased_m;

class ExploreInterests extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = 0;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->model = new ExploreInterests_m();
        $this->seriesModel = new \Models\api\Series_m();
        $this->purchasedModel = new Purchased_m();
    }
    public function index(){
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $prevPage = isset($_POST['prevPage']) ? $_POST['prevPage'] : (isset($_GET['prevPage']) ? $_GET['prevPage'] : 'simplify');
        if ($id){
            $category = $this->model->getCategoryById($id);
        }
        else{
            $category = $this->model->getRandomCategory();
        }
        $series = $this->seriesModel->getSeries(['intSeries_category' => $category['category_ID'], 'boolSeries_isPublic' => 1, 'boolSeries_approved' => 1]);
        foreach ($series as & $sery){
            $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $sery['series_ID']]);
            $sery['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;
        }
        $this->load->view('ExploreInterests_v', ['series' => $series, 'prevPage' => $prevPage, 'category' => $category]);
    }
}
$exploreInterestsHandle = new ExploreInterests();

if (isset($_POST['action'])){

}
else{
    $exploreInterestsHandle->index();
}