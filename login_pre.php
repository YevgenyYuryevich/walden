<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$access = true;
require ($_SERVER['DOCUMENT_ROOT'].'/guardian/class/userauth.class.php');
$auth = new UserAuthentication();

if (isset($_POST['check'])){

    $info['result'] = $auth->check();
    exit(json_encode($info));
} else if (isset($_POST['function'])){
	$function = $_POST['function'];
//	$info['result'] = $auth->$function();

    switch ($_POST['function']){
        case 'signin':
            $info['result'] = $auth->signin();
            break;
        case 'register':
            $info['result'] = $auth->register();
            break;
        case 'send_email':
            $info['result'] = $auth->ajax_sendEmail();
            break;
        case 'reset_pw':
            $info['result'] = $auth->reset_pw();
            break;
        case 'reset_send':
            $info['result'] = $auth->reset_send();
            break;
        default:
            $info['result'] = $auth->signin();
            break;
    }
    exit(json_encode($info));
} else {
	if ($auth->isLoggedIn() && !isset($_GET['myprofile'])){
        $prevPage = isset($_SESSION['uri']) && $_SESSION['uri'] ? $_SESSION['uri'] : '/userpage';
        $prevPage = $prevPage == '/index' || $prevPage == '/index.php' ? '/userpage' : $prevPage;

        header('Location:' . $prevPage);
    }
	else $auth->view_tmpl();
}
?>