<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.08.2018
 * Time: 12:27
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class StripeConnect extends \Core\Controller_core
{
    private $userModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->userModel = new \Models\api\User_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();
    }
    public function index(){

        $code = $_GET['code'];

        $stripeApi_sKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_sKey']);
        $stripeApi_sKey = $stripeApi_sKeyRow ? $stripeApi_sKeyRow['strOption_value'] : false;
        $stripeApi_sKey = \Helpers\jwtDecode($stripeApi_sKey);

        $token_request_body = [
            'client_secret' => $stripeApi_sKey,
            'code' => $code,
            'grant_type' => 'authorization_code',
        ];

        $req = curl_init('https://connect.stripe.com/oauth/token');
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp = json_decode(curl_exec($req), true);

        curl_close($req);

        if (isset($resp['access_token'])) {
            $accountId = $resp['stripe_user_id'];
            $this->userModel->update($_SESSION['client_ID'], ['stripe_account_id' => $accountId]);
            $this->load->view('StripeConnectSuccess_v');
        }
        else {
            echo 'something is wrong';
            die();
        }
    }
}

$handle = new StripeConnect();
$handle->index();
