<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.02.2018
 * Time: 15:33
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/controllers/Subscriptions_c.php';
require_once 'yevgeny/models/Calendar_m.php';
require_once 'yevgeny/models/Favorites_m.php';
require_once 'yevgeny/models/Ideaboxcards_m.php';
require_once 'yevgeny/models/api_m/Subscriptions_m.php';
require_once 'yevgeny/models/api_m/Purchased_m.php';
require_once 'yevgeny/models/api_m/Posts_m.php';

use Core\Controller_core;
use Controllers\Subscriptions_c;
use Models\Calendar_m;
use Models\Favorites_m;
use Models\Ideaboxcards_m;
use Models\api\Subscriptions_m;
use Models\api\Purchased_m;
use Models\api\Posts_m;

class Calendar extends Controller_core{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Calendar_m();
        $this->subsHandle = new Subscriptions_c();
        $this->favoriteModel = new Favorites_m();
        $this->ideaboxModel = new Ideaboxcards_m();
        $this->subsModel = new Subscriptions_m();
        $this->postModel = new Posts_m();
        $this->purchasedModel = new Purchased_m();
        $this->load->model('api_m/Options_m');

        $this->optionModel = new \Models\api\Options_m();
    }
    public function index() {
        $id = isset($_POST['series_id']) ? $_POST['series_id'] : (isset($_GET['series_id']) ? $_GET['series_id'] : false);
        $purchasedSeries = $this->model->getPurchasedSeries();
        foreach ($purchasedSeries as & $purchasedSery){

            $seekSub = $this->subsHandle->seekSubscription($purchasedSery);

            if (!$seekSub) {
                $purchasedSery['subscriptions'] = [];
                continue;
            }
            $cnt = $this->subsModel->countSubscriptions(['intClientSubscription_purchased_ID' => $purchasedSery['purchased_ID'], 'intClientSubscriptions_parent' => $seekSub['intClientSubscriptions_parent'], 'intClientSubscriptions_order >=' => $seekSub['intClientSubscriptions_order']]);

            if ($cnt < 100){
                $postParent = 0;
                if ($seekSub['intClientSubscriptions_parent'] !== '0') {
                    $subParent = $this->subsModel->get($seekSub['intClientSubscriptions_parent']);
                    $postParent = $subParent['intClientSubscription_post_ID'];
                }
                if (!is_null($postParent)) {
                    $this->subsHandle->subscribeSeries(['intPost_series_ID' => $purchasedSery['series_ID'], 'intPost_parent' => $postParent], $purchasedSery['purchased_ID'], 100);
                }
            }
            $nextSubs = $this->subsHandle->getNextSubs($seekSub);
            $fromSubs = array_merge([$seekSub], $nextSubs);
            foreach ($fromSubs as $key => & $fromSub) {
                $fromSub = $this->subsHandle->formatSubscription($fromSub);
                $fromSub['viewOrder'] = $key + 1;
            }
            $purchasedSery['subscriptions'] = $fromSubs;
            $purchasedSery['favorites'] = $this->favoriteModel->getFavoriteSubsByPurchasedId($purchasedSery['purchased_ID']);
        }
        $ideaboxcards = $this->ideaboxModel->getIdeaboxes();
        foreach ($ideaboxcards as & $ideaboxcard){
            unset($ideaboxcard['strIdeaBox_idea']);
        }
        $usersCanEditSeries = $this->optionModel->get(['strOption_name' => 'users_can_edit_series']);
        $usersCanEditSeries = $usersCanEditSeries ? (int) $usersCanEditSeries['strOption_value'] : false;
        $this->load->view('Calendar_v', ['purchasedSeries' => $purchasedSeries, 'ideaboxcards' => $ideaboxcards, 'id' => $id, 'usersCanEditSeries' => $usersCanEditSeries]);
    }
    public function ajax_updateWithSubs() {
        $id = $_POST['id'];
        $withId = $_POST['withId'];
        $subs = $this->subsModel->get($withId);
        $subs = $this->model->formatSubscription($subs);

        $sets = [
            'strClientSubscription_body' => $subs['strClientSubscription_body'],
            'strClientSubscription_title' => $subs['strClientSubscription_title'],
            'strClientSubscription_image' => $subs['strClientSubscription_image'],
            'intClientSubscriptions_type' => $subs['intClientSubscriptions_type'],
        ];
        if (!is_null($subs['intClientSubscription_post_ID'])){
            $sets['intClientSubscription_post_ID'] = $subs['intClientSubscription_post_ID'];
        }
        $this->subsModel->update($id, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateWithIdea() {
        $ideaId = $_POST['ideaId'];
        $id = $_POST['id'];
        $ideabox = $this->ideaboxModel->getIdeabox($ideaId);
        $sets = [
            'strClientSubscription_body' => $ideabox['strIdeaBox_idea'],
            'strClientSubscription_title' => $ideabox['strIdeaBox_title'],
            'strClientSubscription_image' => $ideabox['strIdeaBox_image'],
            'intClientSubscriptions_type' => 7,
        ];
        $this->subsModel->update($id, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateOrders() {
        $updateSets = $_POST['updateSets'];
        foreach ($updateSets as $updateSet){
            $id = $updateSet['id'];
            $sets = ['intClientSubscriptions_order' => $updateSet['order']];
            $this->subsModel->update($id, $sets);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function test() {
    }
}

$handle = new Calendar();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'update_orders':
            $handle->ajax_updateOrders();
            break;
        case 'update_with_subs':
            $handle->ajax_updateWithSubs();
            break;
        case 'update_with_idea':
            $handle->ajax_updateWithIdea();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
//    $handle->test();
}