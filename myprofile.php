<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 11.07.2018
 * Time: 11:31
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class MyProfile extends \Core\Controller_core
{
    public $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->model = new \Models\api\User_m();
    }
    public function index() {
        $user = $this->model->get($_SESSION['client_ID'], 'id, username, email, f_name, l_name, phone, image');
        $this->load->view('MyProfile_v', ['user' => $user]);
    }
}

$handle = new MyProfile();
$handle->index();