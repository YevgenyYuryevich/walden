<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 23:54
 */
require_once("guardian/access.php");
require_once 'yevgeny/core/Controller_core.php';

use Core\Controller_core;

class ViewPost extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        $prevPage = isset($_POST['prevPage']) ? $_POST['prevPage'] : (isset($_GET['prevPage']) ? $_GET['prevPage'] : '/index');
        $data = [
            'prevPage' => $prevPage,
            'seriesTitle' => $_POST['seriesTitle'],
            'type' => $_POST['type'],
            'img' => $_POST['img'],
            'title' => $_POST['title'],
            'body' => $_POST['body']
        ];
        $data['postData'] = $data;

        $this->load->view('ViewPost_v', $data);
    }
}

$viewHandle = new ViewPost();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'view':
            $viewHandle->index();
            break;
        default:
            $viewHandle->index();
            break;
    }
}
else{
    $viewHandle->index();
}