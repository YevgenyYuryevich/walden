<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 04:50
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/api_m/Ideabox_m.php';

use Core\Controller_core;

class EditIdeabox extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new \Models\api\Ideabox_m();
    }
    public function index(){
        $parentId = isset($_POST['parentId']) ? $_POST['parentId'] : (isset($_GET['parentId']) ? $_GET['parentId'] : 0);
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : 0);
        if ($id){
            $ideabox = $this->model->get($id);
        }
        else{
            $ideabox = false;
        }
        $this->load->view('EditIdeabox_v', ['parentId' => $parentId, 'ideabox' => $ideabox]);
    }
    public function ajax_uploadRef(){
        $file = $_FILES['ref_file'];
        $unique_filename = uniqid('ideabox').'_' . $file['name'];
        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/ideabox-doc-files/' . $unique_filename;
        move_uploaded_file($file['tmp_name'], $destination);
        $url = BASE_URL . '/assets/ideabox-doc-files/' . $unique_filename;
        echo json_encode(['status' => true, 'data' => $url]);
        die();
    }
}

$handle = new EditIdeabox();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'upload_ref':
            $handle->ajax_uploadRef();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}