<?php

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/ViewExperience_m.php';
require_once 'yevgeny/models/api_m/ClientMeta_m.php';
require_once 'yevgeny/helpers/funcs.php';

use Core\Controller_core;
use Models\ViewExperience_m;
use function Helpers\{utf8Encode, htmlPurify, htmlMailHeader};
use Models\api\ClientMeta_m;

class ViewExperience extends Controller_core{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = 0;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
            $this->clientMetaModel = new ClientMeta_m();
        }
        $this->model = new ViewExperience_m();
    }
    public function index(){

        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $prevPage = isset($_POST['prev']) ? $_POST['prev'] : (isset($_GET['prev']) ? $_GET['prev'] : 'simplify');
        if ($id){
            $series = $this->model->getSeriesById($id);
            if ((int)$series['boolSeries_approved'] == 1){
                $posts = $this->model->getPostsBySeriesId($id);
            }
            else{
                echo '<h2>Sorry, This Series has not been approved!</h2>';
                echo '<h2>Please wait.</h2>';
                die();
            }
        }
        else{
            $series = $this->model->getRandomSeries();
            $posts = $series ? $this->model->getPostsBySeriesId($series['series_ID']) : false;
        }

        $introduced_affiliate = isset($_POST['affiliate_id']) ? $_POST['affiliate_id'] : (isset($_GET['affiliate_id']) ? $_GET['affiliate_id'] : false);

        if ($introduced_affiliate) {
            $_SESSION['introduced_affiliate'] = $introduced_affiliate;
        }

        $posts = utf8Encode($posts);
        $purchasedId = $this->model->getPurchasedIdBySeriesId($series['series_ID']);
        $isLoggedIn = $this->isLoggedIn;

        $viewUri = rawurlencode(BASE_URL . '/viewexperience?id=' . $id . '&affiliate_id=' . $_SESSION['affiliate_id']);
        $uriTitle = rawurlencode($series['strSeries_title']);
        require_once 'yevgeny/views/viewexperience_v.php';
    }
    public function joinSeries($seriesId){
        $newId = $this->model->joinSeries($seriesId);
        $flgJoined = $this->clientMetaModel->get(['client_id' => $_SESSION['client_ID'], 'meta_key' => 'first_series_join']);
        if (!$flgJoined) {
            $this->clientMetaModel->insert(['meta_key' => 'first_series_join', 'meta_value' => 1]);
            $headers = htmlMailHeader();
            $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/viewexperience.html');
            mail($_SESSION['guardian']['email'], 'Congratulations on joining your first greyshirt!', $msg, $headers);
        }
        return $newId;
    }
    public function unJoinSeries($seriesId){
        return $this->model->unJoinSeries($seriesId);
    }
}

$viewExperienceHandle = new ViewExperience();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'join_series':
            $newId = $viewExperienceHandle->joinSeries($_POST['seriesId']);
            echo json_encode(['status' => true, 'data' => $newId]);
            break;
        case 'unjoin_series':
            $viewExperienceHandle->unJoinSeries($_POST['seriesId']);
            echo json_encode(['status' => true]);
            break;
        default:
            $viewExperienceHandle->index();
            break;
    }
}
else{
    $viewExperienceHandle->index();
}