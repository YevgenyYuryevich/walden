<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.02.2018
 * Time: 06:39
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/Ideaboxcards_m.php';
require_once 'yevgeny/models/api_m/Series_m.php';
require_once 'yevgeny/options/Unsplash.php';

use Core\Controller_core;
use Models\Ideaboxcards_m;
use Models\api\Series_m;

class Ideaboxcards extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Ideaboxcards_m();
        $this->seriesModel = new Series_m();
        global $unsplashOptions;
        Crew\Unsplash\HttpClient::init($unsplashOptions);
        $this->unsplash = new \Crew\Unsplash\Photo();
        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';
    }
    public function index(){
        $parentId = isset($_POST['parentId']) ? $_POST['parentId'] : (isset($_GET['parentId']) ? $_GET['parentId'] : 0);
        $parentId = (int) $parentId;
        $ideaboxes = $this->model->getIdeaboxes();
        $series = $this->seriesModel->getPurchasedSeries();
        $this->load->view('Ideaboxcards_v', ['ideaboxes' => $ideaboxes, 'parentId' => $parentId, 'series' => $series]);
    }
    public function deleteIdeaboxNest($parentId){
        $childs = $this->model->getIdeaboxesByParentId($parentId);
        foreach ($childs as $child){
            $this->deleteIdeaboxNest($child['ideabox_ID']);
        }
        return $this->model->deleteIdeabox($parentId);
    }
    public function getPhotoFromUnsplash($query, $for){
        $filter = ['query' => $query, 'w' => 300];
        $unsplashPhoto = $this->unsplash::random($filter);
        if ($unsplashPhoto->errors){
            return ['savedUrl' => $this->defaultPhoto, 'originUrl' => $this->defaultPhoto];
        }
        $uPhotoUrl = $unsplashPhoto->urls['small'];
        preg_match('/fm=([^\&]*)/', $uPhotoUrl, $matches);
        $extension = $matches[1];
        preg_match('/^([^\w]*(?P<fName>\w+))/', $query, $matches);
        $savedUrl = $this->putContents($uPhotoUrl, $for, $matches['fName'], $extension);
        return ['savedUrl' => $savedUrl, 'originUrl' => $uPhotoUrl];
    }
    public function putContents($orgUrl, $prefix, $name, $extends){
        $name = preg_replace('/[\?\/\#]\s/', '-', $name);
        $uniqueFilename = uniqid(str_replace(' ', '_', $prefix)).'_' . urlencode($name) . '.' . $extends;
        $savedUrl = 'assets/images/' . urlencode($uniqueFilename);
        file_put_contents($savedUrl, fopen($orgUrl, 'r'));
        return $savedUrl;
    }
    public function ajax_updateIdeaboxImage(){
        $file = $_FILES['ideaImage'];
        $id = $_POST['id'];

        if ( 0 < $file['error'] ) {
            echo json_encode(['status' => false]);
            die();
        }
        else {
            $unique_filename = uniqid('ideabox').'_' . $file['name'];
            $destination = _ROOTPATH_ . '/assets/images/' . $unique_filename;
            $rlt = move_uploaded_file($file['tmp_name'], $destination);
            if ($rlt === true) {
                $saveUrl = 'assets/images/' . $unique_filename;
                $rlt = $this->model->updateIdeabox($id, ['strIdeaBox_image' => $saveUrl]);
                echo json_encode(['status' => $rlt, 'data' => $saveUrl]);
            }
            else { echo json_encode(['status' => false]); }
        }
        die();
    }
    public function ajax_updateIdeaboxRefFile(){
        $file = $_FILES['refFile'];
        $id = $_POST['id'];

        $linkText = \Helpers\aOrb($_POST['linkText'], 'View Doc');
        $unique_filename = uniqid('ideabox').'_' . $file['name'];
        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/ideabox-doc-files/' . $unique_filename;
        $rlt = move_uploaded_file($file['tmp_name'], $destination);
        if ($rlt){
            $saveUrl = BASE_URL . '/assets/ideabox-doc-files/' . $unique_filename;
            $this->model->updateIdeabox($id, ['strIdeaBox_reference_link' => $saveUrl, 'strIdeaBox_reference_text' => $linkText]);
            echo json_encode(['status' => true, 'data' => ['link' => $saveUrl, 'text' => $linkText]]);
        }
        else { echo json_encode(['status' => false]); }
        die();
    }
    public function ajax_updateIdeaboxRefUrl(){
        $id = $_POST['id'];
        $saveUrl = $_POST['refUrl'];
        $linkText = \Helpers\aOrb($_POST['linkText'], 'View Doc');
        $this->model->updateIdeabox($id, ['strIdeaBox_reference_link' => $saveUrl, 'strIdeaBox_reference_text' => $linkText]);
        echo json_encode(['status' => true, 'data' => ['link' => $saveUrl, 'text' => $linkText]]);
        die();
    }
    public function ajax_deleteIdeabox(){
        $id = $_POST['id'];
        $this->deleteIdeaboxNest($id);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_createIdeaboxByImage(){
        $imageFile = $_FILES['ideaImage'];
        $parentId = $_POST['parentId'];

        $unique_filename = uniqid('ideabox').'_' . $imageFile['name'];
        $destination = _ROOTPATH_ . '/assets/images/' . $unique_filename;
        $rlt = move_uploaded_file($imageFile['tmp_name'], $destination);

        if ($rlt){
            $sets = [];
            $sets['intIdeaBox_subcategory'] = $parentId;
            $sets['strIdeaBox_image'] = 'assets/images/' . $unique_filename;
            $sets['strIdeaBox_title'] = preg_replace('/(\.\w*)$/', '', $imageFile['name']);
            $sets['strIdeaBox_reference_link'] = BASE_URL . '/assets/images/' . $unique_filename;
            $sets['strIdeaBox_reference_text'] = 'View Doc';
            $newId = $this->model->insertIdeabox($sets);
            $ideabox = $this->model->getIdeabox($newId);
            echo json_encode(['status' => true, 'data' => $ideabox]);
        }

        die();
    }
    public function ajax_createIdeaboxByDoc(){
        $docFile = $_FILES['docFile'];
        $parentId = $_POST['parentId'];

        $unique_filename = uniqid('ideabox').'_' . $docFile['name'];
        $destination = _ROOTPATH_ . '/assets/ideabox-doc-files/' . $unique_filename;
        $rlt = move_uploaded_file($docFile['tmp_name'], $destination);

        if ($rlt){
            $sets = [];
            $sets['intIdeaBox_subcategory'] = $parentId;
            $sets['strIdeaBox_title'] = preg_replace('/(\.\w*)$/', '', $docFile['name']);
            $sets['strIdeaBox_reference_link'] = BASE_URL . '/assets/ideabox-doc-files/' . $unique_filename;
            $sets['strIdeaBox_reference_text'] = 'View Doc';
            $newId = $this->model->insertIdeabox($sets);
            $ideabox = $this->model->getIdeabox($newId);
            echo json_encode(['status' => true, 'data' => $ideabox]);
        }

        die();
    }
    public function ajax_createIdeaboxByUrl(){
        $url = $_POST['url'];
        $parentId = $_POST['parentId'];
        $sets = [];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = curl_exec($curl);

        $isTitleIn = preg_match('/<title>(.*)<\/title>/', $result, $matches);

        $sets['strIdeaBox_title'] = $isTitleIn ? $matches[1] : 'title';
        $isDescriptionIn = preg_match('/<meta.*name\s*=\s*[\'\"]+description[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
        $sets['strIdeaBox_idea'] = $isDescriptionIn ? $matches[1] : '';
        $sets['strIdeaBox_reference_link'] = $url;
        $sets['intIdeaBox_subcategory'] = $parentId;

        $unsplashImg = $this->getPhotoFromUnsplash('rand', 'ideabox');
        $sets['strIdeaBox_image'] = $unsplashImg['savedUrl'];
        $newId = $this->model->insertIdeabox($sets);
        $ideabox = $this->model->getIdeabox($newId);
        echo json_encode(['status' => true, 'data' => $ideabox]);
        die();
    }
    public function ajax_updateOrders(){
        $updateSets = $_POST['updateSets'];
        foreach ($updateSets as $updateSet){
            $id = $updateSet['id'];
            $order = $updateSet['order'];
            $this->model->updateIdeabox($id, ['intIdeaBox_order' => $order]);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateIdeaTitle(){
        $id = $_POST['id'];
        $title = $_POST['title'];
        $this->model->updateIdeabox($id, ['strIdeaBox_title' => $title]);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateIdeaDescription(){
        $id = $_POST['id'];
        $idea = $_POST['idea'];
        $this->model->updateIdeabox($id, ['strIdeaBox_idea' => $idea]);
        echo json_encode(['status' => true]);
        die();
    }
}

$ideaboxcardsHandle = new Ideaboxcards();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'update_ideaboxImage':
            $ideaboxcardsHandle->ajax_updateIdeaboxImage();
            break;
        case 'update_ideaboxRefFile':
            $ideaboxcardsHandle->ajax_updateIdeaboxRefFile();
            break;
        case 'update_ideaboxRefUrl':
            $ideaboxcardsHandle->ajax_updateIdeaboxRefUrl();
            break;
        case 'delete_ideabox':
            $ideaboxcardsHandle->ajax_deleteIdeabox();
            break;
        case 'create_ideabox_by_image':
            $ideaboxcardsHandle->ajax_createIdeaboxByImage();
            break;
        case 'create_ideabox_by_doc':
            $ideaboxcardsHandle->ajax_createIdeaboxByDoc();
            break;
        case 'create_ideabox_by_url':
            $ideaboxcardsHandle->ajax_createIdeaboxByUrl();
            break;
        case 'update_orders':
            $ideaboxcardsHandle->ajax_updateOrders();
            break;
        case 'update_ideaTitle':
            $ideaboxcardsHandle->ajax_updateIdeaTitle();
            break;
        case 'update_ideaDescription':
            $ideaboxcardsHandle->ajax_updateIdeaDescription();
            break;
        default:
            $ideaboxcardsHandle->index();
            break;
    }
}
else{
    $ideaboxcardsHandle->index();
//    $ideaboxcardsHandle->test();
}