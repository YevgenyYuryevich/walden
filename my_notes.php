<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.12.2018
 * Time: 22:42
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class My_Notes extends \Core\Controller_core
{

    private $categoryModel;
    private $seriesModel;
    private $postsModel;
    private $postNotesModel;
    private $postsHandle;
    public function __construct()
    {
        parent::__construct();

        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();

        $this->load->model('api_m/PostNotes_m');
        $this->postNotesModel = new \Models\api\PostNotes_m();

        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();
        $this->postsModel = $this->postsHandle->model;
    }
    public function index(){
        $noteCategories = [];
        $noteSeries = [];
        $notePosts = [];

        $categoryFlg = [];
        $seriesFlg = [];
        $postKeyIndex = [];

        $notes = $this->postNotesModel->getRows(['postNote_user_ID' => $_SESSION['client_ID']]);
        foreach ($notes as & $note) {
            if (!isset($postKeyIndex[$note['postNote_post_ID']])) {
                $iPost = $this->postsModel->get($note['postNote_post_ID']);
                $postKeyIndex[$iPost['post_ID']] = count($notePosts);
                $iPost['notes'] = [];
                $notePosts[] = $iPost;
            }
            $post = & $notePosts[$postKeyIndex[$note['postNote_post_ID']]];
            $post['notes'][] = $note;

            $series = $this->seriesModel->get($post['intPost_series_ID']);
            if (isset($seriesFlg[$series['series_ID']])) {}
            else {
                $noteSeries[] = $series;
                $seriesFlg[$series['series_ID']] = true;
            }
            $note['series'] = $series['series_ID'];

            $category = $this->categoryModel->get($series['intSeries_category']);
            if (isset($categoryFlg[$category['category_ID']])) {}
            else {
                $categoryFlg[$category['category_ID']] = true;
                $noteCategories[] = $category;
            }
            $post['category'] = $category['category_ID'];
            $note['category'] = $category['category_ID'];
        }
        $noteCategories = \Helpers\utf8Encode($noteCategories);
        \Helpers\customSort($noteCategories, 'strCategory_name');
        $this->load->view('My_Notes_v', ['categories' => $noteCategories, 'series' => $noteSeries, 'posts' => $notePosts]);
    }
}


$handle = new My_Notes();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}