<?php

/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.04.2019
 * Time: 15:45
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Shared_Posts extends \Core\Controller_core
{
    private $sharedPostsModel;
    private $seriesModel;
    private $purchasedModel;
    private $postsModel;
    public function __construct()
    {
        parent::__construct();

        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
        }

        $this->load->model('api_m/SharedPosts_m');
        $this->sharedPostsModel = new \Models\api\SharedPosts_m();

        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();
    }
    public function index() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if ($id === false) {
            echo 'No ID';
            die();
        }
        $rows = $this->sharedPostsModel->getRows(['shared_id' => $id]);
        $posts = [];
        foreach ($rows as $row) {
            $post = $this->postsModel->get($row['post_id']);
            $posts[] = $post;
        }
        $series = $this->seriesModel->get($posts[0]['intPost_series_ID']);
        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        $series['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;
        $series = \Helpers\utf8Encode($series);
        foreach ($posts as &$post) {
            if ($post['strPost_nodeType'] == 'menu') {
                $where = [
                    'intPost_series_ID' => $post['intPost_series_ID'],
                    'intPost_parent' => $post['post_ID']
                ];
                $paths = $this->postsModel->getPosts($where);
                $post['paths'] = $paths;
            }
        }
        $this->load->view('Shared_Posts_v', ['series' => $series, 'posts' => $posts]);
    }
}
$handle = new Shared_Posts();
$handle->index();