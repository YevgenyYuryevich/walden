<?php
//CONNECT TO DATABASE
    global $myconnection;
	try {
	// Make a MySQL Connection
	    $myconnection = new PDO ("mysql:host=$database_host;dbname=$database_name","$database_username","$database_password");
	} catch (PDOException $e) {
	  echo "Failed to get DB handle: " . $e->getMessage() . "\n";
	  exit;
	}
	$myconnection->exec("set names utf8");
?>