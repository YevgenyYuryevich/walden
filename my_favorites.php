<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.10.2018
 * Time: 19:24
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class My_Favorites extends \Core\Controller_core
{
    private $categoryModel;
    private $seriesModel;
    private $postsModel;
    private $favModel;
    private $postsHandle;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();

        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();

        $this->load->model('api_m/Favorite_m');
        $this->favModel = new \Models\api\Favorite_m();

        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();
    }
    public function index() {
        $categories = $this->categoryModel->getRows([], '*', 'strCategory_name ASC');
        $favCategories = [];

        foreach ($categories as $category) {
            $series = $this->seriesModel->getRows(['intSeries_category' => $category['category_ID']]);
            $favSeries = [];
            foreach ($series as $seriesRow) {
                $favs = $this->favModel->getRows(['intFavorite_series_ID' => $seriesRow['series_ID'], 'intFavorite_client_ID' => $_SESSION['client_ID']]);
                $favPosts = [];
                foreach ($favs as $fav) {
                    if ($fav['intFavorite_post_ID']){
                        $favPost = $this->postsModel->get($fav['intFavorite_post_ID']);
                        if ($favPost){
                            $favPost['viewDay'] = $this->postsHandle->calcDay($favPost);
                            array_push($favPosts, $favPost);
                        }
                    }
                }
                if ($favPosts) {
                    $seriesRow['posts'] = $favPosts;
                    $favSeries[] = $seriesRow;
                }
            }
            if ($favSeries) {
                $category['series'] = $favSeries;
                $favCategories[] = $category;
            }
        }
        $favCategories = \Helpers\utf8Encode($favCategories);
        $this->load->view('My_Favorites_v', ['categories' => $favCategories]);
    }
}

$handle = new My_Favorites();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}