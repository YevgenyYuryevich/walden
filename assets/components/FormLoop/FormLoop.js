var FormLoopClass;

(function () {
    'use strict';
    FormLoopClass = function (domRoot, loopData, option) {
        var domTHeadTr, domTBody;

        var self = this;
        var bindData = function () {
            loopData.fields.forEach(function (field) {
                domTHeadTr.find('th.sample').clone().removeClass('sample').removeAttr('hidden').html(field.strFormField_name).appendTo(domTHeadTr);
            });
            for (var i = 0; i < parseInt(loopData.formLoop_rows); i++) {
                var domTr = domTBody.find('tr.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domTBody);
                loopData.fields.forEach(function (field) {
                    var d = domTr.find('td.sample').clone().removeClass('sample').removeAttr('hidden').addClass('type-' + field.strFormField_type).appendTo(domTr);
                    d.data('field', field);
                    d.data('index', i);
                    var answer = field.answers.find(function (value) {
                        return parseInt(value.intFormFieldAnswer_index) == i;
                    });
                    if (answer) {
                        d.data('answer', answer);
                        switch (field.strFormField_type) {
                            case 'text':
                                d.find('.text-value').html(answer.strFormFieldAnswer_answer);
                                break;
                            case 'checkbox':
                                d.find('.checkbox-value').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                                break;
                        }
                    }
                    else {
                    }
                    if (option.mode === 'answer') {
                        switch (field.strFormField_type) {
                            case 'text':
                                d.find('.text-value').attr('contenteditable', false);
                                break;
                            case 'checkbox':
                                break;
                        }
                    }
                });
            }
        }
        var bindEvents = function () {
            domTBody.find('tr td .text-value').blur(function () {
                var domTd = $(this).parents('td');
                var field = domTd.data('field');
                var answer = domTd.data('answer');
                var index = domTd.data('index');
                var value = $(this).html();
                if (answer) {
                    domTd.addClass('status-loading');
                    updateAnswer(answer.formFieldAnswer_ID, {strFormFieldAnswer_answer: value}).then(function () {
                        domTd.removeClass('status-loading');
                    });
                }
                else {
                    domTd.addClass('status-loading');
                    insertAnswer({
                        intFormFieldAnswer_field_ID: field.formField_ID,
                        strFormFieldAnswer_answer: value,
                        intFormFieldAnswer_client_ID: CLIENT_ID,
                        intFormFieldAnswer_index: index
                    }).then(function (res) {
                        domTd.removeClass('status-loading');
                        domTd.data('answer', res);
                    });
                }
            });
            domTBody.find('tr td .checkbox-value').change(function () {
                var domTd = $(this).parents('td');
                var field = domTd.data('field');
                var answer = domTd.data('answer');
                var index = domTd.data('index');
                var value = $(this).prop('checked') ? 1 : 0;
                if (answer) {
                    domTd.addClass('status-loading');
                    updateAnswer(answer.formFieldAnswer_ID, {strFormFieldAnswer_answer: value}).then(function () {
                        domTd.removeClass('status-loading');
                    });
                }
                else {
                    domTd.addClass('status-loading');
                    insertAnswer({
                        intFormFieldAnswer_field_ID: field.formField_ID,
                        strFormFieldAnswer_answer: value,
                        intFormFieldAnswer_client_ID: CLIENT_ID,
                        intFormFieldAnswer_index: index
                    }).then(function (res) {
                        domTd.removeClass('status-loading');
                        domTd.data('answer', res);
                    });
                }
            });
        }
        var insertAnswer = function (sets) {
            return ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                sets.formFieldAnswer_ID = res.data;
                return sets;
            });
        }
        var updateAnswer = function (id, sets) {
            return ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', where: id, sets: sets}, false);
        }
        this.empty = function () {
            domTHeadTr.find('th:not(.sample)').remove();
            domTBody.find('tr:not(.sample)').remove();
        }
        this.dom = function () {
            return domRoot;
        }
        this.init = function () {
            domRoot.attr('data-form-loop', loopData.formLoop_ID);
            domRoot.addClass('mode-' + option.mode);
            domTHeadTr = domRoot.find('thead tr');
            domTBody = domRoot.find('tbody');
            self.empty();
            if (typeof loopData === 'object') {
                loopData.fields.sort(function (a, b) {
                    return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                });
                bindData();
                if (option.mode != 'answer') {
                    bindEvents();
                }
            }
            else {
                ajaxAPiHandle.apiPost('FormLoop.php', {action: 'get_loop', where: loopData}, false).then(function (res) {
                    loopData = res.data;
                    if (loopData) {
                        loopData.fields.sort(function (a, b) {
                            return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                        });
                        bindData();
                        if (option.mode != 'answer') {
                            bindEvents();
                        }
                    }
                    else {
                        domRoot.remove();
                    }
                });
            }
        }
    }
})();