let StripeCheckoutClass;

(function () {
  StripeCheckoutClass = function (stripePublicKey) {
    let stripe;
    let callbackAfterJoin = function() {};
    let redirectToCheckout = function (checkoutSessionID) {
      stripe.redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: checkoutSessionID
      }).then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
    };
    this.open = function (series) {
      ajaxAPiHandle.apiPost('Series.php', {action: 'create_checkout_session', id: series, url: window.location.href}).then(function (res) {
        redirectToCheckout(res.data.id);
      });
    };
    this.afterJoin = function (fn) {
      callbackAfterJoin = fn;
    };
    this.init = function () {
      stripe = Stripe(stripePublicKey);
    }
  }
})();
