var FormLoopEditorClass;
(function () {
    'use strict';
    FormLoopEditorClass = function (domRoot, options) {
        var domPreviewFieldsList, domAddFieldWrap, domLoopFieldList;
        var loopRowsLength;
        var loopFieldItemHandles = [];
        var callbackOnCreate = function (v) {}
        var bindEvents = function () {
            domAddFieldWrap.find('.type-choose-item').click(function () {
                domAddFieldWrap.find('.type-choose-item').removeClass('active-item');
                $(this).addClass('active-item');
            });
            domRoot.find('.add-form-btn').click(function () {
                var sets = {
                    strFormField_name: domAddFieldWrap.find('.form-question-input').val(),
                    strFormField_helper: domAddFieldWrap.find('.form-helper-input').val(),
                    strFormField_type: domAddFieldWrap.find('.type-choose-item.active-item').data('type')
                };
                if (!sets.strFormField_name) {
                    alert('Please type field name.');
                    return false;
                }
                var hdl = new LoopFieldClass(sets);
                hdl.init();
                loopFieldItemHandles.push(hdl);
                domAddFieldWrap.find('.form-question-input').val('');
                domAddFieldWrap.find('.form-helper-input').val('');
            });
            domRoot.find('.create-loop-btn').click(function () {
                createLoop();
            });
        };
        var createLoop = function () {
            var loopSets = {
                formLoop_series_ID: options.seriesId,
                formLoop_name: domRoot.find('.form-loop-name-input').val(),
                formLoop_rows: 1,
                formLoop_cols: loopFieldItemHandles.length,
                formLoop_another_text: domRoot.find('.form-loop-add-another-input').val(),
            }
            if (!loopSets.formLoop_name) {
                alert('Please type loop name');
                return false;
            }
            if (!loopSets.formLoop_cols) {
                alert('Please add at least 1 field');
                return false;
            }
            ajaxAPiHandle.apiPost('FormLoop.php', {action: 'insert', sets: loopSets}).then(function (res) {
                var loop = res.data;
                createFields(loop.formLoop_ID).then(function (fields) {
                    fields.forEach(function (f) {
                        f.answers = [];
                    });
                    loop.fields = fields;
                    callbackOnCreate(loop);
                    domRoot.find('.form-loop-name-input').val('');
                });
            });
        };
        var createFields = function (loopId) {
            var inserts = [];
            loopFieldItemHandles.forEach(function (hdl) {
                var sets = hdl.data();
                sets.formField_loop_ID = loopId;
                inserts.push(sets);
            });
            return ajaxAPiHandle.apiPost('FormFields.php', {action: 'insert_many', inserts: inserts}).then(function (res) {
                return res.data;
            });
        }
        var LoopFieldClass = function (fieldData) {
            var domItem;
            var bindData = function () {
                domItem.addClass('field-type-' + fieldData.strFormField_type);
                domItem.find('.item-name-input').text(fieldData.strFormField_name);
            }
            var bindEvents = function () {
                domItem.find('.delete-action').click(function () {
                    swal({
                        title: "Are you sure?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete) {
                            remove();
                        }
                    });
                });
            }
            var remove = function () {
                var index = domItem.index() - 1;
                loopFieldItemHandles.splice(index, 1);
                domItem.remove();
            }
            this.data = function () {
                fieldData.intFormField_series_ID = options.seriesId;
                fieldData.intFormField_sort = domItem.index();
                return fieldData;
            }
            this.init = function () {
                domItem = domLoopFieldList.find('.loop-form-field-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domLoopFieldList);
                bindData();
                bindEvents();
            }
        }
        this.onCreate = function (fn) {
            callbackOnCreate = fn;
        }
        this.init = function () {
            loopRowsLength = parseInt(domRoot.find('.form-loop-length-input').val());
            domPreviewFieldsList = domRoot.find('.loop-preview-wrapper .fields-list');
            domAddFieldWrap = domRoot.find('.add-field-wrapper');
            domLoopFieldList = domRoot.find('.loop-form-field-list');
            domLoopFieldList.sortable({
                placeholder: 'ui-state-highlight',
                items: '.loop-form-field-item',
                forceHelperSize: true,
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: 'original',
                revert: 0, // animation in milliseconds
                opacity: 0.9,
            });
            bindEvents();
        }
    }
})();