var PostCopyEmbedClass;
(function () {
    'use strict';
    PostCopyEmbedClass = function (domComponent, post) {
        var domEmbed, domCode;
        var self = this;
        var bindEvents = function () {
            domCode.on('focus', function () {
                domCode.select();
                document.execCommand("Copy");
            });
            domCode.on('click', function () {
                domCode.select();
                document.execCommand("Copy");
            });
            domEmbed.find('.copy-embed-code-btn').click(function () {
                domCode.select();
                document.execCommand("Copy");
            });
            $(window).click(function () {
                if (domEmbed.hasClass('in')) {
                    domEmbed.collapse('hide');
                }
            });
            domEmbed.click(function (event) {
                event.stopPropagation();
            });
        };
        var generateEmbedCode = function (url, title, des) {
            var domWrp = $('<div></div>');
            var domEmbed = $('<blockquote class="walden-embedly-card"></blockquote>').appendTo(domWrp);

            $('<h4><a href="'+ url +'">'+ title +'</a></h4>').appendTo(domEmbed);
            $('<p>'+ des +'</p>').appendTo(domEmbed);
            var embedCode = domWrp.html();
            embedCode += '<script async src="'+ BASE_URL + '/assets/js/yevgeny/embed-platform.js?version=1.0" charset="UTF-8"></script>';
            domWrp.remove();
            return embedCode.replace(/&amp;/g, '&');
        }

        this.bindData = function (nPost) {
            if (typeof post !== "undefined") {
                post = $.extend({}, post, nPost);
            }
            var embedUrl = BASE_URL + '/view.php?action=embed_page&id=' + post.id;

            if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                embedUrl += ('&affiliate_id=' + AFFILIATE_ID);
            }
            domCode.val(generateEmbedCode(embedUrl, post.title, post.body));
        };
        this.init = function () {
            domEmbed = domComponent.find('#post-embed-code');
            domCode = domEmbed.find('textarea');
            self.bindData();
            bindEvents();
        }
    };
})();