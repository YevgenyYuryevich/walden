let SeriesOwnersLightClass;

(function () {
    'use strict';
    SeriesOwnersLightClass = function (domRoot, series) {

        let domCreator, domOwnerList;

        let creator = false, owners = [];
        let itemHandles = [];
        let bindData = function () {
            ajaxAPiHandle.apiPost('Series.php', {action: 'get_owners', id: series.series_ID}, false).then(function (res) {
                if (res.data.length === 1) {
                    creator = res.data[0];
                    domRoot.addClass('owner-single');
                }
                else {
                    creator = res.data.find(function (item) {
                        return item.id == series.intSeries_client_ID;
                    });
                    owners = res.data.filter(function (item) {
                        return item.id != series.intSeries_client_ID;
                    });
                }
                domCreator.find('.creator__name').attr('href', '/user_profile?id=' + creator.id);
                if (creator.image) {
                    domCreator.find('.creator__image img').attr('src', creator.image);
                }
                domCreator.find('.creator__name').html(creator.f_name);
                owners.forEach(function (itemData) {
                    var hdl = new OwnerItemClass(itemData);
                    hdl.init();
                    itemHandles.push(hdl);
                });
            });
        };
        let OwnerItemClass = function (owner) {
            let domItem;
            let bindData = function () {
                domItem.attr('href', '/user_profile?id=' + owner.id);
                if (owner.image) {
                    domItem.find('.item__image img').attr('src', owner.image);
                }
                domItem.find('.item__name').html(owner.f_name);
            };
            this.init = function () {
                domItem = domOwnerList.find('.list__item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domOwnerList);
                bindData();
            }
        };
        this.setReadingTime = function (readingTime) {
            domCreator.find('.reading__time').show();
            domCreator.find('.reading__time span').html(readingTime);
        }
        this.init = function () {
            domCreator = domRoot.find('.series__creator__wrapper');
            domOwnerList = domRoot.find('.owners__dropdown .owner__list');
            bindData();
        }
    }
})();
