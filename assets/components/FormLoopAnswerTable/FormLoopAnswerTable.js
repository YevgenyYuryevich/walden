var FormLoopAnswerTableClass;

(function () {
    'use strict';
    FormLoopAnswerTableClass = function (domRoot, formLoopData) {
        var bindData = function () {
            domRoot.attr('data-form-loop', formLoopData.formLoop_ID);
            formLoopData.fields.sort(function (a, b) {
                return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
            });

            formLoopData.fields.forEach(function (field) {
                var domField;
                if (domRoot.find('thead tr th[data-field="' + field.formField_ID + '"]').length) {
                    domField = domRoot.find('thead tr th[data-field="' + field.formField_ID + '"]');
                } else {
                    domField = domRoot.find('thead tr th.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('thead tr'));
                    domField.attr('data-field', field.formField_ID);
                }
                domField.text(field.strFormField_name);
            });
            for (var r = 0; r < parseInt(formLoopData.formLoop_rows); r++) {
                var domTr;
                if (domRoot.find('tbody tr[data-index="' + r + '"]').length) {
                    domTr = domRoot.find('tbody tr[data-index="' + r + '"]');
                } else {
                    domTr = domRoot.find('tbody tr.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('tbody'));
                    domTr.attr('data-index', r);
                }
                formLoopData.fields.forEach(function (field) {
                    var domField;
                    if (domTr.find('td[data-field="' + field.formField_ID + '"]').length) {
                        domField = domTr.find('td[data-field="' + field.formField_ID + '"]');
                    } else {
                        domField = domTr.find('td.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domTr);
                        domField.attr('data-field', field.formField_ID);
                    }
                    domField.addClass('type-' + field.strFormField_type);
                    var answer = field.answers.find(function (answer) {
                        return parseInt(answer.intFormFieldAnswer_index) === r;
                    });
                    if (answer) {
                        if (field.strFormField_type === 'text') {
                            domField.find('.type-value.text-value').html(answer.strFormFieldAnswer_answer);
                        } else {
                            domField.find('.type-value.checkbox-value').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                        }
                    } else {
                        if (field.strFormField_type === 'text') {
                            domField.find('.type-value.text-value').html('');
                        } else {
                            domField.find('.type-value.checkbox-value').prop('checked', false);
                        }
                    }
                });
            }
        }
        this.dom = function () {
            return domRoot;
        }
        this.init = function () {
            if (typeof formLoopData === 'object') {
                formLoopData.fields.sort(function (a, b) {
                    return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                });
                bindData();
            }
            else {
                ajaxAPiHandle.apiPost('FormLoop.php', {action: 'get_loop', where: formLoopData}, false).then(function (res) {
                    formLoopData = res.data;
                    if (formLoopData) {
                        formLoopData.fields.sort(function (a, b) {
                            return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                        });
                        bindData();
                    }
                    else {
                        domRoot.remove();
                    }
                });
            }
        }
    }
})();