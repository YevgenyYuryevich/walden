var EmbedEditorClass = null;
(function () {
    'use strict';
    EmbedEditorClass = function (domRoot) {
        var self = this;

        var domPreview, domTotalCode, domCopyMsg;
        var htmlEditor, cssEditor, jsEditor, totalCodeEditor;

        var reloadTimeout;

        var liveDelay = 2000;

        var callbacksOnReload = [];

        var bindEvents = function () {
            htmlEditor.on('change', function () {
                clearTimeout(reloadTimeout);
                reloadTimeout = setTimeout(function () {
                    self.reloadPreview();
                }, liveDelay);
            });
            cssEditor.on('change', function () {
                clearTimeout(reloadTimeout);
                reloadTimeout = setTimeout(function () {
                    self.reloadPreview();
                }, liveDelay);
            });
            jsEditor.on('change', function () {
                clearTimeout(reloadTimeout);
                reloadTimeout = setTimeout(function () {
                    self.reloadPreview();
                }, liveDelay);
            });
            domRoot.find('.toggle-preview').click(function () {
                domRoot.find('.toggle-total-embed').parent().removeClass('active');
                if ($(this).parent().hasClass('active')) {
                    $(this).parent().removeClass('active');
                    domPreview.parent().hide();
                    domCopyMsg.removeClass('active');
                }
                else {
                    $(this).parent().addClass('active');
                    domPreview.parent().show();
                    domTotalCode.hide();
                    domPreview.show();
                    domCopyMsg.removeClass('active');
                }
            });
            domRoot.find('.toggle-total-embed').click(function () {
                domRoot.find('.toggle-preview').parent().removeClass('active');
                if ($(this).parent().hasClass('active')) {
                    $(this).parent().removeClass('active');
                    domPreview.parent().hide();
                    domCopyMsg.removeClass('active');
                }
                else {
                    $(this).parent().addClass('active');
                    domPreview.parent().show();
                    domPreview.hide();
                    domTotalCode.show();
                    domCopyMsg.addClass('active');
                }
            });
            domRoot.find('.editor-header .nav.nav-tabs li').find('a[href="#html-tab"], a[href="#css-tab"], a[href="#js-tab"]').click(function (e) {
                var $this = $(this);
                if ($(this).parent().hasClass('active')) {
                    setTimeout(function () {
                        $this.parent().removeClass('active');
                    }, 100);
                    domRoot.find('.edit-col').hide();
                    e.preventDefault();
                }
                else {
                    domRoot.find('.edit-col').show();
                }
            });
            domCopyMsg.click(function () {
                self.copyEmbed();
                $(this).addClass('copied');
                setTimeout(function () {
                    domCopyMsg.removeClass('active');
                    domCopyMsg.removeClass('copied');
                }, 1400);
            });
        }
        this.copyEmbed = function () {
            var el = document.createElement('textarea');
            $(el).css({position: 'absolute', left: '-10000px'});
            el.value = self.getTotalEmbed();
            domRoot.append($(el));
            el.select();
            document.execCommand('copy');
            $(el).remove();
        }
        this.onReload = function (cb) {
            callbacksOnReload.push(cb);
        }
        this.reloadPreview = function () {
            clearTimeout(reloadTimeout);
            var totalEmbed = self.getTotalEmbed();
            domPreview.empty();
            domPreview.append(totalEmbed);
            totalCodeEditor.setValue(totalEmbed);
            callbacksOnReload.forEach(function (cb) {
                cb(totalEmbed);
            });
        }

        this.getTotalEmbed = function () {
            var tHtml = '<div class="walden-embedly-card-wrapper" style="line-height: 0">\n';
            tHtml += '<style type="text/src-only" id="walden-embedly-style">' + cssEditor.getValue() + '</style>\n';
            tHtml += '<script type="application/src-only" id="walden-embedly-script">' + jsEditor.getValue() + '</script>\n';
            tHtml += htmlEditor.getValue();
            tHtml += '</div>';
            return tHtml;
        }

        this.setHtml = function (html) {
            htmlEditor.setValue(html);
        }

        this.setCss = function (css) {
            cssEditor.setValue(css);
        }
        this.setJs = function (js) {
            jsEditor.setValue(js);
        }
        this.getHtml = function () {
            return htmlEditor.getValue();
        }

        this.init = function () {
            domPreview = domRoot.find('#embed-editor-preview');
            domTotalCode = domRoot.find('#embed-total-code');
            domCopyMsg = domRoot.find('.copy-msg');
            htmlEditor = ace.edit("html-editor");
            htmlEditor.setTheme("ace/theme/monokai");
            htmlEditor.setFontSize(18);
            htmlEditor.session.setMode("ace/mode/html");
            htmlEditor.getSession().setUseWorker(false);

            cssEditor = ace.edit("css-editor");
            cssEditor.setTheme("ace/theme/monokai");
            cssEditor.setFontSize(18);
            cssEditor.session.setMode("ace/mode/css");

            jsEditor = ace.edit("js-editor");
            jsEditor.setTheme("ace/theme/monokai");
            jsEditor.setFontSize(18);
            jsEditor.session.setMode("ace/mode/javascript");


            totalCodeEditor = ace.edit("embed-total-code");
            totalCodeEditor.setTheme("ace/theme/monokai");
            totalCodeEditor.setFontSize(18);
            totalCodeEditor.session.setMode("ace/mode/html");
            totalCodeEditor.getSession().setUseWorker(false);

            reloadTimeout = setTimeout(function () {
            });
            bindEvents();
        }
    }
})();