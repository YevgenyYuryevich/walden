var FormFieldClass;
(function () {
    FormFieldClass = function (domField, field) {
        var domValue;
        var answer = false;

        var bindEvents = function () {
            switch (field.strFormField_type) {
                case 'text':
                    domValue.blur(function () {
                        updateAnswer();
                    });
                    break;
                case 'checkbox':
                    domValue.change(function () {
                        updateAnswer();
                    });
                    break;
            }
        }
        var updateAnswer = function () {
            var sets = {};
            domField.addClass('status-loading');
            switch (field.strFormField_type) {
                case 'text':
                    sets.strFormFieldAnswer_answer = domValue.html();
                    break;
                case 'checkbox':
                    sets.strFormFieldAnswer_answer = domValue.prop('checked') ? 1 : 0;
                    break;
            }
            if (answer) {
                ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                    domField.removeClass('status-loading');
                    domField.addClass('status-loading-finished');
                    domField.one(animEndEventName, function () {
                        domField.removeClass('status-loading-finished');
                    });
                    answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                });
            }
            else {
                $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                    domField.removeClass('status-loading');
                    domField.addClass('status-loading-finished');
                    answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                    domField.one(animEndEventName, function () {
                        domField.removeClass('status-loading-finished');
                    });
                });
            }
        }
        var bindData = function () {
            if (field.answer) {
                answer = field.answer;
                switch (field.strFormField_type) {
                    case 'text':
                        domValue.html(answer.strFormFieldAnswer_answer);
                        break;
                    case 'checkbox':
                        domValue.prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                        break;
                }
            }
        }
        this.dom = function () {
            return domField;
        }
        this.init = function () {
            domField.attr('data-form-field', field.formField_ID);
            domField.addClass('type-' + field.strFormField_type);
            domValue = domField.find('.form-field-value').find('.' + field.strFormField_type + '-value');
            if (domValue.length === 0) {
                switch (field.strFormField_type) {
                    case 'text':
                        domValue = domField.find('.form-field-value');
                        break;
                    case 'checkbox':
                        domValue = domField.find('.form-field-value input');
                        break;
                }
            }
            domField.find('[data-toggle="popover"]').popover();
            bindData();
            bindEvents();
        }
    }
})();