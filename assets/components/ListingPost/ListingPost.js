var ListingPostClass;

(function () {
    'use strict';
    ListingPostClass = function (series, domListing) {

        var domList, domUnreadList, domDragDropPane, domListingFooter, domSharedPostList, domShareSeries, domSharePost;
        var itemHandles = [], seriesOwnersHandle, seriesStructureTreeHandle, sharedItemHandles = [];
        var selectedPost;
        var isDataLoaded = false;

        var listingHandle = this;
        var callbacksAfterSelected = [];
        var callbacksAfterPathSelected = [];
        var unReadPostCnt = 0;
        var mode = 'default';
        let self = this;
        var bindEvents = function () {
            var dragTimeout = setTimeout(function () {});
            domListing.find('.section-close').click(function () {
                listingHandle.close();
            });
            $('.site-back-drop').click(function () {
                listingHandle.close();
            });
            domListing.find('.listing-post-content').scroll(function () {
                if (domListing.hasClass('mode-edit')) {
                    seriesStructureTreeHandle.sortableRefresh();
                }
            });
            domListing.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('dragover dragenter', function () {
                clearTimeout(dragTimeout);
                domListing.addClass('drag-over');
            }).on('dragenter', function (e) {
            }).on('dragleave dragend drop', function() {
            }).on('dragleave', function (e) {
                dragTimeout = setTimeout(function () {
                    domListing.removeClass('drag-over');
                }, 100);
            }).on('drop', function(e) {
                domListing.removeClass('drag-over');
                var dataInfo = helperHandle.getInfoFromTransfer(e.originalEvent.dataTransfer);
                if (dataInfo.type === 'url') {
                    makePostFromUrl(dataInfo.url).then(function (data) {
                        insertUnreadPost(data);
                    });
                } else if (dataInfo.type === 'other') {
                    alert('Wrong Item');
                } else {
                    makePostFromFile(dataInfo.file).then(function (data) {
                        insertUnreadPost(data);
                    });
                }
            });
            domListingFooter.find('.share-next-btn').click(function () {
                var hdls = seriesStructureTreeHandle.getSharedHandles();
                if (hdls.length === 1) {
                    let hdl = hdls[0];
                    self.sharePostAction(hdl.data());
                } else {
                    sharePostSetsAction();
                }
            });
            domListing.find('.share-series-btn').click(function () {
                listingHandle.gotoShareSeries();
            });
            domListing.find('.share-back-btn').click(function () {
                domListing.removeClass('share-step-next').removeClass('share-series').removeClass('share-post');
            });
        }
        let sharePostSetsAction = function () {
            sharedItemHandles.forEach(function (hdl) {
                hdl.dom().remove();
            });
            sharedItemHandles = [];
            var hdls = seriesStructureTreeHandle.getSharedHandles();
            var posts = [], ids = [];
            hdls.forEach(function (hdl) {
                if ( hdl.data().id ) {
                    posts.push( hdl.data() );
                    ids.push( hdl.data().id );
                }
            });
            if (authHandle.user()) {
                sharePosts(ids).then(function (shareId) {
                    if (shareId) {
                        domListing.addClass('share-step-next');
                        seriesStructureTreeHandle.cleanShared();
                        posts.forEach(function (post) {
                            var hdl = new SharedItemClass(post);
                            hdl.init();
                            sharedItemHandles.push(hdl);
                        });
                    }
                });
            } else {
                authHandle.popup();
                authHandle.activeLogin();
                authHandle.afterLogin(function () {
                    sharePosts(ids).then(function (shareId) {
                        if (shareId) {
                            domListing.addClass('share-step-next');
                            seriesStructureTreeHandle.cleanShared();
                            posts.forEach(function (post) {
                                var hdl = new SharedItemClass(post);
                                hdl.init();
                                sharedItemHandles.push(hdl);
                            });
                        }
                    });
                });
            }
        };
        this.sharePostAction = function (post) {

            domSharePost.find('.item-image').css('background-image', 'url(' + post.image + ')');
            domSharePost.find('.item-title').text(post.title);
            domSharePost.find('.preview-btn').attr('href', 'view_blog?id=' + post.id);

            let socialLinks = helperHandle.socialLinks(BASE_URL + '/view_blog?id=' + post.id, post.title);
            domListing.find('.share-with-list .share-item.item-facebook').attr('href', socialLinks.facebook);
            domListing.find('.share-with-list .share-item.item-twitter').attr('href', socialLinks.twitter);
            domListing.find('.share-with-list .share-item.item-linkedin').attr('href', socialLinks.linkedin);
            domListing.find('.share-with-list .share-item.item-email').attr('href', socialLinks.email);

            listingHandle.setMode('share');
            domListing.addClass('share-post');
            domListing.addClass('share-step-next');
        };
        var sharePosts = function (posts) {
            if (!posts.length) {
                return Promise.resolve(false);
            }
            return ajaxAPiHandle.apiPost('SharedPosts.php', {action: 'share_many', posts: posts}).then(function (res) {
                var share_id = res.data;
                var socialLinks = helperHandle.socialLinks(BASE_URL + '/shared_posts?id=' + share_id, 'walden blog');
                domListing.find('.share-with-list .share-item.item-facebook').attr('href', socialLinks.facebook);
                domListing.find('.share-with-list .share-item.item-twitter').attr('href', socialLinks.twitter);
                domListing.find('.share-with-list .share-item.item-linkedin').attr('href', socialLinks.linkedin);
                domListing.find('.share-with-list .share-item.item-email').attr('href', socialLinks.email);
                return res.data;
            });
        }
        var makePostFromFile = function (file) {
            var postTypeArr = {
                'video': 2,
                'audio': 0,
                'image': 8,
                'text': 7,
                'application': 7,
            };
            var data = {};
            data.title = file.name;
            data.strType = helperHandle.extractTypeFromFileType(file.type);
            data.type = postTypeArr[data.strType];
            data.fullType = file.type;
            var resPromise;
            var ajaxData;
            if (data.strType === 'text' || data.strType === 'application') {
                ajaxData = new FormData();
                ajaxData.append('file', file);
                resPromise = ajaxAPiHandle.multiPartApiPost('FileToHtml.php', ajaxData).then(function (res) {
                    data.body = res.data;
                    return res.data;
                });
            } else if (data.strType === 'image') {
                ajaxData = new FormData();
                ajaxData.append('file', file);
                ajaxData.append('sets', JSON.stringify({where: 'assets/images', prefix: 'post'}));
                resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                    data.image = res.data.abs_url;
                    return data.image;
                });
            } else {
                ajaxData = new FormData();
                ajaxData.append('file', file);
                ajaxData.append('sets', JSON.stringify({where: 'assets/media/' + data.strType, prefix: 'post'}));
                resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                    if (res.status) {
                        data.body = res.data.abs_url;
                        return res.data.abs_url;
                    }
                    else {
                        swal('Sorry, these files are too large memory to upload.');
                        return false;
                    }
                }, function () {
                    swal('Sorry, these files are too large memory to upload.');
                    return false;
                });
            }
            return resPromise.then(function (res) {
                if (res) {
                    return data;
                }
            });
        }
        var makePostFromUrl = function (url) {
            var sourceType = helperHandle.getSourceTypeFromUrl(url);
            var what = 'curl';
            var where = url;
            switch (sourceType) {
                case 'youtube':
                    what = 'youtube';
                    where = {id: helperHandle.parseYoutubeUrl(url).id};
                    break;
                default:
                    break;
            }
            var fData;
            return ajaxAPiHandle.apiPost('Api.php', {action: 'get', what: what, where: where}, true, 200).then(function (res) {
                switch (what) {
                    case 'youtube':
                        fData = helperHandle.formatItemData(res.data, 'youtube');
                        break;
                    default:
                        fData = helperHandle.formatItemData(res.data);
                        fData.body = fData.body + '<div><a href="'+ url +'" target="_blank">Continue Reading...</a></div>';
                        break;
                }
                return fData;
            });
        }
        var insertUnreadPost = function (data) {
            console.log(data);
            var post = helperHandle.postFormat(data);
            post.intPost_series_ID = series.series_ID;
            ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: post}).then(function (res) {
                console.log(res.data);
                var hdl = new ItemClass(res.data);
                hdl.init();
                hdl.dom().appendTo(domUnreadList);
                itemHandles.push(hdl);
                unReadPostCnt++;
                domListing.addClass('has-new');
            });
        }
        var bindData = function () {
            domListing.find('.listing-post-header .series-title').text(series.strSeries_title);
            domListing.find('.listing-post-header .count-value').text(4);
            domListing.find('.top-btn-wrap .edit-btn').attr('href', BASE_URL + '/edit_series?id=' + series.series_ID);
            domListing.find('.top-btn-wrap .add-content-btn').attr('href', BASE_URL + '/add_posts?id=' + series.series_ID);
            domListing.find('.top-btn-wrap .embed-btn').attr('href', BASE_URL + '/edit_series?tab=embed&id=' + series.series_ID);

            domShareSeries.find('.item-image').css('background-image', 'url(' + series.strSeries_image + ')');
            domShareSeries.find('.child-count span').text(series.posts_count);
            domShareSeries.find('.item-title').text(series.strSeries_title);
            domShareSeries.find('.preview-btn').attr('href', 'preview_series?id=' + series.series_ID);
            let socialLinks = helperHandle.socialLinks(BASE_URL + '/preview_series?id=' + series.series_ID, series.strSeries_title);
            domListing.find('.series-share-with-list .share-item.item-facebook').attr('href', socialLinks.facebook);
            domListing.find('.series-share-with-list .share-item.item-twitter').attr('href', socialLinks.twitter);
            domListing.find('.series-share-with-list .share-item.item-linkedin').attr('href', socialLinks.linkedin);
            domListing.find('.series-share-with-list .share-item.item-email').attr('href', socialLinks.email);
        }
        var ItemClass = function (itemData) {
            var domItem;
            var bindEvents = function () {
                domItem.find('.item-title').click(function () {
                    if (itemData.strPost_nodeType !== 'path') {
                        ajaxAPiHandle.pagePost('view_blog', {action: 'get_post', id: itemData.post_ID}).then(function (res) {
                            listingHandle.update(selectedPost, {current: false});
                            callbacksAfterSelected.forEach(function (value) {
                                value(res.data);
                            });
                            selectedPost = res.data.post_ID;
                        });
                    }
                    else {
                        callbacksAfterPathSelected.forEach(function (value) {
                            value(itemData);
                        });
                        // from = 'post';
                        // selectPath(helperHandle.postToViewFormat(itemData));
                    }
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.post_ID}}, false);
                });
            }
            var bindData = function () {
                if (itemData.strPost_nodeType == 'path') {
                    if (itemData.strPost_featuredimage) {
                        domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                    }
                    else {
                        domItem.find('.item-img').attr('src', 'assets/images/global-icons/tree/path/'+ (itemData.intPost_order) +'.svg');
                    }
                }
                else {
                    domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                }
                domItem.find('.item-title').html(itemData.strPost_title);
                domItem.find('.media-icon-wrapper').addClass('type-' + itemData.intPost_type);
                if (itemData.strPost_duration) {
                    domItem.find('.item-duration').html(itemData.strPost_duration);
                }
                else {
                    helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                        domItem.find('.item-duration').html(res);
                        itemData.strPost_duration = res;
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                    });
                }
                domItem.find('.item-view-states-wrapper').removeClass('state-complete').removeClass('state-current').removeClass('state-normal');
                if (parseInt(itemData.boolCompleted)) {
                    domItem.find('.item-view-states-wrapper').addClass('state-complete');
                }
                else if (itemData.current) {
                    domItem.find('.item-view-states-wrapper').addClass('state-current');
                }
                else {
                    domItem.find('.item-view-states-wrapper').addClass('state-normal');
                }
            }
            this.setData = function (sets) {
                itemData = Object.assign(itemData, sets);
                bindData();
            }
            this.data = function () {
                return itemData;
            }
            this.dom = function () {
                return domItem;
            }
            this.init = function () {
                domItem = domListing.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                bindData();
                if (itemData.post_ID === selectedPost) {
                    domItem.find('.item-view-states-wrapper').removeClass('state-normal').addClass('state-current');
                }
                bindEvents();
            }
        }
        var SharedItemClass = function (itemData) {
            var domItem;
            var bindEvents = function () {
                domItem.find('.item-title').click(function () {
                    if (itemData.nodeType !== 'path') {
                        ajaxAPiHandle.pagePost('view_blog', {action: 'get_post', id: itemData.id}).then(function (res) {
                            listingHandle.update(selectedPost, {current: false});
                            callbacksAfterSelected.forEach(function (value) {
                                value(res.data);
                            });
                            selectedPost = res.data.post_ID;
                        });
                    }
                    else {
                        callbacksAfterPathSelected.forEach(function (value) {
                            value(itemData);
                        });
                    }
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.id}}, false);
                });
            }
            var bindData = function () {
                if (itemData.nodeType == 'path') {
                    if (itemData.image) {
                        domItem.find('.item-img').attr('src', itemData.image);
                    }
                    else {
                        domItem.find('.item-img').attr('src', 'assets/images/global-icons/tree/path/'+ (itemData.order) +'.svg');
                    }
                }
                else {
                    domItem.find('.item-img').attr('src', itemData.image);
                }
                domItem.find('.item-title').html(itemData.title);
                domItem.find('.media-icon-wrapper').addClass('type-' + itemData.type);
                if (itemData.duration) {
                    domItem.find('.item-duration').html(itemData.duration);
                }
                else if (itemData.body) {
                    helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                        domItem.find('.item-duration').html(res);
                        itemData.duration = res;
                    });
                }
                domItem.find('.item-view-states-wrapper').removeClass('state-complete').removeClass('state-current').removeClass('state-normal');
                if (parseInt(itemData.boolCompleted)) {
                    domItem.find('.item-view-states-wrapper').addClass('state-complete');
                }
                else if (itemData.current) {
                    domItem.find('.item-view-states-wrapper').addClass('state-current');
                }
                else {
                    domItem.find('.item-view-states-wrapper').addClass('state-normal');
                }
            }
            this.setData = function (sets) {
                itemData = Object.assign(itemData, sets);
                bindData();
            }
            this.data = function () {
                return itemData;
            }
            this.dom = function () {
                return domItem;
            }
            this.init = function () {
                domItem = domListing.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSharedPostList);
                bindData();
                if (itemData.id === selectedPost) {
                    domItem.find('.item-view-states-wrapper').removeClass('state-normal').addClass('state-current');
                }
                bindEvents();
            }
        }
        this.close = function () {
            domListing.removeClass('opened');
            $('.site-back-drop').hide();
            setTimeout(function () {
                $('body').css('padding-right', '0px');
                $('body').removeClass('modal-open');
                domListing.removeClass('share-step-next').removeClass('share-series').removeClass('share-post');
            }, 300);
        }
        this.open = function () {
            var paddingRight = window.innerWidth - $("body").prop("clientWidth") + 'px';
            $('body').addClass('modal-open');
            $('body').css('padding-right', paddingRight);
            if (isDataLoaded) {
                domListing.addClass('opened');
                $('.site-back-drop').show();
                return Promise.resolve(true);
            }
            else {
                var ajaxData = {
                    action: 'all_posts',
                    from: 'post',
                    id: selectedPost,
                };
                return ajaxAPiHandle.pagePost('view_blog', ajaxData).then(function (res) {
                    if (res.unReadPosts.length) {
                        domListing.addClass('has-new');
                        unReadPostCnt = res.unReadPosts.length;
                        res.unReadPosts.forEach(function (v) {
                            var hdl = new ItemClass(v);
                            hdl.init();
                            hdl.dom().appendTo(domUnreadList);
                            itemHandles.push(hdl);
                        });
                    }
                    domListing.find('.listing-post-header .count-value').text(res.data.length);
                    domShareSeries.find('.child-count span').text(res.data.length);
                    seriesStructureTreeHandle.init();
                    seriesStructureTreeHandle.onClick(function (itemData) {
                        if (itemData.nodeType !== 'path') {
                            ajaxAPiHandle.pagePost('view_blog', {action: 'get_post', id: itemData.id}).then(function (res) {
                                var postId = itemData.id;
                                listingHandle.update(postId, {current: false});
                                callbacksAfterSelected.forEach(function (value) {
                                    value(res.data);
                                });
                                selectedPost = res.data.post_ID;
                            });
                        }
                        else {
                            callbacksAfterPathSelected.forEach(function (value) {
                                value(itemData);
                            });

                        }
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.post_ID}}, false);
                    });
                    domListing.addClass('opened');
                    $('.site-back-drop').show();
                    isDataLoaded = true;
                    return seriesStructureTreeHandle.expand();
                });
            }
        }
        this.setSelected = function (p) {
            selectedPost = p;
        }
        this.update = function (id, sets) {
            itemHandles.forEach(function (value) {
                if (value.data().post_ID == id) {
                    value.setData(sets);
                }
            });
        }
        this.afterSelect = function (f) {
            callbacksAfterSelected.push(f);
        }
        this.afterPathSelect = function (f) {
            callbacksAfterPathSelected.push(f);
        }
        this.setMode = function (m) {
            domListing.removeClass('mode-' + mode);
            domListing.removeClass('mode-edit mode-static');
            if (m !== 'share') {
                domListing.removeClass('share-step-next');
            }
            mode = m;
            domListing.addClass('mode-' + m);
            seriesStructureTreeHandle.setMode(m);
        }
        this.getHandle = function (id) {
            return seriesStructureTreeHandle.getHandle(id);
        }
        this.gotoShareSeries = function() {
            listingHandle.setMode('share');
            domListing.addClass('share-series');
            domListing.addClass('share-step-next');
        }
        this.init = function () {
            domList = domListing.find('.listing-posts-content .posts-list');
            domSharedPostList = domListing.find('.shared-post-list');
            domShareSeries = domListing.find('.series-share-preview .series-article');
            domSharePost = domListing.find('.post-share-preview .post-article');
            domListingFooter = domListing.find('.listing-post-footer');
            domDragDropPane = domListing.find('.listing-drag-drop-pane');
            domUnreadList = domListing.find('.listing-post-content .unread-post-list');
            seriesStructureTreeHandle = new SeriesStructureTreeClass(domListing.find('.series-structure-tree-component'), series.series_ID);
            seriesOwnersHandle = new SeriesOwnersClass(domListing.find('.series-owners.component'), series);
            seriesOwnersHandle.init();
            $.fn.dndhover = function(options) {
                return this.each(function() {

                    var self = $(this);
                    var collection = $();

                    self.on('dragenter', function(event) {
                        if (collection.length === 0) {

                            self.trigger('dndHoverStart');
                        }
                        collection = collection.add(event.target);
                    });

                    self.on('dragleave', function(event) {
                        /*
                         * Firefox 3.6 fires the dragleave event on the previous element
                         * before firing dragenter on the next one so we introduce a delay
                         */
                        setTimeout(function() {
                            collection = collection.not(event.target);
                            if (collection.length === 0) {
                                self.trigger('dndHoverEnd');
                            }
                        }, 2000);
                    });
                });
            };
            bindData();
            bindEvents();
        }
    }
})();
