<section class="listing-post-component component">
    <header class="listing-post-header">
        <div class="title-wrapper">
                <span class="series-title">
                    Developing language skills
                </span>
            <div class="posts-count common-radius-aside"><span class="count-value"></span> posts</div>
        </div>
        <?php require_once ASSETS_PATH . '/components/SeriesOwners/SeriesOwners.html';?>
        <a href="javascript:;" class="section-close"></a>
    </header>
    <aside class="top-btn-wrap">
        <div class="flex-row vertical-center margin-between horizon-center">
            <div class="flex-col">
                <a class="site-green-btn edit-btn" href="javascript:;" target="_blank">Edit</a>
            </div>
            <div class="flex-col">
                <a class="site-green-btn add-content-btn" href="javascript:;" target="_blank">Add Content</a>
            </div>
            <div class="flex-col">
                <a class="site-green-btn embed-btn" href="javascript:;" target="_blank">Embed</a>
            </div>
            <div class="flex-col">
                <a class="site-green-btn share-series-btn" href="javascript:;">Share All</a>
            </div>
        </div>
    </aside>
    <aside class="d-mode-none d-mode-share-block text-center mt-1">
        <a class="site-green-btn share-series-btn" href="javascript:;">Share All</a>
    </aside>
    <div class="listing-post-content">
        <li class="item sample" hidden>
            <div class="flex-row margin-between">
                <div class="flex-col fix-col">
                    <img class="item-img" alt="" />
                </div>
                <div class="flex-col fix-col">
                    <div class="item-view-states-wrapper">
                        <svg version="1.1" class="complete-state view-state" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px" viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title>Rectangle 4</title>
                            <desc>Created with Sketch.</desc>
                            <g id="Page-1" sketch:type="MSPage">
                                <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                    <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"></path>
                                </g>
                            </g>
                                                        </svg>
                        <div class="current-state view-state"></div>
                        <div class="normal-state view-state"></div>
                    </div>
                </div>
                <div class="flex-col">
                    <div class="title-wrapper">
                        <aside class="media-icon-wrapper">
                            <i class="fa fa-youtube-play video-icon"></i>
                            <i class="fa fa-volume-up audio-icon"></i>
                            <i class="fa fa-file-text text-icon"></i>
                        </aside>
                        <a href="javascript:;" class="item-title">Great breakfasts</a> | <span class="item-duration">waiting...</span>
                    </div>
                </div>
            </div>
        </li>
        <div class="select-content-title">Select all of the content you would like to share</div>
        <div class="selected-content-title">Selected content you would like to share</div>
        <div class="new-title">New</div>
        <ul class="unread-post-list">
        </ul>
        <div class="all-title">All</div>
        <div class="series-share-preview">
            <article class="series-article">
                <div class="item-title d-none d-xs-block"></div>
                <div class="item-image-wrapper">
                    <div class="item-image"></div>
                    <div class="hover-content">
                        <div class="footer-buttons">
                            <div class="flex-row">
                                <div class="flex-col">
                                    <a href="javascript:;" class="preview-btn" target="_blank">
                                        <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="child-count-wrapper">
                    <div class="child-count"><span>2</span> posts</div>
                </div>
                <div class="item-title d-xs-none"></div>
            </article>
        </div>
        <div class="post-share-preview">
            <article class="post-article">
                <div class="item-title d-none d-xs-block"></div>
                <div class="item-image-wrapper">
                    <div class="item-image"></div>
                    <div class="hover-content">
                        <div class="footer-buttons">
                            <div class="flex-row">
                                <div class="flex-col">
                                    <a href="javascript:;" class="preview-btn" target="_blank">
                                        <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-title d-xs-none"></div>
            </article>
        </div>
        <ul class="shared-post-list">
        </ul>
        <?php require_once ASSETS_PATH . '/components/SeriesStructureTree/SeriesStructureTree.php';?>
        <div class="share-with-list flex-row vertical-center horizon-center margin-between">
            <div class="flex-col fix-col"><div class="share-with-text">Share with</div></div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-facebook">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                <g>
                                    <circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
                                    <path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                        c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                </g>
                                </svg>
                </a>
            </div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-twitter">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
                                                c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
                                                c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
                                                c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
                                                c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
                                                c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
                                                c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
                                                C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                                        </g>
                                    </g>
                                </svg>
                </a>
            </div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-linkedin">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
                                                c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
                                                c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
                                                C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
                                                c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
                                                 M27.865,83.739H41.27V43.409H27.865V83.739z"/>
                                        </g>
                                    </g>
                                </svg>
                </a>
            </div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-email">
                    <svg xmlns="http://www.w3.org/2000/svg" height="512pt" version="1.1" viewBox="0 0 512 512" width="512pt">
                        <g id="surface1">
                            <path d="M 512 256 C 512 397.386719 397.386719 512 256 512 C 114.613281 512 0 397.386719 0 256 C 0 114.613281 114.613281 0 256 0 C 397.386719 0 512 114.613281 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294118%,32.941176%,87.843137%);fill-opacity:1;" />
                            <path d="M 512 256 C 512 245.425781 511.347656 235.003906 510.101562 224.765625 L 379.667969 94.332031 L 100.5 409 L 196.542969 505.042969 C 215.625 509.582031 235.527344 512 256 512 C 397.386719 512 512 397.386719 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(22.745098%,25.490196%,80%);fill-opacity:1;" />
                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 100.5 409 L 411.5 409 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(98.039216%,36.470588%,5.882353%);fill-opacity:1;" />
                            <path d="M 411.5 188 L 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 409 L 411.5 409 Z M 411.5 188 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.705882%,29.803922%,5.882353%);fill-opacity:1;" />
                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 197.046875 293.835938 C 228.695312 328.527344 283.304688 328.527344 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;" />
                            <path d="M 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 319.847656 C 277.40625 319.910156 299.070312 311.246094 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(92.156863%,92.156863%,94.117647%);fill-opacity:1;" />
                            <path d="M 100.5 409 L 197.046875 303.164062 C 228.695312 268.472656 283.304688 268.472656 314.953125 303.164062 L 411.5 409 Z M 100.5 409 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,58.431373%,0%);fill-opacity:1;" />
                            <path d="M 314.953125 303.164062 C 299.070312 285.753906 277.40625 277.085938 255.761719 277.152344 L 255.761719 409 L 411.5 409 Z M 314.953125 303.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(91.764706%,50.980392%,2.352941%);fill-opacity:1;" />
                            <path d="M 132.332031 94.332031 L 132.332031 222.894531 L 197.046875 293.835938 C 212.871094 311.179688 234.4375 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 132.332031 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(90.196078%,90.196078%,90.196078%);fill-opacity:1;" />
                            <path d="M 255.761719 94.332031 L 255.761719 319.847656 C 255.839844 319.847656 255.921875 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 255.761719 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.313725%,84.313725%,84.313725%);fill-opacity:1;" />
                            <path d="M 159 124.332031 L 231 124.332031 L 231 211 L 159 211 Z M 159 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 247 124.332031 L 355.667969 124.332031 L 355.667969 139 L 247 139 Z M 247 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 124.332031 L 355.667969 124.332031 L 355.667969 139 L 255.761719 139 Z M 255.761719 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                            <path d="M 247 160.332031 L 355.667969 160.332031 L 355.667969 175 L 247 175 Z M 247 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 160.332031 L 355.667969 160.332031 L 355.667969 175 L 255.761719 175 Z M 255.761719 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                            <path d="M 247 196.332031 L 355.667969 196.332031 L 355.667969 211 L 247 211 Z M 247 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 196.332031 L 355.667969 196.332031 L 355.667969 211 L 255.761719 211 Z M 255.761719 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                            <path d="M 159 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 159 246.40625 Z M 159 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 255.761719 246.40625 Z M 255.761719 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                        </g>
                    </svg>
                </a>
            </div>
        </div>
        <div class="series-share-with-list flex-row vertical-center horizon-center margin-between">
            <div class="flex-col fix-col"><div class="share-with-text">Share with</div></div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-facebook">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                <g>
                                    <circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
                                    <path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                        c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                </g>
                                </svg>
                </a>
            </div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-twitter">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
                                                c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
                                                c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
                                                c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
                                                c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
                                                c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
                                                c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
                                                C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                                        </g>
                                    </g>
                                </svg>
                </a>
            </div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-linkedin">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
                                                c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
                                                c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
                                                C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
                                                c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
                                                 M27.865,83.739H41.27V43.409H27.865V83.739z"/>
                                        </g>
                                    </g>
                                </svg>
                </a>
            </div>
            <div class="flex-col fix-col">
                <a target="_blank" href="javascript:;" class="share-item item-email">
                    <svg xmlns="http://www.w3.org/2000/svg" height="512pt" version="1.1" viewBox="0 0 512 512" width="512pt">
                        <g id="surface1">
                            <path d="M 512 256 C 512 397.386719 397.386719 512 256 512 C 114.613281 512 0 397.386719 0 256 C 0 114.613281 114.613281 0 256 0 C 397.386719 0 512 114.613281 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294118%,32.941176%,87.843137%);fill-opacity:1;" />
                            <path d="M 512 256 C 512 245.425781 511.347656 235.003906 510.101562 224.765625 L 379.667969 94.332031 L 100.5 409 L 196.542969 505.042969 C 215.625 509.582031 235.527344 512 256 512 C 397.386719 512 512 397.386719 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(22.745098%,25.490196%,80%);fill-opacity:1;" />
                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 100.5 409 L 411.5 409 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(98.039216%,36.470588%,5.882353%);fill-opacity:1;" />
                            <path d="M 411.5 188 L 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 409 L 411.5 409 Z M 411.5 188 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.705882%,29.803922%,5.882353%);fill-opacity:1;" />
                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 197.046875 293.835938 C 228.695312 328.527344 283.304688 328.527344 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;" />
                            <path d="M 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 319.847656 C 277.40625 319.910156 299.070312 311.246094 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(92.156863%,92.156863%,94.117647%);fill-opacity:1;" />
                            <path d="M 100.5 409 L 197.046875 303.164062 C 228.695312 268.472656 283.304688 268.472656 314.953125 303.164062 L 411.5 409 Z M 100.5 409 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,58.431373%,0%);fill-opacity:1;" />
                            <path d="M 314.953125 303.164062 C 299.070312 285.753906 277.40625 277.085938 255.761719 277.152344 L 255.761719 409 L 411.5 409 Z M 314.953125 303.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(91.764706%,50.980392%,2.352941%);fill-opacity:1;" />
                            <path d="M 132.332031 94.332031 L 132.332031 222.894531 L 197.046875 293.835938 C 212.871094 311.179688 234.4375 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 132.332031 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(90.196078%,90.196078%,90.196078%);fill-opacity:1;" />
                            <path d="M 255.761719 94.332031 L 255.761719 319.847656 C 255.839844 319.847656 255.921875 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 255.761719 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.313725%,84.313725%,84.313725%);fill-opacity:1;" />
                            <path d="M 159 124.332031 L 231 124.332031 L 231 211 L 159 211 Z M 159 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 247 124.332031 L 355.667969 124.332031 L 355.667969 139 L 247 139 Z M 247 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 124.332031 L 355.667969 124.332031 L 355.667969 139 L 255.761719 139 Z M 255.761719 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                            <path d="M 247 160.332031 L 355.667969 160.332031 L 355.667969 175 L 247 175 Z M 247 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 160.332031 L 355.667969 160.332031 L 355.667969 175 L 255.761719 175 Z M 255.761719 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                            <path d="M 247 196.332031 L 355.667969 196.332031 L 355.667969 211 L 247 211 Z M 247 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 196.332031 L 355.667969 196.332031 L 355.667969 211 L 255.761719 211 Z M 255.761719 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                            <path d="M 159 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 159 246.40625 Z M 159 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                            <path d="M 255.761719 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 255.761719 246.40625 Z M 255.761719 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <footer class="listing-post-footer d-mode-none d-mode-share-block">
        <button class="site-green-btn share-next-btn">Next >></button>
        <div class="share-back-wrap">
            <button class="site-green-btn share-back-btn"><< Back</button>
        </div>
    </footer>
    <div class="listing-drag-drop-pane flex-centering">
        <div class="drag-info">Drag Drop Youtube, Site URL,<br>Document, Video, Audio, Image File</div>
    </div>
</section>
