<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.01.2019
 * Time: 01:56
 */
?>

<div class="series-structure-tree-component component jstree jstree-default jstree-default-large">
    <li class="sample post-row jstree-node jstree-closed" hidden>
        <div class="post-row-inner">
            <div class="flex-row vertical-center space-between margin-between">
                <div class="flex-col fix-col"><i class="jstree-icon jstree-ocl" role="presentation"></i></div>
                <div class="flex-col fix-col">
                    <div class="drag-icon-wrapper">
                        <img class="drag-icon type-root" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/series/start-(3).png">
                        <img class="drag-icon type-post" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/post/blog-(4).png">
                        <img class="drag-icon type-menu" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/menu/path-selection-(4).png">
                        <img class="drag-icon type-path" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/option/next-(4).png">
                    </div>
                </div>
                <div class="flex-col fix-col d-type-none d-post-block ml-2 mr-0">
                    <div class="media-icon-wrapper">
                        <i class="fa fa-youtube-play video-icon"></i>
                        <i class="fa fa-volume-up audio-icon"></i>
                        <i class="fa fa-file-text text-icon"></i>
                    </div>
                </div>
                <div class="flex-col min-width-0">
                    <div class="post-title"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="actions">
                        <div class="flex-row margin-between vertical-center">
                            <div class="flex-col publish-col display-mode-none display-mode-default-block">
                                <a class="publish-post">
                                    <img class="active-icon" src="assets/images/global-icons/check-active.png">
                                    <img class="inactive-icon" src="assets/images/global-icons/check-inactive.png">
                                </a>
                            </div>
                            <div class="flex-col charge-col display-mode-none display-mode-default-block">
                                <div class="flex-row vertical-center margin-between horizon-center post-charge-wrapper">
                                    <div class="flex-col fix-col">
                                        <span class="charge-label">Free? </span>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <label class="c-switch">
                                            <input type="checkbox" name="boolPost_free" value="1">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col view-col display-mode-none display-mode-default-block">
                                <a class="view-post" href="javascript:;" target="_blank">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/open-book.svg">
                                </a>
                            </div>
                            <div class="flex-col edit-col display-mode-none display-mode-default-block">
                                <a class="edit-post">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/edit-series.svg">
                                </a>
                            </div>
                            <div class="flex-col save-col display-mode-none">
                                <a class="save-post">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/diskette-save-interface-symbol.svg">
                                </a>
                            </div>
                            <div class="flex-col display-mode-none display-mode-default-block">
                                <a class="clone-post">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/clone.png">
                                </a>
                            </div>
                            <div class="flex-col del-col display-mode-none display-mode-default-block">
                                <a class="delete-post">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cancel.svg">
                                </a>
                            </div>
                            <div class="flex-col display-mode-none display-mode-edit-block">
                                <a class="item-action edit-action flex-centering" href="javascript:;" tabindex="0">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                </a>
                            </div>
                            <div class="flex-col display-mode-none display-mode-edit-block">
                                <a class="item-action delete-action" href="javascript:;" tabindex="0">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                    286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"></polygon>
                                                <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                    C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                    S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col display-mode-none display-mode-share-block">
                                <a href="javascript:;" class="action-item share-action">
                                    <svg version="1.1" id="Layer_1"
                                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                                         viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title></title>
                                        <desc>Created with Sketch.</desc>
                                        <g id="Page-1" sketch:type="MSPage">
                                            <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                            </g>
                                        </g>
                        </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <ul class="posts-list">
    </ul>
    <?php require ASSETS_PATH . '/components/PostEditor/PostEditor.php';?>
</div>