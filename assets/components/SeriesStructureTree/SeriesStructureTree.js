
var SeriesStructureTreeClass;

(function () {
    SeriesStructureTreeClass = function (domRoot, series) {
        var domPostsList;
        var seriesRow;
        var virtualRootData;
        var virtualRootHandle, postEditorHandle;
        var self = this;
        var mode = 'default';
        var clickCallbacks = [];

        var bindEvents = function () {
            domPostsList.on('sortupdate', function (e, helper) {
                var currentHdl = helper.item.data('controlHandle');
                var parentHdl = helper.item.parents('li').data('controlHandle');
                parentHdl = parentHdl ? parentHdl : virtualRootHandle;
                currentHdl.updateParent(parentHdl);
                if (mode == 'add_mode') {
                    parentHdl.relativeOrderChilds();
                }
                else {
                    parentHdl.reOrderChilds();
                }
            });
            domPostsList.on('sortexpand', function (e, helper) {
                var domT = domPostsList.find('li.mjs-nestedSortable-hovering');
                var hdl = domT.data('controlHandle');
                hdl.expandChild();
            });
        }

        var PostRowClass = function (postData) {

            var domRow, domChildList, domInnerWrp;
            var self = this, parentHandle = false, gridItemHandle;

            var isChildsLoaded = false;

            var childHandles = [];

            var metaData = {};

            var bindEvents = function () {
                domInnerWrp.find('.post-title').click(function () {
                    clickCallbacks.forEach(function (value) {
                        value(postData);
                    });
                });
                domInnerWrp.find('.item-action.delete-action').click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete){
                            self.deletePost();
                        }
                    });
                });
                domInnerWrp.find('.delete-post img').click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete){
                            self.deletePost();
                        }
                    });
                });
                domInnerWrp.find('.clone-post img').click(function () {
                    self.clonePost();
                });
                domInnerWrp.find('.edit-post').click(function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'get', where: postData.id}).then(function (res) {
                        postEditorHandle.setData(res.data);
                        postEditorHandle.open();
                        postEditorHandle.afterSave(function (sets) {
                            self.sets(helperHandle.postToViewFormat(sets));
                        });
                    });
                });
                domInnerWrp.find('.item-action.edit-action').click(function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'get', where: postData.id}).then(function (res) {
                        postEditorHandle.setData(res.data);
                        postEditorHandle.open();
                        postEditorHandle.afterSave(function (sets) {
                            self.sets(helperHandle.postToViewFormat(sets));
                        });
                    });
                });
                domInnerWrp.find('.publish-post .inactive-icon').click(function () {
                    var sets = {
                        status: 'publish',
                    };
                    self.update(sets).then(function () {
                    });
                });
                domInnerWrp.find('.publish-post .active-icon').click(function () {
                    var sets = {
                        status: 'draft',
                    };
                    self.update(sets).then(function () {
                    });
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_delete', where: {intClientSubscription_post_ID: self.getId()}});
                });
                domInnerWrp.find('.save-post').click(function () {
                    var sets = {
                        title: domInnerWrp.find('.post-title').html(),
                    };
                    self.update(sets).then(function () {

                    });
                });
                domInnerWrp.find('.jstree-icon.jstree-ocl').click(function () {
                    if (domRow.hasClass('jstree-open') && childHandles.length && self.nodeType() !== 'post') {
                        self.collapseChild();
                    }
                    else {
                        self.expandChild();
                    }
                });
                domInnerWrp.find('.post-title').keypress(function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        e.preventDefault();
                        domInnerWrp.find('.save-post').click();
                    }
                });
                domInnerWrp.find('[name="boolPost_free"]').change(function () {
                    var v = $(this).prop('checked') ? 1 : 0;
                    self.update({free: v});
                });
                domInnerWrp.find('.share-action').click(function () {
                    $(this).toggleClass('shared');
                });
            }
            var loadChilds = function () {
                childHandles = [];
                domRow.addClass('jstree-loading');
                domChildList.empty();

                var resPromise, ajaxData;
                ajaxData = {
                    action: 'get_items',
                    where: {intPost_series_ID: series, intPost_parent: postData.id},
                    select: helperHandle.array_keys(helperHandle.postFormat(postData)),
                };
                resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData, false);
                return resPromise.then(function (res) {

                    var childPosts = res.data;
                    var promises = [];

                    childPosts.forEach(function (childPost) {
                        var fData = helperHandle.postToViewFormat(childPost);
                        promises.push(self.addChild(fData));
                    });
                    isChildsLoaded = true;
                    domRow.removeClass('jstree-loading');
                    domRow.removeClass('jstree-closed').addClass('jstree-open');

                    if (!childHandles.length) {
                        domRow.addClass('jstree-leaf');
                    }
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve(true);
                        }, 200);
                    });
                });
            }

            var bindData = function () {
                if (self.getId()) {
                    domRow.attr('id', 'sortable-tree-item-' + self.getId());
                }
                domInnerWrp.removeClass('content-type-0 content-type-2 content-type-7 content-type-8 content-type-9 content-type-10 content-type-11');
                domInnerWrp.addClass('content-type-' + postData.type);
                domInnerWrp.find('.post-title').html(postData.title);
                domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-' + postData.nodeType);
                domRow.removeClass('jstree-leaf');
                domRow.removeClass('mjs-nestedSortable-leaf');
                domRow.removeClass('mjs-nestedSortable-branch');
                domRow.removeClass('node-status-publish node-status-draft');

                if (postData.nodeType == 'post') {
                    isChildsLoaded = true;
                    domRow.addClass('jstree-leaf');
                    domRow.addClass('mjs-nestedSortable-leaf');
                    self.expandChild();
                    domInnerWrp.find('.drag-icon-wrapper .drag-icon.type-post').attr('src', postData.image);
                }
                else if (isChildsLoaded && !childHandles.length) {
                    domRow.addClass('jstree-leaf');
                    domRow.addClass('mjs-nestedSortable-branch');
                }
                else {
                    domRow.addClass('mjs-nestedSortable-branch');
                }
                if (postData.status) {
                    domRow.addClass('node-status-' + postData.status);
                }
                domInnerWrp.find('[name="boolPost_free"]').prop('checked', parseInt(postData.free));
                domInnerWrp.find('.view-post').attr('href', 'view_blog?id=' + postData.id);
                if (self.nodeType() == 'post') {
                    if (domChildList) {
                        domChildList.remove();
                        domChildList = false;
                    }
                }
                else {
                    if (!domChildList) {
                        domRow.append('<ul class="collapse posts-list" style="height: 0px;"></ul>');
                        domChildList = domRow.find('> ul');
                    }
                }
            }

            this.getPath = function () {
                if (!parentHandle) {
                    return [self.getId()];
                }
                var parentPath = parentHandle.getPath();
                return parentPath.push(self.getId());
            }

            this.expandChild = function () {
                if (!isChildsLoaded) {
                    return loadChilds().then(function (res) {
                        return res.then(function () {
                            domRow.removeClass('jstree-closed').addClass('jstree-open');
                            domRow.find('> ul').collapse('show');
                            return true;
                        });
                    });
                }
                else {
                    domRow.removeClass('jstree-closed').addClass('jstree-open');
                    domRow.find('> ul').collapse('show');
                    return Promise.resolve(true);
                }
            }
            this.emptyExpand = function () {
                isChildsLoaded = true;
                domRow.removeClass('jstree-closed').addClass('jstree-open');
                domRow.find('> ul').collapse('show');
            }
            this.collapseChild = function () {
                domRow.removeClass('jstree-open');
                domRow.find('> ul').collapse('hide');
                domRow.find('> ul').off('hidden.bs.collapse').one('hidden.bs.collapse', function (e) {
                    domRow.addClass('jstree-closed');
                });
            }

            this.childLoaded = function () {
                return isChildsLoaded;
            }
            this.reOrderChilds = function () {
                var updateSets = [];
                childHandles.forEach(function (childHdl) {
                    var newOrder = childHdl.getDomPosition() + 1;
                    var sets = helperHandle.postFormat({order: newOrder});
                    updateSets.push({where: childHdl.getId(), sets: sets});
                    childHdl.setOrder(newOrder);
                });

                ajaxAPiHandle.apiPost('Posts.php', {action: 'update_posts', updateSets: updateSets});
            }
            this.relativeOrderChilds = function () {
                var itemsArranged = [];
                var orders = [];

                domChildList.find('>.post-row').each(function () {
                    var hdl = $(this).data('controlHandle');
                    orders.push(hdl.data().order);
                    itemsArranged.push(hdl);
                });
                orders.sort(function (a, b) {
                    return a - b;
                });
                var updates = [];
                itemsArranged.forEach(function (value, i) {
                    updates.push({where: value.data().id, sets: {intPost_order: orders[i]}});
                    value.sets({order: orders[i]});
                });
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'update_many', updates: updates}).then(function (res) {
                    return res;
                });
            }
            this.sets = function (sets) {
                Object.assign(postData, sets);
                bindData();
            }
            this.spliceChild = function (id) {
                childHandles.forEach(function (childHandle, i) {
                    if (childHandle.getId() === id) {
                        childHandles.splice(i, 1);
                    }
                });
                if (isChildsLoaded && !childHandles.length) {
                    domRow.removeClass('jstree-open').addClass('jstree-leaf');
                }
            }
            this.addChildHandle = function (childHandle) {
                childHandles.push(childHandle);
                domRow.removeClass('jstree-leaf');
            }
            this.addChild = function (childPost) {
                var hdl = new PostRowClass(childPost);
                hdl.init();
                hdl.setParent(self);
                hdl.appendTo(domChildList);
                return hdl;
            }
            this.setParent = function (parentHdl) {
                parentHandle = parentHdl;
                parentHandle.addChildHandle(self);
            }
            this.updateParent = function (parentHdl) {
                parentHandle.spliceChild(self.getId());
                var parentType = parentHdl ? parentHdl.nodeType() : 'path';
                var myNodeType = self.nodeType(), newNodeType = myNodeType;

                parentHandle = parentHdl;
                parentHandle.addChildHandle(self);
                self.makeTypeValid();

                var sets = {};
                sets.parent = parentHdl.getId();

                sets.nodeType = generateValidNodeType(parentType, myNodeType);

                return self.update(sets).then(function (res) {
                    return res;
                });
            }
            this.nodeType = function () {
                return postData.nodeType;
            }
            this.makeTypeValid = function () {
                var parentType = parentHandle ? parentHandle.nodeType() : 'path';
                var myNodeType = self.nodeType(), newNodeType = myNodeType;
                postData.strPost_nodeType = generateValidNodeType(parentType, myNodeType);
                bindData();
                childHandles.forEach(function (childHandle) {
                    childHandle.makeTypeValid();
                });
            }
            this.update = function (sets) {

                var resPromise;

                resPromise = ajaxAPiHandle.apiPost('Posts.php', {action: 'update', sets: helperHandle.postFormat(sets), where: self.getId()});
                return resPromise.then(function (res) {
                    $.extend(postData, sets);
                    if (sets.body) {
                        metaData.body = sets.body;
                    }
                    bindData();
                    return res;
                });
            }
            this.clonePost = function () {
                var resPromiss;
                resPromiss = ajaxAPiHandle.apiPost('Posts.php', {action: 'after_clone', where: self.getId()});
                return resPromiss.then(function (res) {
                    if (res.status){
                        var newHandle = new PostRowClass(helperHandle.postToViewFormat(res.data));
                        newHandle.init();
                        newHandle.setParent(parentHandle);
                        newHandle.appearAfter(domRow);
                        return res;
                    }
                })
            }
            this.setOrder = function (newOrder) {
                postData.order = newOrder;
            }
            this.getOrder = function () {
                return postData.order;
            }
            this.getDomPosition = function () {
                return domRow.index();
            }
            this.getId = function () {
                return postData.id;
            }
            this.deletePost = function () {
                var resPromise = ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: self.getId(), meta: series});

                resPromise.then(function () {
                    self.disappear().then(function () {
                        domRow.remove();
                        parentHandle.spliceChild(self.getId());
                    });
                });
            }
            this.removeDom = function () {
                domRow.remove();
            }
            this.remove = function () {
                self.isRemoved = true;
                domRow.remove();
                parentHandle.refreshChilds();
            }
            this.focus = function () {
                domRow.addClass('focus');
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        domRow.removeClass('focus');
                        resolve();
                    }, 700);
                });
            }
            this.appearAfter = function (domAfter) {
                var newDom;
                if (domAfter){
                    setTimeout(function () {
                        domRow.addClass('appear');
                        newDom = domRow.insertAfter(domAfter);
                    }, 200);
                }
                else {
                    setTimeout(function () {
                        domRow.addClass('appear');
                        newDom = domRow.appendTo(domPostsList);
                    }, 200)
                }
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        domRow.removeClass('appear');
                        resolve();
                    }, 700);
                });
            }
            this.getDom = function () {
                return domRow;
            }
            this.dom = function () {
                return domRow;
            }
            this.domInnerWrap = function () {
                return domInnerWrp;
            }
            this.appendTo = function (domList) {
                if (!domList) {
                    if (parentHandle) {
                        domList = parentHandle.getDom().find('> ul');
                    }
                    else {
                        domList = domPostsList;
                    }

                }
                setTimeout(function () {
                    domRow.addClass('appear');
                    domRow.appendTo(domList);
                }, 200);
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        domRow.removeClass('appear');
                        resolve();
                    }, 700);
                });
            }
            this.disappear = function () {
                setTimeout(function () {
                    domRow.addClass('disappear');
                }, 200)
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        domRow.removeClass('disappear');
                        domRow.detach();
                        resolve();
                    }, 700)
                });
            }
            this.data = function () {
                return postData;
            }
            this.getChilds = function () {
                return childHandles;
            }
            this.isAdded = function () {
                return postData.id ? true : false;
            }
            this.refreshChilds = function () {
                childHandles.forEach(function (value, i) {
                    if (value.isRemoved) {
                        childHandles.splice(i, 1);
                    }
                })
            }
            this.setGridHandle = function (hdl) {
                gridItemHandle = hdl;
            }
            this.isRemoved = false;
            this.getSharedHandles = function () {
                var sharedHandles = [];
                if ( domInnerWrp.find('.share-action').hasClass('shared') ) { sharedHandles.push(self) }
                childHandles.forEach(function (hdl) {
                    sharedHandles = sharedHandles.concat( hdl.getSharedHandles() );
                });
                return sharedHandles;
            }
            this.cleanShared = function () {
                domInnerWrp.find('.share-action').removeClass('shared');
                childHandles.forEach(function (hdl) {
                    hdl.cleanShared();
                });
            }
            this.getHandle = function (id) {
                if (postData.id == id) {
                    return self;
                }
                for (var i = 0; i < childHandles.length; i++) {
                    if (childHandles[i].getHandle(id)) {
                        return childHandles[i].getHandle(id);
                    }
                }
                return false;
            }
            this.init = function () {
                domRow = domRoot.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                domInnerWrp = domRow.find('> .post-row-inner');
                bindData();
                if (self.getId()) {
                    domRow.attr('id', 'sortable-tree-item-' + self.getId());
                }
                bindEvents();
                domRow.data('controlHandle', self);
            }
        }
        var generateValidNodeType = function(parentNodeType, myNodeType) {
            var newNodeType = myNodeType;

            switch (parentNodeType) {
                case 'post':
                    break;
                case 'menu':
                    newNodeType = 'path';
                    break;
                case 'path':
                    if (myNodeType === 'path') {
                        newNodeType = 'menu';
                    }
                    break;
            }
            return newNodeType;
        }
        this.makeSortable = function () {

            var isAllowed = function(placeholder, placeholderParent, currentItem) {
                var currentHandle = currentItem.data('controlHandle');
                var parentHandle = placeholderParent ? placeholderParent.data('controlHandle') : false;

                switch (currentHandle.nodeType()) {
                    case 'post':
                        if (parentHandle) {
                            return parentHandle.nodeType() !== 'post';
                        }
                        else {
                            return true;
                        }
                        break;
                    case 'menu':
                        if (parentHandle) {
                            return parentHandle.nodeType() === 'path' || parentHandle.nodeType() === 'root';
                        }
                        else {
                            return true;
                        }
                        break;
                    case 'path':
                        if (parentHandle) {
                            return parentHandle.nodeType() === 'menu';
                        }
                        else {
                            return false;
                        }
                        break;
                }
                return true;
            }
            var options = {
                appendTo: domRoot.find('> ul'),
                placeholder: 'ui-state-highlight',
                // containment: '.page.site-wrapper',
                items: 'li',
                protectRoot: true,
                forcePlaceholderSize: true,
                handle: '.drag-icon',
                helper:	'clone',
                opacity: .6,
                revert: 0,
                tabSize: 25,
                tolerance: 'pointer',
                toleranceElement: '> .post-row-inner',
                maxLevels: 0,
                isTree: true,
                expandOnHover: 1000,
                doNotClear: true,
                startCollapsed: true,
                listType: 'ul',
                expandedClass: 'jstree-open',
                collapsedClass: 'jstree-closed',
                isAllowed: isAllowed,
            };
            domPostsList.nestedSortable(options);
        }
        this.sortableRefresh = function () {
            domPostsList.sortable( "refreshPositions" );
        }
        this.setMode = function (m) {
            domRoot.removeClass('mode-' + mode);
            mode = m;
            domRoot.addClass('mode-' + mode);
        }
        this.emptyExpand = function () {
            virtualRootHandle.emptyExpand();
        }
        this.expand = function () {
            return virtualRootHandle.expandChild();
        }
        this.add = function (itemData) {
            return virtualRootHandle.addChild(itemData);
        }
        this.getItems = function () {
            return virtualRootHandle.getChilds();
        }
        this.save = function () {
            var items = virtualRootHandle.getChilds();
            var nItems = [];
            var fItems = [];
            items.forEach(function (value) {
                if (!value.isAdded()) {
                    nItems.push(value);
                    var f = helperHandle.postFormat(value.data());
                    f.intPost_series_ID = series;
                    fItems.push(f);
                }
            });
            if (fItems.length) {
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: fItems}).then(function (res) {
                    for (var i = 0; i < nItems.length; i++) {
                        nItems[i].sets({id: res.data[i].post_ID, order: res.data[i].intPost_order, image: res.data[i].strPost_featuredimage});
                    }
                    return res;
                });
            }
            else {
                return Promise.resolve(false);
            }
        }
        this.onClick = function (fn) {
            clickCallbacks.push(fn);
        }
        this.getSharedHandles = function () {
            return virtualRootHandle.getSharedHandles();
        }
        this.cleanShared = function () {
            virtualRootHandle.cleanShared();
        }
        this.getHandle = function (id) {
            return virtualRootHandle.getHandle(id);
        }
        this.init = function () {
            domRoot.addClass('mode-' + mode);
            if (mode == 'static') {
                domRoot.find('li.post-row.sample').find('.actions').parent().remove();
            }
            ajaxAPiHandle.apiPost('Series.php', {action: 'get', where: series}, false).then(function (res) {
                seriesRow = res.data;
                if (parseInt(seriesRow.boolSeries_charge)) {
                    domRoot.addClass('charged');
                }
            });
            domPostsList = domRoot.find('.posts-list');
            virtualRootData = {
                id: 0,
                title: '',
                image: '',
                type: 7,
                order: 1,
                nodeType: 'root',
                parent: -1,
                status: 'publish',
                free: 0,
                origin: {
                    intClientSubscription_post_ID: 0,
                }
            };

            virtualRootHandle = new PostRowClass(virtualRootData);
            virtualRootHandle.init();
            virtualRootHandle.appendTo();
            if (mode != 'static') {
                self.makeSortable();
                postEditorHandle = new PostEditorClass(domRoot.find('.post-editor.component').appendTo('body'));
                postEditorHandle.init();
            }
            bindEvents();
        }
    }
})();