var SkypeLoaderClass;

(function () {
    'use strict';

    SkypeLoaderClass = function (domRoot) {
        var callbacksAfterHide = [];
        var flgHide = false, shown = true;
        var hideInterval;

        this.show = function () {
            domRoot.show();
            shown = true;
            clearInterval(hideInterval);
            hideInterval = setInterval(function () {
                if (flgHide) {
                    flgHide = false;
                    clearInterval(hideInterval);
                    domRoot.hide();
                    shown = false;
                    callbacksAfterHide.forEach(function (value) {
                        value();
                    });
                    callbacksAfterHide = [];
                }
            }, 1500);
        }
        this.hide = function () {
            flgHide = true;
        }
        this.directHide = function () {
            clearInterval(hideInterval);
            domRoot.hide();
            callbacksAfterHide = [];
            shown = false;
        }
        this.afterHide = function (fn) {
            if (shown) {
                callbacksAfterHide.push(fn);
            }
            else {
                fn();
            }
        }
        this.init = function () {

        }
    }
})();