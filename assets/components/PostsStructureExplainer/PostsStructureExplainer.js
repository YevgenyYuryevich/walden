var PostsStructureExplainerClass;

(function () {
    'use strict';

    PostsStructureExplainerClass = function (domRoot) {
        var domExplainsContainer;
        var journeyHandle, forkHandle, skipHandle, formHandle;

        var ExplainerClass = function () {
            var domExplainer, domItemsWrapper;
            var domNodeItems = [];

            var self = this;

            var getNodeCenterPos = function (domNode) {
                var x = parseInt(domNode.css('left')) + parseInt(domNode.css('width')) / 2;
                var y = parseInt(domNode.css('top')) + parseInt(domNode.css('height')) / 2;
                return {
                    x: x,
                    y: y
                }
            }

            this.insertNodeItem = function (pos, option) {
                var domNode = domItemsWrapper.find('.node-item.sample').clone().removeClass('sample').removeAttr('hidden');
                domNode.addClass('node-number-' + domNodeItems.length);
                option.type = option.type ? option.type : 'post';
                switch (option.type) {
                    case 'post':
                        domNode.addClass('type-post');
                        domNode.css('top', 'calc(' + pos.y + ' - 15px)');
                        domNode.css('left', 'calc(' + pos.x + ' - 15px)');
                        break;
                    case 'form':
                        domNode.addClass('type-form');
                        domNode.css('top', 'calc(' + pos.y + ' - 25px)');
                        domNode.css('left', 'calc(' + pos.x + ' - 25px)');
                        break;
                    default:
                        domNode.css('top', 'calc(' + pos.y + ' - 15px)');
                        domNode.css('left', 'calc(' + pos.x + ' - 15px)');
                        break;
                }

                if (option.day) {
                    domNode.find('.day-value').html(option.day);
                }
                else {
                    domNode.find('.node-point-txt').remove();
                }
                domNode.appendTo(domItemsWrapper);
                domNodeItems.push(domNode);
            }
            this.connectNode = function (n1, n2) {
                var pos0 = getNodeCenterPos(domNodeItems[n1]);
                var pos1 = getNodeCenterPos(domNodeItems[n2]);
                self.insertLine(pos0, pos1, {className: 'line-' + n1 + '-' + n2});
            }
            this.skipNode = function (n1, n2) {
                var pos0 = getNodeCenterPos(domNodeItems[n1]);
                var pos1 = getNodeCenterPos(domNodeItems[n2]);
                pos0.x += 15;
                pos0.y += 15;

                pos1.x -= 15;
                pos1.y += 15;
                self.insertSkip(pos0, pos1);
            }
            this.insertLine = function (pos0, pos1, option) {
                var x = pos0.x, y = pos0.y, x1 = pos1.x, y1 = pos1.y;
                var l = domItemsWrapper.find('.line.sample').clone().removeClass('sample').removeAttr('hidden').addClass(option.className);
                l.css({
                    top: y,
                    left: x,
                    width: Math.sqrt((x1-x)*(x1-x) + (y1 - y)*(y1 - y)),
                    transform: 'rotate('+Math.atan2((y1-y),(x1-x))+'rad)'
                });
                domItemsWrapper.append(l);
            }
            this.insertSkip = function (pos0, pos1) {
                var x = pos0.x, y = pos0.y, x1 = pos1.x, y1 = pos1.y;
                var w = Math.sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
                var r = w * 0.7;
                var oh = Math.sqrt(r * r - (w / 2) * (w / 2));
                var h = r - oh;
                var domSkip = domItemsWrapper.find('.skip-bow.sample').clone().removeAttr('hidden').removeClass('sample');
                var domBow = domSkip.find('.bow-circle');
                domSkip.css('left', x + 'px');
                domSkip.css('top', y + 'px');
                domSkip.css('width', w + 'px');
                domSkip.css('height', h + 'px');
                domSkip.css({transform: 'rotate(' + Math.atan2((y1-y), (x1-x)) + 'rad)'});

                domBow.css('width', 2 * r + 'px');
                domBow.css('height', 2 * r + 'px');
                domBow.css('left', 'calc((' + (w - 2 * r) + 'px) / 2)');
                domSkip.appendTo(domItemsWrapper);
            }
            this.dom = function () {
                return domExplainer;
            }
            this.setExlainTxt = function (h) {
                domExplainer.find('.node-type-explain-txt').html(h);
            }
            this.init = function () {
                domExplainer = domExplainsContainer.find('.explain-type-structure.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domExplainsContainer);
                domItemsWrapper = domExplainer.find('.node-type-items-wrapper');
            }
        }

        this.init = function () {
            domExplainsContainer = domRoot.find('.explains-container');
            journeyHandle = new ExplainerClass();
            journeyHandle.init();
            journeyHandle.dom().addClass('type-journey');
            journeyHandle.insertNodeItem({x: '0%', y: '50%'}, {day: 1});
            journeyHandle.insertNodeItem({x: '50%', y: '50%'}, {day: 2});
            journeyHandle.insertNodeItem({x: '100%', y: '50%'}, {day: 3});
            journeyHandle.connectNode(0, 1);
            journeyHandle.connectNode(1, 2);

            forkHandle = new ExplainerClass();
            forkHandle.init();
            forkHandle.dom().addClass('type-fork');
            forkHandle.insertNodeItem({x: '0%', y: '50%'}, {day: 3});
            forkHandle.insertNodeItem({x: '50%', y: '0%'}, {day: 4});
            forkHandle.insertNodeItem({x: '50%', y: '50%'}, {});
            forkHandle.insertNodeItem({x: '50%', y: '100%'}, {});
            forkHandle.insertNodeItem({x: '100%', y: '50%'}, {day: 5});
            forkHandle.connectNode(0, 1);
            forkHandle.connectNode(0, 2);
            forkHandle.connectNode(0, 3);
            forkHandle.connectNode(1, 4);
            forkHandle.connectNode(2, 4);
            forkHandle.connectNode(3, 4);
            forkHandle.setExlainTxt('Add forks in the road<br> to let your user<br> choose their own<br> adventure');

            skipHandle = new ExplainerClass();
            skipHandle.init();
            skipHandle.dom().addClass('type-skip');
            skipHandle.insertNodeItem({x: '0%', y: '50%'}, {day: 6});
            skipHandle.insertNodeItem({x: '50%', y: '50%'}, {day: 7});
            skipHandle.insertNodeItem({x: '100%', y: '50%'}, {day: 8});
            skipHandle.connectNode(0, 1);
            skipHandle.connectNode(1, 2);
            skipHandle.skipNode(0, 2);
            skipHandle.setExlainTxt('Create skip-points<br> so your user can jump <br>to different sections');

            formHandle = new ExplainerClass();
            formHandle.init();
            formHandle.dom().addClass('type-form');
            formHandle.insertNodeItem({x: '0%', y: '50%'}, {day: 8});
            formHandle.insertNodeItem({x: '50%', y: '50%'}, {day: 9, type: 'form'});
            formHandle.insertNodeItem({x: '100%', y: '50%'}, {day: 10});
            formHandle.connectNode(0, 1);
            formHandle.connectNode(1, 2);
            formHandle.setExlainTxt('Add questions and forms<br> to promote active learning,<br> create checklists, and<br> create documents from<br> the data they entered.');
        }
    }
})();