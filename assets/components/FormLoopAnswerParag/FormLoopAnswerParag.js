var FormLoopAnswerParagClass;

(function () {
    'use strict';
    FormLoopAnswerParagClass = function (domRoot, formLoopData) {
        var bindData = function () {
            domRoot.attr('data-form-loop', formLoopData.formLoop_ID);
            for (var r = 0; r < parseInt(formLoopData.formLoop_rows); r ++) {
                var domParag;
                if (domRoot.find('.loop-answer-parag[data-index="' + r + '"]').length) {
                    domParag = domRoot.find('.loop-answer-parag[data-index="' + r + '"]');
                } else {
                    domParag = domRoot.find('.loop-answer-parag.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot);
                    domParag.attr('data-index', r);
                }
                formLoopData.fields.forEach(function (field) {
                    var domField;
                    if (domParag.find('.loop-field[data-field="' + field.formField_ID + '"]').length) {
                        domField = domParag.find('.loop-field[data-field="' + field.formField_ID + '"]');
                    } else {
                        domField = domParag.find('.loop-field.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domParag);
                        domField.attr('data-field', field.formField_ID);
                    }
                    domField.removeClass('type-text type-checkbox').addClass('type-' + field.strFormField_type);
                    domField.find('.loop-field-name').text(field.strFormField_name);

                    var answer = field.answers.find(function (answer) {
                        return parseInt(answer.intFormFieldAnswer_index) === r;
                    });
                    if (answer) {
                        if (field.strFormField_type === 'text') {
                            domField.find('.type-value.text-value').html(answer.strFormFieldAnswer_answer);
                        } else {
                            domField.find('.type-value.checkbox-value').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                        }
                    } else {
                        if (field.strFormField_type === 'text') {
                            domField.find('.type-value.text-value').html('');
                        } else {
                            domField.find('.type-value.checkbox-value').prop('checked', false);
                        }
                    }
                });
            }
        }
        this.dom = function () {
            return domRoot;
        }
        this.init = function () {
            if (typeof formLoopData === 'object') {
                formLoopData.fields.sort(function (a, b) {
                    return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                });
                bindData();
            }
            else {
                ajaxAPiHandle.apiPost('FormLoop.php', {action: 'get_loop', where: formLoopData}, false).then(function (res) {
                    formLoopData = res.data;
                    if (formLoopData) {
                        formLoopData.fields.sort(function (a, b) {
                            return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                        });
                        bindData();
                    }
                    else {
                        domRoot.remove();
                    }
                });
            }
        }
    }
})();