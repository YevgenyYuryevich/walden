var EditPostModalClass = null;
(function () {
    'use strict';
    EditPostModalClass = function (domModalContent, data) {

        var modalHandle;
        var callbackAfterUpdate = [];
        var EditClass = function (modalRootHandle, domContent) {

            var self = this, formFieldsHandle;

            var series = data.series;
            var domForm, domRoot, domDes;
            var redirectTreeHandle;
            var isSeriesMine = data.isSeriesMine;

            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent',
                free: 'boolPost_free',
            };

            var subscriptionFiels = {
                id: 'clientsubscription_ID',
                title: 'strClientSubscription_title',
                body: 'strClientSubscription_body',
                image: 'strClientSubscription_image',
                type: 'intClientSubscriptions_type',
                order: 'intClientSubscriptions_order',
                nodeType: 'strClientSubscription_nodeType',
                parent: 'intClientSubscriptions_parent'
            };

            var viewFormat = function (data) {
                var fData = {};
                var fk;

                for (var k in data) {
                    if (isSeriesMine) {
                        fk = helperHandle.keyOfVal(postFields, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                    else {
                        fk = helperHandle.keyOfVal(subscriptionFiels, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                }
                fData.origin = data;
                return fData;
            }

            var postFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (postFields[k]) {
                        fData[postFields[k]] = data[k];
                    }
                }
                return fData;
            }

            var subscriptionFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (subscriptionFiels[k]) {
                        fData[subscriptionFiels[k]] = data[k];
                    }
                }
                return fData;
            }

            var originFormat = function (data) {
                if (isSeriesMine) {
                    return postFormat(data);
                }
                return subscriptionFormat(data);
            }

            var bindEvents = function () {
                modalRootHandle.onOpened(function () {
                    if (isSeriesMine) {
                        redirectTreeHandle = new SeriesTreeClass(series.series_ID, domForm.find('.redirect-item .series-tree'));
                        redirectTreeHandle.init();
                        redirectTreeHandle.dom().on('load_node.jstree', function () {
                            redirectTreeHandle.refHandle().disable_node(data.id);
                            redirectTreeHandle.refHandle().disable_node('series_' + series.series_ID);
                        });
                        redirectTreeHandle.onChange(function () {
                            if (parseInt(domForm.find('select[name="type"]').val()) === 9) {
                                var selectedNode = redirectTreeHandle.getSelectedNode();
                                domForm.find('textarea[name="body"]').summernote('code', selectedNode.id);
                            }
                        });
                    }
                });
                modalRootHandle.beforeClose(function () {
                    domForm.find('textarea[name="body"]').summernote('destory');
                });
                domForm.submit(function () {
                    save().then(function (res) {
                        modalRootHandle.cancelClose();
                    });
                    return false;
                });
                domForm.find('.cover-image-wrapper [name="image"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.find('select[name="type"]').change(function () {
                    switch (parseInt($(this).val())) {
                        case 7:
                            domForm.find('.form-field-col').show();
                            domForm.find('.redirect-item').hide();
                            break;
                        case 9:
                            domForm.find('.redirect-item').show();
                            domForm.find('.form-field-col').hide();
                            break;
                        case 10:
                            domForm.find('.redirect-item').hide();
                            domForm.find('.form-field-col').show();
                            break;
                        default:
                            domForm.find('.redirect-item').hide();
                            domForm.find('.form-field-col').hide();
                            break;
                    }
                });
            };
            var getFormData = function () {
                var data = helperHandle.formSerialize(domForm);
                var files = domForm.find('input[name="image"]').prop('files');
                var image = files.length ? files[0] : false;
                return {sets: data, image: image};
            }
            var makeDescriptionValid = function () {
                var domEditor = domForm.find('.note-editor.note-frame .note-editable');
                var tpPreBr = false;
                domEditor.find('div>br, p>br').each(function (i, d) {
                    var p = $(d).parent();
                    if (p.html() === '<br>') {
                        if (tpPreBr) {
                            tpPreBr.remove();
                        }
                        tpPreBr = p;
                    }
                });
                domEditor.find('.form-field-wrapper').each(function (i, d) {
                    if ($(d).find('.form-field-name').length === 0) {
                        $(d).remove();
                    }
                });
            }

            var insertNodeToBody = function (domNode) {
                domDes.summernote('editor.restoreRange');
                domDes.summernote('editor.focus');
                domDes.summernote('insertNode', domNode);
                domDes.summernote('insertText', ' ');
                makeDescriptionValid();
                $(domNode).find('[data-toggle="popover"]').popover();
            }

            var FormFieldsClass = function () {
                var domFormFields, domList;
                var fieldRowHandles = [], createFieldHandle;
                var bindEvents = function () {
                    domFormFields.find('.create-form').click(function () {
                        createFieldHandle.open();
                    })
                }
                var FieldRowClass = function (fieldData) {
                    var domRow;
                    var bindData = function () {
                        domRow.find('.field-name').html(fieldData.strFormField_name);
                    }
                    var bindEvents = function () {
                        domRow.find('.delete-post.action').click(function () {
                            swal({
                                title: "Are you sure?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete) {
                                    deleteRow();
                                }
                            });
                        });
                        domRow.find('.append-item.action').on('click', function () {
                            insertNode();
                        });
                        domRow.find('.append-answered-item.action').click(function () {
                            insertAnsweredNode();
                        });
                    }
                    var deleteRow = function () {
                        ajaxAPiHandle.apiPost('FormFields.php', {action: 'delete', where: fieldData.formField_ID}).then(function () {
                            domRow.remove();
                            fieldRowHandles.forEach(function (hdl, i) {
                                if (hdl.data().formField_ID === fieldData.formField_ID) {
                                    fieldRowHandles.splice(i, 1);
                                    return true;
                                }
                            });
                        });
                    }
                    var insertNode = function () {
                        var domNode = $('<div data-form-field="'+ fieldData.formField_ID +'" class="form-field-wrapper"></div>');
                        var domNodeInner = $('<div class="form-field-inner"></div>').appendTo(domNode);
                        domNodeInner.append('<label contenteditable="false" class="form-field-name">'+ fieldData.strFormField_name +':</label>');
                        switch (fieldData.strFormField_type) {
                            case 'text':
                                domNodeInner.append('<div class="form-field-value" contenteditable="true" placeholder="' + fieldData.strFormField_default + '"></div>');
                                break;
                            case 'checkbox':
                                domNodeInner.append('<div class="form-field-value" contenteditable="false"><input type="checkbox" ' + (parseInt(fieldData.strFormField_default) === 1 ? 'checked' : '') + ' /></div>');
                                break;
                        }
                        domNodeInner.append('<span class="form-field-helper" style="font-style: italic;" data-toggle="popover"  data-trigger="hover" contenteditable="false" data-content="'+ fieldData.strFormField_helper +'">i</span>');
                        insertNodeToBody(domNode.get(0));
                        domDes.on('summernote.change', function(e) {
                            if (domNode.find('.form-field-value').html() == '<br>') {
                                domNode.find('.form-field-value').empty();
                            }
                        });
                    }
                    var insertAnsweredNode = function () {
                        var domNode = domFormFields.find('.sample.answered-form-field-wrapper').find('.answered-form-field').clone();
                        domNode.attr('data-answered-form-field', fieldData.formField_ID);
                        var answer = false;
                        formFieldAnswers.forEach(function (formFieldAnswer) {
                            if (formFieldAnswer.intFormFieldAnswer_field_ID == fieldData.formField_ID) {
                                answer = formFieldAnswer;
                            }
                        });
                        fieldData.answer = answer ? answer.strFormFieldAnswer_answer : false;
                        var hdl = new AnsweredFormFieldClass(domNode, fieldData);
                        hdl.init();
                        insertNodeToBody(domNode.get(0));
                        domDes.on('summernote.change', function(e) {
                            if (domNode.find('.form-field-value').html() == '<br>') {
                                domNode.find('.form-field-value').empty();
                            }
                        });
                    }
                    this.data = function () {
                        return fieldData;
                    }
                    this.init = function () {
                        domRow = domList.find('li.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domList);
                        bindData();
                        bindEvents();
                    }
                }
                var CreateFieldClass = function (modalRootHandle, domContent) {
                    var domForm;
                    var bindEvents = function () {
                        domContent.find('select[name=strFormField_type]').change(function () {
                            switch ($(this).val()) {
                                case 'text':
                                    domContent.find('.text-type').show();
                                    domContent.find('.checkbox-type').hide();
                                    break;
                                case 'checkbox':
                                    domContent.find('.checkbox-type').show();
                                    domContent.find('.text-type').hide();
                                    break;
                            }
                        });
                        domForm.submit(function () {
                            createField().then(function (rowData) {

                                addField(rowData);
                                domForm[0].reset();
                                domContent.find('select[name=strFormField_type]').change();
                            });
                            return false;
                        });
                        domContent.find('.create-btn').click(function () {
                            createField().then(function (rowData) {
                                addField(rowData);
                                domForm[0].reset();
                                domContent.find('select[name=strFormField_type]').change();
                                modalRootHandle.cancelClose();
                            });
                        })
                    }
                    var createField = function () {
                        var sets = helperHandle.formSerialize(domForm);
                        sets.intFormField_series_ID = initialSeries.series_ID;
                        switch (domForm.find('select[name=strFormField_type]').val()) {
                            case 'text':
                                sets.strFormField_default = domForm.find('.text-type input').val();
                                break;
                            case 'checkbox':
                                sets.strFormField_default = domForm.find('.checkbox-type input').prop('checked') ? 1 : 0;
                                break;
                        }
                        return ajaxAPiHandle.apiPost('FormFields.php', {action: 'insert', sets: sets}).then(function (res) {
                            return $.extend(sets, {formField_ID: res.data});
                        });
                    }
                    this.init = function () {
                        domContent.find('#form-field-default').attr('id', 'default-value');
                        domForm = domContent.find('form');
                        bindEvents();
                    }
                }
                var addField = function (fieldData) {
                    var hdl = new FieldRowClass(fieldData);
                    hdl.init();
                    fieldRowHandles.push(hdl);
                }
                this.init = function () {
                    domFormFields = domForm.find('.form-fields');
                    domList = domFormFields.find('.fields-list');
                    initialFormFields.forEach(function (f) {
                        var hdl = new FieldRowClass(f);
                        hdl.init();
                        fieldRowHandles.push(hdl);
                    });
                    createFieldHandle = new SiteLightboxClass({
                        domLightboxMainContent: domForm.find('.modal-create-field').removeAttr('hidden'),
                        mainContentHdlClass: CreateFieldClass,
                        openDuration: 300,
                        closeDuration: 400,
                        lightboxType: 'cssMain',
                        openedCss: {
                            width: '1000px'
                        }
                    });
                    createFieldHandle.init();
                    bindEvents();
                }
            }

            var save = function () {
                var formData = getFormData();
                if (formData.image) {
                    var ajaxData = new FormData();
                    ajaxData.append('prefix', isSeriesMine ? 'post' : 'subscription');
                    ajaxData.append('file', formData.image);
                    return ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData).then(function (res) {
                        formData.sets.image = res.data.url;
                        return self.update(formData.sets);
                    });
                }
                else {
                    return self.update(formData.sets);
                }
            }

            this.update = function (sets) {

                var resPromise;

                if (isSeriesMine) {
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', {action: 'update', sets: originFormat(sets), where: data.id});
                }
                else {
                    resPromise = ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', sets: originFormat(sets), where: data.id});
                }
                return resPromise.then(function (res) {
                    $.extend(data, sets);
                    callbackAfterUpdate.forEach(function (callback) {
                        callback(data);
                    });
                    return res;
                });
            }

            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domRoot).addClass('touched');
                        $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            var bindData = function () {
                switch (data.nodeType) {
                    case 'menu':
                        domRoot.find('.node-type').html('Menu');
                        break;
                    case 'path':
                        domRoot.find('.node-type').html('Option');
                        break;
                    case 'post':
                        domRoot.find('.node-type').html('Post');
                        break;
                }
                domForm.find('[name="title"]').val(data.title);
                domForm.find('[name="body"]').val(data.body);
                domForm.find('[name="type"]').val(data.type);
                domForm.find('[name="keywords"]').val(data.keywords);
                domForm.find('.cover-image-wrapper .img-wrapper img').attr('src', data.image);
                if (isSeriesMine) {
                    if ( parseInt(data.type) === 9 ) {
                        domForm.find('.redirect-item').removeAttr('hidden');
                    }
                    else {
                        domForm.find('.redirect-item').attr('hidden', true);
                    }
                }
                var initialType = domForm.find('select[name="type"]').val();
                switch (parseInt(initialType)) {
                    case 7:
                        domForm.find('.form-field-col').removeAttr('hidden');
                        domForm.find('.redirect-item').hide();
                        break;
                    case 9:
                        domForm.find('.redirect-item').show();
                        domForm.find('.form-field-col').hide();
                        break;
                    case 10:
                        domForm.find('.redirect-item').hide();
                        domForm.find('.form-field-col').removeAttr('hidden');
                        break;
                    default:
                        domForm.find('.redirect-item').hide();
                        domForm.find('.form-field-col').hide();
                        break;
                }
            }

            this.init = function () {
                domRoot = domContent;
                domForm = domRoot.find('form');
                domDes = domForm.find('textarea[name="body"]');
                if (typeof data.isSeriesMine !== 'undefined') {
                    isSeriesMine = data.isSeriesMine;
                }
                else {
                    isSeriesMine = parseInt(series.intSeries_client_ID) === parseInt(CLIENT_ID);
                }
                formFieldsHandle = new FormFieldsClass();
                formFieldsHandle.init();
                bindData();
                $.summernote.dom.emptyPara = "<div><br></div>";
                domForm.find('textarea[name="body"]').summernote({
                    height: 250,
                    tabsize: 2
                });
                $('body').find('.ui-helper-hidden-accessible').remove();
                bindEvents();
            }
        };

        this.afterUpdate = function (callback) {
            callbackAfterUpdate.push(callback);
        }
        this.data = function () {
            return data;
        }
        this.modalHandle = function () {
            return modalHandle;
        }
        this.init = function () {
            modalHandle = new SiteLightboxClass({
                domLightboxMainContent: domModalContent,
                mainContentHdlClass: EditClass,
                openDuration: 300,
                closeDuration: 400,
                lightboxType: 'cssMain',
                openedCss: {
                    width: '1200px'
                }
            });
            modalHandle.init();
        }
    }
})();