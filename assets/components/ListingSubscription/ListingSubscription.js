var ListingSubscriptionClass;

(function () {
    'use strict';
    ListingSubscriptionClass = function (purchasedSeries, domListingPanel) {

        var domListing, domList, domUnreadList, domListingFooter, domSharedPostList, domShareSeries, domSharePost;
        var itemHandles = [], unreadItemHandles = [], self = this, seriesOwnersHandle, sharedItemHandles = [];
        var callbacksAfterLoad = [], callbacksAfterSelect = [];
        var purchasedStructureTreeHandle;
        var currentSelectedHandle;
        var mode = 'default';
        var hasInit = false;
        var bindEvents = function () {
            domListing.find('.section-close').click(function () {
                self.close();
            });
            domListingPanel.click(function (e) {
                if (e.target === this) {
                    self.close();
                }
            });
            domListingFooter.find('.share-next-btn').click(function () {
                var hdls = purchasedStructureTreeHandle.getSharedHandles();
                if (hdls.length === 1) {
                    self.sharePostAction(hdls[0].data());
                } else {
                    sharePostSetsAction();
                }
            });
            domListing.find('.share-series-btn').click(function () {
                self.gotoShareSeries();
            });
            domListing.find('.share-back-btn').click(function () {
                domListing.removeClass('share-step-next').removeClass('share-series').removeClass('share-post');
            });
        }
        var bindData = function () {
            domListing.find('.listing-post-header .series-title').text(purchasedSeries.strSeries_title);
            domListing.find('.top-btn-wrap .embed-btn').attr('href', BASE_URL + '/edit_series?tab=embed&id=' + purchasedSeries.series_ID);

            domShareSeries.find('.item-image').css('background-image', 'url(' + purchasedSeries.strSeries_image + ')');
            domShareSeries.find('.child-count span').text(purchasedSeries.posts_count);
            domShareSeries.find('.item-title').text(purchasedSeries.strSeries_title);
            domShareSeries.find('.preview-btn').attr('href', 'preview_series?id=' + purchasedSeries.series_ID);
            let socialLinks = helperHandle.socialLinks(BASE_URL + '/preview_series?id=' + purchasedSeries.series_ID, purchasedSeries.strSeries_title);
            domListing.find('.series-share-with-list .share-item.item-facebook').attr('href', socialLinks.facebook);
            domListing.find('.series-share-with-list .share-item.item-twitter').attr('href', socialLinks.twitter);
            domListing.find('.series-share-with-list .share-item.item-linkedin').attr('href', socialLinks.linkedin);
            domListing.find('.series-share-with-list .share-item.item-email').attr('href', socialLinks.email);
        }
        let sharePostSetsAction = function () {
            sharedItemHandles.forEach(function (hdl) {
                hdl.dom().remove();
            });
            sharedItemHandles = [];
            var hdls = purchasedStructureTreeHandle.getSharedHandles();
            var posts = [], ids = [];
            hdls.forEach(function (hdl) {
                if ( hdl.data().origin.intClientSubscription_post_ID ) {
                    posts.push( hdl.data() );
                    ids.push( hdl.data().origin.intClientSubscription_post_ID );
                }
            });
            sharePosts(ids).then(function (shareId) {
                if (shareId) {
                    domListing.addClass('share-step-next');
                    purchasedStructureTreeHandle.cleanShared();
                    posts.forEach(function (post) {
                        var hdl = new SharedItemClass(post);
                        hdl.init();
                        sharedItemHandles.push(hdl);
                    });
                }
            });
        }
        this.sharePostAction = function (post) {

            domSharePost.find('.item-image').css('background-image', 'url(' + post.image + ')');
            domSharePost.find('.item-title').text(post.title);
            domSharePost.find('.preview-btn').attr('href', 'view_blog?id=' + post.id + '&from=subscription');

            let socialLinks = helperHandle.socialLinks(BASE_URL + '/view_blog?id=' + post.id + '&from=subscription', post.title);
            domListing.find('.share-with-list .share-item.item-facebook').attr('href', socialLinks.facebook);
            domListing.find('.share-with-list .share-item.item-twitter').attr('href', socialLinks.twitter);
            domListing.find('.share-with-list .share-item.item-linkedin').attr('href', socialLinks.linkedin);
            domListing.find('.share-with-list .share-item.item-email').attr('href', socialLinks.email);

            self.setMode('share');
            domListing.addClass('share-post');
            domListing.addClass('share-step-next');
        };
        var UnReadItemClass = function (itemData) {
            var domItem;
            var self = this;
            var bindEvents = function () {
                domItem.find('.item-title').click(function () {
                    if (currentSelectedHandle) {
                        currentSelectedHandle.sets({current: false});
                    }
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_subscribe', end_point: {intClientSubscription_purchased_ID: purchasedSeries.purchased_ID, intClientSubscription_post_ID: itemData.post_ID}}, true, 200).then(function (res) {
                        var sub = res.data;
                        var parent = sub.strClientSubscription_nodeType == 'path' ? sub.clientsubscription_ID : sub.intClientSubscriptions_parent;
                        ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: purchasedSeries.purchased_ID, parentId: parent, order: sub.intClientSubscriptions_order}).then(function (res) {
                            if (res.status) {
                                callbacksAfterSelect.forEach(function (value) {
                                    value(res.data);
                                });
                                currentSelectedHandle = self;
                                currentSelectedHandle.sets({current: true});
                            }
                        });
                    });
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.post_ID}}, false);
                });
            }
            var bindData = function () {
                if (itemData.strPost_nodeType == 'path') {
                    if (itemData.strPost_featuredimage) {
                        domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                    }
                    else {
                        domItem.find('.item-img').attr('src', 'assets/images/global-icons/tree/path/'+ (itemData.intPost_order) +'.svg');
                    }
                }
                else {
                    domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                }
                domItem.find('.item-title').html(itemData.strPost_title);
                domItem.find('.media-icon-wrapper').addClass('type-' + itemData.intPost_type);
                if (itemData.strPost_duration) {
                    domItem.find('.item-duration').html(itemData.strPost_duration);
                }
                else {
                    helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                        domItem.find('.item-duration').html(res);
                        itemData.strPost_duration = res;
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                    });
                }
                domItem.find('.item-view-states-wrapper').removeClass('state-complete').removeClass('state-current').removeClass('state-normal');
                if (parseInt(itemData.boolCompleted)) {
                    domItem.find('.item-view-states-wrapper').addClass('state-complete');
                }
                else if (itemData.current) {
                    domItem.find('.item-view-states-wrapper').addClass('state-current');
                }
                else {
                    domItem.find('.item-view-states-wrapper').addClass('state-normal');
                }
            }
            this.sets = function (sets) {
                itemData = Object.assign(itemData, sets);

                bindData();
            }
            this.data = function () {
                return itemData;
            }
            this.init = function () {
                domItem = domListing.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUnreadList);
                bindData();
                bindEvents();
            }
        }
        var SharedItemClass = function (itemData) {
            var domItem;
            var self = this;
            var bindEvents = function () {
                domItem.find('.item-title').click(function () {
                    if (currentSelectedHandle) {
                        currentSelectedHandle.sets({current: false});
                    }
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_subscribe', end_point: {intClientSubscription_purchased_ID: purchasedSeries.purchased_ID, intClientSubscription_post_ID: itemData.post_ID}}, true, 200).then(function (res) {
                        var sub = res.data;
                        var parent = sub.strClientSubscription_nodeType == 'path' ? sub.clientsubscription_ID : sub.intClientSubscriptions_parent;
                        ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: purchasedSeries.purchased_ID, parentId: parent, order: sub.intClientSubscriptions_order}).then(function (res) {
                            if (res.status) {
                                callbacksAfterSelect.forEach(function (value) {
                                    value(res.data);
                                });
                                currentSelectedHandle = self;
                                currentSelectedHandle.sets({current: true});
                            }
                        });
                    });
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.post_ID}}, false);
                });
            }
            var bindData = function () {
                if (itemData.nodeType == 'path') {
                    if (itemData.image) {
                        domItem.find('.item-img').attr('src', itemData.image);
                    }
                    else {
                        domItem.find('.item-img').attr('src', 'assets/images/global-icons/tree/path/'+ (itemData.order) +'.svg');
                    }
                }
                else {
                    domItem.find('.item-img').attr('src', itemData.image);
                }
                domItem.find('.item-title').html(itemData.title);
                domItem.find('.media-icon-wrapper').addClass('type-' + itemData.type);
                if (itemData.duration) {
                    domItem.find('.item-duration').html(itemData.duration);
                }
                else {
                    helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                        domItem.find('.item-duration').html(res);
                    });
                }
                domItem.find('.item-view-states-wrapper').removeClass('state-complete').removeClass('state-current').removeClass('state-normal');
                if (parseInt(itemData.completed)) {
                    domItem.find('.item-view-states-wrapper').addClass('state-complete');
                }
                else if (itemData.current) {
                    domItem.find('.item-view-states-wrapper').addClass('state-current');
                }
                else {
                    domItem.find('.item-view-states-wrapper').addClass('state-normal');
                }
            }
            this.sets = function (sets) {
                itemData = Object.assign(itemData, sets);

                bindData();
            }
            this.data = function () {
                return itemData;
            }
            this.dom = function () {
                return domItem;
            }
            this.init = function () {
                domItem = domListing.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSharedPostList);
                bindData();
                bindEvents();
            }
        }
        var sharePosts = function (posts) {
            if (!posts.length) {
                return Promise.resolve(false);
            }
            return ajaxAPiHandle.apiPost('SharedPosts.php', {action: 'share_many', posts: posts}).then(function (res) {
                var share_id = res.data;
                var socialLinks = helperHandle.socialLinks(BASE_URL + '/shared_posts?id=' + share_id, 'walden blog');
                domListing.find('.share-with-list .share-item.item-facebook').attr('href', socialLinks.facebook);
                domListing.find('.share-with-list .share-item.item-twitter').attr('href', socialLinks.twitter);
                domListing.find('.share-with-list .share-item.item-linkedin').attr('href', socialLinks.linkedin);
                domListing.find('.share-with-list .share-item.item-email').attr('href', socialLinks.email);
                return res.data;
            });
        }
        this.dom = function () {
            return domListing;
        }
        this.open = function () {
            var paddingRight = window.outerWidth - $("body").prop("clientWidth") + 'px';
            $('body').css('overflow', 'hidden');
            $('body').css('padding-right', paddingRight);
            domListingPanel.show();
            domListingPanel.addClass('opened');
            setTimeout(function () {
                domListing.removeClass('collapsed');
            }, 10);
        }
        this.close = function () {
            domListing.addClass('collapsed');
            setTimeout(function () {
                domListingPanel.removeClass('opened');
                domListingPanel.hide();
                $('body').css('padding-right', '0px');
                $('body').css('overflow', 'auto');
            }, 300);
        }
        this.update = function (id, sets) {
            itemHandles.forEach(function (value) {
                if (value.data().clientsubscription_ID == id) {
                    value.sets(sets);
                }
            });
        }
        this.afterLoad = function (fn) {
            callbacksAfterLoad.push(fn);
        }
        this.afterSelect = function (fn) {
            callbacksAfterSelect.push(fn);
        }
        this.setCurrent = function (id, type) {
            if (currentSelectedHandle) {
                currentSelectedHandle.sets({current: false});
            }
            if (type === 'post') {
                itemHandles.forEach(function (value) {
                    if (value.data().post_ID === id) {
                        currentSelectedHandle = value;
                        currentSelectedHandle.sets({current: true});
                    }
                })
            } else {

            }
        }
        this.setMode = function (m) {
            domListing.removeClass('mode-' + mode);
            if (m !== 'share') {
                domListing.removeClass('share-step-next');
            }
            mode = m;
            domListing.addClass('mode-' + mode);
            if (purchasedStructureTreeHandle) {
                purchasedStructureTreeHandle.setMode('mode-' + mode);
            }
        }
        this.hasInit = function () {
            return hasInit;
        }
        this.getHandle = function (id) {
            return purchasedStructureTreeHandle.getHandle(id);
        }
        this.gotoShareSeries = function() {
            self.setMode('share');
            domListing.addClass('share-series');
            domListing.addClass('share-step-next');
        }
        this.init = function () {
            hasInit = true;
            domListing = domListingPanel.find('.listing-subscription-inner');
            domList = domListing.find('.listing-posts-content .posts-list');
            domListingFooter = domListing.find('.listing-post-footer');
            domUnreadList = domListing.find('.listing-posts-content .unread-posts-list');
            domSharedPostList = domListing.find('.listing-posts-content .shared-post-list');
            domShareSeries = domListing.find('.series-share-preview .series-article');
            domSharePost = domListing.find('.post-share-preview .post-article');
            ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'get_many', where: {intClientSubscription_purchased_ID: purchasedSeries.purchased_ID, intClientSubscriptions_parent: 0}, meta: {get_unread_posts: {series_id: purchasedSeries.series_ID}}}).then(function (res) {
                if (res.unReadPosts.length) {
                    domListing.addClass('has-new');
                    res.unReadPosts.forEach(function (v) {
                        var hdl = new UnReadItemClass(v);
                        hdl.init();
                        unreadItemHandles.push(hdl);
                    });
                }
                purchasedStructureTreeHandle = new PurchasedStructureTreeClass(domListing.find('.purchased-structure-tree-component'), purchasedSeries);
                purchasedStructureTreeHandle.init();
                purchasedStructureTreeHandle.setMode(mode);
                purchasedStructureTreeHandle.expand().then(function () {
                    callbacksAfterLoad.forEach(function (value) {
                        value();
                    });
                });
                purchasedStructureTreeHandle.onClick(function (itemData, hdl) {
                    if (currentSelectedHandle) {
                        currentSelectedHandle.sets({current: false});
                    }
                    currentSelectedHandle = hdl;
                    currentSelectedHandle.sets({current: true});
                    var parentId = itemData.nodeType == 'path' ? itemData.id : itemData.parent;
                    var order = itemData.nodeType == 'path' ? 1 : itemData.order;
                    ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: purchasedSeries.purchased_ID, parentId: parentId, order: order}).then(function (res) {
                        if (res.status) {
                            callbacksAfterSelect.forEach(function (value) {
                                value(res.data);
                            });
                        }
                        else {
                            swal('Something is wrong!');
                        }
                    });
                });
                domListing.find('.posts-count .count-value').html(res.data.length);
                domShareSeries.find('.child-count span').text(res.data.length);
            });

            seriesOwnersHandle = new SeriesOwnersClass(domListing.find('.series-owners.component'), purchasedSeries);
            seriesOwnersHandle.init();
            bindData();
            bindEvents();
        }
    }
})();
