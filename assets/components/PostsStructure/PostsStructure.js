var PostsStructureClass;

(function () {
    'use structure';

    PostsStructureClass = function (domRoot, series) {

        var structureListHandle, structureGridHandle, postCreatorHandle;
        var currentTab = 'list';
        var self = this;

        var bindEvents = function () {
            domRoot.find('[href="#structure-grid-tab"]').on('shown.bs.tab', function () {
                currentTab = 'grid';
                domRoot.removeClass('list-structure-mode');
                domRoot.addClass('grid-structure-mode');
            });
            domRoot.find('[href="#structure-list-tab"]').on('shown.bs.tab', function () {
                currentTab = 'list';
                domRoot.removeClass('grid-structure-mode');
                domRoot.addClass('list-structure-mode');
            });
            domRoot.find('.add-item-btn').click(function () {
                postCreatorHandle.clean();
                var type = $(this).data('type');
                switch (type) {
                    case 'fork':
                        domRoot.find('[href="#structure-list-tab"]').tab('show');
                        postCreatorHandle.setType('menu');
                        break;
                    case 'form':
                        postCreatorHandle.setType(10);
                        break;
                    case 'skip-point':
                        domRoot.find('[href="#structure-list-tab"]').tab('show');
                        postCreatorHandle.setType(9);
                        break;
                    case 'form_loop':
                        domRoot.find('[href="#structure-list-tab"]').tab('show');
                        postCreatorHandle.setType('form_loop');
                        break;
                }
                postCreatorHandle.open();
            });
            postCreatorHandle.afterSave(function (nData) {
                var vData = helperHandle.postToViewFormat(nData);
                self.add(vData);
            });
        }

        var StructureListClass = function () {
            var domStructureList;
            var seriesStructureTreeHandle;
            var bindEvents = function () {
            }
            this.save = function () {
                return seriesStructureTreeHandle.save();
            }
            this.add = function (post) {
                return seriesStructureTreeHandle.add(post);
            }
            this.getItems = function () {
                return seriesStructureTreeHandle.getItems();
            }
            this.setMode = function (m) {
                seriesStructureTreeHandle.setMode(m);
                switch (m) {
                    case 'add_mode':
                        seriesStructureTreeHandle.emptyExpand();
                        break;
                    default:
                        seriesStructureTreeHandle.expand();
                        break;
                }
            }
            this.init = function () {
                domStructureList = domRoot.find('#structure-list-tab');
                seriesStructureTreeHandle = new SeriesStructureTreeClass(domStructureList.find('.series-structure-tree-component'), series);
                seriesStructureTreeHandle.init();
                bindEvents();
            }
        }

        var StructureGridClass = function () {
            var domStructureGrid, domPostsList, domLoadMoreWrap;
            var mode;
            var itemHandles = [], self = this;

            var bindEvents = function () {
                domPostsList.on('sortupdate', function () {
                    self.saveOrders();
                });
                domLoadMoreWrap.find('.load-more').click(function () {
                    self.loadMore();
                });
            }

            var PostItemClass = function (itemData) {

                var domItem, domBody, domTypeBody;
                var dataLoaded = false, isRemoved = false, isAdded = false;

                var self = this, searchItemHandle, originItemHandle, listItemHandle;

                var remove = function () {
                    isRemoved = true;
                    domItem.remove();
                }
                this.setSearchHandle = function (hdl) {
                    searchItemHandle = hdl;
                }
                this.setOriginHandle = function (hdl) {
                    originItemHandle = hdl;
                }
                this.setListHandle = function (hdl) {
                    listItemHandle = hdl;
                }
                this.isRemoved = function () {
                    return isRemoved;
                }
                this.setAdded = function (v) {
                    isAdded = v;
                }
                this.isAdded = function () {
                    return itemData.id ? true : false;
                }
                this.setId = function (id) {
                    itemData.id = id;
                }
                this.sets = function (sets) {
                    Object.assign(itemData, sets);
                }
                var bindData = function () {
                    dataLoaded = true;
                    domItem.addClass('type-' + itemData.type);
                    switch (parseInt(itemData.type)) {
                        case 0:
                            domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            break;
                        case 2:
                            domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.find('img').attr('src', itemData.image);
                            break;
                        default:
                            domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.html(itemData.body);
                            break;
                    }
                    domItem.find('.item-title').html(itemData.title);
                    if (itemData.duration) {
                        domBody.find('.blog-duration').html(itemData.duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            domBody.find('.blog-duration').html(res);
                            itemData.duration = res;
                        });
                    }
                }
                var bindEvents = function () {
                    domItem.find('.item-action.delete-action').click(function (e) {
                        swal({
                            title: "Are you sure to delete?",
                            icon: "warning",
                            dangerMode: true,
                            buttons: true
                        }).then(function(willDelete){
                            if (willDelete){
                                self.delete().then(function () {
                                    if (listItemHandle) {
                                        listItemHandle.remove();
                                    }
                                    if (originItemHandle) {
                                        originItemHandle.deSelect();
                                    }
                                });
                            }
                        });
                    });
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domPostsList);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = bindData;
                this.remove = remove;
                this.isDataLoaded = function () {
                    return dataLoaded;
                }
                this.delete = function () {
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: itemData.id}).then(function (res) {
                        remove();
                        return res.status;
                    })
                }
                this.init = function () {
                    domItem = domStructureGrid.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    domBody = domItem.find('.item-type-body');
                    domItem.data('controlHandle', self);
                    bindEvents();
                }
            }

            this.saveOrders = function () {
                var itemsArranged = [];
                var orders = [];

                domPostsList.find('>.post-item').each(function () {
                    var hdl = $(this).data('controlHandle');
                    orders.push(hdl.data().order);
                    itemsArranged.push(hdl);
                });
                orders.sort(function (a, b) {
                    return a - b;
                });
                var updates = [];
                itemsArranged.forEach(function (value, i) {
                    updates.push({where: value.data().id, sets: {intPost_order: orders[i]}});
                    value.sets({order: orders[i]});
                });
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'update_many', updates: updates}).then(function (res) {
                    return res;
                });
            }
            this.getItems = function () {
                var rtn = [];
                domPostsList.find('>.post-item').each(function (i) {
                    rtn.push($(this).data('controlHandle'));
                });
                return rtn;
            }
            this.save = function () {
                var items = self.getItems();
                var fItems = [];
                var nItems = [];
                items.forEach(function (value) {
                    if (!value.isAdded()) {
                        nItems.push(value);
                        value.setAdded(true);
                        var f = helperHandle.postFormat(value.data());
                        f.intPost_series_ID = series;
                        fItems.push(f);
                    }
                });
                if (fItems.length) {
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: fItems}).then(function (res) {
                        for (var i = 0; i < nItems.length; i++) {
                            nItems[i].sets({id: res.data[i].post_ID, order: res.data[i].intPost_order});
                        }
                        return res;
                    });
                }
                else {
                    return Promise.resolve(false);
                }
            }
            this.add = function (post) {
                if (!post.nodeType) {
                    post.nodeType = 'post';
                }
                var hdl = new PostItemClass(post);
                hdl.init();
                hdl.bindData();
                hdl.append();
                itemHandles.push(hdl);
                return hdl;
            }
            this.setMode = function (m) {
                mode = m;
                switch (mode) {
                    case 'add_mode':
                        domLoadMoreWrap.remove();
                        break;
                    default:
                        self.load();
                        break;
                }
            }
            this.load = function () {
                var ajaxData = {
                    action: 'get_items',
                    where: {intPost_series_ID: series, intPost_parent: 0},
                    limit: {offset: 0, size: 20},
                    select: '*',
                };
                ajaxAPiHandle.apiPost('Posts.php', ajaxData, false).then(function (res) {
                    res.data.forEach(function (post) {
                        var hdl = new PostItemClass(helperHandle.postToViewFormat(post));
                        hdl.init();
                        hdl.bindData();
                        hdl.append();
                        itemHandles.push(hdl);
                    });
                    if (res.data.length < ajaxData.limit.size) {
                        domLoadMoreWrap.hide();
                    }
                });
            }
            this.loadMore = function () {
                domLoadMoreWrap.addClass('status-loading');
                var ajaxData = {
                    action: 'get_many',
                    where: {
                        intPost_series_ID: series,
                        intPost_parent: 0,
                    },
                    limit: {
                        offset: itemHandles.length,
                        size: 20,
                    }
                };
                return ajaxAPiHandle.apiPost('Posts.php', ajaxData, false).then(function (res) {
                    res.data.forEach(function (post) {
                        var hdl = new PostItemClass(helperHandle.postToViewFormat(post));
                        hdl.init();
                        hdl.bindData();
                        hdl.append();
                        itemHandles.push(hdl);
                    });
                    domLoadMoreWrap.removeClass('status-loading');
                    if (res.data.length < ajaxData.limit.size) {
                        domLoadMoreWrap.hide();
                    }
                });
            }
            this.init = function () {
                domStructureGrid = domRoot.find('#structure-grid-tab');
                domPostsList = domStructureGrid.find('.items-container');
                domLoadMoreWrap = domStructureGrid.find('.load-more-status');
                domPostsList.sortable({
                    placeholder: 'ui-state-highlight',
                    items: 'article.post-item',
                    coneHelperSize: true,
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    revert: 300, // animation in milliseconds
                    // handle: '.img-wrapper',
                    opacity: 0.9,
                    update: function (a, b) {}
                });
                bindEvents();
            }
        }

        this.add = function (post) {
            var l = structureListHandle.add(post);
            var g = structureGridHandle.add(post);
            // g.setListHandle(l);
            return {
                list: l,
                grid: g
            };
        }
        this.save = function () {
            if (currentTab == 'list') {
                return structureListHandle.save();
            }
            else {
                return structureGridHandle.save();
            }
        }
        this.emailAddPosts = function () {
            var hdls = structureListHandle.getItems();
            var posts = [];
            hdls.forEach(function (value) {
                posts.push(value.data().id);
            });
            return ajaxAPiHandle.apiPost('Series.php', {action: 'email_add_posts', series: series, posts: posts}, false).then(function () {
                swal('All owners and users joined will receive email, Thanks!');
                return true;
            });
        }
        this.getItems = function () {
            return structureListHandle.getItems();
        }
        this.setMode = function (m) {
            structureGridHandle.setMode(m);
            structureListHandle.setMode(m);
        }
        this.init = function () {

            structureListHandle = new StructureListClass();
            structureListHandle.init();

            structureGridHandle = new StructureGridClass();
            structureGridHandle.init();

            postCreatorHandle = new PostCreatorClass(domRoot.find('.post-creator-component'), {series_ID: series});
            postCreatorHandle.init();
            bindEvents();
        }
    }
})();