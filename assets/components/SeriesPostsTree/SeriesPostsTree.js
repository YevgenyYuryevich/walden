var SeriesPostsTreeClass;
(function () {

    'use strict';
    SeriesPostsTreeClass = function (series, domRoot, option) {

        var virtualRootData;
        var virtualRootHandle;
        var selectedHandle = false, self = this;

        var ItemRowClass = function (itemData, parentHandle) {

            var domItem, domItemInner;

            var childHandles = [], self = this;

            var bindEvents = function () {
                domItemInner.click(function () {
                    selectedHandle.setActive(false);
                    selectedHandle = self;
                    self.setActive(true);
                });
            }

            var bindData = function () {
                domItemInner.addClass('type-' + itemData.intPost_type);
                domItemInner.find('.item-title').html(itemData.strPost_title);
                domItem.addClass('node-type-' + itemData.strPost_nodeType);
            }
            this.empty = function () {
                childHandles.forEach(function (value) {
                    value.dom().remove();
                });
                childHandles = [];
            }
            this.data = function () {
                return itemData;
            }
            this.setActive = function (v) {
                if (v) {
                    domItemInner.addClass('item-selected');
                }
                else {
                    domItemInner.removeClass('item-selected');
                }
            }
            this.dom = function () {
                return domItem;
            }
            this.loadChilds = function () {
                var ajaxData = {
                    action: 'get_items',
                    where: {
                        intPost_series_ID: series.series_ID,
                        intPost_parent: itemData.post_ID,
                    },
                    select: ['post_ID', 'strPost_title', 'strPost_nodeType', "intPost_type", 'intPost_parent', 'intPost_order'],
                };
                ajaxAPiHandle.apiPost('Posts.php', ajaxData, false).then(function (res) {
                    res.data.forEach(function (itemData) {
                        var hdl = new ItemRowClass(itemData, self);
                        hdl.init();
                        childHandles.push(hdl);
                    });
                });
            }
            this.init = function () {
                domItem = domRoot.find('.item-row.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(parentHandle ? parentHandle.dom().find('>ul') : domRoot.find('> ul'));
                domItemInner = domItem.find('> .row-inner');
                bindData();
                bindEvents();
            }
        }

        var bindData = function () {
            if (series) {
                domRoot.find('.series-title').html(series.strSeries_title);
                virtualRootHandle = new ItemRowClass(virtualRootData, false);
                virtualRootHandle.init();
                virtualRootHandle.loadChilds();
                selectedHandle = virtualRootHandle;
            }
        }
        this.empty = function () {
            domRoot.find('.series-title').html('');
            if (virtualRootHandle) {
                virtualRootHandle.dom().remove();
                virtualRootHandle = false;
            }
        }
        this.setSeries = function (nSeries) {
            self.empty();
            series = nSeries;
            if (typeof series === 'number' || typeof series === 'string') {
                ajaxAPiHandle.apiPost('Series.php', {action: 'get', where: series}, false).then(function (res) {
                    series = res.data;
                    bindData();
                });
            }
            else {
                bindData();
            }
        }
        this.postInsertPosition = function () {
            if (selectedHandle.data().post_ID === 0) {
                return {
                    intPost_parent: 0,
                }
            }
            if (selectedHandle.data().strPost_nodeType === 'path') {
                return {
                    intPost_parent: selectedHandle.data().post_ID,
                }
            }
            return {
                intPost_parent: selectedHandle.data().intPost_parent,
                intPost_order: selectedHandle.data().intPost_order
            }
        }
        this.postRedirectPosition = function () {
            if (selectedHandle.data().strPost_nodeType === 'path') {
                return selectedHandle.data().intPost_parent
            }
            return selectedHandle.data().post_ID;
        }
        this.init = function () {
            option = option ? option : {};
            if (option.showSeriesTitle === false) {
                domRoot.find('.series-title').hide();
            }
            virtualRootData = {
                post_ID: 0,
                strPost_title: '',
                strPost_featuredimage: '',
                intPost_type: 7,
                intPost_order: 1,
                strPost_nodeType: 'root',
                intPost_parent: -1,
                strPost_status: 'publish',
                boolPost_free: 0,
            };
            if (typeof series === 'number' || typeof series === 'string') {
                ajaxAPiHandle.apiPost('Series.php', {action: 'get', where: series}, false).then(function (res) {
                    series = res.data;
                    bindData();
                });
            }
            else {
                bindData();
            }
        }
    }
})();