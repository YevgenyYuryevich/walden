var FormsListClass;

(function () {
    'use strict';
    FormsListClass = function (domRoot, options) {
        var domFormsContainer, domComponentSamples;
        var self = this, fieldRowHandles = [], loopRowHandles = [];
        var formFields = [], formLoops = [], formAnswers = [];
        var callbackOnQuestionClick = function (d) {};
        var callbackOnAnswerClick = function (d) {};
        var FieldRowClass = function (fieldData) {
            var domRow;
            var self = this;
            var bindData = function () {
                domRow.find('.item-name-input').html(fieldData.strFormField_name);
                domRow.addClass('field-type-' + fieldData.strFormField_type);
            }
            var bindEvents = function () {
                domRow.find('.delete-action').click(function () {
                    swal({
                        title: "Are you sure?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete) {
                            deleteRow();
                        }
                    });
                });
                domRow.find('.append-action.append-question').on('click', function () {
                    var d = self.getQuestion();
                    callbackOnQuestionClick(d);
                });
                domRow.find('.append-action.append-answer').click(function () {
                    var d = self.getAnswer();
                    callbackOnAnswerClick(d);
                });
            }
            var deleteRow = function () {
                ajaxAPiHandle.apiPost('FormFields.php', {action: 'delete', where: fieldData.formField_ID}).then(function () {
                    domRow.remove();
                    fieldRowHandles.forEach(function (hdl, i) {
                        if (hdl.data().formField_ID === fieldData.formField_ID) {
                            fieldRowHandles.splice(i, 1);
                            return true;
                        }
                    });
                });
            }
            this.getQuestion = function () {
                var qDom = domComponentSamples.find('.form-field-component').clone();
                var formFieldHandle = new FormFieldClass(qDom, fieldData);
                formFieldHandle.init();
                return formFieldHandle.dom();
            }
            this.getAnswer = function () {
                var aDom = domComponentSamples.find('.answered-form-field-component').clone();
                var formAnswerHandle = new AnsweredFormFieldClass(aDom, fieldData);
                formAnswerHandle.init();
                return formAnswerHandle.dom();
            }
            this.data = function () {
                return fieldData;
            }
            this.dom = function () {
                return domRow;
            }
            this.init = function () {
                domRow = domFormsContainer.find('.form-field-item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domFormsContainer);
                bindData();
                bindEvents();
            }
        }
        var LoopRowClass = function (loopData) {
            var domRow;
            var self = this;
            var bindData = function () {
                domRow.find('.item-name-input').html(loopData.formLoop_name);
            }
            var bindEvents = function () {
                domRow.find('.delete-action').click(function () {
                    swal({
                        title: "Are you sure?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete) {
                            deleteRow();
                        }
                    });
                });
                domRow.find('.append-action.append-question').on('click', function () {
                    var d = self.getQuestion();
                    callbackOnQuestionClick(d);
                });
                domRow.find('.append-action.append-answer-parag').click(function () {
                    var d = self.getAnswerParag();
                    callbackOnAnswerClick(d);
                });
                domRow.find('.append-action.append-answer-table').click(function () {
                    var d = self.getAnswerTable();
                    callbackOnAnswerClick(d);
                });
            }
            var deleteRow = function () {
                ajaxAPiHandle.apiPost('FormLoop.php', {action: 'delete', where: loopData.formLoop_ID}).then(function () {
                    domRow.remove();
                    loopRowHandles.forEach(function (hdl, i) {
                        if (hdl.data().formLoop_ID === loopData.formLoop_ID) {
                            loopRowHandles.splice(i, 1);
                            return true;
                        }
                    });
                });
            }
            this.getQuestionPre = function () {
                var qDom = domComponentSamples.find('.form-loop-component').clone();
                loopData.mode = 'question';
                var hdl = new FormLoopClass(qDom, loopData, {mode: 'question'});
                hdl.init();
                return hdl.dom();
            }
            this.getQuestion = function () {
                var qDom = domComponentSamples.find('.form-loop-question-component').clone();
                var hdl = new FormLoopQuestionClass(qDom, loopData);
                hdl.init();
                return hdl.dom();
            }
            this.getAnswer = function () {
                var qDom = domComponentSamples.find('.form-loop-component').clone();
                loopData.mode = 'answer';
                var hdl = new FormLoopClass(qDom, loopData, {mode: 'answer'});
                hdl.init();
                return hdl.dom();
            }
            this.getAnswerParag = function () {
                var aDom = domComponentSamples.find('.form-loop-answer-parag-component').clone();
                var hdl = new FormLoopAnswerParagClass(aDom, loopData);
                hdl.init();
                return hdl.dom();
            }
            this.getAnswerTable = function () {
                var aDom = domComponentSamples.find('.form-loop-answer-table-component').clone();
                var hdl = new FormLoopAnswerTableClass(aDom, loopData);
                hdl.init();
                return hdl.dom();
            }
            this.data = function () {
                return loopData;
            }
            this.dom = function () {
                return domRow;
            }
            this.init = function () {
                domRow = domFormsContainer.find('.form-loop-item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domFormsContainer);
                bindData();
                bindEvents();
            }
        }

        var bindData = function () {
            var fieldsResolve = ajaxAPiHandle.apiPost('FormFields.php', {action: 'get_many', where: {intFormField_series_ID: options.seriesId}}, false).then(function (res) {
                formFields = res.data;
                return res.data;
            });
            var loopResolve = ajaxAPiHandle.apiPost('FormLoop.php', {action: 'get_many', where: {formLoop_series_ID: options.seriesId}}, false).then(function (res) {
                formLoops = res.data;
                return res.data;
            });
            var answerResolve = ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'get_many', where: {intFormFieldAnswer_client_ID: CLIENT_ID}}, false).then(function (res) {
                formAnswers = res.data;
                return res.data;
            });
            Promise.all([loopResolve, fieldsResolve, answerResolve]).then(function () {
                formFields.forEach(function (value) {
                    if (value.formField_loop_ID) {
                        value.answers = formAnswers.filter(function (value1) {
                            return value1.intFormFieldAnswer_field_ID == value.formField_ID;
                        });
                        return false;
                    }
                    var answer = formAnswers.find(function (value1) {
                        value1.intFormFieldAnswer_field_ID == value.formField_ID;
                    });
                    if (answer) {
                        value.answer = answer;
                    }
                    var hdl = new FieldRowClass(value);
                    hdl.init();
                    fieldRowHandles.push(hdl);
                });
                formLoops.forEach(function (value) {
                    value.fields = formFields.filter(function (value1) {
                        return value1.formField_loop_ID == value.formLoop_ID;
                    });
                    var hdl = new LoopRowClass(value);
                    hdl.init();
                    loopRowHandles.push(hdl);
                });
            });
        }
        this.setSeries = function (v) {
            self.empty();
            options.seriesId = v;
            bindData();
        }
        this.onQuestionClick = function (fn) {
            callbackOnQuestionClick = fn;
        }
        this.onAnswerClick = function (fn) {
            callbackOnAnswerClick = fn;
        }
        this.addField = function (value) {
            var hdl = new FieldRowClass(value);
            hdl.init();
            fieldRowHandles.push(hdl);
        }
        this.addLoop = function (value) {
            var hdl = new LoopRowClass(value);
            hdl.init();
            loopRowHandles.push(hdl);
        }
        this.empty = function () {
            fieldRowHandles.forEach(function (value) {
               value.dom().remove();
            });
            loopRowHandles.forEach(function (value) {
                value.dom().remove();
            });
            fieldRowHandles = [];
            loopRowHandles = [];
        }
        this.init = function () {
            domFormsContainer = domRoot.find('.forms-list-container');
            domComponentSamples = domRoot.find('.component-samples');
            bindData();
        }
    }
})();