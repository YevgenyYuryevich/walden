<div class="component forms-list-component">
    <div class="forms-list-header">
        <div class="flex-row vertical-center">
            <div class="flex-col">
                <div class="form-fields-title">Forms List</div>
            </div>
            <div class="flex-col fix-col">
                <a href="javascript:;" class="preview-form-link bottom-underline">preview form</a>
            </div>
        </div>
    </div>
    <div class="forms-list-container">
        <div class="form-field-item sample" hidden>
            <div class="flex-row margin-between vertical-center">
                <div class="flex-col fix-col">
                    <div class="append-action append-question flex-centering"><div>Q</div></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="append-action append-answer flex-centering"><div>A</div></div>
                </div>
                <div class="flex-col fix-col">
                    <a class="item-action delete-action" href="javascript:;">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                            <g>
                                <g>
                                    <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                        286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
                                    <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                        C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                        S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
                <div class="flex-col">
                    <div type="text" class="form-control item-name-input"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="field-item-type-wrapper">
                        <svg class="text-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <path style="fill:#303C42;" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224C83.146,0,64,19.135,64,42.667
                                                                    v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                <path style="fill:#E6E6E6;" d="M341.333,36.417l70.25,70.25h-48.917c-11.771,0-21.333-9.573-21.333-21.333V36.417z"/>
                                                                <path style="fill:#FFFFFF;" d="M405.333,490.667H106.667c-11.771,0-21.333-9.573-21.333-21.333V42.667
                                                                    c0-11.76,9.563-21.333,21.333-21.333H320v64C320,108.865,339.146,128,362.667,128h64v341.333
                                                                    C426.667,481.094,417.104,490.667,405.333,490.667z"/>
                                                                <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-30.0002" y1="641.8779" x2="-25.7514" y2="637.6291" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                    <stop  offset="0" style="stop-color:#000000;stop-opacity:0.1"/>
                                                                    <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                                                                </linearGradient>
                                                                <path style="fill:url(#SVGID_1_);" d="M362.667,128c-10.005,0-19.098-3.605-26.383-9.397l-0.259-0.027l90.642,90.642V128H362.667z"
                                                                />
                                                                <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-46.4381" y1="639.0643" x2="-23.653" y2="628.4363" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                    <stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
                                                                    <stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
                                                                </linearGradient>
                                                                <path style="fill:url(#SVGID_2_);" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224
                                                                    C83.146,0,64,19.135,64,42.667v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                <g>
                                                                    <path style="fill:#303C42;" d="M352,277.333h-85.333c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h21.333V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667H320c5.896,0,10.667-4.771,10.667-10.667c0-5.896-4.771-10.667-10.667-10.667v-85.333h21.333
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667V288C362.667,282.104,357.896,277.333,352,277.333z"/>
                                                                    <path style="fill:#303C42;" d="M234.667,384V234.667h42.667c0,5.896,4.771,10.667,10.667,10.667
                                                                        c5.896,0,10.667-4.771,10.667-10.667V224c0-5.896-4.771-10.667-10.667-10.667H160c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h42.667V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667h21.333c5.896,0,10.667-4.771,10.667-10.667C245.333,388.771,240.563,384,234.667,384z"/>
                                                                </g>
                                                                </svg>
                        <svg class="checkbox-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 469.333 469.333;" xml:space="preserve">
                            <g>
                                <g>
                                    <g>
                                        <path d="M426.667,0h-384C19.146,0,0,19.135,0,42.667v384c0,23.531,19.146,42.667,42.667,42.667h384
                                            c23.521,0,42.667-19.135,42.667-42.667v-384C469.333,19.135,450.188,0,426.667,0z M448,426.667
                                            c0,11.76-9.563,21.333-21.333,21.333h-384c-11.771,0-21.333-9.573-21.333-21.333v-384c0-11.76,9.563-21.333,21.333-21.333h384
                                            c11.771,0,21.333,9.573,21.333,21.333V426.667z"/>
                                        <path d="M365.583,131.344L192.187,314.927l-88.25-98.062c-3.979-4.385-10.708-4.729-15.083-0.792s-4.729,10.688-0.792,15.063
                                            l96,106.667c2,2.219,4.833,3.49,7.813,3.531c0.042,0,0.083,0,0.125,0c2.938,0,5.729-1.208,7.75-3.344l181.333-192
                                            c4.042-4.281,3.854-11.031-0.417-15.073C376.396,126.865,369.604,127.062,365.583,131.344z"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-loop-item sample" hidden>
            <div class="flex-row margin-between vertical-center">
                <div class="flex-col fix-col">
                    <div class="append-action append-question flex-centering"><div>Q</div></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="append-action append-answer-parag flex-centering"><div>AP</div></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="append-action append-answer-table flex-centering"><div>AT</div></div>
                </div>
                <div class="flex-col fix-col">
                    <a class="item-action delete-action" href="javascript:;">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                            <g>
                                <g>
                                    <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                        286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
                                    <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                        C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                        S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
                <div class="flex-col">
                    <div type="text" class="form-control item-name-input"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="loop-icon-wrapper">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 480 480" style="enable-background:new 0 0 480 480;" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <path d="M472,0H8C3.582,0,0,3.582,0,8v464c0,4.418,3.582,8,8,8h464c4.418,0,8-3.582,8-8V8C480,3.582,476.418,0,472,0z M464,464
                                        H16V80h448V464z M464,64H16V16h448V64z"/>
                                    <rect x="32" y="32" width="16" height="16"/>
                                    <rect x="64" y="32" width="16" height="16"/>
                                    <rect x="96" y="32" width="16" height="16"/>
                                    <path d="M200,392h80c4.418,0,8-3.582,8-8v-64c0-4.418-3.582-8-8-8h-80c-4.418,0-8,3.582-8,8v64C192,388.418,195.582,392,200,392z
                                         M208,328h64v48h-64V328z"/>
                                    <path d="M200,232h80c4.418,0,8-3.582,8-8v-64c0-4.418-3.582-8-8-8h-80c-4.418,0-8,3.582-8,8v64C192,228.418,195.582,232,200,232z
                                         M208,168h64v48h-64V168z"/>
                                    <path d="M328,392h80c4.418,0,8-3.582,8-8v-64c0-4.418-3.582-8-8-8h-80c-4.418,0-8,3.582-8,8v64C320,388.418,323.582,392,328,392z
                                         M336,328h64v48h-64V328z"/>
                                    <path d="M328,232h80c4.418,0,8-3.582,8-8v-64c0-4.418-3.582-8-8-8h-80c-4.418,0-8,3.582-8,8v64C320,228.418,323.582,232,328,232z
                                         M336,168h64v48h-64V168z"/>
                                    <path d="M72,392h80c4.418,0,8-3.582,8-8v-64c0-4.418-3.582-8-8-8H72c-4.418,0-8,3.582-8,8v64C64,388.418,67.582,392,72,392z
                                         M80,328h64v48H80V328z"/>
                                    <path d="M72,232h80c4.418,0,8-3.582,8-8v-64c0-4.418-3.582-8-8-8H72c-4.418,0-8,3.582-8,8v64C64,228.418,67.582,232,72,232z
                                         M80,168h64v48H80V168z"/>
                                </g>
                            </g>
                        </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="component-samples" hidden>
        <?php require ASSETS_PATH . '/components/FormField/FormField.html';?>
        <?php require ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
        <?php require ASSETS_PATH . '/components/FormLoop/FormLoop.html';?>
        <?php require ASSETS_PATH . '/components/FormLoopQuestion/FormLoopQuestion.html';?>
        <?php require ASSETS_PATH . '/components/FormLoopAnswerParag/FormLoopAnswerParag.html';?>
        <?php require ASSETS_PATH . '/components/FormLoopAnswerTable/FormLoopAnswerTable.html';?>
    </div>
</div>