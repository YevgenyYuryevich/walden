var SeriesCopyEmbedClass;
var seriesCopyEmbedCount = 0;
(function () {
    'use strict';
    SeriesCopyEmbedClass = function (domComponet, series) {
        var domEmbed, domCode;
        var self = this;
        var bindEvents = function () {
            domCode.on('focus', function () {
                domCode.select();
                document.execCommand("Copy");
            });
            domCode.on('click', function () {
                domCode.select();
                document.execCommand("Copy");
            });
            domEmbed.find('.copy-embed-code-btn').click(function () {
                domCode.select();
                document.execCommand("Copy");
            });
            $(window).click(function () {
                if (domEmbed.hasClass('in')) {
                    domEmbed.collapse('hide');
                }
            });
            domEmbed.click(function (event) {
                event.stopPropagation();
            });
        };

        this.bindData = function () {
            domComponet.find('[href="#series-embed-code"]').attr('href', '#series-embed-code' + (++seriesCopyEmbedCount));
            domComponet.find('#series-embed-code').attr('id', 'series-embed-code' + seriesCopyEmbedCount);
            var url = BASE_URL + '/series_embed?viewVersion=guided' +'&id=' + series.series_ID;
            if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                url += '&affiliate_id=' + AFFILIATE_ID;
            }
            domCode.val(helperHandle.generateEmbedCode(url, series.strSeries_title, series.strSeries_description));
        };
        this.init = function () {
            domEmbed = domComponet.find('.series-embed-code');
            domCode = domEmbed.find('textarea');
            self.bindData();
            bindEvents();
        }
    };
})();