<div class="component series-conversation-component">
    <div class="component-inner height-100">
        <div class="flex-row height-100">
            <div class="flex-col conversation-col">
                <section class="conversation-pane height-100">
                    <div class="tab-content height-100">
                        <div class="tab-pane fade in active public-pane height-100">
                            <div class="conversation-public-inner height-100 d-flex flex-direction-column">
                                <header class="conversation-header fg-0">
                                    <div class="flex-row vertical-center">
                                        <div class="flex-col fix-col d-width-md-none d-width-sm-block">
                                            <a href="javascript:;" class="show-users-pane-link">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve">
                                                <path style="" d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
                                                    c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
                                                    c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
                                                </svg>
                                            </a>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="conversation-txt">Conversation</div>
                                        </div>
                                    </div>
                                </header>
                                <div class="conversation-content fg-1">
                                    <?php require ASSETS_PATH . '/components/SkypeLoader/SkypeLoader.html';?>
                                    <ul class="message-list">
                                        <li class="message-item sample" hidden>
                                            <div class="item-inner">
                                                <div class="item-header">
                                                    <div class="flex-row vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <a href="javascript:;" class="item-action item-delete">
                                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     viewBox="0 0 220.176 220.176" style="enable-background:new 0 0 220.176 220.176;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <g>
                                                                            <path style="" d="M131.577,110.084l84.176-84.146c5.897-5.928,5.897-15.565,0-21.492
                                                                                c-5.928-5.928-15.595-5.928-21.492,0l-84.176,84.146L25.938,4.446c-5.928-5.928-15.565-5.928-21.492,0s-5.928,15.565,0,21.492
                                                                                l84.146,84.146L4.446,194.26c-5.928,5.897-5.928,15.565,0,21.492c5.928,5.897,15.565,5.897,21.492,0l84.146-84.176l84.176,84.176
                                                                                c5.897,5.897,15.565,5.897,21.492,0c5.897-5.928,5.897-15.595,0-21.492L131.577,110.084z"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                            </a>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="item-user">
                                                                Hasin Hyder
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="item-time">18 minutes ago</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">
                                                    How was the traffic coming over here?
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <footer class="conversation-footer fg-0">
                                    <form action="javascript:;">
                                        <div class="input-wrapper">
                                            <textarea placeholder="Type your message" rows="3" required></textarea>
                                        </div>
                                        <div class="flex-row">
                                            <div class="flex-col fix-col ml-auto">
                                                <button type="submit">SEND</button>
                                            </div>
                                        </div>
                                    </form>
                                </footer>
                            </div>
                        </div>
                        <div class="tab-pane fade private-pane height-100">
                            <div class="conversation-private-inner height-100 d-flex flex-direction-column">
                                <header class="conversation-header fg-0">
                                    <div class="flex-row vertical-center">
                                        <div class="flex-col fix-col d-width-md-none d-width-sm-block">
                                            <a href="javascript:;" class="show-users-pane-link">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve">
                                                <path style="" d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
                                                    c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
                                                    c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
                                                </svg>
                                            </a>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <img class="user-img" src="assets/images/global-icons/user-silhouette.svg" alt=""/>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="user-name">Hasin Hyder</div>
                                        </div>
                                    </div>
                                </header>
                                <div class="conversation-content fg-1">
                                    <?php require ASSETS_PATH . '/components/SkypeLoader/SkypeLoader.html';?>
                                    <ul class="message-list">
                                        <li class="message-item sample" hidden>
                                            <div class="item-inner">
                                                <div class="item-header">
                                                    <div class="flex-row vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <a href="javascript:;" class="item-action item-delete">
                                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     viewBox="0 0 220.176 220.176" style="enable-background:new 0 0 220.176 220.176;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <g>
                                                                            <path style="" d="M131.577,110.084l84.176-84.146c5.897-5.928,5.897-15.565,0-21.492
                                                                                c-5.928-5.928-15.595-5.928-21.492,0l-84.176,84.146L25.938,4.446c-5.928-5.928-15.565-5.928-21.492,0s-5.928,15.565,0,21.492
                                                                                l84.146,84.146L4.446,194.26c-5.928,5.897-5.928,15.565,0,21.492c5.928,5.897,15.565,5.897,21.492,0l84.146-84.176l84.176,84.176
                                                                                c5.897,5.897,15.565,5.897,21.492,0c5.897-5.928,5.897-15.595,0-21.492L131.577,110.084z"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                            </a>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="item-user">
                                                                Hasin Hyder
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="item-time">18 minutes ago</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-content">
                                                    How was the traffic coming over here?
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <footer class="conversation-footer fg-0">
                                    <form action="javascript:;">
                                        <div class="input-wrapper">
                                            <textarea placeholder="Type your message" rows="3" required></textarea>
                                        </div>
                                        <div class="flex-row">
                                            <div class="flex-col fix-col ml-auto">
                                                <button type="submit">SEND</button>
                                            </div>
                                        </div>
                                    </form>
                                </footer>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="flex-col fix-col users-col">
                <section class="users-pane">
                    <header class="users-pane-header">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>Community</span>
                    </header>
                    <ul class="users-list">
                        <li class="user-item sample" hidden>
                            <div class="item-inner">
                                <div class="flex-row vertical-center">
                                    <div class="flex-col fix-col">
                                        <div class="name-prefix flex-centering"><span class="prefix-val">HH</span></div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="user-name">Hasin Hyder</div>
                                    </div>
                                    <div class="flex-col">
                                        <div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="user-item for-public active">
                            <div class="item-inner">
                                <div class="flex-row vertical-center">
                                    <div class="flex-col fix-col">
                                        <div class="name-prefix flex-centering"><span class="prefix-val">AL</span></div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="user-name">Public Chat</div>
                                    </div>
                                    <div class="flex-col">
                                        <div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </div>
    <aside class="tab-links-container" hidden>
        <a href="javascript:;" class="to-public-tab" data-toggle="tab"></a>
        <a href="javascript:;" class="to-private-tab" data-toggle="tab"></a>
    </aside>
</div>