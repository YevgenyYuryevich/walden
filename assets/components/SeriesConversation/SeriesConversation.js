var SeriesConversationClass;

(function () {
    'use strict';

    SeriesConversationClass = function (domRoot, series, options) {

        var domConversationPane, domAsideLinks;

        var publicConversationHandle, privateConversationHandle, usersHandle;

        var users, widthSize = 'md';

        var showUsersPane = function () {
            domRoot.addClass('show-users-pane');
        }
        var showConversationPane = function () {
            domRoot.removeClass('show-users-pane');
        }
        var PublicConversationClass = function () {

            var domPublicConversation, domMsgList, domForm;

            var databaseScopeRef, itemHandles = [], skypeLoaderHandle;

            var typingTimeout;
            var bindData = function () {
                databaseScopeRef.once('value', function(snapshot) {
                    skypeLoaderHandle.hide();
                    skypeLoaderHandle.afterHide(function () {
                        var rows = snapshot.val();
                        if (rows) {
                            for (var key in rows) {
                                var itemData = rows[key];
                                itemData.key = key;
                                var hdl = new MsgItemClass(itemData);
                                hdl.init();
                                hdl.append();
                                itemHandles[itemData.key] = hdl;
                            }
                        }
                        databaseScopeRef.endAt().limitToLast(1).on('child_added', function(data) {
                            var itemData = data.val();
                            if (itemHandles[data.key]) {
                                return false;
                            }
                            itemData.key = data.key;
                            var hdl = new MsgItemClass(itemData);
                            hdl.init();
                            hdl.append();
                            itemHandles[itemData.key] = hdl;
                        });
                    });
                });
            }
            var bindEvents = function () {
                domPublicConversation.find('.show-users-pane-link').click(function () {
                    showUsersPane();
                });
                domForm.submit(function () {
                    var msg = domForm.find('textarea').val();
                    domForm.find('textarea').val('');
                    usersHandle.updateMe({isTyping: 0});
                    clearTimeout(typingTimeout);
                    insertMsg(msg);
                });
                domForm.find('textarea').keyup(function (e) {
                    clearTimeout(typingTimeout);
                    usersHandle.updateMe({isTyping: 1});
                    typingTimeout = setTimeout(function () {
                        usersHandle.updateMe({isTyping: 0});
                    }, 1000);
                });
                databaseScopeRef.on('child_removed', function(data) {
                    itemHandles[data.key].removeItem();
                });
            }
            var insertMsg = function (msg) {
                var newRef = databaseScopeRef.push();
                newRef.set({
                    user_id: CLIENT_ID,
                    message: msg,
                    status: 'publish',
                    timestamp: new Date().getTime(),
                });
            }
            var MsgItemClass = function (itemData) {
                var domMsgItem;
                var itemUser, itemRef;
                var self = this;
                var bindTimeInterval;

                var bindEvents = function () {
                    domMsgItem.find('.item-delete').click(function () {
                        swal({
                            title: "Are you sure?",
                            text: "Do you want to delete this message?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                itemRef.remove();
                            }
                        });
                    });
                    bindTimeInterval = setInterval(function () {
                        bindTime();
                    }, 1000 * 60);
                }
                var bindTime = function () {
                    var offset = new Date().getTime() - parseInt(itemData.timestamp);
                    var displayTime;
                    if (offset / 1000 / 60 / 60 / 24 >= 1) {
                        displayTime = new Date(parseInt(itemData.timestamp)).toDateString();
                    }
                    else if (offset / 1000 / 60 / 60 >= 1) {
                        var oh = offset / 1000 / 60 / 60;
                        displayTime = (parseInt(oh) > 1 ? parseInt(oh) + ' hours ago' : 'a hour ago');
                    }
                    else if (offset / 1000 / 60 >= 1) {
                        var om = offset / 1000 / 60;
                        displayTime = (parseInt(om) > 1 ? parseInt(om) + ' minutes ago' : 'a minute ago');
                    }
                    else {
                        displayTime = 'a minute ago';
                    }
                    domMsgItem.find('.item-time').html(displayTime);
                }
                this.removeItem = function () {
                    domMsgItem.remove();
                    clearInterval(bindTimeInterval);
                    delete itemHandles[itemData.key];
                }
                this.bindData = function () {
                    users.forEach(function (u) {
                        if (parseInt(u.id) === parseInt(itemData.user_id)) {
                            itemUser = u;
                        }
                    });
                    if (itemUser.id == CLIENT_ID) {
                        domMsgItem.addClass('justify-right');
                    }
                    else {
                        domMsgItem.addClass('justify-left');
                        domMsgItem.find('.item-delete').parent().remove();
                    }
                    domMsgItem.find('.item-user').html(itemUser.f_name);
                    domMsgItem.find('.item-content').html(itemData.message);
                    bindTime();
                }
                this.append = function () {
                    domMsgItem.appendTo(domMsgList);
                }
                this.init = function () {
                    itemRef = databaseScopeRef.child(itemData.key);
                    domMsgItem = domMsgList.find('.message-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domMsgList);
                    self.bindData();
                    bindEvents();
                }
            }
            this.tabShow = function () {
                domAsideLinks.find('[href="#' + domPublicConversation.parent().attr('id') + '"]').tab('show');
            }
            this.init = function () {
                domPublicConversation = domConversationPane.find('.conversation-public-inner');
                skypeLoaderHandle = new SkypeLoaderClass(domPublicConversation.find('.skype-loader-component'));
                skypeLoaderHandle.init();
                skypeLoaderHandle.show();

                domPublicConversation.parent().attr('id', 'series-conversation-public-' + series);
                domAsideLinks.find('.to-public-tab').attr('href', '#series-conversation-public-' + series);
                domMsgList = domPublicConversation.find('.message-list');
                domForm = domPublicConversation.find('form');
                databaseScopeRef = firebase.database().ref('series-conversations/' + series + '/public');
                bindData();
                bindEvents();
            }
        }

        var PrivateConversationClass = function () {

            var domPrivateConversation, domPrivateHeader, domMsgList, domForm;

            var databaseScopeRef, itemHandles = [], skypeLoaderHandle;
            var toUser;

            var typingTimeout;
            var bindData = function () {
            }
            var bindEvents = function () {
                domPrivateConversation.find('.show-users-pane-link').click(function () {
                    showUsersPane();
                });
                domForm.submit(function () {
                    var msg = domForm.find('textarea').val();
                    domForm.find('textarea').val('');
                    usersHandle.updateMe({isTyping: 0});
                    clearTimeout(typingTimeout);
                    insertMsg(msg);
                });
                domForm.find('textarea').keyup(function (e) {
                    clearTimeout(typingTimeout);
                    usersHandle.updateMe({isTyping: 1});
                    typingTimeout = setTimeout(function () {
                        usersHandle.updateMe({isTyping: 0});
                    }, 1000);
                });
                domPrivateConversation.find('.to-public').click(function () {
                    publicConversationHandle.tabShow();
                });
                databaseScopeRef.on('child_added', function(data) {
                    if (toUser) {
                        var itemData = data.val();
                        itemData.key = data.key;
                        if ((itemData.from_id == CLIENT_ID && itemData.to_id == toUser.id) || (itemData.from_id == toUser.id && itemData.to_id == CLIENT_ID)) {
                            var hdl = new MsgItemClass(itemData);
                            hdl.init();
                            hdl.append();
                            itemHandles[itemData.key] = hdl;
                        }
                    }
                });
                databaseScopeRef.on('child_removed', function(data) {
                    if (itemHandles[data.key]) {
                        itemHandles[data.key].removeItem();
                    }
                });
            }
            var insertMsg = function (msg) {
                var newRef = databaseScopeRef.push();
                newRef.set({
                    from_id: CLIENT_ID,
                    to_id: toUser.id,
                    message: msg,
                    status: 'publish',
                    timestamp: new Date().getTime(),
                });
            }
            var MsgItemClass = function (itemData) {
                var domMsgItem;
                var itemUser, itemRef;
                var self = this;
                var bindTimeInterval;

                var bindEvents = function () {
                    domMsgItem.find('.item-delete').click(function () {
                        swal({
                            title: "Are you sure?",
                            text: "Do you want to delete this message?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                itemRef.remove();
                            }
                        });
                    });
                    bindTimeInterval = setInterval(function () {
                        bindTime();
                    }, 1000 * 60);
                }
                var bindTime = function () {
                    var offset = new Date().getTime() - parseInt(itemData.timestamp);
                    var displayTime;
                    if (offset / 1000 / 60 / 60 / 24 >= 1) {
                        displayTime = new Date(parseInt(itemData.timestamp)).toDateString();
                    }
                    else if (offset / 1000 / 60 / 60 >= 1) {
                        var oh = offset / 1000 / 60 / 60;
                        displayTime = (parseInt(oh) > 1 ? parseInt(oh) + ' hours ago' : 'a hour ago');
                    }
                    else if (offset / 1000 / 60 >= 1) {
                        var om = offset / 1000 / 60;
                        displayTime = (parseInt(om) > 1 ? parseInt(om) + ' minutes ago' : 'a minute ago');
                    }
                    else {
                        displayTime = 'a minute ago';
                    }
                    domMsgItem.find('.item-time').html(displayTime);
                }
                this.removeItem = function () {
                    domMsgItem.remove();
                    clearInterval(bindTimeInterval);
                    delete itemHandles[itemData.key];
                }
                this.bindData = function () {
                    users.forEach(function (u) {
                        if (parseInt(u.id) === parseInt(itemData.from_id)) {
                            itemUser = u;
                        }
                    });
                    if (itemUser.id == CLIENT_ID) {
                        domMsgItem.addClass('justify-right');
                    }
                    else {
                        domMsgItem.addClass('justify-left');
                        domMsgItem.find('.item-delete').parent().remove();
                    }
                    domMsgItem.find('.item-user').html(itemUser.f_name);
                    domMsgItem.find('.item-content').html(itemData.message);
                    bindTime();
                }
                this.append = function () {
                    domMsgItem.appendTo(domMsgList);
                }
                this.init = function () {
                    itemRef = databaseScopeRef.child(itemData.key);
                    domMsgItem = domMsgList.find('.message-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domMsgList);
                    self.bindData();
                    bindEvents();
                }
            }
            this.empty = function () {
                skypeLoaderHandle.directHide();
                for (var k in itemHandles) {
                    itemHandles[k].removeItem();
                }
                toUser = false;
            }
            this.setToUser = function (u) {
                toUser = u;
                skypeLoaderHandle.show();
                databaseScopeRef.once('value', function(data) {
                    if (toUser) {
                        skypeLoaderHandle.afterHide(function () {
                            var rows = data.val();
                            for (var key in rows) {
                                var itemData = rows[key];
                                itemData.key = key;
                                if ((itemData.from_id == CLIENT_ID && itemData.to_id == toUser.id) || (itemData.from_id == toUser.id && itemData.to_id == CLIENT_ID)) {
                                    var hdl = new MsgItemClass(itemData);
                                    hdl.init();
                                    hdl.append();
                                    itemHandles[itemData.key] = hdl;
                                }
                            }
                        });
                    }
                    skypeLoaderHandle.hide();
                });
                if (toUser.image) {
                    domPrivateHeader.find('.user-img').attr('src', toUser.image);
                }
                else {
                    domPrivateHeader.find('.user-img').attr('src', 'assets/images/global-icons/user-silhouette.svg');
                }
                domPrivateHeader.find('.user-name').html(toUser.f_name);
            }
            this.tabShow = function () {
                domAsideLinks.find('[href="#' + domPrivateConversation.parent().attr('id') + '"]').tab('show');
            }
            this.init = function () {
                domPrivateConversation = domConversationPane.find('.conversation-private-inner');
                skypeLoaderHandle = new SkypeLoaderClass(domPrivateConversation.find('.skype-loader-component'));
                skypeLoaderHandle.init();
                domPrivateHeader = domPrivateConversation.find('.conversation-header');
                domPrivateConversation.parent().attr('id', 'series-conversation-private-' + series);
                domAsideLinks.find('.to-private-tab').attr('href', '#series-conversation-private-' + series);
                domMsgList = domPrivateConversation.find('.message-list');
                domForm = domPrivateConversation.find('form');
                databaseScopeRef = firebase.database().ref('series-conversations/' + series + '/private');
                bindData();
                bindEvents();
            }
        }

        var UsersClass = function () {
            var domUsersPane, domUsersList;
            var userItemHandles = [];
            var databaseUsersRef;
            var loadResolve;

            var bindEvents = function () {
                databaseUsersRef.on('child_changed', function (data) {
                    if (userItemHandles[data.key]) {
                        userItemHandles[data.key].set(data.val());
                    }
                });
                domUsersList.find('.user-item.for-public').click(function () {
                    domUsersList.find('.user-item').removeClass('active');
                    $(this).addClass('active');
                    privateConversationHandle.empty();
                    if (widthSize == 'sm') {
                        showConversationPane();
                    }
                    else {
                    }
                    publicConversationHandle.tabShow();
                });
            }
            var bindData = function () {
                loadResolve = ajaxAPiHandle.apiPost('Series.php', {action: 'get_owners', id: series}, false).then(function (res) {
                    users = res.data;
                    users.forEach(function (itemData) {
                        if (itemData.id == CLIENT_ID) {
                            return ;
                        }
                        var hdl = new UserItemClass(itemData);
                        hdl.init();
                        userItemHandles[itemData.id] = hdl;
                    });
                    return users;
                });
            }
            var generateColor = function (size) {
                var ar = [
                    '#005aa7',
                    '#02111d',
                    '#ffe259',
                    '#acb6e5',
                    '#536976',
                    '#bbd2c5',
                    '#b79891',
                    '#bbd2c5',
                    '#076585',
                    '#00467f',
                    '#1488cc',
                    '#ec008c',
                    '#cc2b5e',
                    '#2193b0',
                    '#e65c00',
                    '#2b5876',
                    '#314755',
                    '#77a1d3',
                    '#ff6e7f',
                    '#e52d27',
                    '#603813',
                    '#16a085',
                    '#d31027',
                    '#ede574',
                    '#02aab0',
                    '#da22ff',
                    '#348f50',
                    '#3ca55c',
                    '#cc95c0',
                    '#003973',
                    '#e55d87',
                    '#403b4a',
                    '#f09819',
                    '#ff512f',
                    '#aa076b',
                    '#1a2980',
                    '#ff512f',
                    '#1d2b64',
                    '#1fa2ff',
                    '#4cb8c4'
                ]
                size = parseInt(size);
                return ar[size % ar.length];
            }
            var UserItemClass = function (itemData) {
                var domItem;
                var bindEvents = function () {
                    domItem.click(function () {
                        domUsersList.find('.user-item').removeClass('active');
                        domItem.addClass('active');
                        privateConversationHandle.empty();
                        if (widthSize == 'sm') {
                            showConversationPane();
                        }
                        else {

                        }
                        privateConversationHandle.tabShow();
                        domAsideLinks.find('.to-private-tab').off('shown.bs.tab').on('shown.bs.tab', function () {
                            privateConversationHandle.setToUser(itemData);
                        });
                    });
                }
                var bindData = function () {
                    domItem.find('.user-name').html(itemData.f_name);
                    domItem.find('.name-prefix').css('background-color', generateColor(itemData.id));
                    var nameArr = itemData.f_name.split(" ");
                    if (nameArr.length > 1) {
                        domItem.find('.prefix-val').html(nameArr[0].substr(0, 1).toUpperCase() + nameArr[1].substr(0, 1).toUpperCase());
                    }
                    else {
                        domItem.find('.prefix-val').html(itemData.f_name.substr(0, 2).toUpperCase());
                    }
                    if (parseInt(itemData.isTyping) === 1) {
                        domItem.addClass('typing-true');
                    }
                    else {
                        domItem.removeClass('typing-true');
                    }
                }
                this.set = function (sets) {
                    Object.assign(itemData, sets);
                    bindData();
                }
                this.init = function () {
                    domItem = domUsersList.find('.user-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUsersList);
                    bindData();
                    bindEvents();
                }
            }

            this.updateMe = function (sets) {
                databaseUsersRef.child(CLIENT_ID).update(sets);
            }
            this.afterLoad = function () {
                return loadResolve;
            }
            this.init = function () {
                domUsersPane = domRoot.find('.users-pane');
                domUsersList = domUsersPane.find('.users-list');
                databaseUsersRef = firebase.database().ref('series-conversations/' + series + '/users');
                bindData();
                bindEvents();
            }
        }

        this.init = function () {
            options = options ? options : {};
            if (options.css) {
                domRoot.css(options.css);
            }
            if (parseInt(domRoot.css('width')) >= 700) {
                widthSize = 'lg';
                domRoot.addClass('width-large');
            }
            else if (parseInt(domRoot.css('width')) <= 400) {
                widthSize = 'sm';
                domRoot.addClass('width-small');
            }
            if (!firebase.apps.length) {
                firebase.initializeApp(FireBaseConfig);
            }
            domConversationPane = domRoot.find('.conversation-pane');
            domAsideLinks = domRoot.find('.tab-links-container');
            usersHandle = new UsersClass();
            usersHandle.init();
            usersHandle.afterLoad().then(function () {
                publicConversationHandle = new PublicConversationClass();
                publicConversationHandle.init();

                privateConversationHandle = new PrivateConversationClass();
                privateConversationHandle.init();
            });
        }
    }
})();