var PayBlogClass = null;
var seryStripeHandler = null;
(function () {
    PayBlogClass = function (domRoot, data, stripeCardModalHandle) {
        var self = this;
        var fnAfterPaid = [];
        var popupErroJoin = function () {
            swal("Sorry, Something went wrong!", {
                content: 'please try again later'
            });
        }
        var popupNotLogin = function() {
            swal("You should login first");
        }
        var popupThanksJoin = function (pData) {
            swal({
                icon: "success",
                title: 'SUCCESS!',
                text: 'Thanks for joining! Would you like to go to your experiences?',
                buttons: {
                    returnHome: {
                        text: "Go to my Experiences",
                        value: 'return_home',
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    customize: {
                        text: "Enjoy View",
                        value: 'keep_here',
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                closeOnClickOutside: false,
            }).then(function (value) {
                if (value == 'return_home'){
                }
                else {
                }
            });
        }
        var openStripeCardModal = function () {
            stripeCardModalHandle.open();
            stripeCardModalHandle.afterJoin(function (res) {
                if (res.status == true) {
                    fnAfterPaid.forEach(function (fn) {
                        fn(res.data);
                    });
                }
            });
        }
        var bindEvents = function () {
            if (domRoot) {
                domRoot.find('a.join-now').click(function () {
                    if (CLIENT_ID === -1) {
                        authHandle.activeLogin();
                        authHandle.popup();
                        authHandle.afterLogin(function () {
                            window.location.reload();
                        });
                    }
                    else {
                        if (stripeCheckoutHandle) {
                            stripeCheckoutHandle.open(data.series_ID);
                        }
                    }
                });
            }
        }
        this.onPaid = function (fn) {
            fnAfterPaid.push(fn);
        }
        this.join = function (token) {
            ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: data.series_ID, token: token}).then(function (res) {
                if (res.status === true){
                    data.purchased = res.data;
                    fnAfterPaid.forEach(function (fn) {
                        fn(data.purchased);
                    });
                    popupThanksJoin(data.purchased);
                }
            });
        }
        this.init = function () {
            bindEvents();
        }
    }
})();
