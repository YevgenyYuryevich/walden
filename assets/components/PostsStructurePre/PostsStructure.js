var PostsStructureClass;

(function () {
    'use structure';

    PostsStructureClass = function (domRoot, series) {

        var structureListHandle, structureGridHandle, postCreatorHandle;
        var currentTab = 'list';
        var self = this;

        var bindEvents = function () {
            domRoot.find('[href="#structure-grid-tab"]').on('shown.bs.tab', function () {
                currentTab = 'grid';
                domRoot.removeClass('list-structure-mode');
                domRoot.addClass('grid-structure-mode');
            });
            domRoot.find('[href="#structure-list-tab"]').on('shown.bs.tab', function () {
                currentTab = 'list';
                domRoot.removeClass('grid-structure-mode');
                domRoot.addClass('list-structure-mode');
            });
            domRoot.find('.add-item-btn').click(function () {
                postCreatorHandle.clean();
                var type = $(this).data('type');
                switch (type) {
                    case 'fork':
                        postCreatorHandle.setType('menu');
                        break;
                    case 'form':
                        postCreatorHandle.setType(10);
                        break;
                    case 'skip-point':
                        postCreatorHandle.setType(9);
                        break;
                }
                postCreatorHandle.open();
            });
            postCreatorHandle.afterSave(function (nData) {
                var vData = helperHandle.postToViewFormat(nData);
                self.add(vData);
            });
        }

        var StructureListClass = function () {
            var domStructureList, domPostsList;
            var self = this;
            var bindEvents = function () {
                domPostsList.on('sortupdate', function () {
                    self.saveOrders();
                });
            }
            var PostRowClass = function (postData) {

                var domRow, domInnerWrp;
                var self = this, parentHandle = false, originItemHandle, gridItemHandle;
                var isRemoved = false, isAdded = false;

                var bindEvents = function () {
                    domInnerWrp.find('.delete-action').click(function () {
                        swal({
                            title: "Are you sure to delete?",
                            icon: "warning",
                            dangerMode: true,
                            buttons: true,
                        }).then(function(willDelete){
                            if (willDelete){
                                self.delete().then(function () {
                                    if (originItemHandle) {
                                        originItemHandle.deSelect();
                                    }
                                    gridItemHandle.remove();
                                });
                            }
                        });
                    });
                }

                var bindData = function () {
                    domInnerWrp.find('.post-title').html(postData.title);
                    domInnerWrp.find('.item-img').attr('src', postData.image);
                    domInnerWrp.addClass('type-' + postData.type);
                    domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-post');
                }
                this.setId = function (id) {
                    postData.id = id;
                }
                this.sets = function (sets) {
                    Object.assign(postData, sets);
                }
                this.delete = function () {
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: postData.id}).then(function (res) {
                        self.remove();
                        return res.status;
                    })
                }
                this.setOriginHandle = function (hdl) {
                    originItemHandle = hdl;
                }
                this.setGridHandle = function (hdl) {
                    gridItemHandle = hdl;
                }
                this.isRemoved = function () {
                    return isRemoved;
                }
                this.isAdded = function () {
                    return postData.id ? true : false;
                }
                this.setAdded = function (v) {
                    isAdded = v;
                }
                this.getDomPosition = function () {
                    return domRow.index();
                }
                this.remove = function () {
                    isRemoved = true;
                    domRow.remove();
                }
                this.appendTo = function (domList) {
                    if (!domList) {
                        if (parentHandle) {
                            domList = parentHandle.getDom().find('> ul');
                        }
                        else {
                            domList = domPostsList;
                        }

                    }
                    domRow.appendTo(domList);
                }
                this.data = function () {
                    return postData;
                }
                this.init = function () {
                    domRow = domStructureList.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                    domInnerWrp = domRow.find('> .post-row-inner');
                    bindData();
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }
            this.saveOrders = function () {
                var itemsArranged = [];
                var orders = [];

                domPostsList.find('>.post-row').each(function () {
                    var hdl = $(this).data('controlHandle');
                    orders.push(hdl.data().order);
                    itemsArranged.push(hdl);
                });
                orders.sort(function (a, b) {
                    return a - b;
                });
                var updates = [];
                itemsArranged.forEach(function (value, i) {
                    updates.push({where: value.data().id, sets: {intPost_order: orders[i]}});
                    value.sets({order: orders[i]});
                });
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'update_many', updates: updates}).then(function (res) {
                    return res;
                });
            }
            this.save = function () {
                var items = self.getItems();
                var nItems = [];
                var fItems = [];
                items.forEach(function (value) {
                    if (!value.isAdded()) {
                        nItems.push(value);
                        value.setAdded(true);
                        var f = helperHandle.postFormat(value.data());
                        f.intPost_series_ID = series;
                        fItems.push(f);
                    }
                });
                if (fItems.length) {
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: fItems}).then(function (res) {
                        for (var i = 0; i < nItems.length; i++) {
                            nItems[i].sets({id: res.data[i].post_ID, order: res.data[i].intPost_order});
                        }
                        return res;
                    });
                }
                else {
                    return Promise.resolve(false);
                }
            }
            this.makeSortable = function () {
                domPostsList.sortable({
                    placeholder: 'ui-state-highlight',
                    items: 'li',
                    coneHelperSize: true,
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    revert: 300,
                    opacity: 0.9,
                    update: function (a, b) {}
                });
            }
            this.add = function (post) {
                var hdl = new PostRowClass(post);
                hdl.init();
                hdl.appendTo();
                return hdl;
            }
            this.getItems = function () {
                var rtn = [];
                domPostsList.find('>.post-row').each(function (i) {
                    rtn.push($(this).data('controlHandle'));
                });
                return rtn;
            }
            this.init = function () {
                domStructureList = domRoot.find('#structure-list-tab');
                domPostsList = domStructureList.find('.posts-list');
                self.makeSortable();
                bindEvents();
            }
        }

        var StructureGridClass = function () {
            var domStructureGrid, domPostsList;

            var itemHandles = [], self = this;

            var bindEvents = function () {
                domPostsList.on('sortupdate', function () {
                    self.saveOrders();
                });
            }

            var PostItemClass = function (itemData) {

                var domItem, domBody, domTypeBody;
                var dataLoaded = false, isRemoved = false, isAdded = false;

                var self = this, searchItemHandle, originItemHandle, listItemHandle;

                var remove = function () {
                    isRemoved = true;
                    domItem.remove();
                }
                this.setSearchHandle = function (hdl) {
                    searchItemHandle = hdl;
                }
                this.setOriginHandle = function (hdl) {
                    originItemHandle = hdl;
                }
                this.setListHandle = function (hdl) {
                    listItemHandle = hdl;
                }
                this.isRemoved = function () {
                    return isRemoved;
                }
                this.setAdded = function (v) {
                    isAdded = v;
                }
                this.isAdded = function () {
                    return itemData.id ? true : false;
                }
                this.setId = function (id) {
                    itemData.id = id;
                }
                this.sets = function (sets) {
                    Object.assign(itemData, sets);
                }
                var bindData = function () {
                    dataLoaded = true;
                    domItem.addClass('type-' + itemData.type);
                    switch (parseInt(itemData.type)) {
                        case 0:
                            domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            break;
                        case 2:
                            domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.find('img').attr('src', itemData.image);
                            break;
                        default:
                            domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.html(itemData.body);
                            break;
                    }
                    domItem.find('.item-title').html(itemData.title);
                    if (itemData.duration) {
                        domBody.find('.blog-duration').html(itemData.duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            domBody.find('.blog-duration').html(res);
                            itemData.duration = res;
                        });
                    }
                }
                var bindEvents = function () {
                    domItem.find('.item-action.delete-action').click(function (e) {
                        swal({
                            title: "Are you sure to delete?",
                            icon: "warning",
                            dangerMode: true,
                            buttons: true
                        }).then(function(willDelete){
                            if (willDelete){
                                self.delete().then(function () {
                                    listItemHandle.remove();
                                    if (originItemHandle) {
                                        originItemHandle.deSelect();
                                    }
                                });
                            }
                        });
                    });
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domPostsList);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = bindData;
                this.remove = remove;
                this.isDataLoaded = function () {
                    return dataLoaded;
                }
                this.delete = function () {
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: itemData.id}).then(function (res) {
                        remove();
                        return res.status;
                    })
                }
                this.init = function () {
                    domItem = domStructureGrid.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    domBody = domItem.find('.item-type-body');
                    domItem.data('controlHandle', self);
                    bindEvents();
                }
            }

            this.saveOrders = function () {
                var itemsArranged = [];
                var orders = [];

                domPostsList.find('>.post-item').each(function () {
                    var hdl = $(this).data('controlHandle');
                    orders.push(hdl.data().order);
                    itemsArranged.push(hdl);
                });
                orders.sort(function (a, b) {
                    return a - b;
                });
                var updates = [];
                itemsArranged.forEach(function (value, i) {
                    updates.push({where: value.data().id, sets: {intPost_order: orders[i]}});
                    value.sets({order: orders[i]});
                });
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'update_many', updates: updates}).then(function (res) {
                    return res;
                });
            }
            this.getItems = function () {
                var rtn = [];
                domPostsList.find('>.post-item').each(function (i) {
                    rtn.push($(this).data('controlHandle'));
                });
                return rtn;
            }
            this.save = function () {
                var items = self.getItems();
                var fItems = [];
                var nItems = [];
                items.forEach(function (value) {
                    if (!value.isAdded()) {
                        nItems.push(value);
                        value.setAdded(true);
                        var f = helperHandle.postFormat(value.data());
                        f.intPost_series_ID = series;
                        fItems.push(f);
                    }
                });
                if (fItems.length) {
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: fItems}).then(function (res) {
                        for (var i = 0; i < nItems.length; i++) {
                            nItems[i].sets({id: res.data[i].post_ID, order: res.data[i].intPost_order});
                        }
                        return res;
                    });
                }
                else {
                    return Promise.resolve(false);
                }
            }
            this.add = function (post) {
                var hdl = new PostItemClass(post);
                hdl.init();
                hdl.bindData();
                hdl.append();
                itemHandles.push(hdl);
                return hdl;
            }
            this.init = function () {
                domStructureGrid = domRoot.find('#structure-grid-tab');
                domPostsList = domStructureGrid.find('.items-container');
                domPostsList.sortable({
                    placeholder: 'ui-state-highlight',
                    items: 'article.post-item',
                    coneHelperSize: true,
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    revert: 300, // animation in milliseconds
                    // handle: '.img-wrapper',
                    opacity: 0.9,
                    update: function (a, b) {}
                });
                bindEvents();
            }
        }

        this.add = function (post) {
            var l = structureListHandle.add(post);
            var g = structureGridHandle.add(post);
            l.setGridHandle(g);
            g.setListHandle(l);
            return {
                list: l,
                grid: g
            };
        }
        this.save = function () {
            if (currentTab == 'list') {
                return structureListHandle.save();
            }
            else {
                return structureGridHandle.save();
            }
        }
        this.emailAddPosts = function () {
            var hdls = structureListHandle.getItems();
            var posts = [];
            hdls.forEach(function (value) {
                posts.push(value.data().id);
            });
            return ajaxAPiHandle.apiPost('Series.php', {action: 'email_add_posts', series: series, posts: posts}, false).then(function () {
                swal('All owners and users joined will receive email, Thanks!');
                return true;
            });
        }
        this.getItems = function () {
            return structureListHandle.getItems();
        }
        this.init = function () {

            structureListHandle = new StructureListClass();
            structureListHandle.init();

            structureGridHandle = new StructureGridClass();
            structureGridHandle.init();

            postCreatorHandle = new PostCreatorClass(domRoot.find('.post-creator-component'), {series_ID: series});
            postCreatorHandle.init();
            bindEvents();
        }
    }
})();