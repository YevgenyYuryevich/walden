<div class="component posts-structure-component list-structure-mode">
    <header class="structure-header d-xs-none">
        <div class="flex-row space-between flex-wrap vertical-center">
            <div class="flex-col fix-col mr-auto">
                <div class="drag-title-wrapper">
                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/move.png" alt=""> <span class="drag-title-txt">Drag on items to change the order</span>
                </div>
            </div>
            <div class="flex-col fix-col mr-1">
                <button class="add-item-btn site-green-btn" data-type="fork">Add fork</button>
            </div>
            <div class="flex-col fix-col mr-1">
                <button class="add-item-btn site-green-btn" data-type="skip-point">Add skip-point</button>
            </div>
            <div class="flex-col fix-col mr-2">
                <button class="add-item-btn site-green-btn" data-type="form">Add form</button>
            </div>
            <div class="flex-col fix-col">
                <div class="structure-type-select-wrapper">
                    <a href="#structure-grid-tab" data-toggle="tab" class="show-on-list-structure">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"></path>
                                                    <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"></path>
                                                    <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"></path>
                                                    <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"></path>
                                                </g>
                                            </g>
                                        </svg>
                    </a>
                    <a href="#structure-list-tab" data-toggle="tab" class="show-on-grid-structure">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                                    c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                                <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                                    h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                                <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                                    h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                            </g>
                            <g>
                                <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                                <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                                <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                            </g>
                                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div class="structure-body">
        <div class="tab-content">
            <div class="tab-pane fade in active" id="structure-list-tab">
                <div class="posts-container">
                    <li class="sample post-row" hidden>
                        <div class="post-row-inner">
                            <div class="flex-row space-between margin-between">
                                <div class="flex-col fix-col">
                                    <a class="item-action delete-action" href="javascript:;">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                    </a>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="item-img-wrapper">
                                        <img class="item-img">
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="item-type-wrapper">
                                        <div class="item-type-icon type-post-icon show-on-post-node"></div>
                                        <div class="item-type-icon type-post-icon show-on-menu-node"></div>
                                        <div class="item-type-icon type-post-icon show-on-path-node"></div>
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="media-icon-wrapper">
                                        <i class="fa fa-youtube-play video-icon"></i>
                                        <i class="fa fa-volume-up audio-icon"></i>
                                        <i class="fa fa-file-text text-icon"></i>
                                    </div>
                                </div>
                                <div class="flex-col">
                                    <div class="post-title"></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <ul class="posts-list">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade" id="structure-grid-tab">
                <article class="post-item sample" hidden>
                    <header class="item-header">
                        <div class="flex-row vertical-center margin-between">
                            <div class="flex-col fix-col">
                                <div class="media-icon-wrapper">
                                    <i class="fa fa-youtube-play video-icon"></i>
                                    <i class="fa fa-volume-up audio-icon"></i>
                                    <i class="fa fa-file-text text-icon"></i>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="day-wrapper">
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <a class="item-action delete-action" href="javascript:;">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                                        286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
                                                                    <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                                        C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                                        S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
                                                                </g>
                                                            </g>
                                                        </svg>
                                </a>
                            </div>
                        </div>
                    </header>
                    <div class="item-type-body">
                        <div class="blog-type type-0 sample" hidden>
                        </div>
                        <div class="blog-type type-2 sample" hidden>
                            <img alt="">
                        </div>
                        <div class="blog-type type-default sample" hidden>
                            Lorem lpsum is simply dummy text of the printing and type
                        </div>
                        <aside class="blog-duration">
                            waiting...
                        </aside>
                    </div>
                    <div class="item-title">Why gifted student needs to be taught formal writing</div>
                </article>
                <div class="items-container">
                </div>
            </div>
        </div>
    </div>
    <?php require_once ASSETS_PATH . '/components/PostCreator/PostCreator.php';?>
</div>