<div class="component view-post-popup-widget maximize-view">
    <div class="widget-inner">
        <header class="widget-header view-only-min">
            <div class="flex-row space-between margin-between vertical-center">
                <div class="flex-col">
                    <div class="post-title"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_max"></div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_close"></div>
                    </div>
                </div>
            </div>
        </header>
        <aside class="widget-top-control view-only-max">
            <div class="mv_top_button mv_top_close">
                <div class="mv_close_icon"></div>
            </div>
            <div class="mv_top_button mv_top_minimize">
                <div class="mv_minimize_icon"></div>
            </div>
        </aside>
        <div class="widget-body">
            <div class="video-widget sample" hidden>
                <video controls class="video-js" data-autoresize="fit" width="960" height="540"></video>
            </div>
            <div class="audio-widget sample" hidden>
                <div class="audiogallery">
                    <div class="items">
                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                    </div>
                </div>
            </div>
            <div class="image-widget sample" hidden></div>
            <div class="text-widget sample" hidden></div>
            <div class="form-widget sample" hidden></div>
            <div class="pay-widget sample" hidden>
                <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
            </div>
        </div>
        <footer class="widget-footer view-only-max">
            <div class="post-title"></div>
        </footer>
    </div>
</div>