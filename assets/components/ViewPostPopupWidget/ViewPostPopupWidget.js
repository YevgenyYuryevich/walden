var ViewPostPopupWidgetClass = null;

(function () {
    'use strict';
    ViewPostPopupWidgetClass = function (domViewWidget) {
        var domUsedBody, domHeader, domBodyWrp, domFooter, domTopCtrl;
        var postData, metaData;
        var audioCnt;

        var videoHandle;

        var bindEvents = function () {
            domHeader.find('.mv_min_control_max, .post-title').parent().click(function () {
                maximize();
            });
            domHeader.find('.mv_min_control_close').parent().click(function () {
                close();
            });
            domTopCtrl.find('.mv_minimize_icon').parent().click(function () {
                minimize();
            });
            domTopCtrl.find('.mv_close_icon').parent().click(function () {
                close();
            });
            domViewWidget.click(function (e) {
                if (e.target !== this) return;
                close();
            });
        };

        var close = function () {
            destroyBody();
            domViewWidget.removeClass('active');
            $('body').css('padding-right', '0px');
            $('body').css('overflow', 'auto');
        }

        var destroyBody = function () {
            if (!domUsedBody || !domUsedBody.length) {
                return false;
            }
            switch (parseInt(postData.type)) {
                case 0:
                    domViewWidget.removeClass('audio-type');
                    break;
                case 2:
                    videoHandle.dispose();
                    break;
                case 8:
                    domViewWidget.removeClass('image-type');
                    break;
                default:
                    domViewWidget.removeClass('text-type');
                    break;
            }
            domUsedBody.remove();
            return true;
        }

        var maximize = function () {
            domViewWidget.addClass('maximize-view');
            $('body').css('padding-right', window.innerWidth - $("body").prop("clientWidth") + 'px');
            $('body').css('overflow', 'hidden');
        }

        var minimize = function () {
            $('body').css('overflow', 'auto');
            $('body').css('padding-right', '0px');
            domViewWidget.removeClass('maximize-view');
        }

        this.view = function (newPostData) {
            if (domViewWidget.hasClass('active')){
                destroyBody();
            }
            domViewWidget.addClass('active');
            postData = newPostData;
            domHeader.find('.post-title').html(postData.title);
            domFooter.find('.post-title').html(postData.title);
            if (domViewWidget.hasClass('maximize-view')){
                maximize();
            }
            switch (parseInt(postData.type)) {
                case 0:
                    domViewWidget.addClass('audio-type');
                    domUsedBody = domViewWidget.find('.audio-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                    domUsedBody.find('.audioplayer-tobe').attr('data-thumb', helperHandle.makeAbsUrl(postData.image));
                    domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                    domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);

                    var settings_ap = {
                        disable_volume: 'off',
                        disable_scrub: 'default',
                        design_skin: 'skin-wave',
                        skinwave_dynamicwaves: 'on',
                        skinwave_mode: 'alternate',
                    };
                    dzsag_init('#audio-' + audioCnt, {
                        'transition':'fade',
                        'autoplay' : 'on',
                        'settings_ap': settings_ap
                    });
                    audioCnt++;
                    break;
                case 2:
                    domViewWidget.addClass('video-type');
                    domUsedBody = domViewWidget.find('.video-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                    var domVideo = domUsedBody.find('video');
                    if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                        var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                        domVideo.attr('data-setup', JSON.stringify(setup));
                    }
                    else {
                        domVideo.append('<source src="'+ postData.body +'" />');
                    }
                    videoHandle = videojs(domVideo.get(0), {
                        width: domUsedBody.innerWidth(),
                        height: domUsedBody.innerWidth() * 540 / 960,
                    });
                    break;
                case 8:
                    domViewWidget.addClass('image-type');
                    domUsedBody = domViewWidget.find('.image-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                    domUsedBody.append(postData.body);
                    break;
                case 10:
                    domViewWidget.addClass('form-type');
                    domUsedBody = domViewWidget.find('.form-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                    domUsedBody.append(postData.body);
                    domUsedBody.find('[data-form-field]').each(function (i, d) {
                        var field = {};
                        postData.formFields.forEach(function (formField) {
                            if (parseInt(formField.formField_ID) === parseInt($(d).attr('data-form-field'))) {
                                field = formField;
                            }
                        });
                        postData.formFieldAnswers.forEach(function (formFieldAnswer) {
                            if (parseInt(formFieldAnswer.intFormFieldAnswer_field_ID) === parseInt(field.formField_ID)) {
                                field.answer = formFieldAnswer;
                            }
                        });
                        var hdl = new FormFieldClass($(d), field);
                        hdl.init();
                    });
                    break;
                default:
                    domViewWidget.addClass('text-type');
                    domUsedBody = domViewWidget.find('.text-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                    domUsedBody.append(postData.body);
                    break;
            }
        }
        this.init = function () {
            domHeader = domViewWidget.find('header.widget-header');
            domBodyWrp = domViewWidget.find('.widget-body');
            domFooter = domViewWidget.find('footer.widget-footer');
            domTopCtrl = domViewWidget.find('aside.widget-top-control');
            metaData = {};
            bindEvents();
        }
    }

})();