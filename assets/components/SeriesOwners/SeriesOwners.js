var SeriesOwnersClass;
if (typeof stripeApi_pKey === 'undefined') {
    var stripeApi_pKey;
}

(function () {
    'use strict';
    SeriesOwnersClass = function (domRoot, series, options) {

        var domSingleOwner, domMultiOwners, domOwnersList;
        var itemHandles = [];

        var bindData = function () {
            ajaxAPiHandle.apiPost('Series.php', {action: 'get_owners', id: series.series_ID}, false).then(function (res) {
                if (res.data.length === 1) {
                    var o = res.data[0];
                    var hdl = new OwnerItemClass(o, true);
                    hdl.init();
                    domRoot.addClass('owner-single');
                }
                else {
                    domRoot.addClass('owner-multi');
                    res.data.forEach(function (itemData) {
                        var hdl = new OwnerItemClass(itemData);
                        hdl.init();
                        itemHandles.push(hdl);
                    });
                }
            });
        }

        var OwnerItemClass = function (itemData, singular) {
            var domItem, domItemInner;
            var self = this, detailsHandle;
            var bindEvents = function () {
                domItemInner.click(function () {
                    itemHandles.forEach(function (value) {
                        value.setActive(false);
                    });
                    self.setActive(true);
                });
            }
            var bindData = function () {
                if (itemData.image) {
                    domItemInner.find('.item-img').attr('src', itemData.image);
                }
                else {
                }
                domItemInner.find('.item-name').html(itemData.f_name);
            }
            var OwnerItemDetailsClass = function () {
                var domDetails, domDetailsList, domOwnerProfile;

                var itemHandles = [];

                var bindData = function () {
                    domDetails.find('.owner-name').html(itemData.f_name);
                    if (itemData.about_me) {
                        domOwnerProfile.find('.owner-description').html(itemData.about_me);
                    }
                    domDetails.find('.view-profile').attr('href', 'user_profile?id=' + itemData.id);
                    if (itemData.social_facebook) {
                        domOwnerProfile.find('.item-facebook').attr('href', itemData.social_facebook);
                    } else {
                        domOwnerProfile.find('.item-facebook').removeAttr('target');
                    }
                    if (itemData.social_twitter) {
                        domOwnerProfile.find('.item-twitter').attr('href', itemData.social_twitter);
                    } else {
                        domOwnerProfile.find('.item-twitter').removeAttr('target');
                    }
                    if (itemData.social_pinterest) {
                        domOwnerProfile.find('.item-pinterest').attr('href', itemData.social_pinterest);
                    } else {
                        domOwnerProfile.find('.item-pinterest').removeAttr('target');
                    }
                    if (itemData.social_linkedin) {
                        domOwnerProfile.find('.item-linkedin').attr('href', itemData.social_linkedin);
                    } else {
                        domOwnerProfile.find('.item-linkedin').removeAttr('target');
                    }
                    if (itemData.series.length == 0) {
                        domDetails.addClass('no-series');
                    }
                    itemData.series.forEach(function (sItem) {
                        var hdl = new SeriesItemClass(sItem);
                        hdl.init();
                    });
                }

                var bindEvents = function () {
                    $(document).mouseup(function(e)
                    {
                        var container = domDetails;

                        // if the target of the click isn't the container nor a descendant of the container
                        if (!container.is(e.target) && container.has(e.target).length === 0)
                        {
                            domDetails.hide();
                        }
                    });
                }

                var SeriesItemClass = function (seriesItemData) {
                    var domItem;
                    var self = this, payHandle = null;

                    var viewUserPage = function (data) {
                        $('<form action="my_solutions" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
                    }
                    var popupThanksJoin = function (data) {
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Thanks for joining! Would you like to go to your experiences?',
                            buttons: {
                                returnHome: {
                                    text: "Go to my Experiences",
                                    value: 'return_home',
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                customize: {
                                    text: "Not yet",
                                    value: 'customize',
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            },
                            closeOnClickOutside: false,
                        }).then(function (value) {
                            if (value == 'return_home'){
                                viewUserPage(data);
                            }
                            else {
                            }
                        });
                        var domSwalRoot, domDisableCheckbox;
                        var disablePopup = function () {
                            localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
                        }
                        var enabelPopup = function () {
                            localStorage.removeItem('thegreyshirt_show_popup_every_join');
                        }
                        var bindEvents = function () {
                            domDisableCheckbox.change(function () {
                                $(this).prop('checked') ? disablePopup() : enabelPopup();
                            })
                        }
                        var init = function () {
                            setTimeout(function () {
                                domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                                domSwalRoot.addClass('thanks-join');
                                domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                                domDisableCheckbox = domSwalRoot.find('#disable_popup');
                                bindEvents();
                            }, 100);
                        }();
                    }
                    var popupErroJoin = function () {
                        swal("Sorry, Something went wrong!", {
                            content: 'please try again later'
                        });
                    }

                    var openStripe = function () {
                        if (payHandle) {
                            payHandle.open({
                                name: seriesItemData.strSeries_title,
                                description: seriesItemData.strSeries_description,
                                currency: 'usd',
                                amount: parseFloat((seriesItemData.intSeries_price * 100).toFixed(2))
                            });
                        }
                        else {
                            popupErroJoin();
                        }
                    }

                    var join = function (token) {
                        ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: seriesItemData.series_ID, token: token}).then(function (res) {
                            if (res.status == true){
                                seriesItemData.purchased = res.data;
                                domItem.addClass('purchased');
                            }
                        });
                    }

                    var unJoin = function () {
                        swal("You already joined this series!", {
                            content: 'Thanks!'
                        });
                        return false;

                        ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: seriesItemData.purchased}).then(function (res) {
                            if (res.status == true){
                                seriesItemData.purchased = 0;
                            }
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.join-btn').click(function () {
                            if (CLIENT_ID > -1){
                                if (parseInt(seriesItemData.boolSeries_charge) === 1 && parseInt(seriesItemData.purchased) == 0) {
                                    openStripe();
                                }
                                else {
                                    parseInt(seriesItemData.purchased) ? unJoin() : join();
                                }
                            }
                            else{
                                swal({
                                    title: "Please Login first!",
                                    icon: "warning",
                                    dangerMode: true,
                                    buttons: {
                                        cancel: "Cancel",
                                        login: "Login"
                                    },
                                }).then(function(value){
                                    if (value == 'login'){
                                        window.location.href = 'login';
                                    }
                                });
                            }
                        });
                    }

                    this.isFiltered = function () {
                        var filterData = filterHandle.data().filter;
                        if (filterData.free || filterData.premium || filterData.affiliate) {
                            if (filterData.free && !parseInt(seriesItemData.boolSeries_charge)) {
                                return true;
                            }
                            if (filterData.premium && parseInt(seriesItemData.boolSeries_charge)) {
                                return true;
                            }
                            if (filterData.affiliate && parseInt(seriesItemData.boolSeries_affiliated)) {
                                return true;
                            }
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    this.data = function () {
                        return seriesItemData;
                    }
                    this.append = function () {
                        domItem.appendTo(domDetailsList);
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.bindData = function () {
                        if (seriesItemData.purchased) {
                            domItem.addClass('purchased');
                        }
                        domItem.find('.item-image').css('background-image', 'url(' + seriesItemData.strSeries_image + ')');
                        domItem.find('.child-count span').text(seriesItemData.posts_count);
                        domItem.find('.item-title').text(seriesItemData.strSeries_title);
                        domItem.find('.preview-btn').attr('href', 'preview_series?id=' + seriesItemData.series_ID);
                        domItem.css('order', itemHandles.length + 1);
                    }
                    this.setOrder = function (order) {
                        domItem.css('order', order);
                    }
                    this.init = function () {
                        domItem = domDetailsList.find('.series-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domDetailsList);
                        self.bindData();
                        if (seriesItemData.boolSeries_charge == 1 && seriesItemData.purchased == 0) {
                            if (!stripeApi_pKey) {
                                ajaxAPiHandle.apiPost('Options.php', {action: 'get', name: 'stripeApi_pKey'}, false).then(function (res) {
                                    stripeApi_pKey = res.data;
                                    if (stripeApi_pKey && seriesItemData.stripe_account_id) {
                                        payHandle = StripeCheckout.configure({
                                            key: stripeApi_pKey,
                                            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                            locale: 'auto',
                                            token: function(token) {
                                                join(token);
                                            }
                                        });
                                    }
                                });
                            }
                            else if (seriesItemData.stripe_account_id) {
                                payHandle = StripeCheckout.configure({
                                    key: stripeApi_pKey,
                                    image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                    locale: 'auto',
                                    token: function(token) {
                                        join(token);
                                    }
                                });
                            }
                        }
                        bindEvents();
                    }
                }

                this.setPosition = function () {
                    var top = domItem.offset().top - options.scrollParent.scrollTop();
                    var left = domItem.offset().left;
                    domDetails.css('top', top - 40);
                    domDetails.css('left', left + domItem.outerWidth());
                }
                this.show = function () {
                    domDetails.show();
                }
                this.hide = function () {
                    domDetails.hide();
                }
                this.init = function () {
                    domDetails = domRoot.find('.owner-item-details.sample').clone().removeClass('sample').appendTo(options.detailsParent);
                    domOwnerProfile = domDetails.find('.owner-detail');
                    domDetailsList = domDetails.find('.details-list');
                    bindData();
                    bindEvents();
                }
            }
            this.setActive = function (v) {
                if (v) {
                    detailsHandle.setPosition();
                    detailsHandle.show();
                    domItem.addClass('active-item');
                }
                else {
                    detailsHandle.hide();
                    domItem.removeClass('active-item');
                }
            }
            this.init = function () {
                if (singular) {
                    domItem = domSingleOwner;
                }
                else {
                    domItem = domOwnersList.find('.owner-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domOwnersList);
                }
                domItemInner = domItem.find('.owner-item-inner');
                detailsHandle = new OwnerItemDetailsClass();
                detailsHandle.init();
                bindData();
                bindEvents();
            }
        }

        this.init = function () {
            domSingleOwner = domRoot.find('.single-owner-wrapper');
            domMultiOwners = domRoot.find('.multi-owners-wrapper');
            domOwnersList = domMultiOwners.find('.owners-list');
            options = options ? options : {};
            options = Object.assign({
                detailsParent: domRoot,
                scrollParent: $(window),
            }, options);
            bindData();
        }
    }
})();