var FormLoopQuestionClass;

(function () {
    'use strict';
    FormLoopQuestionClass = function (domRoot, formLoopData) {

        var domLoopFieldList, domFooter;
        var loopFieldHandles = [];
        var currentAnswerSeek = 0;
        var domChangeCallbacks = [];

        var LoopFieldClass = function (fieldData) {
            var domField, domValue;
            var self = this;
            var answer;
            this.bindData = function () {
                domField.attr('data-loop-field', fieldData.formField_ID);
                domField.removeClass('type-text type-checkbox').addClass('type-' + fieldData.strFormField_type);
                domField.find('.loop-field-name').html(fieldData.strFormField_name);
                domField.find('.loop-field-helper').attr('data-content', fieldData.strFormField_helper);
                if (fieldData.strFormField_type == 'checkbox') {
                    domField.attr('contenteditable', false);
                }
                answer = fieldData.answers.find(function (answer) {
                    return parseInt(answer.intFormFieldAnswer_index) === currentAnswerSeek;
                });
                if (answer) {
                    if (fieldData.strFormField_type === 'text') {
                        domField.find('.type-value.text-value').html(answer.strFormFieldAnswer_answer);
                    } else {
                        domField.find('.type-value.checkbox-value').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                    }
                } else {
                    if (fieldData.strFormField_type === 'text') {
                        domField.find('.type-value.text-value').html('');
                    } else {
                        domField.find('.type-value.checkbox-value').prop('checked', false);
                    }
                }
            }
            var bindEvents = function () {
                switch (fieldData.strFormField_type) {
                    case 'text':
                        domValue.blur(function () {
                            updateAnswer();
                        });
                        break;
                    case 'checkbox':
                        domValue.change(function () {
                            updateAnswer();
                        });
                        break;
                }
            }
            var updateAnswer = function () {
                var sets = {
                    intFormFieldAnswer_index: currentAnswerSeek
                };
                domField.addClass('status-loading');
                switch (fieldData.strFormField_type) {
                    case 'text':
                        sets.strFormFieldAnswer_answer = domValue.html();
                        break;
                    case 'checkbox':
                        sets.strFormFieldAnswer_answer = domValue.prop('checked') ? 1 : 0;
                        break;
                }
                if (answer) {
                    ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                        domField.removeClass('status-loading');
                        domField.addClass('status-loading-finished');
                        domField.one(animEndEventName, function () {
                            domField.removeClass('status-loading-finished');
                        });
                        answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                    });
                }
                else {
                    $.extend(sets, {intFormFieldAnswer_field_ID: fieldData.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                    ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                        domField.removeClass('status-loading');
                        domField.addClass('status-loading-finished');
                        answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                        domField.one(animEndEventName, function () {
                            domField.removeClass('status-loading-finished');
                        });
                        fieldData.answers.push(answer);
                    });
                }
            }
            this.init = function () {
                if (domLoopFieldList.find('[data-loop-field="' + fieldData.formField_ID + '"]').length) {
                    domField = domLoopFieldList.find('[data-loop-field="' + fieldData.formField_ID + '"]');
                } else {
                    domField = domLoopFieldList.find('.loop-field.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domLoopFieldList);
                }
                domValue = domField.find('.loop-field-value').find('.' + fieldData.strFormField_type + '-value');
                self.bindData();
                bindEvents();
                domField.find('[data-toggle="popover"]').popover();
            }
        }
        var bindData = function () {
            domRoot.attr('data-form-loop', formLoopData.formLoop_ID);
            domFooter.find('.add-another-btn').text(formLoopData.formLoop_another_text);
            if (currentAnswerSeek === 0) {
                domFooter.find('.prev-btn').attr('disabled', true);
            } else {
                domFooter.find('.prev-btn').removeAttr('disabled');
            }
            if (currentAnswerSeek + 1 === formLoopData.formLoop_rows) {
                domFooter.find('.next-btn').attr('disabled', true);
            } else {
                domFooter.find('.next-btn').removeAttr('disabled');
            }
            if (currentAnswerSeek + 1 < formLoopData.formLoop_rows) {
                domFooter.find('.add-another-btn').attr('disabled', true);
            } else {
                domFooter.find('.add-another-btn').removeAttr('disabled');
            }
        }
        var bindEvents = function () {
            domFooter.find('.add-another-btn').click(function () {
                addAnother().then(function (rows) {
                    format();
                    bindData();
                    loopFieldHandles.forEach(function (value) {
                        value.bindData();
                    });
                });
            });
            domFooter.find('.next-btn').click(function () {
                next();
            });
            domFooter.find('.prev-btn').click(function () {
                prev();
            });
        }
        var addAnother = function () {
            formLoopData.formLoop_rows = formLoopData.formLoop_rows + 1;
            currentAnswerSeek++;
            return ajaxAPiHandle.apiPost('FormLoop.php', {action: 'update', where: formLoopData.formLoop_ID, sets: {formLoop_rows: formLoopData.formLoop_rows}}).then(function (res) {
                domRoot.attr('current-answer-seek', currentAnswerSeek);
                domChangeCallbacks.forEach(function (value) {
                    value();
                });
                return formLoopData.formLoop_rows;
            });
        }
        var next = function () {
            currentAnswerSeek++;
            bindData();
            loopFieldHandles.forEach(function (value) {
                value.bindData();
            });
            domRoot.attr('current-answer-seek', currentAnswerSeek);
            domChangeCallbacks.forEach(function (value) {
                value();
            });
        }
        var prev = function () {
            currentAnswerSeek--;
            bindData();
            loopFieldHandles.forEach(function (value) {
                value.bindData();
            });
            domRoot.attr('current-answer-seek', currentAnswerSeek);
            domChangeCallbacks.forEach(function (value) {
                value();
            });
        }
        var format = function () {
            formLoopData.formLoop_rows = parseInt(formLoopData.formLoop_rows);
            formLoopData.formLoop_cols = parseInt(formLoopData.formLoop_cols);
        }
        this.afterChange = function (f) {
            domChangeCallbacks.push(f);
        }
        this.dom = function () {
            return domRoot;
        }
        this.init = function () {
            domLoopFieldList = domRoot.find('.loop-field-list');
            domFooter = domRoot.find('.component-footer');
            if (domRoot.attr('current-answer-seek')) {
                currentAnswerSeek = parseInt( domRoot.attr('current-answer-seek') );
            } else {
                domRoot.attr('current-answer-seek', currentAnswerSeek);
            }
            if (typeof formLoopData === 'object') {
                formLoopData.fields.sort(function (a, b) {
                    return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                });
                format();
                bindData();
                formLoopData.fields.forEach(function (fieldData) {
                    var hdl = new LoopFieldClass(fieldData);
                    hdl.init();
                    loopFieldHandles.push(hdl);
                });
                bindEvents();
            }
            else {
                ajaxAPiHandle.apiPost('FormLoop.php', {action: 'get_loop', where: formLoopData}, false).then(function (res) {
                    formLoopData = res.data;
                    if (formLoopData) {
                        formLoopData.fields.sort(function (a, b) {
                            return parseInt(a.intFormField_sort) - parseInt(b.intFormField_sort);
                        });
                        format();
                        bindData();
                        formLoopData.fields.forEach(function (fieldData) {
                            var hdl = new LoopFieldClass(fieldData);
                            hdl.init();
                            loopFieldHandles.push(hdl);
                        });
                        bindEvents();
                    }
                    else {
                        domRoot.remove();
                    }
                });
            }
        }
    }
})();