var AnsweredFormFieldClass = null;
(function () {
    'use strict';
    AnsweredFormFieldClass = function (domRoot, data) {
        var bindData = function () {
            if (data.answer) {
                domRoot.find('.form-field-name').remove();
                switch (data.strFormField_type) {
                    case 'text':
                        domRoot.html(data.answer);
                        break;
                    case 'checkbox':
                        if (domRoot.find('input').length === 0) {
                            domRoot.append('<input type="checkbox" readonly>');
                        }
                        domRoot.find('input').attr('checked', parseInt(data.answer) > 0 ? true : false);
                        break;
                }
            }
            else {
                switch (data.strFormField_type) {
                    case 'text':
                        domRoot.html(data.strFormField_default);
                        break;
                    case 'checkbox':
                        if (domRoot.find('input').length === 0) {
                            domRoot.append('<input type="checkbox" readonly>');
                        }
                        domRoot.find('input').attr('checked', parseInt(data.strFormField_default) > 0 ? true : false);
                        break;
                }
            }
        }
        this.dom = function () {
            return domRoot;
        }
        this.init = function () {
            domRoot.attr('data-answered-form-field', data.formField_ID);
            bindData();
        }
    }
})();