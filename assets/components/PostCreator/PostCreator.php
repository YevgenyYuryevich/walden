<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.01.2019
 * Time: 23:02
 */
?>

<div class="modal fade site-modal post-creator-component component" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <a href="javascript:;" class="modal-close" data-dismiss="modal"></a>
            <div class="modal-body">
                <div class="post-creator-content">
                    <div class="flex-row vertical-center space-between flex-wrap">
                        <div class="flex-col">
                            <h2 class="modal-title">Create Post</h2>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="select-type-wrapper">
                                <div class="flex-row vertical-center">
                                    <div class="flex-col fix-col mr-1">
                                        <label for="" class="control-label">Post Type:</label>
                                    </div>
                                    <div class="flex-col">
                                        <div class="dropdown" data-value="7">
                                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown">
                                                <span>Text</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:;" data-value="0">Audio</a></li>
                                                <li><a href="javascript:;" data-value="2">Video</a></li>
                                                <li hidden><a href="javascript:;" data-value="7">Text</a></li>
                                                <li><a href="javascript:;" data-value="8">Image</a></li>
                                                <li><a href="javascript:;" data-value="9">Switch-To</a></li>
                                                <li><a href="javascript:;" data-value="10">Form</a></li>
                                                <li><a href="javascript:;" data-value="form_loop">Form Loop</a></li>
                                                <li><a href="javascript:;" data-value="menu">Menu</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="post-creator-normal">
                            <div class="create-normal">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for="" class="control-label">Name your post:</label>
                                        <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                    </div>
                                    <div class="form-group post-body-group">
                                        <label for="" class="control-label">Describe it:</label>
                                        <textarea name="strPost_body" class="form-control" rows="3" required id="post-creator-normal-strPost_body"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">
                                            Money Saved, $ <span>(optional)</span>:
                                        </label>
                                        <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                    </div>
                                    <div class="posts-tree-field">
                                        <div class="form-group">
                                            <label for="" class="control-label">Select page where to redirect:</label>
                                            <div class="posts-tree-wrapper">
                                                <?php require ASSETS_PATH . '/components/SeriesPostsTree/SeriesPostsTree.html';?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php require ASSETS_PATH . '/components/FormsList/FormsList.php';?>
                                    <div class="form-group">
                                        <label for="" class="control-label">Keywords (optional):</label>
                                        <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                        <div class="terms-container flex-row margin-between flex-wrap">
                                            <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                        <div class="cover-image-wrapper">
                                            <div class="flex-row margin-between horizon-center vertical-center">
                                                <div class="flex-col fix-col">
                                                    <div class="img-wrapper">
                                                        <img src="assets/images/global-icons/cloud-uploading.png">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="upload-helper-txt">Drag Image here to upload</div>
                                                </div>
                                            </div>
                                            <input class="form-control" type="file" name="strPost_featuredimage" />
                                        </div>
                                    </div>
                                    <div class="answered-form-field-wrapper sample" hidden>
                                        <?php require ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                                    </div>
                                    <div class="button-wrapper">
                                        <button class="save-btn">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="post-creator-fork">
                            <div class="create-menu-decision">
                                <form action="" class="create-menu-decision-form" method="post">
                                    <div class="flex-row margin-between">
                                        <div class="flex-col mr-2">
                                            <div class="form-group">
                                                <label for="" class="control-label">Name your Menu:</label>
                                                <input class="form-control" type="text" name="strPost_title" placeholder="type the name here"/>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="form-group options-cnt-slider">
                                                <label for="" class="control-label">Options quantity:</label>
                                                <input type="range" class="options-cnt-input form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Describe it:</label>
                                        <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                    </div>
                                    <div class="options-container">
                                        <div class="option-item sample" hidden>
                                            <h2 class="option-number">Option <span class="number-value">1</span></h2>
                                            <div class="flex-row margin-between">
                                                <div class="flex-col mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Name of this option:</label>
                                                        <input class="form-control title-input" type="text" placeholder="type the name here"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Description of this option:</label>
                                                        <textarea class="form-control body-input" type="text" rows="5"></textarea>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="cover-image-wrapper">
                                                        <div class="img-wrapper">
                                                            <img class="option-image" />
                                                        </div>
                                                        <div class="cloud-img-wrapper">
                                                            <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                                        </div>
                                                        <div>Drag Image here to upload</div>
                                                        <input class="form-control" type="file"/>
                                                        <a href="javascript:;" class="delete-action">
                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"></polygon>
                                            <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"></path>
                                        </g>
                                    </g>
                                </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-wrapper">
                                        <button class="save-btn">Create Menu</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="post-creator-form">
                            <div class="create-with-form">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for="" class="control-label">Name your post:</label>
                                        <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Describe it:</label>
                                        <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                    </div>
                                    <div class="form-create-wrapper">
                                        <div class="flex-row margin-between vertical-center type-select-row">
                                            <div class="flex-col fix-col mr-2">
                                                <div class="choose-question-type-txt">Choose question type</div>
                                            </div>
                                            <div class="flex-col fix-col mr-2">
                                                <a href="javascript:;" class="type-choose-item active-item" data-type="checkbox">
                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 469.333 469.333;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path d="M426.667,0h-384C19.146,0,0,19.135,0,42.667v384c0,23.531,19.146,42.667,42.667,42.667h384
                                                                            c23.521,0,42.667-19.135,42.667-42.667v-384C469.333,19.135,450.188,0,426.667,0z M448,426.667
                                                                            c0,11.76-9.563,21.333-21.333,21.333h-384c-11.771,0-21.333-9.573-21.333-21.333v-384c0-11.76,9.563-21.333,21.333-21.333h384
                                                                            c11.771,0,21.333,9.573,21.333,21.333V426.667z"/>
                                                                        <path d="M365.583,131.344L192.187,314.927l-88.25-98.062c-3.979-4.385-10.708-4.729-15.083-0.792s-4.729,10.688-0.792,15.063
                                                                            l96,106.667c2,2.219,4.833,3.49,7.813,3.531c0.042,0,0.083,0,0.125,0c2.938,0,5.729-1.208,7.75-3.344l181.333-192
                                                                            c4.042-4.281,3.854-11.031-0.417-15.073C376.396,126.865,369.604,127.062,365.583,131.344z"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                            </svg>
                                                </a>
                                            </div>
                                            <div class="flex-col fix-col mr-2">
                                                <div class="or-txt">or</div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <a href="javascript:;" class="type-choose-item" data-type="text">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <path style="fill:#303C42;" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224C83.146,0,64,19.135,64,42.667
                                                                    v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                        <path style="fill:#E6E6E6;" d="M341.333,36.417l70.25,70.25h-48.917c-11.771,0-21.333-9.573-21.333-21.333V36.417z"/>
                                                        <path style="fill:#FFFFFF;" d="M405.333,490.667H106.667c-11.771,0-21.333-9.573-21.333-21.333V42.667
                                                                    c0-11.76,9.563-21.333,21.333-21.333H320v64C320,108.865,339.146,128,362.667,128h64v341.333
                                                                    C426.667,481.094,417.104,490.667,405.333,490.667z"/>
                                                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-30.0002" y1="641.8779" x2="-25.7514" y2="637.6291" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                            <stop  offset="0" style="stop-color:#000000;stop-opacity:0.1"/>
                                                            <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                                                        </linearGradient>
                                                        <path style="fill:url(#SVGID_1_);" d="M362.667,128c-10.005,0-19.098-3.605-26.383-9.397l-0.259-0.027l90.642,90.642V128H362.667z"
                                                        />
                                                        <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-46.4381" y1="639.0643" x2="-23.653" y2="628.4363" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                            <stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
                                                            <stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
                                                        </linearGradient>
                                                        <path style="fill:url(#SVGID_2_);" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224
                                                                    C83.146,0,64,19.135,64,42.667v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                        <g>
                                                            <path style="fill:#303C42;" d="M352,277.333h-85.333c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h21.333V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667H320c5.896,0,10.667-4.771,10.667-10.667c0-5.896-4.771-10.667-10.667-10.667v-85.333h21.333
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667V288C362.667,282.104,357.896,277.333,352,277.333z"/>
                                                            <path style="fill:#303C42;" d="M234.667,384V234.667h42.667c0,5.896,4.771,10.667,10.667,10.667
                                                                        c5.896,0,10.667-4.771,10.667-10.667V224c0-5.896-4.771-10.667-10.667-10.667H160c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h42.667V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667h21.333c5.896,0,10.667-4.771,10.667-10.667C245.333,388.771,240.563,384,234.667,384z"/>
                                                        </g>
                                                                </svg>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="flex-row margin-between flex-wrap">
                                            <div class="flex-col fb-0 mr-2">
                                                <div class="multi-line-background">
                                                    <textarea class="form-question-input" rows="3" required placeholder="My question is..."></textarea>
                                                    <div class="background-placeholder"></div>
                                                </div>
                                            </div>
                                            <div class="flex-col fb-0">
                                                <div class="multi-line-background">
                                                    <textarea class="form-helper-input" rows="3" required placeholder="This hint is help of..."></textarea>
                                                    <div class="background-placeholder"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-wrapper">
                                            <button class="add-form-btn btn" type="button">Create Form</button>
                                        </div>
                                    </div>
                                    <?php require ASSETS_PATH . '/components/FormsList/FormsList.php';?>
                                    <div class="flex-row flex-wrap margin-between">
                                        <div class="flex-col fb-0 mr-2">
                                            <div class="form-group">
                                                <label for="" class="control-label">Keywords (optional):</label>
                                                <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                                <div class="terms-container flex-row margin-between">
                                                    <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-col fb-0">
                                            <div class="form-group">
                                                <label for="" class="control-label">
                                                    Money Saved, $ <span>(optional)</span>:
                                                </label>
                                                <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                        <div class="cover-image-wrapper">
                                            <div class="flex-row margin-between horizon-center vertical-center">
                                                <div class="flex-col fix-col">
                                                    <div class="img-wrapper">
                                                        <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="upload-helper-txt">Drag Image here to upload</div>
                                                </div>
                                            </div>
                                            <input class="form-control" type="file" name="strPost_featuredimage" />
                                        </div>
                                    </div>
                                    <div class="button-wrapper">
                                        <button class="save-btn">Create</button>
                                    </div>
                                    <div class="answered-form-field-wrapper sample" hidden>
                                        <?php require ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="post-creator-form-loop">
                            <div class="create-with-form-loop">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for="" class="control-label">Name your post:</label>
                                        <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Describe it:</label>
                                        <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                    </div>
                                    <?php require ASSETS_PATH . '/components/FormLoopEditor/FormLoopEditor.html';?>
                                    <?php require ASSETS_PATH . '/components/FormsList/FormsList.php';?>
                                    <div class="flex-row flex-wrap margin-between">
                                        <div class="flex-col fb-0 mr-2">
                                            <div class="form-group">
                                                <label for="" class="control-label">Keywords (optional):</label>
                                                <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                                <div class="terms-container flex-row margin-between">
                                                    <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-col fb-0">
                                            <div class="form-group">
                                                <label for="" class="control-label">
                                                    Money Saved, $ <span>(optional)</span>:
                                                </label>
                                                <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                        <div class="cover-image-wrapper">
                                            <div class="flex-row margin-between horizon-center vertical-center">
                                                <div class="flex-col fix-col">
                                                    <div class="img-wrapper">
                                                        <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="upload-helper-txt">Drag Image here to upload</div>
                                                </div>
                                            </div>
                                            <input class="form-control" type="file" name="strPost_featuredimage" />
                                        </div>
                                    </div>
                                    <div class="button-wrapper">
                                        <button class="save-btn">Create</button>
                                    </div>
                                    <div class="answered-form-field-wrapper sample" hidden>
                                        <?php require ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="virtual-links">
                    <a href="#post-creator-normal" data-toggle="tab"></a>
                    <a href="#post-creator-fork" data-toggle="tab"></a>
                    <a href="#post-creator-form" data-toggle="tab"></a>
                    <a href="#post-creator-form-loop" data-toggle="tab"></a>
                </div>
            </div>
        </div>
    </div>
</div>