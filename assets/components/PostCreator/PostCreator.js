var PostCreatorClass;
(function () {
    'use strict';

    PostCreatorClass = function (domRoot, series) {

        var domVirtualLinks, domPostTypeWrap;
        var normalHandle, forkHandle, formHandle, formLoopHandle;
        var formFields = [], formFieldAnswers = [];
        var callbacksAfterSave = [];
        var self = this;

        var bindEvents = function () {
            domPostTypeWrap.find('ul li a').click(function () {
                domPostTypeWrap.find('ul li').removeAttr('hidden');
                $(this).parent().attr('hidden', true);
                var v = $(this).data('value');
                var txt = $(this).text();
                domPostTypeWrap.data('value', v);
                domPostTypeWrap.find('button span').text(txt);
                self.setType(v);
            });
            $('body').on('hidden.bs.modal', '.modal', function () {
                if ($('body').find('.modal.in').length > 0) {
                    $('body').addClass('modal-open');
                }
            });
        }

        var NormalClass = function () {
            var domForm, domPostTypeWrap, domDes, domFieldsList;
            var self = this, seriesTreeHandle, formsListHandle;
            var fieldRowHandles = [];

            var searchTerms = [];
            var intPost_type = 7, strPost_nodeType;
            var insertNodeToBody = function (domNode) {
                domDes.summernote('editor.restoreRange');
                domDes.summernote('editor.focus');
                domDes.summernote('insertNode', domNode);
                domDes.summernote('insertText', '\u00A0');
                makeDescriptionValid();
                $('[data-toggle="popover"]').popover();
            }
            var makeDescriptionValid = function () {
                var domEditor = domForm.find('.note-editor.note-frame .note-editable');
                var tpPreBr = false;
                domEditor.find('div>br, p>br').each(function (i, d) {
                    var p = $(d).parent();
                    if (p.html() === '<br>') {
                        if (tpPreBr) {
                            tpPreBr.remove();
                        }
                        tpPreBr = p;
                    }
                });
                domEditor.find('.form-loop-component').each(function (i, d) {
                    if ($(d).find('thead').length === 0) {
                        $(d).remove();
                    }
                    $(d).find('p thead').unwrap();
                })
                domEditor.find('.form-field-wrapper').each(function (i, d) {
                    if ($(d).find('.form-field-name').length === 0) {
                        $(d).remove();
                    }
                });
            }
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var setType = function (v) {
                domPostTypeWrap.find('ul li').removeAttr('hidden');
                var a = domPostTypeWrap.find('ul li a[data-value="'+ v +'"]');
                a.parent().attr('hidden', true);
                var txt = a.text();
                domPostTypeWrap.data('value', v);
                domPostTypeWrap.find('button span').text(txt);
                v = parseInt(v) ? parseInt(v) : v;
                domForm.find('[name="strPost_body"]').attr('required', true);
                switch (v) {
                    case 'menu':
                        strPost_nodeType = 'menu';
                        intPost_type = 7;
                        domForm.attr('data-type', 'menu');
                        break;
                    case 'path':
                        strPost_nodeType = 'path';
                        intPost_type = 7;
                        domForm.attr('data-type', 'path');
                        break;
                    case 9:
                        strPost_nodeType = 'post';
                        intPost_type = v;
                        domForm.attr('data-type', 'switch-to');
                        domForm.find('[name="strPost_body"]').removeAttr('required');
                        break;
                    case 10:
                        strPost_nodeType = 'post';
                        intPost_type = v;
                        domForm.attr('data-type', 'form');
                        break;
                    default:
                        strPost_nodeType = 'post';
                        intPost_type = v;
                        domForm.attr('data-type', v);
                        break;
                }
            }
            var bindData = function () {
                if (series) {
                    seriesTreeHandle.setSeries(series.series_ID);
                }
            }
            var bindEvents = function () {
                domForm.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domForm.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domForm.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domPostTypeWrap.find('ul li a').click(function () {
                    domPostTypeWrap.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domPostTypeWrap.data('value', v);
                    domPostTypeWrap.find('button span').text(txt);
                    setType(v);
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.submit(function () {
                    save();
                    return false;
                });
                domDes.on('summernote.enter', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.focus', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.keydown', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.change', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.parent().find('.note-editable').click(function () {
                    domDes.summernote('editor.saveRange');
                });
                formsListHandle.onQuestionClick(function (d) {
                    insertNodeToBody(d.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (d.find('.form-field-value').html() == '<br>') {
                            d.find('.form-field-value').empty();
                        }
                    });
                });
                formsListHandle.onAnswerClick(function (d) {
                    insertNodeToBody(d.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (d.find('.form-field-value').html() == '<br>') {
                            d.find('.form-field-value').empty();
                        }
                    });
                });
            }
            var save = function () {
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_series_ID = series.series_ID;
                sets.intPost_type = intPost_type;
                sets.strPost_nodeType = strPost_nodeType;
                sets.strPost_keywords = searchTerms.join(',');
                if (sets.intPost_type == 9) {
                    sets.strPost_body = seriesTreeHandle.postRedirectPosition();
                    if (sets.strPost_body == 0) {
                        swal('Please select page where to redirect');
                        return false;
                    }
                }
                ajaxData.append('sets', JSON.stringify(sets));
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                    ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                }
                return ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                    callbacksAfterSave.forEach(function (value) {
                        value(res.data);
                    });
                    domRoot.modal('hide');
                    return res.data;
                });
            }
            this.setType = setType;
            this.empty = function () {
                domForm.find('[name="strPost_title"]').val('');
                domForm.find('[name="intPost_savedMoney"]').val('');
                domForm.find('[name="strPost_body"]').summernote('code', '');
                searchTerms = [];
                domForm.find('.terms-container').find('.term-item:not(.sample)').remove();
                $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', 'assets/images/global-icons/cloud-uploading.png').css('opacity', 0.7);
                seriesTreeHandle.empty();
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#post-creator-normal"]').tab('show');
            }
            this.clean = function () {
                self.empty();
                bindData();
            }
            this.init = function () {
                domForm = domRoot.find('.create-normal form');
                domDes = domForm.find('textarea[name="strPost_body"]');
                domFieldsList = domForm.find('.form-fiels-container');
                domPostTypeWrap = domForm.find('.dropdown');
                formsListHandle = new FormsListClass(domForm.find('.forms-list-component'), {seriesId: series.series_ID});
                formsListHandle.init();
                $.summernote.dom.emptyPara = "<div><br></div>";
                domForm.find('[name="strPost_body"]').summernote({
                    dialogsInBody: true,
                    height: 200,
                    tabsize: 2,
                });
                seriesTreeHandle = new SeriesPostsTreeClass(false, domForm.find('.posts-tree-wrapper .series-posts-tree'), {showSeriesTitle: false});
                seriesTreeHandle.init();

                bindEvents();
            }
        }
        var CreateMenuDecisionClass = function () {
            var domForm, domSortByDropDown, domOptionsContainer;
            var optionItemHandles = [];
            var self = this;
            var bindEvents = function () {
                domForm.find('.options-cnt-input').change(function () {
                    setOptionCnt(parseInt($(this).val()));
                });
                domForm.submit(function () {
                    save();
                    return false;
                })
            }
            var setOptionCnt = function (v) {
                var len = optionItemHandles.length;
                console.log(v, len);
                for (var i = 0; i < v || i < len; i++) {
                    if (i >= v) {
                        var hdl = optionItemHandles[optionItemHandles.length - 1];
                        hdl.remove();
                        optionItemHandles.pop();
                    }
                    else if (i >= len) {
                        var hdl = new OptionItemClass();
                        hdl.init();
                        optionItemHandles.push(hdl);
                    }
                }
            }
            var OptionItemClass = function () {
                var domOption;
                var self = this;
                var updateCoverImage = function(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.cover-image-wrapper .img-wrapper img', domOption).attr('src', e.target.result).css('opacity', 1);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                var bindEvents = function () {
                    domOption.find('.cover-image-wrapper input[type="file"]').change(function () {
                        updateCoverImage(this);
                    });
                    domOption.find('.cover-image-wrapper .delete-action').click(function () {
                        domOption.find('.cover-image-wrapper input[type="file"]').val('');
                        domOption.find('.cover-image-wrapper .img-wrapper img').removeAttr('src');
                    });
                }
                this.data = function () {
                    var rtn = {
                        strPost_title: domOption.find('.title-input').val(),
                        strPost_body: domOption.find('.body-input').val(),
                        strPost_nodeType: 'path',
                        intPost_series_ID: series.series_ID,
                    };
                    if (domOption.find('.cover-image-wrapper input[type=file]').prop('files').length) {
                        rtn.strPost_featuredimage = domOption.find('.cover-image-wrapper input[type=file]').prop('files')[0];
                    }
                    return rtn;
                }
                this.remove = function () {
                    domOption.remove();
                }
                this.save = function (parentId) {
                    var sets = self.data();
                    sets.intPost_parent = parentId;
                    var ajaxData = new FormData();
                    ajaxData.append('action', 'insert');
                    if (sets.strPost_featuredimage) {
                        ajaxData.append('strPost_featuredimage', sets.strPost_featuredimage);
                        delete sets.strPost_featuredimage;
                    }
                    ajaxData.append('sets', JSON.stringify(sets));
                    return ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData, true, 100).then(function (res) {
                        return res.data;
                    });
                }
                this.init = function () {
                    domOption = domOptionsContainer.find('.option-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domOptionsContainer);
                    domOption.find('.option-number .number-value').html(optionItemHandles.length + 1);
                    bindEvents();
                }
            }

            var save = function () {
                var sets = helperHandle.formSerialize(domForm);
                sets.strPost_nodeType = 'menu';
                sets.intPost_series_ID = series.series_ID;
                ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}, true, 100).then(function (res) {
                    if (res.status) {
                        var ps = [];
                        optionItemHandles.forEach(function (hdl) {
                            ps.push(hdl.save(res.data.post_ID));
                        });
                        Promise.all(ps).then(function () {
                            callbacksAfterSave.forEach(function (value) {
                                value(res.data);
                            });
                            domRoot.modal('hide');
                        });
                    }
                });
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#post-creator-fork"]').tab('show');
            }
            this.empty = function () {
                domForm.find('[name="strPost_title"]').val('');
                domForm.find('[name="intPost_savedMoney"]').val('');
                domForm.find('[name="strPost_body"]').summernote('code', '');
                domForm.find('.terms-container').find('.term-item:not(.sample)').remove();
                $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', 'assets/images/global-icons/cloud-uploading.png').css('opacity', 0.7);
            }
            this.clean = function () {
                self.empty();
            }
            this.init = function () {
                domForm = domRoot.find('.create-menu-decision form');
                domOptionsContainer = domForm.find('.options-container');
                domSortByDropDown = domForm.find('.dropdown');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domForm.find('[name="strPost_body"]').summernote({
                    dialogsInBody: true,
                    height: 200,
                    tabsize: 2,
                });
                domForm.find('.options-cnt-input').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 5,
                    value: 2,
                });

                var hdl = new OptionItemClass();
                hdl.init();
                optionItemHandles.push(hdl);

                hdl = new OptionItemClass();
                hdl.init();
                optionItemHandles.push(hdl);

                bindEvents();
            }
        }
        var CreateWithFormClass = function () {
            var domForm, domSortByDropDown, domFieldsList, domDes;
            var self = this, formsListHandle;
            var fieldRowHandles = [];

            var searchTerms = [];
            var intPost_type = 10;
            var fieldCreateType = 'checkbox';
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domForm.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domForm.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domForm.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    intPost_type = v;
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.find('.type-choose-item').click(function () {
                    domForm.find('.type-choose-item').removeClass('active-item');
                    $(this).addClass('active-item');
                    fieldCreateType = $(this).data('type');
                });
                domForm.find('.add-form-btn').click(function () {
                    createField().then(function (res) {
                        if (res) {
                            formsListHandle.addField(res);
                        };
                    })
                });
                domDes.on('summernote.enter', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.focus', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.keydown', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.change', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.parent().find('.note-editable').click(function () {
                    domDes.summernote('editor.saveRange');
                });
                domForm.submit(function () {
                    create();
                    return false;
                });
                domForm.find('.save-btn').click(function (e) {
                    create();
                    e.preventDefault();
                    return false;
                });
                formsListHandle.onQuestionClick(function (d) {
                    insertNodeToBody(d.get(0));
                });
                formsListHandle.onAnswerClick(function (d) {
                    insertNodeToBody(d.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (d.find('.form-field-value').html() == '<br>') {
                            d.find('.form-field-value').empty();
                        }
                    });
                });
            }
            var create = function () {
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_series_ID = series.series_ID;
                sets.intPost_type = intPost_type;
                sets.strPost_keywords = searchTerms.join(',');
                ajaxData.append('sets', JSON.stringify(sets));
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                    ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                }
                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                    callbacksAfterSave.forEach(function (value) {
                        value(res.data);
                    });
                    domRoot.modal('hide');
                });
            }
            var createField = function () {
                var sets = {};
                sets.strFormField_name = domForm.find('.form-question-input').val();
                if (!sets.strFormField_name) {
                    swal('please name your form field');
                    return Promise.resolve(false);
                }
                sets.strFormField_helper = domForm.find('.form-helper-input').val();
                sets.intFormField_series_ID = series.series_ID;
                sets.strFormField_type = fieldCreateType;
                switch (fieldCreateType) {
                    case 'text':
                        sets.strFormField_default = '';
                        break;
                    case 'checkbox':
                        sets.strFormField_default = 0;
                        break;
                }
                return ajaxAPiHandle.apiPost('FormFields.php', {action: 'insert', sets: sets}).then(function (res) {
                    return $.extend(sets, {formField_ID: res.data});
                });
            }
            var insertNodeToBody = function (domNode) {
                domDes.summernote('editor.restoreRange');
                domDes.summernote('editor.focus');
                domDes.summernote('insertNode', domNode);
                domDes.summernote('insertText', '\u00A0');
                makeDescriptionValid();
                $('[data-toggle="popover"]').popover();
            }
            var makeDescriptionValid = function () {
                var domEditor = domForm.find('.note-editor.note-frame .note-editable');
                var tpPreBr = false;
                domEditor.find('div>br, p>br').each(function (i, d) {
                    var p = $(d).parent();
                    if (p.html() === '<br>') {
                        if (tpPreBr) {
                            tpPreBr.remove();
                        }
                        tpPreBr = p;
                    }
                });
                domEditor.find('.form-loop-component').each(function (i, d) {
                    if ($(d).find('thead').length === 0) {
                        $(d).remove();
                    }
                    $(d).find('p thead').unwrap();
                })
                domEditor.find('.form-field-wrapper').each(function (i, d) {
                    if ($(d).find('.form-field-name').length === 0) {
                        $(d).remove();
                    }
                });
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#post-creator-form"]').tab('show');
            }
            this.empty = function () {
                domForm.find('[name="strPost_title"]').val('');
                domForm.find('[name="intPost_savedMoney"]').val('');
                domForm.find('[name="strPost_body"]').summernote('code', '');
                searchTerms = [];
                domForm.find('.terms-container').find('.term-item:not(.sample)').remove();
                $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', 'assets/images/global-icons/cloud-uploading.png').css('opacity', 0.7);
                fieldRowHandles.forEach(function (value) {
                    value.dom().remove();
                });
                fieldRowHandles = [];
            }
            this.clean = function () {
                self.empty();
            }
            this.init = function () {
                domForm = domRoot.find('.create-with-form form');
                domDes = domForm.find('textarea[name="strPost_body"]');
                domFieldsList = domForm.find('.form-fiels-container');
                domSortByDropDown = domForm.find('.dropdown');
                formsListHandle = new FormsListClass(domForm.find('.forms-list-component'), {seriesId: series.series_ID});
                formsListHandle.init();
                $.summernote.dom.emptyPara = "<div><br></div>";
                domForm.find('[name="strPost_body"]').summernote({
                    dialogsInBody: true,
                    height: 200,
                    tabsize: 2,
                });
                bindEvents();
            }
        }
        var CreateWithFormLoopClass = function () {
            var domForm, domFieldsList, domDes;
            var self = this, formLoopEditorHandle, formsListHandle;
            var fieldRowHandles = [];

            var searchTerms = [];
            var intPost_type = 10;
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domForm.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domForm.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domForm.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domDes.on('summernote.enter', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.focus', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.keydown', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.change', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.parent().find('.note-editable').click(function () {
                    domDes.summernote('editor.saveRange');
                });
                domForm.submit(function () {
                    create();
                    return false;
                });
                domForm.find('.save-btn').click(function (e) {
                    create();
                    e.preventDefault();
                    return false;
                });
                formsListHandle.onQuestionClick(function (d) {
                    insertNodeToBody(d.get(0));
                });
                formsListHandle.onAnswerClick(function (d) {
                    insertNodeToBody(d.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (d.find('.form-field-value').html() == '<br>') {
                            d.find('.form-field-value').empty();
                        }
                    });
                });
                formLoopEditorHandle.onCreate(function (value) {
                    formsListHandle.addLoop(value);
                });
            }
            var create = function () {
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_series_ID = series.series_ID;
                sets.intPost_type = intPost_type;
                sets.strPost_keywords = searchTerms.join(',');
                ajaxData.append('sets', JSON.stringify(sets));
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                    ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                }
                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                    callbacksAfterSave.forEach(function (value) {
                        value(res.data);
                    });
                    domRoot.modal('hide');
                });
            }
            var insertNodeToBody = function (domNode) {
                domDes.summernote('editor.restoreRange');
                domDes.summernote('editor.focus');
                domDes.summernote('insertNode', domNode);
                domDes.summernote('insertText', '\u00A0');
                makeDescriptionValid();
                $('[data-toggle="popover"]').popover();
            }
            var makeDescriptionValid = function () {
                var domEditor = domForm.find('.note-editor.note-frame .note-editable');
                var tpPreBr = false;
                domEditor.find('div>br, p>br').each(function (i, d) {
                    var p = $(d).parent();
                    if (p.html() === '<br>') {
                        if (tpPreBr) {
                            tpPreBr.remove();
                        }
                        tpPreBr = p;
                    }
                });
                domEditor.find('.form-loop-component').each(function (i, d) {
                    if ($(d).find('thead').length === 0) {
                        $(d).remove();
                    }
                    $(d).find('p thead').unwrap();
                });
                domEditor.find('.form-loop-answer-table-component').each(function (i, d) {
                    if ($(d).find('thead').length === 0) {
                        $(d).remove();
                    }
                    $(d).find('p thead').unwrap();
                });
                domEditor.find('.form-field-wrapper').each(function (i, d) {
                    if ($(d).find('.form-field-name').length === 0) {
                        $(d).remove();
                    }
                });
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#post-creator-form-loop"]').tab('show');
            }
            this.empty = function () {
                domForm.find('[name="strPost_title"]').val('');
                domForm.find('[name="intPost_savedMoney"]').val('');
                domForm.find('[name="strPost_body"]').summernote('code', '');
                searchTerms = [];
                domForm.find('.terms-container').find('.term-item:not(.sample)').remove();
                $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', 'assets/images/global-icons/cloud-uploading.png').css('opacity', 0.7);
                fieldRowHandles.forEach(function (value) {
                    value.dom().remove();
                });
                fieldRowHandles = [];
            }
            this.clean = function () {
                self.empty();
            }
            this.init = function () {
                domForm = domRoot.find('.create-with-form-loop form');
                domDes = domForm.find('textarea[name="strPost_body"]');
                domFieldsList = domForm.find('.form-fiels-container');
                formLoopEditorHandle = new FormLoopEditorClass(domForm.find('.form-loop-editor-component'), {seriesId: series.series_ID});
                formLoopEditorHandle.init();
                formsListHandle = new FormsListClass(domForm.find('.forms-list-component'), {seriesId: series.series_ID});
                formsListHandle.init();
                $.summernote.dom.emptyPara = "<div><br></div>";
                domForm.find('[name="strPost_body"]').summernote({
                    dialogsInBody: true,
                    height: 200,
                    tabsize: 2,
                });
                bindEvents();
            }
        }
        this.clean = function () {
            normalHandle.clean();
            formHandle.clean();
            formLoopHandle.clean();
            forkHandle.clean();
        }
        this.setType = function (v) {
            domPostTypeWrap.find('ul li').removeAttr('hidden');
            var a = domPostTypeWrap.find('ul li a[data-value="'+ v +'"]');
            a.parent().attr('hidden', true);
            var txt = a.text();
            domPostTypeWrap.data('value', v);
            domPostTypeWrap.find('button span').text(txt);
            v = parseInt(v) ? parseInt(v) : v;
            switch (v) {
                case 'menu':
                    forkHandle.tabShow();
                    break;
                case 10:
                    formHandle.tabShow();
                    break;
                case 'form_loop':
                    formLoopHandle.tabShow();
                    break;
                default:
                    normalHandle.tabShow();
                    normalHandle.setType(v);
                    break;
            }
        }
        this.open = function () {
            domRoot.modal('show');
        }
        this.afterSave = function (v) {
            callbacksAfterSave = [];
            callbacksAfterSave.push(v);
        }
        this.init = function () {
            domPostTypeWrap = domRoot.find('.select-type-wrapper .dropdown');
            domVirtualLinks = domRoot.find('.virtual-links');
            normalHandle = new NormalClass();
            normalHandle.init();
            forkHandle = new CreateMenuDecisionClass();
            forkHandle.init();
            formHandle = new CreateWithFormClass();
            formHandle.init();
            formLoopHandle = new CreateWithFormLoopClass();
            formLoopHandle.init();
            bindEvents();
        }
    }
})();