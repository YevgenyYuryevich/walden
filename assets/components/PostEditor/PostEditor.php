<div class="modal fade site-modal post-editor component" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <a href="javascript:;" class="modal-close" data-dismiss="modal"></a>
            <div class="modal-body">
                <div class="post-editor-content">
                    <h2 class="modal-title">Edit Post</h2>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="" class="control-label">Name your post:</label>
                            <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Subtitle:</label>
                            <input type="text" class="form-control" name="strPost_subtitle" placeholder="type the subtitle here"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Describe it:</label>
                            <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                        </div>
                        <div class="flex-row flex-wrap margin-between">
                            <div class="flex-col fb-0 mr-2">
                                <div class="form-group">
                                    <label for="" class="control-label">Post Type:</label>
                                    <div class="dropdown" data-value="7">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                            <span>Text</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:;" data-value="0">Audio</a></li>
                                            <li><a href="javascript:;" data-value="2">Video</a></li>
                                            <li hidden><a href="javascript:;" data-value="7">Text</a></li>
                                            <li><a href="javascript:;" data-value="8">Image</a></li>
                                            <li><a href="javascript:;" data-value="9">Switch-To</a></li>
                                            <li><a href="javascript:;" data-value="10">Form</a></li>
                                            <li><a href="javascript:;" data-value="menu">Menu</a></li>
                                            <li><a href="javascript:;" data-value="path">Option</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fb-0">
                                <div class="form-group">
                                    <label for="" class="control-label">
                                        Money Saved, $ <span>(optional)</span>:
                                    </label>
                                    <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                </div>
                            </div>
                        </div>
                        <div class="posts-tree-field">
                            <div class="form-group">
                                <label for="" class="control-label">Select page where to redirect:</label>
                                <div class="posts-tree-wrapper">
                                    <?php require ASSETS_PATH . '/components/SeriesPostsTree/SeriesPostsTree.html';?>
                                </div>
                            </div>
                        </div>
                        <?php require ASSETS_PATH . '/components/FormsList/FormsList.php';?>
                        <div class="form-group">
                            <label for="" class="control-label">Keywords (optional):</label>
                            <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                            <div class="terms-container flex-row margin-between flex-wrap">
                                <div class="flex-col fix-col term-item sample" hidden>#design</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload Cover Image for this Post (optional)</label>
                            <div class="cover-image-wrapper">
                                <div class="flex-row margin-between horizon-center vertical-center">
                                    <div class="flex-col fix-col">
                                        <div class="img-wrapper">
                                            <img src="assets/images/global-icons/cloud-uploading.png">
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="upload-helper-txt">Drag Image here to upload</div>
                                    </div>
                                </div>
                                <input class="form-control" type="file" name="strPost_featuredimage" />
                            </div>
                        </div>
                        <div class="group__show-image">
                            <input id="post-editor__show-image" type="checkbox" name="boolPost_show_image" value="1" /><label for="post-editor__show-image">Show Image</label>
                        </div>
                        <div class="answered-form-field-wrapper sample" hidden>
                            <?php require_once ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                        </div>
                        <div class="button-wrapper">
                            <button class="save-btn">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
