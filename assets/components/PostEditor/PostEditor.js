var PostEditorClass;
(function () {
    'use strict';

    PostEditorClass = function (domRoot, post) {

        var domForm, domPostTypeWrap, domDes, domFieldsList;
        var self = this, seriesTreeHandle, formsListHandle;
        var fieldRowHandles = [];
        var callbacksAfterSave = [];

        var searchTerms = [];
        var intPost_type = 7, strPost_nodeType;

        var insertNodeToBody = function (domNode) {
            domDes.summernote('editor.restoreRange');
            domDes.summernote('editor.focus');
            domDes.summernote('insertNode', domNode);
            domDes.summernote('insertText', '\u00A0');
            makeDescriptionValid();
            $('[data-toggle="popover"]').popover();
        }
        var makeDescriptionValid = function () {
            var domEditor = domForm.find('.note-editor.note-frame .note-editable');
            var tpPreBr = false;
            domEditor.find('div>br, p>br').each(function (i, d) {
                var p = $(d).parent();
                if (p.html() === '<br>') {
                    if (tpPreBr) {
                        tpPreBr.remove();
                    }
                    tpPreBr = p;
                }
            });
            domEditor.find('.form-loop-component').each(function (i, d) {
                if ($(d).find('thead').length === 0) {
                    $(d).remove();
                }
                $(d).find('p thead').unwrap();
            });
            domEditor.find('.form-loop-answer-table-component').each(function (i, d) {
                if ($(d).find('thead').length === 0) {
                    $(d).remove();
                }
                $(d).find('p thead').unwrap();
            });
            domEditor.find('.form-field-wrapper').each(function (i, d) {
                if ($(d).find('.form-field-name').length === 0) {
                    $(d).remove();
                }
            });
        }
        var updateCoverImage = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        var setType = function (v) {
            domPostTypeWrap.find('ul li').removeAttr('hidden');
            var a = domPostTypeWrap.find('ul li a[data-value="'+ v +'"]');
            a.parent().attr('hidden', true);
            var txt = a.text();
            domPostTypeWrap.data('value', v);
            domPostTypeWrap.find('button span').text(txt);
            domForm.find('.group__show-image').hide();
            v = parseInt(v) ? parseInt(v) : v;
            switch (v) {
                case 'menu':
                    strPost_nodeType = 'menu';
                    intPost_type = 7;
                    domForm.attr('data-type', 'menu');
                    break;
                case 'path':
                    strPost_nodeType = 'path';
                    intPost_type = 7;
                    domForm.attr('data-type', 'path');
                    break;
                case 9:
                    strPost_nodeType = 'post';
                    intPost_type = v;
                    domForm.attr('data-type', 'switch-to');
                    break;
                case 10:
                    strPost_nodeType = 'post';
                    intPost_type = v;
                    domForm.attr('data-type', 'form');
                    break;
                case 7:
                    domForm.find('.group__show-image').show();
                default:
                    strPost_nodeType = 'post';
                    intPost_type = v;
                    domForm.attr('data-type', v);
                    break;
            }
        }
        var bindData = function () {
            domForm.find('[name="strPost_title"]').val(post.strPost_title);
            domForm.find('[name="strPost_subtitle"]').val(post.strPost_subtitle);
            domForm.find('[name="intPost_savedMoney"]').val(post.intPost_savedMoney);
            domForm.find('[name="strPost_body"]').summernote('code', post.strPost_body);
            switch (post.strPost_nodeType) {
                case 'post':
                    setType(post.intPost_type);
                    break;
                case 'menu':
                    setType('menu');
                    break;
                case 'path':
                    setType('path');
                    break;
            }
            searchTerms = post.strPost_keywords ? post.strPost_keywords.split(/\s*,+\s*/): [];
            searchTerms.forEach(function (v) {
                domForm.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domForm.find('.terms-container'));
            });
            $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', post.strPost_featuredimage).css('opacity', 1);
            const hash = new Date().getTime();
            domForm.find('#post-editor__show-image').attr('id', 'post-editor__show-image' + hash);
            domForm.find('[for="post-editor__show-image"]').attr('for', 'post-editor__show-image' + hash);
            if (parseInt(post.boolPost_show_image)) {
                domForm.find('#post-editor__show-image' + hash).prop('checked', true);
            } else {
                domForm.find('#post-editor__show-image' + hash).prop('checked', false);
            }
            if (post.series) {
                seriesTreeHandle.setSeries(post.series);
            }
            else {
                seriesTreeHandle.setSeries(post.intPost_series_ID);
            }
            if (formsListHandle) {
                formsListHandle.setSeries(post.intPost_series_ID);
            }
            else {
                formsListHandle = new FormsListClass(domForm.find('.forms-list-component'), {seriesId: post.intPost_series_ID});
                formsListHandle.init();
                formsListHandle.onQuestionClick(function (d) {
                    insertNodeToBody(d.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (d.find('.form-field-value').html() == '<br>') {
                            d.find('.form-field-value').empty();
                        }
                    });
                });
                formsListHandle.onAnswerClick(function (d) {
                    insertNodeToBody(d.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (d.find('.form-field-value').html() == '<br>') {
                            d.find('.form-field-value').empty();
                        }
                    });
                });
            }
        }
        var bindEvents = function () {
            domRoot.find('[name="strPost_keywords"]').keyup(function (e) {
                var code = e.which;
                if (code === 13) {
                    var v = $(this).val();
                    if (v) {
                        $(this).val('');
                        domRoot.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domRoot.find('.terms-container'));
                        searchTerms.push(v);
                    }
                }
            });
            domPostTypeWrap.find('ul li a').click(function () {
                domPostTypeWrap.find('ul li').removeAttr('hidden');
                $(this).parent().attr('hidden', true);
                var v = $(this).data('value');
                var txt = $(this).text();
                domPostTypeWrap.data('value', v);
                domPostTypeWrap.find('button span').text(txt);
                setType(v);
            });
            domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                updateCoverImage(this);
            });
            domForm.submit(function () {
                save();
                return false;
            });
            domDes.on('summernote.enter', function(e) {
                domDes.summernote('editor.saveRange');
            });
            domDes.on('summernote.focus', function(e) {
                domDes.summernote('editor.saveRange');
            });
            domDes.on('summernote.keydown', function(e) {
                domDes.summernote('editor.saveRange');
            });
            domDes.on('summernote.change', function(e) {
                domDes.summernote('editor.saveRange');
            });
            domDes.parent().find('.note-editable').click(function () {
                domDes.summernote('editor.saveRange');
            });
            $('body').on('hidden.bs.modal', '.modal', function () {
                if ($('body').find('.modal.in').length > 0) {
                    $('body').addClass('modal-open');
                }
            });
        }
        var save = function () {
            var ajaxData = new FormData();
            ajaxData.append('action', 'update');
            ajaxData.append('where', post.post_ID);
            var sets = helperHandle.formSerialize(domForm);
            sets.intPost_series_ID = post.intPost_series_ID;
            sets.intPost_type = intPost_type;
            sets.strPost_nodeType = strPost_nodeType;
            sets.strPost_keywords = searchTerms.join(',');
            sets.boolPost_show_image = sets.boolPost_show_image ? 1 : 0;
            if (sets.intPost_type == 9) {
                sets.strPost_body = seriesTreeHandle.postRedirectPosition();
                if (sets.strPost_body == 0) {
                    swal('Please select page where to redirect');
                    return false;
                }
            }
            // sets = Object.assign(sets, seriesTreeHandle.postInsertPosition());
            ajaxData.append('sets', JSON.stringify(sets));
            if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
            }
            return ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                callbacksAfterSave.forEach(function (value) {
                    value(res.data);
                });
                domRoot.modal('hide');
                return res.data;
            });
        }
        this.afterSave = function (v) {
            callbacksAfterSave = [];
            callbacksAfterSave.push(v);
        }
        this.setData = function (nPost) {
            self.empty();
            post = nPost;
            bindData();
        }
        this.open = function () {
            domRoot.modal('show');
        }
        this.empty = function () {
            domForm.find('[name="strPost_title"]').val('');
            domForm.find('[name="intPost_savedMoney"]').val('');
            domForm.find('[name="strPost_body"]').summernote('code', '');
            setType(7);
            searchTerms = [];
            domForm.find('.terms-container').find('.term-item:not(.sample)').remove();
            $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', 'assets/images/global-icons/cloud-uploading.png').css('opacity', 0.7);
            seriesTreeHandle.empty();
            if (formsListHandle) {
                formsListHandle.empty();
            }
        }
        this.init = function () {
            domForm = domRoot.find('form');
            domDes = domForm.find('textarea[name="strPost_body"]');
            domFieldsList = domForm.find('.form-fiels-container');
            domPostTypeWrap = domForm.find('.dropdown');
            $.summernote.dom.emptyPara = "<div><br></div>";
            domForm.find('[name="strPost_body"]').summernote({
                height: 200,
                tabsize: 2,
                dialogsInBody: true,
            });
            seriesTreeHandle = new SeriesPostsTreeClass(false, domForm.find('.posts-tree-wrapper .series-posts-tree'), {showSeriesTitle: false});
            seriesTreeHandle.init();

            bindEvents();
        }
    }
})();
