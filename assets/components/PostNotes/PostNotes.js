var PostNotesClass;
(function () {
    PostNotesClass = function (domRoot, data) {

        var domContainer;
        var defaultOptions = {
            collapsedSize: 'small',
            mobileStatus: 'normal'
        };
        var options;

        var itemHandles = [];

        var ItemClass = function (itemData) {
            var domItem;
            var self = this;
            var bindData = function () {
                domItem.find('.note-content').html(itemData.postNote_content);
            }

            var bindEvents = function () {
                domItem.find('.delete-action').click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "Do you want to delete this note?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDo) {
                        if (willDo) {
                            self.delete();
                        }
                    })
                });
            }
            this.data = function () {
                return itemData;
            }
            this.remove = function () {
                domItem.remove();
            }

            this.delete = function () {
                ajaxAPiHandle.apiPost('PostNotes.php', {action: 'delete', where: itemData.postNote_ID}).then(function () {
                    itemHandles.forEach(function (value, i) {
                        if (value.data().postNote_ID === itemData.postNote_ID) {
                            itemHandles.splice(i, 1);
                        }
                    });
                    self.remove();
                });
            }

            this.init = function () {
                domItem = domContainer.find('.note-item.sample').clone().removeClass('sample').removeAttr('hidden').prependTo(domContainer);
                bindData();
                bindEvents();
            }
        }

        var bindEvents = function () {
            domRoot.find('.left-section').click(function () {
                if (domRoot.hasClass('collapsed')) {
                    domRoot.removeClass('collapsed');
                    setTimeout(function () {
                        domRoot.find('.collapse').collapse('show');
                    }, 300);
                }
                else {
                    domRoot.find('.collapse').collapse('hide');
                    domRoot.addClass('collapsed');
                }
            });

            domRoot.find('.add-action').click(function () {
                var txt = domRoot.find('textarea.note-add-input').val();
                if (txt) {
                    insert(txt).then(function () {
                        domRoot.find('textarea.note-add-input').val('');
                    });
                }
            });

        }
        var insert = function (text) {
            var ajaxData = {
                action: 'insert',
                sets: {
                    postNote_content: text
                },
            };
            if (data.postId) {
                ajaxData.sets.postNote_post_ID = data.postId;
            }
            if (data.subscriptionId) {
                ajaxData.sets.postNote_subscription_ID = data.subscriptionId;
            }
            return ajaxAPiHandle.apiPost('PostNotes.php', ajaxData).then(function (res) {
                if (res.status) {
                    var itemData = ajaxData.sets;
                    itemData.postNote_ID = res.data;
                    var hdl = new ItemClass(itemData);
                    hdl.init();
                    itemHandles.push(hdl);
                    return hdl;
                }
                return false;
            });
        }
        var bindData = function () {
            data.postNotes.forEach(function (itemData) {
                var hdl = new ItemClass(itemData);
                hdl.init();
                itemHandles.push(hdl);
            });
        }
        this.setData = function (nData) {
            itemHandles.forEach(function (value) {
                value.remove();
                delete value;
            });
            itemHandles = [];
            data = nData;
            bindData();
        }
        this.init = function () {
            domContainer = domRoot.find('.notes-container');
            options = Object.assign(defaultOptions, data.options);
            if (options.collapsedSize === 'large') {
                domRoot.addClass('large-collapsed-size')
            }
            if (options.mobileStatus === 'static') {
                domRoot.addClass('mobile-expand-static');
            }
            bindData();
            bindEvents();
        }
    }
})();