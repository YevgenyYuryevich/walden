<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 19.01.2019
 * Time: 04:41
 */
?>
<div class="purchased-structure-tree-component component jstree jstree-default jstree-default-large">
    <li class="sample post-row jstree-node jstree-closed" hidden>
        <div class="post-row-inner">
            <div class="flex-row vertical-center space-between margin-between">
                <div class="flex-col fix-col"><i class="jstree-icon jstree-ocl" role="presentation"></i></div>
                <div class="flex-col fix-col">
                    <div class="drag-icon-wrapper">
                        <img class="drag-icon type-root" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/series/start-(3).png">
                        <img class="drag-icon type-post" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/post/blog-(4).png">
                        <img class="drag-icon type-menu" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/menu/path-selection-(4).png">
                        <img class="drag-icon type-path" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/option/next-(4).png">
                    </div>
                </div>
                <div class="flex-col fix-col d-type-none d-post-block ml-2 mr-0">
                    <div class="media-icon-wrapper">
                        <i class="fa fa-youtube-play video-icon"></i>
                        <i class="fa fa-volume-up audio-icon"></i>
                        <i class="fa fa-file-text text-icon"></i>
                    </div>
                </div>
                <div class="flex-col min-width-0">
                    <a href="javascript:;" class="post-title"></a>
                </div>
                <div class="flex-col fix-col d-mode-none d-mode-share-block">
                    <a href="javascript:;" class="action-item share-action">
                        <svg version="1.1" id="Layer_1"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                             viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title></title>
                            <desc>Created with Sketch.</desc>
                            <g id="Page-1" sketch:type="MSPage">
                                <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                    <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </li>
    <ul class="posts-list">
    </ul>
</div>
