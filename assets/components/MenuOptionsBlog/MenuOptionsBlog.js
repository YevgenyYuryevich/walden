var MenuOptionsClass;

(function () {
    MenuOptionsClass = function (domRoot, data) {
        var domOptionsList;
        var pathNumber = 1;
        var optionHandles = [];
        var onClick = function () {};
        var bindData = function () {
            domRoot.find('.menu-title').html(data.title);
            domRoot.find('.menu-description').html(data.body);
            data.paths.forEach(function (option) {
                var hdl = new OptionItemClass(option);
                hdl.init();
                optionHandles.push(hdl);
            });
        }
        var OptionItemClass = function (option) {
            var domOption;

            var bindData = function () {
                if (option.image) {
                    domOption.find('img.option-img').attr('src', option.image);
                }
                else {
                    domOption.find('img.option-img').attr('src', 'assets/images/global-icons/tree/path/'+ (pathNumber) +'.svg');
                }
                domOption.find('.option-title').html(option.title);
                domOption.find('.option-description').html(option.body);
                pathNumber++;
            }
            var bindEvents = function () {
                domOption.click(function () {
                    domOptionsList.find('.option-item').removeClass('active');
                    domOption.addClass('active');
                    onClick(option);
                });
            }
            this.init = function () {
                domOption = domOptionsList.find('.option-item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domOptionsList);
                bindData();
                bindEvents();
            }
        }
        this.onSelect = function (fn) {
            onClick = fn;
        }
        this.init = function () {
            domOptionsList = domRoot.find('.options-list');
            bindData();
        }
    }
})();