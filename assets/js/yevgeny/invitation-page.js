(function () {
    'use strict';

    var is_loading_controlled_in_local = false;
    var invitationHandle;

    var InvitationClass = function () {

        var domRoot = $('main.main-content');
        var domForm = domRoot.find('form');
        var domInvitationCnt = $('.remaining-invitations');

        var invitationCnt = initialInvitationCnt;

        var updateInvitationCnt = function (newCnt) {
            invitationCnt = newCnt;
            if (invitationCnt > 1){
                domInvitationCnt.html(invitationCnt + ' invitations');
            }
            else if (invitationCnt == 1){
                domInvitationCnt.html(invitationCnt + ' invitation');
            }
            else {
                domRoot.find('.content-inner').removeClass('can-invite');
            }
        }

        var inviteFriend = function (friendEmail) {
            if (invitationCnt == 0){

            }
            var ajaxData = {
                action: 'invite_friend',
                friendEmail: friendEmail
            };

            return $.ajax({
                url: ACTION_URL,
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if (res.status){
                        swal("SUCCESS!", "Thank you for sending an Invitation.", "success", {
                            button: "OK"
                        });
                        updateInvitationCnt(invitationCnt - 1);
                    }
                }
            });
        }

        var bindEvents = function () {
            domForm.submit(function () {
                inviteFriend(domForm.find('input[name=email]').val());
                return false;
            });
        }

        this.init = function () {
            bindEvents();
        }
    }

    invitationHandle = new InvitationClass();
    invitationHandle.init();

    $(document).ajaxStart(function () {
        if(!is_loading_controlled_in_local) {
            $('.loading').css('display', 'block');
        }
    });
    $(document).ajaxComplete(function () {
        if(!is_loading_controlled_in_local){
            $('.loading').css('display', 'none');
        }
    });
})();