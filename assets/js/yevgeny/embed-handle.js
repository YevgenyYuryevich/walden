(function () {
    'use strict';
    var EmbedPlatformClass = function () {
        var domRoot, domOriginRoot, domParent, domIframe;
        var SOURCE_BASE_URL, inlineStyle, externalStyle, url, externalScript, inlineScript;
        var embedId;

        var place = function () {

            domParent.empty();
            var h = parseInt(domParent.css('height'));

            h -= parseInt(domParent.css('padding-top'));
            h -= parseInt(domParent.css('padding-bottom'));

            var calcHeight = h;
            var bodyHeight = window.innerHeight;

            bodyHeight -=  parseInt($('html').css('margin-top'));
            bodyHeight -=  parseInt($('html').css('margin-bottom'));
            bodyHeight -=  parseInt($('html').css('padding-top'));
            bodyHeight -=  parseInt($('html').css('padding-bottom'));

            bodyHeight -=  parseInt($('body').css('margin-top'));
            bodyHeight -=  parseInt($('body').css('margin-bottom'));
            bodyHeight -=  parseInt($('body').css('padding-top'));
            bodyHeight -=  parseInt($('body').css('padding-bottom'));

            if (h === 0) {
                calcHeight = bodyHeight;
            }
            else {
                if (h < 300) {
                    calcHeight = bodyHeight;
                }
                else {
                    calcHeight = h;
                }
            }
            var uri = url + '&externalStyle=' + externalStyle + '&inlineStyle=' + inlineStyle + '&externalScript=' + externalScript + '&inlineScript=' + inlineScript + '&embedId=' + embedId;
            domRoot = domParent.hasClass('walden-embedly-card-wrapper') ? domParent : $('<div class="walden-embedly-card-wrapper" style="line-height: 0"></div>').appendTo(domParent);
            domIframe = $('<iframe width="100%" frameborder="0" style="min-height: 200px;" src="'+ uri +'"></iframe>').appendTo(domRoot);
            domIframe.css('max-height', domParent.css('max-height'));
            domIframe.css('min-height', domParent.css('min-height'));
            domIframe.css('height', calcHeight);
            bindEvents();
        }

        var bindEvents = function () {
            domIframe.on('load', function (e) {

            });
        }
        var receiveMessage = function(e) {
            if ( e.data.embedId && e.data.embedId == embedId && e.data.name && e.data.name == 'currentHeight') {
                domIframe.css('height', e.data.height + 'px');
            }
        }
        this.init = function () {
            domOriginRoot = $('blockquote.walden-embedly-card');
            domParent = domOriginRoot.parent();

            SOURCE_BASE_URL = domOriginRoot.find('+ script').attr('src');

            externalStyle = domParent.data('externalStyle');
            externalStyle = externalStyle ? externalStyle : false;
            inlineStyle = domParent.length ? $('style#walden-embedly-style', domParent).text() : $('style#walden-embedly-style').text();
            inlineStyle = inlineStyle ? encodeURIComponent(inlineStyle) : false;

            externalScript = domParent.data('externalScript');
            externalScript = externalScript ? externalScript : false;
            inlineScript = domParent.length ? $('script#walden-embedly-script', domParent).text() : $('script#walden-embedly-script').text();
            inlineScript = inlineScript ? encodeURIComponent(inlineScript) : false;

            var parsed = SOURCE_BASE_URL.match(/http(s?):\/\/[^\/]*/);
            url = domOriginRoot.find('h4 a').attr('href');
            SOURCE_BASE_URL = parsed[0];
            embedId = localStorage.getItem('waldenEmbedCnt');
            embedId = embedId ? embedId : 1;
            localStorage.setItem('waldenEmbedCnt', embedId * 1 + 1);
            window.addEventListener("message", receiveMessage);
            place();
        }
    };

    var embedPlatformHandle = new EmbedPlatformClass();
    embedPlatformHandle.init();
})();