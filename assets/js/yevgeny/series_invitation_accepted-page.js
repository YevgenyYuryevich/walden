(function () {

    'use strict';
    var PageClass = function () {

        var domRoot;

        var seriesHandle, seriesConversationHandle;

        var bindEvents = function () {
            domRoot.find('.invite-friend-form').submit(function () {
                var v = $(this).find('[name="emails"]').val();
                var emails = v.split(/\s*,+\s*/);
                seriesHandle.invite(emails);
                return false;
            });
        }
        var SeriesItemClass = function () {

            var domSeriesItem, domSeriesHeader, domPostsList, domSeriesIncomeWrapper;

            var itemHandles = [], filteredHandles = [], copyEmbedHandle;

            var PostItemClass = function (itemData) {
                var domItem, domBody, domTypeBody;
                var dataLoaded = false;
                var deleteItem = function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: itemData.post_ID}).then(function (res) {
                        if (res.status) {
                            itemHandles.forEach(function (value, i) {
                                if (value.data().post_ID === itemData.post_ID) {
                                    itemHandles.splice(i, 1);
                                    domItem.remove();
                                }
                            });
                        }
                    });
                }
                var bindData = function () {
                    dataLoaded = true;
                    domItem.addClass('type-' + itemData.intPost_type);
                    domItem.find('.day-value').html(itemData.viewDay);
                    switch (parseInt(itemData.intPost_type)) {
                        case 0:
                            domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            break;
                        case 2:
                            domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.find('img').attr('src', itemData.strPost_featuredimage);
                            break;
                        default:
                            domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.html(itemData.strPost_body);
                            break;
                    }
                    domItem.find('.item-title').html(itemData.strPost_title);
                    domItem.find('.item-title').attr('href', 'view_blog?id=' + itemData.post_ID);
                    if (itemData.strPost_duration) {
                        domBody.find('.blog-duration').html(itemData.strPost_duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                            domBody.find('.blog-duration').html(res);
                            itemData.strPost_duration = res;
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                        });
                    }
                }
                var bindEvents = function () {
                    domItem.find('.item-action.delete-action').click(function (e) {
                        swal({
                            title: "Are you sure?",
                            text: "Do you want to delete this post?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                deleteItem();
                            }
                        });
                    });
                    domBody.click(function () {
                        window.open('view_blog?id=' + itemData.post_ID, '_blank ');
                    });
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domPostsList);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = bindData;
                this.isDataLoaded = function () {
                    return dataLoaded;
                }
                this.init = function () {
                    domItem = domSeriesItem.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    domBody = domItem.find('.item-type-body');
                    bindEvents();
                }
            }

            this.invite = function (emails) {
                var ajaxData = {
                    action: 'invite',
                    id: series.series_ID,
                    emails: emails,
                };
                ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Your invitation are sent successfully',
                    }).then(function (value) {
                        if (value == 'return_home'){
                        }
                        else {
                        }
                    });
                });
            }

            var bindData = function () {
                domSeriesHeader.find('.action-item.edit-action').attr('href', 'edit_series?id=' + series.series_ID);
            }

            var bindEvents = function () {
                domPostsList.on('afterChange', function (e, slick, currentSlide) {
                    var loadIndex = currentSlide + 3;
                    for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                        if (itemHandles[i].isDataLoaded()) {
                            continue;
                        }
                        itemHandles[i].bindData();
                    }
                });
            }
            this.data = function () {
                return series;
            }
            this.refreshPosts = function () {
                domPostsList.css('height', domPostsList.css('height'));
                domPostsList.css('opacity', 0);
                setTimeout(function () {
                    domPostsList.slick('unslick');
                    filteredHandles.forEach(function (value) {
                        value.detach();
                    });
                    switch (sortBy) {
                        case 'audio':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().intPost_type == 0;
                            });
                            break;
                        case 'video':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().intPost_type == 2;
                            });
                            break;
                        case 'text':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().intPost_type == 7 || value.data().intPost_type == 8 || value.data().intPost_type == 10;
                            });
                            break;
                        case 'A-z':
                            filteredHandles = itemHandles;
                            filteredHandles.sort(function (a, b) {
                                if (a.data().strPost_title < b.data().strPost_title) {
                                    return 1;
                                }
                                else if (a.data().strPost_title == b.data().strPost_title) {
                                    return 0;
                                }
                                return -1;
                            });
                            break;
                        case 'Z-a':
                            filteredHandles = itemHandles;
                            filteredHandles.sort(function (a, b) {
                                if (a.data().strPost_title > b.data().strPost_title) {
                                    return 1;
                                }
                                else if (a.data().strPost_title == b.data().strPost_title) {
                                    return 0;
                                }
                                return -1;
                            });
                            break;
                        default:
                            filteredHandles = itemHandles;
                            break;
                    }
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domPostsList.css('height', 'auto');
                    domPostsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        nextArrow: domSeriesItem.find('.arrow-right'),
                        prevArrow: domSeriesItem.find('.arrow-left'),
                        responsive: [
                        ]
                    });
                    domPostsList.css('opacity', 1);
                }, 300);
            }
            this.refreshSlick = function () {
                domPostsList.slick('setPosition');
            }
            this.init = function () {
                domSeriesItem = domRoot.find('.series-item');
                domSeriesHeader = domSeriesItem.find('.series-content-header');
                domPostsList = domSeriesItem.find('.posts-list');
                domSeriesIncomeWrapper = domSeriesItem.find('.series-income-wrapper');
                copyEmbedHandle = new SeriesCopyEmbedClass(domSeriesHeader.find('.component.series-copy-embed'), series);
                copyEmbedHandle.init();
                bindData();
                bindEvents();
                series.posts.forEach(function (itemData, i) {
                    var hdl = new PostItemClass(itemData);
                    hdl.init();
                    hdl.append();
                    if (i < 7) {
                        hdl.bindData();
                    }
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                });
                domPostsList.slick({
                    dots: false,
                    infinite: false,
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    nextArrow: domSeriesItem.find('.arrow-right'),
                    prevArrow: domSeriesItem.find('.arrow-left'),
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 960,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        },
                    ]
                });
            }
        }
        this.init = function () {
            domRoot = $('.site-content .main-content');
            if (parseInt(CLIENT_ID) !== -1) {
                seriesHandle = new SeriesItemClass();
                seriesHandle.init();
                seriesConversationHandle = new SeriesConversationClass(domRoot.find('.series-conversation-component'), series.series_ID, {});
                seriesConversationHandle.init();
                bindEvents();
            }
            else {
                authHandle.setFields({
                    loginMessage: 'Congratulations on joining this experience! \nPlease login to start contributing.',
                    email: invitedEmail
                });
                authHandle.popup();
                authHandle.afterLogin(function () {
                    seriesHandle = new SeriesItemClass();
                    seriesHandle.init();
                    bindEvents();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'accept_invitation', id: invitationId}, false).then(function () {
                        seriesConversationHandle = new SeriesConversationClass(domRoot.find('.series-conversation-component'), series.series_ID, {});
                        seriesConversationHandle.init();
                    });
                });
                authHandle.afterRegister(function () {
                    seriesHandle = new SeriesItemClass();
                    seriesHandle.init();
                    bindEvents();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'accept_invitation', id: invitationId}, false).then(function () {
                        seriesConversationHandle = new SeriesConversationClass(domRoot.find('.series-conversation-component'), series.series_ID, {});
                        seriesConversationHandle.init();
                    });
                });
            }
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();