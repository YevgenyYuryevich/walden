var is_loading_controlled_in_local = false;

(function() {

    var postsSectionHandle = null;

    var PostsSectionClass = function () {

        var totalPosts = initSeries;
        var currentIndex = 0;
        var blockSize = 10;
        var postsBlockHandles = [];

        var domRoot = $('main.main-content');
        var PostsBlockClass = function (posts) {
            var domGrid = domRoot.find('.tt-grid.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('.tt-grid-wrapper'));

            var grid = domGrid.get(0),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;
            var itemHandles = [];

            var ItemClass = function (itemData) {
                var domItemRoot = null, domTitleWrp;
                var domTitle = null;
                var self = this;
                var bindEvents = function () {
                    domItemRoot.find('.select-post').parent().click(function () {
                        if (CLIENT_ID > -1){
                            parseInt(itemData.purchased) ? self.unSelectPost() : self.selectPost();
                        }
                        else{
                            swal({
                                title: "Please Login first!",
                                icon: "warning",
                                dangerMode: true,
                                buttons: {
                                    cancel: "Cancel",
                                    login: "Login"
                                },
                            }).then(function(value){
                                if (value == 'login'){
                                    window.location.href = 'login';
                                }
                            });
                        }
                    });
                }
                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', itemData.strSeries_image);
                    cloneDom.find('.title-wrapper .item-title').html(itemData.strSeries_title);
                    cloneDom.find('.view-post').attr('href', BASE_URL + '/viewexperience.php?id=' + itemData.series_ID + '&prev=' + CURRENT_PAGE);
                    return cloneDom.html();
                }
                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                }
                this.selectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    btn.html("");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (itemData.purchased){
                                btn.removeClass("circle");
                                btn.addClass("filled");
                                $(".wrap img", domItemRoot).css("display", "block");
                                $("svg", domItemRoot).css("display", "none");
                                clearInterval(timer);
                                setTimeout(function () {
                                    var s = localStorage.getItem('thegreyshirt_show_popup_every_join');
                                    if (s == null || s === '1'){
                                        popupThanksJoin(itemData.purchased);
                                    }
                                }, 500);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    $.ajax({
                        url: API_ROOT_URL + '/Series.php',
                        data: {action: 'join', id: itemData.series_ID},
                        success: function (res) {
                            if (res.status == true){
                                itemData.purchased = res.data;
                            }
                            is_loading_controlled_in_local = false;
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                this.unSelectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (!itemData.purchased){
                                btn.removeClass("circle");
                                btn.html("Join");
                                $(".wrap img", domItemRoot).css("display", "none");
                                $("svg", domItemRoot).css("display", "none");
                                clearInterval(timer);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    $.ajax({
                        url: API_ROOT_URL + '/Series.php',
                        data: {action: 'unJoin', id: itemData.purchased},
                        success: function (res) {
                            if (res.status == true){
                                itemData.purchased = 0;
                            }
                            is_loading_controlled_in_local = true;
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                this.readMore = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                }
                this.init = function () {
                    if (parseInt(itemData.purchased)){
                        domItemRoot.find('.wrap .select-post').addClass('filled');
                        domItemRoot.find('.wrap .select-post').html('');
                        domItemRoot.find('.wrap img').show();
                    }
                    self.readMore();
                    bindEvents();
                }
            }
            var self = this;
            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = $(el).find( '> *' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild.length ) {
                        itemChild.addClass('tt-old');
                    }
                } );

                // apply effect
                setTimeout( function() {
                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find( '> *:last-child' ));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );


                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            };

            this.init = function () {
                posts.forEach(function (post) {
                    var itemHandle = new ItemClass(post);
                    itemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
            }
        }

        var bindEvent = function () {
            $('.site-wrapper.page').scroll(function (e) {
                if ($('.site-wrapper.page').hasClass('header-search-opened')){
                    return true;
                }
                var elem = $(e.currentTarget);
                if (elem[0].scrollHeight - elem.scrollTop() <= elem.outerHeight() + 10){
                    addPostsBlock();
                    addPostsBlock();
                }
            })
        }

        var addPostsBlock = function () {
            var blockPosts = totalPosts.slice(currentIndex, currentIndex + blockSize);
            currentIndex += blockPosts.length;
            if (!blockPosts.length){
                return false;
            }
            var postsBlockHandle = new PostsBlockClass(blockPosts);
            postsBlockHandle.init();
            postsBlockHandles.push(postsBlockHandle);
            return postsBlockHandle;
        }
        var viewUserPage = function (data) {
            $('<form action="userpage" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
        }
        var popupThanksJoin = function (data) {
            swal({
                icon: "success",
                title: 'SUCCESS!',
                text: 'Thanks for joining! Would you like to go to your experiences?',
                buttons: {
                    returnHome: {
                        text: "Go to my Experiences",
                        value: 'return_home',
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    customize: {
                        text: "Not yet",
                        value: 'customize',
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                closeOnClickOutside: false,
            }).then(function (value) {
                if (value == 'return_home'){
                    viewUserPage(data);
                }
                else {
                }
            });
            var domSwalRoot, domDisableCheckbox;
            var disablePopup = function () {
                localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
            }
            var enabelPopup = function () {
                localStorage.removeItem('thegreyshirt_show_popup_every_join');
            }
            var bindEvents = function () {
                domDisableCheckbox.change(function () {
                    $(this).prop('checked') ? disablePopup() : enabelPopup();
                })
            }
            var init = function () {
                setTimeout(function () {
                    domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                    domSwalRoot.addClass('thanks-join');
                    domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                    domDisableCheckbox = domSwalRoot.find('#disable_popup');
                    bindEvents();
                }, 100);
            }();
        }
        this.init = function () {
            addPostsBlock();
            bindEvent();
            siteHeaderHandle.scrollFix({
                container: $('.site-wrapper.page'),
                domReplace: $('.scroll-fix-rplc').clone().removeAttr('hidden'),
            });
        }
    }

    jQuery(document).ready(function () {
        postsSectionHandle = new PostsSectionClass();
        postsSectionHandle.init();
    })
})();

$(document).ajaxStart(function () {
    if(!is_loading_controlled_in_local) {
        $('.loading').css('display', 'block');
    }
});
$(document).ajaxComplete(function () {
    if(!is_loading_controlled_in_local){
        $('.loading').css('display', 'none');
    }
});