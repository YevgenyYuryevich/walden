(function () {
    'use strict';

    var PageClass = function () {
        var domPage, domCurrentSeries;
        var series = [], posts = [];

        var filterCategory = -1, filterSeries = -1, sortBy = -1;
        var categoryTitle;
        var filterHandle, seriesListHandle, postsListHandle;

        var FilterClass = function () {

            var domRoot, domSortByDropDown, domCategoryDropDown;
            var bindEvents = function () {
                domRoot.find('.filter-category').click(function () {
                    domRoot.find('.filter-category').removeClass('active');
                    $(this).addClass('active');
                    filterSeries = -1;
                    filterCategory = $(this).data('category');
                    seriesListHandle.refresh();
                    postsListHandle.refresh();
                    setTimeout(function () {
                        seriesListHandle.dom().find('.item').removeClass('active');
                        seriesListHandle.dom().find('.item:nth-child(2)').addClass('active');
                    }, 300);
                });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    sortBy = v;
                    //    handle actions here
                    postsListHandle.refresh();
                });
                domCategoryDropDown.find('ul li a').click(function () {
                    domCategoryDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('category');
                    var txt = $(this).text();
                    domCategoryDropDown.data('value', v);
                    domCategoryDropDown.find('button span').text(txt);
                    if (v == -1) {
                        domCurrentSeries.text('All Series');
                    }
                    else {
                        domCurrentSeries.text('All Series of ' + txt);
                        categoryTitle = txt;
                    }
                });
            }
            this.init = function () {
                domRoot = domPage.find('.filter-wrapper');
                domSortByDropDown = domRoot.find('.sort-by .dropdown');
                domCategoryDropDown = domRoot.find('.categories-container-mobile .dropdown');
                bindEvents();
            }
        }
        var SeriesListClass = function () {
            var domRoot;
            var itemHandles = [], filteredHandles = [];

            var ItemClass = function (itemData) {
                var domItem;
                var bindEvents = function () {
                    domItem.click(function () {
                        filterSeries = itemData.series_ID;
                        domRoot.find('.item').removeClass('active');
                        domItem.addClass('active');
                        if (filterCategory == -1) {
                            domCurrentSeries.text(itemData.strSeries_title);
                        }
                        else if (filterSeries == -1) {
                            domCurrentSeries.text('All series of ' + categoryTitle);
                        }
                        else {
                            domCurrentSeries.text(itemData.strSeries_title);
                        }
                        postsListHandle.refresh();
                    });
                }
                var bindData = function () {
                    domItem.find('span').html(itemData.strSeries_title);
                    domItem.addClass('level-' + itemData.boolSeries_level);
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domRoot);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.init = function () {
                    domItem = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                    if (itemData.series_ID == -1) {
                        domItem.addClass('all-item');
                    }
                    bindData();
                    bindEvents();
                }
            }
            this.dom = function () {
                return domRoot;
            }
            this.refresh = function () {
                domRoot.css('height', domRoot.css('height'));
                domRoot.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (value) {
                        value.detach();
                    });
                    filteredHandles = itemHandles.filter(function (value) {
                        if (filterCategory == -1) {
                            return true;
                        }
                        if (value.data().intSeries_category == -1 || value.data().intSeries_category == filterCategory) {
                            return true;
                        }
                        return false;
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domRoot.css('height', 'auto');
                    domRoot.css('opacity', 1);
                }, 300);
            }
            this.init = function () {
                series.sort(function (a, b) {
                    if (b.series_ID == -1) {
                        return 1;
                    }
                    if (a.boolSeries_level > b.boolSeries_level) {
                        return 1;
                    }
                    else if (a.boolSeries_level === b.boolSeries_level) {
                        return 0;
                    }
                    return -1;
                });
                domRoot = domPage.find('.main-content .series-col .series-list');
                series.forEach(function (value) {
                    var hdl = new ItemClass(value);
                    hdl.init();
                    hdl.append();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                });
                domRoot.find('.item:nth-child(2)').addClass('active');
            }
        }

        var PostsListClass = function () {

            var domRoot;

            var itemHandles = [], filteredHandles = [];

            var ItemClass = function (itemData) {
                var domItem, domBody, domTypeBody;
                var bindEvents = function () {
                    domBody.click(function () {
                        window.open('view_blog?id=' + itemData.post_ID, '_blank ');
                    });
                }
                var bindData = function () {
                    domItem.addClass('type-' + itemData.intPost_type);
                    domItem.find('.day-value').html(itemData.viewDay);
                    switch (parseInt(itemData.intPost_type)) {
                        case 0:
                            domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            break;
                        case 2:
                            domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.find('img').attr('src', itemData.strPost_featuredimage);
                            break;
                        default:
                            domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                            domTypeBody.html(itemData.strPost_body);
                            break;
                    }
                    domItem.find('.item-title').html(itemData.strPost_title).attr('href', 'view_blog?id=' + itemData.post_ID);
                    domItem.find('.item-series').html(itemData.series.strSeries_title).attr('href', 'preview_series?id=' + itemData.series.series_ID);

                    if (itemData.strPost_duration) {
                        domBody.find('.blog-duration').html(itemData.strPost_duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                            domBody.find('.blog-duration').html(res);
                            itemData.strPost_duration = res;
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                        });
                    }

                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domRoot);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.init = function () {
                    domItem = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                    domBody = domItem.find('.item-type-body');
                    bindData();
                    bindEvents();
                }
            }
            this.refresh = function () {
                domRoot.css('height', domRoot.css('height'));
                domRoot.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (value) {
                        value.detach();
                    });
                    filteredHandles = itemHandles.filter(function (value) {
                        if (filterCategory == -1) {
                            if (filterSeries == -1) {
                                return true;
                            }
                            else if (filterSeries == value.data().intPost_series_ID) {
                                return true;
                            }
                        }
                        else {
                            if (filterSeries == -1) {
                                return value.data().category == filterCategory;
                            }
                            else {
                                return value.data().intPost_series_ID == filterSeries;
                            }
                        }
                        return false;
                    });
                    switch (sortBy) {
                        case 'audio':
                            filteredHandles = filteredHandles.filter(function (value) {
                                return value.data().intPost_type == 0;
                            });
                            break;
                        case 'video':
                            filteredHandles = filteredHandles.filter(function (value) {
                                return value.data().intPost_type == 2;
                            });
                            break;
                        case 'A-z':
                            filteredHandles.sort(function (a, b) {
                                if (a.data().strPost_title < b.data().strPost_title) {
                                    return 1;
                                }
                                else if (a.data().strPost_title == b.data().strPost_title) {
                                    return 0;
                                }
                                return -1;
                            });
                            break;
                        case 'Z-a':
                            filteredHandles.sort(function (a, b) {
                                if (a.data().strPost_title > b.data().strPost_title) {
                                    return 1;
                                }
                                else if (a.data().strPost_title == b.data().strPost_title) {
                                    return 0;
                                }
                                return -1;
                            });
                            break;
                        default:
                            break;
                    }
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domRoot.css('height', 'auto');
                    domRoot.css('opacity', 1);
                }, 300);
            }
            this.init = function () {
                domRoot = domPage.find('.main-content .posts-col .posts-container');
                posts.forEach(function (value) {
                    var hdl = new ItemClass(value);
                    hdl.init();
                    hdl.append();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                })
            }
        }
        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            domCurrentSeries = domPage.find('.current-series');
            series.push({
                series_ID: -1,
                intSeries_category: -1,
                boolSeries_level: 0,
                strSeries_title: 'All Series',
                posts: [],
            });
            categories.forEach(function (category) {
                series = series.concat(category.series);
            });
            series.forEach(function (value) {
                value.posts.forEach(function (post) {
                    post.category = value.intSeries_category;
                    post.series = value;
                });
                posts = posts.concat(value.posts);
            });

            filterHandle = new FilterClass();
            filterHandle.init();

            seriesListHandle = new SeriesListClass();
            seriesListHandle.init();

            postsListHandle = new PostsListClass();
            postsListHandle.init();

        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();