(function () {
    'use strict';
    let PageClass = function () {
        let domMain, domHeader;
        let diamondsHandle, contentChannelHandle;
        let bindEvents = function () {
            domHeader.find('.search-input-wrapper').submit(function (e) {
                e.preventDefault();
                let keyword = domHeader.find('.search-input-wrapper input').val();
                siteSearchHandle.applyUserSeriesSearch(user.id, keyword);
                return false;
            });
        }
        var DiamondsClass = function () {
            // from https://uigradients.com

            var gradients = [
                'linear-gradient(to right, #005aa7, #fffde4)',
                'linear-gradient(to right, #02111d, #037bb5, #02111d)',
                'linear-gradient(to right, #ffe259, #ffa751)',
                'linear-gradient(to top, #acb6e5, #86fde8)',
                'linear-gradient(to top, #536976, #292e49)',
                'linear-gradient(to right, #bbd2c5, #536976, #292e49)',
                'linear-gradient(to right, #b79891, #94716b)',
                'linear-gradient(to right, #bbd2c5, #536976)',
                'linear-gradient(to left, #076585, #fff)',
                'linear-gradient(to left, #00467f, #a5cc82)',
                'linear-gradient(to left, #1488cc, #2b32b2)',
                'linear-gradient(to top, #ec008c, #fc6767)',
                'linear-gradient(to top, #cc2b5e, #753a88)',
                'linear-gradient(to top, #2193b0, #6dd5ed)',
                'linear-gradient(to bottom, #e65c00, #f9d423)',
                'linear-gradient(to top, #2b5876, #4e4376)',
                'linear-gradient(to right, #314755, #26a0da)',
                'linear-gradient(to right, #77a1d3, #79cbca, #e684ae)',
                'linear-gradient(to left, #ff6e7f, #bfe9ff)',
                'linear-gradient(to right, #e52d27, #b31217)',
                'linear-gradient(to top, #603813, #b29f94)',
                'linear-gradient(to top, #16a085, #f4d03f)',
                'linear-gradient(to left, #d31027, #ea384d)',
                'linear-gradient(to left, #ede574, #e1f5c4)',
                'linear-gradient(to left, #02aab0, #00cdac)',
                'linear-gradient(to top, #da22ff, #9733ee)',
                'linear-gradient(to left, #348f50, #56b4d3)',
                'linear-gradient(to bottom, #3ca55c, #b5ac49)',
                'linear-gradient(to left, #cc95c0, #dbd4b4, #7aa1d2)',
                'linear-gradient(to bottom, #003973, #e5e5be)',
                'linear-gradient(to bottom, #e55d87, #5fc3e4)',
                'linear-gradient(to right, #403b4a, #e7e9bb)',
                'linear-gradient(to top, #f09819, #edde5d)',
                'linear-gradient(to top, #ff512f, #dd2476)',
                'linear-gradient(to right, #aa076b, #61045f)',
                'linear-gradient(to right, #1a2980, #26d0ce)',
                'linear-gradient(to bottom, #ff512f, #f09819)',
                'linear-gradient(to bottom, #1d2b64, #f8cdda)',
                'linear-gradient(to right, #1fa2ff, #12d8fa, #a6ffcb)',
                'linear-gradient(to right, #4cb8c4, #3cd3ad)'
            ];

            var domDiamondsWrapper = domMain.find('.diamonds-wrapper');
            var domDiamonds = domDiamondsWrapper.find('.diamonds-container');

            let index = Math.floor(userSeries.length * Math.random());
            let topRightSeries = userSeries.length ? userSeries[index] : false;
            var series = userSeries.filter(function (series, i) {
                return i !== index;
            });
            var screenKind = 1;
            var fillCount = 27;
            var size = 300;

            var makeVirtualDiamonds = function(size){
                var cloneDoms = [];
                while (size > 0){
                    var cloneDom = domDiamondsWrapper.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                    cloneDom.attr('href', 'javascript:;');
                    cloneDom.removeAttr('target');
                    var randKey = Math.floor(Math.random() * gradients.length);
                    cloneDom.css('background-image', gradients[randKey]);
                    cloneDoms.push(cloneDom);
                    size--;
                }
                return cloneDoms;
            }
            var bindEvents = function () {
                $(window).resize(function () {
                    var newScreenKind = getScreenKind();
                    if (newScreenKind !== screenKind){
                        domDiamonds.diamonds("destroy");
                        domDiamonds.empty();
                        fillMainDiamonds();
                        fillSides();
                    }
                });
            }

            var getScreenKind = function () {
                var windowWidth = $(window).width();

                if (windowWidth >= 1637){
                    return 1;
                }
                else if (windowWidth >= 1320){
                    return 2;
                }
                else if (windowWidth >= 1020){
                    return 3;
                }
                else if (windowWidth >= 768){
                    return 4;
                }
                return 5;
            }
            var shuffle = function(array) {
                var currentIndex = array.length, temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;
            }

            var fillMainDiamonds = function () {
                screenKind = getScreenKind();
                switch (screenKind){
                    case 1:
                        size = 300;
                        fillCount = 27;
                        break;
                    case 2:
                        size = 300;
                        fillCount = 21;
                        break;
                    case 3:
                        size = 300;
                        fillCount = 20;
                        break;
                    case 4:
                        size = 200;
                        fillCount = 20;
                        break;
                    case 5:
                        size = 200;
                        fillCount = 15;
                    default:
                        break;
                }
                var cloneDoms = [];
                for (var i = 0; i < fillCount && i < series.length; i++){
                    var post = series[i];
                    var cloneDom = domDiamondsWrapper.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                    cloneDom.attr('href', 'preview_series?id=' + post.series_ID);
                    cloneDom.css('background-image', 'url("'+ post.strSeries_image +'")');
                    cloneDoms.push(cloneDom);
                }

                var virtualDoms = makeVirtualDiamonds(fillCount - series.length);
                cloneDoms = cloneDoms.concat(virtualDoms);

                cloneDoms = shuffle(cloneDoms);

                cloneDoms.forEach(function (cloneDom) {
                    cloneDom.appendTo(domDiamonds);
                });
                domDiamonds.diamonds({
                    size: size,
                    gap: 5,
                    hideIncompleteRow: false,
                    autoReDraw: false,
                    itemSelector: '.item'
                });
            }

            var getCloneDomByPos = function (rowNumber, isUpper, offset) {
                var domRow = domDiamonds.find('.diamond-row-wrap').eq(rowNumber - 1);
                var domLayer = isUpper ? domRow.find('.diamond-row-upper') : domRow.find('.diamond-row-lower');
                return domLayer.find('.diamond-box-wrap').eq(offset - 1).clone();
            }

            var fillSides = function () {
                domDiamondsWrapper.find('.diamond-box-wrap.for-side:not(.sample)').remove();
                var rowsCount = domDiamonds.find('>.diamonds').find('.diamond-row-wrap').length;

                var cloneDoms = [];
                var size = rowsCount * 2;
                while (size > 0){
                    var cloneDom = domDiamondsWrapper.find('.diamond-box-wrap.for-side.sample').clone().removeClass('sample').removeAttr('hidden');
                    cloneDom.find('.item').attr('href', 'javascript:;');
                    var randKey = Math.floor(Math.random() * gradients.length);
                    cloneDom.find('.item').css('background-image', gradients[randKey]);
                    cloneDoms.push(cloneDom);
                    size--;
                }

                var restPosts = series.slice(fillCount, fillCount + rowsCount * 2);

                restPosts.forEach(function (post) {
                    var cloneDom = domDiamondsWrapper.find('.diamond-box-wrap.for-side.sample').clone().removeClass('sample').removeAttr('hidden');
                    cloneDom.find('.item').attr('href', 'view.php?id=' + post.post_ID + '&prevpage=viewexperience');
                    cloneDom.find('.item').css('background-image', 'url("'+ post.strPost_featuredimage +'")');
                    cloneDoms.push(cloneDom);
                });

                switch (screenKind){
                    case 1:
                        cloneDoms.push(getCloneDomByPos(1, true, 3));
                        cloneDoms.push(getCloneDomByPos(1, true, 4));
                        cloneDoms.push(getCloneDomByPos(1, false, 3));

                        cloneDoms.push(getCloneDomByPos(1, false, 1));
                        cloneDoms.push(getCloneDomByPos(2, true, 1));
                        cloneDoms.push(getCloneDomByPos(2, true, 2));
                        cloneDoms.push(getCloneDomByPos(2, false, 1));

                        cloneDoms.push(getCloneDomByPos(2, false, 4));
                        cloneDoms.push(getCloneDomByPos(3, true, 4));
                        cloneDoms.push(getCloneDomByPos(3, true, 5));
                        cloneDoms.push(getCloneDomByPos(3, false, 4));

                        break;
                    case 2:
                        cloneDoms.push(getCloneDomByPos(1, true, 3));
                        cloneDoms.push(getCloneDomByPos(1, true, 4));
                        cloneDoms.push(getCloneDomByPos(1, false, 3));

                        cloneDoms.push(getCloneDomByPos(1, false, 1));
                        cloneDoms.push(getCloneDomByPos(2, true, 1));
                        cloneDoms.push(getCloneDomByPos(2, true, 2));
                        cloneDoms.push(getCloneDomByPos(2, false, 1));

                        cloneDoms.push(getCloneDomByPos(2, false, 3));
                        cloneDoms.push(getCloneDomByPos(3, true, 3));
                        cloneDoms.push(getCloneDomByPos(3, true, 4));
                        cloneDoms.push(getCloneDomByPos(3, false, 3));

                        break;
                    case 3:
                        cloneDoms.push(getCloneDomByPos(1, true, 2));
                        cloneDoms.push(getCloneDomByPos(1, true, 3));
                        cloneDoms.push(getCloneDomByPos(1, false, 2));

                        cloneDoms.push(getCloneDomByPos(2, false, 1));
                        cloneDoms.push(getCloneDomByPos(3, true, 1));
                        cloneDoms.push(getCloneDomByPos(3, true, 2));
                        cloneDoms.push(getCloneDomByPos(3, false, 1));

                        cloneDoms.push(getCloneDomByPos(3, false, 2));
                        cloneDoms.push(getCloneDomByPos(4, true, 2));
                        cloneDoms.push(getCloneDomByPos(4, true, 3));
                        cloneDoms.push(getCloneDomByPos(4, false, 2));

                        break;
                    case 4:
                        cloneDoms.push(getCloneDomByPos(1, true, 2));
                        cloneDoms.push(getCloneDomByPos(1, true, 3));
                        cloneDoms.push(getCloneDomByPos(1, false, 2));

                        cloneDoms.push(getCloneDomByPos(2, false, 1));
                        cloneDoms.push(getCloneDomByPos(3, true, 1));
                        cloneDoms.push(getCloneDomByPos(3, true, 2));
                        cloneDoms.push(getCloneDomByPos(3, false, 1));

                        cloneDoms.push(getCloneDomByPos(3, false, 2));
                        cloneDoms.push(getCloneDomByPos(4, true, 2));
                        cloneDoms.push(getCloneDomByPos(4, true, 3));
                        cloneDoms.push(getCloneDomByPos(4, false, 2));
                        break;
                    case 5:
                        cloneDoms.push(getCloneDomByPos(1, true, 1));
                        cloneDoms.push(getCloneDomByPos(1, true, 2));
                        cloneDoms.push(getCloneDomByPos(1, false, 1));

                        cloneDoms.push(getCloneDomByPos(2, false, 1));
                        cloneDoms.push(getCloneDomByPos(3, true, 1));
                        cloneDoms.push(getCloneDomByPos(3, true, 2));
                        cloneDoms.push(getCloneDomByPos(3, false, 1));

                        cloneDoms.push(getCloneDomByPos(4, false, 1));
                        cloneDoms.push(getCloneDomByPos(5, true, 1));
                        cloneDoms.push(getCloneDomByPos(5, true, 2));
                        cloneDoms.push(getCloneDomByPos(5, false, 1));
                        break;
                    default:
                        cloneDoms.push(getCloneDomByPos(1, true, 3));
                        cloneDoms.push(getCloneDomByPos(1, true, 4));
                        cloneDoms.push(getCloneDomByPos(1, false, 3));

                        cloneDoms.push(getCloneDomByPos(1, false, 1));
                        cloneDoms.push(getCloneDomByPos(2, true, 1));
                        cloneDoms.push(getCloneDomByPos(2, true, 2));
                        cloneDoms.push(getCloneDomByPos(2, false, 1));

                        cloneDoms.push(getCloneDomByPos(2, false, 4));
                        cloneDoms.push(getCloneDomByPos(3, true, 4));
                        cloneDoms.push(getCloneDomByPos(3, true, 5));
                        cloneDoms.push(getCloneDomByPos(3, false, 4));
                        break;
                }

                cloneDoms.sort(function (a, b) {
                    var aKind = a.find('.item').attr('href');
                    var bKind = b.find('.item').attr('href');
                    if (aKind !== 'javascript:;'){
                        return 1;
                    }
                    else if (bKind !== 'javascript:;'){
                        return -1;
                    }
                    return 0;
                });

                for (var i = 0; i < rowsCount; i ++){
                    var cloneDom = cloneDoms.pop();
                    cloneDom.addClass('left-side for-side layer-' + (i + 1));
                    domDiamondsWrapper.append(cloneDom);

                    cloneDom = cloneDoms.pop();
                    cloneDom.addClass('right-side for-side layer-' + (i + 1));
                    domDiamondsWrapper.append(cloneDom);
                }
            }

            this.init = function () {
                fillMainDiamonds();
                fillSides();
                if (topRightSeries) {
                    domDiamondsWrapper.find('.series-content .diamond-box--lg-top .item img').attr('src', topRightSeries.strSeries_image);
                } else {
                    let index = Math.floor(Math.random() * gradients.length);
                    let gradient = gradients[index];
                    domDiamondsWrapper.find('.series-content .diamond-box--lg-top .item').css('background-image', gradient);
                }
                bindEvents();
            }
        }
        let ContentChannelClass = function () {
            let domSection;
            let itemHandles = [];
            var ItemClass = function (itemData) {
                var domItem;
                var self = this, payHandle = null;

                var viewUserPage = function (data) {
                    $('<form action="my_solutions" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
                }
                var popupThanksJoin = function (data) {
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Thanks for joining! Would you like to go to your experiences?',
                        buttons: {
                            returnHome: {
                                text: "My Experiences",
                                value: 'return_home',
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            customize: {
                                text: "Not yet",
                                value: 'customize',
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        },
                        closeOnClickOutside: false,
                    }).then(function (value) {
                        if (value == 'return_home'){
                            viewUserPage(data);
                        }
                        else {
                        }
                    });
                    var domSwalRoot, domDisableCheckbox;
                    var disablePopup = function () {
                        localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
                    }
                    var enabelPopup = function () {
                        localStorage.removeItem('thegreyshirt_show_popup_every_join');
                    }
                    var bindEvents = function () {
                        domDisableCheckbox.change(function () {
                            $(this).prop('checked') ? disablePopup() : enabelPopup();
                        })
                    }
                    var init = function () {
                        setTimeout(function () {
                            domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                            domSwalRoot.addClass('thanks-join');
                            domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                            domDisableCheckbox = domSwalRoot.find('#disable_popup');
                            bindEvents();
                        }, 100);
                    }();
                }
                var popupErroJoin = function () {
                    swal("Sorry, Something went wrong!", {
                        content: 'please try again later'
                    });
                }

                var openStripe = function () {
                    if (payHandle) {
                        payHandle.open({
                            name: itemData.strSeries_title,
                            description: itemData.strSeries_description,
                            currency: 'usd',
                            amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                        });
                    }
                    else {
                        popupErroJoin();
                    }
                }
                var openStripeCard = function () {
                    stripeCardModalHandle.setSeries(itemData);
                    stripeCardModalHandle.open();
                    stripeCardModalHandle.afterJoin(function (res) {
                        if (res.status == true){
                            itemData.purchased = res.data;
                            domItem.addClass('purchased');
                        }
                    });
                }
                var join_pre = function (token) {
                    ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = res.data;
                            domItem.addClass('purchased');
                        }
                    });
                }
                var join = function(source) {
                    ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, source: source}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = res.data;
                            domItem.addClass('purchased');
                        }
                    });
                }

                var unJoin = function () {
                    swal("You already joined this series!", {
                        content: 'Thanks!'
                    });
                    return false;

                    ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: itemData.purchased}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = 0;
                        }
                    });
                }

                var bindEvents = function () {
                    domItem.find('.join-btn').click(function () {
                        if (CLIENT_ID > -1){
                            if (parseInt(itemData.boolSeries_charge) === 1 && parseInt(itemData.purchased) == 0) {
                                openStripeCard();
                            }
                            else {
                                parseInt(itemData.purchased) ? unJoin() : join();
                            }
                        }
                        else{
                            authHandle.activeLogin();
                            authHandle.popup();
                            authHandle.afterLogin(function () {
                                window.location.reload();
                            })
                        }
                    });
                }

                this.isFiltered = function () {
                    var filterData = filterHandle.data().filter;
                    if (filterData.free || filterData.premium || filterData.affiliate) {
                        if (filterData.free && !parseInt(itemData.boolSeries_charge)) {
                            return true;
                        }
                        if (filterData.premium && parseInt(itemData.boolSeries_charge)) {
                            return true;
                        }
                        if (filterData.affiliate && parseInt(itemData.boolSeries_affiliated)) {
                            return true;
                        }
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domRoot.find('>.flex-row'));
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = function () {
                    if (itemData.purchased) {
                        domItem.addClass('purchased');
                    }
                    domItem.find('.item-image').css('background-image', 'url(' + itemData.strSeries_image + ')');
                    domItem.find('.child-count span').text(itemData.posts_count);
                    domItem.find('.item-title').text(itemData.strSeries_title);
                    domItem.find('.preview-btn').attr('href', 'preview_series?id=' + itemData.series_ID);
                    domItem.css('order', itemHandles.length + 1);
                    domItem.find('.item-category').attr('href', 'browse?id=' + itemData.category.category_ID).html(itemData.category.strCategory_name);
                }
                this.setOrder = function (order) {
                    domItem.css('order', order);
                }
                this.init = function () {
                    domItem = domSection.find('.list__item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSection.find('.series--list'));
                    self.bindData();
                    bindEvents();
                }
            }

            this.init = function () {
                domSection = domMain.find('section.content-channel');
                userSeries.forEach(function (item) {
                    let itemHandle = new ItemClass(item);
                    itemHandle.init();
                    itemHandles.push(item);
                });
            }
        }
        this.init = function () {
            domMain = $('main.main-content');
            domHeader = $('.site-content > .content-header');
            diamondsHandle = new DiamondsClass();
            diamondsHandle.init();

            contentChannelHandle = new ContentChannelClass();
            contentChannelHandle.init();

            bindEvents();
        }
    };
    let pageHandle = new PageClass();
    pageHandle.init();
})();