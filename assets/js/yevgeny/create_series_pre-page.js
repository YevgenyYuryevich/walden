(function () {
    'use strict';

    var PageClass = function () {

        var domPage, domPageHeader;

        var step1Handle, step2Handle, step3Handle, congratulationsHandle;

        var seriesItemData = false;

        var Step1Class = function () {
            var domStep;
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domStep).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domStep.find('[href="#tab-step-2"]').on('shown.bs.tab', function () {
                    domPageHeader.find('.step-value').html(2);
                    domPageHeader.find('.step-progress-wrapper').removeClass('step-progress-1').removeClass('step-progress-3').addClass('step-progress-2');
                    domPageHeader.find('.progress-item.step-2').addClass('active');
                });
                domStep.find('.cover-image-wrapper [name="strSeries_image"]').change(function () {
                    updateCoverImage(this);
                });
                domStep.find('[name="boolSeries_isPublic"]').change(function () {
                    if ($(this).prop('checked')) {
                        domStep.find('.public-value-txt').html('available now');
                    }
                    else {
                        domStep.find('.public-value-txt').html('');
                    }
                });
            }
            this.data = function () {
                var data = {};
                data.strSeries_title = domStep.find('[name="strSeries_title"]').val();
                data.strSeries_description = domStep.find('[name="strSeries_description"]').val();
                data.intSeries_category = domStep.find('[name="intSeries_category"]').val();
                data.boolSeries_isPublic = domStep.find('[name="boolSeries_isPublic"]').prop('checked') ? 1 : 0;
                data.available_days = '';
                domStep.find('.day-item input').each(function (i, d) {
                    data.available_days += $(this).prop('checked') ? '1' : '0';
                });
                if (domStep.find('[name="strSeries_image"]').prop('files').length) {
                    data.strSeries_image = domStep.find('[name="strSeries_image"]').prop('files')[0];
                }
                return data;
            }
            this.init = function () {
                domStep = domPage.find('.create-series-step-1');
                domStep.find('select[name="intSeries_category"]').select2();
                bindEvents();
            }
        }

        var Step2Class = function () {

            var domStep, domStepHeader, domSearchContainer, domStepFooter;

            var itemHandles = [], self = this, youtubeListHandle, podcastsListHandle, blogsListHandle, ideaboxListHandle, postsListHandle;

            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';

            var bindEvents = function () {
                domStepHeader.find('.search-input').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        domStepHeader.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domStepHeader.find('.terms-container'));
                        self.applySearch(v);
                    }
                });
                domStepFooter.find('[href="#tab-step-3"]').on('shown.bs.tab', function () {
                    self.data();
                    domPageHeader.find('.step-value').html(3);
                    domPageHeader.find('.step-progress-wrapper .step-wrapper').html('final step');
                    domPageHeader.find('.step-progress-wrapper').removeClass('step-progress-1').removeClass('step-progress-2').addClass('step-progress-3');
                    domPageHeader.find('.progress-item.step-3').addClass('active');
                });
            }

            this.applySearch = function (kwd) {
                self.searchByKeyword(kwd).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, kwd);
                    podcastsListHandle.setItems(res.data.spreaker, kwd);
                    blogsListHandle.setItems(res.data.blogs, kwd);
                    ideaboxListHandle.setItems(res.data.ideabox, kwd);
                    postsListHandle.setItems(res.data.posts, kwd);
                });
            }

            this.searchByKeyword = function (keyword) {
                var ajaxData = {
                    q: keyword,
                    size: 50,
                    items: [
                        {name: 'youtube', token: false},
                        {name: 'spreaker', token: false},
                        {name: 'blogs', token: false},
                        {name: 'recipes', token: false},
                        {name: 'ideabox', token: false},
                        {name: 'posts', token: false},
                        {name: 'rssbPosts', token: false},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData);
            }

            var SearchRowClass = function (from) {
                var domSearchRow, domItemsList;

                var self = this, itemHandles = [], filteredHandles = [];

                var bindData = function () {
                    switch (from) {
                        case 'youtube':
                            domSearchRow.find('.from-name').html('from Youtube');
                            break;
                        case 'podcasts':
                            domSearchRow.find('.from-name').html('from Podcasts');
                            break;
                        case 'blog':
                            domSearchRow.find('.from-name').html('from Blogs');
                            break;
                        case 'recipes':
                            domSearchRow.find('.from-name').html('from Recipes');
                            break;
                        case 'ideabox':
                            domSearchRow.find('.from-name').html('from Ideabox');
                            break;
                        case 'posts':
                            domSearchRow.find('.from-name').html('from Posts');
                            break;
                        case 'rssb-posts':
                            domSearchRow.find('.from-name').html('from RSSBlogPost');
                            break;
                        default:
                            domSearchRow.find('.from-name').html('from Posts');
                            break;

                    }
                }

                var bindEvents = function () {
                    domItemsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 10;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }

                var formatItemData = function(itemData){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.name;
                            fData.summary = '';
                            fData.body = itemData.body;
                            fData.image = itemData.image;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    fData.origin = itemData;
                    return fData;
                }

                var SearchItemClass = function (itemData) {

                    var dataLoaded = false, isAdded = false;
                    var domItem;

                    var self = this;

                    var bindData = function () {
                        dataLoaded = true;
                        domItem.find('.item-img').attr('src', itemData.image);
                        domItem.find('.item-title').html(itemData.title);
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            domItem.find('.blog-duration').html(res);
                            itemData.duration = res;
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.join-btn').click(function () {
                            if (isAdded) {
                                isAdded = false;
                                domItem.removeClass('added-item');
                            }
                            else {
                                isAdded = true;
                                domItem.addClass('added-item');
                            }
                        })
                    }
                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }
                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }
                        return ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }

                    this.subscriptionFormat = function () {
                        var f = {
                            strClientSubscription_title: itemData.title,
                            strClientSubscription_body: itemData.body,
                            strClientSubscription_image: itemData.image,
                            intClientSubscriptions_type: itemData.type,
                        }
                        if (itemData.duration) {
                            f.strSubscription_duration = itemData.duration;
                        }
                        return f;
                    }
                    this.postFormat = function () {
                        var f = {
                            strPost_title: itemData.title,
                            strPost_body: itemData.body,
                            strPost_summary: itemData.summary,
                            intPost_type: itemData.type,
                            strPost_featuredimage: itemData.image,
                        }
                        if (itemData.duration) {
                            f.strPost_duration = itemData.duration;
                        }
                        return f;
                    }

                    this.remove = function () {
                        domItem.remove();
                    }
                    this.isAdded = function () {
                        return isAdded;
                    }
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.bindData = bindData;
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domItemsList);
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.init = function () {
                        domItem = domSearchRow.find('.search-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        bindEvents();
                    }
                }

                this.setItems = function (itemsData, newKeyword) {
                    domItemsList.slick('unslick');
                    filteredHandles.forEach(function (value) {
                        value.remove();
                    });
                    domSearchRow.find('.found-value').html(itemsData.items.length);
                    filteredHandles = [];
                    itemsData.items.forEach(function (item, i) {
                        var itemHandle = new SearchItemClass(formatItemData(item));
                        itemHandle.init();
                        if (i < 15) {
                            itemHandle.bindData();
                        }
                        itemHandles.push(itemHandle);
                        filteredHandles.push(itemHandle);
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        centerMode: false,
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                        ]
                    });
                }
                this.selectedItems = function () {
                    var sItems = [];
                    itemHandles.forEach(function (value) {
                        if (value.isAdded()) {
                            sItems.push(value.postFormat());
                        }
                    });
                    return sItems;
                }
                this.refreshItems = function () {
                    domItemsList.css('height', domItemsList.css('height'));
                    domItemsList.css('opacity', 0);
                    setTimeout(function () {
                        domItemsList.slick('unslick');
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        switch (sortBy) {
                            case 'audio':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 0;
                                });
                                break;
                            case 'video':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 2;
                                });
                                break;
                            case 'text':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                });
                                break;
                            default:
                                filteredHandles = itemHandles;
                                break;
                        }
                        filteredHandles.forEach(function (value) {
                            value.append();
                        });
                        domItemsList.css('height', 'auto');
                        domItemsList.slick({
                            dots: false,
                            infinite: false,
                            centerMode: false,
                            arrows: true,
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            nextArrow: domSearchRow.find('.arrow-right'),
                            prevArrow: domSearchRow.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 3,
                                    }
                                },
                            ]
                        });
                        domItemsList.css('opacity', 1);
                    }, 300);
                }
                this.init = function () {
                    domSearchRow = domSearchContainer.find('.search-row.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSearchContainer);
                    domItemsList = domSearchRow.find('.search-items-list');
                    bindData();
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        centerMode: false,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                        ]
                    });
                    bindEvents();
                }
            }

            this.addToSeries = function (seriesId) {
                var items = self.data();
                items.forEach(function (item) {
                    item.intPost_series_ID = seriesId;
                });
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: items}).then(function (res) {
                    return res;
                })
            }
            this.data = function () {
                var items = [];
                items = items.concat(youtubeListHandle.selectedItems());
                items = items.concat(podcastsListHandle.selectedItems());
                items = items.concat(blogsListHandle.selectedItems());
                items = items.concat(postsListHandle.selectedItems());
                items = items.concat(ideaboxListHandle.selectedItems());
                return items;
            }
            this.init = function () {
                domStep = domPage.find('.create-series-step-2');
                domStepHeader = domStep.find('.step-header');
                domStepFooter = domStep.find('.step-footer');

                domSearchContainer = domStep.find('.search-container');

                youtubeListHandle = new SearchRowClass('youtube');
                youtubeListHandle.init();

                podcastsListHandle = new SearchRowClass('podcasts');
                podcastsListHandle.init();

                blogsListHandle = new SearchRowClass('blog');
                blogsListHandle.init();

                postsListHandle = new SearchRowClass('posts');
                postsListHandle.init();

                ideaboxListHandle = new SearchRowClass('ideabox');
                ideaboxListHandle.init();
                bindEvents();
            }
        }

        var Step3Class = function () {
            var domStep;

            var self = this;

            var seriesData;

            var bindEvents = function () {
                domStep.find('input').change(function () {
                    var type = $(this).attr('type');
                    var v;
                    if (type === 'checkbox') {
                        v = $(this).prop('checked') ? 1 : 0;
                    }
                    else {
                        v = $(this).val();
                    }
                    seriesData[$(this).attr('name')] = v;
                    bindData();
                });
                domStep.find('.save-btn').click(function () {
                    var d1 = step1Handle.data();
                    var d3 = self.data();
                    var sets = Object.assign({}, d1, d3);
                    save(sets).then(function (res) {
                        if (res.status) {
                            seriesItemData = res.data;
                            step2Handle.addToSeries(res.data.series_ID).then(function (resP) {
                                if (resP.status) {
                                    seriesItemData.posts = resP.data;
                                    $('.site-content').css('min-height', $('.site-content').css('height'));
                                    $('.site-content .create-series-content').css('opacity', 0);
                                    setTimeout(function () {
                                        $('.site-content .create-series-content').hide();
                                        $('.site-content .after-adding-content').show();
                                        congratulationsHandle.setSeries();
                                        setTimeout(function () {
                                            $('.site-content .after-adding-content').css('opacity', 1);
                                            $('.site-content').css('min-height', 'none');
                                        }, 50);
                                    }, 300);
                                }
                            });
                        }
                        else {
                            alert('something wrong');
                        }
                    });
                });
            }

            var bindData = function () {

                if (seriesData.boolSeries_charge) {
                    domStep.find('[name="boolSeries_charge"]').prop('checked', true);
                    domStep.find('[name="intSeries_price"]').removeAttr('disabled');
                    domStep.find('[name="intSeries_price"]').val(seriesData.intSeries_price);

                    if (seriesData.boolSeries_affiliated) {
                        domStep.find('[name="boolSeries_affiliated"]').prop('checked', true);
                        domStep.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('enable');
                        domStep.find('[name="intSeries_affiliate_percent"]').val(seriesData.intSeries_affiliate_percent);

                        var ownerMoney = seriesData.intSeries_price * (100 - parseInt(seriesData.intSeries_affiliate_percent)) / 100;
                        ownerMoney = parseInt(ownerMoney);
                        domStep.find('.you-will-get-value').html(ownerMoney + '$');

                        var affiliateMoney = seriesData.intSeries_price - ownerMoney;
                        affiliateMoney = parseInt(affiliateMoney);
                        domStep.find('.affiliate-will-get-value').html(affiliateMoney + '$');
                    }
                    else {
                        domStep.find('[name="boolSeries_affiliated"]').prop('checked', false);
                        domStep.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                        domStep.find('[name="intSeries_affiliate_percent"]').val('');
                        domStep.find('.you-will-get-value').html(seriesData.intSeries_price + '$');
                        domStep.find('.affiliate-will-get-value').html('NO Affiliated');
                    }
                }
                else {
                    domStep.find('[name="boolSeries_charge"]').prop('checked', false);
                    domStep.find('[name="intSeries_price"]').attr('disabled', true);
                    domStep.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                    domStep.find('[name="boolSeries_affiliated"]').prop('checked', false);
                    domStep.find('[name="intSeries_affiliate_percent"]').val('');
                    domStep.find('[name="intSeries_price"]').val('');
                    domStep.find('.you-will-get-value').html('No Charged');
                    domStep.find('.affiliate-will-get-value').html('NO Affiliated');
                }
            }
            var save = function (sets) {
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                if (sets.strSeries_image) {
                    ajaxData.append('strSeries_image', sets.strSeries_image);
                    delete sets.strSeries_image;
                }
                ajaxData.append('sets', JSON.stringify(sets));
                return ajaxAPiHandle.multiPartApiPost('Series.php', ajaxData, true, 300);
            }
            this.data = function () {
                var sets = {
                    boolSeries_charge: seriesData.boolSeries_charge,
                    boolSeries_affiliated: seriesData.boolSeries_affiliated,
                };
                if (seriesData.intSeries_price) {
                    sets.intSeries_price = seriesData.intSeries_price;
                }
                if (seriesData.intSeries_affiliate_percent) {
                    sets.intSeries_affiliate_percent = seriesData.intSeries_affiliate_percent;
                }
                return sets;
            }
            this.dom = function () {
                return domStep;
            }
            this.init = function () {
                seriesData = Object.assign({}, {
                    boolSeries_charge: 0,
                    boolSeries_affiliated: 0,
                    intSeries_price: 5,
                    intSeries_affiliate_percent: 0,
                });
                seriesData.boolSeries_charge = parseInt(seriesData.boolSeries_charge);
                seriesData.boolSeries_affiliated = parseInt(seriesData.boolSeries_affiliated);
                domStep = domPage.find('.create-series-step-3');
                domStep.find('[name="intSeries_affiliate_percent"]').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 100,
                    value: parseInt(seriesData.intSeries_affiliate_percent),
                });
                bindData();
                bindEvents();
            }
        }

        var CongratulationsClass = function () {

            var domRoot;

            var seriesHandle;
            var bindEvents = function () {
                domRoot.find('.invite-friend-form').submit(function () {
                    var v = $(this).find('[name="emails"]').val();
                    var emails = v.split(/\s*,+\s*/);
                    seriesHandle.invite(emails);
                    return false;
                });
            }
            var SeriesItemClass = function () {

                var domSeriesItem, domSeriesHeader, domPostsList, domSeriesIncomeWrapper;

                var itemHandles = [], filteredHandles = [], copyEmbedHandle;

                var PostItemClass = function (itemData) {
                    var domItem, domBody, domTypeBody;
                    var dataLoaded = false;
                    var deleteItem = function () {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: itemData.post_ID}).then(function (res) {
                            if (res.status) {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().post_ID === itemData.post_ID) {
                                        itemHandles.splice(i, 1);
                                        domItem.remove();
                                    }
                                });
                            }
                        });
                    }
                    var bindData = function () {
                        dataLoaded = true;
                        domItem.addClass('type-' + itemData.intPost_type);
                        domItem.find('.day-value').html(itemData.viewDay);
                        switch (parseInt(itemData.intPost_type)) {
                            case 0:
                                domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                break;
                            case 2:
                                domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                domTypeBody.find('img').attr('src', itemData.strPost_featuredimage);
                                break;
                            default:
                                domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                domTypeBody.html(itemData.strPost_body);
                                break;
                        }
                        domItem.find('.item-title').html(itemData.strPost_title);
                        if (itemData.strPost_duration) {
                            domBody.find('.blog-duration').html(itemData.strPost_duration);
                        }
                        else {
                            helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                                domBody.find('.blog-duration').html(res);
                                itemData.strPost_duration = res;
                                ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                            });
                        }
                    }
                    var bindEvents = function () {
                        domItem.find('.item-action.delete-action').click(function (e) {
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this post?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteItem();
                                }
                            });
                        });
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.append = function () {
                        domItem.appendTo(domPostsList);
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.bindData = bindData;
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.init = function () {
                        domItem = domSeriesItem.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        domBody = domItem.find('.item-type-body');
                        bindEvents();
                    }
                }

                this.invite = function (emails) {
                    var ajaxData = {
                        action: 'invite',
                        id: seriesItemData.series_ID,
                        emails: emails,
                    };
                    ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Your invitation are sent successfully',
                        }).then(function (value) {
                            if (value == 'return_home'){
                            }
                            else {
                            }
                        });
                    });
                }

                var bindData = function () {
                    domSeriesHeader.find('.action-item.edit-action').attr('href', 'edit_series?id=' + seriesItemData.series_ID);
                }

                var bindEvents = function () {
                    domPostsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 3;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }
                this.data = function () {
                    return seriesItemData;
                }
                this.refreshPosts = function () {
                    domPostsList.css('height', domPostsList.css('height'));
                    domPostsList.css('opacity', 0);
                    setTimeout(function () {
                        domPostsList.slick('unslick');
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        switch (sortBy) {
                            case 'audio':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().intPost_type == 0;
                                });
                                break;
                            case 'video':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().intPost_type == 2;
                                });
                                break;
                            case 'text':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().intPost_type == 7 || value.data().intPost_type == 8 || value.data().intPost_type == 10;
                                });
                                break;
                            case 'A-z':
                                filteredHandles = itemHandles;
                                filteredHandles.sort(function (a, b) {
                                    if (a.data().strPost_title < b.data().strPost_title) {
                                        return 1;
                                    }
                                    else if (a.data().strPost_title == b.data().strPost_title) {
                                        return 0;
                                    }
                                    return -1;
                                });
                                break;
                            case 'Z-a':
                                filteredHandles = itemHandles;
                                filteredHandles.sort(function (a, b) {
                                    if (a.data().strPost_title > b.data().strPost_title) {
                                        return 1;
                                    }
                                    else if (a.data().strPost_title == b.data().strPost_title) {
                                        return 0;
                                    }
                                    return -1;
                                });
                                break;
                            default:
                                filteredHandles = itemHandles;
                                break;
                        }
                        filteredHandles.forEach(function (value) {
                            value.append();
                        });
                        domPostsList.css('height', 'auto');
                        domPostsList.slick({
                            dots: false,
                            infinite: false,
                            arrows: true,
                            slidesToShow: 3,
                            slidesToScroll: 2,
                            nextArrow: domSeriesItem.find('.arrow-right'),
                            prevArrow: domSeriesItem.find('.arrow-left'),
                            responsive: [
                            ]
                        });
                        domPostsList.css('opacity', 1);
                    }, 300);
                }
                this.refreshSlick = function () {
                    domPostsList.slick('setPosition');
                }
                this.init = function () {
                    domSeriesItem = domRoot.find('.series-item');
                    domSeriesHeader = domSeriesItem.find('.series-content-header');
                    domPostsList = domSeriesItem.find('.posts-list');
                    domSeriesIncomeWrapper = domSeriesItem.find('.series-income-wrapper');
                    copyEmbedHandle = new SeriesCopyEmbedClass(domSeriesHeader.find('.component.series-copy-embed'), seriesItemData);
                    copyEmbedHandle.init();
                    bindData();
                    bindEvents();
                    seriesItemData.posts.forEach(function (itemData, i) {
                        var hdl = new PostItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        if (i < 7) {
                            hdl.bindData();
                        }
                        itemHandles.push(hdl);
                        filteredHandles.push(hdl);
                    });
                    domPostsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        nextArrow: domSeriesItem.find('.arrow-right'),
                        prevArrow: domSeriesItem.find('.arrow-left'),
                        responsive: [
                        ]
                    });
                }
            }

            this.setSeries = function () {
                domRoot.find('.series-name-wrapper .series-name').html(seriesItemData.strSeries_title);
                seriesHandle = new SeriesItemClass();
                seriesHandle.init();
            }
            this.init = function () {
                domRoot = $('.site-content .after-adding-content');
                bindEvents();
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            domPageHeader = domPage.find('header.content-header');

            step1Handle = new Step1Class();
            step1Handle.init();

            step2Handle = new Step2Class();
            step2Handle.init();

            step3Handle = new Step3Class();
            step3Handle.init();

            congratulationsHandle = new CongratulationsClass();
            congratulationsHandle.init();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();