(function () {
    'use strict';

    var is_loading_controlled_in_local = false;
    var mainHandle, headerHandle;

    var MainClass = function () {

        var domRoot = $('main.main-content');

        var series = initial_purchasedSeries;

        var postsBlockHandles = [];

        var PostsBlockClass = function (sery) {
            var self = this;

            var domBlock = domRoot.find('.posts-block.sample').clone().removeClass('sample').removeAttr('hidden');
            var domGrid = domBlock.find('.tt-grid');

            var grid = domGrid.get(0),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var itemHandles = [];
            var pageSize = 5;
            var currentIndex = 0;
            var arrPageStatus = [
                {class: 'scope-width-5', pageSize: 5},
                {class: 'scope-width-5-sm', pageSize: 5},
                {class: 'scope-width-4', pageSize: 4},
                {class: 'scope-width-3', pageSize: 3},
                {class: 'scope-width-2', pageSize: 2},
                {class: 'scope-width-1', pageSize: 1},
            ];
            var currentPageStatus;

            var ItemClass = function (itemData) {
                var domItemRoot = null;
                var domTitle, domTitleWrp;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.view-btn').click(function () {
                        viewPost();
                    });
                    domItemRoot.find('.switch-btn').click(function () {
                        openSwitchPage();
                    });
                    domItemRoot.find('.trash-btn').click(function () {
                        swal({
                            title: "Are you sure?",
                            text: "Do you want to trash this post?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                self.delete();
                            }
                        });
                    });
                };
                var openSwitchPage = function () {
                    var postId = itemData.intClientSubscription_post_ID;
                    var subscriptionId = itemData.clientsubscription_ID;
                    var owner = sery.intSeries_client_ID == CLIENT_ID ? 'me' : 'other';
                    $('<form action="switchsubscription" method="post" target="_blank" hidden><input name="owner" value="'+ owner +'"/><input name="postId" value="'+ postId +'" /><input name="subscriptionId" value="'+ subscriptionId +'" /></form>').appendTo('body').submit().remove();
                }
                var viewPost = function () {
                    var cloneForm = $('<form action="view" method="get" target="_blank" hidden><input name="id" value="'+ itemData.intClientSubscription_post_ID +'"><input name="prevpage" value="calendar"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }

                this.data = function () {
                    return itemData;
                }
                this.delete = function () {
                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        type: 'post',
                        data: {action: 'trash', id: self.id()},
                        dataType: 'json',
                        success: function (res) {
                            if (res.status){
                                itemHandles.forEach(function (handle, i) {
                                    if (self.id() == handle.id()){
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize));
                            }
                        }
                    });
                }
                this.focus = function () {
                    domItemRoot.addClass('focus');
                    domItemRoot.one(animEndEventName, function () {
                        domItemRoot.removeClass('focus');
                    });
                }
                this.id = function () {
                    return itemData.clientsubscription_ID;
                }
                this.postId = function () {
                    return itemData.intClientSubscription_post_ID;
                }
                this.order = function () {
                    return parseInt(itemData.intClientSubscriptions_order);
                }
                this.viewOrder = function () {
                    return parseInt(itemData.viewOrder);
                }
                this.setOrder = function (newOrder) {
                    itemData.intClientSubscriptions_order = newOrder;
                }
                this.setViewOrder = function (newOrder) {
                    itemData.viewOrder = newOrder;
                }
                this.setParent = function (parent) {
                    itemData.intClientSubscriptions_parent = parent;
                }
                this.title = function () {
                    return itemData.strClientSubscription_title;
                }
                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', itemData.strClientSubscription_image);
                    cloneDom.find('.title-wrapper .item-title').html(itemData.strClientSubscription_title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }
                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domTitle = domItemRoot.find('.item-title');
                }
                this.init = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domItemRoot.data('controlHandle', self);
                    bindEvents();
                }
            }
            var loadNewSet = function(set) {
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                return new Promise(function (resolve, reject) {
                    if (isAnimating === true){
                        resolve();
                        return true;
                    }
                    isAnimating = true;
                    checkCanPagging();

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = $(el).find( '>div' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild.length ) {
                            itemChild.addClass('tt-old');
                        }
                    } );

                    // apply effect
                    setTimeout( function() {
                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find( '>div:last-child' ));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                            resolve();
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                });
            };

            var checkCanPagging = function () {
                if (currentIndex == 0){
                    domBlock.find('.prev-pagging').addClass('disable');
                }
                else {
                    domBlock.find('.prev-pagging').removeClass('disable');
                }
                if (currentIndex + pageSize >= itemHandles.length){
                    domBlock.find('.next-pagging').addClass('disable');
                }
                else {
                    domBlock.find('.next-pagging').removeClass('disable');
                }
            }
            var linkCustomize = function(){
                var $form = $('<form method="post" action="editseries" target="_blank"></form>');
                $form.append('<input name="id" value="'+ sery.series_ID +'"/>');
                $form.hide();
                $form.appendTo('body').submit();
                $form.remove();
            }
            var linkAddNewIdeas = function () {
                var $form = $('<form method="post" action="editseries" target="_blank"></form>');
                $form.append('<input name="id" value="'+ sery.series_ID +'"/>');
                $form.append('<input name="panel" value="addNew"/>');
                $form.hide();
                $form.appendTo('body').submit();
                $form.remove();
            }
            var bindEvents = function () {
                domBlock.find('.prev-pagging').click(function () {
                    self.previous();
                });
                domBlock.find('.next-pagging').click(function () {
                    self.next();
                });
                domGrid.on('sortupdate', function (e, h) {
                    self.sortUpdateCallback(h);
                });
                domBlock.find('header .customize-series').click(function () {
                    linkCustomize();
                });
                domBlock.find('header .add-new-posts').click(function () {
                    linkAddNewIdeas();
                });
                $(window).resize(function () {
                    var pageStatus = arrPageStatus[getScreenStatus()];
                    if (pageStatus != currentPageStatus){
                        if (pageStatus.pageSize > pageSize){
                            self.setPageStatus(pageStatus);
                            loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize));
                        }
                        else {
                            self.setPageStatus(pageStatus);
                        }
                    }
                });
            }

            var getScreenStatus = function () {
                var w = domRoot.outerWidth();
                if (w > 1300){
                    return 0;
                }
                else if (w > 1100){
                    return 1;
                }
                else if (w > 1000){
                    return 2;
                }
                else if (w > 850){
                    return 3;
                }
                else if (w > 668){
                    return 4;
                }
                else {
                    return 5;
                }
            }

            var sortBlock = function () {
                itemHandles.sort(function (a, b) {
                    return a.viewOrder() - b.viewOrder();
                });
            }

            this.updatePageOrderPre = function () {
                var orders = [], handles = [], sets = [];

                var postUpdateSets = [];

                domGrid.find('>li>a').each(function (i, dom) {
                    var handle = $(dom).data('controlHandle');
                    orders.push(handle.order());
                    handles.push(handle);
                });

                orders.sort(function (a, b) {
                    return a - b;
                });

                handles.forEach(function (handle, i) {
                    sets.push({id: handle.id(), order: orders[i]});
                });

                var rtn = $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: {action: 'update_orders', updateSets: sets},
                    dataType: 'json',
                    success: function(res) {
                        if (res.status){
                            handles.forEach(function (handle, i) {
                                handle.setOrder(orders[i]);
                            });
                            itemHandles.sort(function (a, b) {
                                return a.order() - b.order();
                            });
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });

                if (parseInt(CLIENT_ID) == parseInt(sery.intSeries_client_ID)){
                    handles.forEach(function (handle, i) {
                        if (handle.postId()){
                            postUpdateSets.push({where: handle.postId(), sets: {intPost_order: orders[i]}});
                        }
                    });
                    $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: {action: 'update_posts', updateSets: postUpdateSets},
                        success: function (res) {
                            if (res.status == true){
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                return rtn;
            }

            this.sortUpdateCallback = function (h) {
                var orders = [], handles = [];

                domGrid.find('>li>div').each(function (i, dom) {
                    var handle = $(dom).data('controlHandle');
                    orders.push(handle.viewOrder());
                    handles.push(handle);
                });

                orders.sort(function (a, b) {
                    return a - b;
                });

                handles.forEach(function (handle, i) {
                    handle.setViewOrder(orders[i]);
                });
                sortBlock();

                var el = h.item;
                var hdl = el.find('>div').data('controlHandle');
                var ajaxData = {where: hdl.id(), sets: {}};

                if (el.prev().length) {
                    var prevDom = el.prev();
                    var prevHdl = prevDom.find('>div').data('controlHandle');
                    var prevData = prevHdl.data();
                    ajaxData.sets.intClientSubscriptions_parent = prevData.intClientSubscriptions_parent;
                    ajaxData.sets.intClientSubscriptions_order = prevHdl.order() + 1;
                }
                else {
                    var nextDom = el.next();
                    var nextHdl = nextDom.find('>div').data('controlHandle');
                    var nextData = nextHdl.data();
                    ajaxData.sets.intClientSubscriptions_parent = nextData.intClientSubscriptions_parent;
                    ajaxData.sets.intClientSubscriptions_order = nextHdl.order();
                }

                var nextSiblings = itemHandles.filter(function (e) {
                    var data = e.data();
                    if (data.intClientSubscriptions_parent !== ajaxData.sets.intClientSubscriptions_parent) {
                        return false;
                    }
                    if (e.order() < ajaxData.sets.intClientSubscriptions_order) {
                        return false;
                    }
                    return true;
                });
                hdl.setParent(ajaxData.sets.intClientSubscriptions_parent);
                hdl.setOrder(ajaxData.sets.intClientSubscriptions_order);
                var updateSets = [ajaxData];
                nextSiblings.forEach(function (e, i) {
                    e.setOrder(ajaxData.sets.intClientSubscriptions_order + i + 1);
                    updateSets.push({where: e.id(), sets: {intClientSubscriptions_order: e.order()}});
                });
                var synchronizeWithPost = false;
                if (parseInt(CLIENT_ID) === parseInt(sery.intSeries_client_ID)){
                    synchronizeWithPost = true;
                }
                ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update_items', updateSets: updateSets, synchronizeWithPost: synchronizeWithPost}).then(function (res) {});
            }
            this.setPageStatus = function (pageStatus) {
                for (var i = pageStatus.pageSize; i < items.length; i ++){
                    $(items[i]).remove();
                }
                for (var i = 0; i < pageStatus.pageSize - items.length; i++){
                    domGrid.append('<li class="tt-empty"></li>');
                }
                domBlock.find('.tt-grid-wrapper').addClass(pageStatus.class);
                if (currentPageStatus) { domBlock.find('.tt-grid-wrapper').removeClass(currentPageStatus.class);}
                currentPageStatus = pageStatus;
                pageSize = currentPageStatus.pageSize;
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                checkCanPagging();
            }

            this.sortItemsBy = function (sortBy) {
                switch (sortBy){
                    case 'a-z':
                        itemHandles.sort(sortByAZ);
                        break;
                    case 'z-a':
                        itemHandles.sort(sortByZA);
                        break;
                    default:
                        break;
                }
            }
            this.disappear = function () {
                currentIndex = 0;
                return new Promise(function (resolve, reject) {
                    loadNewSet([]).then(function () {
                        domBlock.addClass('disappear');
                        setTimeout(function () {
                            // domBlock.detach();
                            domBlock.removeClass('disappear');
                            domBlock.hide();
                            resolve();
                        }, 500);
                    });
                });
            }
            this.appear = function () {
                domBlock.addClass('appear');
                domBlock.show();
                // domBlock.appendTo(domRoot);
                return new Promise(function (resolve, reject) {
                    loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize)).then(function () {
                        domBlock.removeClass('appear');
                        resolve();
                    })
                });
            }
            this.previous = function () {
                if (currentIndex == 0 || isAnimating){
                    return false;
                }
                currentIndex -= pageSize;
                currentIndex = currentIndex > 0 ? currentIndex : 0;
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize));
            }
            this.next = function () {
                if (currentIndex + pageSize >= itemHandles.length || isAnimating){
                    return false;
                }
                currentIndex += pageSize;
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize));
            }
            this.init = function () {
                self.setPageStatus(arrPageStatus[getScreenStatus()]);
                domBlock.find('.sery-title').html(sery.strSeries_title);
                sery.subscriptions.forEach(function (post) {
                    var itemHandle = new ItemClass(post);
                    itemHandles.push(itemHandle);
                });
                itemHandles.sort(function (a, b) {
                    return a.viewOrder() - b.viewOrder();
                });
                domBlock.find('.tt-grid').sortable({
                    placeholder: 'ui-state-highlight',
                    items: 'li:not(.tt-empty)',
                    coneHelperSize: true,
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    revert: 300, // animation in milliseconds
                    handle: '.img-wrapper',
                    opacity: 0.9,
                    update: function (a, b) {}
                });
                bindEvents();
                domBlock.hide();
                domBlock.appendTo(domRoot);
            }
        }
        var sortByAZ = function (a, b) {
            var title1 = a.title().toLowerCase();
            var title2 = b.title().toLowerCase();
            return title1 > title2 ? 1 : (title1 < title2 ? -1 : 0);
        }
        var sortByZA = function (a, b) {
            var title1 = a.title().toLowerCase();
            var title2 = b.title().toLowerCase();
            return title1 > title2 ? -1 : (title1 < title2 ? 1 : 0);
        }

        this.appearBlock = function (seryId) {
            postsBlockHandles[seryId].appear();
        }
        this.disappearBlock = function (seryId) {
            postsBlockHandles[seryId].disappear();
        }
        this.init = function () {
            series.forEach(function (sery) {
                var handle = new PostsBlockClass(sery);
                handle.init();
                postsBlockHandles[sery.series_ID] = handle;
            });
        }
    }
    var HeaderClass = function () {

        var domRoot = $('header.content-header');
        var domDropdown = domRoot.find('.dropdown');
        var domDropdownMenu = $('.dropdown-menu', domDropdown);
        var series = [];

        var self = this;

        var bindEvents = function () {
            domDropdownMenu.find('li').click(function () {
                var checkbox = $(this).find('input');
                checkbox.prop('checked', !checkbox.prop('checked')).change();
                return false;
            });
            domDropdownMenu.find(':checkbox').change(function () {
                this.checked ? mainHandle.appearBlock($(this).val()) : mainHandle.disappearBlock($(this).val());
            });
        }

        this.selectSery = function (series_id) {
            domDropdownMenu.find('#select-series-checkbox-' + series_id).parents('li').click();
        }

        this.init = function () {
            initial_purchasedSeries.forEach(function (sery) {
                series.push({id: sery.series_ID, title: sery.strSeries_title});
            });
            series.forEach(function (sery) {
                var checkbox_html = '<li><a href="javascript:;"><input id="select-series-checkbox-' + sery.id + '" type="checkbox" value="' + sery.id + '"/><label for="select-series-checkbox-' + sery.id + '">' + sery.title + '</label></a></li>';
                domDropdownMenu.append(checkbox_html);
            });
            bindEvents();
            if (initial_id){
                self.selectSery(initial_id);
            }
            else {
                var maxCnt = 0, idOfMaxCnt;
                initial_purchasedSeries.forEach(function (sery) {
                    if (maxCnt < sery.subscriptions.length){
                        maxCnt = sery.subscriptions.length;
                        idOfMaxCnt = sery.series_ID;
                    }
                });
                if(maxCnt){
                    self.selectSery(idOfMaxCnt);
                }
            }
        }
    }

    mainHandle = new MainClass();
    mainHandle.init();

    headerHandle = new HeaderClass();
    headerHandle.init();

    $(document).ajaxStart(function () {
        if(!is_loading_controlled_in_local) {
            $('.loading').css('display', 'block');
        }
    });
    $(document).ajaxComplete(function () {
        if(!is_loading_controlled_in_local){
            $('.loading').css('display', 'none');
        }
    });
})();