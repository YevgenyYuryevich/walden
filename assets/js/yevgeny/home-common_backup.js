var authHandle;
var siteSearchHandle;
let stripeCardReady;
let stripeCardModalHandle;
(function () {
    'use strict';

    var HeaderClass = function () {

        var domRoot;
        var domBrowseWrapper;
        var domCategoriesList;
        var domMobileSidebar;
        var domMobileCategoriesList;

        var categories = [];
        var alphabet = '';
        var mAlphabet = '';

        var bindEvents = function () {
            domRoot.find('a.hamburger-nav-toggle').click(function () {
                $('[href="#menu-section"]').tab('show');
                $('.site-wrapper').addClass('side-menu-opened');
            });
            domRoot.find('[href="#login-modal"][data-toggle="modal"]').click(function () {
                var action = $(this).data('action');
                if (action == 'login') {
                    authHandle.activeLogin();
                }
                else {
                    authHandle.activeRegister();
                }
            });
            $('.site-back-drop').click(function () {
                if ($('.site-wrapper').hasClass('side-menu-opened')) {
                    $('.site-wrapper').removeClass('side-menu-opened')
                }
            });
            domRoot.find('.browse.drop-down-browse').hover(function () {
                domRoot.addClass('browse-hovering');
            });
            domRoot.find('.browse.drop-down-browse').mouseleave(function () {
                domRoot.removeClass('browse-hovering');
            });

            domRoot.find('.dropdown-icon.drop-down-menu').hover(function () {
                domRoot.addClass('drop-menu-hovering');
            });
            domRoot.find('.dropdown-icon.drop-down-menu').parent().mouseleave(function () {
                domRoot.removeClass('drop-menu-hovering');
            });
            domBrowseWrapper.find('.sort-by-alphabet').click(function () {
                alphabet = !alphabet ? 'a-z' : (alphabet == 'a-z' ? 'z-a' : 'a-z');
                $(this).removeClass('a-z z-a');
                $(this).addClass(alphabet);
                domCategoriesList.find('li:not(.sample)').remove();
                if (alphabet === 'a-z') {
                    categories.sort(function (a, b) {
                        var av = a.strCategory_name.toLowerCase();
                        var bv = b.strCategory_name.toLowerCase();
                        if (av > bv) {
                            return 1;
                        }
                        else if (av == bv) {
                            return 0;
                        }
                        return -1;
                    });
                }
                else {
                    categories.sort(function (a, b) {
                        var av = a.strCategory_name.toLowerCase();
                        var bv = b.strCategory_name.toLowerCase();
                        if (av > bv) {
                            return -1;
                        }
                        else if (av == bv) {
                            return 0;
                        }
                        return 1;
                    });
                }
                categories.forEach(function (value) {
                    domCategoriesList.find('li.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domCategoriesList).find('span').html(value.strCategory_name).parent().attr('href', 'browse?id=' + value.category_ID);
                });
            });
            domMobileSidebar.find('.sort-by-alphabet').click(function () {
                mAlphabet = !mAlphabet ? 'a-z' : (mAlphabet === 'a-z' ? 'z-a' : 'a-z');
                $(this).removeClass('a-z z-a');
                $(this).addClass(mAlphabet);
                domMobileCategoriesList.find('li:not(.sample)').remove();
                if (mAlphabet === 'a-z') {
                    categories.sort(function (a, b) {
                        var av = a.strCategory_name.toLowerCase();
                        var bv = b.strCategory_name.toLowerCase();
                        if (av > bv) {
                            return 1;
                        }
                        else if (av === bv) {
                            return 0;
                        }
                        return -1;
                    });
                }
                else {
                    categories.sort(function (a, b) {
                        var av = a.strCategory_name.toLowerCase();
                        var bv = b.strCategory_name.toLowerCase();
                        if (av > bv) {
                            return -1;
                        }
                        else if (av === bv) {
                            return 0;
                        }
                        return 1;
                    });
                }
                categories.forEach(function (value) {
                    domMobileCategoriesList.find('li.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domMobileCategoriesList).find('span').html(value.strCategory_name).parent().attr('href', 'browse?id=' + value.category_ID);
                });
            });

        }

        var fillCategories = function () {
            ajaxAPiHandle.apiPost('Category.php', {action: 'get_many'}, false).then(function (res) {
                if (res.status) {
                    categories = res.data;
                    categories.forEach(function (value) {
                        domCategoriesList.find('li.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domCategoriesList).find('span').html(value.strCategory_name).parent().attr('href', 'browse?id=' + value.category_ID);
                        domMobileCategoriesList.find('li.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domMobileCategoriesList).find('span').html(value.strCategory_name).parent().attr('href', 'browse?id=' + value.category_ID);
                    });
                }
                else {
                }
            })
        }

        this.init = function () {
            domRoot = $('header.home-header');
            domBrowseWrapper = domRoot.find('.browse-drop-down-wrapper');
            domCategoriesList = domBrowseWrapper.find('.make-wrapper ul');
            domMobileSidebar = $('#side-menu-for-mobile');
            domMobileCategoriesList = domMobileSidebar.find('#browse-section ul');
            domRoot.find('[data-toggle="popover"]').popover({
                trigger: 'hover',
                placement: 'bottom',
            });
            fillCategories();
            bindEvents();
        }
    }

    var AuthClass = function () {
        var domRoot, domLoginForm, domSignupForm;
        var callbacksAfterLogin = [];
        var callbacksAfterRegister = [];
        var userData = false;

        var login = function (username, password) {
            ajaxAPiHandle.pagePost('login.php', {function: 'signin', username: username, password: password}).then(function (res) {
                if (res.status) {
                    myAccount = res.result.data;
                    CLIENT_ID = res.result.data.id;
                    callbacksAfterLogin.forEach(function (fn) {
                        fn();
                    });
                    $('.site-wrapper').addClass('logged-in');
                    domRoot.modal('hide');
                    // window.location.href = 'my_solutions';
                }
                else {
                    domLoginForm.find('.alert.alert-danger').show();
                }
            });
        }
        var register = function (sets) {
            var ajaxData = {function: 'register'};
            $.extend(ajaxData, sets);
            ajaxAPiHandle.pagePost('login.php', ajaxData).then(function (res) {
                if (res.result.registered) {
                    CLIENT_ID = res.result.data.id;
                    myAccount = res.result.data;
                    $('.site-wrapper').addClass('logged-in');
                    domRoot.modal('hide');
                    setTimeout(function () {
                        swal("SUCCESS!", 'You have successfully registered', "success", {
                            button: "OK"
                        }).then(function () {
                            callbacksAfterRegister.forEach(function (fn) {
                                fn();
                            });
                        });
                    }, 500);
                }
                else {
                    domSignupForm.find('.alert.alert-danger').show();
                }
            });
        }
        var resetPassword = function (email) {
            return ajaxAPiHandle.pagePost('login.php', {'function': 'reset_send', username: email}).then(function (res) {
                if (res.status) {
                    swal("SUCCESS!", 'We will send message to your email soon', "success", {
                        button: "OK"
                    });
                }
                else {
                    swal("Sorry!", 'username or email does not exist', "error", {
                        button: "OK"
                    });
                }
                return res.status;
            });
        }
        var bindEvents = function () {
            domLoginForm.submit(function () {
                var username = domLoginForm.find('input#login-username').val();
                var password = domLoginForm.find('input#login-password').val();
                login(username, password);
                return false;
            });
            domLoginForm.find('.reset-password').click(function () {
                var username = domLoginForm.find('#login-reset-password').val();
                resetPassword(username);
            });
            domSignupForm.submit(function () {
                var sets = {};
                sets.email = domSignupForm.find('[name="email"]').val();
                sets.f_name = domSignupForm.find('[name="f_name"]').val();
                sets.password = domSignupForm.find('[name="password"]').val();
                register(sets);
                return false;
            });
        }
        this.activeLogin = function () {
            domRoot.find('#signup-tab').removeClass('in').removeClass('active');
            domRoot.find('#login-tab').addClass('in').addClass('active');
            domRoot.find('[data-toggle="collapse"][href="#forgot-password"]').addClass('collapsed');
            domRoot.find('#forgot-password').removeClass('in').css('height', '0px');
        }
        this.activeRegister = function () {
            domRoot.find('#login-tab').removeClass('in').removeClass('active');
            domRoot.find('#signup-tab').addClass('in').addClass('active');
        }
        this.popup = function () {
            domRoot.modal('show');
        }
        this.setFields = function (sets) {
            if (sets.loginMessage) {
                domLoginForm.find('.form-name').html(sets.loginMessage);
            }
            if (sets.email) {
                domLoginForm.find('#login-username').val(sets.email);
                domSignupForm.find('#signup-email').val(sets.email);
            }
        }
        this.afterLogin = function (fn) {
            callbacksAfterLogin.push(fn);
        }
        this.afterRegister = function (fn) {
            callbacksAfterRegister.push(fn);
        }
        this.user = function () {
            return CLIENT_ID == -1 ? false : myAccount;
        }
        this.init = function () {
            domRoot = $('#login-modal');
            domLoginForm = domRoot.find('form.login-form');
            domSignupForm = domRoot.find('form.signup-form');
            bindEvents();
        }
    }

    var SiteSearchClass = function () {

        var domSiteSearch, domHeaderSearchForm, domFooterSearchForm;
        var self = this;
        var isSearchOpened = false;
        var isSwitched = false;
        var seriesSearchHandle, findNewContentHandle, postsSearchHandle;
        var callbacksMainActive = [];

        var bindEvents = function () {
            domSiteSearch.find('.back-to-main').click(function () {
                domSiteSearch.css('opacity', 0);
                setTimeout(function () {
                    $('.site-wrapper').removeClass('site-search-opened');
                    callbacksMainActive.forEach(function (value) {
                        value();
                    });
                    setTimeout(function () {
                        $('.site-wrapper .site-content').css('opacity', 1);
                        isSearchOpened = false;
                        isSwitched = false;
                    });
                }, 300);
            });
        }

        var activeSearch = function () {
            $('.site-wrapper .site-content').css('opacity', 0);
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    domSiteSearch.css('min-height', $('.site-wrapper .site-content').css('height'));
                    $('.site-wrapper').addClass('site-search-opened');
                    setTimeout(function () {
                        domSiteSearch.css('opacity', 1);
                        resolve();
                        domSiteSearch.css('min-height', '0px');
                        isSearchOpened = true;
                    }, 50);
                }, 300);
            });
        }

        var FindNewContentClass = function () {

            var domRoot, domSearchHeader, domSortByDropDown;
            var self = this, filterHandle, youtubeListHandle, podcastsListHandle, blogsListHandle, recipesListHandle, ideaboxListHandle, postsListHandle, rssbPostsListHandle;
            var series = [];
            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';
            var searchedData = {}, loadMore = false, sortBy = -1;
            var isActive = false;
            var currentKeyword;
            var FilterClass = function () {

                var domFilterRoot;
                var domSortByDropDown;
                var self = this;

                var bindEvents = function () {
                    domSortByDropDown.find('ul li a').click(function () {
                        domSortByDropDown.find('ul li').removeAttr('hidden');
                        $(this).parent().attr('hidden', true);
                        var v = $(this).data('value');
                        var txt = $(this).text();
                        domSortByDropDown.data('value', v);
                        domSortByDropDown.find('button span').text(txt);
                        sortBy = v;
                        youtubeListHandle.refreshItems();
                        podcastsListHandle.refreshItems();
                        blogsListHandle.refreshItems();
                    });
                    domFilterRoot.find('.filters input[type="checkbox"]').change(function () {
                        // mainHandle.applyFilter();
                    });
                    domFilterRoot.find('.resize-filter').click(function () {
                        if (domFilterRoot.hasClass('size-full')) {
                            self.small();
                        }
                        else {
                            self.maximize();
                        }
                    });
                    domFilterRoot.find('.open-img').click(function () {
                        self.small();
                    });
                    domFilterRoot.find('.close-img').click(function () {
                        self.close();
                    });
                    domFilterRoot.find('[data-toggle="tab"]').on('shown.bs.tab', function () {
                        isActive = false;
                        switch ($(this).data('value')) {
                            case 'show-series':
                                seriesSearchHandle.setActive(true);
                                isSwitched = true;
                                seriesSearchHandle.applySearch(currentKeyword);
                                break;
                            case 'show-posts':
                                postsSearchHandle.setActive(true);
                                isSwitched = true;
                                postsSearchHandle.applySearch(currentKeyword);
                                break;
                            case 'find-new':
                                break;
                        }
                    });
                }
                this.maximize = function () {
                    domFilterRoot.draggable( "destroy" );
                    domFilterRoot.removeClass('size-small');
                    domFilterRoot.removeClass('size-closed');
                    domFilterRoot.addClass('size-full');
                    domFilterRoot.css('height', 'auto');
                    domFilterRoot.css('left', '0px');
                    domFilterRoot.css('top', '0px');
                    domFilterRoot.parent().removeClass('filter-collapsed');
                }
                this.small = function () {
                    domFilterRoot.removeClass('size-full');
                    domFilterRoot.removeClass('size-closed');
                    domFilterRoot.draggable({
                        addClasses: false,
                        handle: ".drag-handler",
                        appendTo: "parent"
                    });
                    domFilterRoot.css('left', 'calc(100% - 800px)');
                    domFilterRoot.css('top', '100px');
                    domFilterRoot.addClass('size-small');
                    domFilterRoot.parent().addClass('filter-collapsed');
                }
                this.close = function () {
                    domFilterRoot.draggable( "destroy" );
                    domFilterRoot.removeClass('size-full');
                    domFilterRoot.removeClass('size-small');
                    domFilterRoot.css('height', domFilterRoot.css('height'));
                    domFilterRoot.addClass('size-closed');
                    domFilterRoot.parent().addClass('filter-collapsed');
                }
                this.data = function () {
                    return {
                        filter: {
                            free: domFilterRoot.find('input#free-content').prop('checked') ? 1 : 0,
                            premium: domFilterRoot.find('input#premium-content').prop('checked') ? 1 : 0,
                            affiliate: domFilterRoot.find('input#affiliates-content').prop('checked') ? 1 : 0
                        },
                        sort: domSortByDropDown.data('value')
                    };
                }
                this.fillCategories = function (cData) {
                    cData.simplify.forEach(function (c) {
                        var domTp = domRoot.find('.simplify-browse .browse-drop-down-inner ul li.sample').clone().removeClass('sample').removeAttr('hidden');
                        domTp.find('a').attr('href', 'browse?id=' + c.category_ID).find('span').text(c.strCategory_name);
                        domTp.appendTo(domRoot.find('.simplify-browse .browse-drop-down-inner ul'));
                    });
                    cData.enrich.forEach(function (c) {
                        var domTp = domRoot.find('.enrich-browse .browse-drop-down-inner ul li.sample').clone().removeClass('sample').removeAttr('hidden');
                        domTp.find('a').attr('href', 'browse?id=' + c.category_ID).find('span').text(c.strCategory_name);
                        domTp.appendTo(domRoot.find('.enrich-browse .browse-drop-down-inner ul'));
                    });
                }
                this.init = function () {
                    domFilterRoot = domRoot.find('.filter-wrapper');
                    domSortByDropDown = domFilterRoot.find('.sort-by .dropdown');
                    bindEvents();
                }
            }

            var SearchRowClass = function (from) {
                var domSearchRow, domItemsList;

                var self = this, itemHandles = [], filteredHandles = [];

                var bindData = function () {
                    switch (from) {
                        case 'youtube':
                            domSearchRow.find('.from-name').html('from Youtube');
                            break;
                        case 'podcasts':
                            domSearchRow.find('.from-name').html('from Podcasts');
                            break;
                        case 'blog':
                            domSearchRow.find('.from-name').html('from Blogs');
                            break;
                        case 'recipes':
                            domSearchRow.find('.from-name').html('from Recipes');
                            break;
                        case 'ideabox':
                            domSearchRow.find('.from-name').html('from Ideabox');
                            break;
                        case 'posts':
                            domSearchRow.find('.from-name').html('from Posts');
                            break;
                        case 'rssb-posts':
                            domSearchRow.find('.from-name').html('from RSSBlogPost');
                            break;
                        default:
                            domSearchRow.find('.from-name').html('from Posts');
                            break;

                    }
                }

                var bindEvents = function () {
                    domItemsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 10;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }

                var formatItemData = function(itemData){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.name;
                            fData.summary = '';
                            fData.body = itemData.body;
                            fData.image = itemData.image;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    fData.origin = itemData;
                    return fData;
                }

                var SearchItemClass = function (itemData) {

                    var dataLoaded = false, isAdded = false;
                    var domItem;

                    var self = this;

                    var bindData = function () {
                        dataLoaded = true;
                        domItem.find('.item-img').attr('src', itemData.image);
                        domItem.find('.item-title').html(itemData.title);
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            itemData.duration = res;
                            domItem.find('.blog-duration').html(res);
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.add-btn').click(function () {
                            var sery = series[domItem.find('select[name="series"]').val()];
                            if (parseInt(sery.intSeries_client_ID) === parseInt(CLIENT_ID)){
                                self.addToPost(sery.series_ID).then(function (res) {
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                        domItem.find('.preview-btn').click(function () {
                            preview();
                        });
                        domItem.find('.item-title').click(function () {
                            preview();
                        });
                    }
                    let preview = function () {
                        let form = $('<form action="viewpost" method="post" hidden target="_blank"></form>');
                        form.append('<input name="type" value="'+ itemData['type'] +'"/>');
                        form.append('<input name="img" value="'+ itemData['image'] +'"/>');
                        form.append('<input name="seriesTitle" value="New Content From '+ from +'"/>');
                        form.append('<input name="title" value="'+ itemData['title'] +'"/>');
                        form.append('<input name="body" value="'+ itemData['body'] +'"/>');
                        form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                        form.appendTo('body').submit().remove();
                    }
                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }
                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }
                        return ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }

                    this.subscriptionFormat = function () {
                        var f = {
                            strClientSubscription_title: itemData.title,
                            strClientSubscription_body: itemData.body,
                            strClientSubscription_image: itemData.image,
                            intClientSubscriptions_type: itemData.type
                        }
                        if (itemData.duration) {
                            f.strSubscription_duration = itemData.duration;
                        }
                        return f;
                    }
                    this.postFormat = function () {
                        var f = {
                            strPost_title: itemData.title,
                            strPost_body: itemData.body,
                            strPost_summary: itemData.summary,
                            intPost_type: itemData.type,
                            strPost_featuredimage: itemData.image
                        }
                        if (itemData.duration) {
                            f.strPost_duration = itemData.duration;
                        }
                        return f;
                    }

                    this.remove = function () {
                        domItem.remove();
                    }
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.bindData = bindData;
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domItemsList);
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.init = function () {
                        domItem = domSearchRow.find('.search-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        domItem.find('select[name="series"]').select2({
                            // dropdownParent: domItem.find('.hover-content')
                        });
                        bindEvents();
                    }
                }
                this.setItems = function (itemsData, newKeyword) {
                    domItemsList.slick('unslick');
                    itemHandles.forEach(function (value) {
                        value.remove();
                    });
                    itemHandles = [];
                    domSearchRow.find('.found-value').html(itemsData.items.length);
                    itemsData.items.forEach(function (item, i) {
                        var itemHandle = new SearchItemClass(formatItemData(item));
                        itemHandle.init();
                        if (i < 15) {
                            itemHandle.bindData();
                        }
                        itemHandles.push(itemHandle);
                    });
                    switch (sortBy) {
                        case 'audio':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().type == 0;
                            });
                            break;
                        case 'video':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().type == 2;
                            });
                            break;
                        case 'text':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                            });
                            break;
                        default:
                            filteredHandles = itemHandles;
                            break;
                    }
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        centerMode: false,
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                            {
                                breakpoint: 769,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            },
                        ]
                    });
                }

                this.refreshItems = function () {
                    domItemsList.css('height', domItemsList.css('height'));
                    domItemsList.css('opacity', 0);
                    setTimeout(function () {
                        domItemsList.slick('unslick');
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        switch (sortBy) {
                            case 'audio':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 0;
                                });
                                break;
                            case 'video':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 2;
                                });
                                break;
                            case 'text':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                });
                                break;
                            default:
                                filteredHandles = itemHandles;
                                break;
                        }
                        filteredHandles.forEach(function (value) {
                            value.append();
                        });
                        domItemsList.css('height', 'auto');
                        domItemsList.slick({
                            dots: false,
                            infinite: false,
                            centerMode: false,
                            arrows: true,
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            nextArrow: domSearchRow.find('.arrow-right'),
                            prevArrow: domSearchRow.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 3,
                                    }
                                },
                                {
                                    breakpoint: 769,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                    }
                                },
                            ]
                        });
                        domItemsList.css('opacity', 1);
                    }, 300);
                }
                this.init = function () {
                    domSearchRow = domRoot.find('.search-row.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('.search-container'));
                    domItemsList = domSearchRow.find('.search-items-list');
                    bindData();
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        centerMode: false,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                            {
                                breakpoint: 769,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            },
                        ]
                    });
                    bindEvents();
                }
            }

            var bindEvents = function () {
                domHeaderSearchForm.submit(function () {
                    if (isActive) {
                        currentKeyword = domHeaderSearchForm.find('input').val();
                        self.applySearch(currentKeyword);
                        return false;
                    }
                });

                domFooterSearchForm.submit(function () {
                    if (isActive) {
                        currentKeyword = domFooterSearchForm.find('input').val();
                        self.applySearch(currentKeyword);
                        return false;
                    }
                });
            }
            this.fillCategories = function (cateData) {
                filterHandle.fillCategories(cateData);
            }
            this.applySearch = function (kwd) {
                currentKeyword = kwd;
                self.searchByKeyword(kwd).then(function (res) {
                    searchedData = res.data;
                    searchedData.keyword = kwd;
                    var isEmpty = !(res.data.youtube.items.length || res.data.spreaker.items.length || res.data.blogs.items.length);
                    if (isSwitched) {
                        if (isEmpty) {
                            domRoot.addClass('no-content');
                        }
                        else {
                            domRoot.removeClass('no-content');
                            youtubeListHandle.setItems(res.data.youtube, kwd);
                            podcastsListHandle.setItems(res.data.spreaker, kwd);
                            blogsListHandle.setItems(res.data.blogs, kwd);
                        }
                        isSwitched = false;
                    }
                    else if (isSearchOpened) {
                        domSiteSearch.css('opacity', 0);
                        setTimeout(function () {
                            if (isEmpty) {
                                domRoot.addClass('no-content');
                            }
                            else {
                                domRoot.removeClass('no-content');
                                youtubeListHandle.setItems(res.data.youtube, kwd);
                                podcastsListHandle.setItems(res.data.spreaker, kwd);
                                blogsListHandle.setItems(res.data.blogs, kwd);
                            }
                            setTimeout(function () {
                                domSiteSearch.css('opacity', 1);
                            }, 0);
                        }, 300);
                    }
                    else {
                        activeSearch().then(function () {
                            if (isEmpty) {
                                domRoot.addClass('no-content');
                            }
                            else {
                                domRoot.removeClass('no-content');
                                youtubeListHandle.setItems(res.data.youtube, kwd);
                                podcastsListHandle.setItems(res.data.spreaker, kwd);
                                blogsListHandle.setItems(res.data.blogs, kwd);
                            }
                        })
                    }
                });
            }
            this.searchByKeyword = function (keyword) {
                var ajaxData = {
                    q: keyword,
                    size: 50,
                    items: [
                        {name: 'youtube', token: false},
                        {name: 'spreaker', token: false},
                        {name: 'blogs', token: false},
                        // {name: 'recipes', token: false},
                        // {name: 'ideabox', token: false},
                        // {name: 'posts', token: false},
                        // {name: 'rssbPosts', token: false},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData);
            }
            this.setActive = function (v) {
                isActive = v;
            }
            this.init = function () {
                domRoot = domSiteSearch.find('.find-new-content');
                domSearchHeader = domRoot.find('header.search-header');
                domSortByDropDown = domSearchHeader.find('.sort-by .dropdown');
                domHeaderSearchForm = $('.home-header form.search-for-inspiration');
                domFooterSearchForm = $('.home-footer form.search-for-inspiration');

                filterHandle = new FilterClass();
                filterHandle.init();

                ajaxAPiHandle.apiPost('Series.php', {}, false).then(function (res) {
                    res.data.forEach(function (sery) {
                        series[sery.series_ID] = sery;
                        var optHtml = '<option value="'+ sery.series_ID +'">'+ sery.strSeries_title +'</option>';
                        domSiteSearch.find('.search-container .search-item.sample select[name="series"]').append(optHtml);
                    });
                });
                youtubeListHandle = new SearchRowClass('youtube');
                youtubeListHandle.init();

                podcastsListHandle = new SearchRowClass('podcasts');
                podcastsListHandle.init();

                blogsListHandle = new SearchRowClass('blog');
                blogsListHandle.init();

                bindEvents();
            }
        }

        var SeriesSearchClass = function () {

            var domSeriesRoot;

            var mainHandle = null;
            var filterHandle = null;
            var isActive = true;
            var currentKeyword;
            var sortBy = -1;
            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';
            var series = [];
            var self = this;

            var FilterClass = function () {

                var domRoot;
                var domSortByDropDown;
                var self = this;

                var bindEvents = function () {
                    domSortByDropDown.find('ul li a').click(function () {
                        domSortByDropDown.find('ul li').removeAttr('hidden');
                        $(this).parent().attr('hidden', true);
                        var v = $(this).data('value');
                        var txt = $(this).text();
                        domSortByDropDown.data('value', v);
                        domSortByDropDown.find('button span').text(txt);
                        mainHandle.applyFilter();
                    });
                    domRoot.find('.filters input[type="checkbox"]').change(function () {
                        mainHandle.applyFilter();
                    });
                    domRoot.find('.resize-filter').click(function () {
                        if (domRoot.hasClass('size-full')) {
                            self.small();
                        }
                        else {
                            self.maximize();
                        }
                    });
                    domRoot.find('.open-img').click(function () {
                        self.small();
                    });
                    domRoot.find('.close-img').click(function () {
                        self.close();
                    });
                    domRoot.find('[data-toggle="tab"]').on('shown.bs.tab', function () {
                        isActive = false;
                        switch ($(this).data('value')) {
                            case 'show-posts':
                                postsSearchHandle.setActive(true);
                                isSwitched = true;
                                postsSearchHandle.applySearch(currentKeyword);
                                break;
                            case 'find-new':
                                findNewContentHandle.setActive(true);
                                isSwitched = true;
                                findNewContentHandle.applySearch(currentKeyword);
                                break;
                        }
                    });
                }
                this.maximize = function () {
                    domRoot.draggable( "destroy" );
                    domRoot.removeClass('size-small');
                    domRoot.removeClass('size-closed');
                    domRoot.addClass('size-full');
                    domRoot.css('height', 'auto');
                    domRoot.css('left', '0px');
                    domRoot.css('top', '0px');
                    domRoot.parent().removeClass('filter-collapsed');
                }
                this.small = function () {
                    domRoot.removeClass('size-full');
                    domRoot.removeClass('size-closed');
                    domRoot.draggable({
                        addClasses: false,
                        handle: ".drag-handler",
                        appendTo: "parent"
                    });
                    domRoot.css('left', 'calc(100% - 800px)');
                    domRoot.css('top', '100px');
                    domRoot.addClass('size-small');
                    domRoot.parent().addClass('filter-collapsed');
                }
                this.close = function () {
                    domRoot.draggable( "destroy" );
                    domRoot.removeClass('size-full');
                    domRoot.removeClass('size-small');
                    domRoot.css('height', domRoot.css('height'));
                    domRoot.addClass('size-closed');
                    domRoot.parent().addClass('filter-collapsed');
                }
                this.data = function () {
                    return {
                        filter: {
                            free: domRoot.find('input#free-content').prop('checked') ? 1 : 0,
                            premium: domRoot.find('input#premium-content').prop('checked') ? 1 : 0,
                            affiliate: domRoot.find('input#affiliates-content').prop('checked') ? 1 : 0
                        },
                        sort: domSortByDropDown.data('value')
                    };
                }
                this.fillCategories = function (cData) {
                    cData.simplify.forEach(function (c) {
                        var domTp = domRoot.find('.simplify-browse .browse-drop-down-inner ul li.sample').clone().removeClass('sample').removeAttr('hidden');
                        domTp.find('a').attr('href', 'browse?id=' + c.category_ID).find('span').text(c.strCategory_name);
                        domTp.appendTo(domRoot.find('.simplify-browse .browse-drop-down-inner ul'));
                    });
                    cData.enrich.forEach(function (c) {
                        var domTp = domRoot.find('.enrich-browse .browse-drop-down-inner ul li.sample').clone().removeClass('sample').removeAttr('hidden');
                        domTp.find('a').attr('href', 'browse?id=' + c.category_ID).find('span').text(c.strCategory_name);
                        domTp.appendTo(domRoot.find('.enrich-browse .browse-drop-down-inner ul'));
                    });
                }
                this.init = function () {
                    domRoot = domSeriesRoot.find('.filter-wrapper');
                    domSortByDropDown = domRoot.find('.sort-by .dropdown');
                    bindEvents();
                }
            }

            var MainClass = function () {
                var domRoot, domUserResult;

                var itemHandles = [];
                var filteredHandles = [];
                let userResultHandle;
                var UserResultClass = function () {
                    let domList;
                    let itemHandles = [];
                    let ItemClass = function (user) {
                        let domItem;
                        let bindData = function () {
                            if (user.image) {
                                domItem.find('.item-image').css('background-image', 'url("'+ user.image +'")');
                            }
                            domItem.find('.user-name').text(user.f_name);
                        }
                        this.remove = function () {
                            domItem.remove();
                        }
                        this.init = function () {
                            domItem = domUserResult.find('.list__item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domList);
                            bindData();
                        }
                    }
                    this.setItems = function (items) {
                        if (items.length > 0) {
                            domUserResult.show();
                            domRoot.find('.series-result-title').show();
                        }
                        domList.slick('unslick');
                        itemHandles.forEach(function(hdl) {
                            hdl.remove();
                        });
                        items.forEach(function (user) {
                            let hdl = new ItemClass(user);
                            hdl.init();
                            itemHandles.push(hdl);
                        });
                        domList.slick({
                            dots: false,
                            infinite: false,
                            centerMode: false,
                            arrows: true,
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            nextArrow: domUserResult.find('.arrow-right'),
                            prevArrow: domUserResult.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 3,
                                    }
                                },
                                {
                                    breakpoint: 900,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 2,
                                    }
                                },
                                {
                                    breakpoint: 769,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                    }
                                },
                            ]
                        });
                        if (items.length === 0) {
                            domUserResult.hide();
                            domRoot.find('.series-result-title').hide();
                        }
                    }
                    this.init = function () {
                        domList = domUserResult.find('.user--list');
                        domList.slick({
                            dots: false,
                            infinite: false,
                            centerMode: false,
                            arrows: true,
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            nextArrow: domUserResult.find('.arrow-right'),
                            prevArrow: domUserResult.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 3,
                                    }
                                },
                                {
                                    breakpoint: 900,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 2,
                                    }
                                },
                                {
                                    breakpoint: 769,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                    }
                                },
                            ]
                        });
                    }
                }
                var SeriesRowClass = function (seriesData) {
                    let relevantHandle;
                    let domRow, domSeriesCard, domPostSection, domPostsList, domCircleNavWrap;
                    let self = this, rowRootHandle = this;
                    let bindData = function () {
                        if (seriesData.purchased) {
                            domRow.addClass('purchased');
                        }
                        domSeriesCard.find('.item-image').css('background-image', 'url(' + seriesData.strSeries_image + ')');
                        domSeriesCard.find('.item-title').text(seriesData.strSeries_title);
                        domSeriesCard.find('.preview-btn').attr('href', 'preview_series?id=' + seriesData.series_ID);
                        domSeriesCard.css('order', itemHandles.length + 1);
                        domSeriesCard.find('.item-category').attr('href', 'browse?id=' + seriesData.category.category_ID).html(seriesData.category.strCategory_name);
                        domSeriesCard.find('.child-total-count').html(seriesData.posts_count);
                        relevantHandle.setItems({items: seriesData.posts});
                        domRow.find('.preview--wrap').attr('href', BASE_URL + '/preview_series?id=' + seriesData.series_ID);
                        let social_links = socialLinks();
                        domCircleNavWrap.find('.facebook-link').attr('href', social_links.facebook);
                        domCircleNavWrap.find('.twitter-link').attr('href', social_links.twitter);
                        domCircleNavWrap.find('.google-link').attr('href', social_links.google);
                        domCircleNavWrap.find('.linkedin-link').attr('href', social_links.linkedin);
                        domCircleNavWrap.find('.mail-link').attr('href', social_links.email);
                    }
                    let join = function(source) {
                        ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: seriesData.series_ID, source: source}).then(function (res) {
                            if (res.status == true){
                                seriesData.purchased = res.data;
                                domRow.addClass('purchased');
                            }
                        });
                    }

                    let unJoin = function () {
                        swal("You already joined this series!", {
                            content: 'Thanks!'
                        });
                        return false;
                        //
                        // ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: itemData.purchased}).then(function (res) {
                        //     if (res.status == true){
                        //         itemData.purchased = 0;
                        //     }
                        // });
                    }
                    var openStripeCard = function () {
                        stripeCardModalHandle.setSeries(seriesData);
                        stripeCardModalHandle.open();
                        stripeCardModalHandle.afterJoin(function (res) {
                            if (res.status == true){
                                seriesData.purchased = res.data;
                                domRow.addClass('purchased');
                            }
                        });
                    }
                    let bindEvents = function () {
                        domSeriesCard.find('.show--relevant-content').click(function () {
                            self.showRelevant();
                        });
                        if (helperHandle.isMobile()) {
                            domPostSection.find('.relevant--title').click(function () {
                                self.hideRelevant();
                            });
                        }
                        domSeriesCard.find('.join-btn').click(function () {
                            if (CLIENT_ID > -1) {
                                if (parseInt(seriesData.boolSeries_charge) === 1 && parseInt(seriesData.purchased) == 0) {
                                    openStripeCard();
                                }
                                else {
                                    parseInt(seriesData.purchased) ? unJoin() : join();
                                }
                            } else {
                                authHandle.activeLogin();
                                authHandle.popup();
                                authHandle.afterLogin(function () {
                                    window.location.reload();
                                });
                            }
                        })
                    }
                    let socialLinks = function () {
                        var viewUri = encodeURIComponent(BASE_URL + '/preview_series?id=' + seriesData.series_ID);
                        var title = encodeURIComponent(seriesData.strSeries_title);
                        var subject = encodeURIComponent('Shared from Walden.ly - ' + seriesData.strSeries_title);
                        var shareLinks = {
                            facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + viewUri + '&title=' + title,
                            twitter: 'https://twitter.com/intent/tweet?url=' + viewUri,
                            google: 'https://plus.google.com/share?url=' + viewUri,
                            email: 'mailto:?subject=' + subject + '&body=' + encodeURIComponent('I thought you might like this: ') + viewUri,
                            linkedin: 'https://www.linkedin.com/shareArticle?mini=true&url='+ viewUri +'&title='+ title +'&summary='+ subject +'&source='
                        }
                        return shareLinks;
                    }
                    var RelevantClass = function (from) {
                        var domSearchRow, domItemsList;

                        var self = this, itemHandles = [], filteredHandles = [];

                        var bindData = function () {
                            domSearchRow.find('.relevant--title span').html(seriesData.strSeries_title);
                            switch (from) {
                                case 'youtube':
                                    domSearchRow.find('.from-name').html('from Youtube');
                                    break;
                                case 'podcasts':
                                    domSearchRow.find('.from-name').html('from Podcasts');
                                    break;
                                case 'blog':
                                    domSearchRow.find('.from-name').html('from Blogs');
                                    break;
                                case 'recipes':
                                    domSearchRow.find('.from-name').html('from Recipes');
                                    break;
                                case 'ideabox':
                                    domSearchRow.find('.from-name').html('from Ideabox');
                                    break;
                                case 'posts':
                                    domSearchRow.find('.from-name').html('from Posts');
                                    break;
                                case 'rssb-posts':
                                    domSearchRow.find('.from-name').html('from RSSBlogPost');
                                    break;
                                default:
                                    domSearchRow.find('.from-name').html('from Posts');
                                    break;

                            }
                        }

                        var bindEvents = function () {
                            domItemsList.on('afterChange', function (e, slick, currentSlide) {
                                var loadIndex = currentSlide + 10;
                                for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                                    if (itemHandles[i].isDataLoaded()) {
                                        continue;
                                    }
                                    itemHandles[i].bindData();
                                }
                            });
                        }

                        var formatItemData = function(itemData){
                            var fData = {};
                            switch (from) {
                                case 'youtube':
                                    fData.title = itemData.snippet.title;
                                    fData.summary = itemData.snippet.description;
                                    fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                                    fData.image = itemData.snippet.thumbnails.high.url;
                                    fData.type = 2;
                                    break;
                                case 'podcasts':
                                    fData.title = itemData.title;
                                    fData.summary = '';
                                    fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                                    fData.image = itemData.image_url;
                                    fData.type = 0;
                                    break;
                                case 'blog':
                                    fData.title = itemData.name;
                                    fData.summary = '';
                                    fData.body = itemData.body;
                                    fData.image = itemData.image;
                                    fData.type = 7;
                                    break;
                                case 'recipes':
                                    fData.title = itemData.title;
                                    fData.summary = '';
                                    fData.body = itemData.source_url;
                                    fData.image = itemData.image_url;
                                    fData.type = 7;
                                    break;
                                case 'ideabox':
                                    fData.title = itemData.strIdeaBox_title;
                                    fData.summary = '';
                                    fData.body = itemData.strIdeaBox_idea;
                                    fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                                    fData.type = 7;
                                    break;
                                case 'posts':
                                    fData.id = itemData.post_ID;
                                    fData.title = itemData.strPost_title;
                                    fData.summary = itemData.strPost_summary;
                                    fData.body = itemData.strPost_body;
                                    fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                                    fData.type = itemData.intPost_type;
                                    break;
                                case 'rssb-posts':
                                    fData.title = itemData.strRSSBlogPosts_title;
                                    fData.summary = itemData.strRSSBlogPosts_description;
                                    fData.body = itemData.strRSSBlogPosts_content;
                                    fData.image = DEFAULT_IMAGE;
                                    fData.type = 7;
                                    break;
                                default:
                                    fData.title = itemData.strPost_title;
                                    fData.summary = itemData.strPost_summary;
                                    fData.body = itemData.strPost_body;
                                    fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                                    fData.type = itemData.intPost_type;
                                    break;
                            }
                            fData.origin = itemData;
                            return fData;
                        }

                        var SearchItemClass = function (itemData) {

                            var dataLoaded = false, isAdded = false;
                            var domItem;

                            var self = this;

                            var bindData = function () {
                                dataLoaded = true;
                                domItem.find('.item-img').attr('src', itemData.image);
                                domItem.find('.item-title').html(itemData.title).attr('href', 'view_blog?id=' + itemData.id);
                                domItem.find('.preview-btn').attr('href', 'view_blog?id=' + itemData.id);
                                helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                                    domItem.find('.blog-duration').html(res);
                                });
                            }

                            var bindEvents = function () {
                                domItem.find('.add-btn').click(function () {
                                    var sery = series[domItem.find('select[name="series"]').val()];
                                    if (parseInt(sery.intSeries_client_ID) === parseInt(CLIENT_ID)){
                                        self.addToPost(sery.series_ID).then(function (res) {
                                        });
                                    }
                                    else {
                                        self.addToSuscription(sery.purchased_ID, false);
                                    }
                                });
                            }
                            this.addToPost = function (seryId) {
                                var ajaxData = {
                                    action: 'add',
                                    sets: self.postFormat()
                                };
                                ajaxData.sets.intPost_series_ID = seryId;
                                return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function (res) {
                                    if (res.status){
                                        isAdded = true;
                                        domItem.addClass('added-post');
                                    }
                                    return true;
                                });
                            }
                            this.addToSuscription = function (purchasedId, postId) {
                                var ajaxData = {
                                    action: 'add',
                                    sets: self.subscriptionFormat()
                                };
                                ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                                if (postId){
                                    ajaxData.sets.intClientSubscription_post_ID = postId;
                                }
                                return ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                                    if (res.status){
                                        isAdded = true;
                                        domItem.addClass('added-post');
                                    }
                                    return true;
                                });
                            }

                            this.subscriptionFormat = function () {
                                return {
                                    strClientSubscription_title: itemData.title,
                                    strClientSubscription_body: itemData.body,
                                    strClientSubscription_image: itemData.image,
                                    intClientSubscriptions_type: itemData.type
                                }
                            }
                            this.postFormat = function () {
                                return {
                                    strPost_title: itemData.title,
                                    strPost_body: itemData.body,
                                    strPost_summary: itemData.summary,
                                    intPost_type: itemData.type,
                                    strPost_featuredimage: itemData.image
                                }
                            }

                            this.remove = function () {
                                domItem.remove();
                            }
                            this.isDataLoaded = function () {
                                return dataLoaded;
                            }
                            this.bindData = bindData;
                            this.detach = function () {
                                domItem.detach();
                            }
                            this.append = function () {
                                domItem.appendTo(domItemsList);
                            }
                            this.data = function () {
                                return itemData;
                            }
                            this.init = function () {
                                domItem = domSearchRow.find('.search-item.sample').clone().removeClass('sample').removeAttr('hidden');
                                domItem.find('select[name="series"]').select2({
                                    // dropdownParent: domItem.find('.hover-content')
                                });
                                bindEvents();
                            }
                        }
                        this.renderView = function () {
                            filteredHandles.forEach(function (value) {
                                value.append();
                            });
                            domItemsList.slick({
                                dots: false,
                                infinite: false,
                                centerMode: false,
                                arrows: true,
                                slidesToShow: 3,
                                slidesToScroll: 2,
                                nextArrow: domSearchRow.find('.arrow-right'),
                                prevArrow: domSearchRow.find('.arrow-left'),
                                responsive: [
                                    {
                                        breakpoint: 1200,
                                        settings: {
                                            slidesToShow: 3,
                                            slidesToScroll: 2,
                                        }
                                    },
                                    {
                                        breakpoint: 769,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                        }
                                    },
                                ]
                            });
                        }
                        this.detachView = function () {
                            domItemsList.slick('unslick');
                            itemHandles.forEach(function (value) {
                                value.detach();
                            });
                        }
                        this.setItems = function (itemsData, newKeyword) {
                            domSeriesCard.find('.child-filtered-count').html(itemsData.items.length);
                            domItemsList.slick('unslick');
                            itemHandles.forEach(function (value) {
                                value.remove();
                            });
                            itemHandles = [];
                            domSearchRow.find('.found-value').html(itemsData.items.length);
                            itemsData.items.forEach(function (item, i) {
                                var itemHandle = new SearchItemClass(formatItemData(item));
                                itemHandle.init();
                                if (i < 15) {
                                    itemHandle.bindData();
                                }
                                itemHandles.push(itemHandle);
                            });
                            var sortBy = filterHandle.data().sort;
                            switch (sortBy) {
                                case 'audio':
                                    filteredHandles = itemHandles.filter(function (value) {
                                        return value.data().type == 0;
                                    });
                                    break;
                                case 'video':
                                    filteredHandles = itemHandles.filter(function (value) {
                                        return value.data().type == 2;
                                    });
                                    break;
                                case 'text':
                                    filteredHandles = itemHandles.filter(function (value) {
                                        return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                    });
                                    break;
                                default:
                                    filteredHandles = itemHandles;
                                    break;
                            }
                            if (!helperHandle.isMobile()) {
                                self.renderView();
                            }
                        }
                        this.isFiltered = function () {
                            var sortBy = filterHandle.data().sort;
                            var fHandles = [];
                            switch (sortBy) {
                                case 'audio':
                                    fHandles = itemHandles.filter(function (value) {
                                        return value.data().type == 0;
                                    });
                                    break;
                                case 'video':
                                    fHandles = itemHandles.filter(function (value) {
                                        return value.data().type == 2;
                                    });
                                    break;
                                case 'text':
                                    fHandles = itemHandles.filter(function (value) {
                                        return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                    });
                                    break;
                                default:
                                    fHandles = itemHandles;
                                    break;
                            }
                            return fHandles.length;
                        }
                        this.refreshItems = function () {
                            domItemsList.css('height', domItemsList.css('height'));
                            domItemsList.css('opacity', 0);
                            setTimeout(function () {
                                domItemsList.slick('unslick');
                                filteredHandles.forEach(function (value) {
                                    value.detach();
                                });
                                var sortBy = filterHandle.data().sort;
                                switch (sortBy) {
                                    case 'audio':
                                        filteredHandles = itemHandles.filter(function (value) {
                                            return value.data().type == 0;
                                        });
                                        break;
                                    case 'video':
                                        filteredHandles = itemHandles.filter(function (value) {
                                            return value.data().type == 2;
                                        });
                                        break;
                                    case 'text':
                                        filteredHandles = itemHandles.filter(function (value) {
                                            return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                        });
                                        break;
                                    default:
                                        filteredHandles = itemHandles;
                                        break;
                                }
                                filteredHandles.forEach(function (value) {
                                    value.append();
                                });
                                domItemsList.css('height', 'auto');
                                domItemsList.slick({
                                    dots: false,
                                    infinite: false,
                                    centerMode: false,
                                    arrows: true,
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    nextArrow: domSearchRow.find('.arrow-right'),
                                    prevArrow: domSearchRow.find('.arrow-left'),
                                    responsive: [
                                        {
                                            breakpoint: 1200,
                                            settings: {
                                                slidesToShow: 3,
                                                slidesToScroll: 2,
                                            }
                                        },
                                        {
                                            breakpoint: 769,
                                            settings: {
                                                slidesToShow: 1,
                                                slidesToScroll: 1,
                                            }
                                        },
                                    ]
                                });
                                domItemsList.css('opacity', 1);
                            }, 300);
                        }
                        this.init = function () {
                            domSearchRow = domPostSection;
                            domItemsList = domSearchRow.find('.search-items-list');
                            bindData();
                            domItemsList.slick({
                                dots: false,
                                infinite: false,
                                centerMode: false,
                                arrows: true,
                                slidesToShow: 3,
                                slidesToScroll: 2,
                                nextArrow: domSearchRow.find('.arrow-right'),
                                prevArrow: domSearchRow.find('.arrow-left'),
                                responsive: [
                                    {
                                        breakpoint: 1200,
                                        settings: {
                                            slidesToShow: 3,
                                            slidesToScroll: 2,
                                        }
                                    },
                                    {
                                        breakpoint: 769,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                        }
                                    },
                                ]
                            });
                            bindEvents();
                        }
                    }
                    this.isFiltered = function () {
                        var filterData = filterHandle.data().filter;
                        if (filterData.free || filterData.premium || filterData.affiliate) {
                            if (filterData.free && !parseInt(seriesData.boolSeries_charge)) {
                                return relevantHandle.isFiltered();
                            }
                            if (filterData.premium && parseInt(seriesData.boolSeries_charge)) {
                                return relevantHandle.isFiltered();
                            }
                            if (filterData.affiliate && parseInt(seriesData.boolSeries_affiliated)) {
                                return relevantHandle.isFiltered();
                            }
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    this.remove = function () {
                        domRow.remove();
                    }
                    this.detach = function () {
                        return domRow.detach();
                    }
                    this.append = function () {
                        domRow.appendTo(domRoot.find('.content__list'));
                    }
                    this.data = function () {
                        return seriesData;
                    }
                    this.relevantHandle = function () {
                        return relevantHandle;
                    }
                    this.showRelevant = function () {
                        domRow.css('min-height', domRow.css('height'));
                        domRow.css('opacity', 0);
                        setTimeout(function () {
                            domRow.addClass('state-show-relevant');
                            relevantHandle.renderView();
                            domRow.css('opacity', 1);
                            domRow.css('min-height', '0px');
                        }, 300);
                    }
                    this.hideRelevant = function () {
                        domRow.css('min-height', domRow.css('height'));
                        domRow.css('opacity', 0);
                        setTimeout(function () {
                            domRow.removeClass('state-show-relevant');
                            relevantHandle.detachView();
                            domRow.css('opacity', 1);
                            domRow.css('min-height', '0px');
                        }, 300);
                    }
                    this.init = function () {
                        domRow = domRoot.find('.content__list .list__item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domRoot.find('.content__list'));
                        domSeriesCard = domRow.find('.series-article');
                        domPostSection = domRow.find('.relevant-result');
                        domPostsList = domPostSection.find('.search-items-list');
                        domCircleNavWrap = domRow.find('.circle-nav-wrapper');
                        relevantHandle = new RelevantClass('posts');
                        relevantHandle.init();
                        bindData();
                        domRow.find('.share--wrap').circleNav(domCircleNavWrap);
                        bindEvents();
                    }
                };
                this.setItems = function (items) {
                    itemHandles.forEach(function (hdl) {
                        hdl.remove();
                    });
                    itemHandles = [];
                    filteredHandles = [];
                    items.forEach(function (itemData) {
                        var hdl = new SeriesRowClass(itemData);
                        hdl.init();
                        itemHandles.push(hdl);
                        filteredHandles.push(hdl);
                    });
                }
                this.setUsers = function (users) {
                    userResultHandle.setItems(users);
                }
                this.noContent = function () {
                    domSeriesRoot.addClass('no-content');
                }
                this.hasContent = function () {
                    domSeriesRoot.removeClass('no-content');
                }
                this.setItemsWithAnimation = function (items, users) {
                    domRoot.css('min-height', domRoot.css('height'));
                    domRoot.css('opacity', 0);
                    domSeriesRoot.find('.no-content-wrapper').css('opacity', 0);
                    setTimeout(function () {
                        mainHandle.setItems(items);
                        mainHandle.setUsers(users);
                        domRoot.css('opacity', 1);
                        domSeriesRoot.find('.no-content-wrapper').css('opacity', 1);
                        domRoot.css('min-height', '0px');
                    }, 300);
                }

                this.applyFilter = function () {
                    domRoot.css('height', domRoot.css('height'));
                    domRoot.css('opacity', 0);
                    setTimeout(function () {
                        filteredHandles.forEach(function (hdl) {
                            hdl.detach();
                        });
                        filteredHandles = itemHandles.filter(function (hdl) {
                            return  hdl.isFiltered();
                        });
                        var sortBy = filterHandle.data().sort;
                        filteredHandles.forEach(function (hdl) {
                            hdl.relevantHandle().refreshItems();
                        });
                        filteredHandles = filteredHandles.sort(function (a, b) {
                            var ad = a.data();
                            var bd = b.data();
                            switch (sortBy) {
                                case 'new':
                                    return parseInt(bd.series_ID) -  parseInt(ad.series_ID);
                                    break;
                                case 'a-z':
                                    return ad.strSeries_title > bd.strSeries_title ? 1 : (ad.strSeries_title == bd.strSeries_title ? 0 : -1);
                                    break;
                                case 'z-a':
                                    return ad.strSeries_title > bd.strSeries_title ? -1 : (ad.strSeries_title == bd.strSeries_title ? 0 : 1);
                                    break;
                                default:
                                    return parseInt(ad.series_ID) -  parseInt(bd.series_ID);;
                                    break;
                            }
                        });
                        filteredHandles.forEach(function (value, i) {
                            value.append();
                        });
                        domRoot.css('opacity', 1);
                        domRoot.css('height', 'auto');
                    }, 300);
                }

                this.init = function () {
                    domRoot = domSeriesRoot.find('.series-search-content');
                    domUserResult = domRoot.find('.user-result');
                    userResultHandle = new UserResultClass();
                    userResultHandle.init();
                }
            };

            var bindEvents = function () {
                domHeaderSearchForm.submit(function () {
                    if (isActive) {
                        currentKeyword = domHeaderSearchForm.find('input').val();
                        self.applySearch(currentKeyword);
                        return false;
                    }
                });
                domFooterSearchForm.submit(function () {
                    if (isActive) {
                        currentKeyword = domFooterSearchForm.find('input').val();
                        self.applySearch(currentKeyword);
                        return false;
                    }
                });
            }
            this.setActive = function (v) {
                isActive = v;
            }
            this.applySearch = function (kwd) {
                currentKeyword = kwd;
                domSeriesRoot.find('.search-txt span').html(kwd);
                Promise.all([self.searchByKeyword(kwd), self.searchPostsByKeyword(kwd), self.searchUsersByKeyword(kwd)]).then(function (res) {
                    let seriesResult = res[0];
                    let postResult = res[1];
                    let userResult = res[2];
                    seriesResult.forEach(function (result) {
                        result.posts = postResult.filter(function (item) {
                            return item.intPost_series_ID == result.series_ID;
                        });
                    });
                    if (isSwitched) {
                        if (seriesResult.length + userResult.length) {
                            mainHandle.hasContent();
                            mainHandle.setItems(seriesResult);
                            mainHandle.setUsers(userResult);
                            isSwitched = false;
                        } else {
                            mainHandle.noContent();
                            mainHandle.setItems([]);
                            mainHandle.setUsers([]);
                        }
                    }
                    else if (isSearchOpened) {
                        if (seriesResult.length + userResult.length) {
                            mainHandle.hasContent();
                            mainHandle.setItemsWithAnimation(seriesResult, userResult);
                            isSwitched = false;
                        } else {
                            mainHandle.noContent();
                            mainHandle.setItems([]);
                            mainHandle.setUsers([]);
                        }
                    }
                    else {
                        activeSearch().then(function () {
                            if (seriesResult.length + userResult.length) {
                                mainHandle.setItems(seriesResult);
                                mainHandle.setUsers(userResult);
                            } else {
                                mainHandle.noContent();
                                mainHandle.setItems([]);
                                mainHandle.setUsers([]);
                            }
                        });
                    }
                });
            }
            this.searchByKeyword = function (kwd) {
                return ajaxAPiHandle.apiPost('Series.php', {action: 'search', where: [], keyword: kwd, pageToken: 0, size: 100}).then(function (res) {
                    if (res.status) {
                        return res.data;
                    }
                    return false;
                });
            }
            this.searchPostsByKeyword = function (keyword) {
                var ajaxData = {
                    q: keyword,
                    size: 300,
                    items: [
                        // {name: 'youtube', token: false},
                        // {name: 'spreaker', token: false},
                        // {name: 'blogs', token: false},
                        // {name: 'recipes', token: false},
                        // {name: 'ideabox', token: false},
                        {name: 'posts', token: false},
                        // {name: 'rssbPosts', token: false},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData).then(function (res) {
                    if (res.status) {
                        return res.data.posts.items;
                    }
                    return false;
                });
            }
            this.searchUsersByKeyword = function (keyword) {
                var ajaxData = {
                    action: 'search',
                    q: keyword,
                    size: 300,
                };
                return ajaxAPiHandle.apiPost('User.php', ajaxData).then(function (res) {
                    if (res.status) {
                        return res.data;
                    }
                    return [];
                });
            }
            this.fillCategories = function (cateData) {
                filterHandle.fillCategories(cateData);
            }
            this.init = function () {
                domSeriesRoot = domSiteSearch.find('.series-search-results');
                mainHandle = new MainClass();
                mainHandle.init();
                filterHandle = new FilterClass();
                filterHandle.init();
                bindEvents();
                ajaxAPiHandle.apiPost('Series.php', {}, false).then(function (res) {
                    res.data.forEach(function (sery) {
                        series[sery.series_ID] = sery;
                        var optHtml = '<option value="'+ sery.series_ID +'">'+ sery.strSeries_title +'</option>';
                        domSiteSearch.find('.relevant-result .search-item.sample select[name="series"]').append(optHtml);
                    });
                });
            }
        }

        var PostsSearchClass = function () {

            var domRoot, domSearchHeader, domSortByDropDown;
            var self = this, filterHandle, youtubeListHandle, podcastsListHandle, blogsListHandle, recipesListHandle, ideaboxListHandle, postsListHandle, rssbPostsListHandle;
            var series = [];
            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';
            var searchedData = {}, loadMore = false, sortBy = -1;
            var isActive = false;
            var currentKeyword;
            var FilterClass = function () {

                var domFilterRoot;
                var domSortByDropDown;
                var self = this;

                var bindEvents = function () {
                    domSortByDropDown.find('ul li a').click(function () {
                        domSortByDropDown.find('ul li').removeAttr('hidden');
                        $(this).parent().attr('hidden', true);
                        var v = $(this).data('value');
                        var txt = $(this).text();
                        domSortByDropDown.data('value', v);
                        domSortByDropDown.find('button span').text(txt);
                        sortBy = v;
                        postsListHandle.refreshItems();
                        rssbPostsListHandle.refreshItems();
                    });
                    domFilterRoot.find('.filters input[type="checkbox"]').change(function () {
                        // mainHandle.applyFilter();
                    });
                    domFilterRoot.find('.resize-filter').click(function () {
                        if (domFilterRoot.hasClass('size-full')) {
                            self.small();
                        }
                        else {
                            self.maximize();
                        }
                    });
                    domFilterRoot.find('.open-img').click(function () {
                        self.small();
                    });
                    domFilterRoot.find('.close-img').click(function () {
                        self.close();
                    });
                    domFilterRoot.find('[data-toggle="tab"]').on('shown.bs.tab', function () {
                        isActive = false;
                        switch ($(this).data('value')) {
                            case 'show-series':
                                seriesSearchHandle.setActive(true);
                                isSwitched = true;
                                seriesSearchHandle.applySearch(currentKeyword);
                                break;
                            case 'show-posts':
                                break;
                            case 'find-new':
                                findNewContentHandle.setActive(true);
                                isSwitched = true;
                                findNewContentHandle.applySearch(currentKeyword);
                                break;
                        }
                    });
                }
                this.maximize = function () {
                    domFilterRoot.draggable( "destroy" );
                    domFilterRoot.removeClass('size-small');
                    domFilterRoot.removeClass('size-closed');
                    domFilterRoot.addClass('size-full');
                    domFilterRoot.css('height', 'auto');
                    domFilterRoot.css('left', '0px');
                    domFilterRoot.css('top', '0px');
                    domFilterRoot.parent().removeClass('filter-collapsed');
                }
                this.small = function () {
                    domFilterRoot.removeClass('size-full');
                    domFilterRoot.removeClass('size-closed');
                    domFilterRoot.draggable({
                        addClasses: false,
                        handle: ".drag-handler",
                        appendTo: "parent"
                    });
                    domFilterRoot.css('left', 'calc(100% - 800px)');
                    domFilterRoot.css('top', '100px');
                    domFilterRoot.addClass('size-small');
                    domFilterRoot.parent().addClass('filter-collapsed');
                }
                this.close = function () {
                    domFilterRoot.draggable( "destroy" );
                    domFilterRoot.removeClass('size-full');
                    domFilterRoot.removeClass('size-small');
                    domFilterRoot.css('height', domFilterRoot.css('height'));
                    domFilterRoot.addClass('size-closed');
                    domFilterRoot.parent().addClass('filter-collapsed');
                }
                this.data = function () {
                    return {
                        filter: {
                            free: domFilterRoot.find('input#free-content').prop('checked') ? 1 : 0,
                            premium: domFilterRoot.find('input#premium-content').prop('checked') ? 1 : 0,
                            affiliate: domFilterRoot.find('input#affiliates-content').prop('checked') ? 1 : 0
                        },
                        sort: domSortByDropDown.data('value')
                    };
                }
                this.fillCategories = function (cData) {
                    cData.simplify.forEach(function (c) {
                        var domTp = domRoot.find('.simplify-browse .browse-drop-down-inner ul li.sample').clone().removeClass('sample').removeAttr('hidden');
                        domTp.find('a').attr('href', 'browse?id=' + c.category_ID).find('span').text(c.strCategory_name);
                        domTp.appendTo(domRoot.find('.simplify-browse .browse-drop-down-inner ul'));
                    });
                    cData.enrich.forEach(function (c) {
                        var domTp = domRoot.find('.enrich-browse .browse-drop-down-inner ul li.sample').clone().removeClass('sample').removeAttr('hidden');
                        domTp.find('a').attr('href', 'browse?id=' + c.category_ID).find('span').text(c.strCategory_name);
                        domTp.appendTo(domRoot.find('.enrich-browse .browse-drop-down-inner ul'));
                    });
                }
                this.init = function () {
                    domFilterRoot = domRoot.find('.filter-wrapper');
                    domSortByDropDown = domFilterRoot.find('.sort-by .dropdown');
                    bindEvents();
                }
            }

            var SearchRowClass = function (from) {
                var domSearchRow, domItemsList;

                var self = this, itemHandles = [], filteredHandles = [];

                var bindData = function () {
                    switch (from) {
                        case 'youtube':
                            domSearchRow.find('.from-name').html('from Youtube');
                            break;
                        case 'podcasts':
                            domSearchRow.find('.from-name').html('from Podcasts');
                            break;
                        case 'blog':
                            domSearchRow.find('.from-name').html('from Blogs');
                            break;
                        case 'recipes':
                            domSearchRow.find('.from-name').html('from Recipes');
                            break;
                        case 'ideabox':
                            domSearchRow.find('.from-name').html('from Ideabox');
                            break;
                        case 'posts':
                            domSearchRow.find('.from-name').html('from Posts');
                            break;
                        case 'rssb-posts':
                            domSearchRow.find('.from-name').html('from RSSBlogPost');
                            break;
                        default:
                            domSearchRow.find('.from-name').html('from Posts');
                            break;

                    }
                }

                var bindEvents = function () {
                    domItemsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 10;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }

                var formatItemData = function(itemData){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.name;
                            fData.summary = '';
                            fData.body = itemData.body;
                            fData.image = itemData.image;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    fData.origin = itemData;
                    return fData;
                }

                var SearchItemClass = function (itemData) {

                    var dataLoaded = false, isAdded = false;
                    var domItem;

                    var self = this;

                    var bindData = function () {
                        dataLoaded = true;
                        domItem.find('.item-img').attr('src', itemData.image);
                        domItem.find('.item-title').html(itemData.title);
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            domItem.find('.blog-duration').html(res);
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.add-btn').click(function () {
                            var sery = series[domItem.find('select[name="series"]').val()];
                            if (parseInt(sery.intSeries_client_ID) === parseInt(CLIENT_ID)){
                                self.addToPost(sery.series_ID).then(function (res) {
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                    }
                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }
                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }
                        return ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }

                    this.subscriptionFormat = function () {
                        return {
                            strClientSubscription_title: itemData.title,
                            strClientSubscription_body: itemData.body,
                            strClientSubscription_image: itemData.image,
                            intClientSubscriptions_type: itemData.type
                        }
                    }
                    this.postFormat = function () {
                        return {
                            strPost_title: itemData.title,
                            strPost_body: itemData.body,
                            strPost_summary: itemData.summary,
                            intPost_type: itemData.type,
                            strPost_featuredimage: itemData.image
                        }
                    }

                    this.remove = function () {
                        domItem.remove();
                    }
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.bindData = bindData;
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domItemsList);
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.init = function () {
                        domItem = domSearchRow.find('.search-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        domItem.find('select[name="series"]').select2({
                            // dropdownParent: domItem.find('.hover-content')
                        });
                        bindEvents();
                    }
                }

                this.setItems = function (itemsData, newKeyword) {
                    domItemsList.slick('unslick');
                    itemHandles.forEach(function (value) {
                        value.remove();
                    });
                    itemHandles = [];
                    domSearchRow.find('.found-value').html(itemsData.items.length);
                    itemsData.items.forEach(function (item, i) {
                        var itemHandle = new SearchItemClass(formatItemData(item));
                        itemHandle.init();
                        if (i < 15) {
                            itemHandle.bindData();
                        }
                        itemHandles.push(itemHandle);
                    });
                    switch (sortBy) {
                        case 'audio':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().type == 0;
                            });
                            break;
                        case 'video':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().type == 2;
                            });
                            break;
                        case 'text':
                            filteredHandles = itemHandles.filter(function (value) {
                                return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                            });
                            break;
                        default:
                            filteredHandles = itemHandles;
                            break;
                    }
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        centerMode: false,
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                            {
                                breakpoint: 769,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            },
                        ]
                    });
                }

                this.refreshItems = function () {
                    domItemsList.css('height', domItemsList.css('height'));
                    domItemsList.css('opacity', 0);
                    setTimeout(function () {
                        domItemsList.slick('unslick');
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        switch (sortBy) {
                            case 'audio':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 0;
                                });
                                break;
                            case 'video':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 2;
                                });
                                break;
                            case 'text':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                });
                                break;
                            default:
                                filteredHandles = itemHandles;
                                break;
                        }
                        filteredHandles.forEach(function (value) {
                            value.append();
                        });
                        domItemsList.css('height', 'auto');
                        domItemsList.slick({
                            dots: false,
                            infinite: false,
                            centerMode: false,
                            arrows: true,
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            nextArrow: domSearchRow.find('.arrow-right'),
                            prevArrow: domSearchRow.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 3,
                                    }
                                },
                                {
                                    breakpoint: 769,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                    }
                                },
                            ]
                        });
                        domItemsList.css('opacity', 1);
                    }, 300);
                }
                this.init = function () {
                    domSearchRow = domRoot.find('.search-row.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('.search-container'));
                    domItemsList = domSearchRow.find('.search-items-list');
                    bindData();
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        centerMode: false,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                            {
                                breakpoint: 769,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            },
                        ]
                    });
                    bindEvents();
                }
            }

            var bindEvents = function () {
                domHeaderSearchForm.submit(function () {
                    if (isActive) {
                        currentKeyword = domHeaderSearchForm.find('input').val();
                        self.applySearch(currentKeyword);
                        return false;
                    }
                });

                domFooterSearchForm.submit(function () {
                    if (isActive) {
                        currentKeyword = domFooterSearchForm.find('input').val();
                        self.applySearch(currentKeyword);
                        return false;
                    }
                });
            }
            this.fillCategories = function (cateData) {
                filterHandle.fillCategories(cateData);
            }
            this.applySearch = function (kwd) {
                currentKeyword = kwd;
                self.searchByKeyword(kwd).then(function (res) {
                    searchedData = res.data;
                    searchedData.keyword = kwd;
                    var isEmpty = !(searchedData.posts.items.length || searchedData.rssbPosts.items.length);

                    if (isSwitched) {
                        if (isEmpty) {
                            domRoot.addClass('no-content');
                        }
                        else {
                            domRoot.removeClass('no-content');
                            postsListHandle.setItems(res.data.posts, kwd);
                            rssbPostsListHandle.setItems(res.data.rssbPosts, kwd);
                        }
                        isSwitched = false;
                    }
                    else if (isSearchOpened) {
                        domSiteSearch.css('opacity', 0);
                        setTimeout(function () {
                            if (isEmpty) {
                                domRoot.addClass('no-content');
                            }
                            else {
                                domRoot.removeClass('no-content');
                                postsListHandle.setItems(res.data.posts, kwd);
                                rssbPostsListHandle.setItems(res.data.rssbPosts, kwd);
                            }
                            setTimeout(function () {
                                domSiteSearch.css('opacity', 1);
                            }, 0);
                        }, 300);
                    }
                    else {
                        activeSearch().then(function () {
                            if (isEmpty) {
                                domRoot.addClass('no-content');
                            }
                            else {
                                domRoot.removeClass('no-content');
                                postsListHandle.setItems(res.data.posts, kwd);
                                rssbPostsListHandle.setItems(res.data.rssbPosts, kwd);
                            }
                        })
                    }
                });
            }
            this.searchByKeyword = function (keyword) {
                var ajaxData = {
                    q: keyword,
                    size: 50,
                    items: [
                        // {name: 'youtube', token: false},
                        // {name: 'spreaker', token: false},
                        // {name: 'blogs', token: false},
                        // {name: 'recipes', token: false},
                        // {name: 'ideabox', token: false},
                        {name: 'posts', token: false},
                        {name: 'rssbPosts', token: false},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData);
            }
            this.setActive = function (v) {
                isActive = v;
            }
            this.init = function () {
                domRoot = domSiteSearch.find('.posts-search-content');
                domSearchHeader = domRoot.find('header.search-header');
                domSortByDropDown = domSearchHeader.find('.sort-by .dropdown');
                domHeaderSearchForm = $('.home-header form.search-for-inspiration');
                domFooterSearchForm = $('.home-footer form.search-for-inspiration');

                filterHandle = new FilterClass();
                filterHandle.init();

                ajaxAPiHandle.apiPost('Series.php', {}, false).then(function (res) {
                    res.data.forEach(function (sery) {
                        series[sery.series_ID] = sery;
                        var optHtml = '<option value="'+ sery.series_ID +'">'+ sery.strSeries_title +'</option>';
                        domSiteSearch.find('.search-container .search-item.sample select[name="series"]').append(optHtml);
                    });
                });
                postsListHandle = new SearchRowClass('posts');
                postsListHandle.init();

                rssbPostsListHandle = new SearchRowClass('rssb-posts');
                rssbPostsListHandle.init();

                bindEvents();
            }
        }
        this.afterMainActive = function (fn) {
            callbacksMainActive.push(fn);
        }
        this.fillCategories = function (cData) {
            seriesSearchHandle.fillCategories(cData);
            findNewContentHandle.fillCategories(cData);
            postsSearchHandle.fillCategories(cData);
        }
        this.init = function () {
            domSiteSearch = $('.site-wrapper .site-search');
            findNewContentHandle = new FindNewContentClass();
            findNewContentHandle.init();

            seriesSearchHandle = new SeriesSearchClass();
            seriesSearchHandle.init();

            postsSearchHandle = new PostsSearchClass();
            postsSearchHandle.init();

            bindEvents();
        }
    }

    var headerHandle = new HeaderClass();
    headerHandle.init();

    authHandle = new AuthClass();
    authHandle.init();

    siteSearchHandle = new SiteSearchClass();
    siteSearchHandle.init();

    stripeCardReady = ajaxAPiHandle.apiPost('Options.php', {name: 'stripeApi_pKey'}, false).then(function (res) {
        if (res.data != false) {
            stripeCardModalHandle = new StripeCardModalClass($('body .site-wrapper > .stripe-card-modal'), res.data);
            stripeCardModalHandle.init();
            return stripeCardModalHandle;
        }
        return false;
    });
})();