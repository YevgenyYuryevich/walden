(function () {
    'use strict'
    var pageHandle;
    var PageClass = function () {

        var domRoot;
        var domFilesList, domActionsContainer;

        var viewWidgetHandle;

        var key = 1;
        var fileRowHandles = [];

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent'
        };

        var postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }

        var postTypeArr = {
            'video': 2,
            'audio': 0,
            'image': 8,
            'text': 7,
        }

        var FileRowClass = function (file) {

            var domRow, domActions;
            var postData = {};

            var self = this;
            var keyI = key++;

            var bindData = function () {
                postData.title = file.name;
                postData.strType = helperHandle.extractTypeFromFileType(file.type);
                postData.type = postTypeArr[postData.strType];
                postData.fullType = file.type;
                var dType = file.type;

                if (file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    dType = 'application/docx';
                } else if (file.type === 'application/msword') {
                    dType = 'application/doc';
                } else if (file.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                    dType = 'application/ppt';
                }

                domRow.find('.file-title').attr('placeholder', postData.title);
                domRow.find('.file-type').html(dType);
                domRow.find('.file-thumb').addClass('data-type-' + postData.strType);

                switch (postData.strType) {
                    case 'video':
                        domActions.find('.preview').find('.glyphicon').addClass('glyphicon-facetime-video');
                        break;
                    case 'music':
                        domActions.find('.preview').find('.glyphicon').addClass('glyphicon-music');
                        break;
                    case 'image':
                        domActions.find('.preview').find('.glyphicon').addClass('glyphicon-picture');
                        break;
                    case 'application':
                    case 'text':
                        fileToText();
                        domActions.find('.preview').find('.glyphicon').addClass('glyphicon-font');
                        break;
                }
            }
            var fileToText = function () {
                var ajaxData = new FormData();
                ajaxData.append('file', file);
                return ajaxAPiHandle.multiPartApiPost('FileToHtml.php', ajaxData).then(function (res) {
                    console.log(res);
                    postData.body = res.data;
                    return res.data;
                });
            }
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    postData.imageFile = input.files[0];
                    if (helperHandle.extractTypeFromFileType(postData.imageFile.type) != 'image') {
                        swal("Not Image!", "Sorry, This is not image file, Please select image file.").then(function () {
                        });
                        return false;
                    }
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        domRow.find('img.file-image').attr('src', e.target.result);
                        domRow.find('.file-image-wrapper').css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domRow.find('.image-input').change(function () {
                    updateCoverImage(this);
                });
                domActions.find('.start').click(function () {
                    self.upload();
                });
                domRow.find('input.file-title').change(function () {
                    var title = $(this).val();
                    postData.title = title ? title : $(this).attr('placeholder');
                });
                domActions.find('.delete').click(function () {
                    self.delete();
                });
                domActions.find('.cancel').click(function () {
                    self.cancel();
                });
                domActions.find('.preview').click(function () {
                    self.preview();
                });
            }
            this.preview = function () {
                var wData = postData;
                if (!wData.body) {
                    wData.body = window.URL.createObjectURL(file);
                }
                viewWidgetHandle.view(wData);
            }
            this.key = function () {
                return keyI;
            }
            this.isUploaded = function () {
                return postData.id ? true : false;
            }
            this.delete = function () {
                if (Array.isArray(postData.id)) {
                    postData.id.forEach(function (id) {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: id}).then(function (res) {
                        });
                    });
                    self.cancel();
                }
                else {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: postData.id}).then(function (res) {
                        self.cancel();
                    });
                }
            }
            this.cancel = function () {
                fileRowHandles.forEach(function (hdl, i) {
                    if (hdl.key() === self.key()) {
                        fileRowHandles.splice(i, 1);
                        domRow.remove();
                        filterActionsAvailable();
                    }
                });
            }
            this.upload = function () {

                var resPromise;

                if (postData.strType === 'text' || postData.strType === 'application') {
                    resPromise = Promise.resolve();
                }
                else {
                    var ajaxData = new FormData();
                    ajaxData.append('file', file);
                    ajaxData.append('sets', JSON.stringify({where: 'assets/media/' + postData.strType, prefix: 'post'}));
                    resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                        postData.body = res.data.abs_url;
                        return true;
                    });
                }
                resPromise.then(function () {
                    if (Array.isArray(postData.body)) {
                        postData.id = [];
                        postData.body.forEach(function (body, i) {
                            var ajaxPost = new FormData();
                            ajaxPost.append('action', 'insert');
                            var postFData = postFormat($.extend({}, postData, {body: body, title: postData.title + '('+ (i + 1) +')'}));
                            postFData.intPost_series_ID = seriesId;
                            delete postFData.post_ID;
                            ajaxPost.append('sets', JSON.stringify(postFData));
                            if (postData.imageFile) {
                                ajaxPost.append('strPost_featuredimage', postData.imageFile);
                            }
                            ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxPost).then(function (res) {
                                postData.id.push(res.data.post_ID);
                                domRow.addClass('uploaded');
                                filterActionsAvailable();
                            });
                        });
                    }
                    else {
                        var ajaxPost = new FormData();
                        ajaxPost.append('action', 'insert');
                        var postFData = postFormat(postData);
                        postFData.intPost_series_ID = seriesId;
                        ajaxPost.append('sets', JSON.stringify(postFData));
                        if (postData.imageFile) {
                            ajaxPost.append('strPost_featuredimage', postData.imageFile);
                        }
                        ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxPost).then(function (res) {
                            postData.id = res.data.post_ID;
                            domRow.addClass('uploaded');
                            filterActionsAvailable();
                        });
                    }
                });
            }
            this.init = function () {
                domRow = domFilesList.find('li.sample.file-row').clone().removeAttr('hidden').removeClass('sample').appendTo(domFilesList);
                domActions = domRow.find('.actions');
                bindData();
                bindEvents();
            }
        }
        var filterActionsAvailable = function () {
            var cntToUpload = 0;
            fileRowHandles.forEach(function (hdl) {
                cntToUpload += hdl.isUploaded() ? 0 : 1;
            });
            if (cntToUpload) {
                domActionsContainer.find('.start').removeClass('disabled');
            }
            else {
                domActionsContainer.find('.start').addClass('disabled');
            }
            if (fileRowHandles.length) {
                domActionsContainer.find('.cancel').removeClass('disabled');
            }
            else {
                domActionsContainer.find('.cancel').addClass('disabled');
            }
        }
        var bindEvents = function () {
            domActionsContainer.find('.action.add-new-files input').change(function () {
                var files = $(this).prop('files');
                for (var i = 0; i < files.length; i ++) {
                    var hdl = new FileRowClass(files[i]);
                    hdl.init();
                    fileRowHandles.push(hdl);
                }
                $(this).val('');
                filterActionsAvailable();
            });
            domActionsContainer.find('.start').click(function () {
                if ($(this).hasClass('disabled')) {
                    return ;
                }
                fileRowHandles.forEach(function (hdl) {
                    hdl.upload();
                });
            });
            domActionsContainer.find('.cancel').click(function () {
                if ($(this).hasClass('disabled')) {
                    return ;
                }
                var len = fileRowHandles.length;
                for (var i = 0; i < len; i++) {
                    fileRowHandles[0].cancel();
                }
            });
            domRoot.find('.return-btn').click(function () {
                goTo_editSeries();
            });
        }
        var goTo_editSeries = function () {
            $('<form action="editseries" method="post" hidden><input name="id" value="'+ seriesId +'"></form>').appendTo('body').submit().remove();
        }
        var ViewWidgetClass = function () {
            var domViewWidget, domUsedBody, domHeader, domBodyWrp, domFooter, domTopCtrl;
            var postData, metaData;
            var audioCnt = 1, videoCnt = 1, afterGlowReady;

            var bindEvents = function () {
                domHeader.find('.mv_min_control_max, .post-title').parent().click(function () {
                    maximize();
                });
                domHeader.find('.mv_min_control_close').parent().click(function () {
                    close();
                });
                domTopCtrl.find('.mv_minimize_icon').parent().click(function () {
                    minimize();
                });
                domTopCtrl.find('.mv_close_icon').parent().click(function () {
                    close();
                });
                domViewWidget.click(function (e) {
                    if (e.target !== this) return;
                    close();
                });
            };

            var close = function () {
                destroyBody();
                domViewWidget.removeClass('active');
                $('body').css('padding-right', '0px');
                $('body').css('overflow', 'auto');
            }

            var destroyBody = function () {
                if (!domUsedBody || !domUsedBody.length) {
                    return false;
                }
                switch (parseInt(postData.type)) {
                    case 0:
                        domViewWidget.removeClass('audio-type');
                        break;
                    case 2:
                        domViewWidget.removeClass('video-type');
                        window.afterglow.players.forEach(function (p) {
                            window.afterglow.destroyPlayer(p.id);
                        });
                        break;
                    case 8:
                        domViewWidget.removeClass('image-type');
                        break;
                    default:
                        domViewWidget.removeClass('text-type');
                        break;
                }
                domUsedBody.remove();
                return true;
            }

            var maximize = function () {
                domViewWidget.addClass('maximize-view');
                $('body').css('padding-right', window.innerWidth - $("body").prop("clientWidth") + 'px');
                $('body').css('overflow', 'hidden');
            }

            var minimize = function () {
                $('body').css('overflow', 'auto');
                $('body').css('padding-right', '0px');
                domViewWidget.removeClass('maximize-view');
            }

            this.view = function (newPostData) {
                destroyBody();
                domViewWidget.addClass('active');
                postData = newPostData;
                domHeader.find('.post-title').html(postData.title);
                domFooter.find('.post-title').html(postData.title);
                if (domViewWidget.hasClass('maximize-view')){
                    maximize();
                }
                switch (parseInt(postData.type)) {
                    case 0:
                        domViewWidget.addClass('audio-type');
                        domUsedBody = domViewWidget.find('.audio-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                        domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                        domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                        domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);

                        var settings_ap = {
                            disable_volume: 'off',
                            disable_scrub: 'default',
                            design_skin: 'skin-wave',
                            skinwave_dynamicwaves: 'on',
                            skinwave_mode: 'alternate',
                        };
                        dzsag_init('#audio-' + audioCnt, {
                            'transition':'fade',
                            'autoplay' : 'on',
                            'settings_ap': settings_ap
                        });
                        audioCnt++;
                        break;
                    case 2:
                        domViewWidget.addClass('video-type');
                        domUsedBody = domViewWidget.find('.video-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                        domUsedBody.find('video').addClass('afterglow');
                        // domUsedBody.find('video').attr('data-youtube-id', postData.body);
                        metaData.videoId = 'video_' + videoCnt;
                        domUsedBody.find('video').attr('id', metaData.videoId);
                        domUsedBody.find('source').attr('type', postData.fullType).attr('src', postData.body);
                        domUsedBody.find('source').onLoad = function () {
                            window.URL.revokeObjectURL(this.src);
                        }
                        if (window.afterglow && !window.afterglow.getPlayer(metaData.videoId)){
                            window.afterglow.init();
                            afterGlowReady = new Promise(function (resolve) {
                                var resolved = false;
                                window.afterglow.on(metaData.videoId, 'ready', function () {
                                    setTimeout(function () {
                                        resolved = true;
                                        resolve();
                                    }, 500);
                                });
                                setTimeout(function () {
                                    if (resolved) {
                                        return true;
                                    }
                                    resolve();
                                }, 3000);
                            });
                        }
                        videoCnt++;
                        break;
                    case 8:
                        domViewWidget.addClass('image-type');
                        domUsedBody = domViewWidget.find('.image-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                        domUsedBody.append(postData.body);
                        break;
                    case 10:
                        domViewWidget.addClass('form-type');
                        domUsedBody = domViewWidget.find('.form-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                        domUsedBody.append(postData.body);
                        domUsedBody.find('[data-form-field]').each(function (i, d) {
                            var hdl = new FormFieldClass($(d));
                            hdl.init();
                        });
                        break;
                    default:
                        domViewWidget.addClass('text-type');
                        domUsedBody = domViewWidget.find('.text-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                        if (Array.isArray(postData.body)) {
                            domUsedBody.append(postData.body.join('<br><br>'));
                        }
                        else {
                            domUsedBody.append(postData.body);
                        }
                        break;
                }
            }
            this.init = function () {
                domViewWidget = $('.view-widget');
                domHeader = domViewWidget.find('header.widget-header');
                domBodyWrp = domViewWidget.find('.widget-body');
                domFooter = domViewWidget.find('footer.widget-footer');
                domTopCtrl = domViewWidget.find('aside.widget-top-control');
                metaData = {};
                afterGlowReady = Promise.resolve();
                bindEvents();
            }
        }
        this.init = function () {
            domRoot = $('main.main-content');
            domFilesList = domRoot.find('ul.files-list');
            domActionsContainer = domRoot.find('.actions-container');
            filterActionsAvailable();
            viewWidgetHandle = new ViewWidgetClass();
            viewWidgetHandle.init();

            bindEvents();
        }
    }

    pageHandle = new PageClass();
    pageHandle.init();
})();