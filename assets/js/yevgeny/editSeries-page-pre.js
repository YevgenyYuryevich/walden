(function(){
    'use strict';

    var is_loading_controlled_in_local = false;
    var accept_doc_types = ['application/pdf','text/plain'];
    var accept_image_type = ['image/png', 'image/jpeg'];
    var accept_audio_types = ['audio/mp3'];
    var accept_video_types = ['video/mp4', 'video/avi'];

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    var EditSeriesClass = function () {

        var mainHandle, addPostsHandle;
        var isAdvancedUpload;

        var MainClass = function () {

            var domRoot = $('#main-tab');
            var domAddLinks = domRoot.find('.add-to-links-section');
            var domPostsList = $('.posts-container .posts-list');

            var series = initialSeries;
            var purchase = initialPurchase;
            var availableDays = purchase.available_days;

            var postRowHandles = [], virtualRootHandle, virtualRootData;
            var self = this, editStripeHandle;
            var isFirstAddPostOpen = true;
            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent',
                free: 'boolPost_free',
            };

            var subscriptionFiels = {
                id: 'clientsubscription_ID',
                title: 'strClientSubscription_title',
                body: 'strClientSubscription_body',
                image: 'strClientSubscription_image',
                type: 'intClientSubscriptions_type',
                order: 'intClientSubscriptions_order',
                nodeType: 'strClientSubscription_nodeType',
                parent: 'intClientSubscriptions_parent'
            };

            var viewFormat = function (data) {
                var fData = {};
                var fk;

                for (var k in data) {
                    if (isSeriesMine) {
                        fk = helperHandle.keyOfVal(postFields, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                    else {
                        fk = helperHandle.keyOfVal(subscriptionFiels, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                }
                fData.origin = data;
                return fData;
            }

            var postFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (postFields[k]) {
                        fData[postFields[k]] = data[k];
                    }
                }
                return fData;
            }

            var subscriptionFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (subscriptionFiels[k]) {
                        fData[subscriptionFiels[k]] = data[k];
                    }
                }
                return fData;
            }

            var originFormat = function (data) {
                if (isSeriesMine) {
                    return postFormat(data);
                }
                return subscriptionFormat(data);
            }

            var EditStripeClass = function (modalRootHandle, domContent) {
                var domForm;
                var bindEvents = function () {
                    domForm.submit(function () {
                        update().then(function () {
                            modalRootHandle.cancelClose().then(function () {
                                setTimeout(function () {
                                    domRoot.find('[name="boolSeries_charge"]').prop('checked', true).change();
                                }, 300);
                            });

                        });
                        return false;
                    })
                }
                var update = function () {
                    var sets = helperHandle.formSerialize(domForm);
                    return ajaxAPiHandle.apiPost('User.php', {action: 'update', sets: sets}).then(function (res) {
                        if (res.status) {
                            isStripeAvailable = true;
                        }
                        return true;
                    });
                }
                this.init = function () {
                    domForm = domContent.find('form');
                    bindEvents();
                }
            }

            var PostRowClass = function (postData) {

                var domRow, domChildList, domInnerWrp;
                var self = this, parentHandle = false;

                var isChildsLoaded = false;

                var childHandles = [];
                var editModalHandle = null;

                var metaData = {};

                var openEditModal = function () {
                    var edited = false;
                    var resP;
                    if (typeof postData.body !== 'undefined') {
                        resP = Promise.resolve(postData.body);
                    }
                    else {
                        if (isSeriesMine) {
                            resP = ajaxAPiHandle.apiPost('Posts.php', {action: 'get', where: self.getId()}).then(function (res) {
                                postData.body = res.data.strPost_body;
                                return postData.body;
                            });
                        }
                        else {
                            resP = ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'get', where: self.getId()}).then(function (res) {
                                postData.body = res.data.strClientSubscription_body;
                                return postData.body;
                            });
                        }
                    }
                    resP.then(function () {
                        postData.series = initialSeries;
                        editModalHandle = new EditPostModalClass(domRoot.find('.modal-edit-entry-wrapper .modal-edit-post').clone(), postData);
                        editModalHandle.init();
                        editModalHandle.afterUpdate(function (sets) {
                            edited = true;
                            bindData();
                        });
                        editModalHandle.modalHandle().onClosed(function () {
                            if (edited) {
                                self.focus();
                            }
                        });
                        editModalHandle.modalHandle().open();
                    });
                }
                var bindEvents = function () {
                    domInnerWrp.find('.delete-post img').click(function () {
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover this!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                self.deletePost();
                            }
                        });
                    });
                    domInnerWrp.find('.clone-post img').click(function () {
                        self.clonePost();
                    });
                    domInnerWrp.find('.edit-post').click(function () {
                        openEditModal();
                    });
                    domInnerWrp.find('.view-post').click(function () {
                        openView();
                    });
                    domInnerWrp.find('.comeback-post').click(function () {
                        bindData();
                        makeNormalStatus();
                    });
                    domInnerWrp.find('.publish-post .inactive-icon').click(function () {
                        var sets = {
                            status: 'publish',
                        };
                        self.update(sets).then(function () {
                            makeNormalStatus();
                        });
                    });
                    domInnerWrp.find('.publish-post .active-icon').click(function () {
                        var sets = {
                            status: 'draft',
                        };
                        self.update(sets).then(function () {
                            makeNormalStatus();
                        });
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_delete', where: {intClientSubscription_post_ID: self.getId()}});
                    });
                    domInnerWrp.find('.save-post').click(function () {
                        var sets = {
                            title: domInnerWrp.find('.post-title').html(),
                        };
                        self.update(sets).then(function () {
                            makeNormalStatus();
                        });
                    });
                    domInnerWrp.find('.jstree-icon.jstree-ocl').click(function () {
                        if (domRow.hasClass('jstree-open') && childHandles.length && self.nodeType() !== 'post') {
                            self.collapseChild();
                        }
                        else {
                            self.expandChild();
                        }
                    });
                    domInnerWrp.find('.post-title').keypress(function (e) {
                        var key = e.which;
                        if(key == 13)  // the enter key code
                        {
                            e.preventDefault();
                            domInnerWrp.find('.save-post').click();
                        }
                    });
                    if (isSeriesMine) {
                        domInnerWrp.find('[name="boolPost_free"]').change(function () {
                            var v = $(this).prop('checked') ? 1 : 0;
                            self.update({free: v});
                        });
                    }
                }
                var openView = function () {
                    var from = isSeriesMine ? 'post' : 'subscription';
                    var cloneForm = $('<form action="view" method="get" target="_blank" hidden><input name="id" value="'+ postData.id +'"><input name="from" value="'+ from +'" /><input name="prevpage" value="editseries"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }
                var loadChilds = function () {
                    childHandles = [];
                    domRow.addClass('jstree-loading');
                    is_loading_controlled_in_local = true;
                    domChildList.empty();

                    var resPromise, ajaxData;
                    if (isSeriesMine) {
                        ajaxData = {
                            action: 'get_items',
                            where: {intPost_series_ID: initialSeries.series_ID, intPost_parent: postData.id},
                            select: helperHandle.array_keys(postFormat(postData)),
                        };
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData, false);
                    }
                    else {
                        var select = helperHandle.array_keys(subscriptionFormat(postData));
                        select = select.concat('intClientSubscription_post_ID');

                        ajaxData = {
                            action: 'get_items',
                            where: {intClientSubscription_purchased_ID: initialPurchase.purchased_ID, intClientSubscriptions_parent: postData.id},
                            select: select,
                        };
                        if (postData.origin.intClientSubscription_post_ID !== null) {
                            ajaxData.meta = {
                                pre_action: 'subscribe',
                                where: {
                                    intPost_series_ID: initialSeries.series_ID,
                                    intPost_parent: postData.origin.intClientSubscription_post_ID
                                },
                                intClientSubscription_purchased_ID: initialPurchase.purchased_ID
                            };
                        }
                        resPromise = ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData, false);
                    }
                    return resPromise.then(function (res) {

                        var childPosts = res.data;
                        var promises = [];

                        childPosts.forEach(function (childPost) {
                            var fData = viewFormat(childPost);
                            promises.push(self.addChild(fData));
                        });
                        isChildsLoaded = true;
                        domRow.removeClass('jstree-loading');
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        is_loading_controlled_in_local = false;

                        if (!childHandles.length) {
                            domRow.addClass('jstree-leaf');
                        }
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve(true);
                            }, 200);
                        });
                    });
                }

                var bindData = function () {
                    domInnerWrp.find('.post-title').html(postData.title);

                    domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-' + postData.nodeType);
                    domRow.removeClass('jstree-leaf');
                    domRow.removeClass('mjs-nestedSortable-leaf');
                    domRow.removeClass('mjs-nestedSortable-branch');
                    domRow.removeClass('node-status-publish node-status-draft');

                    if (postData.nodeType == 'post') {
                        isChildsLoaded = true;
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-leaf');
                        self.expandChild();
                    }
                    else if (isChildsLoaded && !childHandles.length) {
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    else {
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    if (isSeriesMine) {
                        domRow.addClass('node-status-' + postData.status);
                        domInnerWrp.find('[name="boolPost_free"]').prop('checked', parseInt(postData.free));
                    }
                }

                var makeEditStatus = function () {
                    domRow.addClass('edit-status');
                    domInnerWrp.find('.post-title').attr('contenteditable', true);
                }

                var makeNormalStatus = function () {
                    domRow.removeClass('edit-status');
                    domInnerWrp.find('.post-title').removeAttr('contenteditable');
                }

                this.getPath = function () {
                    if (!parentHandle) {
                        return [self.getId()];
                    }
                    var parentPath = parentHandle.getPath();
                    return parentPath.push(self.getId());
                }

                this.expandChild = function () {
                    if (!isChildsLoaded) {
                        loadChilds().then(function (res) {
                            domRow.removeClass('jstree-closed').addClass('jstree-open');
                            domRow.find('> ul').collapse('show');
                        });
                    }
                    else {
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        domRow.find('> ul').collapse('show');
                    }
                }

                this.collapseChild = function () {
                    domRow.removeClass('jstree-open');
                    domRow.find('> ul').collapse('hide');
                    domRow.find('> ul').off('hidden.bs.collapse').one('hidden.bs.collapse', function (e) {
                        domRow.addClass('jstree-closed');
                    });
                }

                this.childLoaded = function () {
                    return isChildsLoaded;
                }
                this.reOrderChilds = function () {
                    var updateSets = [];
                    childHandles.forEach(function (childHdl) {
                        var newOrder = childHdl.getDomPosition() + 1;
                        updateSets.push({where: childHdl.getId(), sets: originFormat({order: newOrder})});
                        childHdl.setOrder(newOrder);
                    });
                    if (isSeriesMine) {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'update_posts', updateSets: updateSets});
                    }
                    else {
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update_items', updateSets: updateSets});
                    }
                }
                this.spliceChild = function (id) {
                    childHandles.forEach(function (childHandle, i) {
                        if (childHandle.getId() === id) {
                            childHandles.splice(i, 1);
                        }
                    });
                    if (isChildsLoaded && !childHandles.length) {
                        domRow.removeClass('jstree-open').addClass('jstree-leaf');
                    }
                }
                this.addChildHandle = function (childHandle) {
                    childHandles.push(childHandle);
                    domRow.removeClass('jstree-leaf');
                }
                this.addChild = function (childPost) {
                    var hdl = new PostRowClass(childPost);
                    hdl.init();
                    hdl.setParent(self);
                    return hdl.appendTo(domChildList);
                }
                this.setParent = function (parentHdl) {
                    parentHandle = parentHdl;
                    parentHandle.addChildHandle(self);
                }
                this.updateParent = function (parentHdl) {
                    parentHandle.spliceChild(self.getId());
                    var parentType = parentHdl ? parentHdl.nodeType() : 'path';
                    var myNodeType = self.nodeType(), newNodeType = myNodeType;

                    parentHandle = parentHdl;
                    parentHandle.addChildHandle(self);
                    self.makeTypeValid();

                    var sets = {};
                    sets.parent = parentHdl.getId();

                    sets.nodeType = generateValidNodeType(parentType, myNodeType);

                    return self.update(sets).then(function (res) {
                        return res;
                    });
                }
                this.nodeType = function () {
                    return postData.nodeType;
                }
                this.makeTypeValid = function () {
                    var parentType = parentHandle ? parentHandle.nodeType() : 'path';
                    var myNodeType = self.nodeType(), newNodeType = myNodeType;
                    postData.strPost_nodeType = generateValidNodeType(parentType, myNodeType);
                    bindData();
                    childHandles.forEach(function (childHandle) {
                        childHandle.makeTypeValid();
                    });
                }
                this.update = function (sets) {

                    var resPromise;

                    if (isSeriesMine) {
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', {action: 'update', sets: originFormat(sets), where: self.getId()});
                    }
                    else {
                        resPromise = ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', sets: originFormat(sets), where: self.getId()});
                    }
                    return resPromise.then(function (res) {
                        $.extend(postData, sets);
                        if (sets.body) {
                            metaData.body = sets.body;
                        }
                        bindData();
                        return res;
                    });
                }
                this.clonePost = function () {
                    var resPromiss;
                    if (isSeriesMine){
                        resPromiss = ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'clone_post', where: self.getId()});
                    }
                    else {
                        resPromiss = ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'clone_subscription', where: self.getId()});
                    }
                    return resPromiss.then(function (res) {
                        if (res.status){
                            var newHandle = new PostRowClass(viewFormat(res.data));
                            newHandle.init();
                            newHandle.setParent(parentHandle);
                            newHandle.appearAfter(domRow);
                            return res;
                        }
                    })
                }
                this.setOrder = function (newOrder) {
                    postData.order = newOrder;
                }
                this.getOrder = function () {
                    return postData.order;
                }
                this.getDomPosition = function () {
                    return domRow.index();
                }
                this.getId = function () {
                    return postData.id;
                }
                this.deletePost = function () {
                    var resPromise = ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'delete_post', where: self.getId(), meta: initialSeries.series_ID});

                    resPromise.then(function () {
                        self.disappear().then(function () {
                            domRow.remove();
                            parentHandle.spliceChild(self.getId());
                        });
                    });
                }
                this.removeDom = function () {
                    domRow.remove();
                }
                this.focus = function () {
                    domRow.addClass('focus');
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('focus');
                            resolve();
                        }, 700);
                    });
                }
                this.appearAfter = function (domAfter) {
                    var newDom;
                    if (domAfter){
                        setTimeout(function () {
                            domRow.addClass('appear');
                            newDom = domRow.insertAfter(domAfter);
                        }, 200);
                    }
                    else {
                        setTimeout(function () {
                            domRow.addClass('appear');
                            newDom = domRow.appendTo(domPostsList);
                        }, 200)
                    }
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('appear');
                            resolve();
                        }, 700);
                    });
                }
                this.getDom = function () {
                    return domRow;
                }
                this.appendTo = function (domList) {
                    if (!domList) {
                        if (parentHandle) {
                            domList = parentHandle.getDom().find('> ul');
                        }
                        else {
                            domList = domPostsList;
                        }

                    }
                    setTimeout(function () {
                        domRow.addClass('appear');
                        domRow.appendTo(domList);
                    }, 200);
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('appear');
                            resolve();
                        }, 700);
                    });
                }
                this.disappear = function () {
                    setTimeout(function () {
                        domRow.addClass('disappear');
                    }, 200)
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('disappear');
                            domRow.detach();
                            resolve();
                        }, 700)
                    });
                }
                this.init = function () {
                    domRow = domRoot.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                    domInnerWrp = domRow.find('> .post-row-inner');
                    bindData();
                    domRow.attr('id', 'sortable-tree-item-' + self.getId());
                    if (self.nodeType() !== 'post') {
                        domRow.append('<ul class="collapse" style="height: 0px;"></ul>');
                        domChildList = domRow.find('> ul');
                    }
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }

            var generateValidNodeType = function(parentNodeType, myNodeType) {
                var newNodeType = myNodeType;

                switch (parentNodeType) {
                    case 'post':
                        break;
                    case 'menu':
                        newNodeType = 'path';
                        break;
                    case 'path':
                        if (myNodeType === 'path') {
                            newNodeType = 'menu';
                        }
                        break;
                }
                return newNodeType;
            }

            var toggleAvailableDay = function (dayName, domTarget) {
                var dayNames = ['mon', 'tus', 'wed', 'thi', 'fri', 'sat', 'sun'];
                var dayKey = dayNames.indexOf(dayName);

                availableDays = availableDays.replaceAt(dayKey, availableDays[dayKey] == '1' ? '0' : '1');

                ajaxAPiHandle.post(ACTION_URL, {action: 'set_available_days', seriesId: series.series_ID, purchasedId: purchase.purchased_ID, days: availableDays}).then(function (res) {
                    if (res.status == true){
                        domTarget.toggleClass('active-day');
                    }
                    else {
                        availableDays = availableDays.replaceAt(dayKey, availableDays[dayKey] == '1' ? '0' : '1');
                    }
                })
            }
            var updateCoverImage = function (input) {
                if (input.files && input.files[0]) {
                    var sendData = new FormData();
                    sendData.append('action', 'update_cover_image');
                    sendData.append('seriesId', series.series_ID);
                    sendData.append('src', input.files[0]);

                    ajaxAPiHandle.multiPartPost(ACTION_URL, sendData).then(function (res) {
                        if (res.status){
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    });
                }
            }
            var bindEvents = function () {
                domRoot.find('.available-days-list > li > .icon-wrapper').click(function () {
                    toggleAvailableDay($(this).data('day'), $(this).parent());
                });
                domPostsList.on('sortupdate', function (e, helper) {
                    var currentHdl = helper.item.data('controlHandle');
                    var parentHdl = helper.item.parents('li').data('controlHandle');
                    parentHdl = parentHdl ? parentHdl : virtualRootHandle;
                    currentHdl.updateParent(parentHdl);
                    parentHdl.reOrderChilds();
                });
                domPostsList.on('sortexpand', function (e, helper) {
                    var domT = domPostsList.find('li.mjs-nestedSortable-hovering');
                    var hdl = domT.data('controlHandle');
                    hdl.expandChild();
                });
                domRoot.find('.open-add-items').click(function () {
                    openCreateSeriesContent({});
                });
                domRoot.find('.add-from-search').click(function () {
                    if (isFirstAddPostOpen) {
                        addPostsHandle.setSearchTerm(initialSeries.strSeries_title).then(function () {
                            $('.nav.nav-tabs li:last-child a').tab('show');
                            isFirstAddPostOpen = false;
                        });
                    }
                    else {
                        $('.nav.nav-tabs li:last-child a').tab('show');
                    }
                });
                domRoot.find('.cover-image-wrapper [name="cover_img"]').change(function () {
                    updateCoverImage(this);
                });
                domAddLinks.find('[data-node-type]').click(function () {
                    var sets = {};
                    sets.nodeType = $(this).attr('data-node-type');
                    if (sets.nodeType == 'post') {
                        sets.type = $(this).attr('data-post-type');
                    }
                    openCreateSeriesContent(sets);
                });
                domAddLinks.find('[data-tab]').click(function () {
                    var sets = {};
                    sets.tab = $(this).attr('data-tab');
                    openCreateSeriesContent(sets);
                });
                domRoot.find('[name="boolSeries_charge"]').change(function () {
                    if (isStripeAvailable) {
                        var checked = $(this).prop('checked');
                        var v = checked ? 1 : 0;
                        ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: series.series_ID, sets: {boolSeries_charge: v}}).then(function () {
                            if (checked) {
                                domRoot.find('.series-charge-wrapper').addClass('charged');
                                domRoot.find('.charge-price').removeAttr('disabled');
                                domRoot.find('.posts-container.jstree').addClass('charged');
                                series.boolSeries_charge = 1;
                            }
                            else {
                                domRoot.find('.series-charge-wrapper').removeClass('charged');
                                domRoot.find('.charge-price').attr('disabled', true);
                                domRoot.find('.posts-container.jstree').removeClass('charged');
                                series.boolSeries_charge = 0;
                            }
                        });
                    }
                    else {
                        if ($(this).prop('checked')) {
                            $(this).prop('checked', false).change();
                        }
                        else {
                            editStripeHandle.open();
                        }
                    }
                });
                domRoot.find('[name="intSeries_price"]').change(function () {
                    var v = $(this).val();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: series.series_ID, sets: {intSeries_price: v}});
                });
                domRoot.find('[name="strSeries_description"]').change(function () {
                    var v = $(this).val();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'update', sets: {strSeries_description: v}, where: series.series_ID});
                });
            }

            var openCreateSeriesContent = function (sets) {
                var vForm = $('<form action="createseriescontent" method="post" target="_blank"></form>').hide().appendTo('body');
                vForm.append('<input name="id" value="'+ initialSeries.series_ID +'">');
                var inputs = '';
                for (var k in sets) {
                    inputs += '<input name="'+ k +'" value="' + sets[k] +'">';
                }
                vForm.append($(inputs));
                vForm.submit().remove();
            }

            this.addPostRow = function (post) {
                var postRowHandle = new PostRowClass(viewFormat(post));
                postRowHandle.init();
                postRowHandle.setParent(virtualRootHandle);
                postRowHandle.appendTo();
                postRowHandles.push(postRowHandle);
                return postRowHandle;
            }

            this.deleteInFront = function (id) {
                postRowHandles.forEach(function (hdl, i) {
                    if (hdl.getId() == id) {
                        hdl.removeDom();
                        postRowHandles.splice(i, 1);
                    }
                });
            }

            this.makeSortable = function () {

                var isAllowed = function(placeholder, placeholderParent, currentItem) {
                    var currentHandle = currentItem.data('controlHandle');
                    var parentHandle = placeholderParent ? placeholderParent.data('controlHandle') : false;

                    if (isSeriesMine) {
                        switch (currentHandle.nodeType()) {
                            case 'post':
                                if (parentHandle) {
                                    return parentHandle.nodeType() !== 'post';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'menu':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'path' || parentHandle.nodeType() === 'root';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'path':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'menu';
                                }
                                else {
                                    return false;
                                }
                                break;
                        }
                        return true;
                    }
                    else {
                        switch (currentHandle.nodeType()) {
                            case 'post':
                                if (parentHandle) {
                                    return parentHandle.nodeType() !== 'post';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'menu':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'path' || parentHandle.nodeType() === 'root';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'path':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'menu';
                                }
                                else {
                                    return false;
                                }
                                break;
                        }
                        return true;
                    }
                }
                var options = {
                    appendTo: domRoot.find('.posts-container'),
                    placeholder: 'ui-state-highlight',
                    // containment: '.page.site-wrapper',
                    items: 'li',
                    protectRoot: true,
                    forcePlaceholderSize: true,
                    handle: '.drag-icon',
                    helper:	'clone',
                    opacity: .6,
                    revert: 250,
                    tabSize: 25,
                    tolerance: 'pointer',
                    toleranceElement: '> .post-row-inner',
                    maxLevels: 0,
                    isTree: true,
                    expandOnHover: 1000,
                    doNotClear: true,
                    startCollapsed: true,
                    listType: 'ul',
                    expandedClass: 'jstree-open',
                    collapsedClass: 'jstree-closed',
                    isAllowed: isAllowed,
                };
                if (!isSeriesMine) {
                    options.disableParentChange = true;
                }
                domPostsList.nestedSortable(options);
            }

            this.init = function () {
                virtualRootData = {
                    id: 0,
                    title: '',
                    image: '',
                    type: 7,
                    order: 1,
                    nodeType: 'root',
                    parent: -1,
                    status: 'publish',
                    free: 0,
                    origin: {
                        intClientSubscription_post_ID: 0,
                    }
                };

                virtualRootHandle = new PostRowClass(virtualRootData);
                virtualRootHandle.init();
                virtualRootHandle.appendTo();

                // posts.forEach(function (post) {
                //     self.addPostRow(post);
                // });
                self.makeSortable();

                editStripeHandle = new SiteLightboxClass({
                    domLightboxMainContent: $('.modal-edit-stripe').clone().removeAttr('hidden'),
                    mainContentHdlClass: EditStripeClass,
                    openDuration: 300,
                    closeDuration: 400,
                    lightboxType: 'cssMain',
                    openedCss: {
                        width: '1000px'
                    }
                });
                editStripeHandle.init();

                bindEvents();
            }
        }

        var AddPostsClass = function () {

            var domRoot = $('#add-items-tab');
            var domBlocksContainer, domMetaBlocksContainer;
            var domSearchForm = domRoot.find('.search-term-wrapper');
            var domSearch = domSearchForm.find('#search-term2');

            var urlListHandle = null;
            var youtubeListHandle = null;
            var podcastsListHandle = null;
            var blogsListHandle = null;
            var recipesListHandle = null;
            var ideaboxListHandle = null;
            var postsListHandle = null;
            var rssbPostsListHandle = null;
            var self = this;

            var postFormat = function (data) {
                return {
                    intPost_series_ID: initialSeries.series_ID,
                    strPost_title: data.title,
                    strPost_body: data.body,
                    strPost_summary: data.summary,
                    strPost_featuredimage: data.image,
                    intPost_type: data.type,
                };
            }
            var subscriptionFormat = function (data) {
                return {
                    intClientSubscription_purchased_ID: initialPurchase.purchased_ID,
                    strClientSubscription_title: data.title,
                    strClientSubscription_body: data.body,
                    strClientSubscription_image: data.image,
                    intClientSubscriptions_type: data.type,
                };
            }

            var searchPostsByKeyword = function (keyword, pageTokenY, pageTokenS, pageTokenT, pageTokenF, pageTokenI, pageTokenP, pageTokenRssbP) {
                var ajaxData = {
                    q: keyword,
                    size: 10,
                    items: [
                        {name: 'youtube', token: pageTokenY},
                        {name: 'spreaker', token: pageTokenS},
                        {name: 'blogs', token: pageTokenT},
                        {name: 'recipes', token: pageTokenF},
                        {name: 'ideabox', token: pageTokenI},
                        {name: 'posts', token: pageTokenP},
                        {name: 'rssbPosts', token: pageTokenRssbP},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData);
            }

            var bindEvents = function () {
                domSearchForm.submit(function () {
                    searchPostsByKeyword(domSearch.val(), false, false, false, false, false, false, false).then(function (res) {
                        youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                        podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                        blogsListHandle.setItems(res.data.blogs, domSearch.val());
                        recipesListHandle.setItems(res.data.recipes, domSearch.val());
                        ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                        postsListHandle.setItems(res.data.posts, domSearch.val());
                        rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                    });
                    return false;
                });
                domSearchForm.find('.clear-search-term').click(function () {
                    searchPostsByKeyword('', false, false, false, false, false, false, false).then(function (res) {
                        youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                        podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                        blogsListHandle.setItems(res.data.blogs, domSearch.val());
                        recipesListHandle.setItems(res.data.recipes, domSearch.val());
                        ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                        postsListHandle.setItems(res.data.posts, domSearch.val());
                        rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                    });
                })
                domRoot.find('.show-more').click(function () {
                    self.showMore();
                });
                domRoot.find('[name="from"]').change(function () {
                    var val = $(this).val();
                    var checked = $(this).prop('checked');
                    var handle = null;
                    switch (val){
                        case 'youtube':
                            handle = youtubeListHandle;
                            break;
                        case 'spreaker':
                            handle = podcastsListHandle;
                            break;
                        case 'twingly':
                            handle = blogsListHandle;
                            break;
                        case 'recipes':
                            handle = recipesListHandle;
                            break;
                        case 'ideabox':
                            handle = ideaboxListHandle;
                            break;
                        case 'posts':
                            handle = postsListHandle;
                            break;
                        case 'rssbposts':
                            handle = rssbPostsListHandle;
                            break;
                        case 'url':
                            handle = urlListHandle;
                            break;
                        default:
                            handle = youtubeListHandle;
                            break;
                    }
                    checked ? handle.show() : handle.hide();
                });
                domRoot.find('.i-am-done').click(function () {
                    self.IAmDone();
                });
                domRoot.find('.back-to-main').click(function () {
                    self.IAmDone();
                });
                domRoot.find('.results-from').find('.dropdown-menu').find('li').click(function () {
                    var checkbox = $(this).find('input');
                    checkbox.prop('checked', !checkbox.prop('checked')).change();
                    return false;
                });
                domRoot.find('main.main-content').dndhover().on('dndHoverStart', function (event) {
                    domRoot.find('[name="from"][value="url"]').prop('checked', true).change();;
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                });

                domRoot.find('.add-new-posts').click(function () {
                    goTo_addNewPosts();
                });
            }

            var MetaPostsBlockClass = function (from) {
                var domResultsRoot, domBlockTitle, domNewAdder;

                var self = this;
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];
                var grid, items, isAnimating;
                var maxId = 0;
                var isDragOnchild = false;

                var ItemClass = function (itemData) {

                    var domItemRoot = null, domTitleWrp, domImgWrp, domBodyWrp;
                    var self = this;
                    var addedPost = false, addedSubscription = false;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').parent().click(function () {
                            itemData.selected ? self.unSelectPost() : self.selectPost();
                        });

                        domImgWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                        })
                            .on('dragover dragenter', function(e) {

                                domResultsRoot.addClass('is-dragover');
                                domImgWrp.addClass('is-dragover');
                                isDragOnchild = true;
                            })
                            .on('dragleave dragend drop', function() {
                                domImgWrp.removeClass('is-dragover');
                                isDragOnchild = false;
                            })
                            .on('drop', function(e) {
                                domResultsRoot.removeClass('is-dragover');
                                var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                                if (dataInfo.type === 'image'){
                                    var droppedFile = dataInfo.file;
                                    uploadImageFile(droppedFile);
                                    return true;
                                }
                                else if (dataInfo.type === 'url' && dataInfo.contentType === 'image') {
                                    itemData.image = dataInfo.url;
                                    domImgWrp.addClass('is-uploading');
                                    bindData();
                                    update();
                                    return true;
                                }
                                alert('Sorry, This is not Image file');
                                // self.updateImage(droppedFile);
                            });
                        domImgWrp.find('img').on('load', function () {
                            domImgWrp.removeClass('is-uploading');
                        });
                        domImgWrp.find('input[name="item_image"]').change(function () {
                            if ($(this).prop('files').length){
                                var file = $(this).prop('files')[0];
                                if (checkFile(file) === 'image') {
                                    uploadImageFile(file);
                                }
                                else {
                                    alert('sorry! this is not image file');
                                }
                            }
                        });
                        domTitleWrp.find('.edit-entry').click(function () {
                            domTitleWrp.find('textarea').val(itemData.title);
                            domTitleWrp.addClass('edit-status');
                            domItemRoot.parent().addClass('editing');
                        });

                        domTitleWrp.find('.save-entry').click(function () {
                            itemData.title = domTitleWrp.find('textarea').val();
                            itemData.editStatus = 'creating';
                            domTitleWrp.removeClass('edit-status');
                            bindData();
                            update();
                            if (!domBodyWrp.hasClass('edit-status')) {
                                domItemRoot.parent().removeClass('editing');
                            }
                        });

                        domTitleWrp.find('.back-to-origin').click(function () {
                            domTitleWrp.removeClass('edit-status');
                            bindData();
                            if (!domBodyWrp.hasClass('edit-status')) {
                                domItemRoot.parent().removeClass('editing');
                            }
                        });

                        domBodyWrp.find('.edit-entry').click(function () {
                            domBodyWrp.find('textarea').val(itemData.body);
                            domBodyWrp.addClass('edit-status');
                            domItemRoot.parent().addClass('editing');
                        });

                        domBodyWrp.find('.save-entry').click(function () {
                            itemData.body = domBodyWrp.find('textarea').val();
                            itemData.editStatus = 'creating';
                            domBodyWrp.removeClass('edit-status');
                            bindData();
                            update();
                            if (!domTitleWrp.hasClass('edit-status')) {
                                domItemRoot.parent().removeClass('editing');
                            }
                        });

                        domBodyWrp.find('.back-to-origin').click(function () {
                            domBodyWrp.removeClass('edit-status');
                            bindData();
                            if (!domTitleWrp.hasClass('edit-status')) {
                                domItemRoot.parent().removeClass('editing');
                            }
                        });
                        domBodyWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                        })
                            .on('dragover dragenter', function(e) {

                                domResultsRoot.addClass('is-dragover');
                                domBodyWrp.addClass('is-dragover');
                                isDragOnchild = true;
                            })
                            .on('dragleave dragend drop', function() {
                                domBodyWrp.removeClass('is-dragover');
                                isDragOnchild = false;
                            })
                            .on('drop', function(e) {
                                domResultsRoot.removeClass('is-dragover');
                                var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                                if (dataInfo.type !== 'image' && dataInfo.type !== 'other' && dataInfo.type !== 'url'){
                                    var droppedFile = dataInfo.file;
                                    uploadBodyFile(droppedFile);
                                    return true;
                                }
                                else if (dataInfo.type === 'url' && dataInfo.contentType === 'video') {
                                    itemData.body = dataInfo.url;
                                    itemData.type = 2;
                                    bindData();
                                    update();
                                    return true;
                                }
                                else if (dataInfo.type === 'url' && dataInfo.contentType === 'youtube') {
                                    setWithYoutube(dataInfo.url);
                                    return true;
                                }
                                alert('Sorry, This is not Valid Type');
                            });
                        domItemRoot.find('.delete-item').click(function () {
                            swal({
                                title: "Are you sure?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteItem();
                                }
                            });
                        });
                        domItemRoot.find('select[name="type"]').change(function () {
                            itemData.type = $(this).val();
                            update();
                        });
                    }
                    var setWithYoutube = function(url){
                        var parse = helperHandle.parseYoutubeUrl(url);
                        ajaxAPiHandle.apiPost('Api.php', {action: 'get', what: 'youtube', 'where': {id: parse.id}}).then(function (res) {
                            var fData = formatItemData(res.data, 'youtube');
                            $.extend(itemData, fData);
                            itemData.body = url;
                            bindData();
                            update();
                        });
                    }
                    var bindData = function () {
                        if (itemData.image){
                            domImgWrp.addClass('setted');
                            domImgWrp.find('img.item-img').attr('src', itemData.image);
                        }
                        else {
                            domImgWrp.removeClass('setted');
                        }
                        domTitleWrp.find('.item-title').html(itemData.title);
                        // domTitleWrp.find('textarea').val(itemData.title);

                        domBodyWrp.find('.item-description').html(itemData.body);
                        // domBodyWrp.find('textarea').val(itemData.body);

                        domItemRoot.find('.type-wrapper select').val(itemData.type);
                        readMore();

                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        if (itemData.selected) {
                            domItemRoot.find('.wrap .select-post').addClass('filled');
                            domItemRoot.find('.wrap .select-post').html('');
                            domItemRoot.find('.wrap img').show();
                        }
                        else {
                            domItemRoot.find('.wrap .select-post').removeClass('filled');
                            domItemRoot.find('.wrap .select-post').html('Select');
                            domItemRoot.find('.wrap img').hide();
                        }
                    }
                    var readMore = function () {
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 24,
                            moreLink: '<a href="javascript:;">Read More</a>',
                            lessLink: '<a href="javascript:;">View Less</a>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                // domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        // domTitleWrp.addClass('collapsed');

                        domBodyWrp.find('.item-description').readmore({
                            collapsedHeight: 47,
                            moreLink: '<a href="javascript:;">Read More</a>',
                            lessLink: '<a href="javascript:;">View Less</a>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                // domBodyWrp.toggleClass('collapsed');
                            }
                        });
                        // domBodyWrp.addClass('collapsed');
                    }
                    var uploadImageFile = function (file) {
                        domImgWrp.addClass('is-uploading');

                        var data = new FormData();
                        var sets = {
                            prefix: 'post'
                        };
                        data.append('file', file);
                        data.append('sets', JSON.stringify(sets));
                        ajaxAPiHandle.multiPartApiPost('Uploads.php', data).then(function (res) {
                            if (res.status) {
                                itemData.image = res.data.url;
                                bindData();
                                update();
                            }
                        });
                    }
                    var update = function () {
                        if (!itemData.selected) {
                            return false;
                        }
                        var sets = isSeriesMine ? postFormat(itemData) : subscriptionFormat(itemData);
                        if (isSeriesMine) {
                            return ajaxAPiHandle.apiPost('Posts.php', {action: 'update', sets: sets, where: {post_ID: addedPost.post_ID}});
                        }
                        else {
                            return ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', sets: sets, where: {clientsubscription_ID: addedSubscription.clientsubscription_ID}});
                        }
                    }
                    var uploadBodyFile = function (file) {
                        domBodyWrp.addClass('is-uploading');

                        var data = new FormData();
                        var sets = {
                            prefix: 'post'
                        };
                        switch ( checkFile(file) ) {
                            case 'video':
                                sets.where = 'assets/media/video';
                                break;
                            case 'audio':
                                sets.where = 'assets/media/audio';
                                break;
                            case 'document':
                                sets.where = 'assets/media/document';
                                break;
                        }
                        data.append('file', file);
                        data.append('sets', JSON.stringify(sets));
                        ajaxAPiHandle.multiPartApiPost('Uploads.php', data).then(function (res) {
                            if (res.status) {
                                switch ( checkFile(file) ) {
                                    case 'video':
                                        itemData.body = res.data.abs_url;
                                        itemData.type = 2;
                                        break;
                                    case 'audio':
                                        itemData.body = res.data.abs_url;
                                        itemData.type = 0;
                                        break;
                                    case 'document':
                                        itemData.body += '<a href="'+ res.data.abs_url +'">' + res.data.abs_url + '</a>';
                                        itemData.type = 7;
                                        break;
                                }
                                bindData();
                                update();
                            }
                        });
                    }
                    var deleteItem = function () {
                        if (!itemData.selected) {
                            allItemHandles.forEach(function (hdl, i) {
                                if (hdl.id == self.id) {
                                    allItemHandles.splice(i, 1);
                                    domItemRoot.parent().remove();
                                }
                            });
                            return true;
                        }
                        var action, data = {};
                        if (isSeriesMine) {
                            action = 'Posts.php';
                            data.action = 'delete';
                            data.where = addedPost.post_ID;
                        }
                        else {
                            action = 'Subscriptions.php';
                            data.action = 'delete';
                            data.where = {clientsubscription_ID: addedSubscription.clientsubscription_ID};
                        }
                        ajaxAPiHandle.apiPost(action, data).then(function (res) {
                            allItemHandles.forEach(function (hdl, i) {
                                if (hdl.id == self.id) {
                                    allItemHandles.splice(i, 1);
                                    domItemRoot.parent().remove();
                                    isSeriesMine ? mainHandle.deleteInFront(addedPost.post_ID) : mainHandle.deleteInFront(addedSubscription.clientsubscription_ID);
                                }
                            });
                        })
                    }

                    var addToPost = function(){
                        var sets = self.getFormatData();
                        sets = postFormat(sets);
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            addedPost = res.data;
                            mainHandle.addPostRow(res.data);
                        });
                    };

                    var addToSubscription = function () {
                        var sets = self.getFormatData();
                        sets = subscriptionFormat(sets);
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            addedSubscription = res.data;
                            mainHandle.addPostRow(res.data);
                        });
                    }

                    var deleteFromPost = function () {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: addedPost.post_ID}, false).then(function (res) {
                            mainHandle.deleteInFront(addedPost.post_ID);
                            addedPost = false;
                        });
                    }
                    var deleteFromSubscription = function () {
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'delete', where: addedSubscription.clientsubscription_ID}, false).then(function (res) {
                            mainHandle.deleteInFront(addedSubscription.clientsubscription_ID);
                            addedSubscription = false;
                        });
                    }

                    this.selected = function () {
                        return itemData.selected;
                    }
                    this.dom = function () {
                        if (domItemRoot == null) {
                            domItemRoot = domMetaBlocksContainer.find('.item-sample').clone().removeAttr('hidden').removeClass('item-sample');
                            domTitleWrp = domItemRoot.find('.title-wrapper');
                            domImgWrp = domItemRoot.find('.img-wrapper');
                            domBodyWrp = domItemRoot.find('.description-wrapper');
                        }
                        return domItemRoot;
                    }
                    this.selectPost = function () {
                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        btn.removeClass("filled");
                        btn.addClass("circle");
                        btn.html("");
                        $(".wrap  svg", domItemRoot).css("display", "block");
                        $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                        var timer = setInterval(
                            function tick() {
                                if (addedPost || addedSubscription) {
                                    btn.removeClass("circle");
                                    btn.addClass("filled");
                                    $(".wrap img", domItemRoot).css("display", "block");
                                    $("svg", domItemRoot).css("display", "none");
                                    itemData.selected = true;
                                    clearInterval(timer);
                                }
                            }, 2000);
                        is_loading_controlled_in_local = true;
                        isSeriesMine ? addToPost() : addToSubscription();
                        is_loading_controlled_in_local = false;
                    }
                    this.unSelectPost = function () {
                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        btn.removeClass("filled");
                        btn.addClass("circle");
                        $(".wrap  svg", domItemRoot).css("display", "block");
                        $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                        var timer = setInterval(
                            function tick() {
                                if (!addedPost && !addedSubscription) {
                                    btn.removeClass("circle");
                                    btn.html("Select");
                                    $(".wrap img", domItemRoot).css("display", "none");
                                    $("svg", domItemRoot).css("display", "none");
                                    itemData.selected = false;
                                    clearInterval(timer);
                                }
                            }, 2000);
                        is_loading_controlled_in_local = true;
                        isSeriesMine ? deleteFromPost() : deleteFromSubscription();
                        is_loading_controlled_in_local = false;
                    }
                    this.setKeyword = function (kwd) {
                        self.keyword = kwd;
                    }
                    this.getFormatData = function () {
                        itemData.keyword = self.keyword;
                        return {
                            title: itemData.title,
                            body: itemData.body,
                            image: itemData.image,
                            type: itemData.type,
                            keyword: self.keyword,
                            summary: '',
                        };
                    }
                    this.init = function () {
                        if (isSeriesMine) {
                            addedPost = itemData.orgData;
                        }
                        else {
                            addedSubscription = itemData.orgData;
                        }
                        if (itemData.editStatus == 'empty' || itemData.editStatus == 'creating') {
                            domTitleWrp.addClass('edit-status');
                            domBodyWrp.addClass('edit-status');
                            domTitleWrp.find('textarea').val(itemData.title);
                            domBodyWrp.find('textarea').val(itemData.body);
                            domItemRoot.parent().addClass('editing');
                        }
                        bindData();
                        bindEvents();
                        self.keyword = '';
                        self.id = maxId + 1;
                        maxId++;
                    }
                }

                var formatItemData = function(itemData, from){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = itemData.id;
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.text;
                            fData.image = itemData.imgOrg;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    return fData;
                }

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = $(el).find( '> *' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild.length ) {
                            itemChild.addClass('tt-old');
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('>*:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                var addNew = function (data) {
                    if (typeof data == "undefined") {
                        data = {};
                    }
                    var itemData = {
                        title: '',
                        body: '',
                        image: '',
                        type: 7,
                        summary: '',
                        keyword: '',
                        selected: true,
                        editStatus: 'creating'
                    }
                    $.extend(itemData, data);
                    if (isSeriesMine) {
                        var postData = postFormat(itemData);

                        ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: postData}).then(function (res) {
                            itemData.orgData = res.data;
                            var handle = new ItemClass(itemData);
                            $(grid).append($('<li></li>').append(handle.dom()));
                            handle.init();
                            allItemHandles.push(handle);
                        });
                    }
                    else {
                        var postData = subscriptionFormat(itemData);

                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: postData}).then(function (res) {
                            itemData.orgData = res.data;
                            var handle = new ItemClass(itemData);
                            $(grid).append($('<li></li>').append(handle.dom()));
                            handle.init();
                            allItemHandles.push(handle);
                        });
                    }
                }
                var addByFile = function (file) {
                    var ajaxData = new FormData();
                    var sets = {};
                    var fileType = checkFile(file);
                    switch (fileType) {
                        case 'video':
                            sets.where = 'assets/media/video';
                            break;
                        case 'audio':
                            sets.where = 'assets/media/audio';
                            break;
                        case 'image':
                            sets.where = 'assets/images';
                            break;
                        case 'document':
                            sets.where = 'assets/media/document';
                            break;
                    }
                    sets.prefix = 'post';
                    ajaxData.append('file', file);
                    ajaxData.append('sets', JSON.stringify(sets));
                    ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData).then(function (res) {
                        var itemData = {};
                        switch (fileType) {
                            case 'video':
                                itemData.type = 2;
                                itemData.body = res.data.url;
                                break;
                            case 'audio':
                                itemData.type = 0;
                                itemData.body = res.data.url;
                                break;
                            case 'image':
                                itemData.image = res.data.url;
                                break;
                            case 'document':
                                itemData.body = '<a href="' + res.data.url + '">' + res.data.url + '</a>';
                                break;
                        }
                        addNew(itemData);
                    });
                }
                var addByUrl = function (url) {
                    var urlType  = checkUrl(url);
                    var itemData = {};
                    switch (urlType) {
                        case 'video':
                            itemData.type = 2;
                            itemData.body = url;
                            break;
                        case 'audio':
                            itemData.type = 0;
                            itemData.body = url;
                            break;
                        case 'image':
                            itemData.image = url;
                            break;
                        case 'youtube':
                            var parse = helperHandle.parseYoutubeUrl(url);
                            ajaxAPiHandle.apiPost('Api.php', {action: 'get', where: {id: parse.id}, what: 'youtube'}).then(function (res) {
                                var itemData = formatItemData(res.data, 'youtube');
                                itemData.body = url;
                                itemData.editStatus = 'pending';
                                addNew(itemData);
                            });
                            return ;
                            break;
                        default:
                            itemData.body = '<a href="' + url + '">' + url + '</a>';
                            break;
                    }
                    addNew(itemData);
                }

                var bindEvents = function () {
                    domNewAdder.find('> *').click(function () {
                        addNew({editStatus: 'empty'});
                    });
                    domResultsRoot.dndhover().on('dndHoverStart', function (event) {
                        domResultsRoot.addClass('is-dragover');
                        domResultsRoot.addClass('is-dragover-on-parent');
                        event.stopPropagation();
                        event.preventDefault();
                        return true;
                    }).on('dndHoverEnd', function (event) {
                        if (isDragOnchild === false){
                            domResultsRoot.removeClass('is-dragover');
                        }
                        domResultsRoot.removeClass('is-dragover-on-parent');
                        event.stopPropagation();
                        event.preventDefault();
                        return true;
                    });
                    domResultsRoot.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                    })
                    domResultsRoot.on('drop', function (e) {
                        domResultsRoot.removeClass('is-dragover');
                        domResultsRoot.removeClass('is-dragover-on-parent');
                        var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                        switch (dataInfo.type) {
                            case 'video':
                            case 'audio':
                            case 'document':
                            case 'image':
                                addByFile(dataInfo.file);
                                break;
                            case 'url':
                                addByUrl(dataInfo.url);
                                break;
                        }
                    });
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(formatItemData(item));
                        itemHandle.setKeyword(newKeyword);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.keyword = '';
                this.nextPageToken = false;

                this.show = function () {
                    domResultsRoot.show();
                    self.isShown = true;
                }

                this.hide = function () {
                    domResultsRoot.hide();
                    self.isShown = false;
                }

                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }

                this.emptySelectedItems = function () {
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            itemHandle.unSelectPost();
                        }
                    });
                }

                this.init = function () {
                    domResultsRoot = domMetaBlocksContainer.find('.tt-grid-wrapper.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domMetaBlocksContainer);

                    domBlockTitle = domResultsRoot.find('.from-where');
                    domNewAdder = domResultsRoot.find('.new-adder');
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    isAnimating = false;
                    bindEvents();
                }
            }

            var PostsBlockClass = function (from) {
                var domResultsRoot, domBlockTitle;

                var self = this;
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];
                var grid, items, isAnimating;

                var ItemClass = function (itemData) {

                    var domItemRoot = null, domTitleWrp;
                    var self = this;

                    var addedPost = false, addedSubscription = false;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').parent().click(function () {
                            self.selected ? self.unSelectPost() : self.selectPost();
                        });
                    }
                    var readMore = function () {
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<a href="javascript:;">Read More</a>',
                            lessLink: '<a href="javascript:;">View Less</a>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                    }

                    var addToPost = function(){
                        var sets = self.getFormatData();
                        sets = postFormat(sets);
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            addedPost = res.data;
                            mainHandle.addPostRow(res.data);
                        });
                    };

                    var addToSubscription = function () {
                        var sets = self.getFormatData();
                        sets = subscriptionFormat(sets);
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            addedSubscription = res.data;
                            mainHandle.addPostRow(res.data);
                        });
                    }

                    var deleteFromPost = function () {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: addedPost.post_ID}, false).then(function (res) {
                            mainHandle.deleteInFront(addedPost.post_ID);
                            addedPost = false;
                        });
                    }
                    var deleteFromSubscription = function () {
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'delete', where: addedSubscription.clientsubscription_ID}, false).then(function (res) {
                            mainHandle.deleteInFront(addedSubscription.clientsubscription_ID);
                            addedSubscription = false;
                        });
                    }

                    this.html = function () {
                        var cloneDom = domBlocksContainer.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img.item-img').attr('src', itemData.image);
                        cloneDom.find('.title-wrapper .item-title').html(itemData.title);
                        var html = cloneDom.html();
                        cloneDom.remove();
                        return html;
                    }
                    this.selectPost = function () {
                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        btn.removeClass("filled");
                        btn.addClass("circle");
                        btn.html("");
                        $(".wrap  svg", domItemRoot).css("display", "block");
                        $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                        var timer = setInterval(
                            function tick() {
                                if (addedPost || addedSubscription) {
                                    btn.removeClass("circle");
                                    btn.addClass("filled");
                                    $(".wrap img", domItemRoot).css("display", "block");
                                    $("svg", domItemRoot).css("display", "none");
                                    self.selected = true;
                                    clearInterval(timer);
                                }
                            }, 2000);
                        is_loading_controlled_in_local = true;
                        isSeriesMine ? addToPost() : addToSubscription();
                        is_loading_controlled_in_local = false;
                    }
                    this.unSelectPost = function () {
                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        btn.removeClass("filled");
                        btn.addClass("circle");
                        $(".wrap  svg", domItemRoot).css("display", "block");
                        $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                        var timer = setInterval(
                            function tick() {
                                if (!addedPost && !addedSubscription) {
                                    btn.removeClass("circle");
                                    btn.html("Select");
                                    $(".wrap img", domItemRoot).css("display", "none");
                                    $("svg", domItemRoot).css("display", "none");
                                    self.selected = false;
                                    clearInterval(timer);
                                }
                            }, 2000);
                        is_loading_controlled_in_local = true;
                        isSeriesMine ? deleteFromPost() : deleteFromSubscription();
                        is_loading_controlled_in_local = false;
                    }
                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                    }
                    this.selected = false;
                    this.setKeyword = function (kwd) {
                        self.keyword = kwd;
                    }
                    this.getFormatData = function () {
                        itemData.keyword = self.keyword;
                        return itemData;
                    }
                    this.init = function () {
                        bindEvents();
                        readMore();
                    }
                }

                var formatItemData = function(itemData){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.text;
                            fData.image = itemData.imgOrg;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    return fData;
                }

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = $(el).find( '> *' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild.length ) {
                            itemChild.addClass('tt-old');
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('>*:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(formatItemData(item));
                        itemHandle.setKeyword(newKeyword);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }

                this.keyword = '';
                this.nextPageToken = false;

                this.show = function () {
                    domResultsRoot.show();
                }

                this.hide = function () {
                    domResultsRoot.hide();
                }

                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }

                this.emptySelectedItems = function () {
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            itemHandle.unSelectPost();
                        }
                    });
                }

                this.init = function () {
                    domResultsRoot = domBlocksContainer.find('.tt-grid-wrapper.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domBlocksContainer);
                    domBlockTitle = domResultsRoot.find('.from-where');
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    switch (from) {
                        case 'youtube':
                            domBlockTitle.html('Results from Youtube');
                            break;
                        case 'podcasts':
                            domBlockTitle.html('Results from Podcasts');
                            break;
                        case 'blog':
                            domBlockTitle.html('Results from Blogs');
                            break;
                        case 'recipes':
                            domBlockTitle.html('Results from Recipes');
                            break;
                        case 'ideabox':
                            domBlockTitle.html('Results from Ideabox');
                            break;
                        case 'posts':
                            domBlockTitle.html('Results from Posts');
                            break;
                        case 'rssb-posts':
                            domBlockTitle.html('Results from RSSBlogPost');
                            break;
                        default:
                            domBlockTitle.html('Results from Posts');
                            break;

                    }
                    isAnimating = false;
                }
            }

            var goTo_addNewPosts = function(){
                $('<form action="createseriescontent" method="post" hidden><input name="from_page" value="editseries" /><input name="id" value="'+ initialSeries.series_ID +'"/></form>').appendTo('body').submit().remove();
            };

            var checkUrl = function (url) {
                if(url.match(/\.(jpeg|jpg|gif|png)$/) != null){
                    return 'image';
                }
                if (url.match(/\.(mp4|avi)$/)) {
                    return 'video';
                }
                if (url.match(/\.(pdf|text|txt)$/)) {
                    return 'document';
                }
                if (helperHandle.getApiTypeFromUrl(url) == 'youtube') {
                    return 'youtube';
                }
                return 'other';
            }
            var checkFile = function (file) {
                var type = file.type;
                if (accept_image_type.indexOf(type) > -1){
                    return 'image';
                }else if (accept_doc_types.indexOf(type) > -1){
                    return 'document'
                } else if (accept_video_types.indexOf(type) > -1){
                    return 'video';
                } else if (accept_audio_types.indexOf(type) > -1){
                    return 'audio';
                } else {
                    return {type: 'other'};
                }
            }
            var getDataInfo = function (dataTransfer) {

                var droppedUrl = dataTransfer.getData('URL');

                if(droppedUrl != ''){
                    return {url: droppedUrl, type: 'url', contentType: checkUrl(droppedUrl)}
                }

                if (dataTransfer.files.length < 1){
                    return {type: 'other'};
                }
                return { type: checkFile(dataTransfer.files[0]), file: dataTransfer.files[0] };
            }
            var setupDragFn = function () {
                $.fn.dndhover = function(options) {
                    return this.each(function() {

                        var self = $(this);
                        var collection = $();

                        self.on('dragenter', function(event) {
                            if (collection.length === 0) {

                                self.trigger('dndHoverStart');
                            }
                            collection = collection.add(event.target);
                        });

                        self.on('dragleave', function(event) {
                            /*
                             * Firefox 3.6 fires the dragleave event on the previous element
                             * before firing dragenter on the next one so we introduce a delay
                             */
                            setTimeout(function() {
                                collection = collection.not(event.target);
                                if (collection.length === 0) {
                                    self.trigger('dndHoverEnd');
                                }
                            }, 100);
                        });
                    });
                };

                isAdvancedUpload = function() {
                    var div = document.createElement('div');
                    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
                }();
            }

            this.searchPostsByKeyword = searchPostsByKeyword;

            this.IAmDone = function () {
                $('.nav.nav-tabs li:first-child a').tab('show');
                setTimeout(function () {
                    var scrollTop = $('.page.site-wrapper').prop('scrollHeight') - $('.page.site-wrapper').height() - 100;
                    var currentScrollTop = $('.page.site-wrapper').scrollTop();
                    var offset = scrollTop - currentScrollTop;
                    var dur = 200 + offset * 0.3;
                    dur = dur > 1000 ? 1000 : dur;
                    $('.page.site-wrapper').animate({scrollTop: scrollTop + 'px'}, dur);
                }, 210);
            }

            this.viewPage = function(id){
                $('<form action="viewexperience" method="post" hidden><input name="id" value="'+ id +'"></form>').appendTo('body').submit();
            }
            this.showMore = function () {
                var kwd = youtubeListHandle.keyword;
                var pageTokenY = youtubeListHandle.nextPageToken;
                var pageTokenS = podcastsListHandle.nextPageToken;
                var pageTokenT = blogsListHandle.nextPageToken;
                var pageTokenF = recipesListHandle.nextPageToken;
                var pageTokenI = ideaboxListHandle.nextPageToken;
                var pageTokenP = postsListHandle.nextPageToken;
                var pageTokenRssbP = rssbPostsListHandle.nextPageToken;
                self.searchPostsByKeyword(kwd, pageTokenY, pageTokenS, pageTokenT, pageTokenF, pageTokenI, pageTokenP, pageTokenRssbP).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, kwd);
                    podcastsListHandle.setItems(res.data.spreaker, kwd);
                    blogsListHandle.setItems(res.data.blogs, kwd);
                    recipesListHandle.setItems(res.data.recipes, kwd);
                    ideaboxListHandle.setItems(res.data.ideabox, kwd);
                    postsListHandle.setItems(res.data.posts, kwd);
                    rssbPostsListHandle.setItems(res.data.rssbPosts, kwd);
                })
            }
            this.setSearchTerm = function (new_term) {
                domSearch.val(new_term);
                return new Promise(function (resolve, reject) {
                    searchPostsByKeyword(domSearch.val(), false, false, false, false, false, false, false).then(function (res) {
                        setTimeout(function () {
                            resolve();
                        }, 300);
                        setTimeout(function () {
                            youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                            podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                            blogsListHandle.setItems(res.data.blogs, domSearch.val());
                            recipesListHandle.setItems(res.data.recipes, domSearch.val());
                            ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                            postsListHandle.setItems(res.data.posts, domSearch.val());
                            rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                        }, 1000);
                    });
                })
            }
            this.init = function () {

                setupDragFn();

                domBlocksContainer = domRoot.find('main.main-content .posts-block-container');
                domMetaBlocksContainer = domRoot.find('main.main-content .meta-posts-block-container');

                urlListHandle = new MetaPostsBlockClass('url');

                youtubeListHandle = new PostsBlockClass('youtube');
                podcastsListHandle = new PostsBlockClass('podcasts');
                blogsListHandle = new PostsBlockClass('blog');
                recipesListHandle = new PostsBlockClass('recipes');
                ideaboxListHandle = new PostsBlockClass('ideabox');
                postsListHandle = new PostsBlockClass('posts');
                rssbPostsListHandle = new PostsBlockClass('rssb-posts');

                urlListHandle.init();
                youtubeListHandle.init();
                podcastsListHandle.init();
                blogsListHandle.init();
                recipesListHandle.init();
                ideaboxListHandle.init();
                postsListHandle.init();
                rssbPostsListHandle.init();
                domRoot.find('[name="from"]').each(function () {
                    var val = $(this).val();
                    var checked = $(this).prop('checked');
                    var handle = null;
                    switch (val){
                        case 'youtube':
                            handle = youtubeListHandle;
                            break;
                        case 'spreaker':
                            handle = podcastsListHandle;
                            break;
                        case 'blogs':
                            handle = blogsListHandle;
                            break;
                        case 'recipes':
                            handle = recipesListHandle;
                            break;
                        case 'ideabox':
                            handle = ideaboxListHandle;
                            break;
                        case 'posts':
                            handle = postsListHandle;
                            break;
                        case 'rssbposts':
                            handle = rssbPostsListHandle;
                            break;
                        case 'url':
                            handle = urlListHandle;
                            break;
                        default:
                            handle = youtubeListHandle;
                            break;
                    }
                    checked ? handle.show() : handle.hide();
                });
                bindEvents();
            }
        }

        this.init = function () {

            mainHandle = new MainClass();
            mainHandle.init();

            addPostsHandle = new AddPostsClass();
            addPostsHandle.init();

            if (PANEL == 'addNew'){
                addPostsHandle.setSearchTerm(initialSeries.strSeries_title);
            }
            else {
            }
        }
    }

    var editSeriesHandle = new EditSeriesClass();
    editSeriesHandle.init();
})();