(function () {
    'use strict';

    var is_loading_controlled_in_local = false;
    var mainHandle, headerHandle;

    var MainClass = function () {

        var domRoot = $('main.main-content');

        var series = initial_purchasedSeries;

        var postsBlockHandles = [];

        var PostsBlockClass = function (sery) {
            var self = this;

            var domBlock = domRoot.find('.posts-block.sample').clone().removeClass('sample').removeAttr('hidden');
            var domGrid = domBlock.find('.tt-grid');

            var grid = domGrid.get(0),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var itemHandles = [];
            var pageSize = 5;
            var currentIndex = 0;

            var ItemClass = function (itemData) {
                var domItemRoot = null;
                var domTitle, domTitleWrp, domFavWrp;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.view-btn').click(function () {
                        viewPost();
                    });
                    domItemRoot.find('.favorite-wrapper').click(function () {
                        itemData.favorite_ID !== -1 ? self.makeUnfavorite() : self.makeFavorite();
                    });
                };
                var viewPost = function () {
                    var cloneForm = $('<form action="view" method="get" target="_blank" hidden><input name="id" value="'+ itemData.intClientSubscription_post_ID +'"><input name="prevpage" value="favorites"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }

                this.makeFavorite = function () {
                    return $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: {action: 'make_favorite', subsId: itemData.clientsubscription_ID, postId: itemData.intClientSubscription_post_ID, seriesId: sery.series_ID},
                        dataType: 'json',
                        success: function(res) {
                            if (res.status){
                                itemData.favorite_ID = res.data;
                                domFavWrp.addClass('favorited');
                            }
                            else {
                                alert('Sorry. Something wrong!');
                            }
                        },
                        error: function() {}
                    });
                }
                this.makeUnfavorite = function(){
                    return $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: {action: 'make_unFavorite', id: itemData.favorite_ID},
                        dataType: 'json',
                        success: function(res) {
                            if (res.status){
                                itemData.favorite_ID = -1;
                                domFavWrp.removeClass('favorited');
                            }
                            else {
                                alert('Sorry. Something wrong!');
                            }
                        },
                        error: function() {}
                    });
                }

                this.title = function () {
                    return itemData.strClientSubscription_title;
                }
                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', itemData.strClientSubscription_image);
                    cloneDom.find('.title-wrapper .item-title').html(itemData.strClientSubscription_title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }
                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domTitle = domItemRoot.find('.item-title');
                    domFavWrp = domItemRoot.find('.favorite-wrapper');
                }
                this.init = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    itemData.favorite_ID !== -1 ? domFavWrp.addClass('favorited') : domFavWrp.removeClass('favorited');
                    bindEvents();
                }
            }
            var loadNewSet = function(set) {
                return new Promise(function (resolve, reject) {
                    if (isAnimating === true){
                        resolve();
                    }
                    isAnimating = true;
                    checkCanPagging();

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );

                    // apply effect
                    setTimeout( function() {
                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ].querySelector( 'a:last-child' )));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                            resolve();
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                });
            };

            var bindEvents = function () {
                domBlock.find('.prev-pagging').click(function () {
                    self.previous();
                });
                domBlock.find('.next-pagging').click(function () {
                    self.next();
                });
                domBlock.find('.view-category').click(function () {
                    viewCategory();
                });
            }

            var checkCanPagging = function () {
                if (currentIndex - pageSize < 0){
                    domBlock.find('.prev-pagging').addClass('disable');
                }
                else {
                    domBlock.find('.prev-pagging').removeClass('disable');
                }
                if (currentIndex + pageSize >= itemHandles.length){
                    domBlock.find('.next-pagging').addClass('disable');
                }
                else {
                    domBlock.find('.next-pagging').removeClass('disable');
                }
            }

            this.title = function () {
                return sery.strCategory_name;
            }
            this.sortItemsBy = function (sortBy) {
                switch (sortBy){
                    case 'a-z':
                        itemHandles.sort(sortByAZ);
                        break;
                    case 'z-a':
                        itemHandles.sort(sortByZA);
                        break;
                    default:
                        break;
                }
            }
            this.disappear = function () {
                currentIndex = 0;
                return new Promise(function (resolve, reject) {
                    loadNewSet([]).then(function () {
                        domBlock.addClass('disappear');
                        setTimeout(function () {
                            domBlock.detach();
                            domBlock.removeClass('disappear');
                            resolve();
                        }, 500);
                    });
                });
            }
            this.appear = function () {
                domBlock.addClass('appear');
                domBlock.appendTo(domRoot);
                return new Promise(function (resolve, reject) {
                    loadNewSet(itemHandles.slice(currentIndex, currentIndex + 5)).then(function () {
                        domBlock.removeClass('appear');
                        resolve();
                    })
                });
            }
            this.previous = function () {
                if (currentIndex - pageSize < 0 || isAnimating){
                    return false;
                }
                currentIndex -= pageSize;
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize));
            }
            this.next = function () {
                if (currentIndex + pageSize >= itemHandles.length || isAnimating){
                    return false;
                }
                currentIndex += pageSize;
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize));
            }
            this.init = function () {
                domBlock.find('.sery-title').html(sery.strSeries_title);
                sery.subscriptions.forEach(function (post) {
                    var itemHandle = new ItemClass(post);
                    itemHandles.push(itemHandle);
                });
                bindEvents();
            }
        }
        var sortByAZ = function (a, b) {
            var title1 = a.title().toLowerCase();
            var title2 = b.title().toLowerCase();
            return title1 > title2 ? 1 : (title1 < title2 ? -1 : 0);
        }
        var sortByZA = function (a, b) {
            var title1 = a.title().toLowerCase();
            var title2 = b.title().toLowerCase();
            return title1 > title2 ? -1 : (title1 < title2 ? 1 : 0);
        }

        this.appearBlock = function (seryId) {
            postsBlockHandles[seryId].appear();
        }
        this.disappearBlock = function (seryId) {
            postsBlockHandles[seryId].disappear();
        }
        this.init = function () {
            series.forEach(function (sery) {
                var handle = new PostsBlockClass(sery);
                handle.init();
                postsBlockHandles[sery.series_ID] = handle;
            });
        }
    }
    var HeaderClass = function () {

        var domRoot = $('header.content-header');
        var domDropdown = domRoot.find('.dropdown');
        var domDropdownMenu = $('.dropdown-menu', domDropdown);
        var series = [];

        var self = this;

        var bindEvents = function () {
            domDropdownMenu.find('li').click(function () {
                var checkbox = $(this).find('input');
                checkbox.prop('checked', !checkbox.prop('checked')).change();
                return false;
            });
            domDropdownMenu.find(':checkbox').change(function () {
                this.checked ? mainHandle.appearBlock($(this).val()) : mainHandle.disappearBlock($(this).val());
            });
        }

        this.selectSery = function (series_id) {
            domDropdownMenu.find('#select-series-checkbox-' + series_id).parents('li').click();
        }

        this.init = function () {
            initial_purchasedSeries.forEach(function (sery) {
                series.push({id: sery.series_ID, title: sery.strSeries_title});
            });
            series.forEach(function (sery) {
                var checkbox_html = '<li><a href="javascript:;"><input id="select-series-checkbox-' + sery.id + '" type="checkbox" value="' + sery.id + '"/><label for="select-series-checkbox-' + sery.id + '">' + sery.title + '</label></a></li>';
                domDropdownMenu.append(checkbox_html);
            });
            bindEvents();

            var maxCnt = 0, idOfMaxCnt;
            initial_purchasedSeries.forEach(function (sery) {
                if (maxCnt < sery.subscriptions.length){
                    maxCnt = sery.subscriptions.length;
                    idOfMaxCnt = sery.series_ID;
                }
            });
            if(maxCnt){
                self.selectSery(idOfMaxCnt);
            }
        }
    }

    mainHandle = new MainClass();
    mainHandle.init();

    headerHandle = new HeaderClass();
    headerHandle.init();

    $(document).ajaxStart(function () {
        if(!is_loading_controlled_in_local) {
            $('.loading').css('display', 'block');
        }
    });
    $(document).ajaxComplete(function () {
        if(!is_loading_controlled_in_local){
            $('.loading').css('display', 'none');
        }
    });
})();