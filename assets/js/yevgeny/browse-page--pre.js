(function () {
    'use strict';

    var PageClass = function () {

        var domPage;
        var mainHandle = null;
        var filterHandle = null;

        var FilterClass = function () {

            var domRoot;
            var domSortByDropDown;
            var self = this;

            var bindEvents = function () {
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    mainHandle.applyFilter();
                });
                domRoot.find('.filters input[type="checkbox"]').change(function () {
                    mainHandle.applyFilter();
                });
                domRoot.find('.resize-filter').click(function () {
                    if (domRoot.hasClass('size-full')) {
                        self.small();
                    }
                    else {
                        self.maximize();
                    }
                });
                domRoot.find('.open-img').click(function () {
                    self.small();
                });
                domRoot.find('.close-img').click(function () {
                    self.close();
                });
            }
            this.maximize = function () {
                domRoot.draggable( "destroy" );
                domRoot.removeClass('size-small');
                domRoot.removeClass('size-closed');
                domRoot.addClass('size-full');
                domRoot.css('height', 'auto');
                domRoot.css('left', '0px');
                domRoot.css('top', '0px');
                domRoot.parent().removeClass('filter-collapsed');
            }
            this.small = function () {
                domRoot.removeClass('size-full');
                domRoot.removeClass('size-closed');
                domRoot.draggable({
                    addClasses: false,
                    handle: ".drag-handler",
                    appendTo: "parent"
                });
                domRoot.css('left', 'calc(100% - 800px)');
                domRoot.css('top', '100px');
                domRoot.addClass('size-small');
                domRoot.parent().addClass('filter-collapsed');
            }
            this.close = function () {
                domRoot.draggable( "destroy" );
                domRoot.removeClass('size-full');
                domRoot.removeClass('size-small');
                domRoot.css('height', domRoot.css('height'));
                domRoot.addClass('size-closed');
                domRoot.parent().addClass('filter-collapsed');
            }
            this.data = function () {
                return {
                    filter: {
                        free: domRoot.find('input#browse-free-content').prop('checked') ? 1 : 0,
                        premium: domRoot.find('input#browse-premium-content').prop('checked') ? 1 : 0,
                        affiliate: domRoot.find('input#browse-affiliates-content').prop('checked') ? 1 : 0
                    },
                    sort: domSortByDropDown.data('value')
                };
            }
            this.init = function () {
                domRoot = domPage.find('.filter-wrapper');
                domSortByDropDown = domRoot.find('.sort-by .dropdown');
                bindEvents();
            }
        }

        var MainClass = function () {
            var domRoot;

            var itemHandles = [];
            var filteredHandles = [];

            var ItemClass = function (itemData) {
                var domItem;
                var self = this, payHandle = null;

                var viewUserPage = function (data) {
                    $('<form action="my_solutions" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
                }
                var popupThanksJoin = function (data) {
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Thanks for joining! Would you like to go to your experiences?',
                        buttons: {
                            returnHome: {
                                text: "My Experiences",
                                value: 'return_home',
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            customize: {
                                text: "Not yet",
                                value: 'customize',
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        },
                        closeOnClickOutside: false,
                    }).then(function (value) {
                        if (value == 'return_home'){
                            viewUserPage(data);
                        }
                        else {
                        }
                    });
                    var domSwalRoot, domDisableCheckbox;
                    var disablePopup = function () {
                        localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
                    }
                    var enabelPopup = function () {
                        localStorage.removeItem('thegreyshirt_show_popup_every_join');
                    }
                    var bindEvents = function () {
                        domDisableCheckbox.change(function () {
                            $(this).prop('checked') ? disablePopup() : enabelPopup();
                        })
                    }
                    var init = function () {
                        setTimeout(function () {
                            domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                            domSwalRoot.addClass('thanks-join');
                            domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                            domDisableCheckbox = domSwalRoot.find('#disable_popup');
                            bindEvents();
                        }, 100);
                    }();
                }
                var popupErroJoin = function () {
                    swal("Sorry, Something went wrong!", {
                        content: 'please try again later'
                    });
                }

                var openStripe = function () {
                    if (payHandle) {
                        payHandle.open({
                            name: itemData.strSeries_title,
                            description: itemData.strSeries_description,
                            currency: 'usd',
                            amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                        });
                    }
                    else {
                        popupErroJoin();
                    }
                }

                var join = function (token) {
                    ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = res.data;
                            domItem.addClass('purchased');
                        }
                    });
                }

                var unJoin = function () {
                    swal("You already joined this series!", {
                        content: 'Thanks!'
                    });
                    return false;

                    ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: itemData.purchased}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = 0;
                        }
                    });
                }

                var bindEvents = function () {
                    domItem.find('.join-btn').click(function () {
                        if (CLIENT_ID > -1){
                            if (parseInt(itemData.boolSeries_charge) === 1 && parseInt(itemData.purchased) == 0) {
                                openStripe();
                            }
                            else {
                                parseInt(itemData.purchased) ? unJoin() : join();
                            }
                        }
                        else{
                            swal({
                                title: "Please Login first!",
                                icon: "warning",
                                dangerMode: true,
                                buttons: {
                                    cancel: "Cancel",
                                    login: "Login"
                                },
                            }).then(function(value){
                                if (value == 'login'){
                                    window.location.href = 'login';
                                }
                            });
                        }
                    });
                }

                this.isFiltered = function () {
                    var filterData = filterHandle.data().filter;
                    if (filterData.free || filterData.premium || filterData.affiliate) {
                        if (filterData.free && !parseInt(itemData.boolSeries_charge)) {
                            return true;
                        }
                        if (filterData.premium && parseInt(itemData.boolSeries_charge)) {
                            return true;
                        }
                        if (filterData.affiliate && parseInt(itemData.boolSeries_affiliated)) {
                            return true;
                        }
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domRoot.find('>.flex-row'));
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = function () {
                    if (itemData.purchased) {
                        domItem.addClass('purchased');
                    }
                    domItem.find('.item-image').css('background-image', 'url(' + itemData.strSeries_image + ')');
                    domItem.find('.child-count span').text(itemData.posts_count);
                    domItem.find('.item-title').text(itemData.strSeries_title);
                    domItem.find('.preview-btn').attr('href', 'preview_series?id=' + itemData.series_ID);
                    domItem.css('order', itemHandles.length + 1);
                    domItem.find('.item-category').attr('href', 'browse?id=' + itemData.category.category_ID).html(itemData.category.strCategory_name);
                }
                this.setOrder = function (order) {
                    domItem.css('order', order);
                }
                this.init = function () {
                    domItem = domRoot.find('.series-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('>.flex-row'));
                    self.bindData();
                    if (itemData.boolSeries_charge == 1 && itemData.purchased == 0) {
                        if (stripeApi_pKey && itemData.stripe_account_id) {
                            payHandle = StripeCheckout.configure({
                                key: stripeApi_pKey,
                                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                locale: 'auto',
                                token: function(token) {
                                    join(token);
                                }
                            });
                        }
                    }
                    bindEvents();
                }
            }

            this.applyFilter = function () {
                domRoot.css('height', domRoot.css('height'));
                domRoot.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (hdl) {
                        hdl.detach();
                    });
                    filteredHandles = itemHandles.filter(function (hdl) {
                        return  hdl.isFiltered();
                    });
                    var sortBy = filterHandle.data().sort;
                    filteredHandles = filteredHandles.sort(function (a, b) {
                        var ad = a.data();
                        var bd = b.data();
                        switch (sortBy) {
                            case 'new':
                                return parseInt(bd.series_ID) -  parseInt(ad.series_ID);
                                break;
                            case 'a-z':
                                return ad.strSeries_title > bd.strSeries_title ? 1 : (ad.strSeries_title == bd.strSeries_title ? 0 : -1);
                                break;
                            case 'z-a':
                                return ad.strSeries_title > bd.strSeries_title ? -1 : (ad.strSeries_title == bd.strSeries_title ? 0 : 1);
                                break;
                            default:
                                return parseInt(ad.series_ID) -  parseInt(bd.series_ID);;
                                break;
                        }
                    });
                    filteredHandles.forEach(function (value, i) {
                        value.setOrder(i+1);
                        value.append();
                    });
                    domRoot.css('opacity', 1);
                    domRoot.css('height', 'auto');
                }, 300);
            }

            this.init = function () {
                domRoot = domPage.find('main.main-content');
                domRoot.find('.category-article .category-title').html(category.strCategory_name);
                domRoot.find('.category-article .category-description').html(category.strCategory_description);
                series.forEach(function (itemData) {
                    var hdl = new ItemClass(itemData);
                    hdl.init();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                });
            }
        };

        this.init = function () {
            domPage = $('.site-content');
            mainHandle = new MainClass();
            mainHandle.init();
            filterHandle = new FilterClass();
            filterHandle.init();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();