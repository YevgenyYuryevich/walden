(function () {
    'use strict';

    var createEventHandle;
    var is_loading_controlled_in_local = 0;

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }
    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('-');
    };

    String.prototype.stringToDate = function () {
        var yyyy = this.split('-')[0];
        var mm = this.split('-')[1] * 1 - 1;
        var dd = this.split('-')[2];
        return new Date(yyyy, mm, dd);
    }

    var CreateEventClass = function () {
        var domRoot, domStartDate, domStartTime;
        var start_date = '';
        var start_time = '';

        var self = this;

        var createEvent = function () {
            var ajaxData = new FormData();
            ajaxData.append('action', 'insert');
            var files = domRoot.find('.cover-image-wrapper [name="cover_img"]').prop('files');
            if (files.length){
                ajaxData.append('event_image', files[0]);
            }
            var sets = {
                event_title: domRoot.find('[name="title"]').val(),
                event_description: domRoot.find('[name="description"]').val(),
                event_location: domRoot.find('[name="location"]').val(),
                start_date: start_date.yyyymmdd(),
                start_time: start_time.value + ':00'
            }
            // var is_recurring = domRoot.find('[name="is_recurring"]').prop('checked') ? 1 : 0;
            var is_recurring = 0;
            sets.is_recurring = is_recurring;
            if (is_recurring){
                if (domRoot.find('[name="recurring_type"]').val() == 'd') {
                    sets.recurring_pattern = dailyRecurringHandle.getDailyRecurringData();
                }
                else {
                    sets.recurring_pattern = weeklyRecurringHandle.getWeeklyRecurringData();
                }
            }
            ajaxData.append('sets', JSON.stringify(sets));

            $.ajax({
                url: API_ROOT_URL + '/Events.php',
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    console.log(res);
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Your event are saved correctly',
                        buttons: {
                            returnHome: {
                                text: "RETURN HOME",
                                value: 'return_home',
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            customize: {
                                text: "CUSTOMIZE",
                                value: 'customize',
                                visible: false,
                                className: "",
                                closeModal: true
                            }
                        },
                        closeOnClickOutside: false,
                    }).then(function (value) {
                        if (value == 'return_home'){
                            viewUserPage();
                        }
                        else {

                        }
                    });
                }
            });
        };
        var updateCoverImage = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.cover-image-wrapper .img-wrapper img', domRoot).addClass('touched');
                    $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        var bindEvents = function () {
            domStartDate.on('changeDate', function (e) {
                start_date = e.date;
            });
            domStartTime.on('changeTime.timepicker', function (e) {
                start_time = e.time;
            });
            domRoot.find('.cover-image-wrapper [name="cover_img"]').change(function () {
                updateCoverImage(this);
            });
            domRoot.submit(function () {
                createEvent();
                return false;
            });
        }

        var viewUserPage = function () {
            $('<form action="userpage" method="post" hidden></form>').appendTo('body').submit().remove();
        }
        this.init = function () {
            var now = new Date();
            domRoot = $('form#event-form');
            domStartDate = domRoot.find('input[name="start_date"]');
            domStartTime = domRoot.find('input[name="start_time"]');
            domStartDate.datepicker();
            domStartTime.timepicker({
                defaultTime: now.getHours() + ':' + now.getMinutes(),
                showMeridian: false
            });
            start_date = now;
            bindEvents();
            domStartDate.datepicker('setDate', new Date(now.getYear() + 1900, now.getMonth(), now.getDate()));
            domStartTime.timepicker('setTime', new Date().getHours() + ':' + new Date().getMinutes());
        }
    }

    createEventHandle = new CreateEventClass();
    createEventHandle.init();
    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });
})();