var is_loading_controlled_in_local = false;


(function() {

    var explorePageHandle;

    var ExplorePageClass = function () {

        var postsSectionHandle;

        var PostsSectionClass = function () {

            var categoriesHistory = [];
            var categories = [];
            var currentIndex = 0;
            var postsBlockHandles = [];

            var domRoot = $('main.main-content');
            var PostsBlockClass = function (posts) {
                var self = this;

                var domBlock = domRoot.find('.posts-block.sample').clone().removeClass('sample').removeAttr('hidden');
                var domGrid = domBlock.find('.tt-grid');

                var grid = domGrid.get(0),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var totalItemHandles = [];
                var filteredItemHandles = [];
                var pageSize = 5;
                var currentIndex = 0;
                var arrPageStatus = [
                    {class: 'scope-width-5', pageSize: 5},
                    {class: 'scope-width-5-sm', pageSize: 5},
                    {class: 'scope-width-4', pageSize: 4},
                    {class: 'scope-width-3', pageSize: 3},
                    {class: 'scope-width-2', pageSize: 4},
                    {class: 'scope-width-1', pageSize: 2},
                ];
                var currentPageStatus;

                var getScreenStatus = function () {
                    var w = domRoot.outerWidth();
                    if (w > 1300){
                        return 0;
                    }
                    else if (w > 1100){
                        return 1;
                    }
                    else if (w > 1000){
                        return 2;
                    }
                    else if (w > 850){
                        return 3;
                    }
                    else if (w > 668){
                        return 4;
                    }
                    else {
                        return 5;
                    }
                }
                var ItemClass = function (itemData) {
                    var domItemRoot = null;
                    var domTitleWrp;
                    var self = this;
                    var stripeHandler = null;
                    var bindEvents = function () {
                        domItemRoot.find('.select-post').parent().click(function () {
                            if (CLIENT_ID > -1){
                                if (parseInt(itemData.purchased)) {
                                    self.unSelectPost()
                                }
                                else {
                                    if (itemData.boolSeries_charge) {
                                        openStripe();
                                    }
                                    else {
                                        self.selectPost();
                                    }
                                }
                            }
                            else{
                                swal({
                                    title: "Please Login first!",
                                    icon: "warning",
                                    dangerMode: true,
                                    buttons: {
                                        cancel: "Cancel",
                                        login: "Login"
                                    },
                                }).then(function(value){
                                    if (value == 'login'){
                                        window.location.href = 'login';
                                    }
                                });
                            }
                        });
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.title = function () {
                        return itemData.strSeries_title;
                    }
                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', itemData.strSeries_image);
                        cloneDom.find('.title-wrapper .item-title').html(itemData.strSeries_title);
                        cloneDom.find('.view-post').attr('href', BASE_URL + '/viewexperience.php?id=' + itemData.series_ID + '&prev=' + CURRENT_PAGE);
                        cloneDom.find('.preview-post').attr('href', BASE_URL + '/previewseries.php?id=' + itemData.series_ID + '&prev=' + CURRENT_PAGE);
                        if (parseInt(itemData.boolSeries_charge)) {
                            cloneDom.find('.select-post').html('Join ' + '$' + itemData.intSeries_price);
                        }
                        var html = cloneDom.html();
                        cloneDom.remove();
                        return html;
                    }
                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                    }
                    var readMore = function () {
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                    }
                    var openStripe = function () {
                        if (stripeHandler) {
                            stripeHandler.open({
                                name: itemData.strSeries_title,
                                description: itemData.strSeries_description,
                                currency: 'usd',
                                amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                            });
                        }
                        else {
                            popupErroJoin();
                        }
                    }
                    this.selectPost = function (token) {
                        token = token ? token : 0;
                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        btn.removeClass("filled");
                        btn.addClass("circle");
                        btn.html("");
                        $(".wrap  svg", domItemRoot).css("display", "block");
                        $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                        var sRes = null;
                        var timer = setInterval(
                            function tick() {
                                if (sRes === true){
                                    btn.removeClass("circle");
                                    btn.addClass("filled");
                                    $(".wrap img", domItemRoot).css("display", "block");
                                    $(".wrap svg", domItemRoot).css("display", "none");
                                    clearInterval(timer);
                                    setTimeout(function () {
                                        var s = localStorage.getItem('thegreyshirt_show_popup_every_join');
                                        if (s == null || s === '1'){
                                            popupThanksJoin(itemData.purchased);
                                        }
                                    }, 500);
                                }
                                else if (sRes === false) {
                                    btn.removeClass("circle");
                                    btn.html("Join");
                                    if (parseInt(itemData.boolSeries_charge)) {
                                        setTimeout(function () {
                                            btn.html('Join $' + itemData.intSeries_price);
                                        }, 400);
                                    }
                                    $(".wrap img", domItemRoot).css("display", "none");
                                    $(".wrap svg", domItemRoot).css("display", "none");
                                    clearInterval(timer);
                                    setTimeout(function () {
                                        popupErroJoin();
                                    }, 500);
                                }
                            }, 2000);
                        is_loading_controlled_in_local = true;
                        ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}, false).then(function (res) {
                            if (res.status === true){
                                sRes = true;
                                itemData.purchased = res.data;
                            }
                            else {
                                sRes = false;
                            }
                        });
                        is_loading_controlled_in_local = false;
                    }
                    this.unSelectPost = function () {
                        var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                        btn.removeClass("filled");
                        btn.addClass("circle");
                        $(".wrap  svg", domItemRoot).css("display", "block");
                        $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                        var timer = setInterval(
                            function tick() {
                                if (!itemData.purchased){
                                    btn.removeClass("circle");
                                    btn.html("Join");
                                    if (parseInt(itemData.boolSeries_charge)) {
                                        setTimeout(function () {
                                            btn.html('Join $' + itemData.intSeries_price);
                                        }, 400);
                                    }
                                    $(".wrap img", domItemRoot).css("display", "none");
                                    $(".wrap svg", domItemRoot).css("display", "none");
                                    clearInterval(timer);
                                }
                            }, 2000);
                        is_loading_controlled_in_local = true;
                        $.ajax({
                            url: API_ROOT_URL + '/Series.php',
                            data: {action: 'unJoin', id: itemData.purchased},
                            success: function (res) {
                                if (res.status == true){
                                    itemData.purchased = 0;
                                }
                                is_loading_controlled_in_local = true;
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.init = function () {
                        itemData.boolSeries_charge = parseInt(itemData.boolSeries_charge);
                        if (itemData.boolSeries_charge) {
                            itemData.intSeries_price =  parseFloat(Math.round(itemData.intSeries_price * 100) / 100).toFixed(2);
                            itemData.intSeries_price = parseFloat(itemData.intSeries_price);
                            domItemRoot.addClass('charged');
                            if (itemData.stripe_account_id && stripeApi_pKey) {
                                stripeHandler = StripeCheckout.configure({
                                    key: stripeApi_pKey,
                                    image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                    locale: 'auto',
                                    token: function(token) {
                                        self.selectPost(token);
                                        // You can access the token ID with `token.id`.
                                        // Get the token ID to your server-side code for use.
                                    }
                                });
                            }
                        }
                        if (parseInt(itemData.purchased)){
                            domItemRoot.find('.wrap .select-post').addClass('filled');
                            domItemRoot.find('.wrap .select-post').html('');
                            domItemRoot.find('.wrap img').show();
                        }
                        readMore();
                        bindEvents();
                    }
                }
                var loadNewSet = function(set) {
                    return new Promise(function (resolve, reject) {
                        if (isAnimating === true){
                            resolve();
                        }
                        isAnimating = true;
                        checkCanPagging();

                        var newImages = set;
                        items.forEach( function( el ) {
                            var itemChild = $(el).find( '> *' );
                            // add class "tt-old" to the elements/images that are going to get removed
                            if( itemChild.length ) {
                                itemChild.addClass('tt-old');
                            }
                        } );

                        // apply effect
                        setTimeout( function() {
                            // append new elements
                            if(newImages){
                                [].forEach.call( newImages, function( el, i ) {
                                    items[ i ].innerHTML += el.html();
                                    el.bindDom($(items[ i ]).find( '> *:last-child' ));
                                    el.init();
                                } );
                            }

                            // add "effect" class to the grid
                            classie.add( grid, 'tt-effect-active' );


                            // wait that animations end
                            var onEndAnimFn = function() {
                                // remove old elements
                                items.forEach( function( el , i) {
                                    // remove old elems
                                    var old = el.querySelector( '.tt-old' );
                                    if( old ) { el.removeChild( old ); }
                                    // remove class "tt-empty" from the empty items
                                    classie.remove( el, 'tt-empty' );
                                    // now apply that same class to the items that got no children (special case)
                                    if ( !$(el).children().length ) {
                                        classie.add( el, 'tt-empty' );
                                    }
                                    else {
                                        // newImages[i].bindDom($(el));
                                    }
                                } );
                                // remove the "effect" class
                                classie.remove( grid, 'tt-effect-active' );
                                isAnimating = false;
                                resolve();
                            };

                            if( support ) {
                                onAnimationEnd( items, items.length, onEndAnimFn );
                            }
                            else {
                                onEndAnimFn.call();
                            }
                        }, 25 );
                    });
                };

                var viewCategory = function () {
                    var cloneForm = $('<form action="exploreinterests" method="post" target="_blank" hidden><input name="id" value="'+ posts.category_ID +'"><input name="prevPage" value="'+ CURRENT_PAGE +'"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }

                var bindEvents = function () {
                    domBlock.find('.prev-pagging').click(function () {
                        self.previous();
                    });
                    domBlock.find('.next-pagging').click(function () {
                        self.next();
                    });
                    domBlock.find('.view-category').click(function () {
                        viewCategory();
                    });
                    $(window).resize(function () {
                        var pageStatus = arrPageStatus[getScreenStatus()];
                        if (pageStatus != currentPageStatus){
                            if (pageStatus.pageSize > pageSize){
                                self.setPageStatus(pageStatus);
                                loadNewSet(filteredItemHandles.slice(currentIndex, currentIndex + pageSize));
                            }
                            else {
                                self.setPageStatus(pageStatus);
                            }
                        }
                    });
                }

                var checkCanPagging = function () {
                    if (currentIndex - pageSize < 0){
                        domBlock.find('.prev-pagging').addClass('disable');
                    }
                    else {
                        domBlock.find('.prev-pagging').removeClass('disable');
                    }
                    if (currentIndex + pageSize >= filteredItemHandles.length){
                        domBlock.find('.next-pagging').addClass('disable');
                    }
                    else {
                        domBlock.find('.next-pagging').removeClass('disable');
                    }
                }
                this.setPageStatus = function (pageStatus) {
                    for (var i = pageStatus.pageSize; i < items.length; i ++){
                        $(items[i]).remove();
                    }
                    for (var i = 0; i < pageStatus.pageSize - items.length; i++){
                        domGrid.append('<li class="tt-empty"></li>');
                    }
                    domBlock.find('.tt-grid-wrapper').addClass(pageStatus.class);
                    if (currentPageStatus) { domBlock.find('.tt-grid-wrapper').removeClass(currentPageStatus.class);}
                    currentPageStatus = pageStatus;
                    pageSize = currentPageStatus.pageSize;
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    checkCanPagging();
                }
                this.title = function () {
                    return posts.strCategory_name;
                }
                this.sortItemsBy = function (sortBy) {
                    switch (sortBy){
                        case 'a-z':
                            filteredItemHandles.sort(sortByAZ);
                            break;
                        case 'z-a':
                            filteredItemHandles.sort(sortByZA);
                            break;
                        default:
                            break;
                    }
                }

                this.paymentFilter = function () {
                    filteredItemHandles = [];
                    var isPremium = $('[data-field="premium-filter"]').parents('.filter-item').hasClass('active-item') ? 1 : 0;
                    var isAffiliated = $('[data-field="affiliate-filter"]').parents('.filter-item').hasClass('active-item') ? 1 : 0;
                    // marked
                    filteredItemHandles = totalItemHandles.filter(function (value) {
                        return (!isPremium || parseInt(value.data().boolSeries_charge) === isPremium) && (!isAffiliated || parseInt(value.data().boolSeries_affiliated) === isAffiliated);
                    });
                }
                this.disappear = function () {
                    currentIndex = 0;
                    return new Promise(function (resolve, reject) {
                        loadNewSet([]).then(function () {
                            domBlock.addClass('disappear');
                            setTimeout(function () {
                                domBlock.detach();
                                domBlock.removeClass('disappear');
                                resolve();
                            }, 500);
                        });
                    });
                }
                this.appear = function () {
                    domBlock.addClass('appear');
                    domBlock.appendTo(domRoot);
                    return new Promise(function (resolve, reject) {
                        loadNewSet(filteredItemHandles.slice(currentIndex, currentIndex + pageSize)).then(function () {
                            domBlock.removeClass('appear');
                            resolve();
                        })
                    });
                }
                this.emptyCards = function () {
                    currentIndex = 0;
                    return loadNewSet([]);
                }
                this.filteredItemHandles = function () {
                    return filteredItemHandles;
                }
                this.previous = function () {
                    if (currentIndex - pageSize < 0 || isAnimating){
                        return false;
                    }
                    currentIndex -= pageSize;
                    loadNewSet(filteredItemHandles.slice(currentIndex, currentIndex + pageSize));
                }
                this.next = function () {
                    if (currentIndex + pageSize >= filteredItemHandles.length || isAnimating){
                        return false;
                    }
                    currentIndex += pageSize;
                    loadNewSet(filteredItemHandles.slice(currentIndex, currentIndex + pageSize));
                }
                this.init = function () {
                    domBlock.find('.category-title').html(posts.strCategory_name);
                    self.setPageStatus(arrPageStatus[getScreenStatus()]);
                    posts.series.forEach(function (post) {
                        var itemHandle = new ItemClass(post);
                        totalItemHandles.push(itemHandle);
                    });
                    self.paymentFilter();
                    bindEvents();
                }
            }

            var bindEvents = function () {
                $('.site-wrapper.page').scroll(function (e) {
                    if ($('.site-wrapper.page').hasClass('header-search-opened')){
                        return true;
                    }
                    var elem = $(e.currentTarget);
                    if (elem[0].scrollHeight - elem.scrollTop() <= elem.outerHeight() + 10){
                        addPostsBlock();
                        addPostsBlock();
                        addPostsBlock();
                    }
                });
                $('.site-content .search-form').submit(function () {
                    var keyword = $(this).find('#search-term').val();
                    searchPostsBykeyword(keyword);
                    return false;
                });
                $('#sort-by').change(function () {
                    switchBlocksWith(postsBlockHandles);
                });
                $('[data-field="premium-filter"]').click(function () {
                    $(this).parents('.filter-item').toggleClass('active-item');
                    paymentFilter();
                });
                $('[data-field="affiliate-filter"]').click(function () {
                    $(this).parents('.filter-item').toggleClass('active-item');
                    paymentFilter();
                });
            }
            var switchBlocksWith = function (newBlockHandles) {
                var disappearPromises = [];
                postsBlockHandles.reverse();
                postsBlockHandles.forEach(function (postsBlockHandle) {
                    disappearPromises.push(postsBlockHandle.disappear());
                });
                postsBlockHandles = newBlockHandles;
                currentIndex = 0;
                sortBlockHandles($('#sort-by').val());
                Promise.all(disappearPromises).then(function () {
                    addPostsBlock();
                    addPostsBlock();
                });
            };

            var paymentFilter = function () {
                var disappearPromises = [];
                postsBlockHandles.reverse();
                postsBlockHandles.forEach(function (postsBlockHandle) {
                    disappearPromises.push(postsBlockHandle.disappear());
                    postsBlockHandle.paymentFilter();
                });
                currentIndex = 0;
                postsBlockHandles.reverse();
                Promise.all(disappearPromises).then(function () {
                    if ($('[data-field="affiliate-filter"]').parents('.filter-item').hasClass('active-item')) {
                        domRoot.addClass('active-affiliate');
                    }
                    else {
                        domRoot.removeClass('active-affiliate');
                    }
                    setTimeout(function () {
                        addPostsBlock();
                        addPostsBlock();
                    }, 500);
                });
            }
            var sortByAZ = function (a, b) {
                var title1 = a.title().toLowerCase();
                var title2 = b.title().toLowerCase();
                return title1 > title2 ? 1 : (title1 < title2 ? -1 : 0);
            }
            var sortByZA = function (a, b) {
                var title1 = a.title().toLowerCase();
                var title2 = b.title().toLowerCase();
                return title1 > title2 ? -1 : (title1 < title2 ? 1 : 0);
            }
            var sortBlockHandles = function (sortBy) {
                switch (sortBy){
                    case 'a-z':
                        postsBlockHandles.sort(sortByAZ);
                        break;
                    case 'z-a':
                        postsBlockHandles.sort(sortByZA);
                        break;
                    default:
                        break;
                }
                postsBlockHandles.forEach(function (postBlockHandle) {
                    postBlockHandle.sortItemsBy(sortBy);
                });
            };
            var makeHandles = function (newCategories) {
                var newBlockHandles = [];
                newCategories.forEach(function (category) {
                    var postsBlockHandle = new PostsBlockClass(category);
                    postsBlockHandle.init();
                    newBlockHandles.push(postsBlockHandle);
                });
                return newBlockHandles;
            };
            var searchPostsBykeyword = function (keyword) {
                if (typeof categoriesHistory[keyword] !== 'undefined'){
                    var newBlockHandles = makeHandles(categoriesHistory[keyword]);
                    switchBlocksWith(newBlockHandles);
                }
                else {
                    $.ajax({
                        url: ACTION_URL,
                        data: {action: 'search_categories_by_keyword', keyword: keyword},
                        success: function (res) {
                            if (res.status == true){
                                categories = res.data;
                                categoriesHistory[keyword] = categories;
                                var newBlockHandles = makeHandles(res.data);
                                setTimeout(function () {
                                    switchBlocksWith(newBlockHandles);
                                }, 300);
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
            }
            var addPostsBlock = function () {
                if (currentIndex >= postsBlockHandles.length){
                    return false;
                }
                var postsBlockHandle = postsBlockHandles[currentIndex];
                currentIndex ++;
                if (postsBlockHandle.filteredItemHandles().length === 0) {
                    return addPostsBlock();
                }
                postsBlockHandle.appear();
                return postsBlockHandle;
            }
            this.init = function () {
                categories = initCategories;
                categoriesHistory[''] = categories;
                postsBlockHandles = makeHandles(categories);
                switchBlocksWith(postsBlockHandles);
                bindEvents();
            }
        }
        var viewUserPage = function (data) {
            $('<form action="userpage" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
        }
        var popupThanksJoin = function (data) {
            swal({
                icon: "success",
                title: 'SUCCESS!',
                text: 'Thanks for joining! Would you like to go to your experiences?',
                buttons: {
                    returnHome: {
                        text: "Go to my Experiences",
                        value: 'return_home',
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    customize: {
                        text: "Not yet",
                        value: 'customize',
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                closeOnClickOutside: false,
            }).then(function (value) {
                if (value == 'return_home'){
                    viewUserPage(data);
                }
                else {
                }
            });
            var domSwalRoot, domDisableCheckbox;
            var disablePopup = function () {
                localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
            }
            var enabelPopup = function () {
                localStorage.removeItem('thegreyshirt_show_popup_every_join');
            }
            var bindEvents = function () {
                domDisableCheckbox.change(function () {
                    $(this).prop('checked') ? disablePopup() : enabelPopup();
                })
            }
            var init = function () {
                setTimeout(function () {
                    domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                    domSwalRoot.addClass('thanks-join');
                    domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                    domDisableCheckbox = domSwalRoot.find('#disable_popup');
                    bindEvents();
                }, 100);
            }();
        }
        var popupErroJoin = function () {
            swal("Sorry, Something went wrong!", {
                content: 'please try again later'
            });
        }
        this.init = function () {

            postsSectionHandle = new PostsSectionClass();
            postsSectionHandle.init();
            siteHeaderHandle.scrollFix({
                container: $('.site-wrapper.page'),
                domReplace: $('.scroll-fix-rplc').clone().removeAttr('hidden'),
            });
            if (isFirstVisit){
                setTimeout(function () {
                    swal({
                        title: "Welcome to the Walden.ly!",
                        text: "Please use the simplify page to find daily solutions that appeal to you.",
                        className: 'welcome'
                    });
                }, 500);
            }
        }
    }
    explorePageHandle = new ExplorePageClass();
    explorePageHandle.init();
})();

$(document).ajaxStart(function () {
    if(!is_loading_controlled_in_local) {
        $('.loading').css('display', 'block');
    }
});
$(document).ajaxComplete(function () {
    if(!is_loading_controlled_in_local){
        $('.loading').css('display', 'none');
    }
});