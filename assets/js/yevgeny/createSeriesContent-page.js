(function () {
    'use strict';

    var createSeriesContentHandle;
    var seriesTreeHandle;

    var CreateSeriesContentClass = function () {

        var switchOptionsHandle, createContentHandle, createMenuHandle, addFromSearchHandle, addFromBulk;

        var series, purchased;

        var openUserpage = function () {
            $('<form action="userpage" method="post" hidden><input name="purchasedId" value="'+ subscription.intClientSubscription_purchased_ID +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
        }
        var openView = function () {
            $('<form action="view" method="get" hidden><input name="id" value="'+ postId +'"><input name="prevpage" value="userpage"></form>').appendTo('body').submit().remove();
        }

        var SwitchOptionsClass = function () {
            var domRoot = $('#options-tab');
            var bindEvents = function () {
                domRoot.find('a[data-toggle="tab"][href="#create-new-content"]').click(function () {
                    type = $(this).data('type');
                });
            }
            this.init = function () {
                bindEvents();
            }
        }

        var goTo_editSeries = function () {
            $('<form action="editseries" method="post" hidden><input name="id" value="'+ series.series_ID +'"></form>').appendTo('body').submit().remove();
        }

        var CreateContentClass = function () {

            var domForm, domRoot, domDes, submitActor = 'allSet';

            var seriesTreeHandle, redirectTreeHandle, formFieldsHandle;

            var FormFieldsClass = function () {
                var domFormFields, domList;
                var fieldRowHandles = [], createFieldHandle;
                var bindEvents = function () {
                    domFormFields.find('.create-form').click(function () {
                        createFieldHandle.open();
                    })
                }
                var FieldRowClass = function (fieldData) {
                    var domRow;
                    var bindData = function () {
                        domRow.find('.field-name').html(fieldData.strFormField_name);
                    }
                    var bindEvents = function () {
                        domRow.find('.delete-post.action').click(function () {
                            swal({
                                title: "Are you sure?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete) {
                                    deleteRow();
                                }
                            });
                        });
                        domRow.find('.append-item.action').on('click', function () {
                            insertNode();
                        });
                        domRow.find('.append-answered-item.action').click(function () {
                            insertAnsweredNode();
                        });
                    }
                    var deleteRow = function () {
                        ajaxAPiHandle.apiPost('FormFields.php', {action: 'delete', where: fieldData.formField_ID}).then(function () {
                            domRow.remove();
                            fieldRowHandles.forEach(function (hdl, i) {
                                if (hdl.data().formField_ID === fieldData.formField_ID) {
                                    fieldRowHandles.splice(i, 1);
                                    return true;
                                }
                            });
                        });
                    }
                    var insertNode = function () {
                        var domNode = $('<div data-form-field="'+ fieldData.formField_ID +'" class="form-field-wrapper"></div>');
                        var domNodeInner = $('<div class="form-field-inner"></div>').appendTo(domNode);
                        domNodeInner.append('<label contenteditable="false" class="form-field-name">'+ fieldData.strFormField_name +':</label>');
                        switch (fieldData.strFormField_type) {
                            case 'text':
                                domNodeInner.append('<div class="form-field-value" contenteditable="true" placeholder="' + fieldData.strFormField_default + '"></div>');
                                break;
                            case 'checkbox':
                                domNodeInner.append('<div class="form-field-value" contenteditable="false"><input type="checkbox" ' + (parseInt(fieldData.strFormField_default) === 1 ? 'checked' : '') + ' /></div>');
                                break;
                        }
                        domNodeInner.append('<span class="form-field-helper" style="font-style: italic;" data-toggle="popover"  data-trigger="hover" contenteditable="false" data-content="'+ fieldData.strFormField_helper +'">i</span>');
                        insertNodeToBody(domNode.get(0));
                        domDes.on('summernote.change', function(e) {
                            if (domNode.find('.form-field-value').html() == '<br>') {
                                domNode.find('.form-field-value').empty();
                            }
                        });
                    }
                    var insertAnsweredNode = function () {
                        var domNode = domFormFields.find('.sample.answered-form-field-wrapper').find('.answered-form-field').clone();
                        domNode.attr('data-answered-form-field', fieldData.formField_ID);
                        var answer = false;
                        formFieldAnswers.forEach(function (formFieldAnswer) {
                            if (formFieldAnswer.intFormFieldAnswer_field_ID == fieldData.formField_ID) {
                                answer = formFieldAnswer;
                            }
                        });
                        fieldData.answer = answer ? answer.strFormFieldAnswer_answer : false;
                        console.log(fieldData);
                        var hdl = new AnsweredFormFieldClass(domNode, fieldData);
                        hdl.init();
                        insertNodeToBody(domNode.get(0));
                        domDes.on('summernote.change', function(e) {
                            if (domNode.find('.form-field-value').html() == '<br>') {
                                domNode.find('.form-field-value').empty();
                            }
                        });
                    }
                    this.data = function () {
                        return fieldData;
                    }
                    this.init = function () {
                        domRow = domList.find('li.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domList);
                        bindData();
                        bindEvents();
                    }
                }
                var CreateFieldClass = function (modalRootHandle, domContent) {
                    var domForm;
                    var bindEvents = function () {
                        domContent.find('select[name=strFormField_type]').change(function () {
                            switch ($(this).val()) {
                                case 'text':
                                    domContent.find('.text-type').show();
                                    domContent.find('.checkbox-type').hide();
                                    break;
                                case 'checkbox':
                                    domContent.find('.checkbox-type').show();
                                    domContent.find('.text-type').hide();
                                    break;
                            }
                        });
                        domForm.submit(function () {
                            createField().then(function (rowData) {

                                addField(rowData);
                                domForm[0].reset();
                                domContent.find('select[name=strFormField_type]').change();
                            });
                            return false;
                        });
                        domContent.find('.create-btn').click(function () {
                            createField().then(function (rowData) {
                                addField(rowData);
                                domForm[0].reset();
                                domContent.find('select[name=strFormField_type]').change();
                                modalRootHandle.cancelClose();
                            });
                        })
                    }
                    var createField = function () {
                        var sets = helperHandle.formSerialize(domForm);
                        sets.intFormField_series_ID = initialSeries.series_ID;
                        switch (domForm.find('select[name=strFormField_type]').val()) {
                            case 'text':
                                sets.strFormField_default = domForm.find('.text-type input').val();
                                break;
                            case 'checkbox':
                                sets.strFormField_default = domForm.find('.checkbox-type input').prop('checked') ? 1 : 0;
                                break;
                        }
                        return ajaxAPiHandle.apiPost('FormFields.php', {action: 'insert', sets: sets}).then(function (res) {
                            return $.extend(sets, {formField_ID: res.data});
                        });
                    }
                    this.init = function () {
                        domContent.find('#form-field-default').attr('id', 'default-value');
                        domForm = domContent.find('form');
                        bindEvents();
                    }
                }
                var addField = function (fieldData) {
                    var hdl = new FieldRowClass(fieldData);
                    hdl.init();
                    fieldRowHandles.push(hdl);
                }
                this.init = function () {
                    domFormFields = domForm.find('.form-fields');
                    domList = domFormFields.find('.fields-list');
                    initialFormFields.forEach(function (f) {
                        var hdl = new FieldRowClass(f);
                        hdl.init();
                        fieldRowHandles.push(hdl);
                    });
                    createFieldHandle = new SiteLightboxClass({
                        domLightboxMainContent: $('.modal-create-field.sample').clone().removeClass('sample').removeAttr('hidden'),
                        mainContentHdlClass: CreateFieldClass,
                        openDuration: 300,
                        closeDuration: 400,
                        lightboxType: 'cssMain',
                        openedCss: {
                            width: '1000px'
                        }
                    });
                    createFieldHandle.init();
                    bindEvents();
                }
            }

            var insertNodeToBody = function (domNode) {
                domDes.summernote('editor.restoreRange');
                domDes.summernote('editor.focus');
                domDes.summernote('insertNode', domNode);
                domDes.summernote('insertText', ' ');
                makeDescriptionValid();
                $('[data-toggle="popover"]').popover();
            }

            var clearForm = function () {
                domForm.find('input:text').val('');
                domForm.find('textarea[name="description"]').summernote('code', '');
                domForm.find('select[name="type"]').val(7);
                domForm.find('input[type="file"]').val('');
                domForm.find('.cover-image-wrapper .img-wrapper img').attr('src', 'assets/images/global-icons/cloud-uploading.png');
            };

            var bindEvents = function () {
                $('a[data-toggle="tab"][href="#create-new-content"]').on('shown.bs.tab', function (e) {
                    if (type) {
                        domForm.find('select[name="type"]').val(type).change();
                    }
                });
                domForm.submit(function () {
                    createContent().then(function (res) {
                        if (submitActor == 'createMore') {
                            clearForm();
                            seriesTreeHandle.refresh();
                        }
                        else {
                            goTo_editSeries();
                        }
                    });
                    return false;
                });
                domForm.find('select[name="type"]').change(function () {
                    switch (parseInt($(this).val())) {
                        case 7:
                            domForm.find('.form-field-col').show();
                            domForm.find('.redirect-item').hide();
                            break;
                        case 9:
                            domForm.find('.redirect-item').show();
                            domForm.find('.form-field-col').hide();
                            break;
                        case 10:
                            domForm.find('.redirect-item').hide();
                            domForm.find('.form-field-col').show();
                            break;
                        default:
                            domForm.find('.redirect-item').hide();
                            domForm.find('.form-field-col').hide();
                            break;
                    }
                });
                domForm.find('[type="submit"]').click(function () {
                    submitActor = this.name;
                });
                domForm.find('.cover-image-wrapper [name="image"]').change(function () {
                    updateCoverImage(this);
                });
                domDes.on('summernote.enter', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.focus', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.keydown', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.change', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.parent().find('.note-editable').click(function () {
                    domDes.summernote('editor.saveRange');
                });
                if (isSeriesMine) {
                    redirectTreeHandle.onChange(function (data) {
                        if (domForm.find('select[name="type"]').val() == 9) {
                            var selectedNode = redirectTreeHandle.getSelectedNode();
                            domForm.find('textarea[name="description"]').summernote('code', selectedNode.id);
                        }
                    });
                    redirectTreeHandle.dom().on('load_node.jstree', function () {
                        redirectTreeHandle.refHandle().disable_node('series_' + initialSeries.series_ID);
                    });
                }
            };

            var getFormData = function () {
                var data = helperHandle.formSerialize(domForm);
                var files = domForm.find('input[name="image"]').prop('files');
                var image = files.length ? files[0] : false;
                return {sets: data, image: image};
            }

            var createContent = function () {
                var formData = getFormData();
                if (isSeriesMine) {
                    var sets = postFormat(formData.sets);
                    //mark
                    var positionSets = seriesTreeHandle.getSelectedPositionData();
                    $.extend(sets, positionSets);

                    if (formData.image) {
                        var ajaxData = new FormData();
                        ajaxData.append('prefix', 'post');
                        ajaxData.append('file', formData.image);
                        return ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData).then(function (res) {
                            sets.strPost_featuredimage = res.data.url;
                            return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets});
                        });
                    }
                    else {
                        return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets});
                    }
                }
                else {
                    var sets = subscriptionFormat(formData.sets);
                    if (formData.image) {
                        var ajaxData = new FormData();
                        ajaxData.append('prefix', 'subscription');
                        ajaxData.append('file', formData.image);
                        return ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData).then(function (res) {
                            sets.strClientSubscription_image = res.data.url;
                            return ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: sets});
                        });
                    }
                    else {
                        return ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: sets});
                    }
                }
            }

            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domRoot).addClass('touched');
                        $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            var makeDescriptionValid = function () {
                var domEditor = domForm.find('.note-editor.note-frame .note-editable');
                var tpPreBr = false;
                domEditor.find('div>br, p>br').each(function (i, d) {
                    var p = $(d).parent();
                    if (p.html() === '<br>') {
                        if (tpPreBr) {
                            tpPreBr.remove();
                        }
                        tpPreBr = p;
                    }
                });
                domEditor.find('.form-field-wrapper').each(function (i, d) {
                    if ($(d).find('.form-field-name').length === 0) {
                        $(d).remove();
                    }
                });
            }

            this.init = function () {
                domRoot = $('#create-new-content');
                domForm = domRoot.find('form');
                domDes = domForm.find('textarea[name="description"]');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domDes.summernote({
                    height: 250,
                    tabsize: 2,
                });
                if (isSeriesMine) {
                    seriesTreeHandle = new SeriesTreeClass(initialSeries.series_ID, domRoot.find('.insert-position .series-tree'));
                    seriesTreeHandle.init();
                    redirectTreeHandle = new SeriesTreeClass(initialSeries.series_ID, domRoot.find('.redirect-item .series-tree'));
                    redirectTreeHandle.init();
                }
                formFieldsHandle = new FormFieldsClass();
                formFieldsHandle.init();
                bindEvents();
                var initialType = domForm.find('select[name="type"]').val();
                switch (parseInt(initialType)) {
                    case 7:
                        domForm.find('.form-field-col').show();
                        domForm.find('.redirect-item').hide();
                        break;
                    case 9:
                        domForm.find('.redirect-item').show();
                        domForm.find('.form-field-col').hide();
                        break;
                    case 10:
                        domForm.find('.redirect-item').hide();
                        domForm.find('.form-field-col').show();
                        break;
                    default:
                        domForm.find('.redirect-item').hide();
                        domForm.find('.form-field-col').hide();
                        break;
                }
            }
        }

        var CreateMenuClass = function () {
            var domRoot, domPathContainer;
            var menuHandle;
            var pathHandles = [], pathCnt;
            var createdMenu;

            var bindEvents = function () {
                domRoot.find('[name="allSet"]').click(function () {
                    createMenu();
                });
            }

            var createMenu = function () {
                var pathPromises = [];
                menuHandle.create().then(function (menuData) {
                    pathHandles.forEach(function (hdl) {
                        var prms = hdl.create(menuData.post_ID);
                        pathPromises.push(prms);
                    });
                    Promise.all(pathPromises).then(function () {
                        // seriesTreeHandle.refresh();
                        goTo_editSeries();
                    });
                });
            }

            var MenuClass = function () {
                var domMenu;

                var bindEvents = function () {
                    domMenu.find('select[name="path_count"]').change(function () {
                        var newCnt = $(this).val();
                        newCnt = parseInt(newCnt);
                        if (newCnt > pathCnt) {
                            for(var i = pathCnt; i < newCnt; i ++){
                                var hdl = new PathClass();
                                hdl.init();
                                pathHandles.push(hdl);
                            }
                        }
                        else if (newCnt < pathCnt) {
                            for(var i = newCnt; i < pathCnt; i ++){
                                var hdl = pathHandles[i];
                                hdl.remove();
                            }
                            pathHandles.splice(newCnt);
                        }
                        pathCnt = newCnt;
                    });
                    domMenu.submit(function () {
                        return false;
                    });
                }

                this.create = function () {
                    var positionData = seriesTreeHandle.getSelectedPositionData();
                    var sets = helperHandle.formSerialize(domMenu);
                    sets.strPost_nodeType = 'menu';
                    delete sets.path_count;
                    delete sets._wysihtml5_mode;
                    console.log(positionData);
                    $.extend(sets, positionData);
                    console.log(sets);
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}).then(function (res) {
                        createdMenu = res.data;
                        return createdMenu;
                    });
                }
                this.init = function () {
                    domMenu = domRoot.find('#menu-form');
                    domMenu.find('textarea[name="strPost_body"]').summernote({
                        height: 250,
                        tabsize: 2
                    });
                    pathCnt = domMenu.find('select[name="path_count"]').val();
                    pathCnt = parseInt(pathCnt);
                    for (var i = 0; i < pathCnt; i ++){
                        var hdl = new PathClass();
                        hdl.init();
                        pathHandles.push(hdl);
                    }
                    bindEvents();
                }
            }

            var PathClass = function () {
                var domPath;

                var pathNumber = 1;

                var bindEvents = function () {
                    domPath.find('.path-form').submit(function () {
                        return false;
                    });
                    domPath.find('.cover-image-wrapper [name="image"]').change(function () {
                        updateCoverImage(this);
                    });
                }
                var updateCoverImage = function(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.cover-image-wrapper .img-wrapper img', domPath).addClass('touched');
                            $('.cover-image-wrapper .img-wrapper img', domPath).attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                this.create = function (parentId) {
                    var sets = helperHandle.formSerialize(domPath.find('form'));
                    sets.strPost_nodeType = 'path';
                    sets.intPost_parent = parentId;
                    sets.intPost_series_ID = initialSeries.series_ID;
                    sets.intPost_order = pathNumber;
                    var ajaxData = new FormData();

                    ajaxData.append('action', 'insert');
                    ajaxData.append('sets', JSON.stringify(sets));

                    var files = domPath.find('input[name="image"]').prop('files');
                    var image = files.length ? files[0] : false;
                    if (image) {
                        ajaxData.append('strPost_featuredimage', image);
                    }

                    return ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData);
                }
                this.remove = function () {
                    domPath.remove();
                };
                this.init = function () {
                    domPath = domRoot.find('.paths-wrapper .path-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domPathContainer);
                    pathNumber = pathHandles.length + 1;
                    domPath.find('.path-no').html(pathNumber);
                    bindEvents();
                };
            }

            this.init = function () {
                domRoot = $('#create-new-menu');
                domPathContainer = domRoot.find('.path-list');
                menuHandle = new MenuClass();
                menuHandle.init();
                bindEvents();
            }
        }

        var AddFromBulkUpload = function () {

            var domRoot;
            var domFilesList, domActionsContainer;

            var viewWidgetHandle;

            var key = 1;
            var fileRowHandles = [];
            var seriesId = initialSeries.series_ID;
            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent'
            };

            var postFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (postFields[k]) {
                        fData[postFields[k]] = data[k];
                    }
                }
                return fData;
            }

            var postTypeArr = {
                'video': 2,
                'audio': 0,
                'image': 8,
                'text': 7,
            }

            var FileRowClass = function (file) {

                var domRow, domActions;
                var postData = {};

                var self = this;
                var keyI = key++;

                var bindData = function () {
                    postData.title = file.name;
                    postData.strType = helperHandle.extractTypeFromFileType(file.type);
                    postData.type = postTypeArr[postData.strType];
                    postData.fullType = file.type;
                    var dType = file.type;
                    if (file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        dType = 'application/docx';
                    } else if (file.type === 'application/msword') {
                        dType = 'application/doc';
                    } else if (file.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                        dType = 'application/ppt';
                    }

                    domRow.find('.file-title').attr('placeholder', postData.title);
                    domRow.find('.file-type').html(dType);
                    domRow.find('.file-thumb').addClass('data-type-' + postData.strType);

                    switch (postData.strType) {
                        case 'video':
                            domActions.find('.preview').find('.glyphicon').addClass('glyphicon-facetime-video');
                            break;
                        case 'music':
                            domActions.find('.preview').find('.glyphicon').addClass('glyphicon-music');
                            break;
                        case 'image':
                            domActions.find('.preview').find('.glyphicon').addClass('glyphicon-picture');
                            break;
                        case 'application':
                        case 'text':
                            fileToText();
                            domActions.find('.preview').find('.glyphicon').addClass('glyphicon-font');
                            break;
                    }
                }
                var fileToText = function () {
                    var ajaxData = new FormData();
                    ajaxData.append('file', file);
                    return ajaxAPiHandle.multiPartApiPost('FileToHtml.php', ajaxData).then(function (res) {
                        postData.body = res.data;
                        return res.data;
                    });
                }
                var updateCoverImage = function(input) {
                    if (input.files && input.files[0]) {
                        postData.imageFile = input.files[0];
                        if (helperHandle.extractTypeFromFileType(postData.imageFile.type) != 'image') {
                            swal("Not Image!", "Sorry, This is not image file, Please select image file.").then(function () {
                            });
                            return false;
                        }
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            domRow.find('img.file-image').attr('src', e.target.result);
                            domRow.find('.file-image-wrapper').css('opacity', 1);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                var bindEvents = function () {
                    domRow.find('.image-input').change(function () {
                        updateCoverImage(this);
                    });
                    domActions.find('.start').click(function () {
                        self.upload();
                    });
                    domRow.find('input.file-title').change(function () {
                        var title = $(this).val();
                        postData.title = title ? title : $(this).attr('placeholder');
                    });
                    domActions.find('.delete').click(function () {
                        self.delete();
                    });
                    domActions.find('.cancel').click(function () {
                        self.cancel();
                    });
                    domActions.find('.preview').click(function () {
                        self.preview();
                    });
                }
                this.preview = function () {
                    var wData = postData;
                    if (!wData.body) {
                        wData.body = window.URL.createObjectURL(file);
                    }
                    viewWidgetHandle.view(wData);
                }
                this.key = function () {
                    return keyI;
                }
                this.isUploaded = function () {
                    return postData.id ? true : false;
                }
                this.delete = function () {
                    if (Array.isArray(postData.id)) {
                        postData.id.forEach(function (id) {
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: id}).then(function (res) {
                            });
                        });
                        self.cancel();
                    }
                    else {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: postData.id}).then(function (res) {
                            self.cancel();
                        });
                    }
                }
                this.cancel = function () {
                    fileRowHandles.forEach(function (hdl, i) {
                        if (hdl.key() === self.key()) {
                            fileRowHandles.splice(i, 1);
                            domRow.remove();
                            filterActionsAvailable();
                        }
                    });
                }
                this.upload = function () {

                    var resPromise;

                    if (postData.strType === 'text' || postData.strType === 'application') {
                        resPromise = Promise.resolve();
                    }
                    else {
                        var ajaxData = new FormData();
                        ajaxData.append('file', file);
                        ajaxData.append('sets', JSON.stringify({where: 'assets/media/' + postData.strType, prefix: 'post'}));
                        resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                            postData.body = res.data.abs_url;
                            return true;
                        });
                    }
                    resPromise.then(function () {
                        if (Array.isArray(postData.body)) {
                            postData.id = [];
                            postData.body.forEach(function (body, i) {
                                var ajaxPost = new FormData();
                                ajaxPost.append('action', 'insert');
                                var postFData = postFormat($.extend({}, postData, {body: body, title: postData.title + '('+ (i + 1) +')'}));
                                postFData.intPost_series_ID = seriesId;
                                delete postFData.post_ID;
                                ajaxPost.append('sets', JSON.stringify(postFData));
                                if (postData.imageFile) {
                                    ajaxPost.append('strPost_featuredimage', postData.imageFile);
                                }
                                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxPost).then(function (res) {
                                    postData.id.push(res.data.post_ID);
                                    domRow.addClass('uploaded');
                                    filterActionsAvailable();
                                });
                            });
                        }
                        else {
                            var ajaxPost = new FormData();
                            ajaxPost.append('action', 'insert');
                            var postFData = postFormat(postData);
                            postFData.intPost_series_ID = seriesId;
                            ajaxPost.append('sets', JSON.stringify(postFData));
                            if (postData.imageFile) {
                                ajaxPost.append('strPost_featuredimage', postData.imageFile);
                            }
                            ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxPost).then(function (res) {
                                postData.id = res.data.post_ID;
                                domRow.addClass('uploaded');
                                filterActionsAvailable();
                            });
                        }
                    });
                }
                this.init = function () {
                    domRow = domFilesList.find('li.sample.file-row').clone().removeAttr('hidden').removeClass('sample').appendTo(domFilesList);
                    domActions = domRow.find('.actions');
                    bindData();
                    bindEvents();
                }
            }
            var filterActionsAvailable = function () {
                var cntToUpload = 0;
                fileRowHandles.forEach(function (hdl) {
                    cntToUpload += hdl.isUploaded() ? 0 : 1;
                });
                if (cntToUpload) {
                    domActionsContainer.find('.start').removeClass('disabled');
                }
                else {
                    domActionsContainer.find('.start').addClass('disabled');
                }
                if (fileRowHandles.length) {
                    domActionsContainer.find('.cancel').removeClass('disabled');
                }
                else {
                    domActionsContainer.find('.cancel').addClass('disabled');
                }
            }
            var bindEvents = function () {
                domActionsContainer.find('.action.add-new-files input').change(function () {
                    var files = $(this).prop('files');
                    for (var i = 0; i < files.length; i ++) {
                        var hdl = new FileRowClass(files[i]);
                        hdl.init();
                        fileRowHandles.push(hdl);
                    }
                    $(this).val('');
                    filterActionsAvailable();
                });
                domActionsContainer.find('.start').click(function () {
                    if ($(this).hasClass('disabled')) {
                        return ;
                    }
                    fileRowHandles.forEach(function (hdl) {
                        hdl.upload();
                    });
                });
                domActionsContainer.find('.cancel').click(function () {
                    if ($(this).hasClass('disabled')) {
                        return ;
                    }
                    var len = fileRowHandles.length;
                    for (var i = 0; i < len; i++) {
                        fileRowHandles[0].cancel();
                    }
                });
                domRoot.find('.return-btn').click(function () {
                    goTo_editSeries();
                });
            }
            var ViewWidgetClass = function () {
                var domViewWidget, domUsedBody, domHeader, domBodyWrp, domFooter, domTopCtrl;
                var postData, metaData;
                var audioCnt = 1, videoCnt = 1, afterGlowReady;
                var videoHandle = null;
                var bindEvents = function () {
                    domHeader.find('.mv_min_control_max, .post-title').parent().click(function () {
                        maximize();
                    });
                    domHeader.find('.mv_min_control_close').parent().click(function () {
                        close();
                    });
                    domTopCtrl.find('.mv_minimize_icon').parent().click(function () {
                        minimize();
                    });
                    domTopCtrl.find('.mv_close_icon').parent().click(function () {
                        close();
                    });
                    domViewWidget.click(function (e) {
                        if (e.target !== this) return;
                        close();
                    });
                };

                var close = function () {
                    destroyBody();
                    domViewWidget.removeClass('active');
                    $('body').css('padding-right', '0px');
                    $('body').css('overflow', 'auto');
                }

                var destroyBody = function () {
                    if (!domUsedBody || !domUsedBody.length) {
                        return false;
                    }
                    switch (parseInt(postData.type)) {
                        case 0:
                            domViewWidget.removeClass('audio-type');
                            break;
                        case 2:
                            domViewWidget.removeClass('video-type');
                            videoHandle.dispose();
                            break;
                        case 8:
                            domViewWidget.removeClass('image-type');
                            break;
                        default:
                            domViewWidget.removeClass('text-type');
                            break;
                    }
                    domUsedBody.remove();
                    return true;
                }

                var maximize = function () {
                    domViewWidget.addClass('maximize-view');
                    $('body').css('padding-right', window.innerWidth - $("body").prop("clientWidth") + 'px');
                    $('body').css('overflow', 'hidden');
                }

                var minimize = function () {
                    $('body').css('overflow', 'auto');
                    $('body').css('padding-right', '0px');
                    domViewWidget.removeClass('maximize-view');
                }

                this.view = function (newPostData) {
                    domViewWidget.addClass('active');
                    postData = newPostData;
                    domHeader.find('.post-title').html(postData.title);
                    domFooter.find('.post-title').html(postData.title);
                    if (domViewWidget.hasClass('maximize-view')){
                        maximize();
                    }
                    switch (parseInt(postData.type)) {
                        case 0:
                            domViewWidget.addClass('audio-type');
                            domUsedBody = domViewWidget.find('.audio-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                            domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                            domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);

                            var settings_ap = {
                                disable_volume: 'off',
                                disable_scrub: 'default',
                                design_skin: 'skin-wave',
                                skinwave_dynamicwaves: 'on',
                                skinwave_mode: 'alternate',
                            };
                            dzsag_init('#audio-' + audioCnt, {
                                'transition':'fade',
                                'autoplay' : 'on',
                                'settings_ap': settings_ap
                            });
                            audioCnt++;
                            break;
                        case 2:
                            domViewWidget.addClass('video-type');
                            domUsedBody = domViewWidget.find('.video-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            metaData.videoId = 'video_' + videoCnt;
                            var domVideo = domUsedBody.find('video');
                            domVideo.append('<source src="'+ postData.body +'" type="video/mp4"/>');
                            videoHandle = videojs(domVideo.get(0), {
                                height: domUsedBody.innerWidth() * 540 / 960,
                            });
                            break;
                        case 8:
                            domViewWidget.addClass('image-type');
                            domUsedBody = domViewWidget.find('.image-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.append(postData.body);
                            break;
                        case 10:
                            domViewWidget.addClass('form-type');
                            domUsedBody = domViewWidget.find('.form-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });
                            break;
                        default:
                            domViewWidget.addClass('text-type');
                            domUsedBody = domViewWidget.find('.text-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            if (Array.isArray(postData.body)) {
                                domUsedBody.append(postData.body.join('<br><br>'));
                            }
                            else {
                                domUsedBody.append(postData.body);
                            }
                            break;
                    }
                }
                this.init = function () {
                    domViewWidget = $('.view-widget');
                    domHeader = domViewWidget.find('header.widget-header');
                    domBodyWrp = domViewWidget.find('.widget-body');
                    domFooter = domViewWidget.find('footer.widget-footer');
                    domTopCtrl = domViewWidget.find('aside.widget-top-control');
                    metaData = {};
                    afterGlowReady = Promise.resolve();
                    bindEvents();
                }
            }
            this.init = function () {
                domRoot = $('#create-from-bulkupload main.main-content');
                domFilesList = domRoot.find('ul.files-list');
                domActionsContainer = domRoot.find('.actions-container');
                filterActionsAvailable();
                viewWidgetHandle = new ViewWidgetClass();
                viewWidgetHandle.init();

                bindEvents();
            }
        }

        var postFormat = function (data) {
            return {
                intPost_series_ID: series.series_ID,
                strPost_title: data.title,
                strPost_body: data.description,
                strPost_featuredimage: data.image,
                intPost_type: data.type,
                strPost_keywords: data.keywords,
                intPost_savedMoney: data.saved,
            };
        }
        var subscriptionFormat = function (data) {
            return {
                intClientSubscription_purchased_ID: purchased.purchased_ID,
                strClientSubscription_title: data.title,
                strClientSubscription_body: data.description,
                strClientSubscription_image: data.image,
                intClientSubscriptions_type: data.type,
            };
        }

        var bindEvents = function () {
            $('a[data-toggle="tab"][href="#add-from-search"]').on('shown.bs.tab', function (e) {
                setTimeout(function () {
                    addFromSearchHandle.setSearchTerm('');
                }, 300);
            });
            addFromSearchHandle.dom().find('.back-to-main').click(function () {
                goTo_editSeries();
            });
        }

        this.init = function () {

            series = initialSeries;
            purchased = initialPurchased;

            switchOptionsHandle = new SwitchOptionsClass();
            switchOptionsHandle.init();

            createContentHandle = new CreateContentClass();
            createContentHandle.init();

            createMenuHandle = new CreateMenuClass();
            createMenuHandle.init();

            addFromSearchHandle = new SeriesSearchPostsClass($('.component.series-search-posts'), {isSeriesMine: isSeriesMine, series: initialSeries, purchased: initialPurchased});
            addFromSearchHandle.init();

            addFromSearchHandle.dom().find('.back-to-main').html('Finish');

            addFromBulk = new AddFromBulkUpload();
            addFromBulk.init();

            bindEvents();
        }
    }

    seriesTreeHandle = new SeriesTreeClass(initialSeries.series_ID);
    seriesTreeHandle.init();

    createSeriesContentHandle = new CreateSeriesContentClass();
    createSeriesContentHandle.init();
})();