var SiteLightboxClass = null;
var ajaxAPiHandle = null;
var helperHandle = null;
var SeriesTreeClass = null;
var API_ROOT_URL = BASE_URL + '/yevgeny/api';
var siteLoadingHandle = null;

(function () {
    SiteLightboxClass = function (options) {
        // var domRoot = $('.site-lightbox.sample').clone().removeClass('sample');
        var domRoot;
        var domContentWrapper;
        var domFrom, domOpeningFrom, domClosingTo, domContent;

        var self = this, mainHandle;
        var defaults = {
            openDuration: 600,
            closeDuration: 400,
            cancelCloseDuration: 200,
            openEasing: 'swing',
            closeEasing: 'swing',
            isFlexWidth: false,
            closedCss: {
                opacity: 0.3,
                paddingTop: '0px',
                paddingBottom: '0px',
                paddingLeft: '0px',
                paddingRight: '0px',
            },
            openedCss: {
                top: '50px',
                left: '200px',
                width: window.outerWidth - 200 * 2,
                opacity: 1,
                paddingTop: '20px',
                paddingBottom: '20px',
                paddingLeft: '20px',
                paddingRight: '20px',
            },
            domFrom: false,
            openType: 'from_dom',
            closeType: 'to_dom',
            cancelCloseType: 'to_top',
            lightboxType: 'jsMain'
        }
        var isOpened = false;
        var arrCssByScreen = function () {
            var fullWidth = window.outerWidth;
            return [
                {left: '200px', width: fullWidth - 200 * 2},
                {left: '100px', width: fullWidth - 100 * 2},
                {left: '70px', width: fullWidth - 70 * 2},
                {left: '50px', width: fullWidth - 50 * 2},
                {left: '30px', width: fullWidth - 30 * 2},
                {left: '10px', width: fullWidth - 10 * 2},
            ];
        }
        var getScreenStatus = function () {
            var w = window.outerWidth;
            if (w > 1300){
                return 0;
            }
            else if (w > 1100){
                return 1;
            }
            else if (w > 1000){
                return 2;
            }
            else if (w > 850){
                return 3;
            }
            else if (w > 668){
                return 4;
            }
            else {
                return 5;
            }
        }

        var callbacksOnOpend = [], callbacksOnClosed = [], callbacksBeforeClose = [];

        this.openedCss = function () {
            switch (options.lightboxType){
                case 'jsMain':
                    var cloneDom;
                    if (!isOpened){
                        cloneDom = domRoot.clone();
                        var toCss = $.extend({}, options.openedCss, arrCssByScreen()[getScreenStatus()]);
                        cloneDom.appendTo('body').addClass('opened').find('.lightbox-content-wrapper').css($.extend({}, toCss, {left: '-3000px', width: toCss.width}));
                    }
                    else {
                        cloneDom = domRoot;
                    }
                    var tpWidth = cloneDom.find('.lightbox-content-wrapper').css('width');
                    cloneDom.find('.lightbox-content-wrapper').find('.lightbox-content').css('display', 'inline-block');
                    var tpInnerWidth = cloneDom.find('.lightbox-content-wrapper').find('.lightbox-content').css('width');
                    tpInnerWidth = (parseInt(tpInnerWidth) + parseInt(options.openedCss.paddingLeft) + parseInt(options.openedCss.paddingRight)) + 'px';
                    var tpHeight = cloneDom.find('.lightbox-content-wrapper').css('height');
                    var openedCss = {};
                    if (options.isFlexWidth){
                        openedCss = {
                            width: tpInnerWidth,
                            height: tpHeight,
                            left: ((cloneDom.outerWidth() - parseInt(tpInnerWidth)) / 2) + 'px',
                        }
                    }
                    else {
                        openedCss = {
                            width: tpWidth,
                            height: tpHeight
                        }
                    }
                    if (!isOpened){
                        cloneDom.remove();
                    }
                    return openedCss;
                    break;
                case 'cssMain':
                    var cloneDom;
                    if (!isOpened){
                        cloneDom = domRoot.clone();
                        cloneDom = cloneDom.addClass('opened').appendTo('body');
                    }
                    else {
                        cloneDom = domRoot;
                    }
                    var w = cloneDom.find('.lightbox-content-wrapper').css('width');
                    var h = cloneDom.find('.lightbox-content-wrapper').css('height');
                    if (!isOpened){
                        cloneDom.remove();
                    }
                    return {
                        width: w,
                        height: h
                    };
                    break;
            }
        }

        this.onOpened = function (callback) {
            callbacksOnOpend.push(callback);
        }
        this.beforeClose = function(callback) {
            callbacksBeforeClose.push(callback);
        }
        this.onClosed = function (callback) {
            callbacksOnClosed.push(callback);
        }
        this.open = function () {
            switch (options.lightboxType){
                case 'jsMain':
                    var openedCss = self.openedCss();
                    return new Promise(function (resolve, reject) {

                        options.openedCss.height = options.openedCss.height ? options.openedCss.height : openedCss.height;

                        if (options.isFlexWidth){
                            options.openedCss.left = openedCss.left;
                            options.openedCss.width = openedCss.width;
                        }

                        $('body').css('padding-right', window.outerWidth - $("body").prop("clientWidth") + 'px');
                        $('body').addClass('site-lightbox-open');
                        domRoot.appendTo('body');
                        domRoot.addClass('openning');
                        var pos = domFrom.offset();

                        pos.top = pos.top - $(window).scrollTop();
                        var width = domOpeningFrom.outerWidth();

                        var fromCss = {};

                        fromCss.top = pos.top;
                        fromCss.left = pos.left;
                        fromCss.width = width;
                        fromCss.height = domOpeningFrom.outerHeight();

                        fromCss = $.extend({}, fromCss, options.closedCss);

                        var toCss;
                        if (options.isFlexWidth){
                            toCss = $.extend({}, options.openedCss);
                        }
                        else {
                            toCss = $.extend({}, options.openedCss, arrCssByScreen()[getScreenStatus()]);
                        }

                        switch (options.openType){
                            case 'from_dom':
                                break;
                            case 'from_top':
                                fromCss = $.extend({}, toCss, {opacity: options.closedCss.opacity, top: '-20px'});
                                domRoot.addClass('from-top');
                                break;
                            case 'from_center':
                                break;
                        }

                        domContentWrapper.css(fromCss);

                        domContentWrapper.animate(toCss, {
                            duration: options.openDuration,
                            easing: options.openEasing,
                            complete: function () {
                                domRoot.removeClass('openning');
                                domRoot.addClass('opened');
                                domRoot.removeClass('from-top');
                                if (options.onOpened){
                                    options.onOpened();
                                }
                                callbacksOnOpend.forEach(function (callback) {
                                    callback();
                                });
                                isOpened = true;
                                resolve();
                            }
                        })
                    });
                    break;
                case 'cssMain':
                    return new Promise(function (resolve, reject) {
                        $('body').css('padding-right', window.outerWidth - $("body").prop("clientWidth") + 'px');
                        $('body').addClass('site-lightbox-open');
                        domRoot.appendTo('body');
                        domRoot.addClass('openning');
                        domContentWrapper.one('animationend', function () {
                            domRoot.addClass('opened');
                            domRoot.removeClass('openning');
                            if (options.onOpened){
                                options.onOpened();
                            }
                            callbacksOnOpend.forEach(function (callback) {
                                callback();
                            });
                            isOpened = true;
                            resolve();
                        });
                    });
                    break;
            }
        }

        this.close = function () {
            var pos = domClosingTo.offset();
            pos.top = pos.top - $(window).scrollTop();
            pos.right = $(window).width() - pos.left - domClosingTo.outerWidth();

            var height = domClosingTo.outerHeight();
            var toCss = $.extend({height: height}, pos, options.closedCss);

            domRoot.addClass('closing');
            domRoot.removeClass('opened');
            return new Promise(function (resolve, reject) {
                domContentWrapper.animate(toCss, {
                    duration: options.closeDuration,
                    easy: options.closeEasing,
                    complete: function () {
                        $('body').removeClass('site-lightbox-open');
                        $('body').css('padding-right', '0px');
                        domRoot.removeClass('closing');
                        domRoot.detach();
                        if (options.onClosed){
                            options.onClosed();
                        }
                        resolve();
                        isOpened = false;
                    }
                })
            })
        }
        this.cancelClose = function () {
            var toCss = {
                top: '-10px',
                opacity: 0
            }
            switch (options.lightboxType){
                case 'jsMain':
                    domRoot.addClass('cancel-closing');
                    domRoot.removeClass('opened');
                    return new Promise(function (resolve, reject) {
                        domContentWrapper.animate(toCss, {
                            duration: options.cancelCloseDuration,
                            easy: options.closeEasing,
                            complete: function () {
                                $('body').css('padding-right', '0px');
                                $('body').removeClass('site-lightbox-open');
                                domRoot.removeClass('cancel-closing');
                                domRoot.detach();
                                if (options.onClosed){
                                    options.onClosed();
                                }
                                callbacksOnClosed.forEach(function (callback) {
                                    callback();
                                });
                                isOpened = false;
                                resolve();
                            }
                        })
                    })
                    break;
                case 'cssMain':
                    domRoot.addClass('closing');
                    domRoot.removeClass('opened');
                    return new Promise(function (resolve, reject) {
                        callbacksBeforeClose.forEach(function (callback) {
                            callback();
                        });
                        domContentWrapper.one('animationend', function () {
                            $('body').css('padding-right', '0px');
                            $('body').removeClass('site-lightbox-open');
                            domRoot.removeClass('closing');
                            domRoot.detach();
                            if (options.onClosed){
                                options.onClosed();
                            }
                            callbacksOnClosed.forEach(function (callback) {
                                callback();
                            });
                            isOpened = false;
                            resolve();
                        });
                    });
                    break;
            }
        }
        var bindEvents = function () {
            domContentWrapper.find('.close-lightbox, .cancel-close').click(function () {
                self.cancelClose();
            });
            domRoot.click(function (e) {
                if (e.target === this) {
                    self.cancelClose();
                }
            })
        }
        this.init = function () {

            options = $.extend({}, defaults, options);

            domRoot = $('<div class="site-lightbox '+ options.lightboxType +'">\n' +
                '    <div class="lightbox-content-wrapper">\n' +
                '        <aside class="close-lightbox">&times;</aside>\n' +
                '        <div class="lightbox-content"></div>\n' +
                '    </div></div>');
            domContentWrapper = domRoot.find('.lightbox-content-wrapper');
            domContent = domContentWrapper.find('.lightbox-content');
            domContent.append(options.domLightboxMainContent);

            switch (options.lightboxType){
                case 'jsMain':
                    domFrom = options.domFrom;
                    domOpeningFrom = options.domOpeningFrom ? options.domOpeningFrom : domFrom;
                    domClosingTo = options.domClosingTo ? options.domClosingTo : domFrom;
                    break;
                case 'cssMain':
                    options.isFlexWidth ? domRoot.addClass('flex-width'): false;
                    if (!options.isFlexWidth){
                        domContentWrapper.css('width', options.openedCss.width);
                    }
                    break;
            }

            if (options.mainContentHdlClass){
                mainHandle = new options.mainContentHdlClass(self, domContent);
                mainHandle.init();
            }

            bindEvents();
        }
    }
    var AjaxApiClass = function () {

        var self = this;
        var loadingCnt = 0;

        this.post = function (url, data, showLoading, loadingHideDelay) {

            if (typeof showLoading == 'undefined') {
                showLoading = true;
            }

            if (typeof loadingHideDelay == 'undefined') {
                loadingHideDelay = 0;
            }

            if (showLoading) {
                loadingCnt ++;
                siteLoadingHandle.show();
            }
            return $.ajax({
                url: url,
                type: 'post',
                data: data,
                dataType: 'json',
            }).then(function (res) {
                if (showLoading) {
                    loadingCnt--;
                    setTimeout(function () {
                        if (!loadingCnt) {
                            siteLoadingHandle.hide();
                        }
                    }, loadingHideDelay);
                    return res;
                }
                return res;
            });
        }
        this.multiPartPost = function (url, data, showLoading, loadingHideDelay, progressCallBack) {
            if (typeof showLoading == 'undefined') {
                showLoading = true;
            }
            if (typeof loadingHideDelay == 'undefined') {
                loadingHideDelay = 0;
            }
            if (showLoading) {
                loadingCnt++;
                siteLoadingHandle.show();
            }
            var progressHdl;
            if (typeof progressCallBack == 'undefined') {
                progressCallBack = function (evt) {
                    if (progressHdl) {
                        var p = evt.loaded * 100 / evt.total;
                        progressHdl.toPercent(p);
                    }
                }
            }
            return $.ajax({
                url: url,
                type: 'post',
                data: data,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                xhr: function() {
                    if (progressCallBack) {
                        var myXhr = $.ajaxSettings.xhr();
                        if(myXhr.upload){
                            progressHdl = siteLoadingHandle.addProgress();
                            myXhr.upload.addEventListener('progress', progressCallBack, false);
                        }
                    }
                    return myXhr;
                },
            }).then(function (res) {
                if (showLoading) {
                    loadingCnt--;
                    setTimeout(function () {
                        if (!loadingCnt) {
                            siteLoadingHandle.hide();
                        }
                    }, loadingHideDelay);
                    return res;
                }
                return res;
            });
        }

        this.apiPost = function (endPoint, data, showLoading, loadingHideDelay) {
            if (typeof showLoading == 'undefined') {
                showLoading = true;
            }
            if (typeof loadingHideDelay == 'undefined') {
                loadingHideDelay = 0;
            }
            var url = API_ROOT_URL + '/' + endPoint;
            return self.post(url, data, showLoading, loadingHideDelay);
        }
        this.multiPartApiPost = function (endPoint, data, showLoading, loadingHideDelay, progressCallBack) {
            if (typeof showLoading == 'undefined') {
                showLoading = true;
            }
            if (typeof loadingHideDelay == 'undefined') {
                loadingHideDelay = 0;
            }
            var url = API_ROOT_URL + '/' + endPoint;
            return self.multiPartPost(url, data, showLoading, loadingHideDelay, progressCallBack);
        }
        this.pagePost = function (endPoint, data, showLoading, loadingHideDelay) {
            var url = BASE_URL + '/' + endPoint;
            if (typeof showLoading == 'undefined') {
                showLoading = true;
            }
            if (typeof loadingHideDelay == 'undefined') {
                loadingHideDelay = 0;
            }
            return self.post(url, data, showLoading, loadingHideDelay);
        }
        this.multiPartPagePost = function(endPoint, data, showLoading, loadingHideDelay){
            var url = BASE_URL + '/' + endPoint;
            if (typeof showLoading == 'undefined') {
                showLoading = true;
            }
            if (typeof loadingHideDelay == 'undefined') {
                loadingHideDelay = 0;
            }
            return self.multiPartPost(url, data, showLoading, loadingHideDelay);
        }
    }
    ajaxAPiHandle = new AjaxApiClass();

    var HelperClass = function () {
        var self = this;

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            subtitle: 'strPost_subtitle',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            show_image: 'boolPost_show_image',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            duration: 'strPost_duration',
            keywords: 'strPost_keywords',
            free: 'boolPost_free',
        };

        var subscriptionFields = {
            id: 'clientsubscription_ID',
            title: 'strClientSubscription_title',
            subtitle: 'strClientSubscription_subtitle',
            body: 'strClientSubscription_body',
            image: 'strClientSubscription_image',
            show_image: 'boolClientSubscription_show_image',
            type: 'intClientSubscriptions_type',
            order: 'intClientSubscriptions_order',
            nodeType: 'strClientSubscription_nodeType',
            parent: 'intClientSubscriptions_parent',
            paths: 'paths',
            completed: 'boolCompleted',
            duration: 'strSubscription_duration',
            favorite: 'favorite',
            viewDay: 'viewDay',
        };

        this.valid_doc_types = ['application/pdf','text/plain'];
        this.valid_image_type = ['image/png', 'image/jpeg'];
        this.valid_video_type = ['video/mp4'];
        this.valid_audio_type = ['audio/mp3'];
        this.isMobile = function () {
            return window.innerWidth <= 768;
        }
        this.postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }
        this.subscriptionFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (subscriptionFields[k]) {
                    fData[subscriptionFields[k]] = data[k];
                }
            }
            return fData;
        }
        this.postToViewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                fk = helperHandle.keyOfVal(postFields, k);
                if (fk) { fData[fk] = data[k]; }
            }
            fData.origin = data;
            return fData;
        }
        this.subscriptionToViewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                fk = helperHandle.keyOfVal(subscriptionFields, k);
                if (fk) { fData[fk] = data[k]; }
            }
            fData.origin = data;
            return fData;
        }
        this.getApiTypeFromUrl = function (url) {
            if (url.match(/^(https:\/\/www.youtube.com)/)){
                return 'youtube';
            }
            var regex = RegExp("^(" + BASE_URL + ")");
            if (url.match(regex)) {
                return 'local';
            }
            return 'other';
        }
        this.parseYoutubeUrl = function (url) {
            var reg = RegExp("(embed/(?<video_id>[^/]+))$");
            var res = url.match(reg);
            if (res) {
                return {
                    type: 'embed',
                    id: res.groups.video_id
                }
            }

            reg = RegExp("(watch\\?v\\=(?<video_id>[^/&]+))");
            res = url.match(reg);
            if (res) {
                return {
                    type: 'watch',
                    id: res.groups.video_id
                }
            }
            return false;
        }
        this.makeYoutubeUrlById = function (id) {
            return 'https://www.youtube.com/watch?v=' + id;
        }
        this.makeSpreakerUrlById = function(id){
            return 'https://api.spreaker.com/v2/episodes/'+ id +'/play';
        }
        this.formSerialize = function (domForm) {
            var formArray = domForm.serializeArray();
            var returnArray = {};
            for (var i = 0; i < formArray.length; i++){
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            }
            return returnArray;
        }
        this.array_keys = function (arr) {
            var rtn = [];
            for (var k in arr) {
                rtn.push(k);
            }
            return rtn;
        }
        this.keyOfVal = function (arr, findVal) {
            for (var k in arr) {
                if (arr[k] === findVal) {
                    return k;
                }
            }
            return false;
        }
        this.extractTypeFromFileType = function (fType) {
            var ar = fType.split('/');
            return ar[0];
        }
        this.isAbsUrl = function (url) {
            return url.match(/^https?:\/\//);
        }
        this.makeAbsUrl = function (url) {
            return self.isAbsUrl(url) ? url : BASE_URL + '/' + url;
        }
        this.getVideoDurationPre = function (url) {
            var resolved = false;
            return new Promise(function (resolve) {
                var tpDom = $('<video class="video-js" autoplay height="100" width="100" style="position: fixed; left: -1000px; bottom: -1000px; opacity: 0;"></video>').appendTo('body');
                if (self.getApiTypeFromUrl(url) === 'youtube') {
                    var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": url}], "youtube": { "customVars": { "wmode": "transparent" } } };
                    tpDom.attr('data-setup', JSON.stringify(setup));
                }
                else {
                    tpDom.append('<source src="'+ url +'" />');
                }
                videojs(tpDom.get(0)).ready(function(){
                    var myPlayer = this;
                    if (self.getApiTypeFromUrl(url) === 'youtube') {
                        myPlayer.on('timeupdate', function () {
                            if (resolved) {
                                return;
                            }
                            var d = myPlayer.duration();
                            resolved = true;
                            resolve(d);
                            setTimeout(function () {
                                myPlayer.dispose();
                                setTimeout(function () {
                                    tpDom.remove();
                                }, 1000);
                            }, 100);
                        });
                    }
                    else {
                        myPlayer.on('loadeddata', function () {
                            if (resolved) {
                                return;
                            }
                            var d = myPlayer.duration();
                            resolved = true;
                            resolve(d);
                            setTimeout(function () {
                                myPlayer.dispose();
                                setTimeout(function () {
                                    tpDom.remove();
                                }, 1000);
                            }, 100);
                        });
                    }
                });
            });
        }
        this.getVideoDuration = function (url) {
            var resolved = false;
            if (self.getApiTypeFromUrl(url) === 'youtube') {
                return ajaxAPiHandle.apiPost('Api.php', {action: 'get', what: 'youtube', where: {id: self.parseYoutubeUrl(url).id}}, false).then(function (res) {
                    if (res.data && res.data.contentDetails && res.data.contentDetails.duration) {
                        var secs = self.secsFromYoutubeDuration(res.data.contentDetails.duration);
                        return secs;
                    }
                    else {
                        return false;
                    }
                });
            }
            else {
                var tpDom = $('<video class="video-js" autoplay height="100" width="100" style="position: fixed; left: -1000px; bottom: -1000px; opacity: 0;"></video>').appendTo('body');
                tpDom.append('<source src="'+ url +'" />');
                return new Promise(function (resolve) {
                    videojs(tpDom.get(0)).ready(function(){
                        var myPlayer = this;
                        myPlayer.on('loadeddata', function () {
                            if (resolved) {
                                return;
                            }
                            var d = myPlayer.duration();
                            resolved = true;
                            resolve(d);
                            setTimeout(function () {
                                myPlayer.dispose();
                                setTimeout(function () {
                                    tpDom.remove();
                                }, 1000);
                            }, 100);
                        });
                    });
                });
            }
        }
        this.getAudioDuration = function (url) {
            var tpDom = $('<audio src="'+ url +'" controls style="position: fixed; left: -1000px; bottom: -1000px; opacity: 1;"></audio>').appendTo('body');
            return new Promise(function (resolve) {
                tpDom.on('loadedmetadata', function () {
                    resolve(tpDom.prop('duration'));
                    tpDom.remove();
                });
            });
        }
        this.getReadingDuration = function (content) {
            var tdDom = $('<article style="position: fixed; left: -1000px; bottom: -1000px; opacity: 1;"><div class="eta"></div>'+ content +'</article>').appendTo('body');
            tdDom.readingTime({
                wordCountTarget: '.words',
                wordsPerMinute: 200
            });
            var rlt = Promise.resolve(tdDom.find('.eta').html());
            tdDom.remove();
            return rlt;
        }
        this.getMinutes = function( time ) {
            let units = time.split(':');
            if (units.length === 1) { return false; }
            units.reverse();
            let secs = 0;
            let weight = 1;
            units.forEach(function (unit) {
                secs += unit * weight;
                weight *= 60;
            });
            return Math.round(secs / 60);
        }
        this.formatTimeFromSeconds = function (ss) {

            var h = Math.floor(ss / 3600);
            ss = ss - h * 3600;
            var m = Math.floor(ss / 60);
            ss = ss - m * 60;

            var s = Math.floor(ss);
            if (h) {
                h = h < 10 ? '0' + h : h;
                m = m < 10 ? '0' + m : m;
                s = s < 10 ? '0' + s : s;
                return h + ':' + m + ':' + s;
            }
            if (m) {
                m = m < 10 ? '0' + m : m;
                s = s < 10 ? '0' + s : s;
                return m + ':' + s;
            }
            return 'less than a minute';
        }
        this.getBlogDuration = function (content, type) {
            switch (parseInt(type)) {
                case 0:
                    return self.getAudioDuration(content).then(function (value) {
                        return self.formatTimeFromSeconds(value);
                    });
                    break;
                case 2:
                    return self.getVideoDuration(content).then(function (value) {
                        return self.formatTimeFromSeconds(value);
                    });
                    break;
                default:
                    return self.getReadingDuration(content);
                    break;

            }
        }
        this.generateEmbedCode = function (url, title, des) {
            var embedCode = '<blockquote class="walden-embedly-card">\n' +
                '    <h4>\n' +
                '        <a href="'+ url +'">'+ title +'</a>\n' +
                '    </h4>\n' +
                '    <p>' + des + '</p>\n' +
                '</blockquote>\n' +
                '<script async src="'+ BASE_URL +'/assets/js/yevgeny/embed-platform.js?version=1" charset="UTF-8"></script>';
            return embedCode.replace('&amp;', '&');
        }
        this.seriesEmbedCode = function (series, layoutType) {
            var viewVersion = 'guided';
            var css = '';
            var js = '';
            var html = '';
            switch (layoutType) {
                case 'grid':
                    viewVersion = 'grid';
                    var url = BASE_URL + '/viewexperience_embed?viewVersion=grid&id='+ series.series_ID +'&postViewVersion=popup';
                    if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                        url += '&affiliate_id=' + AFFILIATE_ID;
                    }
                    html = '<blockquote class="walden-embedly-card">\n' +
                        '    <h4>\n' +
                        '<!-- set postViewVersion with one of popup, turbotax and default or you can set on Javascript Tab-->\n' +
                        '        <a href="'+ url +'">'+ series.strSeries_title +'</a>\n' +
                        '    </h4>\n' +
                        '    <p>'+ series.strSeries_description +'</p>\n' +
                        '</blockquote>\n' +
                        '<script async src="'+ BASE_URL + '/assets/js/yevgeny/embed-platform.js?version=1" charset="UTF-8"></script>';

                    css = '/* content wrapper */\n' +
                        'html body .site-wrapper main.main-content{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* top left welcome back */\n' +
                        'html body .site-wrapper main.main-content .content-inner .content-header .welcome{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid wrapper */\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid{\n' +
                        '    \n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid item */\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li{\n' +
                        '    \n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid item inner*/\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid item title*/\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .title-wrapper .item-title{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid item button wrapper*/\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .buttons-wrapper{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid item button*/\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .buttons-wrapper .btn.btn-circle{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* grid item left bottom line*/\n' +
                        'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner > aside .bottom-line{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* nav footer */\n' +
                        'html body .site-wrapper main.main-content .content-inner footer.content-footer{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer nav(back, continue) wrapper*/\n' +
                        'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer nav back*/\n' +
                        'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper .step-back{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer nav continue*/\n' +
                        'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper .step-next{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer powered by*/\n' +
                        'html body .site-wrapper main.main-content .content-inner footer.content-footer .powered-by{\n' +
                        '    \n' +
                        '}\n';
                    js = '// you can set range 1 ~ 5;\n' +
                        'const COLS_COUNT = 5;\n' +
                        '\n' +
                        '// you can set range 1+;\n' +
                        'const ROWS_COUNT = 2;\n' +
                        '\n' +
                        '// you can set one of popup, turbotax, default\n' +
                        '// var postViewVersion = \'popup\';';
                    break;
                case 'list':
                    viewVersion = 'popup';
                    css = '/* site wrapper */\n' +
                        '.walden-site .site-wrapper {\n' +
                        '    background: white;\n' +
                        '}\n' +
                        '\n' +
                        '/* left panel */\n' +
                        '.walden-site .site-wrapper .left-panel{\n' +
                        '    background-color: white;\n' +
                        '}\n' +
                        '\n' +
                        '/* top left welcome back message */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .welcome{\n' +
                        '    color: inherit;\n' +
                        '}\n' +
                        '\n' +
                        '/* top left login btn */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .login-btn {\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* left panel tree list */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* left panel tree list row */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* left panel tree list row meta info*/\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row .post-meta-info{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* popup widget */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .view-widget{\n' +
                        '    background-color: transparent;\n' +
                        '}';
                    break;
                case 'guided':
                    viewVersion = "guided";
                    css = '/* site wrapper */\n' +
                        '.walden-site .site-wrapper {\n' +
                        '    background: white;\n' +
                        '}\n' +
                        '\n' +
                        '/* left panel */\n' +
                        '.walden-site .site-wrapper .left-panel{\n' +
                        '    background-color: white;\n' +
                        '}\n' +
                        '\n' +
                        '/* top left welcome back message */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .welcome{\n' +
                        '    color: inherit;\n' +
                        '}\n' +
                        '\n' +
                        '/* top left login btn */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .login-btn {\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* left panel tree list */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* left panel tree list row */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* left panel tree list row meta info*/\n' +
                        '\n' +
                        '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row .post-meta-info{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '\n' +
                        '/* right panel */\n' +
                        '\n' +
                        '.walden-site .site-wrapper .right-panel{\n' +
                        '}\n' +
                        '\n' +
                        '/* top right saved money wrapper */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* top right saved money amount */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved .money-amount{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* top right saved money text */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved .w-name{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel blog wrapper */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel blog image wrapper */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-image-wrapper{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel blog image */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-image-wrapper .post-img{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel blog title */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-title{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel blog body (description) */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-body{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer */\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer nav(back, continue) wrapper*/\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer nav back*/\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper .step-back{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer nav continue*/\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper .step-next{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* right panel footer powered by*/\n' +
                        '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .powered-by{\n' +
                        '    \n' +
                        '}';
                    break;
                case 'gallery':
                    css = '/* content wrapper */\n' +
                        'html body .site-wrapper .main-content{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* top left welcome back */\n' +
                        'html body .site-wrapper .main-content .content-inner .content-header .welcome{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* gallery list wrapper */\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* gallery item */\n' +
                        '\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* gallery item inner*/\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* gallery item header */\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* gallery item header day*/\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header .day-wrapper{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '/* gallery item header title*/\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header .post-title{\n' +
                        '    \n' +
                        '}\n' +
                        '\n' +
                        '\n' +
                        '/* gallery item body(description)*/\n' +
                        'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-body{\n' +
                        '    \n' +
                        '}';
                    viewVersion = 'gallery';
                    break;
            }
            var url = BASE_URL + '/series_embed?viewVersion='+ viewVersion +'&id=' + series.series_ID;
            if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                url += '&affiliate_id=' + AFFILIATE_ID;
            }
            html = html ? html : self.generateEmbedCode(url, series.strSeries_title, series.strSeries_description);
            return {html: html, css: css, js: js};
        }
        this.getInfoFromTransfer = function (dataTransfer) {

            var droppedUrl = dataTransfer.getData('URL');

            if(droppedUrl != ''){
                return {url: droppedUrl, type: 'url'}
            }

            if (dataTransfer.files.length < 1){
                return {type: 'other'};
            }

            var type = dataTransfer.files[0].type;

            if (self.valid_image_type.indexOf(type) > -1){
                return {type: 'image', file: dataTransfer.files[0]};
            } else if (self.valid_doc_types.indexOf(type) > -1){
                return {type: 'document', file: dataTransfer.files[0]};
            } else if (self.valid_video_type.indexOf(type)) {
                return {type: 'video', file: dataTransfer.files[0]};
            } else if (self.valid_audio_type.indexOf(type)) {
                return {type: 'audio', file: dataTransfer.files[0]};
            } else {
                return {type: 'other', data: type};
            }
        }
        this.formatItemData = function(itemData, from) {
            from = from ? from : 'default';
            var fData = {};
            switch (from) {
                case 'youtube':
                    fData.title = itemData.snippet.title;
                    fData.summary = itemData.snippet.description;
                    fData.body = self.makeYoutubeUrlById(itemData.id.videoId ? itemData.id.videoId : itemData.id);
                    fData.image = itemData.snippet.thumbnails.high.url;
                    if (itemData.snippet.tags) {
                        fData.keyword = itemData.snippet.tags;
                    }
                    if (itemData.contentDetails && itemData.contentDetails.duration) {
                        var secs = self.secsFromYoutubeDuration(itemData.contentDetails.duration);
                        fData.duration = self.formatTimeFromSeconds(secs)
                    }
                    fData.type = 2;
                    break;
                case 'podcasts':
                    fData.title = itemData.title;
                    fData.summary = '';
                    fData.body = self.makeSpreakerUrlById(itemData.episode_id);
                    fData.image = itemData.image_url;
                    fData.type = 0;
                    break;
                case 'blog':
                    fData.title = itemData.name;
                    fData.summary = '';
                    fData.body = itemData.body;
                    fData.image = itemData.image;
                    fData.type = 7;
                    break;
                case 'recipes':
                    fData.title = itemData.title;
                    fData.summary = '';
                    fData.body = itemData.source_url;
                    fData.image = itemData.image_url;
                    fData.type = 7;
                    break;
                case 'ideabox':
                    fData.title = itemData.strIdeaBox_title;
                    fData.summary = '';
                    fData.body = itemData.strIdeaBox_idea;
                    fData.image = itemData.strIdeaBox_image;
                    fData.type = 7;
                    break;
                case 'posts':
                    fData.title = itemData.strPost_title;
                    fData.summary = itemData.strPost_summary;
                    fData.body = itemData.strPost_body;
                    fData.image = itemData.strPost_featuredimage;
                    fData.type = itemData.intPost_type;
                    break;
                case 'rssb-posts':
                    fData.title = itemData.strRSSBlogPosts_title;
                    fData.summary = itemData.strRSSBlogPosts_description;
                    fData.body = itemData.strRSSBlogPosts_content;
                    fData.type = 7;
                    break;
                default:
                    fData.title = itemData.title;
                    fData.summary = itemData.summary;
                    fData.body = itemData.body;
                    fData.image = itemData.image;
                    fData.type = itemData.type || 7;
                    break;
            }
            fData.nodeType = 'post';
            fData.origin = itemData;
            return fData;
        }
        this.getSourceTypeFromUrl = function (url) {
            if(url.match(/\.(jpeg|jpg|gif|png)$/) != null){
                return 'image';
            }
            if (url.match(/\.(mp4|avi)$/)) {
                return 'video';
            }
            if (url.match(/\.(pdf|text|txt)$/)) {
                return 'document';
            }
            if (self.getApiTypeFromUrl(url) == 'youtube') {
                return 'youtube';
            }
            return 'other';
        }
        this.secsFromYoutubeDuration = function(duration) {
            var a = duration.match(/\d+/g);

            if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
                a = [0, a[0], 0];
            }

            if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
                a = [a[0], 0, a[1]];
            }
            if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
                a = [a[0], 0, 0];
            }

            duration = 0;

            if (a.length == 3) {
                duration = duration + parseInt(a[0]) * 3600;
                duration = duration + parseInt(a[1]) * 60;
                duration = duration + parseInt(a[2]);
            }

            if (a.length == 2) {
                duration = duration + parseInt(a[0]) * 60;
                duration = duration + parseInt(a[1]);
            }

            if (a.length == 1) {
                duration = duration + parseInt(a[0]);
            }
            return duration
        }
        this.socialLinks = function (url, title) {
            var shareUri = encodeURIComponent(url);
            var shareTitle = encodeURIComponent(title);
            var subject = encodeURIComponent('Shared from Walden.ly - ' + title);
            var shareLinks = {
                facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + shareUri + '&title=' + shareTitle,
                twitter: 'https://twitter.com/intent/tweet?url=' + shareUri,
                google: 'https://plus.google.com/share?url=' + shareUri,
                email: 'mailto:?subject=' + subject + '&body=' + encodeURIComponent('I thought you might like this: ') + shareUri,
                linkedin: 'https://www.linkedin.com/shareArticle?mini=true&url='+ shareUri +'&title='+ shareTitle +'&summary='+ subject +'&source='
            }
            return shareLinks;
        }
        this.doScrollTop = function () {
            window.scrollTo(0, 0);
        }
    }
    helperHandle = new HelperClass();
    SeriesTreeClass = function (seriesId, domTree, options) {

        var domRoot;
        var refHandle;

        var series;

        var self = this;

        var defaultOptions = {
            onChange: function (data) {},
        };

        var getSeries = function () {
            return ajaxAPiHandle.apiPost('Series.php', {action: 'get', where: seriesId}, false).then(function (res) {
                series = res.data;
                return {id: 'series_' + series.series_ID, text: series.strSeries_title, children: true, type: 'root', orgData: $.extend({}, series, {post_ID: 0})};
            });
        }

        var getChildren = function (parentId) {
            var ajaxData = {
                action: 'get_items',
                where: {
                    intPost_series_ID: seriesId,
                    intPost_parent: parentId,
                },
                select: ['post_ID', 'strPost_title', 'strPost_nodeType', 'intPost_parent', 'intPost_order'],
            };
            return ajaxAPiHandle.apiPost('Posts.php', ajaxData, false).then(function (res) {
                var items = [];
                res.data.forEach(function (item) {
                    var fItem = nodeFormat(item);
                    items.push(fItem);
                });
                return items;
            });
        }

        var nodeFormat = function (item) {
            var fItem = {id: item.post_ID, text: item.strPost_title, type: item.strPost_nodeType, orgData: item};
            if (fItem.type == 'menu' || fItem.type == 'path') {
                fItem.children = true;
            }
            return fItem;
        }

        var bindEvents = function () {
            domRoot.on('changed.jstree', function (e, data) {
                options.onChange(data);
            });
        }
        this.refHandle = function () {
            return refHandle;
        }
        this.dom = function () {
            return domRoot;
        }
        this.onChange = function (fn) {
            options.onChange = fn;
        }
        this.refresh = function () {
            refHandle.refresh();
        }
        this.getSelectedNode = function(){
            var selectedNodes = refHandle.get_selected(true);
            return selectedNodes.length ? selectedNodes[0] : false;
        }

        this.getSelectedPositionData = function () {
            var selectedNode = self.getSelectedNode();

            var fData = {
                intPost_parent: 0,
                intPost_series_ID: seriesId
            };
            if (selectedNode) {
                switch (selectedNode.type) {
                    case 'root':
                        break;
                    case 'path':
                        fData.intPost_order = 1;
                        fData.intPost_parent = selectedNode.original.orgData.post_ID;
                        break;
                    default:
                        fData.intPost_parent = selectedNode.original.orgData.intPost_parent;
                        fData.intPost_order = selectedNode.original.orgData.intPost_order;
                        break;
                }
            }

            return fData;
        }

        this.init = function () {
            if (typeof domTree !== "undefined" && domTree) {
                domRoot = domTree;
            }
            else {
                domRoot = $('#series-tree');
            }
            options = $.extend({}, defaultOptions, options);
            domRoot.jstree({
                "core" : {
                    "animation" : 300,
                    "check_callback" : function (op, node, par, pos, more) {
                        if (op === 'move_node' && more.core) {
                            var ajaxData = {
                                action: 'update',
                                where: node.original.orgData.post_ID,
                                sets: {intPost_parent: par.type === 'root' ? 0 : par.original.orgData.post_ID},
                            };
                            return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function () {
                                var updateItems = [];
                                $.extend(node.original.orgData, ajaxData.sets);
                                par.children.forEach(function (child, i) {
                                    updateItems.push({where: child, sets: {intPost_order: i + 1}});
                                    var cn = refHandle.get_node(child);
                                    $.extend(cn.original.orgData, {intPost_order: i + 1});
                                });
                                ajaxAPiHandle.apiPost('Posts.php', {action: 'update_posts', updateSets: updateItems}).then(function (res) {
                                });
                            });
                        }
                        else if (op === 'rename_node') {
                            var ajaxData = {
                                action: 'update',
                                where: node.original.orgData.post_ID,
                                sets: {strPost_title: pos},
                            };
                            return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function () {
                                $.extend(node.original.orgData, ajaxData.sets);
                                return true;
                            });
                        }
                        else if (op == 'delete_node') {

                            var ajaxData = {
                                action: 'delete',
                                where: node.original.orgData.post_ID,
                            };
                            ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function () {});
                            return true;
                        }
                        else {
                            return true;
                        }
                    },
                    "themes" : { "stripes" : true },
                    'data' : function (node, cb) {
                        // console.log($.extend({}, node));
                        if (node.id === '#') {
                            getSeries().then(function (data) {
                                cb(data);
                            });
                            return ;
                        }
                        switch (node.type) {
                            case '#':
                                getSeries().then(function (data) {
                                    cb(data);
                                });
                                break;
                            case 'root':
                                getChildren(0).then(function (data) {
                                    cb(data);
                                });
                                break;
                            default:
                                getChildren(node.id).then(function (data) {
                                    cb(data);
                                });
                                break;
                        }
                    }
                },
                "types" : {
                    "#" : {
                        "max_children" : 1,
                        "max_depth" : 100,
                        "valid_children" : ["root"]
                    },
                    "root" : {
                        "icon" : "/assets/images/global-icons/tree/series/start-(3).png",
                        "valid_children" : ["menu", "post"]
                    },
                    "menu" : {
                        "icon" : "/assets/images/global-icons/tree/menu/path-selection-(4).png",
                        "valid_children" : ["path"]
                    },
                    "path" : {
                        "icon" : "/assets/images/global-icons/tree/option/next-(4).png",
                        "valid_children" : ["post", "menu"]
                    },
                    "post" : {
                        "icon" : "/assets/images/global-icons/tree/post/blog-(4).png",
                        "valid_children" : []
                    }
                },
                'contextmenu' : {
                    'items' : function(node) {
                        if (this.get_type(node) === 'root') {
                            return false;
                        }
                        var tmp = $.jstree.defaults.contextmenu.items();

                        delete tmp.remove.action;
                        delete tmp.create;
                        delete tmp.ccp;

                        tmp.remove.action = function (data) {
                            var obj = refHandle.get_node(data.reference);
                            swal({
                                title: "Are you sure?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    refHandle.delete_node(obj);
                                }
                            });
                        }

                        return tmp;
                    }
                },
                "plugins" : [
                    "contextmenu", "dnd", "search",
                    "state", "types", "wholerow"
                ]
            });
            refHandle = domRoot.jstree(true);
            bindEvents();
        }
    }
    if ('scrollRestoration' in history) {
        history.scrollRestoration = 'manual';
    }
    var SiteLoadingClass = function () {

        var domRoot, domProgressList;
        var progressItemHandles = [];

        var ProgressItemClass = function () {
            var domItem;
            var startLoadedV = 0, calcTxt = '', startTimestamp;
            var self = this;
            this.toPercent = function (loadedPercent) {
                if (loadedPercent == 100) {
                    self.toDone();
                }
                else {
                    var dur = new Date().getTime() - startTimestamp;
                    var durLoadedV = loadedPercent - startLoadedV;
                    var tSpeed = dur / durLoadedV;
                    var remainDur = Math.round((100 - loadedPercent) * tSpeed / 1000 + .4);
                    domItem.find('.loaded-bar').css('width', loadedPercent + '%');
                    domItem.find('.progress-txt').html(remainDur + ' seconds left');
                }
            }
            this.toDone = function () {
                domItem.find('.loaded-bar').css('transition-duration', '.3s');
                domItem.find('.loaded-bar').css('width', '100%');
                setTimeout(function () {
                    domItem.remove();
                }, 300);
            }
            this.remove = function () {
                domItem.remove();
            }
            this.init = function () {
                domItem = domProgressList.find('.progress-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domProgressList);
                startTimestamp = new Date().getTime();
            }
        }

        this.show = function () {
            domRoot.show();
        }
        this.hide = function () {
            domRoot.hide();
            progressItemHandles.forEach(function (value, i) {
                value.remove();
                delete progressItemHandles[i];
            });
            progressItemHandles = [];
        }
        this.addProgress = function () {
            var hdl = new ProgressItemClass();
            hdl.init();
            progressItemHandles.push(hdl);
            return hdl;
        }
        this.init = function () {
            domRoot = $('.site-loading');
            domProgressList = domRoot.find('.progress-container');
        }
    }

    siteLoadingHandle = new SiteLoadingClass();
    siteLoadingHandle.init();
})();
