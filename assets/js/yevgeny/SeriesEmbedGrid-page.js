(function () {
    'use strict';
    var PageClass = function () {

        var domPage;
        var postsBlockHandle, authHandle;
        var PostsBlockClass = function (posts) {
            var self = this, popupViewHandle;

            var domRoot = domPage.find('.posts-grid');
            var domGrid = domRoot.find('.tt-grid');

            var maxColsCnt, rowsCnt;
            var grid = domGrid.get(0),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var itemHandles = [];
            var pageSize = 5;
            var currentIndex = 0;
            var arrPageStatus = [
                {class: 'scope-width-5', pageSize: 5},
                {class: 'scope-width-5', pageSize: 5},
                {class: 'scope-width-4', pageSize: 4},
                {class: 'scope-width-3', pageSize: 3},
                {class: 'scope-width-2', pageSize: 2},
                {class: 'scope-width-1', pageSize: 1},
            ];
            var currentPageStatus;

            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent',
                paths: 'paths',
                free: 'boolPost_free'
            };

            var subscriptionFiels = {
                id: 'clientsubscription_ID',
                title: 'strClientSubscription_title',
                body: 'strClientSubscription_body',
                image: 'strClientSubscription_image',
                type: 'intClientSubscriptions_type',
                order: 'intClientSubscriptions_order',
                nodeType: 'strClientSubscription_nodeType',
                parent: 'intClientSubscriptions_parent',
                paths: 'paths',
                completed: 'boolCompleted',
            };

            var viewFormat = function (data) {

                var fData = {};
                var fk;

                for (var k in data) {
                    if (true || from === 'post') {
                        fk = helperHandle.keyOfVal(postFields, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                    else {
                        fk = helperHandle.keyOfVal(subscriptionFiels, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                }
                fData.origin = data;
                return fData;
            }

            var getScreenStatus = function () {
                var w = domRoot.outerWidth();
                if (w > 1300){
                    return 0;
                }
                else if (w > 1100){
                    return 1;
                }
                else if (w > 1000){
                    return 2;
                }
                else if (w > 850){
                    return 3;
                }
                else if (w > 668){
                    return 4;
                }
                else {
                    return 5;
                }
            }
            var ItemClass = function (itemData) {
                var domItemRoot = null;
                var domTitleWrp;
                var self = this;
                var stripeHandler = null;
                var bindEvents = function () {
                    domItemRoot.find('.view-post').click(function () {
                        if (postViewVersion === 'popup') {
                            popupView();
                        }
                    });
                }
                var popupView = function () {
                    var d = viewFormat(itemData);
                    d.formFields = formFields;
                    d.formFieldAnswers = formFieldAnswers;
                    popupViewHandle.view(d);
                }
                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', itemData.strPost_featuredimage);
                    cloneDom.find('.title-wrapper .item-title').html(itemData.strPost_title);
                    switch (postViewVersion) {
                        case 'popup':
                            break;
                        case 'default':
                            cloneDom.find('.view-post').attr('href', BASE_URL + '/view.php?id=' + itemData.post_ID);
                            break;
                        case 'turbotax':
                            cloneDom.find('.view-post').attr('href', BASE_URL + '/view.php?id=' + itemData.post_ID + '&action=embed_page');
                            break;
                    }
                    var html = cloneDom.html();
                    cloneDom.remove();
                    return html;
                }
                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                }
                var readMore = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                }
                var openStripe = function () {
                    if (stripeHandler) {
                        stripeHandler.open({
                            name: itemData.strSeries_title,
                            description: itemData.strSeries_description,
                            currency: 'usd',
                            amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                        });
                    }
                    else {
                        popupErroJoin();
                    }
                }
                this.selectPost = function (token) {
                    token = token ? token : 0;
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    btn.html("");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var sRes = null;
                    var timer = setInterval(
                        function tick() {
                            if (sRes === true){
                                btn.removeClass("circle");
                                btn.addClass("filled");
                                $(".wrap img", domItemRoot).css("display", "block");
                                $(".wrap svg", domItemRoot).css("display", "none");
                                clearInterval(timer);
                                setTimeout(function () {
                                    var s = localStorage.getItem('thegreyshirt_show_popup_every_join');
                                    if (s == null || s === '1'){
                                        popupThanksJoin(itemData.purchased);
                                    }
                                }, 500);
                            }
                            else if (sRes === false) {
                                btn.removeClass("circle");
                                btn.html("Join");
                                if (parseInt(itemData.boolSeries_charge)) {
                                    setTimeout(function () {
                                        btn.html('Join $' + itemData.intSeries_price);
                                    }, 400);
                                }
                                $(".wrap img", domItemRoot).css("display", "none");
                                $(".wrap svg", domItemRoot).css("display", "none");
                                clearInterval(timer);
                                setTimeout(function () {
                                    popupErroJoin();
                                }, 500);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}, false).then(function (res) {
                        if (res.status === true){
                            sRes = true;
                            itemData.purchased = res.data;
                        }
                        else {
                            sRes = false;
                        }
                    });
                    is_loading_controlled_in_local = false;
                }
                this.unSelectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (!itemData.purchased){
                                btn.removeClass("circle");
                                btn.html("Join");
                                if (parseInt(itemData.boolSeries_charge)) {
                                    setTimeout(function () {
                                        btn.html('Join $' + itemData.intSeries_price);
                                    }, 400);
                                }
                                $(".wrap img", domItemRoot).css("display", "none");
                                $(".wrap svg", domItemRoot).css("display", "none");
                                clearInterval(timer);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    $.ajax({
                        url: API_ROOT_URL + '/Series.php',
                        data: {action: 'unJoin', id: itemData.purchased},
                        success: function (res) {
                            if (res.status == true){
                                itemData.purchased = 0;
                            }
                            is_loading_controlled_in_local = true;
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                this.init = function () {
                    itemData.boolSeries_charge = parseInt(itemData.boolSeries_charge);
                    if (itemData.boolSeries_charge) {
                        itemData.intSeries_price =  parseFloat(Math.round(itemData.intSeries_price * 100) / 100).toFixed(2);
                        itemData.intSeries_price = parseFloat(itemData.intSeries_price);
                        domItemRoot.addClass('charged');
                        if (itemData.stripeApi_pKey && itemData.stripeApi_cUrl) {
                            stripeHandler = StripeCheckout.configure({
                                key: itemData.stripeApi_pKey,
                                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                locale: 'auto',
                                token: function(token) {
                                    self.selectPost(token);
                                    // You can access the token ID with `token.id`.
                                    // Get the token ID to your server-side code for use.
                                }
                            });
                        }
                    }
                    if (parseInt(itemData.purchased)){
                        domItemRoot.find('.wrap .select-post').addClass('filled');
                        domItemRoot.find('.wrap .select-post').html('');
                        domItemRoot.find('.wrap img').show();
                    }
                    readMore();
                    bindEvents();
                }
            }
            var loadNewSet = function(set) {
                return new Promise(function (resolve, reject) {
                    if (isAnimating === true){
                        resolve();
                    }
                    isAnimating = true;
                    checkCanPagging();

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = $(el).find( '> *' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild.length ) {
                            itemChild.addClass('tt-old');
                        }
                    } );

                    // apply effect
                    setTimeout( function() {
                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find( '> *:last-child' ));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                            resolve();
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                });
            };

            var bindEvents = function () {
                domPage.find('.step-back').click(function () {
                    self.previous();
                });
                domPage.find('.step-next').click(function () {
                    self.next();
                });
                $(window).resize(function () {
                    var pageStatus = arrPageStatus[getScreenStatus()];
                    if (pageStatus != currentPageStatus){
                        if (pageStatus.pageSize > pageSize){
                            self.setPageStatus(pageStatus);
                            loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize * rowsCnt));
                        }
                        else {
                            self.setPageStatus(pageStatus);
                        }
                    }
                });
            }

            var checkCanPagging = function () {
                if (currentIndex - pageSize < 0){
                    domRoot.find('.prev-pagging').addClass('disable');
                }
                else {
                    domRoot.find('.prev-pagging').removeClass('disable');
                }
                if (currentIndex + pageSize >= itemHandles.length){
                    domRoot.find('.next-pagging').addClass('disable');
                }
                else {
                    domRoot.find('.next-pagging').removeClass('disable');
                }
            }
            this.setPageStatus = function (pageStatus) {
                if (pageStatus.pageSize > maxColsCnt) {
                    pageStatus.class = pageStatus.class.replace(pageStatus.pageSize, maxColsCnt);
                    pageStatus.pageSize = maxColsCnt;
                }
                for (var i = pageStatus.pageSize * rowsCnt; i < items.length; i ++){
                    $(items[i]).remove();
                }
                for (var i = 0; i < pageStatus.pageSize * rowsCnt - items.length; i++){
                    domGrid.append('<li class="tt-empty"></li>');
                }
                domRoot.find('.tt-grid-wrapper').addClass(pageStatus.class);
                if (currentPageStatus) { domRoot.find('.tt-grid-wrapper').removeClass(currentPageStatus.class);}
                currentPageStatus = pageStatus;
                pageSize = currentPageStatus.pageSize;
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                checkCanPagging();
            }
            this.sortItemsBy = function (sortBy) {
                switch (sortBy){
                    case 'a-z':
                        itemHandles.sort(sortByAZ);
                        break;
                    case 'z-a':
                        itemHandles.sort(sortByZA);
                        break;
                    default:
                        break;
                }
            }
            this.previous = function () {
                if (currentIndex - pageSize * rowsCnt < 0 || isAnimating){
                    return false;
                }
                currentIndex -= pageSize * rowsCnt;
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize * rowsCnt));
            }
            this.next = function () {
                if (currentIndex + pageSize * rowsCnt >= itemHandles.length || isAnimating){
                    return false;
                }
                currentIndex += pageSize * rowsCnt;
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize * rowsCnt));
            }
            this.init = function () {
                popupViewHandle = new ViewPostPopupWidgetClass($('.view-post-popup-widget.component'));
                popupViewHandle.init();

                if (typeof COLS_COUNT !== 'undefined') {
                    maxColsCnt = parseInt(COLS_COUNT);
                }
                else {
                    maxColsCnt = 5;
                }

                if (typeof ROWS_COUNT !== 'undefined') {
                    rowsCnt = parseInt(ROWS_COUNT);
                }
                else {
                    rowsCnt = 2;
                }
                self.setPageStatus(arrPageStatus[getScreenStatus()]);
                posts.forEach(function (post) {
                    var itemHandle = new ItemClass(post);
                    itemHandles.push(itemHandle);
                });
                bindEvents();
                loadNewSet(itemHandles.slice(currentIndex, currentIndex + pageSize * rowsCnt));
            }
        }
        var AuthClass = function () {
            var domAuth, domLoginForm, domRegisterForm;

            function showRegisterForm(){
                $('.loginBox').fadeOut('fast',function(){
                    $('.registerBox').fadeIn('fast');
                    $('.login-footer').fadeOut('fast',function(){
                        $('.register-footer').fadeIn('fast');
                    });
                    $('.modal-title').html('Register with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function showLoginForm(){
                $('#loginModal .registerBox').fadeOut('fast',function(){
                    $('.loginBox').fadeIn('fast');
                    $('.register-footer').fadeOut('fast',function(){
                        $('.login-footer').fadeIn('fast');
                    });

                    $('.modal-title').html('Login with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function openLoginModal(){
                showLoginForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);
            }
            function openRegisterModal(){
                showRegisterForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);

            }

            function shakeModal(errorMsg){
                if (!errorMsg) {
                    errorMsg = 'Invalid email/password combination';
                }
                $('#loginModal .modal-dialog').addClass('shake');
                $('.error').addClass('alert alert-danger').html(errorMsg);
                $('input[type="password"]').val('');
                setTimeout( function(){
                    $('#loginModal .modal-dialog').removeClass('shake');
                }, 1000 );
            }
            var login = function (username, password) {
                ajaxAPiHandle.pagePost('login.php', {function: 'signin', username: username, password: password}, true, 200).then(function (res) {
                    if (res.result.success) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        ajaxAPiHandle.apiPost('Purchased.php', {action: 'get', where: {intPurchased_series_ID: series.series_ID}}).then(function (pRes) {
                            purchased = pRes.data;
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            if (purchased) {
                            }
                        });
                    }
                    else {
                        shakeModal();
                    }
                });
            }
            var register = function (sets) {
                $('.loading').show();
                var ajaxData = {function: 'register'};
                $.extend(ajaxData, sets);
                ajaxAPiHandle.pagePost('login.php', ajaxData, false).then(function (res) {
                    if (res.result.registered) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        setTimeout(function () {
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            postsSectionHandle.showWelcome();
                        }, 300);
                    }
                    else {
                        shakeModal('something is wrong');
                    }
                });
            }
            var bindEvents = function () {
                $('.big-login').click(function () {
                    openLoginModal();
                });
                $('.show-register').click(function () {
                    showRegisterForm();
                });
                $('.show-login').click(function () {
                    showLoginForm();
                });
                domLoginForm.submit(function () {
                    var username = domLoginForm.find('[name="email"]').val();
                    var password = domLoginForm.find('[name="password"]').val();
                    login(username, password);
                    return false;
                });
                domRegisterForm.submit(function () {
                    var sets = {};
                    sets.email = domRegisterForm.find('[name="email"]').val();
                    sets.f_name = domRegisterForm.find('[name="first_name"]').val();
                    sets.l_name = domRegisterForm.find('[name="last_name"]').val();
                    sets.password = domRegisterForm.find('[name="password"]').val();
                    var confirmPassword = domRegisterForm.find('[name="password_confirmation"]').val();
                    if (sets.password === confirmPassword) {
                        register(sets);
                    }
                    else {
                        shakeModal("Passwords don't match");
                    }
                    return false;
                });
            }
            this.init = function () {
                domAuth = $('#loginModal');
                domLoginForm = domAuth.find('.loginBox form');
                domRegisterForm = domAuth.find('.registerBox form');
                bindEvents();
            }
        }

        this.init = function () {

            domPage = $('main.main-content');

            postsBlockHandle = new PostsBlockClass(initialPosts);
            postsBlockHandle.init();

            authHandle = new AuthClass();
            authHandle.init();

            var embedGlobalHandle = new EmbedPageGlobalClass();
            embedGlobalHandle.init();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();