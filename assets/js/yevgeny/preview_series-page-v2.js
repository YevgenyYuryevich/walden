(function () {
  'use strict';

  var PageClass = function () {
    var domPage;
    var payHandle = null, self = this;

    var contentHeaderHandle, contentFooterHandle, mainHandle, listingHandle, listingPostHandle, postEditorHandle;

    var viewUserPage = function (data) {
      $('<form action="my_solutions" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
    }

    var popupThanksJoin = function (data) {
      swal({
        icon: "success",
        title: 'SUCCESS!',
        text: 'Thanks for joining! Would you like to go to your experiences?',
        buttons: {
          returnHome: {
            text: "My Experiences",
            value: 'return_home',
            visible: true,
            className: "",
            closeModal: true,
          },
          customize: {
            text: "Not yet",
            value: 'customize',
            visible: true,
            className: "",
            closeModal: true
          }
        },
        closeOnClickOutside: false,
      }).then(function (value) {
        if (value == 'return_home'){
          viewUserPage(data);
        }
        else {
        }
      });
      var domSwalRoot, domDisableCheckbox;
      var disablePopup = function () {
        localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
      }
      var enabelPopup = function () {
        localStorage.removeItem('thegreyshirt_show_popup_every_join');
      }
      var bindEvents = function () {
        domDisableCheckbox.change(function () {
          $(this).prop('checked') ? disablePopup() : enabelPopup();
        })
      }
      var init = function () {
        setTimeout(function () {
          domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
          domSwalRoot.addClass('thanks-join');
          domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
          domDisableCheckbox = domSwalRoot.find('#disable_popup');
          bindEvents();
        }, 100);
      }();
    }

    var popupErroJoin = function () {
      swal("Sorry, Something went wrong!", {
        content: 'please try again later'
      });
    }

    var openStripeCheckout = function () {
      if (stripeCheckoutHandle) {
        stripeCheckoutHandle.open(series.series_ID);
      } else {
        popupErroJoin();
      }
    };
    var join = function (showLoading) {

      var btn = domPage.find('.wrap .select-post');
      btn.removeClass("filled");
      btn.addClass("circle");
      btn.html("");
      $(".wrap  svg", domPage).css("display", "block");
      $(".wrap .circle_2", domPage).attr("class", "circle_2 fill_circle");
      var timer = setInterval(
        function tick() {
          if (series.purchased){
            btn.removeClass("circle");
            btn.addClass("filled");
            $(".wrap img", domPage).css("display", "block");
            $(".wrap svg", domPage).css("display", "none");
            clearInterval(timer);
            setTimeout(function () {
              var s = localStorage.getItem('thegreyshirt_show_popup_every_join');
              if (s == null || s === '1'){
                popupThanksJoin(series.purchased);
              }
            }, 500);
          }
        }, 2000);

      ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: series.series_ID}, showLoading).then(function (res) {
        if (res.status == true){
          series.purchased = res.data;
          contentHeaderHandle.bindData();
          series.join_count++;
          domPage.find('.join__count__value').text(series.join_count);
        }
      });
    }

    var unJoin = function (showLoading) {
      var btn = domPage.find('.wrap .select-post');
      btn.removeClass("filled");
      btn.addClass("circle");
      $(".wrap  svg", domPage).css("display", "block");
      $(".wrap .circle_2", domPage).attr("class", "circle_2 fill_circle");
      var timer = setInterval(
        function tick() {
          if (!series.purchased){
            btn.removeClass("circle");
            btn.html("Join");
            $(".wrap img", domPage).css("display", "none");
            $(".wrap svg", domPage).css("display", "none");
            clearInterval(timer);
            if (series.boolSeries_charge == 1) {
              setTimeout(function () {
                btn.html("Join $" + series.intSeries_price);
              }, 400);
            }
          }
        }, 2000);
      ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: series.purchased}, showLoading).then(function (res) {
        if (res.status == true){
          series.purchased = 0;
          contentHeaderHandle.bindData();
          series.join_count--;
          domPage.find('.join__count__value').text(series.join_count);
        }
      });
    }

    var bindEvents = function () {
      domPage.find('.wrap .select-post').parent().click(function () {
        if (CLIENT_ID > -1){
          if (series.boolSeries_charge == 1 && series.purchased == 0) {
            openStripeCheckout();
          }
          else {
            parseInt(series.purchased) ? unJoin(false) : join(false);
          }
        }
        else{
          authHandle.activeLogin();
          authHandle.popup();
          authHandle.afterLogin(function () {
            window.location.reload();
          });
        }
      });
      domPage.find('.series__share-join .share-btn').click(function () {
        listingPostHandle.setSelected(posts[0].post_ID);
        listingPostHandle.gotoShareSeries();
        listingPostHandle.open();
      });
    };

    var ContentHeaderClass = function () {
      var domRoot;
      let self = this;

      let bindEvents = function () {
        domRoot.find('.btn__join').click(function () {
          if (CLIENT_ID > -1){
            if (series.boolSeries_charge == 1 && series.purchased == 0) {
              openStripeCheckout();
            }
            else {
              parseInt(series.purchased) ? unJoin(true) : join(true);
            }
          }
          else{
            authHandle.activeLogin();
            authHandle.popup();
            authHandle.afterLogin(function () {
              window.location.reload();
            });
          }
        });
      }
      this.bindData = function () {
        if (parseInt(series.purchased)) {
          domRoot.find('.btn__join').find('span').hide();
          domRoot.find('.btn__join').find('svg').show();
        } else {
          domRoot.find('.btn__join').find('span').show();
          domRoot.find('.btn__join').find('svg').hide();
        }
      }
      this.dom = function () {
        return domRoot;
      }
      this.init = function () {
        domRoot = domPage.find('.page__header');
        self.bindData();
        bindEvents();
      }
    }

    let MainClass = function () {
      let domRoot, domContainer;
      let itemHandles = [];

      let availableTypes = [0, 2, 8], audioCnt = 1;

      let ItemClass = function (itemData) {
        var domItem, domBody, domUsedBody, domVideo;
        var metaData = {isBodyLoaded: false};
        var self = this;
        let payBlogHandle;
        this.dom = function () {
          return domItem;
        }
        let bindEvents = function () {
          domItem.find('.share-btn').click(function () {
            listingPostHandle.setSelected(itemData.post_ID);
            listingPostHandle.open().then(function () {
              var hdl = listingPostHandle.getHandle(itemData.post_ID);
              if (hdl) {
                hdl.domInnerWrap().find('.share-action').addClass('shared');
                setTimeout(function () {
                  hdl.dom().get(0).scrollIntoView();
                }, 700);
              }
            });
          });
          domItem.find('.edit-btn').click(function () {
            postEditorHandle.setData(itemData);
            postEditorHandle.open();
            postEditorHandle.afterSave(function (change) {
              itemData = Object.assign(itemData, change);
              self.bindData();
              self.makeBody();
            });
          });
        }
        this.makeBody = function () {
          metaData.isBodyLoaded = true;
          var purchased = series.purchased;
          var type = parseInt(itemData.intPost_type);
          type = availableTypes.indexOf(type) != -1 ? type : 'other';
          if (series.boolSeries_charge == 1 && !parseInt(purchased) && itemData.boolPost_free == 0) {
            newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
            domUsedBody = newDomUsedBody;
            payBlogHandle = new PayBlogClass(domUsedBody, series);
            payBlogHandle.init();
          }
          else if (itemData.strPost_nodeType === 'menu') {
            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
            domUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
            var menuData = helperHandle.postToViewFormat(itemData);
            menuData.paths = itemData.paths.map(function (path, i) {
              return helperHandle.postToViewFormat(path);
            });
            var menuHandle = new MenuOptionsClass(domUsedBody.find('.menu-options-component'), menuData);
            menuHandle.init();
            menuHandle.onSelect(function (option) {
              selectPath(option);
            });
          }
          else {
            var newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
            switch (parseInt(type)){
              case 0:
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                if (!itemData.strPost_body) {
                  swal('Sorry, This is wrong data');
                  return false;
                }
                domUsedBody.find('.audioplayer-tobe').attr('data-thumb', itemData.strPost_featuredimage);
                domUsedBody.find('.audioplayer-tobe').attr('data-source', itemData.strPost_body);
                domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                var settings_ap = {
                  disable_volume: 'off',
                  disable_scrub: 'default',
                  design_skin: 'skin-wave',
                  skinwave_dynamicwaves: 'off'
                };
                dzsag_init('#audio-' + audioCnt, {
                  'transition':'fade',
                  'autoplay' : 'off',
                  'settings_ap': settings_ap
                });
                audioCnt++;
                break;
              case 2:
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                domVideo = domUsedBody.find('video');
                setTimeout(function () {
                  if (helperHandle.getApiTypeFromUrl(itemData.strPost_body) === 'youtube') {
                    var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": itemData.strPost_body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                    domVideo.attr('data-setup', JSON.stringify(setup));
                  }
                  else {
                    domVideo.append('<source src="'+ itemData.strPost_body +'" />');
                  }
                  domVideo.attr('width', domUsedBody.innerWidth());
                  domVideo.attr('height', domUsedBody.innerWidth() * 540 / 960);
                  videojs(domVideo.get(0), {}, function () {
                    domBody.find('.blog-duration').hide();
                  });
                }, 100);
                break;
              case 8:
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                domUsedBody.append(itemData.strPost_body);
                break;
              default:
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                domUsedBody.append(itemData.strPost_body);
                break;
            }
          }
        }
        this.bindData = function () {
          domItem.find('.item-title').html(itemData.strPost_title).attr('href', 'view_blog?id=' + itemData.post_ID);
          domItem.find('.day-value').html(itemHandles.length + 1);
          domItem.addClass('type-' + itemData.intPost_type);
          domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);

          if (itemData.strPost_duration) {
            domItem.find('.blog-duration').html(itemData.strPost_duration);
          }
          else {
            helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
              domItem.find('.blog-duration').html(res);
              itemData.strPost_duration = res;
              ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
            });
          }
        }
        this.isBodyLoaded = function () {
          return metaData.isBodyLoaded;
        }
        this.data = function () {
          return itemData;
        }
        this.init = function(){
          domItem = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domContainer);
          domBody = domItem.find('.item-body');
          bindEvents();
        }
      }
      let bindEvents = function () {
        domContainer.on('afterChange', function (e, slick, currentSlide) {

          var loadIndex = currentSlide + 2;
          if (loadIndex < itemHandles.length && !itemHandles[loadIndex].isBodyLoaded()) {
            itemHandles[loadIndex].makeBody();
          }
          itemHandles[currentSlide].dom().removeClass('previous-item').removeClass('next-item');
          if (currentSlide > 0) {
            itemHandles[currentSlide - 1].dom().addClass('previous-item');
          }
          if (currentSlide < itemHandles.length -1) {
            itemHandles[currentSlide + 1].dom().addClass('next-item');
          }
          listingHandle.setCurrentPost(itemHandles[currentSlide].data());
        });
      }
      let selectPath = function (path) {
        ajaxAPiHandle.apiPost('Posts.php', {action: 'get_serial_items', where: {intPost_series_ID: series.series_ID, intPost_parent: path.id}, limit: 100}).then(function (res) {
          var lastHandle;
          for (var i = itemHandles.length - 1; i >= 0; i--) {
            lastHandle = itemHandles[i];
            if (lastHandle.data().post_ID == path.parent) {
              break;
            }
            domContainer.slick('slickRemove', i);
            lastHandle.dom().remove();
            itemHandles.pop();
          }
          setTimeout(function () {
            addNextItems(res.data);
          }, 100);
        });
      }
      let empty = function () {
        itemHandles.forEach(function (value, i) {
          delete itemHandles[i];
        });
        itemHandles = [];
        domContainer.slick('unslick');
        domContainer.empty();
      }
      let addNextItems = function (nextPosts) {
        nextPosts.forEach(function (itemData, i) {
          var hdl = new ItemClass(itemData);
          hdl.init();
          hdl.bindData();
          if (i < 3){
            hdl.makeBody();
          }
          itemHandles.push(hdl);
          domContainer.slick('slickAdd', hdl.dom());
        });
      }
      let bindData = function () {
        posts.forEach(function (itemData, i) {
          itemData.day = itemData.viewDay;
          var hdl = new ItemClass(itemData);
          hdl.init();
          hdl.bindData();
          if (i < 3){
            hdl.makeBody();
          }
          itemHandles.push(hdl);
        });
        domContainer.slick({
          dots: false,
          arrows: true,
          infinite: false,
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: false,
          autoplaySpeed: 5000,
          centerPadding: '20%',
          nextArrow: $('.arrow-right'),
          prevArrow: $('.arrow-left'),
          responsive: [
            {
              breakpoint: 769,
              settings: {
                dots: false,
                arrows: false,
                infinite: false,
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 5000,
              }
            },
          ]
        });
      }
      this.refreshBody = function () {
        itemHandles.forEach(function (hdl) {
          if (hdl.isBodyLoaded()) {
            hdl.makeBody();
          }
        });
      }
      this.setPosition = function () {
        domContainer.slick('setPosition');
      }
      this.init = function () {
        domRoot = domPage.find('main.content__main');
        domContainer = domRoot.find('.posts-container');
        bindData();
        bindEvents();
      }
    }

    let ListingClass = function () {
      var domRoot, domLoadMoreWrap;

      var itemHandles = [];

      var ItemClass = function (itemData) {
        var domItem, domBody, domTypeBody;
        var bindData = function () {
          domItem.addClass('type-' + itemData.intPost_type);
          domItem.find('.day-value').html(itemData.viewDay);
          switch (parseInt(itemData.intPost_type)) {
            case 0:
              domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
              break;
            case 2:
              domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
              domTypeBody.find('img').attr('src', itemData.strPost_featuredimage);
              break;
            default:
              domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
              domTypeBody.html(itemData.strPost_body);
              break;
          }
          domItem.find('.item-title').html(itemData.strPost_title).attr('href', 'view_blog?id=' + itemData.post_ID);
          if (itemData.strPost_duration) {
            domItem.find('.blog-duration').html(itemData.strPost_duration);
          }
          else {
            helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
              domItem.find('.blog-duration').html(res);
              itemData.strPost_duration = res;
              ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
            });
          }
        }
        let bindEvents = function () {
          domItem.find('.share-btn').click(function () {
            listingPostHandle.setSelected(itemData.post_ID);
            listingPostHandle.open().then(function () {
              var hdl = listingPostHandle.getHandle(itemData.post_ID);
              if (hdl) {
                hdl.domInnerWrap().find('.share-action').addClass('shared');
                setTimeout(function () {
                  hdl.dom().get(0).scrollIntoView();
                }, 700);
              }
            });
          });
          domItem.find('.edit-action').click(function () {
            postEditorHandle.setData(itemData);
            postEditorHandle.open();
            postEditorHandle.afterSave(function (change) {
              itemData = Object.assign(itemData, change);
              bindData();
            });
          });
        }
        this.data = function () {
          return itemData;
        }
        this.append = function () {
          domItem.appendTo(domRoot.find('.posts-container'));
        }
        this.setDuration = function () {
          if (itemData.strPost_duration) {
            domBody.find('.blog-duration').html(itemData.strPost_duration);
          }
          else {
            helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
              domBody.find('.blog-duration').html(res);
              itemData.strPost_duration = res;
              ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
            });
          }
        }
        this.detach = function () {
          domItem.detach();
        }
        this.init = function () {
          domItem = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
          domBody = domItem.find('.item-type-body');
          bindData();
          bindEvents();
        }
      }

      var bindEvents = function () {
        domLoadMoreWrap.find('.load-more').click(function () {
          loadMore();
        });
      }
      var loadMore = function () {
        domLoadMoreWrap.addClass('status-loading');
        var ajaxData = {
          action: 'get_many',
          where: {
            intPost_series_ID: series.series_ID,
            intPost_parent: 0,
          },
          limit: {
            offset: itemHandles.length,
            size: 20,
          }
        };
        return ajaxAPiHandle.apiPost('Posts.php', ajaxData, false).then(function (res) {
          res.data.forEach(function (post) {
            var hdl = new ItemClass(post);
            hdl.init();
            hdl.append();
            hdl.setDuration();
            itemHandles.push(hdl);
          });
          domLoadMoreWrap.removeClass('status-loading');
          if (res.data.length < ajaxData.limit.size) {
            domLoadMoreWrap.hide();
          }
        });
      }
      this.setCurrentPost = function (post) {
      }
      this.setDuration = function () {
        itemHandles.forEach(function (value) {
          value.setDuration();
        });
      }
      this.init = function () {
        domRoot = domPage.find('.quick__overview #post__list');
        domLoadMoreWrap = domRoot.find('.load-more-status');
        if (parseInt(series.boolSeries_affiliated)) {
          domRoot.find('.affiliate-wrapper').show();
          var v = parseInt(series.intSeries_price * series.intSeries_affiliate_percent / 100);
          domRoot.find('.affiliate-wrapper').find('.affiliate-value').html(v);
        }
        posts.forEach(function (post, i) {
          if (i < 20){
            var hdl = new ItemClass(post);
            hdl.init();
            hdl.append();
            itemHandles.push(hdl);
          }
        });
        bindEvents();
      }
    };

    var ContentFooterClass = function () {
      var domRoot;
      let bindEvents = function () {
        domRoot.find('.share-btn').click(function () {
          listingPostHandle.setSelected(posts[0].post_ID);
          listingPostHandle.gotoShareSeries();
          listingPostHandle.open();
        });
      }
      this.init = function () {
        domRoot = domPage.find('.page__footer');
        domRoot.find('.share-action').circleNav(domRoot.find('.circle-nav-wrapper'));
        bindEvents();
      }
    }

    this.init = function () {

      domPage = $('.site-wrapper .site-content');

      mainHandle = new MainClass();
      mainHandle.init();

      listingHandle = new ListingClass();
      listingHandle.init();

      contentHeaderHandle = new ContentHeaderClass();
      contentHeaderHandle.init();

      contentFooterHandle = new ContentFooterClass();
      contentFooterHandle.init();

      if (parseInt(series.purchased)){
        domPage.find('.wrap .select-post').addClass('filled');
        domPage.find('.wrap .select-post').html('');
        domPage.find('.wrap img').show();
      }
      listingPostHandle = new ListingPostClass(series, $('.site-wrapper.page .listing-post-component'));
      listingPostHandle.init();
      listingPostHandle.setMode('share');

      postEditorHandle = new PostEditorClass($('.site-wrapper > .post-editor'));
      postEditorHandle.init();
      bindEvents();
    }
  }
  var pageHandle = new PageClass();
  pageHandle.init();
})();
