(function () {

    'use strict';

    var is_loading_controlled_in_local = false;
    var createSeriesHandle = null;

    var selectPostsHandle = null;

    var createdSeries = false;

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    var accept_doc_types = ['application/pdf','text/plain'];
    var accept_image_type = ['image/png', 'image/jpeg'];
    var accept_audio_types = ['audio/mp3'];
    var accept_video_types = ['video/mp4', 'video/avi'];

    var CreateSeriesClass = function () {
        var domRoot = $('.for-series');
        var domForm = $('#series-form', domRoot);
        var domCategories = domForm.find('[name="intSeries_category"]');

        var self = this;
        var availableDays = '1111111';

        var bindEvents = function () {
            domForm.submit(function () {
                self.next();
                return false;
            });
            domForm.find('.cover-image-wrapper [name="strSeries_image"]').change(function () {
                updateCoverImage(this);
            });
            domRoot.find('.available-days-list > li > .icon-wrapper').click(function () {
                toggleAvailableDay($(this).data('day'), $(this).parent());
            });
            domRoot.find('[name="boolSeries_charge"]').change(function () {
                if (isStripeAvailable) {
                    var checked = $(this).prop('checked');
                    if (checked) {
                        domRoot.find('.series-charge-wrapper').addClass('charged');
                        domRoot.find('.charge-price').removeAttr('disabled');
                        domForm.find('[name="boolSeries_affiliated"]').removeAttr('disabled');
                    }
                    else {
                        domRoot.find('.series-charge-wrapper').removeClass('charged');
                        domRoot.find('.charge-price').attr('disabled', true);
                        domForm.find('[name="boolSeries_affiliated"]').prop('checked', false).change();
                        domForm.find('[name="boolSeries_affiliated"]').attr('disabled', true);
                    }
                    bindData();
                }
                else {
                    if ($(this).prop('checked')) {
                        $(this).prop('checked', false).change();
                    }
                    else {
                        swal("Connect your account");
                    }
                }
                bindData();
            });
            domRoot.find('.charge-helper a').click(function (e) {
                localStorage.setItem('walden_start_page_stripe_connect', 'createseries');
            });

            domRoot.find('[name="boolSeries_affiliated"]').change(function () {
                if ($(this).prop('checked')) {
                    domForm.find('.affiliate-settings').addClass('affiliated');
                    domForm.find('[name="intSeries_affiliate_percent"]').removeAttr('disabled');
                    domForm.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('enable');
                }
                else {
                    domForm.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                    domForm.find('.affiliate-settings').removeClass('affiliated');
                }
                bindData();
            });
            domRoot.find('[name="intSeries_price"]').change(function () {
                bindData();
            });
            domRoot.find('[name="intSeries_affiliate_percent"]').change(function () {
                bindData();
            });
        }
        var updateCoverImage = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        var toggleAvailableDay = function (dayName, domTarget) {
            var dayNames = ['mon', 'tus', 'wed', 'thi', 'fri', 'sat', 'sun'];
            var dayKey = dayNames.indexOf(dayName);

            availableDays = availableDays.replaceAt(dayKey, availableDays[dayKey] == '1' ? '0' : '1');
            domTarget.toggleClass('active-day');
        }
        var getFormData = function () {
            var data = helperHandle.formSerialize(domForm);

            delete data.search_term;

            data.available_days = availableDays;
            data.boolSeries_level = level;
            var files = domForm.find('input[name="strSeries_image"]').prop('files');
            var imageFile = files.length ? files[0] : false;
            return {sets: data, imageFile: imageFile};
        }
        var createSeries = function () {
            var ajaxData = new FormData();
            var formData = getFormData();

            if (banToCreatePublic) {
                formData.sets.boolSeries_isPublic = 0;
            }
            ajaxData.append('sets', JSON.stringify(formData.sets));
            if (formData.imageFile) {
                ajaxData.append('strSeries_image', formData.imageFile);
            }
            ajaxData.append('action', 'insert');

            return ajaxAPiHandle.multiPartApiPost('Series.php', ajaxData);
        }

        var openCreateSeriesContent = function (sets) {
            var vForm = $('<form action="createseriescontent" method="get"></form>').hide().appendTo('body');
            var inputs = '';
            for (var k in sets) {
                inputs += '<input name="'+ k +'" value="' + sets[k] +'">';
            }
            vForm.append($(inputs));
            vForm.submit().remove();
        }
        var bindData = function () {
            if (domForm.find('[name="boolSeries_charge"]').prop('checked')) {
                if (domForm.find('[name="boolSeries_affiliated"]').prop('checked')) {
                    var ownerMoney = domForm.find('[name="intSeries_price"]').val() * (100 - parseInt(domForm.find('[name="intSeries_affiliate_percent"]').val())) / 100;
                    ownerMoney = parseInt(ownerMoney);
                    domRoot.find('.money-expects .charge-field.field-value').html('$' + ownerMoney);

                    var affiliateMoney = domForm.find('[name="intSeries_price"]').val() - ownerMoney;
                    affiliateMoney = parseInt(affiliateMoney);
                    domRoot.find('.money-expects .affiliate-field.field-value').html('$' + affiliateMoney);
                }
                else {
                    domRoot.find('.money-expects .charge-field.field-value').html('$' + domForm.find('[name="intSeries_price"]').val());
                    domRoot.find('.money-expects .affiliate-field.field-value').html('NO Affiliated');
                }
            }
            else {
                domRoot.find('.money-expects .charge-field.field-value').html('No Charged');
                domRoot.find('.money-expects .affiliate-field.field-value').html('NO Affiliated');
            }
        }

        this.next = function () {
            createSeries().then(function (res) {
                createdSeries = res.data;
                openCreateSeriesContent({id: createdSeries.series_ID});
                // selectPostsHandle.setSearchTerm(searchTerm).then(function () {
                //
                //     $('.nav.nav-tabs li:last-child a').tab('show');
                // });
            });
        }
        this.createCategory = function (text) {
            return $.ajax({
                url: ACTION_URL,
                data: {action: 'create_category', name: text},
                success: function (res) {
                },
                type: 'post',
                dataType: 'json',
            });
        }
        this.makeSelect2 = function () {
            domCategories.select2({tags: true});
            domCategories.on('select2:select', function () {
                var data = domCategories.select2('data')[0];
                if (!data.selected){
                    self.createCategory(data.text).then(function (res) {
                        if (res.status){
                            domCategories.find('[data-select2-tag]').removeAttr('data-select2-tag').attr('value', res.data);
                        }
                    });
                }
            })
        }
        this.getSearchTerm = function () {
            return domForm.find('[name="search_term"]').val();
        }
        this.init = function () {
            domForm.find('[name="intSeries_affiliate_percent"]').bootstrapSlider ({
                handle: 'square',
                min: 0,
                max: 100
            });
            domForm.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
            domRoot.find('[data-toggle="popover"]').popover();
            bindData();
            bindEvents();
            self.makeSelect2();
        }
    }
    var SelectPostsClass = function () {

        var domRoot = $('#for-posts');
        var domBlocksContainer, domMetaBlocksContainer;
        var domSearchForm = domRoot.find('.search-term-wrapper');
        var domSearch = domSearchForm.find('#search-term2');

        var urlListHandle = null;
        var youtubeListHandle = null;
        var podcastsListHandle = null;
        var blogsListHandle = null;
        var recipesListHandle = null;
        var ideaboxListHandle = null;
        var postsListHandle = null;
        var rssbPostsListHandle = null;
        var self = this;

        var isAdvancedUpload;

        var postFormat = function (data) {
            return {
                intPost_series_ID: createdSeries.series_ID,
                strPost_title: data.title,
                strPost_body: data.body,
                strPost_summary: data.summary,
                strPost_featuredimage: data.image,
                intPost_type: data.type,
            };
        }
        var subscriptionFormat = function (data) {
            return {
                strClientSubscription_title: data.title,
                strClientSubscription_body: data.body,
                strClientSubscription_image: data.image,
                intClientSubscriptions_type: data.type,
            };
        }

        var searchPostsByKeyword = function (keyword, pageTokenY, pageTokenS, pageTokenT, pageTokenF, pageTokenI, pageTokenP, pageTokenRssbP) {
            var ajaxData = {
                q: keyword,
                size: 10,
                items: [
                    {name: 'youtube', token: pageTokenY},
                    {name: 'spreaker', token: pageTokenS},
                    {name: 'recipes', token: pageTokenT},
                    {name: 'blogs', token: pageTokenF},
                    {name: 'ideabox', token: pageTokenI},
                    {name: 'posts', token: pageTokenP},
                    {name: 'rssbPosts', token: pageTokenRssbP},
                ]
            };
            return $.ajax({
                url: API_ROOT_URL + '/SearchPosts.php',
                data: ajaxData,
                success: function (res) {
                },
                type: 'post',
                dataType: 'json',
            });
        }

        var bindEvents = function () {
            domRoot.find('.add-new-posts').click(function () {
                goTo_addNewPosts();
            });
            domSearchForm.submit(function () {
                searchPostsByKeyword(domSearch.val(), false, false, false, false, false, false, false).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                    podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                    blogsListHandle.setItems(res.data.blogs, domSearch.val());
                    recipesListHandle.setItems(res.data.recipes, domSearch.val());
                    ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                    postsListHandle.setItems(res.data.posts, domSearch.val());
                    rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                });
                return false;
            });
            domSearchForm.find('.clear-search-term').click(function () {
                searchPostsByKeyword('', false, false, false, false, false, false, false).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                    podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                    blogsListHandle.setItems(res.data.blogs, domSearch.val());
                    recipesListHandle.setItems(res.data.recipes, domSearch.val());
                    ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                    postsListHandle.setItems(res.data.posts, domSearch.val());
                    rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                });
            });
            domRoot.find('.show-more').click(function () {
                self.showMore();
            });
            domRoot.find('[name="from"]').change(function () {
                var val = $(this).val();
                var checked = $(this).prop('checked');
                var handle = null;
                switch (val){
                    case 'youtube':
                        handle = youtubeListHandle;
                        break;
                    case 'spreaker':
                        handle = podcastsListHandle;
                        break;
                    case 'twingly':
                        handle = blogsListHandle;
                        break;
                    case 'recipes':
                        handle = recipesListHandle;
                        break;
                    case 'ideabox':
                        handle = ideaboxListHandle;
                        break;
                    case 'posts':
                        handle = postsListHandle;
                        break;
                    case 'rssbposts':
                        handle = rssbPostsListHandle;
                        break;
                    case 'url':
                        handle = urlListHandle;
                        break;
                    default:
                        handle = youtubeListHandle;
                        break;
                }
                checked ? handle.show() : handle.hide();
            });
            domRoot.find('.i-am-done').click(function () {
                self.IAmDone();
            });
            domRoot.find('.results-from').find('.dropdown-menu').find('li').click(function () {
                var checkbox = $(this).find('input');
                checkbox.prop('checked', !checkbox.prop('checked')).change();
                return false;
            });
            domRoot.find('main.main-content').dndhover().on('dndHoverStart', function (event) {
                domRoot.find('[name="from"][value="url"]').prop('checked', true).change();;
                event.stopPropagation();
                event.preventDefault();
                return true;
            })
        }

        var PostsBlockClass = function (from) {
            var domResultsRoot, domBlockTitle;

            var self = this;
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];
            var grid, items, isAnimating;

            var ItemClass = function (itemData) {

                var domItemRoot = null, domTitleWrp;
                var self = this;

                var addedPost = false, addedSubscription = false;

                var bindEvents = function () {
                    domItemRoot.find('.select-post').parent().click(function () {
                        self.selected ? self.unSelectPost() : self.selectPost();
                    });
                }
                var readMore = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<a href="javascript:;">Read More</a>',
                        lessLink: '<a href="javascript:;">View Less</a>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                }

                var addToPost = function(){
                    var sets = self.getFormatData();
                    sets = postFormat(sets);
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}).then(function (res) {
                        addedPost = res.data;
                    });
                };

                var addToSubscription = function () {
                    var sets = self.getFormatData();
                    sets = subscriptionFormat(sets);
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: sets}).then(function (res) {
                        addedSubscription = res.data;
                    });
                }

                var deleteFromPost = function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: addedPost.post_ID}).then(function (res) {
                        addedPost = false;
                    });
                }
                var deleteFromSubscription = function () {
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'delete', where: addedSubscription.clientsubscription_ID}).then(function (res) {
                        addedSubscription = false;
                    });
                }

                this.html = function () {
                    var cloneDom = domBlocksContainer.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img.item-img').attr('src', itemData.image);
                    cloneDom.find('.title-wrapper .item-title').html(itemData.title);
                    var html = cloneDom.html();
                    cloneDom.remove();
                    return html;
                }
                this.selectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    btn.html("");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (addedPost) {
                                btn.removeClass("circle");
                                btn.addClass("filled");
                                $(".wrap img", domItemRoot).css("display", "block");
                                $("svg", domItemRoot).css("display", "none");
                                self.selected = true;
                                clearInterval(timer);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    addToPost();
                    is_loading_controlled_in_local = false;
                }
                this.unSelectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (!addedPost) {
                                btn.removeClass("circle");
                                btn.html("Select");
                                $(".wrap img", domItemRoot).css("display", "none");
                                $("svg", domItemRoot).css("display", "none");
                                self.selected = false;
                                clearInterval(timer);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    deleteFromPost();
                    is_loading_controlled_in_local = false;
                }
                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                }
                this.selected = false;
                this.setKeyword = function (kwd) {
                    self.keyword = kwd;
                }
                this.getFormatData = function () {
                    itemData.keyword = self.keyword;
                    return itemData;
                }
                this.init = function () {
                    bindEvents();
                    readMore();
                }
            }

            var formatItemData = function(itemData){
                var fData = {};
                switch (from) {
                    case 'youtube':
                        fData.title = itemData.snippet.title;
                        fData.summary = itemData.snippet.description;
                        fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                        fData.image = itemData.snippet.thumbnails.high.url;
                        fData.type = 2;
                        break;
                    case 'podcasts':
                        fData.title = itemData.title;
                        fData.summary = '';
                        fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                        fData.image = itemData.image_url;
                        fData.type = 0;
                        break;
                    case 'blog':
                        fData.title = itemData.title;
                        fData.summary = '';
                        fData.body = itemData.text;
                        fData.image = itemData.imgOrg;
                        fData.type = 7;
                        break;
                    case 'recipes':
                        fData.title = itemData.title;
                        fData.summary = '';
                        fData.body = itemData.source_url;
                        fData.image = itemData.image_url;
                        fData.type = 7;
                        break;
                    case 'ideabox':
                        fData.title = itemData.strIdeaBox_title;
                        fData.summary = '';
                        fData.body = itemData.strIdeaBox_idea;
                        fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                        fData.type = 7;
                        break;
                    case 'posts':
                        fData.title = itemData.strPost_title;
                        fData.summary = itemData.strPost_summary;
                        fData.body = itemData.strPost_body;
                        fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                        fData.type = itemData.intPost_type;
                        break;
                    case 'rssb-posts':
                        fData.title = itemData.strRSSBlogPosts_title;
                        fData.summary = itemData.strRSSBlogPosts_description;
                        fData.body = itemData.strRSSBlogPosts_content;
                        fData.image = DEFAULT_IMAGE;
                        fData.type = 7;
                        break;
                    default:
                        fData.title = itemData.strPost_title;
                        fData.summary = itemData.strPost_summary;
                        fData.body = itemData.strPost_body;
                        fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                        fData.type = itemData.intPost_type;
                        break;
                }
                return fData;
            }

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = $(el).find( '> *' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild.length ) {
                        itemChild.addClass('tt-old');
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('>*:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(formatItemData(item));
                    itemHandle.setKeyword(newKeyword);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }

            this.keyword = '';
            this.nextPageToken = false;

            this.show = function () {
                domResultsRoot.show();
            }

            this.hide = function () {
                domResultsRoot.hide();
            }

            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }

            this.emptySelectedItems = function () {
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        itemHandle.unSelectPost();
                    }
                });
            }

            this.init = function () {
                domResultsRoot = domBlocksContainer.find('.tt-grid-wrapper.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domBlocksContainer);
                domBlockTitle = domResultsRoot.find('.from-where');
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                switch (from) {
                    case 'youtube':
                        domBlockTitle.html('Results from Youtube');
                        break;
                    case 'podcasts':
                        domBlockTitle.html('Results from Podcasts');
                        break;
                    case 'blog':
                        domBlockTitle.html('Results from Blogs');
                        break;
                    case 'recipes':
                        domBlockTitle.html('Results from Recipes');
                        break;
                    case 'ideabox':
                        domBlockTitle.html('Results from Ideabox');
                        break;
                    case 'posts':
                        domBlockTitle.html('Results from Posts');
                        break;
                    case 'rssb-posts':
                        domBlockTitle.html('Results from RSSBlogPost');
                        break;
                    default:
                        domBlockTitle.html('Results from Posts');
                        break;

                }
                isAnimating = false;
            }
        }

        var MetaPostsBlockClass = function (from) {
            var domResultsRoot, domBlockTitle, domNewAdder;

            var self = this;
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];
            var grid, items, isAnimating;
            var maxId = 0;
            var isDragOnchild = false;

            var ItemClass = function (itemData) {

                var domItemRoot = null, domTitleWrp, domImgWrp, domBodyWrp;
                var self = this;
                var addedPost = itemData.orgData, addedSubscription = false;

                var bindEvents = function () {
                    domItemRoot.find('.select-post').parent().click(function () {
                        itemData.selected ? self.unSelectPost() : self.selectPost();
                    });

                    domImgWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                    })
                        .on('dragover dragenter', function(e) {

                            domResultsRoot.addClass('is-dragover');
                            domImgWrp.addClass('is-dragover');
                            isDragOnchild = true;
                        })
                        .on('dragleave dragend drop', function() {
                            domImgWrp.removeClass('is-dragover');
                            isDragOnchild = false;
                        })
                        .on('drop', function(e) {
                            domResultsRoot.removeClass('is-dragover');
                            var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                            if (dataInfo.type === 'image'){
                                var droppedFile = dataInfo.file;
                                uploadImageFile(droppedFile);
                                return true;
                            }
                            else if (dataInfo.type === 'url' && dataInfo.contentType === 'image') {
                                itemData.image = dataInfo.url;
                                domImgWrp.addClass('is-uploading');
                                bindData();
                                update();
                                return true;
                            }
                            alert('Sorry, This is not Image file');
                            // self.updateImage(droppedFile);
                        });
                    domImgWrp.find('img').on('load', function () {
                        domImgWrp.removeClass('is-uploading');
                    });
                    domImgWrp.find('input[name="item_image"]').change(function () {
                        if ($(this).prop('files').length){
                            var file = $(this).prop('files')[0];
                            if (checkFile(file) === 'image') {
                                uploadImageFile(file);
                            }
                            else {
                                alert('sorry! this is not image file');
                            }
                        }
                    });
                    domTitleWrp.find('.edit-entry').click(function () {
                        domTitleWrp.find('textarea').val(itemData.title);
                        domTitleWrp.addClass('edit-status');
                        domItemRoot.parent().addClass('editing');
                    });

                    domTitleWrp.find('.save-entry').click(function () {
                        itemData.title = domTitleWrp.find('textarea').val();
                        itemData.editStatus = 'creating';
                        domTitleWrp.removeClass('edit-status');
                        bindData();
                        update();
                        if (!domBodyWrp.hasClass('edit-status')) {
                            domItemRoot.parent().removeClass('editing');
                        }
                    });

                    domTitleWrp.find('.back-to-origin').click(function () {
                        domTitleWrp.removeClass('edit-status');
                        bindData();
                        if (!domBodyWrp.hasClass('edit-status')) {
                            domItemRoot.parent().removeClass('editing');
                        }
                    });

                    domBodyWrp.find('.edit-entry').click(function () {
                        domBodyWrp.find('textarea').val(itemData.body);
                        domBodyWrp.addClass('edit-status');
                        domItemRoot.parent().addClass('editing');
                    });

                    domBodyWrp.find('.save-entry').click(function () {
                        itemData.body = domBodyWrp.find('textarea').val();
                        itemData.editStatus = 'creating';
                        domBodyWrp.removeClass('edit-status');
                        bindData();
                        update();
                        if (!domTitleWrp.hasClass('edit-status')) {
                            domItemRoot.parent().removeClass('editing');
                        }
                    });

                    domBodyWrp.find('.back-to-origin').click(function () {
                        domBodyWrp.removeClass('edit-status');
                        bindData();
                        if (!domTitleWrp.hasClass('edit-status')) {
                            domItemRoot.parent().removeClass('editing');
                        }
                    });
                    domBodyWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                    })
                        .on('dragover dragenter', function(e) {

                            domResultsRoot.addClass('is-dragover');
                            domBodyWrp.addClass('is-dragover');
                            isDragOnchild = true;
                        })
                        .on('dragleave dragend drop', function() {
                            domBodyWrp.removeClass('is-dragover');
                            isDragOnchild = false;
                        })
                        .on('drop', function(e) {
                            domResultsRoot.removeClass('is-dragover');
                            var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                            if (dataInfo.type !== 'image' && dataInfo.type !== 'other' && dataInfo.type !== 'url'){
                                var droppedFile = dataInfo.file;
                                uploadBodyFile(droppedFile);
                                return true;
                            }
                            else if (dataInfo.type === 'url' && dataInfo.contentType === 'video') {
                                itemData.body = dataInfo.url;
                                itemData.type = 2;
                                bindData();
                                update();
                                return true;
                            }
                            else if (dataInfo.type === 'url' && dataInfo.contentType === 'youtube') {
                                setWithYoutube(dataInfo.url);
                                return true;
                            }
                            alert('Sorry, This is not Valid Type');
                        });
                    domItemRoot.find('.delete-item').click(function () {
                        swal({
                            title: "Are you sure?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                deleteItem();
                            }
                        });
                    });
                    domItemRoot.find('select[name="type"]').change(function () {
                        itemData.type = $(this).val();
                        update();
                    });
                }
                var setWithYoutube = function(url){
                    var parse = helperHandle.parseYoutubeUrl(url);
                    ajaxAPiHandle.apiPost('Api.php', {action: 'get', what: 'youtube', 'where': {id: parse.id}}).then(function (res) {
                        var fData = formatItemData(res.data, 'youtube');
                        $.extend(itemData, fData);
                        itemData.body = url;
                        bindData();
                        update();
                    });
                }
                var bindData = function () {
                    if (itemData.image){
                        domImgWrp.addClass('setted');
                        domImgWrp.find('img.item-img').attr('src', itemData.image);
                    }
                    else {
                        domImgWrp.removeClass('setted');
                    }
                    domTitleWrp.find('.item-title').html(itemData.title);
                    // domTitleWrp.find('textarea').val(itemData.title);

                    domBodyWrp.find('.item-description').html(itemData.body);
                    // domBodyWrp.find('textarea').val(itemData.body);

                    domItemRoot.find('.type-wrapper select').val(itemData.type);
                    readMore();

                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    if (itemData.selected) {
                        domItemRoot.find('.wrap .select-post').addClass('filled');
                        domItemRoot.find('.wrap .select-post').html('');
                        domItemRoot.find('.wrap img').show();
                    }
                    else {
                        domItemRoot.find('.wrap .select-post').removeClass('filled');
                        domItemRoot.find('.wrap .select-post').html('Select');
                        domItemRoot.find('.wrap img').hide();
                    }
                }
                var readMore = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 24,
                        moreLink: '<a href="javascript:;">Read More</a>',
                        lessLink: '<a href="javascript:;">View Less</a>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            // domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    // domTitleWrp.addClass('collapsed');

                    domBodyWrp.find('.item-description').readmore({
                        collapsedHeight: 47,
                        moreLink: '<a href="javascript:;">Read More</a>',
                        lessLink: '<a href="javascript:;">View Less</a>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            // domBodyWrp.toggleClass('collapsed');
                        }
                    });
                    // domBodyWrp.addClass('collapsed');
                }
                var uploadImageFile = function (file) {
                    domImgWrp.addClass('is-uploading');

                    var data = new FormData();
                    var sets = {
                        prefix: 'post'
                    };
                    data.append('file', file);
                    data.append('sets', JSON.stringify(sets));
                    ajaxAPiHandle.multiPartApiPost('Uploads.php', data).then(function (res) {
                        if (res.status) {
                            itemData.image = res.data.url;
                            bindData();
                            update();
                        }
                    });
                }
                var update = function () {
                    var sets = postFormat(itemData);
                    return ajaxAPiHandle.apiPost('Posts.php', {action: 'update', sets: sets, where: {post_ID: itemData.orgData.post_ID}});
                }
                var uploadBodyFile = function (file) {
                    domBodyWrp.addClass('is-uploading');

                    var data = new FormData();
                    var sets = {
                        prefix: 'post'
                    };
                    switch ( checkFile(file) ) {
                        case 'video':
                            sets.where = 'assets/media/video';
                            break;
                        case 'audio':
                            sets.where = 'assets/media/audio';
                            break;
                        case 'document':
                            sets.where = 'assets/media/document';
                            break;
                    }
                    data.append('file', file);
                    data.append('sets', JSON.stringify(sets));
                    ajaxAPiHandle.multiPartApiPost('Uploads.php', data).then(function (res) {
                        if (res.status) {
                            switch ( checkFile(file) ) {
                                case 'video':
                                    itemData.body = res.data.abs_url;
                                    itemData.type = 2;
                                    break;
                                case 'audio':
                                    itemData.body = res.data.abs_url;
                                    itemData.type = 0;
                                    break;
                                case 'document':
                                    itemData.body += '<a href="'+ res.data.abs_url +'">' + res.data.abs_url + '</a>';
                                    itemData.type = 7;
                                    break;
                            }
                            bindData();
                            update();
                        }
                    });
                }
                var deleteItem = function () {
                    if (!addedPost) {
                        allItemHandles.forEach(function (hdl, i) {
                            if (hdl.id == self.id) {
                                allItemHandles.splice(i, 1);
                                domItemRoot.parent().remove();
                            }
                        });
                        return true;
                    }
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: itemData.orgData.post_ID}).then(function (res) {
                        allItemHandles.forEach(function (hdl, i) {
                            if (hdl.id == self.id) {
                                allItemHandles.splice(i, 1);
                                domItemRoot.parent().remove();
                            }
                        });
                    });
                }

                var addToPost = function(){
                    var sets = self.getFormatData();
                    sets = postFormat(sets);
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}).then(function (res) {
                        addedPost = res.data;
                    });
                };
                var addToSubscription = function () {
                    var sets = self.getFormatData();
                    sets = subscriptionFormat(sets);
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'insert', sets: sets}).then(function (res) {
                        addedSubscription = res.data;
                    });
                }

                var deleteFromPost = function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: addedPost.post_ID}).then(function (res) {
                        addedPost = false;
                    });
                }
                var deleteFromSubscription = function () {
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'delete', where: addedSubscription.clientsubscription_ID}).then(function (res) {
                        addedSubscription = false;
                    });
                }

                this.selected = function () {
                    return itemData.selected;
                }
                this.dom = function () {
                    if (domItemRoot == null) {
                        domItemRoot = domMetaBlocksContainer.find('.item-sample').clone().removeAttr('hidden').removeClass('item-sample');
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domImgWrp = domItemRoot.find('.img-wrapper');
                        domBodyWrp = domItemRoot.find('.description-wrapper');
                    }
                    return domItemRoot;
                }
                this.selectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    btn.html("");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (addedPost) {
                                btn.removeClass("circle");
                                btn.addClass("filled");
                                $(".wrap img", domItemRoot).css("display", "block");
                                $("svg", domItemRoot).css("display", "none");
                                itemData.selected = true;
                                clearInterval(timer);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    addToPost();
                    is_loading_controlled_in_local = false;
                }
                this.unSelectPost = function () {
                    var btn = domItemRoot.find('.buttons-wrapper .wrap .select-post');
                    btn.removeClass("filled");
                    btn.addClass("circle");
                    $(".wrap  svg", domItemRoot).css("display", "block");
                    $(".circle_2", domItemRoot).attr("class", "circle_2 fill_circle");
                    var timer = setInterval(
                        function tick() {
                            if (!addedPost) {
                                btn.removeClass("circle");
                                btn.html("Select");
                                $(".wrap img", domItemRoot).css("display", "none");
                                $("svg", domItemRoot).css("display", "none");
                                itemData.selected = false;
                                clearInterval(timer);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    deleteFromPost();
                    is_loading_controlled_in_local = false;
                }
                this.setKeyword = function (kwd) {
                    self.keyword = kwd;
                }
                this.getFormatData = function () {
                    itemData.keyword = self.keyword;
                    return {
                        title: itemData.title,
                        body: itemData.body,
                        image: itemData.image,
                        type: itemData.type,
                        keyword: self.keyword,
                        summary: '',
                    };
                }
                this.init = function () {
                    if (itemData.editStatus == 'empty' || itemData.editStatus == 'creating') {
                        domTitleWrp.addClass('edit-status');
                        domBodyWrp.addClass('edit-status');
                        domTitleWrp.find('textarea').val(itemData.title);
                        domBodyWrp.find('textarea').val(itemData.body);
                        domItemRoot.parent().addClass('editing');
                    }
                    bindData();
                    bindEvents();
                    self.keyword = '';
                    self.id = maxId + 1;
                    maxId++;
                }
            }

            var formatItemData = function(itemData, from){
                var fData = {};
                switch (from) {
                    case 'youtube':
                        fData.title = itemData.snippet.title;
                        fData.summary = itemData.snippet.description;
                        fData.body = itemData.id;
                        fData.image = itemData.snippet.thumbnails.high.url;
                        fData.type = 2;
                        break;
                    case 'podcasts':
                        fData.title = itemData.title;
                        fData.summary = '';
                        fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                        fData.image = itemData.image_url;
                        fData.type = 0;
                        break;
                    case 'blog':
                        fData.title = itemData.title;
                        fData.summary = '';
                        fData.body = itemData.text;
                        fData.image = itemData.imgOrg;
                        fData.type = 7;
                        break;
                    case 'recipes':
                        fData.title = itemData.title;
                        fData.summary = '';
                        fData.body = itemData.source_url;
                        fData.image = itemData.image_url;
                        fData.type = 7;
                        break;
                    case 'ideabox':
                        fData.title = itemData.strIdeaBox_title;
                        fData.summary = '';
                        fData.body = itemData.strIdeaBox_idea;
                        fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                        fData.type = 7;
                        break;
                    case 'posts':
                        fData.title = itemData.strPost_title;
                        fData.summary = itemData.strPost_summary;
                        fData.body = itemData.strPost_body;
                        fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                        fData.type = itemData.intPost_type;
                        break;
                    case 'rssb-posts':
                        fData.title = itemData.strRSSBlogPosts_title;
                        fData.summary = itemData.strRSSBlogPosts_description;
                        fData.body = itemData.strRSSBlogPosts_content;
                        fData.image = DEFAULT_IMAGE;
                        fData.type = 7;
                        break;
                    default:
                        fData.title = itemData.strPost_title;
                        fData.summary = itemData.strPost_summary;
                        fData.body = itemData.strPost_body;
                        fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                        fData.type = itemData.intPost_type;
                        break;
                }
                return fData;
            }

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = $(el).find( '> *' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild.length ) {
                        itemChild.addClass('tt-old');
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('>*:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            var addNew = function (data) {
                if (typeof data == "undefined") {
                    data = {};
                }
                var itemData = {
                    title: '',
                    body: '',
                    image: '',
                    type: 7,
                    summary: '',
                    keyword: '',
                    selected: true,
                    editStatus: 'creating'
                }
                $.extend(itemData, data);
                var postData = postFormat(itemData);

                ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: postData}).then(function (res) {
                    itemData.orgData = res.data;
                    var handle = new ItemClass(itemData);
                    $(grid).append($('<li></li>').append(handle.dom()));
                    handle.init();
                    allItemHandles.push(handle);
                });
            }
            var addByFile = function (file) {
                var ajaxData = new FormData();
                var sets = {};
                var fileType = checkFile(file);
                switch (fileType) {
                    case 'video':
                        sets.where = 'assets/media/video';
                        break;
                    case 'audio':
                        sets.where = 'assets/media/audio';
                        break;
                    case 'image':
                        sets.where = 'assets/images';
                        break;
                    case 'document':
                        sets.where = 'assets/media/document';
                        break;
                }
                sets.prefix = 'post';
                ajaxData.append('file', file);
                ajaxData.append('sets', JSON.stringify(sets));
                ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData).then(function (res) {
                    var itemData = {};
                    switch (fileType) {
                        case 'video':
                            itemData.type = 2;
                            itemData.body = res.data.url;
                            break;
                        case 'audio':
                            itemData.type = 0;
                            itemData.body = res.data.url;
                            break;
                        case 'image':
                            itemData.image = res.data.url;
                            break;
                        case 'document':
                            itemData.body = '<a href="' + res.data.url + '">' + res.data.url + '</a>';
                            break;
                    }
                    addNew(itemData);
                });
            }
            var addByUrl = function (url) {
                var urlType  = checkUrl(url);
                var itemData = {};
                switch (urlType) {
                    case 'video':
                        itemData.type = 2;
                        itemData.body = url;
                        break;
                    case 'audio':
                        itemData.type = 0;
                        itemData.body = url;
                        break;
                    case 'image':
                        itemData.image = url;
                        break;
                    case 'youtube':
                        var parse = helperHandle.parseYoutubeUrl(url);
                        ajaxAPiHandle.apiPost('Api.php', {action: 'get', where: {id: parse.id}, what: 'youtube'}).then(function (res) {
                            var itemData = formatItemData(res.data, 'youtube');
                            itemData.body = url;
                            itemData.editStatus = 'pending';
                            addNew(itemData);
                        });
                        return ;
                        break;
                    default:
                        itemData.body = '<a href="' + url + '">' + url + '</a>';
                        break;
                }
                addNew(itemData);
            }

            var bindEvents = function () {
                domNewAdder.find('> *').click(function () {
                    addNew({editStatus: 'empty'});
                });
                domResultsRoot.dndhover().on('dndHoverStart', function (event) {
                    domResultsRoot.addClass('is-dragover');
                    domResultsRoot.addClass('is-dragover-on-parent');
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                }).on('dndHoverEnd', function (event) {
                    if (isDragOnchild === false){
                        domResultsRoot.removeClass('is-dragover');
                    }
                    domResultsRoot.removeClass('is-dragover-on-parent');
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                });
                domResultsRoot.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                })
                domResultsRoot.on('drop', function (e) {
                    domResultsRoot.removeClass('is-dragover');
                    domResultsRoot.removeClass('is-dragover-on-parent');
                    var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                    switch (dataInfo.type) {
                        case 'video':
                        case 'audio':
                        case 'document':
                        case 'image':
                            addByFile(dataInfo.file);
                            break;
                        case 'url':
                            addByUrl(dataInfo.url);
                            break;
                    }
                });
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(formatItemData(item));
                    itemHandle.setKeyword(newKeyword);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.keyword = '';
            this.nextPageToken = false;

            this.show = function () {
                domResultsRoot.show();
                self.isShown = true;
            }

            this.hide = function () {
                domResultsRoot.hide();
                self.isShown = false;
            }

            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }

            this.emptySelectedItems = function () {
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        itemHandle.unSelectPost();
                    }
                });
            }

            this.init = function () {
                domResultsRoot = domMetaBlocksContainer.find('.tt-grid-wrapper.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domMetaBlocksContainer);

                domBlockTitle = domResultsRoot.find('.from-where');
                domNewAdder = domResultsRoot.find('.new-adder');
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                isAnimating = false;
                bindEvents();
            }
        }

        var goTo_addNewPosts = function(){
            $('<form action="createseriescontent" method="post" hidden><input name="from_page" value="editseries" /><input name="id" value="'+ createdSeries.series_ID +'"/></form>').appendTo('body').submit().remove();
        };

        this.searchPostsByKeyword = searchPostsByKeyword;

        this.IAmDone = function () {
            swal({
                icon: "success",
                title: 'SUCCESS!',
                text: 'All selected posts are added to this series.',
                buttons: {
                    returnHome: {
                        text: "RETURN HOME",
                        value: 'return_home',
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    customize: {
                        text: "CUSTOMIZE",
                        value: 'customize',
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                closeOnClickOutside: false,
            }).then(function (value) {
                if (value == 'return_home'){
                    self.viewUserPage(createdSeries.series_ID);
                }
                else {
                    self.viewEditSeries(createdSeries.series_ID);
                }
            });
        }
        this.viewUserPage = function (id) {
            $('<form action="userpage" method="post" hidden><input name="id" value="'+ id +'"></form>').appendTo('body').submit().remove();
        }
        this.viewEditSeries = function (id) {
            $('<form action="editseries" method="post" hidden><input name="id" value="'+ id +'"></form>').appendTo('body').submit().remove();
        }

        this.showMore = function () {
            var kwd = youtubeListHandle.keyword;
            var pageTokenY = youtubeListHandle.nextPageToken;
            var pageTokenS = podcastsListHandle.nextPageToken;
            var pageTokenT = blogsListHandle.nextPageToken;
            var pageTokenF = recipesListHandle.nextPageToken;
            var pageTokenI = ideaboxListHandle.nextPageToken;
            var pageTokenP = postsListHandle.nextPageToken;
            var pageTokenRssbP = rssbPostsListHandle.nextPageToken;
            self.searchPostsByKeyword(kwd, pageTokenY, pageTokenS, pageTokenT, pageTokenF, pageTokenI, pageTokenP, pageTokenRssbP).then(function (res) {
                youtubeListHandle.setItems(res.data.youtube, kwd);
                podcastsListHandle.setItems(res.data.spreaker, kwd);
                blogsListHandle.setItems(res.data.blogs, kwd);
                recipesListHandle.setItems(res.data.recipes, kwd);
                ideaboxListHandle.setItems(res.data.ideabox, kwd);
                postsListHandle.setItems(res.data.posts, kwd);
                rssbPostsListHandle.setItems(res.data.rssbPosts, kwd);
            })
        }
        this.setSearchTerm = function (new_term) {
            domSearch.val(new_term);
            return new Promise(function (resolve, reject) {
                searchPostsByKeyword(domSearch.val(), false, false, false, false, false, false, false).then(function (res) {
                    setTimeout(function () {
                        resolve();
                    }, 300);
                    setTimeout(function () {
                        youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                        podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                        blogsListHandle.setItems(res.data.blogs, domSearch.val());
                        recipesListHandle.setItems(res.data.recipes, domSearch.val());
                        ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                        postsListHandle.setItems(res.data.posts, domSearch.val());
                        rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                    }, 1000);
                });
            })
        }

        var checkUrl = function (url) {
            if(url.match(/\.(jpeg|jpg|gif|png)$/) != null){
                return 'image';
            }
            if (url.match(/\.(mp4|avi)$/)) {
                return 'video';
            }
            if (url.match(/\.(pdf|text|txt)$/)) {
                return 'document';
            }
            if (helperHandle.getApiTypeFromUrl(url) == 'youtube') {
                return 'youtube';
            }
            return 'other';
        }
        var checkFile = function (file) {
            var type = file.type;
            if (accept_image_type.indexOf(type) > -1){
                return 'image';
            }else if (accept_doc_types.indexOf(type) > -1){
                return 'document'
            } else if (accept_video_types.indexOf(type) > -1){
                return 'video';
            } else if (accept_audio_types.indexOf(type) > -1){
                return 'audio';
            } else {
                return {type: 'other'};
            }
        }
        var getDataInfo = function (dataTransfer) {

            var droppedUrl = dataTransfer.getData('URL');

            if(droppedUrl != ''){
                return {url: droppedUrl, type: 'url', contentType: checkUrl(droppedUrl)}
            }

            if (dataTransfer.files.length < 1){
                return {type: 'other'};
            }
            return { type: checkFile(dataTransfer.files[0]), file: dataTransfer.files[0] };
        }
        var setupDragFn = function () {
            $.fn.dndhover = function(options) {
                return this.each(function() {

                    var self = $(this);
                    var collection = $();

                    self.on('dragenter', function(event) {
                        if (collection.length === 0) {

                            self.trigger('dndHoverStart');
                        }
                        collection = collection.add(event.target);
                    });

                    self.on('dragleave', function(event) {
                        /*
                         * Firefox 3.6 fires the dragleave event on the previous element
                         * before firing dragenter on the next one so we introduce a delay
                         */
                        setTimeout(function() {
                            collection = collection.not(event.target);
                            if (collection.length === 0) {
                                self.trigger('dndHoverEnd');
                            }
                        }, 100);
                    });
                });
            };

            isAdvancedUpload = function() {
                var div = document.createElement('div');
                return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
            }();
        }

        this.init = function () {
            setupDragFn();

            domBlocksContainer = domRoot.find('main.main-content .posts-block-container');
            domMetaBlocksContainer = domRoot.find('main.main-content .meta-posts-block-container');

            urlListHandle = new MetaPostsBlockClass('url');

            youtubeListHandle = new PostsBlockClass('youtube');
            podcastsListHandle = new PostsBlockClass('podcasts');
            blogsListHandle = new PostsBlockClass('blog');
            recipesListHandle = new PostsBlockClass('recipes');
            ideaboxListHandle = new PostsBlockClass('ideabox');
            postsListHandle = new PostsBlockClass('posts');
            rssbPostsListHandle = new PostsBlockClass('rssb-posts');

            bindEvents();
            urlListHandle.init();
            youtubeListHandle.init();
            podcastsListHandle.init();
            blogsListHandle.init();
            recipesListHandle.init();
            ideaboxListHandle.init();
            postsListHandle.init();
            rssbPostsListHandle.init();

            domRoot.find('[name="from"]').each(function () {
                var val = $(this).val();
                var checked = $(this).prop('checked');
                var handle = null;
                switch (val){
                    case 'url':
                        handle = urlListHandle;
                        break;
                    case 'youtube':
                        handle = youtubeListHandle;
                        break;
                    case 'spreaker':
                        handle = podcastsListHandle;
                        break;
                    case 'twingly':
                        handle = blogsListHandle;
                        break;
                    case 'recipes':
                        handle = recipesListHandle;
                        break;
                    case 'ideabox':
                        handle = ideaboxListHandle;
                        break;
                    case 'posts':
                        handle = postsListHandle;
                        break;
                    case 'rssbposts':
                        handle = rssbPostsListHandle;
                        break;
                    default:
                        handle = youtubeListHandle;
                        break;
                }
                checked ? handle.show() : handle.hide();
            });
        }
    }

    createSeriesHandle = new CreateSeriesClass();
    createSeriesHandle.init();
    selectPostsHandle = new SelectPostsClass();
    selectPostsHandle.init();

    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });

})();