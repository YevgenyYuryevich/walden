(function () {
    'use strict';

    var viewHandle;
    var is_loading_controlled_in_local = false;

    var ViewClass = function () {

        var domRoot = $('.site-wrapper.page');
        var domArticle = domRoot.find('article.blog-wrapper');
        var postData = initialPostData;

        var stickyHaderHandle, stickyFooterHandle;
        var fontSizeChangeUnit = 1;
        var defaultFontSize = 24;

        var makeAudio = function () {
            var settings_ap = {
                disable_volume: 'off'
                ,disable_scrub: 'default'
                ,design_skin: 'skin-wave'
                ,skinwave_dynamicwaves:'off'
            };
            dzsag_init('#ag1',{
                'transition':'fade'
                ,'autoplay' : 'on'
                ,'settings_ap':settings_ap
            });
        }
        let makeVideo = function () {
            let domVideo = domArticle.find('video.video-js');
            if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}] , "customControlsOnMobile": true };
                domVideo.attr('data-setup', JSON.stringify(setup));
            }
            else {
                domVideo.append('<source src="'+ postData.body +'" />');
            }
            videojs(domVideo.get(0), {}, function () {
                this.one('timeupdate', function() {
                    domArticle.find('.blog-duration').hide();
                });
            });
        }
        var setReadingTime = function () {
            $('article.blog-wrapper').readingTime({
                wordCountTarget: '.words',
                wordsPerMinute: 200
            });
        }

        var StickyHaderClass = function () {
            var domStickyHeader = domRoot.find('.sticky-header');
            var appearStatus = false;

            var bindEvents = function () {
                $(window).scroll(function () {
                    if ($(this).get(0).scrollY > 120 && !appearStatus){
                        appearStatus = true;
                        domStickyHeader.removeClass('disappear');
                        domStickyHeader.addClass('appear');
                        domArticle.find('.blog-header').addClass('disappear');
                        domArticle.find('.blog-header').removeClass('appear');
                    }
                    if ($(this).get(0).scrollY <= 120 && appearStatus){
                        appearStatus = false;
                        domStickyHeader.removeClass('appear');
                        domStickyHeader.addClass('disappear');
                        domArticle.find('.blog-header').removeClass('disappear');
                        domArticle.find('.blog-header').addClass('appear');
                    }
                });
            }

            this.init = function () {
                bindEvents();
            }
        }

        var StickyFooterClass = function () {

            var domStickyFooter = domRoot.find('.sticky-footer');

            var speakerHandle;

            var bindEvents = function () {
                domStickyFooter.find('.reduce-fontsize').click(function () {
                    var preFontSize = $('html').css('font-size');
                    var newFontSize = parseInt(preFontSize) - fontSizeChangeUnit;
                    newFontSize = newFontSize > 10 ? newFontSize : 10;
                    $('html').css('font-size', newFontSize + 'px');
                });
                domStickyFooter.find('.increase-fontsize').click(function () {
                    var preFontSize = $('html').css('font-size');
                    var newFontSize = parseInt(preFontSize) + fontSizeChangeUnit;
                    newFontSize = newFontSize < 40 ? newFontSize : 40;
                    $('html').css('font-size', newFontSize + 'px');
                });
                domStickyFooter.find('.default-fontsize').click(function () {
                    $('html').css('font-size', defaultFontSize);
                })
            }

            var SpeakerClass = function () {

                var domSpeaker = domStickyFooter.find('.speaker-wrapper');

                var setupLanguage = function () {

                    if ('speechSynthesis' in window) {
                        // window.speechSynthesis.onvoiceschanged = function (ev) {
                        var voiceList = domSpeaker.find('[name="speaker_lang"]');
                        if(voiceList.find('option').length == 0) {
                            speechSynthesis.getVoices().forEach(function(voice, index) {
                                var $option = $('<option>')
                                    .val(index)
                                    .html(voice.name + (voice.default ? ' (default)' :''));
                                voiceList.append($option);
                            });
                            voiceList.material_select();
                        }
                        // }
                    }
                }

                var blockTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'div', 'p'];
                var msg = new SpeechSynthesisUtterance();
                var cleanText = function (dirtyText) {

                    blockTags.forEach(function (blockTag) {
                        dirtyText = dirtyText.replace('</' + blockTag + '>', '\n</' + blockTag + '>');
                    });
                    return $('<div>' + dirtyText + '</div>').text();
                }

                var speakText = function (text) {
                    setTimeout(function () {
                        speechSynthesis.cancel();
                        var voices = window.speechSynthesis.getVoices();
                        msg.voice = voices[domSpeaker.find('[name="speaker_lang"]').val()];
                        msg.rate = domSpeaker.find('[name="speaker_rate"]').val() / 10;
                        msg.pitch = domSpeaker.find('[name="speaker_pitch"]').val();
                        msg.text = text;
                        msg.volume = 1;
                        console.log(msg);
                        window.speechSynthesis.speak(msg);
                    }, 10);
                }

                var makePostText = function () {
                    var text = postData.seriesTitle + '\n\n\n\n';
                    text += postData.title + '\n\n';
                    text += postData.body;
                    return cleanText(text);
                }

                var bindEvents = function () {
                    domSpeaker.find('.play-speaker').click(function () {
                        if (speechSynthesis.pending){
                            return false;
                        }

                        if (speechSynthesis.paused){
                            window.speechSynthesis.resume();
                        }
                        else {
                            var text = makePostText();
                            speakText(text);
                        }
                    });



                    var pendingFunc = function () {
                        domSpeaker.find('.pause-speaker').click(function () {
                            if (window.speechSynthesis.pending || window.speechSynthesis.paused){
                                return false;
                            }

                            window.speechSynthesis.pause();
                        });
                        return true;
                    }();

                    msg.onstart = function(event) {
                        console.log('stat', event);
                        domSpeaker.removeClass('pause');
                        domSpeaker.addClass('speaking');
                    };
                    msg.onpause = function (ev) {
                        console.log('paused', ev);
                        domSpeaker.removeClass('speaking');
                        domSpeaker.addClass('pause');
                    }
                    msg.onresume = function (ev) {
                        console.log('resume', ev);
                        domSpeaker.removeClass('pause');
                        domSpeaker.addClass('speaking');

                    }
                    msg.onend = function(event) {
                        console.log('end', event);
                        domSpeaker.removeClass('pause');
                        domSpeaker.removeClass('speaking');
                    };

                };

                this.init = function () {
                    setupLanguage();
                    window.speechSynthesis.cancel();
                    setTimeout(function () {
                        var voices = window.speechSynthesis.getVoices();
                        msg.voice = voices[0];
                        msg.volume = 0;
                        msg.text = '';
                        window.speechSynthesis.speak(msg);
                    }, 100);

                    bindEvents();
                }
            }

            this.init = function () {
                $(function () {
                    speakerHandle = new SpeakerClass();
                    speakerHandle.init();
                });
                bindEvents();
            }
        }

        var removePluginsEvents = function () {
            $('svg').mousedown(function (e) {
                return false;
            });
        }

        this.init = function () {
            removePluginsEvents();
            switch (parseInt(postData.type)){
                case 0:
                    makeAudio();
                    break;
                case 2:
                    makeVideo();
                    break;
                case 8:
                    stickyFooterHandle = new StickyFooterClass();
                    stickyFooterHandle.init();
                    setReadingTime();
                    break;
                default:
                    stickyFooterHandle = new StickyFooterClass();
                    stickyFooterHandle.init();
                    setReadingTime();
                    break;
            }
            stickyHaderHandle = new StickyHaderClass();
            stickyHaderHandle.init();
        }
    }

    viewHandle = new ViewClass();
    viewHandle.init();

    $(document).ajaxStart(function () {
        if(!is_loading_controlled_in_local) {
            $('.loading').css('display', 'block');
        }
    });
    $(document).ajaxComplete(function () {
        if(!is_loading_controlled_in_local){
            $('.loading').css('display', 'none');
        }
    });
})();