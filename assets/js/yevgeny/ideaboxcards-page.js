(function () {
    'use strict';

    var mainHandle;
    var is_loading_controlled_in_local = false;
    var accept_doc_types = ['application/pdf','text/plain'];
    var accept_image_type = ['image/png', 'image/jpeg'];

    var strToTime = function (str) {
        var str_date = str;
        if (str_date !== false){
            var parts = str_date.split('/');
            var date = new Date(parts[2], parts[0] - 1, parts[1]);
            return date.getTime();
        }
        else {
            return 99999999999999;
        }
    }

    var MainClass = function () {

        var domRoot = $('.site-wrapper .content-wrapper');
        var domGridWrp = domRoot.find('.tt-grid-wrapper');

        var self = this;

        var ideaboxes = initialIdeaboxes;
        var totalSiblingIdeaboxes = [];
        var parentIds = [];
        var series = [];

        var totalSiblingHandles = [];
        var siblingHandles = [];
        var currentParentId = initialParentId;

        var isAdvancedUpload;
        var isDragOnchild = false;

        var siblingIdeabox = function () {
            totalSiblingIdeaboxes[0] = [];
            ideaboxes.forEach(function (ideabox) {
                if (typeof totalSiblingIdeaboxes[ideabox.intIdeaBox_subcategory] == 'undefined'){
                    totalSiblingIdeaboxes[ideabox.intIdeaBox_subcategory] = [];
                }
                totalSiblingIdeaboxes[ideabox.intIdeaBox_subcategory].push(ideabox);
                totalSiblingIdeaboxes[ideabox.ideabox_ID] = [];
                parentIds[ideabox.ideabox_ID] = ideabox.intIdeaBox_subcategory;
            });
        }
        var getEarliestDeadline = function (item) {
            if (item.next_deadline === -1){
                var siblingIdeaboxes = totalSiblingIdeaboxes[item.ideabox_ID];

                var earliest_deadline_time = strToTime(item.dtIdeaBox_deadline);
                var earliest_deadline = item.dtIdeaBox_deadline;
                var sub_cats_len = siblingIdeaboxes.length;

                if (item.boolIdeaBox_done == '1'){
                    item.next_deadline = false;
                    return false;
                }

                for (var i = 0; i < sub_cats_len; i ++){
                    var sub_earliest_deadline = getEarliestDeadline(siblingIdeaboxes[i]);
                    var sub_earliest_deadline_time = strToTime(sub_earliest_deadline);

                    if (earliest_deadline_time > sub_earliest_deadline_time){
                        earliest_deadline_time = sub_earliest_deadline_time;
                        earliest_deadline = sub_earliest_deadline;
                    }
                }
                item.next_deadline = earliest_deadline;
                return earliest_deadline;
            }
            else {
                return item.next_deadline;
            }
        }
        var getDataInfo = function (dataTransfer) {

            var droppedUrl = dataTransfer.getData('URL');

            if(droppedUrl != ''){
                return {url: droppedUrl, type: 'url'}
            }

            if (dataTransfer.files.length < 1){
                return {type: 'other'};
            }

            var type = dataTransfer.files[0].type;

            if (accept_image_type.indexOf(type) > -1){
                return {type: 'image', file: dataTransfer.files[0]};
            }else if (accept_doc_types.indexOf(type) > -1){
                return {type: 'document', file: dataTransfer.files[0]};
            } else {
                return {type: 'other'};
            }
        }

        var ItemClass = function (itemData) {

            var domItem, domTitleWrp, domIdeaWrp, domImgWrp, domRefWrp, domSelectSery, domDeadlineWrp;

            var self = this;
            var isAdded = false;

            var bindEvents = function () {
                domItem.find('.explore-btn').click(function () {
                    loadNewSiblings(itemData.ideabox_ID);
                });
                domItem.find('.view-btn').click(function () {
                    self.viewIdea();
                });
                domItem.find('.todo-btn').click(function () {
                    self.viewGrid();
                });
                domItem.find('.edit-btn').click(function () {
                    openEditPage(itemData.ideabox_ID);
                });
                domRefWrp.find('.reference-link').click(function () {
                    window.open(itemData.strIdeaBox_reference_link);
                });

                domTitleWrp.find('.edit-entry').click(function () {
                    domTitleWrp.addClass('edit-status');
                });

                domTitleWrp.find('.save-entry').click(function () {
                    self.updateTitle().then(function () {
                        domTitleWrp.removeClass('edit-status');
                    });
                });

                domTitleWrp.find('.back-to-origin').click(function () {
                    domTitleWrp.removeClass('edit-status');
                    domTitleWrp.find('textarea').html(itemData.strIdeaBox_title);
                });

                domIdeaWrp.find('.edit-entry').click(function () {
                    domIdeaWrp.addClass('edit-status');
                });

                domIdeaWrp.find('.save-entry').click(function () {
                    self.updateIdea().then(function () {
                        domIdeaWrp.removeClass('edit-status');
                    });
                });

                domIdeaWrp.find('.back-to-origin').click(function () {
                    domIdeaWrp.removeClass('edit-status');
                    domIdeaWrp.find('textarea').html(itemData.strIdeaBox_idea);
                });

                domItem.find('.delete-ideabox').click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "Do you want to delete this ideabox?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete){
                            self.delete();
                        }
                    });
                });
                domImgWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                })
                    .on('dragover dragenter', function(e) {

                        domGridWrp.addClass('is-dragover');
                        domImgWrp.addClass('is-dragover');
                        isDragOnchild = true;
                    })
                    .on('dragleave dragend drop', function() {
                        domImgWrp.removeClass('is-dragover');
                        isDragOnchild = false;
                    })
                    .on('drop', function(e) {
                        domGridWrp.removeClass('is-dragover');
                        var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                        if (dataInfo.type !== 'image'){
                            alert('Sorry, This is not Image file');
                            return false;
                        }
                        var droppedFile = dataInfo.file;
                        self.updateImage(droppedFile);
                    });
                domImgWrp.find('img').on('load', function () {
                    domImgWrp.removeClass('is-uploading');
                });

                domRefWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                })
                    .on('dragover dragenter', function(e) {

                        domGridWrp.addClass('is-dragover');
                        domRefWrp.addClass('is-dragover');
                        isDragOnchild = true;
                    })
                    .on('dragleave dragend drop', function() {
                        domRefWrp.removeClass('is-dragover');
                        isDragOnchild = false;
                    })
                    .on('drop', function(e) {
                        domGridWrp.removeClass('is-dragover');
                        var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                        switch (dataInfo.type){
                            case 'image':
                            case 'document':
                                self.updateRefFile(dataInfo.file);
                                break;
                            case 'url':
                                self.updateRefUrl(dataInfo.url);
                                break;
                            default:
                                alert('Sorry, This is wrong type!');
                                return false;
                                break;
                        }
                    });
                domItem.find('.add-to-series-wrapper .add-post').click(function () {
                    var sery = series[domSelectSery.val()];
                    if (sery.intSeries_client_ID == CLIENT_ID){
                        self.addToPost(sery.series_ID).then(function (res) {});
                    }
                    else {
                        self.addToSuscription(sery.purchased_ID, false);
                    }
                });
                domDeadlineWrp.find('input.deadline').on('changeDate', function () {
                    updateDeadline($(this).val());
                });
            }
            var updateDeadline = function (newDeadline) {
                var ajaxData = new FormData();
                ajaxData.append('action', 'update');
                ajaxData.append('sets', JSON.stringify({dtIdeaBox_deadline: newDeadline}));
                ajaxData.append('where', JSON.stringify({ideabox_ID: itemData.ideabox_ID}));
                return $.ajax({
                    url: API_ROOT_URL + '/Ideabox.php',
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res){
                        itemData.dtIdeaBox_deadline = newDeadline;
                        domDeadlineWrp.find('.deadline-show').html(itemData.dtIdeaBox_deadline);
                        domDeadlineWrp.addClass('setted');
                    }
                });
            }
            this.addToPost = function (seryId) {
                var ajaxData = {
                    action: 'add',
                    sets: self.postFormat()
                };
                ajaxData.sets.intPost_series_ID = seryId;
                return $.ajax({
                    url: API_ROOT_URL + '/Posts.php',
                    data: ajaxData,
                    success: function (res) {
                        if (res.status){
                            isAdded = true;
                            domItem.addClass('added-post');
                        }
                    },
                    type: 'post',
                    dataType: 'json',
                });
            }
            this.addToSuscription = function (purchasedId, postId) {
                var ajaxData = {
                    action: 'add',
                    sets: self.subscriptionFormat()
                };
                ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                if (postId){
                    ajaxData.sets.intClientSubscription_post_ID = postId;
                }

                return $.ajax({
                    url: API_ROOT_URL + '/Subscriptions.php',
                    data: ajaxData,
                    success: function (res) {
                        if (res.status){
                            isAdded = true;
                            domItem.addClass('added-post');
                        }
                    },
                    type: 'post',
                    dataType: 'json',
                });
            }
            this.getFormatData = function () {
                return {
                    title: itemData.strIdeaBox_title,
                    body: itemData.strIdeaBox_idea,
                    summary: '',
                    type: 7,
                    image: itemData.strIdeaBox_image
                }
            }
            this.subscriptionFormat = function () {
                var data = self.getFormatData();

                return {
                    strClientSubscription_title: data.title,
                    strClientSubscription_body: data.body,
                    strClientSubscription_image: data.image,
                    intClientSubscriptions_type: data.type
                }
            }
            this.postFormat = function () {
                var data = self.getFormatData();
                return {
                    strPost_title: data.title,
                    strPost_body: data.body,
                    strPost_summary: data.summary,
                    intPost_type: data.type,
                    strPost_featuredimage: data.image
                }
            }
            this.delete = function () {
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: {action: 'delete_ideabox', id: itemData.ideabox_ID},
                    dataType: 'json',
                    success: function(res) {
                        if (res.status){
                            siblingHandles.forEach(function (handle, i) {
                                if (handle.getId() == self.getId()){
                                    siblingHandles.splice(i, 1);
                                }
                            });
                            loadNewSiblings(itemData.intIdeaBox_subcategory);
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            this.viewIdea = function(){
                var form = $('<form action="idea/index.php" method="post" hidden><input name="id" value="'+ itemData.ideabox_ID +'"></form>');
                form.append('<input name="visual" value="Edit"/><input type="submit" value="send" />');
                form.appendTo('body').submit();
                form.remove();
            }
            this.viewGrid = function () {
                var form = $('<form action="idea/grid.php" method="post" hidden><input name="id" value="'+ itemData.ideabox_ID +'"></form>');
                form.append('<input name="visual" value="Edit"/><input type="submit" value="send" />');
                form.appendTo('body').submit();
                form.remove();
            }
            this.updateTitle = function () {
                var newTitle = domTitleWrp.find('textarea').val();
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: {action: 'update_ideaTitle', id: itemData.ideabox_ID, title: newTitle},
                    dataType: 'json',
                    success: function(res) {
                        if (res.status){
                            itemData.strIdeaBox_title = newTitle;
                            domTitleWrp.find('.item-title').html(newTitle);
                            domTitleWrp.find('.item-title').readmore({
                                collapsedHeight: 47,
                                moreLink: '<span>Read More</span>',
                                lessLink: '<span>View Less</span>',
                                embedCSS: false,
                                beforeToggle: function () {
                                },
                                afterToggle: function () {
                                    domTitleWrp.toggleClass('collapsed');
                                }
                            });
                            domTitleWrp.addClass('collapsed');
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            this.updateIdea = function () {
                var newIdea = domIdeaWrp.find('textarea').val();
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: {action: 'update_ideaDescription', id: itemData.ideabox_ID, idea: newIdea},
                    dataType: 'json',
                    success: function(res) {
                        if (res.status){
                            itemData.strIdeaBox_idea = newIdea;
                            domIdeaWrp.find('.item-idea').html(newIdea);
                            domIdeaWrp.find('.item-idea').readmore({
                                collapsedHeight: 47,
                                moreLink: '<span>Read More</span>',
                                lessLink: '<span>View Less</span>',
                                embedCSS: false,
                                afterToggle: function () {
                                    domIdeaWrp.toggleClass('collapsed');
                                }
                            });
                            domIdeaWrp.addClass('collapsed');
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            this.updateImage = function (imgFile) {

                if (domImgWrp.hasClass('is-uploading')) return new Promise(function (resolve, reject) { resolve(); });

                domImgWrp.addClass('is-uploading').removeClass('is-error');

                var ajaxData = new FormData();
                ajaxData.append('ideaImage', imgFile);
                ajaxData.append('id', itemData.ideabox_ID);
                ajaxData.append('action', 'update_ideaboxImage');
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    complete: function() {
                    },
                    success: function(res) {
                        if (res.status){
                            itemData.strIdeaBox_image = res.data;
                            domImgWrp.find('img').attr('src', itemData.strIdeaBox_image);
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            this.updateRefFile = function (file) {
                var ajaxData = new FormData();
                ajaxData.append('id', itemData.ideabox_ID);
                ajaxData.append('linkText', itemData.strIdeaBox_reference_text);
                ajaxData.append('refFile', file);
                ajaxData.append('action', 'update_ideaboxRefFile');
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res) {
                        if (res.status){
                            itemData.strIdeaBox_reference_link = res.data.link;
                            itemData.strIdeaBox_reference_text = res.data.text;
                            domRefWrp.find('.reference-link').html(itemData.strIdeaBox_reference_text);
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            this.updateRefUrl = function (url) {
                var ajaxData = {
                    action: 'update_ideaboxRefUrl',
                    id: itemData.ideabox_ID,
                    refUrl: url,
                    linkText: itemData.strIdeaBox_reference_text
                }
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    success: function(res) {
                        if (res.status){
                            itemData.strIdeaBox_reference_link = res.data.link;
                            itemData.strIdeaBox_reference_text = res.data.text;
                            domRefWrp.find('.reference-link').html(itemData.strIdeaBox_reference_text);
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            this.updateOrderInFront = function (newOrder) {
                itemData.intIdeaBox_order = newOrder;
            }
            this.bindDom = function (dom) {
                domItem = dom;
                domTitleWrp = domItem.find('.title-wrapper');
                domIdeaWrp = domItem.find('.idea-wrapper');
                domImgWrp = domItem.find('.img-wrapper');
                domRefWrp = domItem.find('.reference-wrapper');
                domSelectSery = domItem.find('.add-to-series-wrapper select');
                domDeadlineWrp = domItem.find('.add-deadline-wrapper');
            }
            this.getHtml = function () {
                var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                cloneDom.find('.item-img').attr('src', itemData.strIdeaBox_image);
                cloneDom.find('.item-title').html(itemData.strIdeaBox_title);
                cloneDom.find('.title-wrapper textarea').html(itemData.strIdeaBox_title);
                cloneDom.find('.item-idea').html(itemData.strIdeaBox_idea);
                cloneDom.find('.idea-wrapper textarea').html(itemData.strIdeaBox_idea);
                cloneDom.find('.reference-link').html(itemData.strIdeaBox_reference_text);
                if (!itemData.next_deadline){
                    cloneDom.find('.next-deadline-wrapper').empty();
                }
                else {
                    cloneDom.find('.next-deadline').html(itemData.next_deadline);
                }
                var tpHtml = cloneDom.html();
                cloneDom.remove();
                return tpHtml;
            }
            this.getId = function () {
                return itemData.ideabox_ID;
            }
            this.getOrder = function () {
                return parseInt(itemData.intIdeaBox_order);
            }
            this.readMore = function () {
                domTitleWrp.find('.item-title').readmore({
                    collapsedHeight: 26,
                    moreLink: '<span>Read More</span>',
                    lessLink: '<span>View Less</span>',
                    embedCSS: false,
                    beforeToggle: function () {
                    },
                    afterToggle: function () {
                        domTitleWrp.toggleClass('collapsed');
                    }
                });
                domTitleWrp.addClass('collapsed');
                domIdeaWrp.find('.item-idea').readmore({
                    collapsedHeight: 47,
                    moreLink: '<span>Read More</span>',
                    lessLink: '<span>View Less</span>',
                    embedCSS: false,
                    afterToggle: function () {
                        domIdeaWrp.toggleClass('collapsed');
                    }
                });
                domIdeaWrp.addClass('collapsed');
            }
            this.init = function () {
                domSelectSery.select2();
                domItem.data('controlHandle', self);
                self.readMore();
                if (itemData.dtIdeaBox_deadline){
                    domDeadlineWrp.addClass('setted');
                    domDeadlineWrp.find('.deadline-show').html(itemData.dtIdeaBox_deadline);
                }
                domDeadlineWrp.find('input.deadline').val(itemData.dtIdeaBox_deadline ? itemData.dtIdeaBox_deadline : '');
                domDeadlineWrp.find('input.deadline').datepicker();
                bindEvents();
            }
        }

        var grid = domRoot.find('.tt-grid').get(0),
            items = [].slice.call( $(grid).find( '>li' ).get() ),
            isAnimating = false;
        var loadNewSet = function(set) {
            return new Promise(function (resolve, reject) {
                if (isAnimating === true){
                    resolve();
                    return false;
                }
                isAnimating = true;

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = $(el).find( '>*' ).get(0);
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );

                // apply effect
                setTimeout( function() {
                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.getHtml();
                            el.bindDom($(items[ i ]).find( '>*:last-child' ));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                        resolve();
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            });
        };

        var loadNewSiblings = function (parentId) {
            currentParentId = parentId;
            siblingHandles = totalSiblingHandles[parentId];
            siblingHandles.sort(function (a, b) {
                return a.getOrder() - b.getOrder();
            });

            items = [].slice.call( $(grid).find( '>li' ).get() );

            for (var i = 0; i < siblingHandles.length - items.length; i ++){
                grid.innerHTML += '<li class="tt-empty"></li>';
            }
            items = [].slice.call( $(grid).find( '>li' ).get() );
            loadNewSet(siblingHandles);
        }

        var openCreatePage = function () {
            $('<form action="editideabox" method="post" hidden><input name="parentId" value="'+ currentParentId +'"></form>').appendTo('body').submit().remove();
        }
        var openEditPage = function (id) {
            $('<form action="editideabox" method="post" hidden><input name="parentId" value="'+ currentParentId +'"><input name="id" value="'+ id +'"></form>').appendTo('body').submit().remove();
        }
        this.makeSortable = function () {
            domGridWrp.find('.tt-grid').sortable({
                cancel: '.buttons-wrapper',
                placeholder: 'ui-state-highlight',
                items: 'li:not(.tt-empty)',
                coneHelperSize: true,
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                revert: 300, // animation in milliseconds
                handle: '.img-wrapper',
                opacity: 0.9,
                update: function (a, b) {}
            });
        }
        this.createIdeaboxByImage = function (imgFile) {
            var ajaxData = new FormData();
            ajaxData.append('action', 'create_ideabox_by_image');
            ajaxData.append('ideaImage', imgFile);
            ajaxData.append('parentId', currentParentId);
            return $.ajax({
                url: ACTION_URL,
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res) {
                    console.log(res);
                    if (res.status){
                        var item = res.data;
                        item.next_deadline = item.dtIdeaBox_deadline;
                        var handle = new ItemClass(item);
                        siblingHandles.push(handle);
                        totalSiblingHandles[handle.getId()] = [];
                        parentIds[handle.getId()] = currentParentId;
                        loadNewSiblings(currentParentId);
                    }
                    else {
                        alert('Sorry. Something wrong!');
                    }
                },
                error: function() {}
            });
        }
        this.createIdeaboxByDoc = function (docFile) {
            var ajaxData = new FormData();
            ajaxData.append('action', 'create_ideabox_by_doc');
            ajaxData.append('docFile', docFile);
            ajaxData.append('parentId', currentParentId);
            return $.ajax({
                url: ACTION_URL,
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res) {
                    console.log(res);
                    if (res.status){
                        var item = res.data;
                        item.next_deadline = item.dtIdeaBox_deadline;
                        var handle = new ItemClass(item);
                        siblingHandles.push(handle);
                        totalSiblingHandles[handle.getId()] = [];
                        parentIds[handle.getId()] = currentParentId;
                        loadNewSiblings(currentParentId);
                    }
                    else {
                        alert('Sorry. Something wrong!');
                    }
                },
                error: function() {}
            });
        }
        this.createIdeaboxByUrl = function (url) {
            var ajaxData = {
                action: 'create_ideabox_by_url',
                url: url,
                parentId: currentParentId
            };
            return $.ajax({
                url: ACTION_URL,
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                success: function(res) {
                    if (res.status){
                        var item = res.data;
                        item.next_deadline = item.dtIdeaBox_deadline;
                        var handle = new ItemClass(item);
                        siblingHandles.push(handle);
                        totalSiblingHandles[handle.getId()] = [];
                        parentIds[handle.getId()] = currentParentId;
                        loadNewSiblings(currentParentId);
                    }
                    else {
                        alert('Sorry. Something wrong!');
                    }
                },
                error: function() {}
            });
        }

        this.updateSiblingsOrder = function () {
            var updateSets = [], updateHandleOrders = [];
            domGridWrp.find('.tt-grid').find('> li').each(function (i, domItemWrp) {
                var handle = $(domItemWrp).find('>*').data('controlHandle');
                var newOrder = i + 1;
                if (newOrder !== handle.getOrder()){
                    updateSets.push({id: handle.getId(), order: newOrder});
                    updateHandleOrders.push({handle: handle, order: newOrder});
                }
            });
            return $.ajax({
                url: ACTION_URL,
                type: 'post',
                data: {action: 'update_orders', updateSets: updateSets},
                dataType: 'json',
                success: function(res) {
                    if (res.status){
                        updateHandleOrders.forEach(function (updateHandleOrders) {
                            var handle = updateHandleOrders.handle;
                            var order = updateHandleOrders.order;
                            handle.updateOrderInFront(order);
                        });
                    }
                    else {
                        alert('Sorry. Something wrong!');
                    }
                },
                error: function() {}
            });
        }

        var bindEvents = function () {
            domRoot.find('.create-ideabox').click(function () {
               openCreatePage(currentParentId);
            });
            domRoot.find('.back-to-top-level img').click(function () {
                if (currentParentId == 0){
                    return false;
                }
                loadNewSiblings(parentIds[currentParentId]);
            });
            domGridWrp.dndhover().on('dndHoverStart', function (event) {
                domGridWrp.addClass('is-dragover');
                domGridWrp.addClass('is-dragover-on-parent');
                event.stopPropagation();
                event.preventDefault();
                return true;
            }).on('dndHoverEnd', function (event) {
                if (isDragOnchild === false){
                    domGridWrp.removeClass('is-dragover');
                }
                domGridWrp.removeClass('is-dragover-on-parent');
                event.stopPropagation();
                event.preventDefault();
                return true;
            });
            domGridWrp.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                e.stopPropagation();
                e.preventDefault();
            }).on('drop', function (e) {
                domGridWrp.removeClass('is-dragover');
                domGridWrp.removeClass('is-dragover-on-parent');

                var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                switch (dataInfo.type){
                    case 'image':
                        self.createIdeaboxByImage(dataInfo.file);
                        break;
                    case 'document':
                        self.createIdeaboxByDoc(dataInfo.file);
                        break;
                    case 'url':
                        self.createIdeaboxByUrl(dataInfo.url);
                        break;
                    default:
                        alert('Sorry, This is not allowed DataType');
                        break;
                }
            });
            domGridWrp.find('.tt-grid').on('sortupdate', function () {
                self.updateSiblingsOrder();
            });
        }

        var setupDragFn = function () {
            $.fn.dndhover = function(options) {
                return this.each(function() {

                    var self = $(this);
                    var collection = $();

                    self.on('dragenter', function(event) {
                        if (collection.length === 0) {

                            self.trigger('dndHoverStart');
                        }
                        collection = collection.add(event.target);
                    });

                    self.on('dragleave', function(event) {
                        /*
                         * Firefox 3.6 fires the dragleave event on the previous element
                         * before firing dragenter on the next one so we introduce a delay
                         */
                        setTimeout(function() {
                            collection = collection.not(event.target);
                            if (collection.length === 0) {
                                self.trigger('dndHoverEnd');
                            }
                        }, 100);
                    });
                });
            };

            isAdvancedUpload = function() {
                var div = document.createElement('div');
                return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
            }();
        }

        this.init = function () {
            initialSeries.forEach(function (sery) {
                series[sery.series_ID] = sery;
                var optHtml = '<option value="'+ sery.series_ID +'">'+ sery.strSeries_title +'</option>';
                domRoot.find('.item-sample-wrapper .add-to-series-wrapper select').append(optHtml);
            });
            siblingIdeabox();
            getEarliestDeadline({ideabox_ID: 0, dtIdeaBox_deadline: '12/31/2200', next_deadline: -1, boolIdeaBox_done: false});

            totalSiblingIdeaboxes.forEach(function (siblingIdeaboxes, parentId) {
                totalSiblingHandles[parentId] = [];
                siblingIdeaboxes.forEach(function (ideabox) {
                    var itemHandle = new ItemClass(ideabox);
                    totalSiblingHandles[parentId].push(itemHandle);
                });
            });

            loadNewSiblings(currentParentId);
            setupDragFn();
            self.makeSortable();
            bindEvents();
        }
    }

    mainHandle = new MainClass();
    mainHandle.init();

    $(document).ajaxStart(function () {
        if(!is_loading_controlled_in_local) {
            $('.loading').css('display', 'block');
        }
    });
    $(document).ajaxComplete(function () {
        if(!is_loading_controlled_in_local){
            $('.loading').css('display', 'none');
        }
    });
})();