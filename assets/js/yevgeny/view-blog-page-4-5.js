(function () {
    'use strict';

    var PageClass = function () {
        var domRoot, domHeader;
        var domUsedBody, domBody, domVideo;
        var postData;
        var audioCnt = 0, metaData = {};
        var availableTypes = [0, 2, 8, 10];
        var self = this, speakerHandle, notesHandle = null, headerHandle, listingPostsHandle;

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            paths: 'paths',
            free: 'boolPost_free',
            duration: 'strPost_duration',
            viewDay: 'viewDay'
        };

        var subscriptionFiels = {
            id: 'clientsubscription_ID',
            title: 'strClientSubscription_title',
            body: 'strClientSubscription_body',
            image: 'strClientSubscription_image',
            type: 'intClientSubscriptions_type',
            order: 'intClientSubscriptions_order',
            nodeType: 'strClientSubscription_nodeType',
            parent: 'intClientSubscriptions_parent',
            paths: 'paths',
            completed: 'boolCompleted',
            duration: 'strSubscription_duration',
            favorite: 'favorite',
            viewDay: 'viewDay',
        };

        var videoHandle = null;
        var videoCnt = 1;
        var payBlogHandle = null;

        var HeaderClass = function () {
            var self = this, copyEmbedHandle;
            var domCircleNavWrap;
            var setComplete = function(v){
                var ajaxData = {
                    action: 'set_complete',
                    sets: {
                        intSubscription_ID: postData.id,
                        boolCompleted: v,
                    },
                };
                ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function () {
                    postData.completed = v;
                    var postId = from === 'subscription' ? postData.origin.intClientSubscription_post_ID : postData.id;
                    listingPostsHandle.update(postId, {boolCompleted: v});
                    if (v) {
                        domHeader.find('.complete-action').addClass('completed');
                    }
                    else {
                        domHeader.find('.complete-action').removeClass('completed');
                    }
                });
            }
            var setFavorite = function (v) {
                var ajaxData = {
                    action: 'set_favorite',
                    subscription_id: postData.id,
                    value: v
                };
                ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                    if (v) {
                        postData.favorite = res.data;
                        domHeader.find('.favorite-action').addClass('favorite');
                    }
                    else {
                        postData.favorite = false;
                        domHeader.find('.favorite-action').removeClass('favorite');
                    }
                })
            }
            var trash = function () {
                var ajaxData = {
                    action: 'trash_post',
                    id: postData.id
                };
                ajaxAPiHandle.pagePost('my_solutions', ajaxData).then(function (res) {
                    if (res.status == true){
                        if (res.data){
                            pageHandle.bindData(viewFormat(res.data));
                            self.bindData(res.data);
                        }
                    }
                    else {
                        alert('something went wrong');
                    }
                })
            }

            var getSocialLinks = function () {
                var viewUri = encodeURIComponent(BASE_URL + '/' + CURRENT_PAGE + '?id=' + (from === 'post' ? postData.id : postData.origin.intClientSubscription_post_ID));
                var title = encodeURIComponent(postData.title);
                var subject = encodeURIComponent('Shared from Walden.ly - ' + postData.title);
                var shareLinks = {
                    facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + viewUri + '&title=' + title,
                    twitter: 'https://twitter.com/intent/tweet?url=' + viewUri,
                    google: 'https://plus.google.com/share?url=' + viewUri,
                    email: 'mailto:?subject=' + subject + '&body=' + encodeURIComponent('I thought you might like this: ') + viewUri,
                }
                return shareLinks;
            }

            this.bindData = function () {
                domHeader.find('.edit-action').attr('href', 'editseries?id=' + series.series_ID);
                if (parseInt(postData.completed)) {
                    domHeader.find('.complete-action').addClass('completed');
                }
                else {
                    domHeader.find('.complete-action').removeClass('completed');
                }
                if (postData.favorite) {
                    domHeader.find('.favorite-action').addClass('favorite');
                }
                else {
                    domHeader.find('.favorite-action').removeClass('favorite');
                }
                domHeader.find('.day-value').html(postData.viewDay);

                var socialLinks = getSocialLinks();

                domCircleNavWrap.find('.facebook-link').attr('href', socialLinks.facebook);
                domCircleNavWrap.find('.twitter-link').attr('href', socialLinks.twitter);
                domCircleNavWrap.find('.google-link').attr('href', socialLinks.google);
                domCircleNavWrap.find('.linkedin-link').attr('href', socialLinks.linkedIn);
                domCircleNavWrap.find('.mail-link').attr('href', socialLinks.email);

                var eData = Object.assign({}, postData);
                if (from === 'subscription') {
                    eData.id = postData.origin.intClientSubscription_post_ID;
                }
                copyEmbedHandle.bindData(eData);
                if (parseInt(series.unreadPostsCount)) {
                    domHeader.find('.listing-toggle').addClass('has-new');
                    domHeader.find('.listing-toggle').find('.unread-posts-count').html(series.unreadPostsCount);
                }
            }
            var bindEvents = function () {
                domHeader.find('.complete-action').click(function () {
                    setComplete(postData.completed ? 0 : 1);
                });
                domHeader.find('.favorite-action').click(function () {
                    setFavorite(postData.favorite ? 0 : 1);
                });
                domHeader.find('.trash-action').click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "Do you want to delete this post?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete){
                            trash();
                        }
                    });
                });
                domHeader.find('.dropdown .dropdown-menu').click(function (e) {
                    return false;
                });
                domHeader.find('.dropdown .post-copy-embed [data-toggle="collapse"]').click(function (e) {
                    domHeader.find('.dropdown .post-copy-embed .post-embed-code').collapse('toggle');
                    return true;
                });
                domHeader.find('.listing-toggle').click(function () {
                    listingPostsHandle.open();
                });
            }
            this.init = function () {
                domCircleNavWrap = domRoot.find('.circle-nav-wrapper');
                domHeader.find('.share-action').circleNav(domRoot.find('.circle-nav-wrapper'));
                var eData = Object.assign({}, postData);
                if (from === 'subscription') {
                    eData.id = postData.origin.intClientSubscription_post_ID;
                }
                copyEmbedHandle = new PostCopyEmbedClass(domHeader.find('.dropdown .post-copy-embed'), eData);
                copyEmbedHandle.init();
                self.bindData();
                bindEvents();
            }
        }

        var ListingPostsClass = function () {

            var domListing, domList, domUnreadList;
            var itemHandles = [], seriesOwnersHandle, seriesStructureTreeHandle;

            var allPosts = [], isDataLoaded = false;

            var bindEvents = function () {
                domListing.find('.section-close').click(function () {
                    domRoot.removeClass('listing-opened');
                });
                $('.site-back-drop').click(function () {
                    domRoot.removeClass('listing-opened');
                });
            }

            var ItemClass = function (itemData) {
                var domItem;
                var bindEvents = function () {
                    domItem.find('.item-title').click(function () {
                        if (itemData.strPost_nodeType !== 'path') {
                            ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'get_post', id: itemData.post_ID}).then(function (res) {
                                var postId = from === 'subscription' ? postData.origin.intClientSubscription_post_ID : postData.id;
                                listingPostsHandle.update(postId, {current: false});
                                from = 'post';
                                pageHandle.bindData(viewFormat(res.data));
                                headerHandle.bindData();
                            });
                        }
                        else {
                            from = 'post';
                            selectPath(helperHandle.postToViewFormat(itemData));
                        }
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.post_ID}}, false);
                    });
                }
                var bindData = function () {
                    if (itemData.strPost_nodeType == 'path') {
                        if (itemData.strPost_featuredimage) {
                            domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                        }
                        else {
                            domItem.find('.item-img').attr('src', 'assets/images/global-icons/tree/path/'+ (itemData.intPost_order) +'.svg');
                        }
                    }
                    else {
                        domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                    }
                    domItem.find('.item-title').html(itemData.strPost_title);
                    domItem.find('.media-icon-wrapper').addClass('type-' + itemData.intPost_type);
                    if (itemData.strPost_duration) {
                        domItem.find('.item-duration').html(itemData.strPost_duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                            domItem.find('.item-duration').html(res);
                            itemData.strPost_duration = res;
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                        });
                    }
                    domItem.find('.item-view-states-wrapper').removeClass('state-complete').removeClass('state-current').removeClass('state-normal');
                    if (parseInt(itemData.boolCompleted)) {
                        domItem.find('.item-view-states-wrapper').addClass('state-complete');
                    }
                    else if (itemData.current) {
                        domItem.find('.item-view-states-wrapper').addClass('state-current');
                    }
                    else {
                        domItem.find('.item-view-states-wrapper').addClass('state-normal');
                    }
                }
                this.setData = function (sets) {
                    itemData = Object.assign(itemData, sets);
                    bindData();
                }
                this.data = function () {
                    return itemData;
                }
                this.dom = function () {
                    return domItem;
                }
                this.init = function () {
                    domItem = domListing.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                    bindData();
                    bindEvents();
                }
            }
            this.open = function () {
                if (isDataLoaded) {
                    domRoot.addClass('listing-opened');
                }
                else {
                    var ajaxData = {
                        action: 'all_posts',
                        from: from,
                        id: from === 'post' ? postData.id : postData.origin.intClientSubscription_post_ID,
                    };
                    if (from === 'subscription') {
                        ajaxData.purchased_ID = purchased.purchased_ID;
                    }
                    ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                        if (res.unReadPosts.length) {
                            domListing.addClass('has-new');
                            res.unReadPosts.forEach(function (v) {
                                var hdl = new ItemClass(v);
                                hdl.init();
                                hdl.dom().appendTo(domUnreadList);
                                itemHandles.push(hdl);
                            });
                        }
                        // allPosts = res.data;
                        // allPosts.forEach(function (v) {
                        //     var hdl = new ItemClass(v);
                        //     hdl.init();
                        //     hdl.dom().appendTo(domList);
                        //     itemHandles.push(hdl);
                        // });
                        seriesStructureTreeHandle.init();
                        seriesStructureTreeHandle.expand();
                        seriesStructureTreeHandle.onClick(function (itemData) {
                            if (itemData.nodeType !== 'path') {
                                ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'get_post', id: itemData.id}).then(function (res) {
                                    var postId = itemData.id;
                                    listingPostsHandle.update(postId, {current: false});
                                    from = 'post';
                                    pageHandle.bindData(viewFormat(res.data));
                                    headerHandle.bindData();
                                });
                            }
                            else {
                                from = 'post';
                                selectPath(helperHandle.postToViewFormat(itemData));
                            }
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'delete_unread', where: {user_id: CLIENT_ID, post_id: itemData.post_ID}}, false);
                        });
                        domRoot.addClass('listing-opened');
                        isDataLoaded = true;
                    });
                }
            }
            this.update = function (id, sets) {
                itemHandles.forEach(function (value) {
                    if (value.data().post_ID == id) {
                        value.setData(sets);
                    }
                });
            }
            this.init = function () {
                domListing = $('#listing-posts');
                domList = domListing.find('.listing-posts-content .posts-list');
                domUnreadList = domListing.find('.listing-posts-content .unread-posts-list');
                seriesStructureTreeHandle = new SeriesStructureTreeClass(domListing.find('.series-structure-tree-component'), series.series_ID);
                seriesStructureTreeHandle.setMode('static');
                seriesOwnersHandle = new SeriesOwnersClass(domListing.find('.series-owners.component'), series);
                seriesOwnersHandle.init();
                bindEvents();
            }
        }

        var FormFieldClass = function (domField) {
            var domValue;
            var field, answer = false;

            var bindEvents = function () {
                switch (field.strFormField_type) {
                    case 'text':
                        domValue.blur(function () {
                            updateAnswer();
                        });
                        break;
                    case 'checkbox':
                        domValue.find('input').change(function () {
                            updateAnswer();
                        });
                        break;
                }
            }
            var updateAnswer = function () {
                if (CLIENT_ID == -1) {
                    return false;
                }
                var sets = {};
                domField.addClass('status-loading');
                switch (field.strFormField_type) {
                    case 'text':
                        sets.strFormFieldAnswer_answer = domValue.html();
                        break;
                    case 'checkbox':
                        sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                        break;
                }
                if (answer) {
                    ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                        domField.removeClass('status-loading');
                        domField.addClass('status-loading-finished');
                        domField.one(animEndEventName, function () {
                            console.log('ani end');
                            domField.removeClass('status-loading-finished');
                        });
                        answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                    });
                }
                else {
                    $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                    ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                        domField.removeClass('status-loading');
                        domField.addClass('status-loading-finished');
                        answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                        domField.one(animEndEventName, function () {
                            console.log('ani end');
                            domField.removeClass('status-loading-finished');
                        });
                    });
                }
            }
            var bindData = function () {
                formFields.forEach(function (formField) {
                    if (formField.formField_ID == domField.attr('data-form-field')) {
                        field = formField;
                    }
                });
                formFieldAnswers.forEach(function (formFieldAnswer) {
                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                        answer = formFieldAnswer;
                    }
                });
                if (answer) {
                    switch (field.strFormField_type) {
                        case 'text':
                            domValue.html(answer.strFormFieldAnswer_answer);
                            break;
                        case 'checkbox':
                            domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                            break;
                    }
                }
            }
            this.init = function () {
                domValue = domField.find('.form-field-value');
                domField.find('[data-toggle="popover"]').popover();
                bindData();
                bindEvents();
            }
        }

        var viewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                if (from === 'post') {
                    fk = helperHandle.keyOfVal(postFields, k);
                    if (fk) { fData[fk] = data[k]; }
                }
                else {
                    fk = helperHandle.keyOfVal(subscriptionFiels, k);
                    if (fk) { fData[fk] = data[k]; }
                }
            }
            fData.origin = data;
            return fData;
        }

        var removePluginsEvents = function () {
            $('svg').mousedown(function (e) {
                return false;
            });
        }
        var selectPath = function (path) {
            var ajaxData = {};

            if (from === 'post') {
                ajaxData.action = 'set_path';
                ajaxData.pathId = path.id;
                ajaxData.seriesId = series.series_ID;
            }
            else {
                ajaxData.action = 'set_subPath';
                ajaxData.pathId = path.id;
                ajaxData.purchasedId = purchased.purchased_ID;
            }
            ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                if (res.status === true){
                    self.bindData(viewFormat(res.data));
                }
                else {
                    nextPost();
                }
            });
        }
        var makeBody = function () {

            var type = parseInt(postData.type);
            type = availableTypes.indexOf(type) != -1 ? type : 'other';
            var newDomUsedBody;
            if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                payBlogHandle = new PayBlogClass(domUsedBody, series);
                payBlogHandle.onPaid(function (purchasedId) {
                    purchased = purchasedId;
                    makeBody();
                });
                removePluginsEvents();
                payBlogHandle.init();
            }
            else if (postData.nodeType === 'menu') {
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                postData.paths = postData.paths.map(function (path, i) {
                    return viewFormat(path);
                });
                var menuHandle = new MenuOptionsClass(domUsedBody.find('.menu-options-component'), postData);
                menuHandle.init();
                menuHandle.onSelect(function (option) {
                    selectPath(option);
                });
            }
            else {
                newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                switch (parseInt(type)){
                    case 0:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                        domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                        domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                        var settings_ap = {
                            disable_volume: 'off',
                            disable_scrub: 'default',
                            design_skin: 'skin-wave',
                            skinwave_dynamicwaves: 'off'
                        };
                        dzsag_init('#audio-' + audioCnt, {
                            'transition':'fade',
                            'autoplay' : 'off',
                            'settings_ap': settings_ap
                        });
                        audioCnt++;
                        break;
                    case 2:
                        if (videoHandle){

                        }
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domVideo = domUsedBody.find('video');
                        if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                            var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}] , "customControlsOnMobile": true };
                            domVideo.attr('data-setup', JSON.stringify(setup));
                        }
                        else {
                            domVideo.append('<source src="'+ postData.body +'" />');
                        }
                        videoHandle = videojs(domVideo.get(0), {}, function () {
                            this.one('timeupdate', function() {
                                domBody.find('.blog-duration').hide();
                            });
                        });
                        break;
                    case 8:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.append(postData.body);
                        break;
                    case 10:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.append(postData.body);
                        domUsedBody.find('[data-form-field]').each(function (i, d) {
                            var hdl = new FormFieldClass($(d));
                            hdl.init();
                        });
                        domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                            var field = false;
                            var answer = false;
                            formFields.forEach(function (formField) {
                                if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                    field = formField;
                                }
                            });
                            formFieldAnswers.forEach(function (formFieldAnswer) {
                                if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                    answer = formFieldAnswer;
                                }
                            });
                            if (field) {
                                if (answer) {
                                    field.answer = answer.strFormFieldAnswer_answer;
                                }
                                else{
                                    field.answer = false;
                                }
                            }
                            var hdl = new AnsweredFormFieldClass($(d), field);
                            hdl.init();
                        });
                        domUsedBody.find('.form-loop-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var mode = $(d).hasClass('mode-answer') ? 'answer' : 'question';
                            var hdl = new FormLoopClass($(d), loopId, {mode: mode});
                            hdl.init();
                        });
                        domUsedBody.find('.form-loop-question-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var hdl = new FormLoopQuestionClass($(d), loopId);
                            hdl.init();
                            hdl.afterChange(function () {
                                updateBody();
                            });
                        });
                        domUsedBody.find('.form-loop-answer-parag-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var hdl = new FormLoopAnswerParagClass($(d), loopId);
                            hdl.init();
                        });
                        domUsedBody.find('.form-loop-answer-table-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var hdl = new FormLoopAnswerTableClass($(d), loopId);
                            hdl.init();
                        });
                        break;
                    default:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        // domUsedBody.find('.img-wrapper').append('<img src="'+ subscription.strClientSubscription_image +'">');
                        domUsedBody.append(postData.body);
                        domUsedBody.find('[data-form-field]').each(function (i, d) {
                            var hdl = new FormFieldClass($(d));
                            hdl.init();
                        });
                        domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                            var field = false;
                            var answer = false;
                            formFields.forEach(function (formField) {
                                if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                    field = formField;
                                }
                            });
                            formFieldAnswers.forEach(function (formFieldAnswer) {
                                if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                    answer = formFieldAnswer;
                                }
                            });
                            if (field) {
                                if (answer) {
                                    field.answer = answer.strFormFieldAnswer_answer;
                                }
                                else{
                                    field.answer = false;
                                }
                            }
                            var hdl = new AnsweredFormFieldClass($(d), field);
                            hdl.init();
                        });
                        domUsedBody.find('.form-loop-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var mode = $(d).hasClass('mode-answer') ? 'answer' : 'question';
                            var hdl = new FormLoopClass($(d), loopId, {mode: mode});
                            hdl.init();
                        });
                        domUsedBody.find('.form-loop-question-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var hdl = new FormLoopQuestionClass($(d), loopId);
                            hdl.init();
                            hdl.afterChange(function () {
                                updateBody();
                            });
                        });
                        domUsedBody.find('.form-loop-answer-parag-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var hdl = new FormLoopAnswerParagClass($(d), loopId);
                            hdl.init();
                        });
                        domUsedBody.find('.form-loop-answer-table-component').each(function (i, d) {
                            var loopId = $(d).attr('data-form-loop');
                            var hdl = new FormLoopAnswerTableClass($(d), loopId);
                            hdl.init();
                        });
                        break;
                }
            }
        }
        var nextPost = function () {
            var ajaxData;

            if (from === 'post') {
                ajaxData = {
                    action: 'next_post',
                    id: postData.id
                };

            }
            else {
                ajaxData = {
                    action: 'next_sub',
                    id: postData.id
                };
            }

            return ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                if (res.status){
                    var postId = from === 'subscription' ? postData.origin.intClientSubscription_post_ID : postData.id;
                    listingPostsHandle.update(postId, {current: false});
                    self.bindData(viewFormat(res.data));
                    headerHandle.bindData();
                }
            });
        }
        var prevPost = function () {

            var ajaxData;

            if (from === 'post') {
                ajaxData = {
                    action: 'prev_post',
                    id: postData.id
                };
            }
            else {
                ajaxData = {
                    action: 'prev_sub',
                    id: postData.id
                };
            }

            return ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                if (res.status){
                    var postId = from === 'subscription' ? postData.origin.intClientSubscription_post_ID : postData.id;
                    listingPostsHandle.update(postId, {current: false});
                    self.bindData(viewFormat(res.data));
                    headerHandle.bindData();
                }
            });
        }
        var updateBody = function () {
            if (from === 'post') {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: postData.id, sets: {strPost_body: domUsedBody.html()}})
            } else {
                ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', where: postData.id, sets: {strClientSubscription_body: domUsedBody.html()}})
            }
        }
        var bindEvents = function () {
            domRoot.find('.arrow-left').click(function () {
                prevPost();
            });
            domRoot.find('.arrow-right').click(function () {
                nextPost();
            });
            domRoot.get(0).addEventListener('swl', function () {
                nextPost();
            }, false);
            domRoot.get(0).addEventListener('swr', function () {
                prevPost();
            }, false);
            domRoot.find('.download-as-pdf-btn').click(function () {
                downloadAsPDF();
            });
        }
        var SpeakerClass = function () {

            var domSpeaker = domHeader.find('.choose-lang-wrapper');

            var setupLanguage = function () {

                if ('speechSynthesis' in window) {
                    // window.speechSynthesis.onvoiceschanged = function (ev) {
                    var voiceList = domSpeaker.find('[name="speaker_lang"]');
                    if(voiceList.find('option').length == 0) {
                        speechSynthesis.getVoices().forEach(function(voice, index) {
                            var $option = $('<option>')
                                .val(index)
                                .html(voice.name + (voice.default ? ' (default)' :''));
                            voiceList.append($option);
                        });
                        voiceList.material_select();
                    }
                    // }
                }
            }

            var blockTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'div', 'p'];
            var msg = new SpeechSynthesisUtterance();
            var cleanText = function (dirtyText) {

                blockTags.forEach(function (blockTag) {
                    dirtyText = dirtyText.replace('</' + blockTag + '>', '\n</' + blockTag + '>');
                });
                return $('<div>' + dirtyText + '</div>').text();
            }

            var speakText = function (text) {
                setTimeout(function () {
                    speechSynthesis.cancel();
                    var voices = window.speechSynthesis.getVoices();
                    msg.voice = voices[domSpeaker.find('[name="speaker_lang"]').val()];
                    // msg.rate = domSpeaker.find('[name="speaker_rate"]').val() / 10;
                    // msg.pitch = domSpeaker.find('[name="speaker_pitch"]').val();
                    msg.text = text;
                    msg.volume = 1;
                    window.speechSynthesis.speak(msg);
                }, 10);
            }

            var makePostText = function () {
                var text = series.strSeries_title + '\n\n\n\n';
                text += postData.title + '\n\n';
                text += postData.body;
                return cleanText(text);
            }

            var bindEvents = function () {
                domSpeaker.find('.play-btn').click(function () {
                    if (speechSynthesis.pending){
                        return false;
                    }

                    if (speechSynthesis.paused){
                        window.speechSynthesis.resume();
                    }
                    else {
                        var text = makePostText();
                        speakText(text);
                    }
                });

                var pendingFunc = function () {
                    domSpeaker.find('.pause-speaker').click(function () {
                        if (window.speechSynthesis.pending || window.speechSynthesis.paused){
                            return false;
                        }

                        window.speechSynthesis.pause();
                    });
                    return true;
                }();

                msg.onstart = function(event) {
                    console.log('stat', event);
                    domSpeaker.removeClass('pause');
                    domSpeaker.addClass('speaking');
                };
                msg.onpause = function (ev) {
                    console.log('paused', ev);
                    domSpeaker.removeClass('speaking');
                    domSpeaker.addClass('pause');
                }
                msg.onresume = function (ev) {
                    console.log('resume', ev);
                    domSpeaker.removeClass('pause');
                    domSpeaker.addClass('speaking');

                }
                msg.onend = function(event) {
                    console.log('end', event);
                    domSpeaker.removeClass('pause');
                    domSpeaker.removeClass('speaking');
                };

            };

            this.init = function () {
                setTimeout(function () {
                    setupLanguage();
                }, 100);
                window.speechSynthesis.cancel();
                setTimeout(function () {
                    var voices = window.speechSynthesis.getVoices();
                    msg.voice = voices[0];
                    msg.volume = 0;
                    msg.text = '';
                    window.speechSynthesis.speak(msg);
                }, 100);
                bindEvents();
            }
        }
        var downloadAsPDF = function () {
            var opt = {
                margin:       1,
                filename:     'myfile.pdf',
                image:        { type: 'jpeg', quality: 0.98 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
            };
            var worker = html2pdf().from(domUsedBody.get(0)).set(opt).save(postData.title + '.pdf');
        }
        var downloadPDF = function () {
            var cloneHtml = $('html').clone();
            cloneHtml.find('script').remove();
            cloneHtml.find('header').remove();
            cloneHtml.find('aside').remove();
            cloneHtml.find('footer').remove();
            var html = '<html>' + cloneHtml.html() + '</html>';
            var domForm = $('<form method="post" action="'+ API_ROOT_URL + '/HtmlToPdf.php' +'" hidden target="_blank"></form>');
            domForm.append($('<input name="html"/>').val(html));
            domForm.appendTo('body').submit().remove();
            console.log(html);
        }
        this.bindData = function (newData) {

            postData = $.extend({}, postData, newData);

            $('head title').html(postData.title);
            $('meta[name="Title"]').attr('content', postData.title);
            $('meta[name="Description"]').attr('content', postData.origin.meta.metaDes);
            $('meta[property="og:title"]').attr('content', postData.title);
            $('meta[property="og:image"]').attr('content', postData.image);
            $('meta[property="og:description"]').attr('content', postData.origin.meta.metaDes);

            for( var i = 0; i < 10; i ++ ){
                domRoot.find('.site-content').removeClass('post-type-' + i);
            }
            domRoot.find('.series-title').html(series.strSeries_title);
            if (postData.nodeType === 'menu') {
                $('.site-wrapper .decoration-container .right-portion').addClass('disabled');
                domRoot.find('.blog-title').html('Please choose from the following selections:');
            }
            else {
                $('.site-wrapper .decoration-container .right-portion').removeClass('disabled');
                domRoot.find('.blog-title').html(postData.title);
            }
            domBody.find('.blog-duration').html('waiting...');
            if (postData.duration) {
                domBody.find('.blog-duration').html(postData.duration);
            }
            else {
                helperHandle.getBlogDuration(postData.body, postData.type).then(function (res) {
                    domBody.find('.blog-duration').html(res);
                });
            }
            var postNotesData = {
                postNotes: postData.origin.postNotes,
                postId: from === 'post' ? postData.id : postData.origin.intClientSubscription_post_ID,
                subscriptionId: from === 'subscription' ? postData.id : false,
                options: {
                    collapsedSize: 'large',
                    mobileStatus: 'static',
                }
            };
            if (notesHandle) {
                notesHandle.setData(postNotesData);
            }
            else {
                notesHandle = new PostNotesClass(domRoot.find('.post-notes-component'), postNotesData);
                notesHandle.init();
            }
            var postId = from === 'subscription' ? postData.origin.intClientSubscription_post_ID : postData.id;
            if (listingPostsHandle) {
                listingPostsHandle.update(postId, {current: true});
            }
            makeBody();
        }
        this.init = function () {
            domRoot = $('.site-wrapper.page');
            domHeader = domRoot.find('header.view-blog-header');
            domBody = domRoot.find('.view-blog-body .body-wrapper');
            removePluginsEvents();
            postData = viewFormat(initialPostData);
            self.bindData(postData);
            $(document).ready(function () {
                speakerHandle = new SpeakerClass();
                speakerHandle.init();
            });
            headerHandle = new HeaderClass();
            headerHandle.init();

            listingPostsHandle = new ListingPostsClass();
            listingPostsHandle.init();
            bindEvents();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();