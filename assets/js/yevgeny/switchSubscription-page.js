(function () {
    'use strict';

    var is_loading_controlled_in_local = 0;
    var switchPageHandle;

    var SwitchPageClass = function () {

        var switchOptionsHandle, switchFromExistingHandle, switchFromScratchHandle;

        var openUserpage = function () {
            $('<form action="userpage" method="post" hidden><input name="purchasedId" value="'+ subscription.intClientSubscription_purchased_ID +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
        }
        var openView = function () {
            $('<form action="view" method="get" hidden><input name="id" value="'+ postId +'"><input name="prevpage" value="userpage"></form>').appendTo('body').submit().remove();
        }

        var updatePost = function(sets){
            var where = {post_ID: postId};
            return $.ajax({
                url: API_ROOT_URL + '/Posts.php',
                data: {action: 'update', where: where, sets: sets},
                success: function (res) {
                    if (res.status){
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Your experience has been customized!',
                            buttons: {
                                returnHome: {
                                    text: "Return to your Experiences",
                                    value: 'return_home',
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                customize: {
                                    text: "View this Experience",
                                    value: 'view',
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            },
                            closeOnClickOutside: false,
                        }).then(function (value) {
                            if (value == 'return_home'){
                                openUserpage();
                            }
                            else {
                                openView();
                            }
                        });
                    }
                },
                type: 'post',
                dataType: 'json',
            });
        }
        var updateSubscription = function (sets) {
            var where = {clientsubscription_ID: subscriptionId};
            return $.ajax({
                url: API_ROOT_URL + '/Subscriptions.php',
                data: {action: 'update', where: where, sets: sets},
                success: function (res) {
                    if (res.status){
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Your experience has been customized!',
                            buttons: {
                                returnHome: {
                                    text: "Return to your Experiences",
                                    value: 'return_home',
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                customize: {
                                    text: "View this Experience",
                                    value: 'view',
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            },
                            closeOnClickOutside: false,
                        }).then(function (value) {
                            if (value == 'return_home'){
                                openUserpage();
                            }
                            else {
                                openView();
                            }
                        });
                    }
                },
                type: 'post',
                dataType: 'json',
            });
        }

        var SwitchOptionsClass = function () {
            var domRoot = $('#options-tab');
            var bindEvents = function () {
                domRoot.find('[href="#switch-from-existing"]').on('shown.bs.tab', function (e) {
                    setTimeout(function () {
                        switchFromExistingHandle.setSearchTerm(subscription.strClientSubscription_title);
                    }, 200);
                });
            }
            this.init = function () {
                bindEvents();
            }
        }

        var SwitchFromExistingClass = function () {

            var domRoot = $('#switch-from-existing .search-results-wrapper');
            var domSearchForm = domRoot.find('.search-term-wrapper form');
            var domSearch = domSearchForm.find('input');

            var flgOpend = false;
            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';
            var series = [];

            var youtubeListHandle = null;
            var podcastsListHandle = null;
            var blogsListHandle = null;
            var recipesListHandle = null;
            var ideaboxListHandle = null;
            var postsListHandle = null;
            var rssbPostsListHandle = null;
            var self = this;

            var searchPosts = function (kwd, size, items) {
                return $.ajax({
                    url: API_ROOT_URL + '/SearchPosts.php',
                    data: {q: kwd, size: size, items: items},
                    success: function (res) {
                    },
                    type: 'post',
                    dataType: 'json',
                });
            }

            var bindEvents = function () {
                domSearchForm.submit(function () {

                    var items = [
                        {name: 'youtube'},
                        {name: 'spreaker'},
                        {name: 'recipes'},
                        {name: 'blogs'},
                        {name: 'ideabox'},
                        {name: 'posts'},
                        {name: 'rssbPosts'}
                    ];
                    searchPosts(domSearch.val(), 10, items).then(function (res) {
                        youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                        podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                        blogsListHandle.setItems(res.data.blogs, domSearch.val());
                        recipesListHandle.setItems(res.data.recipes, domSearch.val());
                        ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                        postsListHandle.setItems(res.data.posts, domSearch.val());
                        rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                    });
                    return false;
                });
                domSearchForm.find('.clear-search-term').click(function () {
                    domSearch.val('');
                    domSearchForm.submit();
                });
                domRoot.find('.show-more').click(function () {
                    self.showMore();
                });
                domRoot.find('[name="from"]').change(function () {
                    var val = $(this).val();
                    var checked = $(this).prop('checked');
                    var handle = null;
                    switch (val){
                        case 'youtube':
                            handle = youtubeListHandle;
                            break;
                        case 'spreaker':
                            handle = podcastsListHandle;
                            break;
                        case 'twingly':
                            handle = blogsListHandle;
                            break;
                        case 'recipes':
                            handle = recipesListHandle;
                            break;
                        case 'ideabox':
                            handle = ideaboxListHandle;
                            break;
                        case 'posts':
                            handle = postsListHandle;
                            break;
                        case 'rssbposts':
                            handle = rssbPostsListHandle;
                            break;
                        default:
                            handle = youtubeListHandle;
                            break;
                    }
                    checked ? handle.show() : handle.hide();
                });
                domRoot.find('.results-from').find('.dropdown-menu').find('li').click(function () {
                    var checkbox = $(this).find('input');
                    checkbox.prop('checked', !checkbox.prop('checked')).change();
                    return false;
                });
            }

            var YoutubeListClass = function () {
                var domResultsRoot = domRoot.find('.youtube-grid.tt-grid-wrapper');

                var self = this;
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];

                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intSeries_client_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        var data = self.getFormatData();
                        var sery = series[domSelectSery.val()];
                        var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.append('<input name="action" value="view"/>');
                        $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                        $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                        $form.append('<input name="type" value="'+ data.type +'"/>');
                        $form.append('<input name="title" value="'+ data.title +'"/>');
                        $form.append('<input name="body" value="'+ data.body +'"/>');
                        $form.append('<input name="img" value="'+ data.image +'"/>');
                        $form.submit();
                        $form.remove();
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.title = itemData.snippet.title;
                    this.summary = itemData.snippet.description;
                    this.body = 'https://www.youtube.com/embed/' + itemData.id.videoId;
                    this.featuredImage = itemData.snippet.thumbnails.high.url;
                    this.type = 2;
                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }
                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domItemRoot.find('.remove-wrapper').remove();
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }
                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }

                this.keyword = '';
                this.nextPageToken = false;
                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.init = function () {
                }
            }

            var PodcastsListClass = function () {
                var domResultsRoot = domRoot.find('.podcasts-grid.tt-grid-wrapper');

                var self = this;
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];

                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intSeries_client_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                        domItemRoot.find('.view-post').click(function () {
                            self.viewPost();
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        var data = self.getFormatData();
                        var sery = series[domSelectSery.val()];
                        var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.append('<input name="action" value="view"/>');
                        $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                        $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                        $form.append('<input name="type" value="'+ data.type +'"/>');
                        $form.append('<input name="title" value="'+ data.title +'"/>');
                        $form.append('<input name="body" value="'+ data.body +'"/>');
                        $form.append('<input name="img" value="'+ data.image +'"/>');
                        $form.submit();
                        $form.remove();
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.title = itemData.title;
                    this.summary = '';
                    this.body = itemData.episode_id;
                    this.body = 'https://api.spreaker.com/v2/episodes/' + itemData.episode_id + '/play';
                    this.featuredImage = itemData.image_url;
                    this.type = 0;
                    this.selected = false;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }

                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domItemRoot.find('.remove-wrapper').remove();
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }

                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.keyword = '';
                this.nextPageToken = false;

                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.init = function () {
                }
            }

            var BlogsListClass = function () {
                var domResultsRoot = domRoot.find('.blogs-grid.tt-grid-wrapper');

                var self = this;
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];

                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intSeries_client_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });

                        domItemRoot.find('.view-post').click(function () {
                            self.viewPost();
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        var data = self.getFormatData();
                        var sery = series[domSelectSery.val()];
                        var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.append('<input name="action" value="view"/>');
                        $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                        $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                        $form.append('<input name="type" value="'+ data.type +'"/>');
                        $form.append('<input name="title" value="'+ data.title +'"/>');
                        $form.append('<input name="body" value="'+ data.body +'"/>');
                        $form.append('<input name="img" value="'+ data.image +'"/>');
                        $form.submit();
                        $form.remove();
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.title = itemData.title;
                    this.summary = '';
                    this.body = itemData.text;
                    this.featuredImage = itemData.imgOrg;
                    this.type = 7;
                    this.selected = false;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }

                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domItemRoot.find('.remove-wrapper').remove();
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }

                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.keyword = '';
                this.nextPageToken = false;

                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.init = function () {
                }
            }

            var RecipesListClass = function () {
                var domResultsRoot = domRoot.find('.recipes-grid.tt-grid-wrapper');

                var self = this;
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];

                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intSeries_client_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                        domItemRoot.find('.view-post').click(function () {
                            self.viewPost();
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        var data = self.getFormatData();
                        var sery = series[domSelectSery.val()];
                        var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.append('<input name="action" value="view"/>');
                        $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                        $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                        $form.append('<input name="type" value="'+ data.type +'"/>');
                        $form.append('<input name="title" value="'+ data.title +'"/>');
                        $form.append('<input name="body" value="'+ data.body +'"/>');
                        $form.append('<input name="img" value="'+ data.image +'"/>');
                        $form.submit();
                        $form.remove();
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.title = itemData.title;
                    this.summary = '';
                    this.body = itemData.source_url;
                    this.featuredImage = itemData.image_url;
                    this.type = 7;
                    this.selected = false;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }

                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domItemRoot.find('.remove-wrapper').remove();
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }

                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.keyword = '';
                this.nextPageToken = false;

                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.init = function () {
                }
            }

            var IdeaboxListClass = function () {
                var domResultsRoot = domRoot.find('.ideabox-grid.tt-grid-wrapper');
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];
                var self = this;

                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            if (itemData.intIdeaBox_contractor_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                        domItemRoot.find('.view-post').click(function () {
                            self.viewPost();
                        });
                        domItemRoot.find('.remove-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intIdeaBox_contractor_ID == CLIENT_ID){
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this ideabox?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.deletePost();
                                    }
                                });
                            }
                            else {
                                alert('Sorry. This is not created by you.')
                            }
                        });
                    }

                    this.deletePost = function () {
                        var ajaxData = {
                            action: 'delete',
                            id: self.id
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Ideabox.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    itemHandles.forEach(function (itemHandle, i) {
                                        if (itemHandle.id == self.id){
                                            itemHandles.splice(i, 1);
                                            loadNewSet(itemHandles);
                                        }
                                    })
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        var $form = $('<form action="ideaboxcards" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.submit();
                        $form.remove();
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.id = itemData.ideabox_ID;
                    this.title = itemData.strIdeaBox_title;
                    this.summary = '';
                    this.body = itemData.strIdeaBox_idea;
                    this.featuredImage = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                    this.type = 7;
                    this.selected = false;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }

                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }

                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.keyword = '';
                this.nextPageToken = false;
                this.init = function () {
                }
            }

            var postsListClass = function () {
                var domResultsRoot = domRoot.find('.posts-grid.tt-grid-wrapper');
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];
                var self = this;

                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intSeries_client_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                        domItemRoot.find('.view-post').click(function () {
                            self.viewPost();
                        });
                        domItemRoot.find('.remove-post').click(function () {
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this post?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    self.deletePost();
                                    self.deleteSubscription();
                                }
                            });
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        window.open(BASE_URL + '/view.php?id=' + itemData.post_ID + '&prevpage=' + CURRENT_PAGE);
                    }

                    this.deletePost = function () {
                        var ajaxData = {
                            action: 'delete',
                            id: self.id
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    itemHandles.forEach(function (itemHandle, i) {
                                        if (itemHandle.id == self.id){
                                        }
                                    })
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }
                    this.deleteSubscription = function () {
                        var ajaxData = {
                            action: 'delete_by_postId',
                            postId: self.id
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    itemHandles.forEach(function (itemHandle, i) {
                                        if (itemHandle.id == self.id){
                                            itemHandles.splice(i, 1);
                                            loadNewSet(itemHandles);
                                        }
                                    })
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }
                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.id = itemData.post_ID;
                    this.title = itemData.strPost_title;
                    this.summary = itemData.strPost_summary;
                    this.body = itemData.strPost_body;
                    this.featuredImage = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                    this.type = itemData.intPost_type;
                    this.selected = false;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }

                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }
                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.keyword = '';
                this.nextPageToken = false;
                this.init = function () {
                }
            }

            var rssbPostsListClass = function () {

                var domResultsRoot = domRoot.find('.rssb-posts-grid.tt-grid-wrapper');
                var pageHistory = [];
                var itemHandles = [];
                var allItemHandles = [];
                var selectedItems = [];
                var self = this;
                var ItemClass = function (itemData) {

                    var domItemRoot, domTitleWrp, domSelectSery;

                    var isAdded = false;

                    var self = this;

                    var bindEvents = function () {
                        domItemRoot.find('.select-post').click(function () {
                            self.switchWith();
                        });
                        domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                            var sery = series[domSelectSery.val()];
                            if (sery.intSeries_client_ID == CLIENT_ID){
                                self.addToPost(sery.series_ID).then(function (res) {
                                    self.addToSuscription(sery.purchased_ID, res.data);
                                });
                            }
                            else {
                                self.addToSuscription(sery.purchased_ID, false);
                            }
                        });
                        domItemRoot.find('.view-post').click(function () {
                            self.viewPost();
                        });
                    }
                    this.switchWith = function () {
                        if (owner == 'me'){
                            var sets = self.postFormat();
                            updatePost(sets);
                        }
                        else{
                            var sets = self.subscriptionFormat();
                            updateSubscription(sets);
                        }
                    }
                    this.viewPost = function () {
                        var data = self.getFormatData();
                        var sery = series[domSelectSery.val()];
                        var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.append('<input name="action" value="view"/>');
                        $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                        $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                        $form.append('<input name="type" value="'+ data.type +'"/>');
                        $form.append('<input name="title" value="'+ data.title +'"/>');
                        $form.append('<input name="body" value="'+ data.body +'"/>');
                        $form.append('<input name="img" value="'+ data.image +'"/>');
                        $form.submit();
                        $form.remove();
                    }

                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return $.ajax({
                            url: API_ROOT_URL + '/Posts.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }

                        return $.ajax({
                            url: API_ROOT_URL + '/Subscriptions.php',
                            data: ajaxData,
                            success: function (res) {
                                if (res.status){
                                    isAdded = true;
                                    domItemRoot.addClass('added-post');
                                }
                            },
                            type: 'post',
                            dataType: 'json',
                        });
                    }

                    this.title = itemData.strRSSBlogPosts_title;
                    this.summary = itemData.strRSSBlogPosts_description;
                    this.body = itemData.strRSSBlogPosts_content;
                    this.featuredImage = DEFAULT_IMAGE;
                    this.type = 7;
                    this.selected = false;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                        cloneDom.find('.title-wrapper .item-title').html(self.title);
                        var tpHtml = cloneDom.html();
                        cloneDom.remove();
                        return tpHtml;
                    }

                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;
                        domTitleWrp = domItemRoot.find('.title-wrapper');
                        domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                    }
                    this.selected = false;
                    this.getFormatData = function () {
                        return {
                            title: self.title,
                            body: self.body,
                            summary: self.summary,
                            type: self.type,
                            image: self.featuredImage
                        }
                    }
                    this.subscriptionFormat = function () {
                        var data = self.getFormatData();

                        return {
                            strClientSubscription_title: data.title,
                            strClientSubscription_body: data.body,
                            strClientSubscription_image: data.image,
                            intClientSubscriptions_type: data.type
                        }
                    }
                    this.postFormat = function () {
                        var data = self.getFormatData();
                        return {
                            strPost_title: data.title,
                            strPost_body: data.body,
                            strPost_summary: data.summary,
                            intPost_type: data.type,
                            strPost_featuredimage: data.image
                        }
                    }
                    this.init = function () {
                        domItemRoot.find('.remove-wrapper').remove();
                        domTitleWrp.find('.item-title').readmore({
                            collapsedHeight: 47,
                            moreLink: '<span>Read More</span>',
                            lessLink: '<span>View Less</span>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTitleWrp.toggleClass('collapsed');
                            }
                        });
                        domTitleWrp.addClass('collapsed');
                        domSelectSery.select2();
                        bindEvents();
                    }
                }
                var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );
                    for (var i = 0; i < set.length - items.length; i++){
                        grid.innerHTML += '<li class="tt-empty"></li>';
                    }
                    grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                                el.init();
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );

                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.setItems = function (itemsData, newKeyword) {
                    self.keyword = newKeyword;
                    itemHandles = [];
                    self.nextPageToken = itemsData.nextPageToken;
                    itemsData.items.forEach(function (item) {
                        var itemHandle = new ItemClass(item);
                        itemHandles.push(itemHandle);
                        allItemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                    pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
                }
                this.show = function () {
                    domResultsRoot.show();
                }
                this.hide = function () {
                    domResultsRoot.hide();
                }
                this.getSelectedItems = function () {
                    selectedItems = [];
                    allItemHandles.forEach(function (itemHandle) {
                        if (itemHandle.selected){
                            selectedItems.push(itemHandle.getFormatData());
                        }
                    });
                    return selectedItems;
                }
                this.keyword = '';
                this.nextPageToken = false;
                this.init = function () {
                }
            }

            this.IAmDone = function () {
                flgOpend = false;
                domRoot.collapse('toggle');
            }
            this.showMore = function () {
                var items = [
                    {name: 'youtube', token: youtubeListHandle.nextPageToken},
                    {name: 'spreaker', token: podcastsListHandle.nextPageToken},
                    {name: 'blogs', token: blogsListHandle.nextPageToken},
                    {name: 'recipes', token: recipesListHandle.nextPageToken},
                    {name: 'ideabox', token: ideaboxListHandle.nextPageToken},
                    {name: 'posts',   token: postsListHandle.nextPageToken},
                    {name: 'rssbPosts', token: rssbPostsListHandle.nextPageToken}
                ];
                searchPosts(domSearch.val(), 10, items).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                    podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                    blogsListHandle.setItems(res.data.blogs, domSearch.val());
                    recipesListHandle.setItems(res.data.recipes, domSearch.val());
                    ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                    postsListHandle.setItems(res.data.posts, domSearch.val());
                    rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                });
            }
            this.setSearchTerm = function (new_term) {
                domSearch.val(new_term);
                domSearchForm.submit();
            }

            this.init = function () {
                youtubeListHandle = new YoutubeListClass();
                podcastsListHandle = new PodcastsListClass();
                blogsListHandle = new BlogsListClass();
                recipesListHandle = new RecipesListClass();
                ideaboxListHandle = new IdeaboxListClass();
                postsListHandle = new postsListClass();
                rssbPostsListHandle = new rssbPostsListClass();

                bindEvents();
                youtubeListHandle.init();
                podcastsListHandle.init();
                blogsListHandle.init();
                recipesListHandle.init();
                ideaboxListHandle.init();
                postsListHandle.init();
                rssbPostsListHandle.init();

                domRoot.find('[name="from"]').each(function () {
                    var val = $(this).val();
                    var checked = $(this).prop('checked');
                    var handle = null;
                    switch (val){
                        case 'youtube':
                            handle = youtubeListHandle;
                            break;
                        case 'spreaker':
                            handle = podcastsListHandle;
                            break;
                        case 'twingly':
                            handle = blogsListHandle;
                            break;
                        case 'recipes':
                            handle = recipesListHandle;
                            break;
                        case 'ideabox':
                            handle = ideaboxListHandle;
                            break;
                        case 'posts':
                            handle = postsListHandle;
                            break;
                        case 'rssbposts':
                            handle = rssbPostsListHandle;
                            break;
                        default:
                            handle = youtubeListHandle;
                            break;
                    }
                    checked ? handle.show() : handle.hide();
                });
            }
        }

        var SwitchFromScratchClass = function () {

            var accept_doc_types = ['application/pdf','text/plain'];
            var accept_image_type = ['image/png', 'image/jpeg'];

            var domRoot, domDescription, domRefLink;
            var parentId = 0;
            var getDataInfo = function (dataTransfer) {

                var droppedUrl = dataTransfer.getData('URL');

                if(droppedUrl != ''){
                    return {url: droppedUrl, type: 'url'}
                }

                if (dataTransfer.files.length < 1){
                    return {type: 'other'};
                }

                var type = dataTransfer.files[0].type;

                if (accept_image_type.indexOf(type) > -1){
                    return {type: 'image', file: dataTransfer.files[0]};
                }else if (accept_doc_types.indexOf(type) > -1){
                    return {type: 'document', file: dataTransfer.files[0]};
                } else {
                    return {type: 'other'};
                }
            }
            var uploadRef = function (file) {
                var ajaxData = new FormData();
                ajaxData.append('action', 'upload_ref');
                ajaxData.append('ref_file', file);
                return $.ajax({
                    url: BASE_URL + '/createideabox',
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res) {
                        if (res.status){
                            domRefLink.val(res.data);
                        }
                        else {
                            alert('Sorry. Something wrong!');
                        }
                    },
                    error: function() {}
                });
            }
            var createIdeabox = function () {
                var sets = {
                    strIdeaBox_title: domRoot.find('[name="title"]').val(),
                    strIdeaBox_idea: domRoot.find('[name="description"]').val(),
                    intIdeaBox_private: domRoot.find('[name="isPrivate"]:checked').val(),
                    intIdeaBox_subcategory: parentId
                }
                var refLink = domRoot.find('[name="refLink"]').val();
                if (refLink){
                    var refText = domRoot.find('[name="refText"]').val();
                    refText = refText ? refText : 'View Doc';
                    sets.strIdeaBox_reference_link = refLink;
                    sets.strIdeaBox_reference_text = refText;
                }
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                ajaxData.append('sets', JSON.stringify(sets));
                if (domRoot.find('[name="cover_img"]').prop('files').length){
                    ajaxData.append('image', domRoot.find('[name="cover_img"]').prop('files')[0]);
                }
                return $.ajax({
                    url: API_ROOT_URL + '/Ideabox.php',
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(res){
                    }
                });
            }
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domRoot).addClass('touched');
                        $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domRoot.submit(function () {
                    createIdeabox().then(function (res) {
                        switchWith(res.data);
                    });
                    return false;
                });
                domRefLink.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                }).on('dragenter', function (e) {
                    domRefLink.addClass('is-dragover');
                }).on('dragleave', function (e) {
                    domRefLink.removeClass('is-dragover');
                }).on('drop', function(e) {
                    domRefLink.removeClass('is-dragover');
                    var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                    switch (dataInfo.type){
                        case 'url':
                            domRefLink.val(dataInfo.url);
                            break;
                        case 'other':
                            break;
                        default:
                            uploadRef(dataInfo.file);
                            break;
                    }
                });
                domRoot.find('.cover-image-wrapper [name="cover_img"]').change(function () {
                    updateCoverImage(this);
                });
            }
            var viewUserPage = function () {
                $('<form action="userpage" method="post" hidden></form>').appendTo('body').submit().remove();
            }
            var viewIdeabox = function (id) {
                $('<form action="ideaboxcards" method="post" hidden><input name="parentId" value="'+ id +'"></form>').appendTo('body').submit().remove();
            }
            var switchWith = function (ideaData) {
                if (owner == 'me'){
                    var sets = {
                        strPost_title: ideaData.strIdeaBox_title,
                        strPost_body: ideaData.strIdeaBox_idea,
                        strPost_summary: ideaData.strIdeaBox_idea,
                        intPost_type: 7,
                        strPost_featuredimage: ideaData.strIdeaBox_image
                    };
                    updatePost(sets);
                }
                else{
                    var sets = {
                        strClientSubscription_title: ideaData.strIdeaBox_title,
                        strClientSubscription_body: ideaData.strIdeaBox_idea,
                        intClientSubscriptions_type: 7,
                        strClientSubscription_image: ideaData.strIdeaBox_image
                    };
                    updateSubscription(sets);
                }
            }
            this.init = function () {
                domRoot = $('#switch-from-scratch #ideabox-form');
                domDescription = domRoot.find('[name="description"]');
                domRefLink = domRoot.find('[name="refLink"]');
                domDescription.wysihtml5({
                    stylesheets: false,
                    image: false,
                    link: false
                });
                bindEvents();
            }
        }

        this.init = function () {
            switchFromExistingHandle = new SwitchFromExistingClass();
            switchFromExistingHandle.init();
            switchOptionsHandle = new SwitchOptionsClass();
            switchOptionsHandle.init();
            switchFromScratchHandle = new SwitchFromScratchClass();
            switchFromScratchHandle.init();
        }
    }
    switchPageHandle = new SwitchPageClass();
    switchPageHandle.init();
    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });
})();