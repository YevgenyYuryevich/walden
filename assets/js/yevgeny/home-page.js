(function () {
    'use strict';

    var PageClass = function () {

        var domPage;
        var categoriesListHandle, testimonialsHandle;
        var categoriesById = [];

        var CategoriesListClass = function () {

            var domRoot, domCategoriesContainer, domTabContent;
            var tabHandles = [], currentTab;

            var scopeSeries = [], scopeSeriesByCategory = [];

            var bindEvents = function () {
                siteSearchHandle.afterMainActive(function () {
                    currentTab.setPosition();
                });
            }

            var TabClass = function (tabSeries, category) {
                var domNavItem, domContent, domItemsContainer;

                var itemHandles = [];
                var isViewed = false;
                var self = this;
                var ItemClass = function (itemData) {
                    var domItem;
                    var self = this, payHandle = null;

                    var viewUserPage = function (data) {
                        $('<form action="my_solutions" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
                    }
                    var popupThanksJoin = function (data) {
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Thanks for joining! Would you like to go to your experiences?',
                            buttons: {
                                returnHome: {
                                    text: "My Experiences",
                                    value: 'return_home',
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                customize: {
                                    text: "Not yet",
                                    value: 'customize',
                                    visible: true,
                                    className: "",
                                    closeModal: true
                                }
                            },
                            closeOnClickOutside: false,
                        }).then(function (value) {
                            if (value == 'return_home'){
                                viewUserPage(data);
                            }
                            else {
                            }
                        });
                        var domSwalRoot, domDisableCheckbox;
                        var disablePopup = function () {
                            localStorage.setItem('walden_show_popup_every_join', '0');
                        }
                        var enabelPopup = function () {
                            localStorage.removeItem('walden_show_popup_every_join');
                        }
                        var bindEvents = function () {
                            domDisableCheckbox.change(function () {
                                $(this).prop('checked') ? disablePopup() : enabelPopup();
                            })
                        }
                        var init = function () {
                            setTimeout(function () {
                                domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                                domSwalRoot.addClass('thanks-join');
                                domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                                domDisableCheckbox = domSwalRoot.find('#disable_popup');
                                bindEvents();
                            }, 100);
                        }();
                    }
                    var popupErroJoin = function () {
                        swal("Sorry, Something went wrong!", {
                            content: 'please try again later'
                        });
                    }

                    var openStripe = function () {
                        if (payHandle) {
                            payHandle.open({
                                name: itemData.strSeries_title,
                                description: itemData.strSeries_description,
                                currency: 'usd',
                                amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                            });
                        }
                        else {
                            popupErroJoin();
                        }
                    }

                    var join = function (token) {
                        ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}).then(function (res) {
                            if (res.status == true){
                                itemData.purchased = res.data;
                                domItem.addClass('purchased');
                                if (!localStorage.getItem('walden_show_popup_every_join')) {
                                    popupThanksJoin(itemData.purchased);
                                }
                            }
                        });
                    }

                    var unJoin = function () {
                        swal("You already joined this series!", {
                            content: 'Thanks!'
                        });
                        return false;

                        ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: itemData.purchased}).then(function (res) {
                            if (res.status == true){
                                itemData.purchased = 0;
                            }
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.join-btn').click(function () {
                            if (payHandle === null && itemData.boolSeries_charge == 1 && itemData.purchased == 0) {
                                if (stripeApi_pKey && itemData.stripe_account_id) {
                                    payHandle = StripeCheckout.configure({
                                        key: stripeApi_pKey,
                                        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                        locale: 'auto',
                                        token: function(token) {
                                            join(token);
                                        }
                                    });
                                }
                            }

                            if (CLIENT_ID > -1){
                                if (parseInt(itemData.boolSeries_charge) === 1 && parseInt(itemData.purchased) == 0) {
                                    openStripe();
                                }
                                else {
                                    parseInt(itemData.purchased) ? unJoin() : join();
                                }
                            }
                            else{
                                swal({
                                    title: "Please Login first!",
                                    icon: "warning",
                                    dangerMode: true,
                                    buttons: {
                                        cancel: "Cancel",
                                        login: "Login"
                                    },
                                }).then(function(value){
                                    if (value == 'login'){
                                        window.location.href = 'login';
                                    }
                                });
                            }
                        });
                    }

                    this.bindData = function () {
                        if (itemData.purchased) {
                            domItem.addClass('purchased');
                        }
                        domItem.find('img.item-image').attr('src', itemData.strSeries_image);
                        domItem.find('.child-count span').text(itemData.itemsCount);
                        domItem.find('.item-title').text(itemData.strSeries_title);
                        domItem.find('.preview-btn').attr('href', 'preview_series?id=' + itemData.series_ID);
                    }
                    this.init = function () {
                        domItem = domContent.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domContent.find('.items-container'));
                        bindEvents();
                    }
                }

                var bindEvents = function () {
                    domNavItem.on('shown.bs.tab', function () {
                        domCategoriesContainer.find('.nav-item').removeClass('active');
                        domNavItem.addClass('active');
                        currentTab = self;
                        if (!isViewed) {
                            isViewed = true;
                            domItemsContainer.slick({
                                arrows: true,
                                infinite: false,
                                centerMode: false,
                                slidesToShow: 5,
                                slidesToScroll: 4,
                                autoplay: false,
                                autoplaySpeed: 5000,
                                nextArrow: domContent.find('.slider-nav.arrow-right'),
                                prevArrow: domContent.find('.slider-nav.arrow-left'),
                                responsive: [
                                    {
                                        breakpoint: 1024,
                                        settings: {
                                            slidesToShow: 4,
                                            slidesToScroll: 3,
                                        }
                                    },
                                    {
                                        breakpoint: 960,
                                        settings: {
                                            slidesToShow: 3,
                                            slidesToScroll: 2,
                                        }
                                    },
                                    {
                                        breakpoint: 768,
                                        settings: {
                                            slidesToShow: 2,
                                            slidesToScroll: 1,
                                            arrows: false,
                                        }
                                    },
                                    {
                                        breakpoint: 576,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                            arrows: false,
                                        }
                                    },
                                ]
                            });
                        }
                    })
                }

                var bindData = function () {
                    domNavItem.attr('href', '#simplify-tab-' + category.category_ID);
                    domNavItem.html(category.strCategory_name);
                    domContent.attr('id', 'simplify-tab-' + category.category_ID);
                }
                this.setPosition = function () {
                    domItemsContainer.slick('setPosition');
                }
                this.active = function () {
                    domCategoriesContainer.find('.nav-item').removeClass('active');
                    domNavItem.addClass('active');
                    domNavItem.tab('show');
                }
                this.init = function () {
                    domNavItem = domRoot.find('a.nav-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domCategoriesContainer);
                    domContent = domRoot.find('.tab-pane.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domTabContent);
                    domItemsContainer = domContent.find('.items-container');
                    bindData();
                    tabSeries.forEach(function (tabSery) {
                        var hdl = new ItemClass(tabSery);
                        hdl.init();
                        hdl.bindData();
                        itemHandles.push(hdl);
                    });
                    bindEvents();
                }
            }

            var scopingData = function () {
                scopeSeries = series.filter(function (s) {
                    return true;
                });
                categories.forEach(function (c) {
                    var cSeries = scopeSeries.filter(function (value) { return parseInt(value.intSeries_category) === parseInt(c.category_ID) });
                    if (cSeries.length) {
                        scopeSeriesByCategory.push({category: c, series: cSeries});
                    }
                });
                scopeSeriesByCategory.sort(function (a, b) {
                    return b.series.length - a.series.length;
                });
            }

            this.init = function () {
                domRoot = domPage.find('section.do-your-life.categories-list-wrapper');
                domCategoriesContainer = domRoot.find('.categories-container');
                domTabContent = domRoot.find('.tab-content');
                scopingData();
                var hdl = new TabClass(scopeSeries, {strCategory_name: 'All categories', category_ID: 0});
                hdl.init();
                tabHandles.push(hdl);
                for(var i = 0; i < scopeSeriesByCategory.length; i ++){
                    hdl = new TabClass(scopeSeriesByCategory[i].series, scopeSeriesByCategory[i].category);
                    hdl.init();
                    tabHandles.push(hdl);
                }
                bindEvents();
                tabHandles[0].active();
                domCategoriesContainer.slick({
                    infinite: false,
                    // slidesToShow: 7,
                    slidesToScroll: 3,
                    variableWidth: true,
                    dots: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToScroll: 2,
                            }
                        },
                        {
                            breakpoint: 960,
                            settings: {
                                slidesToScroll: 1,
                            }
                        },
                    ]
                });
            }
        }
        var TestimonialsClass = function () {
            var domRoot;
            var domList, domItemCenter;
            var length = 0, lifeCnt;

            var bindEvents = function () {
                if (window.innerWidth > 768 || true) {
                    setInterval(function () {
                        rotate();
                    }, 8000);
                }
            }
            var rotate = function () {
                var y = domItemCenter.position().top;
                domList.css('bottom', y + 'px');
                domItemCenter.prev().removeClass('item-middle').clone().appendTo(domList);
                domItemCenter.removeClass('item-center');
                domItemCenter.next().addClass('item-center');
                domItemCenter.next().next().addClass('item-middle');
                domItemCenter = domItemCenter.next();
                var labelTxt = domItemCenter.find('.item-label').html();
                domRoot.find('.label-text').css('opacity', 0);
                setTimeout(function () {
                    domRoot.find('.label-text').addClass('changing').html(labelTxt);
                    domRoot.find('.label-text').css('opacity', 1);
                }, 300);
                lifeCnt--;
                if (lifeCnt === 0) {
                    setTimeout(function () {
                        domList.css('transition', 'none');
                        domList.css('bottom', '0px');
                        domList.find('.testimonial-item').each(function (i) {
                            if (i < length) {
                                $(this).remove();
                            }
                        });
                        setTimeout(function () {
                            domList.css('transition', '');
                        }, 300);
                    }, 400);
                    lifeCnt = length;
                }
            }
            this.init = function () {
                domRoot = domPage.find('.create-with-people .testimonials-wrapper');
                domList = domRoot.find('.testimonials-list');
                domItemCenter = domList.find('.item-center');
                length = domList.find('.testimonial-item').length;
                lifeCnt = length;
                bindEvents();
            }
        }
        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            categories.forEach(function (v) {
                categoriesById[v.category_ID] = v;
            });
            categoriesListHandle = new CategoriesListClass();
            categoriesListHandle.init();
            testimonialsHandle = new TestimonialsClass();
            testimonialsHandle.init();
            authHandle.afterLogin(function () {
                window.location.href = 'my_solutions';
            });
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();