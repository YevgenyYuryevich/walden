(function () {

    'use strict'

    var $ = jQuery;
    var calendarPageHandle = null;
    var is_loading_controlled_in_local = false;

    function mobilecheck() {
        var check = false;
        (function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }

    var animEndEventNames = {
            'WebkitAnimation' : 'webkitAnimationEnd',
            'OAnimation' : 'oAnimationEnd',
            'msAnimation' : 'MSAnimationEnd',
            'animation' : 'animationend'
        },
        // animation end event name
        animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
        // event type (if mobile use touch events)
        eventtype = mobilecheck() ? 'tou' +
            'chstart' : 'click',
        // support for css animations
        support = Modernizr.cssanimations;

    function onAnimationEnd( elems, len, callback ) {
        var re_len = 0;
        elems.forEach(function (el) {
            if (el.hasChildNodes()){
                re_len++;
            }
        });
        if (re_len === 0){
            callback.call();
        }
        var finished = 0,
            onEndFn = function() {
                this.removeEventListener( animEndEventName, onEndFn );
                ++finished;
                if( finished === re_len ) {
                    callback.call();
                }
            };
        elems.forEach( function( el,i ) {
            if (el.querySelector('a') !== null){
                el.querySelector('a').addEventListener( animEndEventName, onEndFn );
            }
        });
    }

    var calendarPageClass = function (data) {

        var dom_root = $('.site-wrapper');

        var selectBoxHandle = null;
        var mainContentHandle = null;

        var selectBoxClass = function () {

            var dom_selectbox_root = $('header.content-header', dom_root);
            var dom_dropdown = $('.dropdown', dom_selectbox_root);
            var dom_dropdown_menu = $('.dropdown-menu', dom_dropdown);

            var series = [];

            data.forEach(function (sery) {
                series.push({id: sery.series_ID, title: sery.strSeries_title});
            });

            var selectSery = function (series_id) {
                dom_dropdown_menu.find('#select-series-checkbox-' + series_id).parents('li').click();
            }

            this.selectSery = selectSery;

            this.init = function () {
                series.forEach(function (sery) {
                    var checkbox_html = '<li><a href="javascript:;"><input id="select-series-checkbox-' + sery.id + '" type="checkbox" value="' + sery.id + '"/><label for="select-series-checkbox-' + sery.id + '">' + sery.title + '</label></a></li>';
                    dom_dropdown_menu.append(checkbox_html);
                });
                dom_dropdown_menu.find('li').click(function () {
                    var checkbox = $(this).find('input');
                    checkbox.prop('checked', !checkbox.prop('checked')).change();
                    return false;
                });
                dom_dropdown_menu.find(':checkbox').change(function () {
                    mainContentHandle.toggleContentRow($(this).val(), this.checked);
                });
            }
        }
        var mainContentClass = function () {

            var dom_main_content_root = $('main.main-content', dom_root);

            var contentRowHandles = [];
            var indexOfId = [];

            var contentRowClass = function (sery) {
                var series_title = sery.strSeries_title;
                var dom_content_row = $('.content-row-sample section.content-row', dom_main_content_root).clone().appendTo(dom_main_content_root);
                var dom_content_row_title = dom_content_row.find('>header.content-row-title').html(series_title);
                var dom_content_row_body = dom_content_row.find('>div.content-row-body');
                var dom_grid = dom_content_row_body.find('.tt-grid');
                var dom_previous = $('.previous-page', dom_content_row_body);
                var dom_next = $('.next-page', dom_content_row_body);

                var allItemObjects = [];
                var pages = [];
                var current_page_index = 0;
                var pages_size = 0;
                var item_count_page = 5;

                var trashPost = function(post_id){
                    swal({
                        // title: 'Are you sure?',
                        text: "Are you sure you want to trash this post?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function () {
                        var ajaxData = {
                            action: 'trash_post',
                            purchased_id: sery.purchased_ID,
                            post_id: post_id
                        };
                        $.ajax({
                            url: ACTION_URL,
                            type: 'post',
                            data: ajaxData,
                            dataType: 'json',
                            success: function (res) {
                                for (var i = 0; allItemObjects[i]; i ++){

                                    if (allItemObjects[i].getPostId() == post_id){
                                        allItemObjects.splice(i, 1);
                                        break;
                                    }
                                }
                                refreshRow();
                            }
                        });
                    }, function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                    });
                }

                var postItemClass = function (post_data) {


                    var postItemHandle = this;
                    var switchLightboxHandle = null;

                    var post_id = post_data.post_ID;
                    var subscription_id = post_data.clientsubscription_ID;
                    var post_title = post_data.strPost_title;
                    var subscription_title = post_data.strClientSubscription_title;
                    var post_image = post_data.strPost_featuredimage;
                    var subscription_image = post_data.strClientSubscription_image;
                    var display_order = post_data.intClientSubscriptions_order;

                    var used_title = subscription_title ||  post_title;
                    var used_description = '';
                    var used_type = post_data.intClientSubscriptions_type || post_data.intPost_type;
                    var used_image = subscription_image || post_image;
                    var display_description = '';

                    var origin_height = 0;

                    var dom_post_item_root = null;
                    var dom_title = null;
                    var dom_image = null;
                    var dom_switch_button = null;
                    var dom_open_in_view = null;
                    var dom_make_trash = null;

                    var html = '<a data-order="'+ display_order +'">' +
                        '<div class="column">' +
                        '<div class="column_object">' +
                        '<div class="image-wrapper"><img src="' + used_image + '" alt="" class="object_image" /></div>' +
                        '<div class="title-wrapper">' +
                        '<div class="object_title">' + used_title + '</div>' +
                        '<div class="view-more-wrapper">' +
                        '<span class="view-more" href="javascript:;">view more ...</span>' +
                        '<span class="view-less" href="javascript:;">view less</span>' +
                        '</div>' +
                        '</div>'+
                        '<div class="buttons-wrapper">' +
                        '<span class="open-in-view" onclick="window.open(\'' + BASE_URL + '/view.php?id='+ post_id +'&prevpage=calendar\');"><img src="assets/images/global-icons/read.png"></span>' +
                        '<div class="switch-button" data-post_id="' + post_id + '">' +
                        '<span href="javascript:;" class="open-switch-lightbox"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></span>' +
                        '</div>' +
                        '<span class="make-trash"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></span>' +
                        '</div>' +
                        '<div class="object_line"></div>' +
                        '</div>' +
                        '</div>' +
                        '</a>';
                    var updateInFront = function (new_data) {

                        post_id = new_data.post_ID;
                        post_title = new_data.strPost_title;
                        subscription_title = new_data.strClientSubscription_title;
                        post_image = new_data.strPost_featuredimage;
                        subscription_image = new_data.strClientSubscription_image;

                        used_title = subscription_title ||  post_title;
                        used_image = subscription_image || post_image;

                        dom_image.attr('src', used_image);
                        dom_title.html(used_title);
                        dom_switch_button.attr('data-post_id', post_id);
                        dom_open_in_view.attr('onclick', "window.open('" + BASE_URL + "/view.php?id=" + post_id + "&prevpage=calendar');");

                        html = '<a data-order="'+ display_order +'">' +
                            '<div class="column">' +
                            '<div class="column_object">' +
                            '<div class="image-wrapper"><img src="' + used_image + '" alt="" class="object_image" /></div>' +
                            '<div class="title-wrapper">' +
                            '<div class="object_title">' + used_title + '</div>' +
                            '<div class="view-more-wrapper">' +
                            '<span class="view-more" href="javascript:;">view more ...</span>' +
                            '<span class="view-less" href="javascript:;">view less</span>' +
                            '</div>' +
                            '</div>'+
                            '<div class="buttons-wrapper">' +
                            '<span class="open-in-view" onclick="window.open(\'' + BASE_URL + '/view.php?id='+ post_id +'&prevpage=calendar\');"><img src="assets/images/global-icons/read.png"></span>' +
                            '<div class="switch-button" data-post_id="' + post_id + '">' +
                            '<span href="javascript:;" class="open-switch-lightbox"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></span>' +
                            '</div>' +
                            '<span class="make-trash"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></span>' +
                            '</div>' +
                            '<div class="object_line"></div>' +
                            '</div>' +
                            '</div>' +
                            '</a>';
                    }
                    var updateSubscription = function (new_subscription_image, new_subscription_title) {
                        subscription_image = new_subscription_image;
                        subscription_title = new_subscription_title;
                        used_title = subscription_title ||  post_title;
                        used_image = subscription_image || post_image;

                        dom_image.attr('src', used_image);
                        dom_title.html(used_title);

                        html = '<a data-order="'+ display_order +'">' +
                            '<div class="column">' +
                            '<div class="column_object">' +
                            '<div class="image-wrapper"><img src="' + used_image + '" alt="" class="object_image" /></div>' +
                            '<div class="object_title">' + used_title + '</div>' +
                            '<div class="buttons-wrapper">' +
                            '<span class="open-in-view" onclick="window.open(\'' + BASE_URL + '/view.php?id='+ post_id +'&prevpage=calendar\');"><img src="assets/images/global-icons/read.png"></span>' +
                            '<div class="switch-button" data-post_id="' + post_id + '">' +
                            '<span href="javascript:;" class="open-switch-lightbox"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></span>' +
                            '</div>' +
                            '<span class="make-trash"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></span>' +
                            '</div>' +
                            '<div class="object_line"></div>' +
                            '</div>' +
                            '</div>' +
                            '</a>';
                    }
                    var updateOrderInFront = function (new_order) {
                        display_order = new_order;
                        html = html = '<a data-order="'+ display_order +'">' +
                            '<div class="column">' +
                            '<div class="column_object">' +
                            '<div class="image-wrapper"><img src="' + used_image + '" alt="" class="object_image" /></div>' +
                            '<div class="object_title">' + used_title + '</div>' +
                            '<div class="buttons-wrapper">' +
                            '<span class="open-in-view" onclick="window.open(\'' + BASE_URL + '/view.php?id='+ post_id +'&prevpage=calendar\');"><img src="assets/images/global-icons/read.png"></span>' +
                            '<div class="switch-button" data-post_id="' + post_id + '">' +
                            '<span href="javascript:;" class="open-switch-lightbox"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></span>' +
                            '</div>' +
                            '<span class="make-trash"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></span>' +
                            '</div>' +
                            '<div class="object_line"></div>' +
                            '</div>' +
                            '</div>' +
                            '</a>';
                        dom_post_item_root.find('a').attr('data-order', new_order);
                    }

                    var switchLightboxClass = function () {

                        var data = sery.lightbox_data;

                        var count_per_page_on_switch_lightbox = 4;
                        var series_id = sery.series_ID;
                        var purchased_id = sery.purchased_ID;

                        var dom_open_from = dom_switch_button;

                        var root_dom = $('.switch-lightbox-wrapper .switch-lightbox').clone().appendTo('body');
                        var dom_lightbox_wrapper = $('.switch-wrapper', root_dom);
                        var dom_close_button = $('.close', root_dom);

                        var newPostsSectionObject = null;
                        var favoritePostsSectionObject = null;
                        var ideaboxPostsSectionObject = null;

                        var ideaboxes = data.ideaboxes;
                        var favorites = data.favorites;
                        var new_posts = data.new_posts.filter(function (new_post) {
                            return new_post.post_ID > post_id;
                        });

                        var openLightBox = function(){
                            dom_open_from.addClass('open-lightbox-from-state');
                            setTimeout(function () {
                                root_dom.css('display', 'block');
                                var pos = dom_open_from.offset();
                                pos.top = pos.top - $(window).scrollTop();
                                var width = dom_open_from.outerWidth();
                                var height = dom_open_from.outerHeight();
                                pos.right = $(window).width() - pos.left - width;
                                pos.bottom = $(window).height() - pos.top - height;
                                dom_lightbox_wrapper.css(pos);
                                dom_lightbox_wrapper.animate({
                                    top: '5vh',
                                    right: '10vw',
                                    left: '10vw',
                                    bottom: '5vh',
                                    fontSize: '10px',
                                    padding: '50px'
                                }, {
                                    duration: 500,
                                    complete: function () {
                                        if (newPostsSectionObject == null){
                                            newPostsSectionObject = new newPostsSectionHandle();
                                        }
                                        newPostsSectionObject.init();
                                        if (favoritePostsSectionObject == null){
                                            favoritePostsSectionObject = new favoritePostsSectionHandle();
                                        }
                                        favoritePostsSectionObject.init();
                                        if (ideaboxPostsSectionObject == null){
                                            ideaboxPostsSectionObject = new ideaboxPostsSectionHandle();
                                        }
                                        ideaboxPostsSectionObject.init();
                                    }
                                });
                            }, 100);
                        }
                        var closeLightBox = function () {

                            newPostsSectionObject.clean();
                            favoritePostsSectionObject.clean();
                            ideaboxPostsSectionObject.clean();

                            var pos = dom_open_from.offset();
                            pos.top = pos.top - $(window).scrollTop();
                            var width = dom_open_from.outerWidth();
                            var height = dom_open_from.outerHeight();
                            pos.right = $(window).width() - pos.left - width;
                            pos.bottom = $(window).height() - pos.top - height;

                            var css = $.extend(pos, {opacity: 0, fontSize: '5px', padding: '0px'});
                            dom_lightbox_wrapper.animate(
                                css,
                                {
                                    duration: 500,
                                    start: function () {
                                    },
                                    complete: function () {
                                        dom_lightbox_wrapper.removeAttr('style');
                                        root_dom.css('display', 'none');
                                        dom_open_from.removeClass('open-lightbox-from-state');
                                        root_dom.remove();
                                    }
                                }
                            );
                        };

                        var newPostsSectionHandle = function () {

                            var root_dom_new_posts = $('.new-posts', root_dom);
                            var dom_grid_wrapper = $('.tt-grid-wrapper', root_dom_new_posts);
                            var dom_list_wrapper = $('ul.tt-grid', root_dom_new_posts);
                            var dom_previous = $('.previous-page', root_dom_new_posts);
                            var dom_next = $('.next-page', root_dom_new_posts);

                            var pages = [];
                            var current_page_index = 0;
                            var pages_size = 0;

                            new_posts.forEach(function (post, i) {
                                var page_index = parseInt(i / count_per_page_on_switch_lightbox);
                                post.html = '<a>' +
                                    '<div class="column">' +
                                    '<div class="column_object">' +
                                    '<div class="image-wrapper has-advanced-upload"><img src="' + post.strPost_featuredimage + '" alt="" class="object_image" /></div>' +
                                    '<div class="object_title">' + post.strPost_title + '</div>' +
                                    '<div class="buttons-wrapper">' +
                                    '<div class="select-post-button" data-post_id="' + post.post_ID + '">' +
                                    '<div class="wrap"><button type="submit">Select</button><img src="../assets/images/check_arrow_2.svg" alt=""><svg width="42px" height="42px"><circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle></svg></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="object_line"></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</a>';

                                if (typeof pages[page_index] == 'undefined'){
                                    pages[page_index] = [];
                                }
                                pages[page_index].push(post);
                                pages_size = page_index + 1;
                            });

                            var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                                isAnimating = false;

                            var selectPost = function (post_id) {
                                var ajaxData = {
                                    action: 'set_post_as_subscription',
                                    subscription_ID: subscription_id,
                                    purchased_ID: purchased_id,
                                    post_ID: post_id
                                };

                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajaxData,
                                    dataType: 'json',
                                    success: function (res) {
                                        var res_data = res.data;
                                        postItemHandle.updateInFront(res_data);
                                        closeLightBox();
                                    }
                                });
                            }

                            var addFuncToEl = function (el) {
                                el .find(':not(.tt-old)').find('.buttons-wrapper .select-post-button').click(function () {
                                    selectPost($(this).data('post_id'));
                                })
                            }

                            // this is just a way we can test this. You would probably get your images with an AJAX request...

                            function loadNewSet(action, set ) {
                                if (isAnimating === true){
                                    return ;
                                }
                                isAnimating = true;

                                var newImages = pages[current_page_index];
                                if (action === 'pagination'){
                                    newImages = pages[set];
                                    current_page_index = set;
                                }
                                items.forEach( function( el ) {
                                    var itemChild = el.querySelector( 'a' );
                                    // add class "tt-old" to the elements/images that are going to get removed
                                    if( itemChild ) {
                                        classie.add( itemChild, 'tt-old' );
                                    }
                                } );

                                // apply effect
                                setTimeout( function() {

                                    // append new elements
                                    if(newImages){

                                        [].forEach.call( newImages, function( el, i ) {
                                            items[ i ].innerHTML += el.html;
                                        } );
                                    }

                                    // add "effect" class to the grid
                                    classie.add( grid, 'tt-effect-active' );


                                    // wait that animations end
                                    var onEndAnimFn = function() {
                                        // remove old elements
                                        items.forEach( function( el ) {
                                            // remove old elems
                                            var old = el.querySelector( '.tt-old' );
                                            if( old ) { el.removeChild( old ); }
                                            // remove class "tt-empty" from the empty items
                                            classie.remove( el, 'tt-empty' );
                                            // now apply that same class to the items that got no children (special case)
                                            if ( !el.hasChildNodes() ) {
                                                classie.add( el, 'tt-empty' );
                                            }
                                            addFuncToEl($(el));
                                        } );
                                        // remove the "effect" class
                                        classie.remove( grid, 'tt-effect-active' );
                                        isAnimating = false;
                                    };

                                    if( support ) {
                                        onAnimationEnd( items, items.length, onEndAnimFn );
                                    }
                                    else {
                                        onEndAnimFn.call();
                                    }
                                }, 25 );
                            }

                            var previous = function () {
                                if (current_page_index > 0 && !isAnimating){
                                    loadNewSet('pagination', current_page_index - 1);
                                }
                            }
                            var next = function () {
                                if (current_page_index < pages_size - 1 && !isAnimating){
                                    loadNewSet('pagination', current_page_index + 1);
                                }
                            }

                            var flipInit = function() {
                                grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' );
                                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                                isAnimating = false;
                                loadNewSet();
                            }

                            return {
                                pages: pages,
                                init: function () {
                                    dom_list_wrapper.html('');
                                    for(var i = 0; i < count_per_page_on_switch_lightbox; i++){
                                        dom_list_wrapper.append('<li class="tt-empty"></li>');
                                    }
                                    flipInit();
                                    dom_previous.click(function () {
                                        previous();
                                    });
                                    dom_next.click(function () {
                                        next();
                                    });
                                },
                                clean: function () {
                                    dom_list_wrapper.html('');
                                }
                            };
                        }
                        var favoritePostsSectionHandle = function () {

                            var root_dom_favorite_posts = $('.favorite-posts', root_dom);
                            var dom_grid_wrapper = $('.tt-grid-wrapper', root_dom_favorite_posts);
                            var dom_list_wrapper = $('ul.tt-grid', root_dom_favorite_posts);
                            var dom_previous = $('.previous-page', root_dom_favorite_posts);
                            var dom_next = $('.next-page', root_dom_favorite_posts);

                            var pages = [];
                            var current_page_index = 0;
                            var pages_size = 0;

                            favorites.forEach(function (post, i) {
                                var page_index = parseInt(i / count_per_page_on_switch_lightbox);
                                post.html = '<a>' +
                                    '<div class="column">' +
                                    '<div class="column_object">' +
                                    '<div class="image-wrapper has-advanced-upload"><img src="' + post.strPost_featuredimage + '" alt="" class="object_image" /></div>' +
                                    '<div class="object_title">' + post.strPost_title + '</div>' +
                                    '<div class="buttons-wrapper">' +
                                    '<div class="select-post-button" data-post_id="' + post.post_ID + '">' +
                                    '<div class="wrap"><button type="submit">Select</button><img src="../assets/images/check_arrow_2.svg" alt=""><svg width="42px" height="42px"><circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle></svg></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="object_line"></div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</a>';

                                if (typeof pages[page_index] == 'undefined'){
                                    pages[page_index] = [];
                                }
                                pages[page_index].push(post);
                                pages_size = page_index + 1;
                            });

                            var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                                isAnimating = false;

                            var selectPost = function (post_id) {
                                var ajaxData = {
                                    action: 'set_post_as_subscription',
                                    subscription_ID: subscription_id,
                                    purchased_ID: purchased_id,
                                    post_ID: post_id
                                };

                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajaxData,
                                    dataType: 'json',
                                    success: function (res) {
                                        var res_data = res.data;
                                        postItemHandle.updateInFront(res_data);
                                        closeLightBox();
                                    }
                                });
                            }

                            var addFuncToEl = function (el) {
                                el.find(':not(.tt-old)').find('.buttons-wrapper .select-post-button').click(function () {
                                    selectPost($(this).data('post_id'));
                                })
                            }

                            // this is just a way we can test this. You would probably get your images with an AJAX request...

                            function loadNewSet(action, set ) {
                                if (isAnimating === true){
                                    return ;
                                }
                                isAnimating = true;

                                var newImages = pages[current_page_index];
                                if (action === 'pagination'){
                                    newImages = pages[set];
                                    current_page_index = set;
                                }
                                items.forEach( function( el ) {
                                    var itemChild = el.querySelector( 'a' );
                                    // add class "tt-old" to the elements/images that are going to get removed
                                    if( itemChild ) {
                                        classie.add( itemChild, 'tt-old' );
                                    }
                                } );

                                // apply effect
                                setTimeout( function() {

                                    // append new elements
                                    if(newImages){

                                        [].forEach.call( newImages, function( el, i ) {
                                            items[ i ].innerHTML += el.html;
                                        } );
                                    }

                                    // add "effect" class to the grid
                                    classie.add( grid, 'tt-effect-active' );


                                    // wait that animations end
                                    var onEndAnimFn = function() {
                                        // remove old elements
                                        items.forEach( function( el ) {
                                            // remove old elems
                                            var old = el.querySelector( '.tt-old' );
                                            if( old ) { el.removeChild( old ); }
                                            // remove class "tt-empty" from the empty items
                                            classie.remove( el, 'tt-empty' );
                                            // now apply that same class to the items that got no children (special case)
                                            if ( !el.hasChildNodes() ) {
                                                classie.add( el, 'tt-empty' );
                                            }
                                            addFuncToEl($(el));
                                        } );
                                        // remove the "effect" class
                                        classie.remove( grid, 'tt-effect-active' );
                                        isAnimating = false;
                                    };

                                    if( support ) {
                                        onAnimationEnd( items, items.length, onEndAnimFn );
                                    }
                                    else {
                                        onEndAnimFn.call();
                                    }
                                }, 25 );
                            }

                            var previous = function () {
                                if (current_page_index > 0 && !isAnimating){
                                    loadNewSet('pagination', current_page_index - 1);
                                }
                            }
                            var next = function () {
                                if (current_page_index < pages_size - 1 && !isAnimating){
                                    loadNewSet('pagination', current_page_index + 1);
                                }
                            }

                            var flipInit = function() {
                                grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' );
                                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                                isAnimating = false;
                                loadNewSet();
                            }

                            return {
                                pages: pages,
                                init: function () {
                                    dom_list_wrapper.html('');
                                    for(var i = 0; i < count_per_page_on_switch_lightbox; i++){
                                        dom_list_wrapper.append('<li class="tt-empty"></li>');
                                    }
                                    flipInit();
                                    dom_previous.click(function () {
                                        previous();
                                    });
                                    dom_next.click(function () {
                                        next();
                                    });
                                },
                                clean: function () {
                                    dom_list_wrapper.html('');
                                }
                            };
                        }
                        var ideaboxPostsSectionHandle = function () {

                            var root_dom_ideabox_posts = $('.ideabox-posts', root_dom);
                            var dom_grid_wrapper = $('.tt-grid-wrapper', root_dom_ideabox_posts);
                            var dom_list_wrapper = $('ul.tt-grid', root_dom_ideabox_posts);
                            var dom_previous = $('.previous-page', root_dom_ideabox_posts);
                            var dom_next = $('.next-page', root_dom_ideabox_posts);
                            var dom_back = $('.back-to-top-level', root_dom_ideabox_posts);

                            // need
                            var allItems = ideaboxes;
                            var current_subcategory_id = 0;
                            var allGroups = [], current_group, current_page_index = 0;
                            var keys_of_id = [];

                            var refreshItems = function () {
                                keys_of_id = [];
                                allGroups = [];
                                allItems.forEach(function (post, i) {
                                    keys_of_id[post.ideabox_ID] = i;
                                    post.html = '<a>' +
                                        '<div class="column">' +
                                        '<div class="column_object">' +
                                        '<div class="image-wrapper has-advanced-upload"><img src="' + (post.strIdeaBox_image ? post.strIdeaBox_image : DEFAULT_IDEABOX_IMAGE) + '" alt="" class="object_image" /></div>' +
                                        '<div class="object_title">' + post.strIdeaBox_title + '</div>' +
                                        '<div class="buttons-wrapper">' +
                                        '<div data-id = "' + post.ideabox_ID + '" class="explorer-ideabox-button" data-link = "load-group"><div><img src="assets/images/global-icons/ideabox.png"></div>Explore</div>' +
                                        '<div class="select-post-button" data-ideabox_id="' + post.ideabox_ID + '">' +
                                        '<div class="wrap"><button type="submit">Select</button><img src="../assets/images/check_arrow_2.svg" alt=""><svg width="42px" height="42px"><circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle></svg></div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="object_line"></div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>';
                                });

                                var flags = [];
                                allItems.forEach(function (item) {
                                    var subcategory_id = item.intIdeaBox_subcategory;

                                    if (flags[subcategory_id] === 1){ return; }

                                    allGroups[subcategory_id] = allItems.filter(function (item) {
                                        return item.intIdeaBox_subcategory === subcategory_id;
                                    });
                                    flags[subcategory_id] = 1;
                                });

                                allGroups.forEach(function (group) {
                                    var len = group.length;

                                    group.pages = [];
                                    for (var i = 0; i*count_per_page_on_switch_lightbox < len; i ++ ){
                                        group.pages[i] = [];
                                        group.pages_size = i + 1;
                                        for(var j = i*count_per_page_on_switch_lightbox; j < (i + 1)*count_per_page_on_switch_lightbox && j < len; j++){
                                            group.pages[i].push(group[j].html);
                                        }
                                    }
                                });
                            };

                            refreshItems();
                            current_group = allGroups[current_subcategory_id];

                            var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                                isAnimating = false;

                            var setIdeaboxAsSubscription = function (ideabox_id) {

                                var ajaxData = {
                                    action: 'set_ideabox_as_subscription',
                                    subscription_ID: subscription_id,
                                    ideabox_ID: ideabox_id,
                                    type: 7
                                };

                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajaxData,
                                    dataType: 'json',
                                    success: function (res) {
                                        if (res.status){
                                            updateSubscription(res.data.strClientSubscription_image, res.data.strClientSubscription_title);
                                        }
                                        closeLightBox();
                                    }
                                });
                            }

                            var back = function () {
                                if (current_subcategory_id == 0){ return; }
                                var parent = allItems[keys_of_id[current_subcategory_id]];
                                loadNewSet('load_setted_group', {subcategory_id: parent.intIdeaBox_subcategory})
                            }

                            var addFuncToEl = function (el) {
                                el.find(':not(.tt-old)').find('.buttons-wrapper .select-post-button').click(function () {
                                    var ideabox_id = $(this).data('ideabox_id');
                                    setIdeaboxAsSubscription(ideabox_id);
                                });
                                el.find(':not(.tt-old)').find('.buttons-wrapper .explorer-ideabox-button').click(function () {
                                    loadNewSet('load_setted_group', {subcategory_id: $(this).data('id')});
                                });
                            }

                            // this is just a way we can test this. You would probably get your images with an AJAX request...

                            function loadNewSet(action, set ) {

                                if (isAnimating === true){
                                    return ;
                                }
                                isAnimating = true;

                                var newImages = (current_page_index > -1 && current_group) ? current_group.pages[current_page_index] : [];

                                if (action === 'pagination'){
                                    current_page_index = set;
                                    newImages = current_group.pages[current_page_index];
                                }
                                else if (action === 'load_setted_group'){
                                    current_page_index = 0;
                                    current_subcategory_id = set.subcategory_id;
                                    current_group = allGroups[current_subcategory_id];

                                    if (typeof current_group !== 'undefined'){
                                        newImages = current_group.pages[current_page_index];
                                    }
                                    else {
                                        current_page_index = -1;
                                        newImages = [];
                                    }
                                }
                                items.forEach( function( el ) {
                                    var itemChild = el.querySelector( 'a' );
                                    // add class "tt-old" to the elements/images that are going to get removed
                                    if( itemChild ) {
                                        classie.add( itemChild, 'tt-old' );
                                    }
                                } );

                                // apply effect
                                setTimeout( function() {

                                    // append new elements
                                    if(newImages){

                                        [].forEach.call( newImages, function( el, i ) {
                                            items[ i ].innerHTML += el;
                                        } );
                                    }

                                    // add "effect" class to the grid
                                    classie.add( grid, 'tt-effect-active' );


                                    // wait that animations end
                                    var onEndAnimFn = function() {
                                        // remove old elements
                                        items.forEach( function( el ) {
                                            // remove old elems
                                            var old = el.querySelector( '.tt-old' );
                                            if( old ) { el.removeChild( old ); }
                                            // remove class "tt-empty" from the empty items
                                            classie.remove( el, 'tt-empty' );
                                            // now apply that same class to the items that got no children (special case)
                                            if ( !el.hasChildNodes() ) {
                                                classie.add( el, 'tt-empty' );
                                            }
                                            addFuncToEl($(el));
                                        } );
                                        // remove the "effect" class
                                        classie.remove( grid, 'tt-effect-active' );
                                        isAnimating = false;
                                    };

                                    if( support ) {
                                        onAnimationEnd( items, items.length, onEndAnimFn );
                                    }
                                    else {
                                        onEndAnimFn.call();
                                    }
                                }, 25 );
                            }

                            var previous = function () {
                                if (current_page_index > 0 && !isAnimating){
                                    loadNewSet('pagination', current_page_index - 1);
                                }
                            }
                            var next = function () {
                                if (current_page_index > -1 && current_page_index < current_group.pages_size - 1 && !isAnimating){
                                    loadNewSet('pagination', current_page_index + 1);
                                }
                            }

                            var flipInit = function() {
                                grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' );
                                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                                isAnimating = false;
                                loadNewSet();
                            }

                            return {
                                allGroups: allGroups,
                                init: function () {
                                    dom_list_wrapper.html('');
                                    for(var i = 0; i < count_per_page_on_switch_lightbox; i++){
                                        dom_list_wrapper.append('<li class="tt-empty"></li>');
                                    }
                                    flipInit();
                                    dom_previous.click(function () {
                                        previous();
                                    });
                                    dom_next.click(function () {
                                        next();
                                    });
                                    dom_back.click(function () {
                                        back();
                                    });
                                },
                                clean: function () {
                                    dom_list_wrapper.html('');
                                }
                            };
                        }

                        return {
                            root_dom: root_dom,
                            openLightBox: openLightBox,
                            init: function () {
                                dom_close_button.click(function () {
                                    closeLightBox();
                                });
                            }
                        };
                    }

                    var getHtml = function () { return html; }
                    var getOrder = function () { return display_order; }
                    var getSubscriptionId = function () { return subscription_id; }
                    var getPostId = function () {
                        return post_id;
                    }

                    this.getHtml = getHtml,
                    this.bindDom = function (dom_item_root) {
                        dom_post_item_root = dom_item_root;
                        dom_title = $('.object_title', dom_post_item_root);
                        dom_image = $('.image-wrapper img.object_image', dom_post_item_root);
                        dom_switch_button = $('.switch-button', dom_post_item_root);
                        dom_open_in_view = $('.open-in-view', dom_post_item_root);
                        dom_make_trash = $('.make-trash', dom_post_item_root);

                        dom_switch_button.click(function () {
                            switchLightboxHandle = null;
                            switchLightboxHandle = new switchLightboxClass();
                            switchLightboxHandle.init();
                            switchLightboxHandle.openLightBox();
                        });
                        dom_title.qtip({
                            content: {
                                text: function(event, api) {
                                    $.ajax({ url: ACTION_URL, data: {action: 'get_description', 'post_id': post_data.post_ID, subscription_id: post_data.clientsubscription_ID}, type: 'post', dataType: 'json'})
                                        .done(function(res) {
                                            used_description = res.data;
                                            switch (used_type){
                                                case '2':
                                                    display_description = '<a href="'+ used_description +'" target="_blank">' + used_description + '</a>';
                                                    break;
                                                case '0':
                                                    display_description = '<a href="'+ used_description +'" target="_blank">' + BASE_URL + used_description + '</a>';
                                                    break;
                                                default:
                                                    display_description = used_description;
                                                    break;
                                            }
                                            api.set('content.text', display_description)
                                        })
                                        .fail(function(xhr, status, error) {
                                            api.set('content.text', status + ': ' + error)
                                        })

                                    return 'Loading...';
                                },
                                title: {
                                    text: 'Description',
                                    button: true
                                }
                            },
                            position: {
                                target: dom_title,
                                at: 'bottom center',
                                my: 'top center'
                            },
                            hide: {
                                fixed: true,
                                delay: 1000
                            }
                        });

                        dom_make_trash.click(function () {
                            trashPost(post_id);
                        });
                        var clone_title = dom_title.clone(true).css({height: 'auto', position: 'absolute', maxHeight: 'none', width: '100%', top: '10000px'}).appendTo(dom_item_root.find('.title-wrapper'));

                        origin_height = clone_title.height();

                        dom_title.css({maxHeight: '105px'});
                        if(origin_height <= 105){
                            dom_item_root.find('.title-wrapper .view-more-wrapper').remove();
                        }
                        clone_title.remove();


                        dom_item_root.find('.title-wrapper .view-more-wrapper .view-more').click(function () {
                            dom_item_root.parent().css('z-index', '1');
                            dom_item_root.find('.title-wrapper').addClass('expanded');
                            dom_title.css({maxHeight: origin_height * 1 + 20});
                        });
                        dom_item_root.find('.title-wrapper .view-more-wrapper .view-less').click(function () {
                            dom_item_root.parent().css('z-index', '0');
                            dom_item_root.find('.title-wrapper').removeClass('expanded');
                            dom_title.css({maxHeight: '105px'});
                        });
                    },
                    this.updateInFront = updateInFront,
                    this.updateOrderInFront = updateOrderInFront,
                    this.updateSubscription = updateSubscription,
                    this.getOrder = getOrder,
                    this.getSubscriptionId = getSubscriptionId,
                    this.getPostId = getPostId,
                    this.init = function () {
                    };
                }

                var refreshRow = function () {
                    pages_size = 0;
                    pages = [];

                    allItemObjects.sort(function(a, b){
                        return parseInt(a.getOrder()) - parseInt(b.getOrder());
                    });

                    for (var i = 0; allItemObjects[i]; i ++){
                        var page_index = parseInt(i / item_count_page);

                        if (typeof pages[page_index] == 'undefined'){
                            pages[page_index] = [];
                        }
                        pages[page_index].push(allItemObjects[i]);
                        pages_size = page_index + 1;
                    }
                    loadNewSet('pagination', current_page_index);
                }
                sery.subscriptions.forEach(function (post, i) {
                    var page_index = parseInt(i / item_count_page);
                    var itemHandle = new postItemClass(post);
                    allItemObjects.push(itemHandle);
                    post.postItemHandle = itemHandle;
                    post.postItemHandle.init();
                    if (typeof pages[page_index] == 'undefined'){
                        pages[page_index] = [];
                    }
                    pages[page_index].push(post.postItemHandle);
                    pages_size = page_index + 1;
                })

                var grid = dom_content_row_body.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                function loadNewSet(action, set ) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    var newImages = pages[current_page_index];
                    if (action === 'pagination'){
                        newImages = pages[set];
                        current_page_index = set;
                    }
                    else if (action == 'clean'){
                        newImages = false;
                    }
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );

                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.getHtml();
                                el.bindDom($(items[ i ]).find('a:last-child'));
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !el.hasChildNodes() ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                            if (action == 'clean'){ set(); }
                            dom_content_row_body.find('.tt-grid').sortable({
                                cancel: '.buttons-wrapper',
                                placeholder: 'ui-state-highlight',
                                items: 'li:not(.tt-empty)',
                                coneHelperSize: true,
                                forcePlaceholderSize: true,
                                tolerance: "pointer",
                                helper: "clone",
                                revert: 300, // animation in milliseconds
                                connectWith: ":not(.tt-empty)",
                                handle: '.image-wrapper',
                                opacity: 0.9,
                                update: function (a, b) {
                                    reOrderCurrrentPageFromDom();
                                }
                            });
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                var reOrderCurrrentPageFromDom = function () {

                    grid = dom_content_row_body.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    var newOrders = [];
                    var ajaxData = [];

                    pages[current_page_index].forEach(function (col, i) {
                        var origin_order = col.getOrder();
                        dom_grid.find('li').each(function (j) {
                            var order = $(this).find('a').attr('data-order');
                            if (origin_order == order){
                                newOrders.push(pages[current_page_index][j].getOrder());
                            }
                        });
                    });

                    ajaxData = {
                        action: 'update_order_of_subscriptions',
                        data: []
                    };

                    pages[current_page_index].forEach(function (col, i) {
                        ajaxData.data.push({subscription_id: col.getSubscriptionId(), order: newOrders[i]});
                        col.updateOrderInFront(newOrders[i]);
                    });
                    pages[current_page_index].sort(function(a, b){
                        return parseInt(a.getOrder()) - parseInt(b.getOrder());
                    });
                    is_loading_controlled_in_local = true;
                    $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: ajaxData,
                        dataType: 'json',
                        success: function (res) {
                            if (!res.status){
                                alert('something is wrong');
                            }
                            is_loading_controlled_in_local = false;
                        }
                    });
                }

                var previous = function () {
                    if (current_page_index > 0 && !isAnimating){
                        loadNewSet('pagination', current_page_index - 1);
                    }
                }
                var next = function () {
                    if (current_page_index < pages_size - 1 && !isAnimating){
                        loadNewSet('pagination', current_page_index + 1);
                    }
                }

                var appearRow = function () {
                    dom_content_row.show();
                    loadNewSet('pagination', current_page_index);
                }
                var disappearRow = function () {
                    loadNewSet('clean', function(){dom_content_row.hide();});
                }

                var toggleRow = function (show) {
                    if (show){ appearRow(); }
                    else { disappearRow(); }
                }

                this.init = function () {
                    dom_next.click(function () {
                        next();
                    });
                    dom_previous.click(function () {
                        previous();
                    });
                }
                this.toggleRow = toggleRow;
            }

            data.forEach(function (sery, i) {
                var contentRowHandle = new contentRowClass(sery);
                contentRowHandle.init();
                contentRowHandles.push(contentRowHandle);
                indexOfId[sery.series_ID] = i;
            });
            var toggleContentRow = function (series_id, show) {
                var contentRowHandle = contentRowHandles[indexOfId[series_id]];
                contentRowHandle.toggleRow(show);
            }
            this.init = function () {

            }
            this.toggleContentRow = toggleContentRow;
        }
        this.init = function () {
            selectBoxHandle = new selectBoxClass();
            mainContentHandle = new mainContentClass();
            selectBoxHandle.init();
            mainContentHandle.init();
            if (initial_selected_sery){ selectBoxHandle.selectSery(initial_selected_sery); }
        }
    }

    jQuery(document).ready(function () {
        is_loading_controlled_in_local = true;
        $('.loading').css('display', 'block');
        $.ajax({
            url: ACTION_URL,
            type: 'post',
            data: {action: 'get_posts_of_all_series'},
            dataType: 'json',
            success: function (res) {

                if (!res.status){ alert('sorry, something wrong'); return false;}
                calendarPageHandle = new calendarPageClass(res.data);
                calendarPageHandle.init();
                $('.loading').css('display', 'none');
                is_loading_controlled_in_local = false;
            }
        });
        $(document).ajaxStart(function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        });
        $(document).ajaxComplete(function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        });
    })
})()