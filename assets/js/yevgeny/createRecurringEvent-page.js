(function () {
    'use strict';

    var createEventHandle;
    var is_loading_controlled_in_local = 0;

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }
    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('-');
    };

    String.prototype.stringToDate = function () {
        var yyyy = this.split('-')[0];
        var mm = this.split('-')[1] * 1 - 1;
        var dd = this.split('-')[2];
        return new Date(yyyy, mm, dd);
    }

    var CreateEventClass = function () {
        var domRoot, domStartDate, domStartTime;
        var start_date = '';
        var start_time = '';

        var self = this, dailyRecurringHandle, weeklyRecurringHandle;

        var DailyRecurringClass = function () {
            var dom_daily_recurring_root, dom_separation_count, dom_week_row;

            var separation_count;

            var dailyRecurringInstance;

            var dates_on_display = [];
            var additional_event_dates = [];
            var omit_dates = [];
            var days_of_week = '1111111';
            var event_dates_on_display = [];
            var is_month_changed = true;
            var is_days_of_week_changed = false;

            function containsObject(obj, list) {
                var x;
                for (x in list) {
                    if (list[x].getTime() === obj.getTime()) {
                        return (x * 1 + 1);
                    }
                }
                return false;
            }

            var isFilledPosWithBasic = function (date) {
                if (date.getTime() < start_date.getTime()){
                    return false;
                }

                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = start_date;
                var secondDate = date;
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

                if (diffDays % separation_count > 0){ return false; }
                if (days_of_week[date.getDay()] == '0'){ return false; }

                return true;
            };

            var isEventDate = function(date) {

                if (date.getTime() < start_date.getTime()){
                    return false;
                }
                if (containsObject(date, additional_event_dates)){ return true; }

                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = start_date;
                var secondDate = date;
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

                if (diffDays % separation_count > 0){ return false; }
                if (containsObject(date, omit_dates)){ return false; }
                if (days_of_week[date.getDay()] == '0'){ return false; }

                return true;
            }

            var refreshEventDates = function () {
                event_dates_on_display = [];
                var current_month = dates_on_display[20].getMonth();
                var i, pos;

                additional_event_dates.forEach(function (date) {
                    if (isFilledPosWithBasic(date)){
                        pos = containsObject(date, additional_event_dates);
                        additional_event_dates.splice(pos - 1, 1);
                    }
                });

                omit_dates.forEach(function (date) {
                    if (!isFilledPosWithBasic(date)){
                        pos = containsObject(date, omit_dates);
                        omit_dates.splice(pos - 1, 1);
                    }
                });


                for(i = 0; i < 42; i ++){
                    if (i > 20 && dates_on_display[i].getMonth() != current_month){ break; }
                    if(isEventDate(dates_on_display[i])){
                        event_dates_on_display.push((dates_on_display[i]));
                    }
                }
                dailyRecurringInstance.datepicker('setDates', event_dates_on_display);
            }

            var get_changed_date = function (new_dates, old_dates) {

                if (new_dates.length > old_dates.length){
                    for (var i = 0; i < new_dates.length; i ++){
                        if (!containsObject(new_dates[i], old_dates)){
                            return new_dates[i];
                        }
                    }
                }
                else {
                    for (var i = 0; i < old_dates.length; i ++){
                        if (!containsObject(old_dates[i], new_dates)){
                            return old_dates[i];
                        }
                    }
                }
                return false;
            }

            var bindEvents = function () {
                dom_separation_count.bind('keyup mouseup change', function () {
                    separation_count = dom_separation_count.val();
                    refreshEventDates();
                });
                dailyRecurringInstance.on('changeMonth', function (e) {
                    dates_on_display = [];
                    setTimeout(function () {
                        is_month_changed = true;
                        refreshEventDates();
                    }, 100);
                });
                dailyRecurringInstance.on('changeDate', function (e) {
                    if (is_month_changed || is_days_of_week_changed){
                        is_month_changed = false;
                        is_days_of_week_changed = false;
                    }
                    else {
                        var changed_date = get_changed_date(e.dates, event_dates_on_display);
                        if (!changed_date){ return ; }
                        if (changed_date.getTime() < start_date.getTime()){
                            alert('sorry, this is less than start date');
                            dailyRecurringInstance.datepicker('setDates', event_dates_on_display);
                            return ;
                        }
                        if (event_dates_on_display.length < e.dates.length){
                            var pos = containsObject(changed_date, omit_dates);
                            if (pos){
                                omit_dates.splice(pos - 1, 1);
                            }
                            else {
                                additional_event_dates.push(changed_date);
                            }
                            event_dates_on_display.push(changed_date);
                        }
                        else {
                            var pos = containsObject(changed_date, additional_event_dates);
                            if(pos){
                                additional_event_dates.splice(pos - 1, 1);
                            }
                            else {
                                omit_dates.push(changed_date);
                            }
                            pos = containsObject(changed_date, event_dates_on_display);
                            event_dates_on_display.splice(pos - 1, 1);
                        }
                    }
                });
                $('th', dom_week_row).each(function (i) {
                    $(this).click(function () {
                        $(this).toggleClass('omit');
                        if ($(this).hasClass('omit')){
                            days_of_week = days_of_week.replaceAt(i, '0');
                            if (days_of_week == '0000000'){
                                alert('Sorry, Please Select At Least One Day');
                            }
                        }
                        else {
                            days_of_week = days_of_week.replaceAt(i, '1');
                        }
                        is_days_of_week_changed = true;
                        refreshEventDates();
                    });
                });
            }

            this.show = function () {
                dom_daily_recurring_root.show();
            }
            this.hide = function () {
                dom_daily_recurring_root.hide();
            }
            this.refresh = function () {
                refreshEventDates();
            }
            this.getDailyRecurringData = function () {
                var event_instance_exceptions = [];
                additional_event_dates.forEach(function (v) {
                    event_instance_exceptions.push({
                        start_date: v.yyyymmdd(),
                        start_time: start_time.value + ':00',
                        is_rescheduled: 1,
                        is_cancelled: 0
                    });
                });
                omit_dates.forEach(function (v) {
                    event_instance_exceptions.push({
                        start_date: v.yyyymmdd(),
                        start_time: start_time.value + ':00',
                        is_rescheduled: 0,
                        is_cancelled: 1
                    });
                });
                return {
                    recurring_type: 'd',
                    days_of_week: days_of_week,
                    separation_count: separation_count,
                    event_instance_exceptions: event_instance_exceptions,
                }
            }
            this.init = function () {
                dom_daily_recurring_root = domRoot.find('.daily-recurring');
                dom_separation_count = domRoot.find('input[name="separation_cnt"]');
                dailyRecurringInstance = dom_daily_recurring_root.datepicker({
                    toggleActive: true,
                    multidate: true,
                    beforeShowDay: function (date) {
                        if (!containsObject(date, dates_on_display)){
                            dates_on_display.push(date);
                        }
                        return true;
                    }
                });
                dom_week_row = $('.datepicker-days table thead tr:last-child', dom_daily_recurring_root);
                separation_count = dom_separation_count.val();
                bindEvents();
                refreshEventDates();
            }
        }

        var WeeklyRecurringClass = function () {

            var dom_weekly_recurring_root, dom_separation_count;

            var separation_count;
            var weeklyRecurringInstance;

            var dates_on_display = [];
            var additional_event_dates = [];
            var omit_dates = [];
            var event_dates_on_display = [];
            var is_month_changed = true;

            function containsObject(obj, list) {
                var x;
                for (x in list) {
                    if (list[x].getTime() === obj.getTime()) {
                        return (x * 1 + 1);
                    }
                }
                return false;
            }

            var isFilledPosWithBasic = function (date) {
                if (date.getTime() < start_date.getTime()){
                    return false;
                }

                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = start_date;
                var secondDate = date;
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

                if (diffDays % (separation_count * 7) > 0){ return false; }

                return true;
            };

            var isEventDate = function(date) {
                if (date.getTime() < start_date.getTime()){
                    return false;
                }

                if (containsObject(date, additional_event_dates)){ return true; }
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = start_date;
                var secondDate = date;
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
                if (diffDays % (separation_count * 7) > 0){ return false; }
                if (containsObject(date, omit_dates)){ return false; }

                return true;
            }

            var refreshEventDates = function () {
                event_dates_on_display = [];
                var current_month = dates_on_display[20].getMonth();
                var i, pos;

                additional_event_dates.forEach(function (date) {
                    if (isFilledPosWithBasic(date)){
                        pos = containsObject(date, additional_event_dates);
                        additional_event_dates.splice(pos - 1, 1);
                    }
                });

                omit_dates.forEach(function (date) {
                    if (!isFilledPosWithBasic(date)){
                        pos = containsObject(date, omit_dates);
                        omit_dates.splice(pos - 1, 1);
                    }
                });


                for(i = 0; i < 42; i ++){
                    if (i > 20 && dates_on_display[i].getMonth() != current_month){ break; }

                    if(isEventDate(dates_on_display[i])){
                        event_dates_on_display.push((dates_on_display[i]));
                    }
                }
                weeklyRecurringInstance.datepicker('setDates', event_dates_on_display);
            }

            var get_changed_date = function (new_dates, old_dates) {

                if (new_dates.length > old_dates.length){
                    for (var i = 0; i < new_dates.length; i ++){
                        if (!containsObject(new_dates[i], old_dates)){
                            return new_dates[i];
                        }
                    }
                }
                else {
                    for (var i = 0; i < old_dates.length; i ++){
                        if (!containsObject(old_dates[i], new_dates)){
                            return old_dates[i];
                        }
                    }
                }
                return false;
            }

            var bindEvents = function () {
                weeklyRecurringInstance.on('changeMonth', function (e) {
                    dates_on_display = [];
                    setTimeout(function () {
                        is_month_changed = true;
                        refreshEventDates();
                    }, 100);
                });
                weeklyRecurringInstance.on('changeDate', function (e) {
                    if (is_month_changed){
                        is_month_changed = false;
                    }
                    else {

                        var changed_date = get_changed_date(e.dates, event_dates_on_display);
                        if (!changed_date){ return ; }
                        if (changed_date.getTime() < start_date.getTime()){
                            alert('sorry, this is less than start date');
                            weeklyRecurringInstance.datepicker('setDates', event_dates_on_display);
                            return ;
                        }
                        if (event_dates_on_display.length < e.dates.length){
                            var pos = containsObject(changed_date, omit_dates);
                            if (pos){
                                omit_dates.splice(pos - 1, 1);
                            }
                            else {
                                additional_event_dates.push(changed_date);
                            }
                            event_dates_on_display.push(changed_date);
                        }
                        else {
                            var pos = containsObject(changed_date, additional_event_dates);
                            if(pos){
                                additional_event_dates.splice(pos - 1, 1);
                            }
                            else {
                                omit_dates.push(changed_date);
                            }
                            pos = containsObject(changed_date, event_dates_on_display);
                            event_dates_on_display.splice(pos - 1, 1);
                        }
                    }
                });
                dom_separation_count.bind('keyup mouseup change', function () {
                    separation_count = dom_separation_count.val();
                    refreshEventDates();
                });
            }
            this.show = function () {
                dom_weekly_recurring_root.show();
            }
            this.hide = function () {
                dom_weekly_recurring_root.hide();
            }
            this.refresh = function () {
                refreshEventDates();
            }
            this.getWeeklyRecurringData = function () {
                var event_instance_exceptions = [];
                additional_event_dates.forEach(function (v) {
                    event_instance_exceptions.push({
                        start_date: v.yyyymmdd(),
                        start_time: start_time.value + ':00',
                        is_rescheduled: 1,
                        is_cancelled: 0
                    });
                });
                omit_dates.forEach(function (v) {
                    event_instance_exceptions.push({
                        start_date: v.yyyymmdd(),
                        start_time: start_time.value + ':00',
                        is_rescheduled: 0,
                        is_cancelled: 1
                    });
                });
                return {
                    recurring_type: 'w',
                    separation_count: separation_count,
                    event_instance_exceptions: event_instance_exceptions
                }
            }
            this.init = function () {
                dom_weekly_recurring_root = domRoot.find('.weekly-recurring');
                dom_separation_count = domRoot.find('input[name="separation_cnt"]');
                separation_count = dom_separation_count.val();
                weeklyRecurringInstance = dom_weekly_recurring_root.datepicker({
                    toggleActive: true,
                    multidate: true,
                    beforeShowDay: function (date) {
                        if (!containsObject(date, dates_on_display)){
                            dates_on_display.push(date);
                        }
                        return true;
                    }
                });
                bindEvents();
                refreshEventDates();
            }
        }

        var createEvent = function () {
            var ajaxData = new FormData();
            ajaxData.append('action', 'insert');
            var files = domRoot.find('.cover-image-wrapper [name="cover_img"]').prop('files');
            if (files.length){
                ajaxData.append('event_image', files[0]);
            }
            var sets = {
                event_title: domRoot.find('[name="title"]').val(),
                event_description: domRoot.find('[name="description"]').val(),
                event_location: domRoot.find('[name="location"]').val(),
                start_date: start_date.yyyymmdd(),
                start_time: start_time.value + ':00'
            }
            // var is_recurring = domRoot.find('[name="is_recurring"]').prop('checked') ? 1 : 0;
            var is_recurring = 1;
            sets.is_recurring = is_recurring;
            if (is_recurring){
                if (domRoot.find('[name="recurring_type"]').val() == 'd') {
                    sets.recurring_pattern = dailyRecurringHandle.getDailyRecurringData();
                }
                else {
                    sets.recurring_pattern = weeklyRecurringHandle.getWeeklyRecurringData();
                }
            }
            console.log(sets);
            ajaxData.append('sets', JSON.stringify(sets));

            $.ajax({
                url: API_ROOT_URL + '/Events.php',
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    console.log(res);
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Your event are saved correctly',
                        buttons: {
                            returnHome: {
                                text: "RETURN HOME",
                                value: 'return_home',
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            customize: {
                                text: "CUSTOMIZE",
                                value: 'customize',
                                visible: false,
                                className: "",
                                closeModal: true
                            }
                        },
                        closeOnClickOutside: false,
                    }).then(function (value) {
                        if (value == 'return_home'){
                            viewUserPage();
                        }
                        else {

                        }
                    });
                }
            });
        };
        var updateCoverImage = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.cover-image-wrapper .img-wrapper img', domRoot).addClass('touched');
                    $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        var bindEvents = function () {
            domStartDate.on('changeDate', function (e) {
                start_date = e.date;
                dailyRecurringHandle.refresh();
                weeklyRecurringHandle.refresh();
            });
            domStartTime.on('changeTime.timepicker', function (e) {
                start_time = e.time;
            });
            domRoot.find('[name="is_recurring"]').change(function () {
                var isRecr = $(this).prop('checked');
                if (isRecr){
                    domRoot.find('.recurring-type-wrapper').show();
                    domRoot.find('.once-every-wrapper').show();
                    domRoot.find('.schedule-section').show();
                }
                else {
                    domRoot.find('.recurring-type-wrapper').hide();
                    domRoot.find('.once-every-wrapper').hide();
                    domRoot.find('.schedule-section').hide();
                }
            });
            domRoot.find('[name="recurring_type"]').change(function () {
                var v = $(this).val();
                if (v == 'd'){
                    dailyRecurringHandle.show();
                    weeklyRecurringHandle.hide();
                    domRoot.find('.once-every-wrapper .d-type').html('day');
                }
                else {
                    dailyRecurringHandle.hide();
                    weeklyRecurringHandle.show();
                    domRoot.find('.once-every-wrapper .d-type').html('week');
                }
            });
            domRoot.find('.cover-image-wrapper [name="cover_img"]').change(function () {
                updateCoverImage(this);
            });
            domRoot.find('input[name="separation_cnt"]').change(function () {
                if (parseInt($(this).val()) > 1){
                    domRoot.find('.once-every-wrapper .multi-flg').show();
                }
                else {
                    domRoot.find('.once-every-wrapper .multi-flg').hide();
                }
            })
            domRoot.submit(function () {
                createEvent();
                return false;
            });
        }

        var viewUserPage = function () {
            $('<form action="userpage" method="post" hidden></form>').appendTo('body').submit().remove();
        }
        this.init = function () {
            var now = new Date();
            domRoot = $('form#event-form');
            domStartDate = domRoot.find('input[name="start_date"]');
            domStartTime = domRoot.find('input[name="start_time"]');
            domStartDate.datepicker();
            domStartTime.timepicker({
                defaultTime: now.getHours() + ':' + now.getMinutes(),
                showMeridian: false
            });
            dailyRecurringHandle = new DailyRecurringClass();
            weeklyRecurringHandle = new WeeklyRecurringClass();
            start_date = now;
            dailyRecurringHandle.init();
            weeklyRecurringHandle.init();
            weeklyRecurringHandle.hide();
            bindEvents();
            domStartDate.datepicker('setDate', new Date(now.getYear() + 1900, now.getMonth(), now.getDate()));
            domStartTime.timepicker('setTime', new Date().getHours() + ':' + new Date().getMinutes());
        }
    }

    createEventHandle = new CreateEventClass();
    createEventHandle.init();
    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });
})();