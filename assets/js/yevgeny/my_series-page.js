(function () {
    'use strict';

    var PageClass = function () {
        var domPage;

        var mainHandle, filterHandle, postEditorHandle;

        var filterCategory = -1, filterSeriesId = -1, sortBy = -1;

        var FilterClass = function () {

            var domFilter, domSortByDropDown, domSeriesFilter, domCategoryDropDown;

            var bindEvents = function () {
                domFilter.find('.categories-container .filter-category').click(function () {
                    filterCategory = $(this).data('category');
                    domFilter.find('.filter-category').removeClass('active');
                    $(this).addClass('active');
                    filterSeries();
                    filterSeriesId = -1;
                    mainHandle.refreshSeries();
                });

                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    sortBy = v;
                    //    handle actions here
                    mainHandle.refreshPosts();
                });
                domCategoryDropDown.find('ul li a').click(function () {
                    domCategoryDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('category');
                    var txt = $(this).text();
                    domCategoryDropDown.data('value', v);
                    domCategoryDropDown.find('button span').text(txt);
                    filterCategory = v;
                    filterSeries();
                    filterSeriesId = -1;
                    mainHandle.refreshSeries();
                });
                domSeriesFilter.find('.filter-item').click(function () {
                    filterSeriesId = $(this).data('id');
                    domSeriesFilter.find('.filter-item').removeClass('active');
                    $(this).addClass('active');
                    mainHandle.refreshSeries();
                });
            }
            var filterSeries = function () {
                domSeriesFilter.css('min-height', domSeriesFilter.css('height'));
                domSeriesFilter.css('opacity', 0);
                setTimeout(function () {
                    domSeriesFilter.find('.filter-item').each(function (i) {
                        $(this).removeClass('active');
                        var id = $(this).data('id');
                        if (id == -1) {
                            $(this).parent().show();
                            $(this).addClass('active');
                        }
                        else {
                            var ss = series.filter(function (s) {
                                return parseInt(s.series_ID) === parseInt(id);
                            });
                            var cid = ss[0]['intSeries_category'];
                            if (filterCategory == -1 || filterCategory == cid) {
                                $(this).parent().show();
                            }
                            else {
                                $(this).parent().hide();
                            }
                        }
                    });
                    domSeriesFilter.css('min-height', '0px');
                    domSeriesFilter.css('opacity', 1);
                }, 300);
            }
            this.init = function () {
                domFilter = domPage.find('.filter-wrapper');
                domSortByDropDown = domFilter.find('.sort-by .dropdown');
                domCategoryDropDown = domFilter.find('.categories-container-mobile .dropdown');
                domSeriesFilter = domFilter.find('.series-filter-wrapper');
                bindEvents();
            }
        }

        var MainClass = function () {
            var domMain, domSeriesContainer;

            var seriesItemHandles = [], filteredHandles = [], currentIndex = 0, isLoadingMore = true, noMore = false;
            var seriesFlgs = [];

            var SeriesItemClass = function (seriesItemData) {

                var domSeriesItem, domSeriesHeader, domPostsList, domSeriesIncomeWrapper;

                var itemHandles = [], filteredHandles = [], copyEmbedHandle, chargeModalHandle, seriesConversationHandle;

                var ChargeModalClass = function () {
                    var domModal;

                    var seriesData;

                    var bindEvents = function () {
                        domModal.find('input').change(function () {
                            var type = $(this).attr('type');
                            var v;
                            if (type === 'checkbox') {
                                v = $(this).prop('checked') ? 1 : 0;
                            }
                            else {
                                v = $(this).val();
                            }
                            seriesData[$(this).attr('name')] = v;
                            bindData();
                        });
                        domModal.find('.save-btn').click(function () {
                            save().then(function (res) {
                                domModal.modal('hide');
                            });
                        });
                        domModal.on('hidden.bs.modal', function (e) {
                        });
                    }

                    var bindData = function () {

                        if (seriesData.boolSeries_charge) {
                            domModal.find('[name="boolSeries_charge"]').prop('checked', true);
                            domModal.find('[name="intSeries_price"]').removeAttr('disabled');
                            domModal.find('[name="intSeries_price"]').val(seriesData.intSeries_price);

                            if (seriesData.boolSeries_affiliated) {
                                domModal.find('[name="boolSeries_affiliated"]').prop('checked', true);
                                domModal.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('enable');
                                domModal.find('[name="intSeries_affiliate_percent"]').val(seriesData.intSeries_affiliate_percent);

                                var ownerMoney = seriesData.intSeries_price * (100 - parseInt(seriesData.intSeries_affiliate_percent)) / 100;
                                ownerMoney = parseInt(ownerMoney);
                                domModal.find('.you-will-get-value').html(ownerMoney + '$');

                                var affiliateMoney = seriesData.intSeries_price - ownerMoney;
                                affiliateMoney = parseInt(affiliateMoney);
                                domModal.find('.affiliate-will-get-value').html(affiliateMoney + '$');
                            }
                            else {
                                domModal.find('[name="boolSeries_affiliated"]').prop('checked', false);
                                domModal.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                                domModal.find('[name="intSeries_affiliate_percent"]').val('');
                                domModal.find('.you-will-get-value').html(seriesData.intSeries_price + '$');
                                domModal.find('.affiliate-will-get-value').html('NO Affiliated');
                            }
                        }
                        else {
                            domModal.find('[name="boolSeries_charge"]').prop('checked', false);
                            domModal.find('[name="intSeries_price"]').attr('disabled', true);
                            domModal.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                            domModal.find('[name="boolSeries_affiliated"]').prop('checked', false);
                            domModal.find('[name="intSeries_affiliate_percent"]').val('');
                            domModal.find('[name="intSeries_price"]').val('');
                            domModal.find('.you-will-get-value').html('No Charged');
                            domModal.find('.affiliate-will-get-value').html('NO Affiliated');
                        }
                    }
                    var save = function () {
                        var sets = {
                            boolSeries_charge: seriesData.boolSeries_charge,
                            boolSeries_affiliated: seriesData.boolSeries_affiliated,
                        };
                        if (seriesData.intSeries_price) {
                            sets.intSeries_price = seriesData.intSeries_price;
                        }
                        if (seriesData.intSeries_affiliate_percent) {
                            sets.intSeries_affiliate_percent = seriesData.intSeries_affiliate_percent;
                        }
                        return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: seriesData.series_ID, sets: sets}).then(function (res) {
                            Object.assign(seriesItemData, sets);
                            domSeriesIncomeWrapper.find('.sign-up-value').html(seriesItemData.intSeries_price ? seriesItemData.intSeries_price : '0');
                            if (parseInt(seriesItemData.boolSeries_charge)) {
                                domSeriesItem.addClass('series-charged');
                            }
                            else {
                                domSeriesIncomeWrapper.find('.total-earned-value').html('0');
                            }
                            return res;
                        });
                    }
                    this.dom = function () {
                        return domModal;
                    }
                    this.init = function () {
                        seriesData = Object.assign({}, seriesItemData);
                        delete seriesData.posts;
                        seriesData.boolSeries_charge = parseInt(seriesData.boolSeries_charge);
                        seriesData.boolSeries_affiliated = parseInt(seriesData.boolSeries_affiliated);
                        domModal = domSeriesItem.find('.modal.charge-modal');
                        domModal.find('[name="intSeries_affiliate_percent"]').bootstrapSlider ({
                            handle: 'square',
                            min: 0,
                            max: 100,
                            value: parseInt(seriesItemData.intSeries_affiliate_percent),
                        });
                        bindData();
                        bindEvents();
                    }
                }

                var PostItemClass = function (itemData) {
                    var domItem, domBody, domTypeBody;
                    var dataLoaded = false;
                    var deleteItem = function () {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: itemData.post_ID}).then(function (res) {
                            if (res.status) {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().post_ID === itemData.post_ID) {
                                        itemHandles.splice(i, 1);
                                        domItem.remove();
                                    }
                                });
                            }
                        });
                    }
                    var bindData = function () {
                        dataLoaded = true;
                        domItem.addClass('type-' + itemData.intPost_type);
                        domItem.find('.day-value').html(itemData.viewDay);
                        switch (parseInt(itemData.intPost_type)) {
                            case 0:
                                domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                break;
                            case 2:
                                domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                domTypeBody.find('img').attr('src', itemData.strPost_featuredimage);
                                break;
                            default:
                                domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                domTypeBody.html(itemData.strPost_body);
                                break;
                        }
                        domItem.find('.item-title').html(itemData.strPost_title).attr('href', 'view_blog?id=' + itemData.post_ID);
                        if (itemData.strPost_duration) {
                            domBody.find('.blog-duration').html(itemData.strPost_duration);
                        }
                        else {
                            helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                                domBody.find('.blog-duration').html(res);
                                itemData.strPost_duration = res;
                                ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                            });
                        }
                    }
                    var empty = function () {
                        dataLoaded = false;
                        domItem.removeClass('type-0').removeClass('type-2').removeClass('type-7').removeClass('type-8').removeClass('type-9').removeClass('type-10');
                        domTypeBody.remove();
                    }
                    var bindEvents = function () {
                        domItem.find('.item-action.delete-action').click(function (e) {
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this post?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteItem();
                                }
                            });
                        });
                        domItem.find('.item-action.edit-action').click(function () {
                            postEditorHandle.setData(itemData);
                            postEditorHandle.open();
                            postEditorHandle.afterSave(function (sets) {
                                itemData = Object.assign(itemData, sets);
                                empty();
                                bindData();
                            });
                        });
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.append = function () {
                        domItem.appendTo(domPostsList);
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.bindData = bindData;
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.init = function () {
                        domItem = domSeriesItem.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        domBody = domItem.find('.item-type-body');
                        bindEvents();
                    }
                }

                var invite = function (emails) {
                    var ajaxData = {
                        action: 'invite',
                        id: seriesItemData.series_ID,
                        emails: emails,
                    };
                    ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Your invitation are sent successfully',
                        }).then(function (value) {
                            if (value == 'return_home'){
                            }
                            else {
                            }
                        });
                    });
                }

                var bindData = function () {
                    domSeriesHeader.find('.series-title').html(seriesItemData.strSeries_title);
                    domSeriesHeader.find('.series-viewed-times').html(parseInt(seriesItemData.totalViews) > 1 ? '(Viewed '+ seriesItemData.totalViews +' times)' : '(Viewed 1 time)');;
                    domSeriesItem.find('.series-item-mobile-header .series-title').html(seriesItemData.strSeries_title);
                    domSeriesItem.find('.series-item-mobile-header .series-viewed-times').html(parseInt(seriesItemData.totalViews) > 1 ? '(Viewed '+ seriesItemData.totalViews +' times)' : '(Viewed 1 time)');
                    domSeriesHeader.find('.action-item.edit-action').attr('href', 'edit_series?id=' + seriesItemData.series_ID);
                    domSeriesHeader.find('.action-item.edit-structure-action').attr('href', 'edit_series?id=' + seriesItemData.series_ID + '&tab=structure');
                    domSeriesItem.find('.add-post-wrapper').attr('href', 'add_posts?id='+ seriesItemData.series_ID);
                    domSeriesItem.find('.full-list-wrapper .full-list-action').attr('href', 'preview_series?id='+ seriesItemData.series_ID);
                    domSeriesIncomeWrapper.find('.sign-up-value').html(seriesItemData.intSeries_price ? seriesItemData.intSeries_price : '0');
                    domSeriesIncomeWrapper.find('.people-join-value').html(seriesItemData.joinCnt);
                    if (parseInt(seriesItemData.boolSeries_charge)) {
                        domSeriesIncomeWrapper.find('.total-earned-value').html(seriesItemData.joinCnt * seriesItemData.intSeries_price);
                        domSeriesItem.addClass('series-charged');
                    }
                    else {
                        domSeriesIncomeWrapper.find('.total-earned-value').html('0');
                    }
                }

                var bindEvents = function () {
                    domSeriesIncomeWrapper.find('[name="boolSeries_charge"]').change(function () {
                        if ($(this).prop('checked')) {
                            chargeModalHandle.dom().modal('show');
                            domSeriesIncomeWrapper.find('[name="boolSeries_charge"]').prop('checked', false).change();
                        }
                    });
                    domSeriesItem.find('.invite-friend-form').submit(function () {
                        var v = $(this).find('[name="emails"]').val();
                        var emails = v.split(/\s*,+\s*/);
                        invite(emails);
                        return false;
                    });
                    domPostsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 3;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }
                this.data = function () {
                    return seriesItemData;
                }
                this.append = function () {
                    domSeriesItem.appendTo(domSeriesContainer);
                }
                this.detach = function () {
                    domSeriesItem.detach();
                }
                this.refreshPosts = function () {
                    domPostsList.css('height', domPostsList.css('height'));
                    domPostsList.css('opacity', 0);
                    setTimeout(function () {
                        domPostsList.slick('unslick');
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        switch (sortBy) {
                            case 'audio':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().intPost_type == 0;
                                });
                                break;
                            case 'video':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().intPost_type == 2;
                                });
                                break;
                            case 'text':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().intPost_type == 7 || value.data().intPost_type == 8 || value.data().intPost_type == 10;
                                });
                                break;
                            case 'A-z':
                                filteredHandles = itemHandles;
                                filteredHandles.sort(function (a, b) {
                                    if (a.data().strPost_title < b.data().strPost_title) {
                                        return 1;
                                    }
                                    else if (a.data().strPost_title == b.data().strPost_title) {
                                        return 0;
                                    }
                                    return -1;
                                });
                                break;
                            case 'Z-a':
                                filteredHandles = itemHandles;
                                filteredHandles.sort(function (a, b) {
                                    if (a.data().strPost_title > b.data().strPost_title) {
                                        return 1;
                                    }
                                    else if (a.data().strPost_title == b.data().strPost_title) {
                                        return 0;
                                    }
                                    return -1;
                                });
                                break;
                            default:
                                filteredHandles = itemHandles;
                                break;
                        }
                        filteredHandles.forEach(function (value) {
                            value.append();
                        });
                        domPostsList.css('height', 'auto');
                        domPostsList.slick({
                            dots: false,
                            infinite: false,
                            arrows: true,
                            slidesToShow: 3,
                            slidesToScroll: 2,
                            nextArrow: domSeriesItem.find('.arrow-right'),
                            prevArrow: domSeriesItem.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                    }
                                },
                                {
                                    breakpoint: 960,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                    }
                                },
                            ]
                        });
                        domPostsList.css('opacity', 1);
                    }, 300);
                }
                this.refreshSlick = function () {
                    domPostsList.slick('setPosition');
                }
                this.init = function () {
                    domSeriesItem = domSeriesContainer.find('.series-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSeriesContainer);
                    domSeriesHeader = domSeriesItem.find('.series-content-header');
                    domPostsList = domSeriesItem.find('.posts-list');
                    domSeriesIncomeWrapper = domSeriesItem.find('.series-income-wrapper');
                    copyEmbedHandle = new SeriesCopyEmbedClass(domSeriesHeader.find('.component.series-copy-embed'), seriesItemData);
                    copyEmbedHandle.init();
                    bindData();
                    bindEvents();
                    seriesItemData.posts.forEach(function (itemData, i) {
                        var hdl = new PostItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        if (i < 7) {
                            hdl.bindData();
                        }
                        itemHandles.push(hdl);
                        filteredHandles.push(hdl);
                    });
                    domPostsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        nextArrow: domSeriesItem.find('.arrow-right'),
                        prevArrow: domSeriesItem.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                }
                            },
                            {
                                breakpoint: 960,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            },
                        ]
                    });
                    chargeModalHandle = new ChargeModalClass();
                    chargeModalHandle.init();
                    seriesConversationHandle = new SeriesConversationClass(domSeriesItem.find('.series-conversation-component'), seriesItemData.series_ID, {css: {height: '450px'}});
                    seriesConversationHandle.init();
                }
            }

            var loadMore = function () {
                isLoadingMore = true;
                domMain.find('.load-more-status').show();
                setTimeout(function () {
                    var to = 5;

                    for (var i = 0; i < series.length && to > 0; i ++) {
                        var sery = series[i];
                        if (seriesFlgs[sery.series_ID]) {
                            continue;
                        }
                        if (filterSeriesId == -1) {
                            if (filterCategory != -1 && filterCategory != sery.intSeries_category) {
                                continue;
                            }
                        }
                        else {
                            if (filterSeriesId != sery.series_ID) {
                                continue;
                            }
                        }
                        seriesFlgs[sery.series_ID] = true;
                        var hdl = new SeriesItemClass(sery);
                        hdl.init();
                        seriesItemHandles.push(hdl);
                        filteredHandles.push(hdl);
                        to--;
                    }
                    if (to === 5) {
                        noMore = true;
                    }
                    domMain.find('.load-more-status').hide();
                    isLoadingMore = false;
                }, 300);
            }

            this.refreshSeries = function () {
                noMore = false;
                domMain.css('height', domMain.css('height'));
                domMain.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (value) {
                        value.detach();
                    });
                    filteredHandles = seriesItemHandles.filter(function (value) {
                        if (filterSeriesId == -1) {
                            if (filterCategory == -1) {
                                return true;
                            }
                            if (value.data().intSeries_category == filterCategory) {
                                return true;
                            }
                        }
                        else {
                            return filterSeriesId == value.data().series_ID;
                        }
                        return false;
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                        value.refreshSlick();
                    });
                    domMain.css('height', 'auto');
                    domMain.css('opacity', 1);
                    loadMore();
                }, 300);
            }

            this.refreshPosts = function () {
                seriesItemHandles.forEach(function (value) {
                    value.refreshPosts();
                });
            }

            var bindEvents = function () {
                $(window).scroll(function (e) {
                    if ($('.site-wrapper').hasClass('site-search-opened')) {
                        return true;
                    }
                    var oh = $('.site-wrapper').outerHeight();
                    var sh = window.scrollY;
                    if (sh + window.innerHeight > oh - $('.site-wrapper .home-footer').outerHeight() + 100) {
                        if (!isLoadingMore && !noMore) {
                            loadMore();
                        }
                    }
                });
            }

            this.init = function () {
                domMain = domPage.find('main.main-content');
                domSeriesContainer = domMain.find('.series-container');
                for (var i = 0; i < 5 && i < series.length; i ++) {
                    var sery = series[i];
                    var hdl = new SeriesItemClass(sery);
                    hdl.init();
                    seriesItemHandles.push(hdl);
                    filteredHandles.push(hdl);
                    currentIndex++;
                    seriesFlgs[sery.series_ID] = true;
                }
                bindEvents();
                isLoadingMore = false;
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            mainHandle = new MainClass();
            mainHandle.init();

            filterHandle = new FilterClass();
            filterHandle.init();

            postEditorHandle = new PostEditorClass(domPage.find('.post-editor.component'));
            postEditorHandle.init();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();