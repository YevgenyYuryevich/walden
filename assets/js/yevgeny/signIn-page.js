(function () {
    'use strict';
    var PageClass = function () {

        var loginHandle, signupHandle, forgotPasswordHandle, resetPasswordHandle, sendEmailHandle;

        function validate(input) {
            if ($(input).parent().hasClass('alert-validate')) {
                return false;
            }
            if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
                if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                    return false;
                }
            }
            else {
                if($(input).val().trim() == ''){
                    return false;
                }
            }
        }

        function showValidate(input) {
            var thisAlert = $(input).parent();

            $(thisAlert).addClass('alert-validate');
        }

        function hideValidate(input) {
            var thisAlert = $(input).parent();

            $(thisAlert).removeClass('alert-validate');
        }

        var globalFuns = function () {
            $('.input100').each(function(){
                $(this).on('blur', function(){
                    if($(this).val().trim() != "") {
                        $(this).addClass('has-val');
                    }
                    else {
                        $(this).removeClass('has-val');
                    }
                })
            });
            $('.validate-form .input100').each(function(){
                $(this).focus(function(){
                    hideValidate(this);
                });
            });
        }

        var LoginClass = function () {

            var self = this;

            var domRoot, domAlert;

            var bindEvents = function () {
                domRoot.on('submit',function(){
                    var input = domRoot.find('.validate-input .input100');
                    var check = true;
                    for(var i=0; i<input.length; i++) {
                        if(validate(input[i]) == false){
                            showValidate(input[i]);
                            check=false;
                        }
                    }
                    if (!check) { return false; }
                    self.login(domRoot.find('[name="username"]').val(), domRoot.find('[name="password"]').val()).then(function (res) {
                        if (res) {
                            window.location.href = prevPage;
                        }
                        else {
                            domAlert.css('opacity', 1);
                        }
                    });
                    return false;
                });
                domRoot.find('input').keyup(function () {
                    domAlert.css('opacity', 0);
                });
            }

            this.login = function (username, password) {
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, {'function': 'signin', username: username, password: password}).then(function (res) {
                    if (res.status) {
                        return true;
                    }
                    else {
                        return false;
                    }
                });
            }

            this.init = function () {
                domRoot = $('#signin-form');
                domAlert = domRoot.find('.alert.alert-danger');
                bindEvents();
            }
        }

        var SignUpClass = function () {
            var self = this;
            var domRoot, domAlert, domUsername, domEmail;

            var bindEvents = function () {
                domRoot.submit(function(){
                    var input = domRoot.find('.validate-input .input100');
                    var check = true;
                    for(var i=0; i<input.length; i++) {
                        if(validate(input[i]) == false){
                            showValidate(input[i]);
                            check=false;
                        }
                    }
                    if (!check) { return false; }
                    var sets = helperHandle.formSerialize(domRoot);
                    delete sets.re_password;
                    self.signUp(sets).then(function (res) {
                        if (res) {
                            window.location.href = 'simplify?first_visit=1';
                        }
                        else {
                            domAlert.css('opacity', 1);
                        }
                    });
                    return false;
                });
                domUsername.blur(function () {
                    if (!domRoot.hasClass('active')) {
                        return ;
                    }
                    if ($(this).val() == '') { return true; }
                    checkUsername($(this).val()).then(function (res) {
                        if (!res) {
                            domUsername.parent().attr('data-validate', domUsername.parent().attr('data-exist'));
                            domUsername.parent().addClass('alert-validate');
                        }
                    });
                });
                domEmail.blur(function () {
                    if (!domRoot.hasClass('active')) {
                        return ;
                    }
                    if ($(this).val() == '') { return true; }
                    checkEmail($(this).val()).then(function (res) {
                        if (!res) {
                            domEmail.parent().attr('data-validate', domEmail.parent().attr('data-exist'));
                            domEmail.parent().addClass('alert-validate');
                        }
                    });
                });
                domEmail.focus(function () {
                    domEmail.parent().attr('data-validate', domRoot.find('username').parent().attr('data-require'));
                });
                domUsername.focus(function () {
                    domUsername.parent().attr('data-validate', domRoot.find('username').parent().attr('data-require'));
                });
                domRoot.find('[name="re_password"]').change(function () {
                    if (domRoot.find('[name="password"]').val() !== domRoot.find('[name="re_password"]').val()) {
                        domRoot.find('[name="re_password"]').parent().addClass('alert-validate');
                    }
                });
            }
            var checkUsername = function (username) {
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, {check: 'username', value: username}).then(function (res) {
                    return res.status;
                })
            }
            var checkEmail = function (email) {
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, {check: 'email', value: email}).then(function (res) {
                    return res.status;
                })
            }
            this.signUp = function (sets) {
                sets['function'] = 'register';
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, sets).then(function (res) {
                    return res.status;
                });
            }
            this.init = function () {
                domRoot = $('#signup-form');
                domAlert = domRoot.find('.alert');
                domUsername = domRoot.find('[name="username"]');
                domEmail = domRoot.find('[name="email"]');
                bindEvents();
            }
        }

        var ForgotPasswordClass = function () {
            var domRoot;
            var bindEvents = function () {
                domRoot.submit(function () {
                    var input = domRoot.find('.validate-input .input100');
                    var check = true;
                    for(var i=0; i<input.length; i++) {
                        if(validate(input[i]) == false){
                            showValidate(input[i]);
                            check=false;
                        }
                    }
                    if (!check) { return false; }
                    sendEmail(domRoot.find('[name="username"]').val()).then(function (res) {
                        if (res) {
                            swal("SUCCESS!", 'We will send message to your email soon', "success", {
                                button: "OK"
                            });
                        }
                    });
                    return false;
                });
            }
            var sendEmail = function (email) {
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, {'function': 'reset_send', username: email}).then(function (res) {
                    return res.status;
                });
            }
            this.init = function () {
                domRoot = $('#forgot-password-form');
                bindEvents();
            }
        }

        var ResetPasswordClass = function () {
            var domRoot, domAlert;

            var bindEvents = function () {
                domRoot.find('[name="re_password"]').change(function () {
                    if (domRoot.find('[name="password"]').val() !== domRoot.find('[name="re_password"]').val()) {
                        domRoot.find('[name="re_password"]').parent().addClass('alert-validate');
                    }
                });
                domRoot.submit(function () {
                    var input = domRoot.find('.validate-input .input100');
                    var check = true;
                    for(var i=0; i<input.length; i++) {
                        if(validate(input[i]) == false){
                            showValidate(input[i]);
                            check=false;
                        }
                    }
                    if (!check) { return false; }
                    var sets = helperHandle.formSerialize(domRoot);
                    delete sets.re_password;
                    restPassword(sets).then(function (res) {
                        if (res) {
                            window.location.href = prevPage;
                        }
                        else {
                            domAlert.css('opacity', 1);
                        }
                    });
                    return false;
                })
            }

            var restPassword = function(sets) {
                sets['function'] = 'reset_pw';
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, sets).then(function (res) {
                    return res.status;
                });
            }

            this.init = function () {
                domRoot = $('#rest-password-form');
                domAlert = domRoot.find('.alert');
                bindEvents();
            }
        }

        var SendEmailClass = function () {
            var domRoot;
            var bindEvents = function () {
                domRoot.submit(function () {
                    var input = domRoot.find('.validate-input .input100');
                    var check = true;
                    for(var i=0; i<input.length; i++) {
                        if(validate(input[i]) == false){
                            showValidate(input[i]);
                            check=false;
                        }
                    }
                    if (!check) { return false; }
                    sendEmail(domRoot.find('[name="email"]').val()).then(function (res) {
                        if (res) {
                            swal("SUCCESS!", 'We will send invitation to your email soon', "success", {
                                button: "OK"
                            });
                        }
                        else {
                            domRoot.find('.alert').css('opacity', 1);
                        }
                    });
                    return false;
                });
            }
            var sendEmail = function (email) {
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, {'function': 'send_email', email: email}).then(function (res) {
                    return res.status;
                });
            }
            this.init = function () {
                domRoot = $('#send-email-form');
                bindEvents();
            }
        }

        this.init = function () {
            globalFuns();
            loginHandle = new LoginClass();
            loginHandle.init();
            signupHandle = new SignUpClass();
            signupHandle.init();
            forgotPasswordHandle = new ForgotPasswordClass();
            forgotPasswordHandle.init();
            resetPasswordHandle = new ResetPasswordClass();
            resetPasswordHandle.init();
            sendEmailHandle = new SendEmailClass();
            sendEmailHandle.init();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();