(function () {
    'use strict';

    var PageClass = function () {

        var viewHandle = null, authHandle = null

        var is_loading_controlled_in_local = false;

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            paths: 'paths',
            free: 'boolPost_free'
        };

        var subscriptionFiels = {
            id: 'clientsubscription_ID',
            title: 'strClientSubscription_title',
            body: 'strClientSubscription_body',
            image: 'strClientSubscription_image',
            type: 'intClientSubscriptions_type',
            order: 'intClientSubscriptions_order',
            nodeType: 'strClientSubscription_nodeType',
            parent: 'intClientSubscriptions_parent',
            paths: 'paths',
            completed: 'boolCompleted',
        };

        var viewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                if (from === 'post') {
                    fk = helperHandle.keyOfVal(postFields, k);
                    if (fk) { fData[fk] = data[k]; }
                }
                else {
                    fk = helperHandle.keyOfVal(subscriptionFiels, k);
                    if (fk) { fData[fk] = data[k]; }
                }
            }
            fData.origin = data;
            return fData;
        }
        var postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }

        var ViewClass = function () {

            var domRoot;
            var domUsedBody, domBody, domFooter, domVideo;
            var videoHandle;

            var seekPost;
            var audioCnt = 0, metaData = {};
            var availableTypes = [0, 2, 8, 10];
            var postData, prevPostData;
            var isWelcome = false;

            var self = this, payBlogHandle = null;

            var FormFieldClass = function (domField) {
                var domValue;
                var field, answer = false;

                var bindEvents = function () {
                    switch (field.strFormField_type) {
                        case 'text':
                            domValue.blur(function () {
                                updateAnswer();
                            });
                            break;
                        case 'checkbox':
                            domValue.find('input').change(function () {
                                updateAnswer();
                            });
                            break;
                    }
                }
                var updateAnswer = function () {
                    if (CLIENT_ID == -1) {
                        return false;
                    }
                    var sets = {};
                    is_loading_controlled_in_local = true;
                    domField.addClass('status-loading');
                    switch (field.strFormField_type) {
                        case 'text':
                            sets.strFormFieldAnswer_answer = domValue.html();
                            break;
                        case 'checkbox':
                            sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                            break;
                    }
                    if (answer) {
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                            answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                        });
                    }
                    else {
                        $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                        });
                    }
                    is_loading_controlled_in_local = false;
                }
                var bindData = function () {
                    formFields.forEach(function (formField) {
                        if (formField.formField_ID == domField.attr('data-form-field')) {
                            field = formField;
                        }
                    });
                    formFieldAnswers.forEach(function (formFieldAnswer) {
                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                            answer = formFieldAnswer;
                        }
                    });
                    if (answer) {
                        switch (field.strFormField_type) {
                            case 'text':
                                domValue.html(answer.strFormFieldAnswer_answer);
                                break;
                            case 'checkbox':
                                domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                                break;
                        }
                    }
                }
                this.init = function () {
                    domValue = domField.find('.form-field-value');
                    domField.find('[data-toggle="popover"]').popover();
                    bindData();
                    bindEvents();
                }
            }

            var PathClass = function (path) {
                var domPath, pathNumber;

                var bindEvents = function () {
                    domPath.click(function () {
                        select();
                    });
                }

                var select = function () {
                    var resPromise;
                    var ajaxData = {};

                    if (from === 'post') {
                        if (CLIENT_ID !== -1) {
                            ajaxData.action = 'set_seek';
                            ajaxData.where = clientSeriesView.clientSeriesView_ID;
                            ajaxData.sets = {
                                intCSView_seekParent: path.id,
                            };
                        }
                        else {
                            ajaxData.action = 'select_path';
                            ajaxData.id = path.id;
                        }
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                    }
                    else {
                        ajaxData.action = 'set_subPath';
                        ajaxData.pathId = path.id;
                        ajaxData.purchasedId = purchased.purchased_ID;
                        resPromise = ajaxAPiHandle.pagePost('view', ajaxData);

                    }
                    resPromise.then(function (res) {
                        if (res.status === true && res.data){
                            self.bindData(viewFormat(res.data));
                        }
                        else {
                            swal("Sorry!", "There is no post in this option that you clicked. You will go to next post.").then(function () {
                                if (CLIENT_ID !== -1) {
                                    seekNext();
                                }
                                else {
                                    nextPost();
                                }
                            });
                        }
                    });
                }

                this.init = function () {

                    domPath = domUsedBody.find('.path-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUsedBody.find('.paths-list'));

                    pathNumber = domUsedBody.find('.paths-list > .path-col').length;

                    domPath.find('.img-wrapper img').attr('src', 'assets/images/global-icons/tree/path/'+ pathNumber +'.svg');


                    var displayTitle = path.title;
                    displayTitle = displayTitle.length > 20 ? displayTitle.substr(0, 50) + '...' : displayTitle.substr(0, 50);

                    var displayDes = path.body;
                    displayDes = displayDes.length > 20 ? displayDes.substr(0, 100) + '...' : displayDes.substr(0, 100);

                    domPath.find('.path-title').html(displayTitle);
                    domPath.find('.path-description').html(displayDes);
                    bindEvents();
                }
            }
            var nextPost = function () {

                var resPromise;
                var ajaxData;

                if (from === 'post') {
                    ajaxData = {
                        action: 'next_post',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                }
                else {
                    ajaxData = {
                        action: 'next_sub',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.pagePost('view', ajaxData);
                }

                return resPromise.then(function (res) {
                    if (res.status){
                        self.bindData(viewFormat(res.data));
                    }
                });
            }
            var prevPost = function () {
                var resPromise;
                var ajaxData;

                if (from === 'post') {
                    ajaxData = {
                        action: 'prev_post',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                }
                else {
                    ajaxData = {
                        action: 'prev_sub',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.pagePost('view', ajaxData);
                }

                return resPromise.then(function (res) {
                    if (res.status){
                        self.bindData(viewFormat(res.data));
                    }
                });
            }

            var seekNext = function () {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_next', CSViewId: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                    self.bindData(viewFormat(res.data));
                });
            }

            var seekPrev = function () {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_prev', CSViewId: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                    self.bindData(viewFormat(res.data));
                });
            }

            var showPrevPost = function () {
                isWelcome = false;
                self.bindData(prevPostData);
            }

            var makeBody = function () {
                var type = parseInt(postData.type);
                type = availableTypes.indexOf(type) !== -1 ? type : 'other';
                var newDomUsedBody;
                domBody.find('> .blog-type:not(.sample)').remove();
                if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                    newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    payBlogHandle = new PayBlogClass(domUsedBody, series);
                    payBlogHandle.onPaid(function (purchasedId) {
                        purchased = purchasedId;
                        makeBody();
                    });
                    payBlogHandle.init();
                }
                else if (postData.nodeType === 'menu') {
                    newDomUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    domUsedBody = newDomUsedBody;
                    // domUsedBody.find('.menu-title').html(postData.title);
                    domUsedBody.find('.menu-description').html(postData.body);

                    postData.paths.forEach(function (path) {
                        var pathHandle = new PathClass(viewFormat(path));
                        pathHandle.init();
                    });
                    domUsedBody.find('.paths-list').addClass('paths-count-' + postData.paths.length);
                }
                else {
                    newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    switch (parseInt(type)){
                        case 0:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                            domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                            domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                            var settings_ap = {
                                disable_volume: 'off',
                                disable_scrub: 'default',
                                design_skin: 'skin-wave',
                                skinwave_dynamicwaves: 'off'
                            };
                            dzsag_init('#audio-' + audioCnt, {
                                'transition':'fade',
                                'autoplay' : 'off',
                                'settings_ap': settings_ap
                            });
                            audioCnt++;
                            break;
                        case 2:
                            // domBody.find('> .blog-type:not(.sample)').remove();
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domVideo = domUsedBody.find('video');
                            if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                                var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                domVideo.attr('data-setup', JSON.stringify(setup));
                            }
                            else {
                                domVideo.append('<source src="'+ postData.body +'" />');
                            }
                            videoHandle = videojs(domVideo.get(0), {
                                width: domUsedBody.innerWidth(),
                                height: domUsedBody.innerWidth() * 540 / 960,
                            });
                            break;
                        case 8:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            break;
                        case 10:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });
                            domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                var field = false;
                                var answer = false;
                                formFields.forEach(function (formField) {
                                    if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                        field = formField;
                                    }
                                });
                                formFieldAnswers.forEach(function (formFieldAnswer) {
                                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                        answer = formFieldAnswer;
                                    }
                                });
                                if (field) {
                                    if (answer) {
                                        field.answer = answer.strFormFieldAnswer_answer;
                                    }
                                    else{
                                        field.answer = false;
                                    }
                                }
                                var hdl = new AnsweredFormFieldClass($(d), field);
                                hdl.init();
                            });
                            break;
                        default:
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });
                            domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                var field = false;
                                var answer = false;
                                formFields.forEach(function (formField) {
                                    if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                        field = formField;
                                    }
                                });
                                formFieldAnswers.forEach(function (formFieldAnswer) {
                                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                        answer = formFieldAnswer;
                                    }
                                });
                                if (field) {
                                    if (answer) {
                                        field.answer = answer.strFormFieldAnswer_answer;
                                    }
                                    else{
                                        field.answer = false;
                                    }
                                }
                                var hdl = new AnsweredFormFieldClass($(d), field);
                                hdl.init();
                            });
                            break;
                    }
                }
            }

            var bindEvents = function () {
                domFooter.find('.step-next').click(function () {
                    nextPost();
                    // if (isWelcome) {
                    //     showPrevPost();
                    // }
                    // else if (CLIENT_ID !== -1) {
                    //     seekNext();
                    // }
                    // else {
                    //     nextPost();
                    // }
                });
                domFooter.find('.step-back').click(function () {
                    prevPost();
                    // if (isWelcome) {
                    //     showPrevPost();
                    // }
                    // else if (CLIENT_ID !== -1) {
                    //     seekPrev();
                    // }
                    // else {
                    //     prevPost();
                    // }
                })
            }

            var insertCSViewAs = function () {
                var sets = {
                    intCSView_client_ID: CLIENT_ID,
                    intCSView_series_ID: initialSeries.series_ID,
                    intCSView_seekParent: postData.parent,
                    intCSView_seekOrder: postData.order
                };
                return ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'insert', sets: sets}, false).then(function (res) {
                    clientSeriesView = res.data;
                    $('.loading').hide();
                    return res.data;
                });
            }

            this.setData = function (data) {
                if (CLIENT_ID !== -1) {
                    var sets = {
                        intCSView_seekParent: data.parent,
                        intCSView_seekOrder: data.order,
                    };
                    ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'update', where: clientSeriesView.clientSeriesView_ID, sets: sets}).then(function (res) {
                        self.bindData(data);
                    })
                }
                else {
                    self.bindData(data);
                }
            }
            this.getPostData = function () {
                return postData ? postData : false;
            }
            this.showWelcome = function () {
                isWelcome = true;
                prevPostData = postData;
                welcomePost.boolPost_free = 1;
                self.bindData(viewFormat(welcomePost));
                var title = domRoot.find('.post-title').html();
                domRoot.find('.post-title').html(title.replace('name', clientName));

                var where = {intCSView_client_ID: CLIENT_ID, intCSView_series_ID: initialSeries.series_ID};
                ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'get', where: where}, false).then(function (res) {
                    if (!res.data) {
                        insertCSViewAs();
                    }
                    else {
                        clientSeriesView = res.data;
                        $('.loading').hide();
                    }
                });
                console.log(domUsedBody.find('a[href="http://linktouserlocation.com"]'));
                domUsedBody.find('a[href="http://linktouserlocation.com"]').removeAttr('target').attr('href', 'javascript:;').click(function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_post', CSView: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                        isWelcome = false;
                        self.bindData(viewFormat(res.data));
                    });
                });
            }
            this.bindData = function (data) {
                if (postData) {
                    domRoot.removeClass('content-type-' + postData.type);
                }
                postData = $.extend({}, postData, data);
                if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                    domRoot.find('.blog-content-wrapper').hide();
                    domRoot.find('.pay-blog-wrapper').show();
                }
                else {
                    domRoot.find('.blog-content-wrapper').show();
                    domRoot.find('.pay-blog-wrapper').hide();

                    domRoot.find('.post-img').attr('src', postData.image);
                    domRoot.find('.post-title').html(postData.title);
                    domRoot.addClass('content-type-' + postData.type);
                    makeBody();
                }
            }

            this.init = function () {
                domRoot = $('.blog-wrapper');
                domFooter = domRoot.find('.right-panel-footer');
                domBody = domRoot.find('.post-body');
                self.bindData(viewFormat(initialPostData));
                payBlogHandle = new PayBlogClass(domRoot.find('.pay-blog-wrapper .pay-blog'), series);
                payBlogHandle.onPaid(function (purchasedId) {
                    purchased = purchasedId;
                    self.bindData(postData);
                });
                payBlogHandle.init();
                bindEvents();
                setInterval(function () {
                    window.parent.postMessage({name: 'currentHeight', height: $('body').prop('scrollHeight')}, '*');
                }, 200);
            }
        }
        var AuthClass = function () {
            var domAuth, domLoginForm, domRegisterForm;

            function showRegisterForm(){
                $('.loginBox').fadeOut('fast',function(){
                    $('.registerBox').fadeIn('fast');
                    $('.login-footer').fadeOut('fast',function(){
                        $('.register-footer').fadeIn('fast');
                    });
                    $('.modal-title').html('Register with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function showLoginForm(){
                $('#loginModal .registerBox').fadeOut('fast',function(){
                    $('.loginBox').fadeIn('fast');
                    $('.register-footer').fadeOut('fast',function(){
                        $('.login-footer').fadeIn('fast');
                    });

                    $('.modal-title').html('Login with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function openLoginModal(){
                showLoginForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);
            }
            function openRegisterModal(){
                showRegisterForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);

            }

            function shakeModal(errorMsg){
                if (!errorMsg) {
                    errorMsg = 'Invalid email/password combination';
                }
                $('#loginModal .modal-dialog').addClass('shake');
                $('.error').addClass('alert alert-danger').html(errorMsg);
                $('input[type="password"]').val('');
                setTimeout( function(){
                    $('#loginModal .modal-dialog').removeClass('shake');
                }, 1000 );
            }
            var login = function (username, password) {
                $('.loading').show();
                ajaxAPiHandle.pagePost('login.php', {function: 'signin', username: username, password: password}, false).then(function (res) {
                    if (res.result.success) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        ajaxAPiHandle.apiPost('Purchased.php', {action: 'get', where: {intPurchased_series_ID: series.series_ID}}, false).then(function (pRes) {
                            purchased = pRes.data;
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            viewHandle.showWelcome();
                        });
                    }
                    else {
                        shakeModal();
                    }
                });
            }
            var register = function (sets) {
                $('.loading').show();
                var ajaxData = {function: 'register'};
                $.extend(ajaxData, sets);
                ajaxAPiHandle.pagePost('login.php', ajaxData, false).then(function (res) {
                    if (res.result.registered) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        setTimeout(function () {
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            viewHandle.showWelcome();
                        }, 300);
                    }
                    else {
                        shakeModal('something is wrong');
                    }
                });
            }
            var bindEvents = function () {
                $('.big-login').click(function () {
                    openLoginModal();
                });
                $('.show-register').click(function () {
                    showRegisterForm();
                });
                $('.show-login').click(function () {
                    showLoginForm();
                });
                domLoginForm.submit(function () {
                    var username = domLoginForm.find('[name="email"]').val();
                    var password = domLoginForm.find('[name="password"]').val();
                    login(username, password);
                    return false;
                });
                domRegisterForm.submit(function () {
                    var sets = {};
                    sets.email = domRegisterForm.find('[name="email"]').val();
                    sets.f_name = domRegisterForm.find('[name="first_name"]').val();
                    sets.l_name = domRegisterForm.find('[name="last_name"]').val();
                    sets.password = domRegisterForm.find('[name="password"]').val();
                    var confirmPassword = domRegisterForm.find('[name="password_confirmation"]').val();
                    if (sets.password === confirmPassword) {
                        register(sets);
                    }
                    else {
                        shakeModal("Passwords don't match");
                    }
                    return false;
                });
            }
            this.init = function () {
                domAuth = $('#loginModal');
                domLoginForm = domAuth.find('.loginBox form');
                domRegisterForm = domAuth.find('.registerBox form');
                bindEvents();
            }
        }
        this.init = function () {
            viewHandle = new ViewClass();
            authHandle = new AuthClass();

            viewHandle.init();
            authHandle.init();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();