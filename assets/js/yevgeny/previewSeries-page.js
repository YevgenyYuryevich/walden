var is_loading_controlled_in_local = false;

(function () {
    'use strict';

    var PreviewSeriesPageClass = function(){

        var domPageRoot;

        var seriesSectionHandle, postsSectionHandle, embedHandle;

        var viewUserPage = function (data) {
            $('<form action="userpage" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
        }

        var popupThanksJoin = function (data) {
            swal({
                icon: "success",
                title: 'SUCCESS!',
                text: 'Thanks for joining! Would you like to go to your experiences?',
                buttons: {
                    returnHome: {
                        text: "Go to my Experiences",
                        value: 'return_home',
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    customize: {
                        text: "Not yet",
                        value: 'customize',
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
                closeOnClickOutside: false,
            }).then(function (value) {
                if (value == 'return_home'){
                    viewUserPage(data);
                }
                else {
                }
            });
            var domSwalRoot, domDisableCheckbox;
            var disablePopup = function () {
                localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
            }
            var enabelPopup = function () {
                localStorage.removeItem('thegreyshirt_show_popup_every_join');
            }
            var bindEvents = function () {
                domDisableCheckbox.change(function () {
                    $(this).prop('checked') ? disablePopup() : enabelPopup();
                })
            }
            var init = function () {
                setTimeout(function () {
                    domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                    domSwalRoot.addClass('thanks-join');
                    domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                    domDisableCheckbox = domSwalRoot.find('#disable_popup');
                    bindEvents();
                }, 100);
            }();
        }

        var SeriesSectionClass = function () {

            var itemData;

            var domRoot;
            var payHandle = null;
            var self = this;

            var bindEvents = function () {
                domRoot.find('.select-post').parent().click(function () {
                    if (CLIENT_ID > -1){
                        if (itemData.boolSeries_charge == 1 && itemData.purchased == 0) {
                            self.openStripe();
                        }
                        else {
                            parseInt(itemData.purchased) ? unSelectPost() : selectPost();
                        }
                    }
                    else{
                        swal({
                            title: "Please Login first!",
                            icon: "warning",
                            dangerMode: true,
                            buttons: {
                                cancel: "Cancel",
                                login: "Login"
                            },
                        }).then(function(value){
                            if (value == 'login'){
                                window.location.href = 'login';
                            }
                        });
                    }
                });
            }
            var popupErroJoin = function () {
                swal("Sorry, Something went wrong!", {
                    content: 'please try again later'
                });
            }
            var selectPost = function (token) {
                var btn = domRoot.find('.buttons-wrapper .wrap .select-post');
                btn.removeClass("filled");
                btn.addClass("circle");
                btn.html("");
                $(".wrap  svg", domRoot).css("display", "block");
                $(".circle_2", domRoot).attr("class", "circle_2 fill_circle");
                var timer = setInterval(
                    function tick() {
                        if (itemData.purchased){
                            btn.removeClass("circle");
                            btn.addClass("filled");
                            $(".wrap img", domRoot).css("display", "block");
                            $("svg", domRoot).css("display", "none");
                            clearInterval(timer);
                            setTimeout(function () {
                                var s = localStorage.getItem('thegreyshirt_show_popup_every_join');
                                if (s == null || s === '1'){
                                    popupThanksJoin(itemData.purchased);
                                }
                            }, 500);
                        }
                    }, 2000);

                ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}, false).then(function (res) {
                    if (res.status == true){
                        itemData.purchased = res.data;
                        if (token) {
                            postsSectionHandle.refreshBody();
                        }
                    }
                });
            }
            var unSelectPost = function () {
                var btn = domRoot.find('.buttons-wrapper .wrap .select-post');
                btn.removeClass("filled");
                btn.addClass("circle");
                $(".wrap  svg", domRoot).css("display", "block");
                $(".circle_2", domRoot).attr("class", "circle_2 fill_circle");
                var timer = setInterval(
                    function tick() {
                        if (!itemData.purchased){
                            btn.removeClass("circle");
                            btn.html("Join");
                            $(".wrap img", domRoot).css("display", "none");
                            $("svg", domRoot).css("display", "none");
                            clearInterval(timer);
                            if (itemData.boolSeries_charge == 1) {
                                setTimeout(function () {
                                    btn.html("Join $" + itemData.intSeries_price);
                                }, 400);
                            }
                        }
                    }, 2000);
                ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: itemData.purchased}, false).then(function (res) {
                    if (res.status == true){
                        itemData.purchased = 0;
                    }
                });
            }
            this.openStripe = function () {
                if (payHandle) {
                    payHandle.open({
                        name: itemData.strSeries_title,
                        description: itemData.strSeries_description,
                        currency: 'usd',
                        amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                    });
                }
                else {
                    popupErroJoin();
                }
            }
            this.init = function () {
                domRoot = domPageRoot.find('section.series-section');
                itemData = initialSeries;
                if (parseInt(itemData.purchased)){
                    domRoot.find('.wrap .select-post').addClass('filled');
                    domRoot.find('.wrap .select-post').html('');
                    domRoot.find('.wrap img').show();
                }
                if (itemData.boolSeries_charge == 1 && itemData.purchased == 0) {
                    if (stripeApi_pKey && itemData.stripe_account_id) {
                        payHandle = StripeCheckout.configure({
                            key: stripeApi_pKey,
                            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                            locale: 'auto',
                            token: function(token) {
                                selectPost(token);
                            }
                        });
                    }
                }
                bindEvents();
            }
        }

        var PostsSectionClass = function () {

            var domRoot, domContainer;
            var itemHandles = [];

            var availableTypes = [0, 2, 8], audioCnt = 1;
            var series = initialSeries;

            var ItemClass = function (itemData) {
                var payBlogHandle;
                var domItemRoot, domUsedBody, domBody, domVideo;

                var metaData = {};


                var bindData = function () {
                    domItemRoot.find('.day-wrapper .item-day').html(itemData.day);
                    domItemRoot.find('.post-title').html(itemData.strPost_title);
                    makeBody();
                    metaData.isDataLoaded = true;
                }

                var makeBody = function () {
                    var purchased = series.purchased;
                    var type = parseInt(itemData.intPost_type);
                    type = availableTypes.indexOf(type) != -1 ? type : 'other';
                    if (series.boolSeries_charge == 1 && !purchased && itemData.boolPost_free == 0) {
                        newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.find('a.join-now').click(function () {
                            seriesSectionHandle.openStripe();
                        });
                    }
                    else {
                        var newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        switch (parseInt(type)){
                            case 0:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.audioplayer-tobe').attr('data-thumb', itemData.strPost_featuredimage);
                                domUsedBody.find('.audioplayer-tobe').attr('data-source', itemData.strPost_body);
                                domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                                var settings_ap = {
                                    disable_volume: 'off',
                                    disable_scrub: 'default',
                                    design_skin: 'skin-wave',
                                    skinwave_dynamicwaves: 'off'
                                };
                                dzsag_init('#audio-' + audioCnt, {
                                    'transition':'fade',
                                    'autoplay' : 'off',
                                    'settings_ap': settings_ap
                                });
                                audioCnt++;
                                break;
                            case 2:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domVideo = domUsedBody.find('video');
                                setTimeout(function () {
                                    if (helperHandle.getApiTypeFromUrl(itemData.strPost_body) === 'youtube') {
                                        var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": itemData.strPost_body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                        domVideo.attr('data-setup', JSON.stringify(setup));
                                    }
                                    else {
                                        domVideo.append('<source src="'+ itemData.strPost_body +'" />');
                                    }
                                    domVideo.attr('width', domUsedBody.innerWidth());
                                    domVideo.attr('height', domUsedBody.innerWidth() * 540 / 960);
                                    videojs(domVideo.get(0));
                                }, 100);
                                break;
                            case 8:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domUsedBody.append(itemData.strPost_body);
                                break;
                            default:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domUsedBody.append(itemData.strPost_body);
                                break;
                        }
                    }
                }
                this.makeBody = makeBody;
                this.isDataLoaded = function () {
                    return metaData.isDataLoaded;
                }
                this.bindData = bindData;
                this.init = function () {
                    domItemRoot = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domContainer);
                    domBody = domItemRoot.find('.item-body');
                    metaData.isDataLoaded = false;
                };
            }

            var bindEvents = function () {
                domContainer.on('afterChange', function (e, slick, currentSlide) {
                    if (currentSlide == itemHandles.length - 1){
                        domContainer.slick('draggable', false);
                    }
                    var loadIndex = currentSlide + 2;
                    if (loadIndex < itemHandles.length && !itemHandles[loadIndex].isDataLoaded()) {
                        itemHandles[loadIndex].bindData();
                    }

                });
            }
            this.refreshBody = function () {
                itemHandles.forEach(function (hdl) {
                    if (hdl.isDataLoaded()) {
                        hdl.makeBody();
                    }
                });
            }
            this.init = function () {
                domRoot = domPageRoot.find('section.posts-section');
                domContainer = domRoot.find('.items-container');


                initialPosts.forEach(function (postData, i) {
                    postData.day = i + 1;
                    var handle = new ItemClass(postData);
                    handle.init();
                    if (i < 3){
                        handle.bindData();
                    }
                    itemHandles.push(handle);
                });

                domContainer.slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: false,
                    autoplaySpeed: 5000,
                    centerPadding: '20%',
                    nextArrow: $('.next-pagging'),
                    prevArrow: $('.prev-pagging'),
                    responsive: [
                    ]
                });
                bindEvents();
            }
        }

        this.init = function () {
            domPageRoot = $('.page.site-wrapper');
            domPageRoot.find('.share-series').circleNav(domPageRoot.find('.main-content .circle-nav-wrapper'));

            seriesSectionHandle = new SeriesSectionClass();
            postsSectionHandle = new PostsSectionClass();
            embedHandle = new SeriesCopyEmbedClass(domPageRoot.find('.series-copy-embed.component'), initialSeries);

            seriesSectionHandle.init();
            postsSectionHandle.init();
            embedHandle.init();
        }
    }

    var previewSeriesPageHandle = new PreviewSeriesPageClass();
    $(document).ready(function () {
        previewSeriesPageHandle.init();
    });
})();