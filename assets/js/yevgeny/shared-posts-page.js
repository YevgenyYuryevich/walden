(function () {
    'use strict';
    var PageClass = function () {
        var domPage, domPostList;

        var itemHandles = [];

        var ItemClass = function (itemData) {
            var domItem, domBody, domTypeBody;
            var self = this;
            var bindData = function () {
                domItem.addClass('type-' + itemData.intPost_type);
                switch (parseInt(itemData.intPost_type)) {
                    case 0:
                        domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        break;
                    case 2:
                        domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        domTypeBody.find('img').attr('src', itemData.strPost_featuredimage);
                        break;
                    default:
                        domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        domTypeBody.html(itemData.strPost_body);
                        break;
                }
                domItem.find('.item-title').html(itemData.strPost_title).attr('href', 'view_blog?id=' + itemData.post_ID);
            }
            this.data = function () {
                return itemData;
            }
            this.setDuration = function () {
                if (itemData.strPost_duration) {
                    domBody.find('.blog-duration').html(itemData.strPost_duration);
                }
                else {
                    helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                        domBody.find('.blog-duration').html(res);
                        itemData.strPost_duration = res;
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                    });
                }
            }
            this.detach = function () {
                domItem.detach();
            }
            this.init = function () {
                domItem = domPostList.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domPostList);
                domBody = domItem.find('.item-type-body');
                bindData();
                self.setDuration();
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            domPostList = domPage.find('.post-list');
            posts.forEach(function (post) {
                var hdl = new ItemClass(post);
                hdl.init();
                itemHandles.push(hdl);
            });
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();