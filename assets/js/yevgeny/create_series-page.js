(function () {
    var PageClass = function () {
        var domPage, domPageHeader, domVirtualLinks;

        var step1SelectFromHandle, step1AddFromScratchHandle, step1AddFromSearchHandle, step1AddFromUploadHandle, step2ArrangeHandle, step3CreateSeriesHandle, step4SelectSetHandle;
        var step0MembershipChargeHandle, step0ChargeTypeHandle, step0MembershipValueHandle, step0OnetimeValueHandle, step0AffiliateChargeHandle, step0AffiliateValueHandle, step0StripeConnectHandle;
        var stepChargeHandle, stepInviteHandle, stepShareHandle;
        var paneHistories = [];
        var postsCnt = 0;
        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            duration: 'strPost_duration',
            keywords: 'strPost_keywords',
        };
        var series = false;

        var postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }
        var viewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                fk = helperHandle.keyOfVal(postFields, k);
                if (fk) { fData[fk] = data[k]; }
            }
            fData.origin = data;
            return fData;
        }
        var scrollTop = function () {
            setTimeout(function () {
                $('html, body').stop().animate({
                    scrollTop: 0
                }, 300, 'swing');
            }, 100);
        }
        var bindEvents = function () {
            domPage.find('[href="#step-0-membership-charge"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-membership-charge');
                scrollTop();
            });
            domPage.find('[href="#step-0-charge-type"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-charge-type');
                scrollTop();
            });
            domPage.find('[href="#step-0-membership-value"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-membership-value');
                scrollTop();
            });
            domPage.find('[href="#step-0-onetime-value"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-onetime-value');
                scrollTop();
            });
            domPage.find('[href="#step-0-affiliate-charge"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-affiliate-charge');
                scrollTop();
            });
            domPage.find('[href="#step-0-affiliate-value"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-affiliate-value');
                scrollTop();
            });
            domPage.find('[href="#step-0-stripe-connect"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(1);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-1').addClass('active');
                domPageHeader.find('.step-name').html('Set a alue for your membership service');
                paneHistories.push('#step-0-stripe-connect');
                scrollTop();
            });
            domPage.find('[href="#step-1-select-from"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(3);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-3').addClass('active');
                domPageHeader.find('.step-name').html('Pull great content together');
                paneHistories.push('#step-1-select-from');
                scrollTop();
            });
            domPage.find('[href="#step-1-add-from-scratch"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(3);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-3').addClass('active');
                paneHistories.push('#step-1-add-from-scratch');
                scrollTop();
            });
            domPage.find('[href="#step-1-add-from-search"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(3);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-3').addClass('active');
                paneHistories.push('#step-1-add-from-search');
                scrollTop();
            });
            domPage.find('[href="#step-1-add-from-upload"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(3);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-3').addClass('active');
                paneHistories.push('#step-1-add-from-upload');
                scrollTop();
            });
            domPage.find('[href="#step-1-add-more"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(3);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-3').addClass('active');
                paneHistories.push('#step-1-add-more');
                scrollTop();
            });
            domPage.find('[href="#step-2-arrange"], [data-target="#step-2-arrange"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(4);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-4').addClass('active');
                paneHistories.push('#step-2-arrange');
                step2ArrangeHandle.save().then(function () {
                    scrollTop();
                });
                step2ArrangeHandle.activeExplainer();
            });
            domPage.find('[href="#step-3-create-series"]').on('shown.bs.tab', function () {
                domPageHeader.find('.title-wrapper .step-value').html(2);
                domPageHeader.find('.progress-item').removeClass('active');
                domPageHeader.find('.progress-item.step-2').addClass('active');
                paneHistories.push('#step-3-create-series');
                scrollTop();
            });
            domPage.find('[href="#step-4-select-set"]').on('shown.bs.tab', function () {
                domPageHeader.find('.step-progress-wrapper').hide();
                paneHistories.push('#step-4-select-set');
                domPageHeader.find('.posts-count-box').addClass('last-step');
                scrollTop();
            });
            domPage.find('[href="#step-charge"]').on('shown.bs.tab', function () {
                paneHistories.push('#step-charge');
                scrollTop();
            });
            domPage.find('[href="#step-invite"]').on('shown.bs.tab', function () {
                paneHistories.push('#step-invite');
                scrollTop();
            });
            domPage.find('[href="#step-share"]').on('shown.bs.tab', function () {
                paneHistories.push('#step-share');
                scrollTop();
            });
            domPage.find('.slider-nav-wrapper.left-wrapper').click(function () {
                paneHistories.pop();
                domVirtualLinks.find('[href="'+ paneHistories[paneHistories.length - 1] +'"]').tab('show');
                paneHistories.pop();
            });
        }

        var Step1SelectFromClass = function () {

            var domPane;

            var bindEvents = function () {
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-1-select-from"]').tab('show');
            }
            this.init = function () {
                domPane = domPage.find('.step-1-select-from-pane');
                bindEvents();
            }
        }

        var Step1AddFromScratchClass = function () {
            var domPane, domForm, domPostTypeWrap;
            var self = this;

            var searchTerms = [];
            var intPost_type = 7, strPost_nodeType = 'post';

            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var setType = function (v) {
                domPostTypeWrap.find('ul li').removeAttr('hidden');
                var a = domPostTypeWrap.find('ul li a[data-value="'+ v +'"]');
                a.parent().attr('hidden', true);
                var txt = a.text();
                domPostTypeWrap.data('value', v);
                domPostTypeWrap.find('button span').text(txt);
                v = parseInt(v) ? parseInt(v) : v;
                switch (v) {
                    case 'menu':
                        strPost_nodeType = 'menu';
                        intPost_type = 7;
                        domForm.attr('data-type', 'menu');
                        break;
                    case 'path':
                        strPost_nodeType = 'path';
                        intPost_type = 7;
                        domForm.attr('data-type', 'path');
                        break;
                    case 9:
                        strPost_nodeType = 'post';
                        intPost_type = v;
                        domForm.attr('data-type', 'switch-to');
                        break;
                    case 10:
                        strPost_nodeType = 'post';
                        intPost_type = v;
                        domForm.attr('data-type', 'form');
                        break;
                    default:
                        strPost_nodeType = 'post';
                        intPost_type = v;
                        domForm.attr('data-type', v);
                        break;
                }
            }
            var bindEvents = function () {
                domPane.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domPane.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domPane.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domPostTypeWrap.find('ul li a').click(function () {
                    domPostTypeWrap.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domPostTypeWrap.data('value', v);
                    domPostTypeWrap.find('button span').text(txt);
                    setType(v);
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.submit(function () {
                    create().then(function (sets) {
                        var sHandles = step2ArrangeHandle.add(viewFormat(sets));

                        var selectedListItemHandle = sHandles.list;
                        var selectedGridItemHandle = sHandles.grid;

                        selectedListItemHandle.setGridHandle(sHandles.grid);
                        selectedGridItemHandle.setListHandle(sHandles.list);
                        self.empty();
                        paneHistories.push('#step-1-add-more');
                        step2ArrangeHandle.tabShow();
                    });
                    return false;
                });
            }
            var create = function () {
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_type = intPost_type;
                sets.strPost_nodeType = strPost_nodeType;
                sets.strPost_keywords = searchTerms.join(',');
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {

                    var ajaxData = new FormData();
                    ajaxData.append('file', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                    ajaxData.append('sets', JSON.stringify({where: 'assets/media/image', prefix: 'post'}));

                    return ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true).then(function (res) {
                        sets.strPost_featuredimage = res.data.abs_url;
                        return sets;
                    });
                }
                return Promise.resolve(sets);
            }
            this.empty = function () {
                domForm.find('[name="strPost_title"]').val('');
                domForm.find('[name="intPost_savedMoney"]').val('');
                domForm.find('[name="strPost_body"]').summernote('code', '');
                setType(7);
                searchTerms = [];
                domForm.find('.terms-container').find('.term-item:not(.sample)').remove();
                $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', 'assets/images/global-icons/cloud-uploading.png').css('opacity', 0.7);
            }
            this.init = function () {
                domPane = domPage.find('.step-1-add-from-scratch-pane');
                domForm = domPane.find('form');
                domPostTypeWrap = domForm.find('.dropdown');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domForm.find('[name="strPost_body"]').summernote({
                    height: 200,
                    tabsize: 2,
                });
                bindEvents();
            }
        }

        var Step1AddFromSearchClass = function () {
            var domPane, domSearchSection, domSearchContainer;

            var self = this, youtubeListHandle, podcastsListHandle, blogsListHandle;

            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';

            var bindEvents = function () {
                domPane.find('form.search-input-wrapper').submit(function () {
                    var kwd = $(this).find('input').val();
                    self.applySearch(kwd);
                    return false;
                });
                domPane.find('.add-selected-btn').click(function () {
                    paneHistories.push('#step-1-add-more');
                    step2ArrangeHandle.tabShow();
                });
                domPane.parent().find('.slider-nav.arrow-right[href="#step-2-arrange"]').click(function () {
                    paneHistories.push('#step-1-add-more');
                    return true;
                });
            }
            var addSelected = function () {
                var items = self.dataToAdd();
                items.forEach(function (value) {
                    var sHandles = step2ArrangeHandle.add(value.data());
                    value.setAdded(true);
                    var selectedListItemHandle = sHandles.list;
                    var selectedGridItemHandle = sHandles.grid;

                    selectedListItemHandle.setGridHandle(sHandles.grid);
                    selectedGridItemHandle.setListHandle(sHandles.list);
                });
            }
            this.applySearch = function (kwd) {
                self.searchByKeyword(kwd).then(function (res) {
                    domSearchSection.show();
                    youtubeListHandle.setItems(res.data.youtube, kwd);
                    podcastsListHandle.setItems(res.data.spreaker, kwd);
                    blogsListHandle.setItems(res.data.blogs, kwd);
                    setTimeout(function () {
                        $('html, body').stop().animate({
                            scrollTop: domSearchSection.offset().top - 100
                        }, 300, 'swing');
                    }, 300);
                    // ideaboxListHandle.setItems(res.data.ideabox, kwd);
                    // postsListHandle.setItems(res.data.posts, kwd);
                });
            }

            this.searchByKeyword = function (keyword) {
                var ajaxData = {
                    q: keyword,
                    size: 50,
                    items: [
                        {name: 'youtube', token: false},
                        {name: 'spreaker', token: false},
                        {name: 'blogs', token: false},
                        {name: 'recipes', token: false},
                        {name: 'ideabox', token: false},
                        {name: 'posts', token: false},
                        {name: 'rssbPosts', token: false},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData);
            }

            var SearchRowClass = function (from) {
                var domSearchRow, domItemsList;

                var self = this, itemHandles = [], filteredHandles = [];

                var bindData = function () {
                    switch (from) {
                        case 'youtube':
                            domSearchRow.find('.from-name').html('from Youtube');
                            break;
                        case 'podcasts':
                            domSearchRow.find('.from-name').html('from Podcasts');
                            break;
                        case 'blog':
                            domSearchRow.find('.from-name').html('from Blogs');
                            break;
                        case 'recipes':
                            domSearchRow.find('.from-name').html('from Recipes');
                            break;
                        case 'ideabox':
                            domSearchRow.find('.from-name').html('from Ideabox');
                            break;
                        case 'posts':
                            domSearchRow.find('.from-name').html('from Posts');
                            break;
                        case 'rssb-posts':
                            domSearchRow.find('.from-name').html('from RSSBlogPost');
                            break;
                        default:
                            domSearchRow.find('.from-name').html('from Posts');
                            break;

                    }
                }

                var bindEvents = function () {
                    domItemsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 10;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }

                var formatItemData = function(itemData){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.name;
                            fData.summary = '';
                            fData.body = itemData.body;
                            fData.image = itemData.image;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    fData.origin = itemData;
                    return fData;
                }

                var SearchItemClass = function (itemData) {

                    var dataLoaded = false, isAdded = false, isSelected = false;
                    var domItem;

                    var self = this, selectedListItemHandle, selectedGridItemHandle;

                    var bindData = function () {
                        dataLoaded = true;
                        domItem.find('.item-img').attr('src', itemData.image);
                        domItem.find('.item-title').html(itemData.title);
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            domItem.find('.blog-duration').html(res);
                            itemData.duration = res;
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.item-img-wrapper').click(function () {
                            if (isSelected) {
                                self.deSelect();
                                postsCnt--;
                                domPageHeader.find('.posts-count-box .posts-count').html(postsCnt);
                            }
                            else {
                                self.select();
                            }
                            if (step1AddFromSearchHandle.selectedData().length) {
                                domPane.parent().find('.slider-nav-wrapper.right-wrapper').show();
                            }
                            else {
                                domPane.parent().find('.slider-nav-wrapper.right-wrapper').hide();
                            }
                        });
                        domItem.find('.item-title').click(function () {
                            self.preview();
                        });
                    }

                    this.setAdded = function (v) {
                        isAdded = v;
                    }

                    this.preview = function () {
                        var data = itemData;
                        var $form = $('<form action="preview_blog" method="post" target="_blank" hidden></form>').appendTo('body');
                        $form.append('<input name="type" value="'+ data.type +'"/>');
                        $form.append('<input name="title" value="'+ data.title +'"/>');
                        $form.append('<input name="body" value="'+ data.body +'"/>');
                        $form.append('<input name="img" value="'+ data.image +'"/>');
                        $form.append('<input name="duration" value="'+ (data.duration ? data.duration : '') +'"/>');
                        $form.submit();
                        $form.remove();
                    }

                    this.postFormat = function () {
                        var f = {
                            strPost_title: itemData.title,
                            strPost_body: itemData.body,
                            strPost_summary: itemData.summary,
                            intPost_type: itemData.type,
                            strPost_featuredimage: itemData.image,
                        }
                        if (itemData.duration) {
                            f.strPost_duration = itemData.duration;
                        }
                        return f;
                    }

                    this.remove = function () {
                        domItem.remove();
                    }
                    this.isSelected = function () {
                        return isSelected;
                    }
                    this.isAdded = function () {
                        return isAdded;
                    }
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.select = function () {
                        isSelected = true;
                        domItem.addClass('added-item');
                        var sHandles = step2ArrangeHandle.add(itemData);
                        isAdded = true;
                        selectedListItemHandle = sHandles.list;
                        selectedGridItemHandle = sHandles.grid;

                        selectedListItemHandle.setGridHandle(sHandles.grid);
                        selectedGridItemHandle.setListHandle(sHandles.list);
                    }
                    this.deSelect = function () {
                        isSelected = false;
                        domItem.removeClass('added-item');
                        selectedListItemHandle.remove();
                        selectedGridItemHandle.remove();

                    }
                    this.bindData = bindData;
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domItemsList);
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.init = function () {
                        domItem = domSearchRow.find('.search-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        bindEvents();
                    }
                }
                this.setItems = function (itemsData, newKeyword) {
                    domItemsList.slick('unslick');
                    filteredHandles.forEach(function (value) {
                        value.remove();
                    });
                    domSearchRow.find('.found-value').html(itemsData.items.length);
                    filteredHandles = [];
                    itemsData.items.forEach(function (item, i) {
                        var itemHandle = new SearchItemClass(helperHandle.formatItemData(item, from));
                        itemHandle.init();
                        if (i < 15) {
                            itemHandle.bindData();
                        }
                        itemHandles.push(itemHandle);
                        filteredHandles.push(itemHandle);
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        centerMode: false,
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                            {
                                breakpoint: 769,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            },
                        ]
                    });
                }
                this.selectedItems = function () {
                    var sItems = [];
                    itemHandles.forEach(function (value) {
                        if (value.isSelected()) {
                            sItems.push(value.data());
                        }
                    });
                    return sItems;
                }
                this.itemsToAdd = function () {
                    var aItems = [];
                    itemHandles.forEach(function (value) {
                        if (value.isSelected() && !value.isAdded()) {
                            aItems.push(value);
                        }
                    });
                    return aItems;
                }
                this.init = function () {
                    domSearchRow = domSearchContainer.find('.search-row.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSearchContainer);
                    domItemsList = domSearchRow.find('.search-items-list');
                    bindData();
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        centerMode: false,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                        ]
                    });
                    bindEvents();
                }
            }
            this.selectedData = function () {
                var items = [];
                items = items.concat(youtubeListHandle.selectedItems());
                items = items.concat(podcastsListHandle.selectedItems());
                items = items.concat(blogsListHandle.selectedItems());
                return items;
            }
            this.dataToAdd = function () {
                var items = [];
                items = items.concat(youtubeListHandle.itemsToAdd());
                items = items.concat(podcastsListHandle.itemsToAdd());
                items = items.concat(blogsListHandle.itemsToAdd());
                return items;
            }
            this.init = function () {
                domPane = domPage.find('.step-1-add-from-search-pane');
                domSearchSection = domPane.find('.search-section-wrapper');
                domSearchContainer = domPane.find('.search-container');

                youtubeListHandle = new SearchRowClass('youtube');
                youtubeListHandle.init();

                podcastsListHandle = new SearchRowClass('podcasts');
                podcastsListHandle.init();

                blogsListHandle = new SearchRowClass('blog');
                blogsListHandle.init();
                bindEvents();
            }
        }

        var Step1AddFromUploadClass = function () {
            var domPane, domBoard, domFilesList, domFooter;
            var fileRowHandles = [];

            var key = 1;

            var postTypeArr = {
                'video': 2,
                'audio': 0,
                'image': 8,
                'text': 7,
            }

            var FileRowClass = function (file, originType) {

                var domRow;
                var postData = {};

                var self = this;
                var keyI, isUploaded = false, isAdded = false, converted = false, howSave = 'keep', convertedHtml = false, savedUrl = false;
                var dType;
                var bindData = function () {
                    if (originType === 'file') {
                        postData.title = file.name;
                        postData.strType = helperHandle.extractTypeFromFileType(file.type);
                        postData.type = postTypeArr[postData.strType];
                        postData.fullType = file.type;
                        dType = file.type;

                        if (file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            dType = 'application/docx';
                        } else if (file.type === 'application/msword') {
                            dType = 'application/doc';
                        } else if (file.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                            dType = 'application/ppt';
                        }
                    }
                    else {
                        postData = file;
                        postData.strType = 'url';
                        dType = originType;
                        if (postData.image) {
                            domRow.find('img.item-img').attr('src', postData.image);
                            domRow.find('.item-img-wrapper').css('opacity', 1);
                        }
                    }

                    domRow.find('.item-title-input').attr('placeholder', postData.title).val(postData.title);

                    switch (postData.strType) {
                        case 'video':
                            break;
                        case 'music':
                            break;
                        case 'image':
                            updateCoverImage(file);
                            break;
                        case 'application':
                        case 'text':
                            domRow.addClass('can-convert');
                            // fileToText();
                            uploadFile();
                            break;
                    }
                }
                var uploadFile = function () {
                    var ajaxData = new FormData();
                    ajaxData.append('file', file);
                    ajaxData.append('sets', JSON.stringify({where: 'assets/media/document', prefix: 'post'}));
                    return ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                        savedUrl = res.data.abs_url;
                        postData.body = '<a href="'+ savedUrl +'" target="_blank">view detail</a>'
                        return true;
                    }, function () {
                        swal('Sorry, these files are too large memory to upload.');
                        return false;
                    });
                }
                var fileToText = function () {
                    var ajaxData = new FormData();
                    ajaxData.append('file', file);
                    return ajaxAPiHandle.multiPartApiPost('FileToHtml.php', ajaxData).then(function (res) {
                        postData.body = res.data;
                        convertedHtml = res.data;
                        return res.data;
                    });
                }
                var updateCoverImage = function(file) {
                    if (helperHandle.extractTypeFromFileType(file.type) != 'image') {
                        swal("Not Image!", "Sorry, This is not image file, Please select image file.").then(function () {
                        });
                        return false;
                    }
                    postData.imageFile = file;
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        domRow.find('img.item-img').attr('src', e.target.result);
                        domRow.find('.item-img-wrapper').css('opacity', 1);
                    }
                    reader.readAsDataURL(file);
                }
                var bindEvents = function () {
                    domRow.find('.image-input').change(function () {
                        if (this.files && this.files[0]) {
                            updateCoverImage(this.files[0]);
                        }
                    });
                    domRow.find('input.item-title-input').change(function () {
                        var title = $(this).val();
                        postData.title = title ? title : $(this).attr('placeholder');
                    });
                    domRow.find('.delete-action').click(function () {
                        if (self.isAdded()) {
                            // self.delete();
                            self.cancel();
                        }
                        else {
                            self.cancel();
                        }
                        if (fileRowHandles.length) {
                            domPane.find('.pane-footer .save-btn').removeAttr('disabled');
                            domPane.parent().find('.slider-nav-wrapper.right-wrapper').show();
                        }
                        else {
                            domPane.find('.pane-footer .save-btn').attr('disabled', true);
                            domPane.parent().find('.slider-nav-wrapper.right-wrapper').hide();
                        }
                    });
                    domRow.find('.preview').click(function () {
                        self.preview();
                    });
                    domRow.find('.convert-btn').click(function () {
                        howSave = 'convert';
                        domRow.find('.data-generate-col button').removeClass('active');

                        if (converted) {
                            $(this).addClass('active');
                            postData.body = convertedHtml;
                        }
                        else {
                            fileToText().then(function (res) {
                                converted = true;
                                domRow.find('.convert-btn').addClass('active');
                                postData.body = res;
                            });
                        }
                    });
                    domRow.find('.keep-btn').click(function () {
                        howSave = 'keep';
                        domRow.find('.data-generate-col button').removeClass('active');
                        if (savedUrl) {
                            postData.body = '<a href="'+ savedUrl +'" target="_blank">view detail</a>'
                            $(this).addClass('active');
                        }
                        else {
                            var ajaxData = new FormData();
                            ajaxData.append('file', file);
                            ajaxData.append('sets', JSON.stringify({where: 'assets/media/' + postData.strType, prefix: 'post'}));
                            ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                                savedUrl = res.data.abs_url;
                                postData.body = '<a href="'+ savedUrl +'" target="_blank">view detail</a>'
                                domRow.find('.keep-btn').addClass('active');
                                return true;
                            }, function () {
                                swal('Sorry, these files are too large memory to upload.');
                                return false;
                            });
                        }
                    });
                }
                this.preview = function () {
                    var wData = postData;
                    if (!wData.body) {
                        wData.body = window.URL.createObjectURL(file);
                    }
                    // viewWidgetHandle.view(wData);
                }
                this.data = function () {
                    return postData;
                }
                this.key = function () {
                    return keyI;
                }
                this.isUploaded = function () {
                    return isUploaded;
                }
                this.isAdded = function () {
                    return isAdded;
                }
                this.setAdded = function (v) {
                    isAdded = v;
                }
                this.delete = function () {
                    if (Array.isArray(postData.id)) {
                        postData.id.forEach(function (id) {
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: id}, true, 200).then(function (res) {
                            });
                        });
                        self.cancel();
                    }
                    else {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: postData.id}).then(function (res) {
                            self.cancel();
                        });
                    }
                }
                this.cancel = function () {
                    fileRowHandles.forEach(function (hdl, i) {
                        if (hdl.key() === self.key()) {
                            fileRowHandles.splice(i, 1);
                            domRow.remove();
                        }
                    });
                }
                this.upload = function () {

                    var resPromise;
                    if (postData.strType === 'text' || postData.strType === 'application' || postData.strType === 'image' || postData.strType === 'url') {
                        resPromise = Promise.resolve(true);
                    }
                    else {
                        var ajaxData = new FormData();
                        ajaxData.append('file', file);
                        ajaxData.append('sets', JSON.stringify({where: 'assets/media/' + postData.strType, prefix: 'post'}));
                        resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                            postData.body = res.data.abs_url;
                            return true;
                        }, function () {
                            swal('Sorry, these files are too large memory to upload.');
                            return false;
                        });
                    }
                    return resPromise.then(function (rp) {
                        if (rp) {
                            if (postData.imageFile) {
                                var ajaxData = new FormData();
                                ajaxData.append('file', postData.imageFile);
                                ajaxData.append('sets', JSON.stringify({where: 'assets/images', prefix: 'post'}));
                                return ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                                    postData.image = res.data.abs_url;
                                    isUploaded = true;
                                    return true;
                                });
                            }
                            else {
                                isUploaded = true;
                                return true;
                            }
                        }
                    });
                }
                this.init = function () {
                    keyI = key++;
                    domRow = domFilesList.find('.upload-term-item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domFilesList);
                    originType = originType ? originType : 'file';
                    bindData();
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }
            var makeItemByUrl = function (url) {
                var sourceType = helperHandle.getSourceTypeFromUrl(url);
                var what = 'curl';
                var where = url;
                switch (sourceType) {
                    case 'youtube':
                        what = 'youtube';
                        where = {id: helperHandle.parseYoutubeUrl(url).id};
                        break;
                    default:
                        break;
                }
                var fData;
                return ajaxAPiHandle.apiPost('Api.php', {action: 'get', what: what, where: where}).then(function (res) {
                    var hdl;
                    switch (what) {
                        case 'youtube':
                            fData = helperHandle.formatItemData(res.data, 'youtube');
                            hdl = new FileRowClass(fData, 'youtube');
                            break;
                        default:
                            fData = helperHandle.formatItemData(res.data);
                            fData.body = fData.body + '<div><a href="'+ url +'" target="_blank">Continue Reading...</a></div>';
                            hdl = new FileRowClass(fData, 'URL');
                            break;
                    }
                    hdl.init();
                    fileRowHandles.push(hdl);
                    domPane.find('.pane-footer .save-btn').removeAttr('disabled');
                    domPane.parent().find('.slider-nav-wrapper').show();
                    return hdl;
                });
            }
            var bindEvents = function () {
                domPane.find('.file-input').change(function () {
                    var files = $(this).prop('files');
                    for (var i = 0; i < files.length; i ++) {
                        var hdl = new FileRowClass(files[i]);
                        hdl.init();
                        fileRowHandles.push(hdl);
                    }
                    domPane.find('.pane-footer .save-btn').removeAttr('disabled');
                    domPane.parent().find('.slider-nav-wrapper').show();
                    $(this).val('');
                });
                domFooter.find('.save-btn').click(function () {
                    addAll().then(function () {
                        paneHistories.push('#step-1-add-more');
                        step2ArrangeHandle.tabShow();
                    })
                });
                domFooter.find('.cancel-all-btn').click(function () {
                    fileRowHandles.forEach(function (value) {
                        value.cancel();
                    });
                });
                domPane.parent().find('.slider-nav.arrow-right').click(function () {
                    addAll().then(function () {
                        paneHistories.push('#step-1-add-more');
                        step2ArrangeHandle.tabShow();
                    });
                    return true;
                });
                domBoard.dndhover().on('dndHoverStart', function (event) {
                    domBoard.addClass('drag-over');
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                }).on('dndHoverEnd', function (event) {
                    domBoard.removeClass('drag-over');
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                });
                domBoard.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                }).on('dragover dragenter', function () {
                }).on('dragenter', function () {
                }).on('dragleave dragend drop', function() {
                }).on('drop', function(e) {
                    domBoard.removeClass('drag-over');
                    var dataInfo = helperHandle.getInfoFromTransfer(e.originalEvent.dataTransfer);
                    if (dataInfo.type === 'url') {
                        domBoard.find('.url-input').val(dataInfo.url);
                        makeItemByUrl(dataInfo.url);
                    }
                    else if (dataInfo.type === 'other') {
                    }
                    else {
                        var hdl = new FileRowClass(dataInfo.file);
                        hdl.init();
                        fileRowHandles.push(hdl);
                        domPane.find('.pane-footer .save-btn').removeAttr('disabled');
                        domPane.parent().find('.slider-nav-wrapper').show();
                    }
                });
                domBoard.find('.url-input').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (helperHandle.isAbsUrl(v)) {
                            makeItemByUrl(v);
                        }
                        else {
                            swal('Please insert valid url');
                        }
                    }
                });

            }
            var addAll = function () {
                var uploadsAll = [];
                fileRowHandles.forEach(function (value) {
                    if (!value.isUploaded()) {
                        uploadsAll.push(value.upload());
                    }
                });
                return Promise.all(uploadsAll).then(function () {
                    domFilesList.find('>.upload-term-item:not(.sample)').each(function () {
                        var hdl = $(this).data('controlHandle');
                        if (hdl.isAdded()) {
                            return true;
                        }
                        hdl.setAdded(true);
                        var value = hdl.data();
                        if (Array.isArray(value.strPost_body)) {
                            value.strPost_body.forEach(function (body, i) {
                                var postFData = $.extend({}, value, {strPost_body: body, strPost_title: value.title + '('+ (i + 1) +')'});
                                var sHandles = step2ArrangeHandle.add(postFData);

                                var selectedListItemHandle = sHandles.list;
                                var selectedGridItemHandle = sHandles.grid;

                                selectedListItemHandle.setGridHandle(sHandles.grid);
                                selectedGridItemHandle.setListHandle(sHandles.list);
                            });
                        }
                        else {
                            var sHandles = step2ArrangeHandle.add(value);

                            var selectedListItemHandle = sHandles.list;
                            var selectedGridItemHandle = sHandles.grid;

                            selectedListItemHandle.setGridHandle(sHandles.grid);
                            selectedGridItemHandle.setListHandle(sHandles.list);
                        }
                    });
                    return true;
                });
            }
            this.init = function () {
                domPane = domPage.find('.step-1-add-from-upload-pane');
                domBoard = domPane.find('.identify-board');
                domFooter = domPane.find('.pane-footer');
                domFilesList = domPane.find('.items-container');
                domFilesList.sortable({
                    placeholder: 'ui-state-highlight',
                    items: '.upload-term-item',
                    coneHelperSize: true,
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    revert: 300, // animation in milliseconds
                    // handle: '.img-wrapper',
                    opacity: 0.9,
                    update: function (a, b) {}
                });
                $.fn.dndhover = function(options) {
                    return this.each(function() {

                        var self = $(this);
                        var collection = $();

                        self.on('dragenter', function(event) {
                            if (collection.length === 0) {

                                self.trigger('dndHoverStart');
                            }
                            collection = collection.add(event.target);
                        });

                        self.on('dragleave', function(event) {
                            /*
                             * Firefox 3.6 fires the dragleave event on the previous element
                             * before firing dragenter on the next one so we introduce a delay
                             */
                            setTimeout(function() {
                                collection = collection.not(event.target);
                                if (collection.length === 0) {
                                    self.trigger('dndHoverEnd');
                                }
                            }, 100);
                        });
                    });
                };
                bindEvents();
            }
        }

        var Step2ArrangeClass = function () {

            var domPane;
            var structureExplainerHandle;
            var postsStructureHandle;
            var isExplainerInited = false;

            var bindEvents = function () {
                domPane.find('.pane-footer .save-btn').click(function () {
                    step4SelectSetHandle.tabShow();
                });
            }

            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-2-arrange"]').tab('show');
            }

            this.add = function (post) {
                postsCnt++;
                domPageHeader.find('.posts-count-box .posts-count').html(postsCnt);
                return postsStructureHandle.add(post);
            }
            this.save = function () {
                return postsStructureHandle.save();
            }
            this.activeExplainer = function () {
                if (!isExplainerInited) {
                    structureExplainerHandle.init();
                    isExplainerInited = true;
                }
            }
            this.init = function () {
                domPane = domPage.find('.step-2-arrange-pane');
                step3CreateSeriesHandle.getId().then(function (series) {
                    postsStructureHandle = new PostsStructureClass(domPane.find('.posts-structure-component'), series);
                    postsStructureHandle.init();
                    postsStructureHandle.setMode('add_mode');
                });
                structureExplainerHandle = new PostsStructureExplainerClass(domPane.find('.posts-structure-explainer-component'));
                bindEvents();
            }
        }

        var Step3CreateSeriesClass = function () {
            var domPane;
            var self = this;
            var isCreateTry = false;
            var seriesId = false;
            var saved = false;
            var canCreatePublicSeries = true;
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domPane).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domPane.find('.cover-image-wrapper [name="strSeries_image"]').change(function () {
                    updateCoverImage(this);
                });
                domPane.find('[name="boolSeries_isPublic"]').change(function () {
                    if ($(this).prop('checked')) {
                        domPane.find('.public-value-txt').html('available now');
                    }
                    else {
                        domPane.find('.public-value-txt').html('');
                    }
                });
                domPage.submit(function () {
                    if (parseInt(CLIENT_ID) !== -1) {
                        save();
                    }
                    else {
                        isCreateTry = true;
                        authHandle.popup();
                    }
                    return false;
                });
                if (parseInt(CLIENT_ID) === -1) {
                    authHandle.afterLogin(function () {
                        setTimeout(function () {
                            swal({
                                icon: "success",
                                title: 'SUCCESS!',
                                text: 'Now you can create series.',
                            }).then(function () {
                                if (isCreateTry) {
                                    save();
                                }
                            });
                        }, 300);
                    });
                }
                window.onbeforeunload = function() {
                    if (!saved) {
                        ajaxAPiHandle.apiPost('Series.php', {action: 'delete', where: seriesId}, false);
                    }
                }
            }
            var save = function () {
                saved = true;
                var sets = self.data();
                var ajaxData = new FormData();
                ajaxData.append('action', 'update');
                if (sets.strSeries_image) {
                    ajaxData.append('strSeries_image', sets.strSeries_image);
                    delete sets.strSeries_image;
                }
                sets.intSeries_client_ID = CLIENT_ID;
                sets.strSeries_status = 'pending';
                sets.boolSeries_approved = 'server_control';
                ajaxData.append('sets', JSON.stringify(sets));
                ajaxData.append('where', seriesId);
                return ajaxAPiHandle.multiPartApiPost('Series.php', ajaxData, true, 300).then(function (res) {
                    series = res.data;
                    step4SelectSetHandle.setSeries(seriesId);
                    stepChargeHandle.init();
                    stepInviteHandle.init();
                    stepShareHandle.init();
                    createPlan();
                    step1SelectFromHandle.tabShow();
                });
            }
            var createPlan = function () {
                ajaxAPiHandle.apiPost('Series.php', {action: 'create_plan', series_id: seriesId}, false).then(function (res) {
                    console.log(res);
                });
            }
            var createVirtual = function () {
                return ajaxAPiHandle.apiPost('Series.php', {action: 'insert_auto_draft'}, false).then(function (res) {
                    seriesId = res.data;
                    series = {series_ID: seriesId};
                    return seriesId;
                });
            }();
            this.getId = function () {
                if (seriesId) {
                    return Promise.resolve(seriesId);
                }
                else {
                    return createVirtual;
                }
            }
            this.data = function () {
                var data = {};
                data.strSeries_title = domPane.find('[name="strSeries_title"]').val();
                data.strSeries_description = domPane.find('[name="strSeries_description"]').val();
                data.strSeries_level = domPane.find('[name="strSeries_level"]').val();
                data.intSeries_category = domPane.find('[name="intSeries_category"]').val();
                data.boolSeries_isPublic = domPane.find('[name="boolSeries_isPublic"]').prop('checked') ? 1 : 0;
                data.available_days = '';
                domPane.find('.day-item input').each(function (i, d) {
                    data.available_days += $(this).prop('checked') ? '1' : '0';
                });
                if (domPane.find('[name="strSeries_image"]').prop('files').length) {
                    data.strSeries_image = domPane.find('[name="strSeries_image"]').prop('files')[0];
                }
                return data;
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-3-create-series"]').tab('show');
            }
            this.init = function () {
                domPane = domPage.find('.step-3-create-series-pane');
                domPane.find('select[name="intSeries_category"]').select2();
                domPane.find('[data-toggle="popover"]').popover({
                    trigger: 'hover',
                    placement: 'bottom',
                });
                if (authHandle.user() && parseInt(authHandle.user().active) === 0 && !allowUnApprovedUserCanCreatePublicSeries) {
                    canCreatePublicSeries = false;
                }
                bindEvents();
            }
        }

        var Step4SelectSetClass = function () {
            var domPane;
            this.setSeries = function (series) {
                domPane.parent().find('.slider-nav').attr('href', 'preview_series?id=' + series);
                domPane.find('.button-wrapper .go-next-btn').attr('href', 'preview_series?id=' + series);
                domPane.find('.actually-link').attr('href', 'edit_series?id=' + series);
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-4-select-set"]').tab('show');
            }
            this.init = function () {
                domPane = domPage.find('.step-4-select-set-pane');
            }
        }

        var StepChargeClass = function () {
            var domPane, domChargeWrap;

            var self = this;

            var bindEvents = function () {
                domChargeWrap.find('input').change(function () {
                    var type = $(this).attr('type');
                    var v;
                    if (type === 'checkbox') {
                        v = $(this).prop('checked') ? 1 : 0;
                    }
                    else {
                        v = $(this).val();
                    }
                    series[$(this).attr('name')] = v;
                    bindData();
                    save();
                });
            }
            var bindData = function () {

                if (series.boolSeries_charge) {
                    domChargeWrap.find('[name="boolSeries_charge"]').prop('checked', true);
                    domChargeWrap.find('[name="intSeries_price"]').removeAttr('disabled');
                    domChargeWrap.find('[name="intSeries_price"]').val(series.intSeries_price);

                    if (series.boolSeries_affiliated) {
                        domChargeWrap.find('[name="boolSeries_affiliated"]').prop('checked', true);
                        domChargeWrap.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('enable');
                        domChargeWrap.find('[name="intSeries_affiliate_percent"]').val(series.intSeries_affiliate_percent);

                        var ownerMoney = series.intSeries_price * (100 - parseInt(series.intSeries_affiliate_percent)) / 100;
                        ownerMoney = parseInt(ownerMoney);
                        domChargeWrap.find('.you-will-get-value').html(ownerMoney + '$');

                        var affiliateMoney = series.intSeries_price - ownerMoney;
                        affiliateMoney = parseInt(affiliateMoney);
                        domChargeWrap.find('.affiliate-will-get-value').html(affiliateMoney + '$');
                    }
                    else {
                        domChargeWrap.find('[name="boolSeries_affiliated"]').prop('checked', false);
                        domChargeWrap.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                        domChargeWrap.find('[name="intSeries_affiliate_percent"]').val('');
                        domChargeWrap.find('.you-will-get-value').html(series.intSeries_price + '$');
                        domChargeWrap.find('.affiliate-will-get-value').html('NO Affiliated');
                    }
                }
                else {
                    domChargeWrap.find('[name="boolSeries_charge"]').prop('checked', false);
                    domChargeWrap.find('[name="intSeries_price"]').attr('disabled', true);
                    domChargeWrap.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                    domChargeWrap.find('[name="boolSeries_affiliated"]').prop('checked', false);
                    domChargeWrap.find('[name="intSeries_affiliate_percent"]').val('');
                    domChargeWrap.find('[name="intSeries_price"]').val('');
                    domChargeWrap.find('.you-will-get-value').html('No Charged');
                    domChargeWrap.find('.affiliate-will-get-value').html('NO Affiliated');
                }
            }
            var save = function () {
                var sets = self.data();
                return ajaxAPiHandle.apiPost('Series.php', {action: 'update', sets: sets, where: series.series_ID});
            }
            this.data = function () {
                var sets = {
                    boolSeries_charge: series.boolSeries_charge,
                    boolSeries_affiliated: series.boolSeries_affiliated,
                };
                if (series.intSeries_price) {
                    sets.intSeries_price = series.intSeries_price;
                }
                if (series.intSeries_affiliate_percent) {
                    sets.intSeries_affiliate_percent = series.intSeries_affiliate_percent;
                }
                return sets;
            }
            this.dom = function () {
                return domChargeWrap;
            }
            this.init = function () {
                domPane = domPage.find('.step-charge-pane');
                domPane.parent().find('.preview-series-link').attr('href', 'preview_series?id=' + series.series_ID);
                domPane.parent().find('.edit-series-link').attr('href', 'edit_series?id=' + series.series_ID);
                domChargeWrap = domPane.find('.charge-series-wrapper');
                series.boolSeries_charge = parseInt(series.boolSeries_charge);
                series.boolSeries_affiliated = parseInt(series.boolSeries_affiliated);
                series.intSeries_price = series.intSeries_price ? series.intSeries_price : '';
                series.intSeries_affiliate_percent = series.intSeries_affiliate_percent ? series.intSeries_affiliate_percent : '';
                domChargeWrap.find('[name="intSeries_affiliate_percent"]').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 100,
                    value: parseInt(series.intSeries_affiliate_percent),
                });
                bindData();
                bindEvents();
            }
        }

        var StepInviteClass = function () {

            var domPane;

            var coOwnersHandle;

            var CoOwnersClass = function () {
                var domCoOwner, domItemsList;

                var filterInvitationStatus = 'accept';
                var invitations = [];
                var seriesOwners = [{
                    strSeriesInvitation_status: 'accept',
                    intSeriesOwner_role: 1,
                    user: authHandle.user(),
                }];
                var itemHandles = [], filteredHandles = [];

                var ItemClass = function (itemData) {
                    var domItem;
                    var bindData = function () {
                        switch (parseInt(itemData.intSeriesOwner_role)) {
                            case 1:
                                domItem.find('.role-txt').html('Creator');
                                break;
                            case 2:
                                domItem.find('.role-txt').html('Admin');
                                break;
                            default:
                                domItem.find('.role-txt').html('Editor');
                                break;
                        }
                        if (itemData.user) {
                            if (itemData.user.image) {
                                domItem.find('.item-img-wrapper').css('background-image', 'url("' + itemData.user.image + '")').css('background-size', 'cover');
                            }
                            if (itemData.strSeriesInvitation_status == 'waiting') {
                                domItem.find('.user-name').html(itemData.strInvitation_email);
                            }
                            else {
                                domItem.find('.user-name').html(itemData.user.f_name);
                            }
                        }
                        else {
                            if (itemData.strSeriesInvitation_status == 'waiting') {
                                domItem.find('.user-name').html(itemData.strInvitation_email);
                            }
                        }
                    }
                    var bindEvents = function () {
                        domItem.find('.delete-action').click(function () {
                            if (itemData.intSeriesOwner_role == 1) {
                                return false;
                            }
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this owner?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteItem();
                                }
                            });
                        });
                    }
                    var deleteItem = function () {
                        if (itemData.strSeriesInvitation_status == 'accept') {
                            ajaxAPiHandle.apiPost('Series.php', {action: 'delete_owner', where: {intSeriesOwner_series_ID: itemData.intSeriesOwner_series_ID, intSeriesOwner_user_ID: itemData.intSeriesOwner_user_ID}}).then(function () {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().seriesOwner_ID == itemData.seriesOwner_ID) {
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                domItem.remove();
                            });
                        }
                        else {
                            ajaxAPiHandle.apiPost('Series.php', {action: 'delete_invitation', where: {intSeries_ID: itemData.intSeries_ID, strInvitation_email: itemData.strInvitation_email}}).then(function () {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().series_invitation_ID == itemData.series_invitation_ID) {
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                domItem.remove();
                            });
                        }
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.append = function () {
                        domItem.insertBefore(domItemsList.find('.add-friend-card'));
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.init = function () {
                        domItem = domItemsList.find('.owner-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        bindData();
                        bindEvents();
                    }
                }
                var invite = function (emails) {
                    var ajaxData = {
                        action: 'invite',
                        id: series.series_ID,
                        emails: emails,
                    };
                    ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                        res.data.forEach(function (row) {
                            row.intSeriesOwner_role = 2;
                            var hdl = new ItemClass(row);
                            hdl.init();
                            itemHandles.push(hdl);
                            if (filterInvitationStatus == 'waiting') {
                                hdl.append();
                                filteredHandles.push(hdl);
                            }
                        });
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Your invitation are sent successfully',
                        });
                    });
                }
                var bindEvents = function () {
                    domCoOwner.find('.owner-switch').change(function () {
                        filterInvitationStatus = $(this).prop('checked') ? 'waiting' : 'accept';
                        applyFilter();
                    });
                    domCoOwner.find('.add-input').keydown(function (e) {
                        var code = e.which;
                        if (code === 13) {
                            e.preventDefault();
                            var v = $(this).val();
                            var emails = v.split(/\s*,+\s*/);
                            invite(emails);
                            $(this).val('');
                            return false;
                        }
                    });
                }
                var applyFilter = function () {
                    domItemsList.css('min-height', domItemsList.css('height'));
                    domItemsList.css('opacity', 0);
                    setTimeout(function () {
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        filteredHandles = itemHandles.filter(function (value) {
                            return value.data().strSeriesInvitation_status === filterInvitationStatus;
                        });
                        filteredHandles.forEach(function (value) {
                            value.append();
                        })
                        domItemsList.css('min-height', '0px');
                        domItemsList.css('opacity', 1);
                    }, 300);
                }
                this.addItem = function (row) {
                    var hdl = new ItemClass(row);
                    hdl.init();
                    itemHandles.push(hdl);
                    if (filterInvitationStatus == 'waiting') {
                        hdl.append();
                        filteredHandles.push(hdl);
                    }
                }
                this.init = function () {
                    domCoOwner = domPane.find('.co-owners-wrapper');
                    domItemsList = domCoOwner.find('.owners-container');
                    seriesOwners.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        itemHandles.push(hdl);
                        filteredHandles.push(hdl);
                    });
                    invitations.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        itemHandles.push(hdl);
                    });
                    bindEvents();
                }
            }

            var bindEvents = function () {
                domPane.find('.invite-friend-form').submit(function () {
                    var v = $(this).find('[name="emails"]').val();
                    var emails = v.split(/\s*,+\s*/);
                    invite(emails);
                    return false;
                });
            }
            var invite = function (emails) {
                var ajaxData = {
                    action: 'invite',
                    id: series.series_ID,
                    emails: emails,
                };
                ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                    res.data.forEach(function (row) {
                        row.intSeriesOwner_role = 2;
                        coOwnersHandle.addItem(row);
                    });
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Your invitation are sent successfully',
                    }).then(function (value) {
                    });
                });
            }
            this.init = function () {
                domPane = domPage.find('.step-invite-pane');
                domPane.parent().find('.preview-series-link').attr('href', 'preview_series?id=' + series.series_ID);
                domPane.parent().find('.edit-series-link').attr('href', 'edit_series?id=' + series.series_ID);
                coOwnersHandle = new CoOwnersClass();
                coOwnersHandle.init();
                bindEvents();
            }
        }

        var StepShareClass = function () {

            var domPane;
            var socialLinks = function () {
                var viewUri = encodeURIComponent(BASE_URL + '/preview_series?id=' + series.series_ID);
                var title = encodeURIComponent(series.strSeries_title);
                var subject = encodeURIComponent('Shared from Walden.ly - ' + series.strSeries_title);
                return {
                    facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + viewUri + '&title=' + title,
                    twitter: 'https://twitter.com/intent/tweet?url=' + viewUri,
                    google: 'https://plus.google.com/share?url=' + viewUri,
                    email: 'mailto:?subject=' + subject + '&body=' + encodeURIComponent('I thought you might like this: ') + viewUri,
                    linkedin: 'https://www.linkedin.com/shareArticle?mini=true&url='+ viewUri +'&title='+ title +'&summary='+ subject +'&source='
                };
            }
            var bindData = function () {
                var sLinks = socialLinks();
                domPane.find('.share-item.item-facebook').attr('href', sLinks.facebook);
                domPane.find('.share-item.item-twitter').attr('href', sLinks.twitter);
                domPane.find('.share-item.item-google').attr('href', sLinks.google);
                domPane.find('.share-item.item-email').attr('href', sLinks.email);
                domPane.find('.share-item.item-linkedin').attr('href', sLinks.linkedin);
            }

            this.init = function () {
                domPane = domPage.find('.step-share-pane');
                domPane.parent().find('.preview-series-link').attr('href', 'preview_series?id=' + series.series_ID);
                domPane.parent().find('.edit-series-link').attr('href', 'edit_series?id=' + series.series_ID);
                bindData();
            }
        }

        var Step0MembershipChargeClass = function () {
            var domPane;
            var bindEvents = function () {
                domPane.find('.btn-yes').click(function () {
                    membershipCharge().then(function () {
                        step0ChargeTypeHandle.tabShow();
                    });
                });
                domPane.find('.btn-no').click(function () {
                    membershipUnCharge().then(function () {
                        step3CreateSeriesHandle.tabShow();
                    });
                });
            }
            var membershipCharge = function () {
                return step3CreateSeriesHandle.getId().then(function (id) {
                    return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: id}, sets: {boolSeries_charge: 1}});
                });
            }
            var membershipUnCharge = function () {
                return step3CreateSeriesHandle.getId().then(function (id) {
                    return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: id}, sets: {boolSeries_charge: 0}});
                });
            }
            this.init = function () {
                domPane = domPage.find('.step-0-membership-charge-pane');
                bindEvents();
            }
        };
        var Step0ChargeTypeClass = function () {
            var domPane;
            var bindEvents = function () {
                domPane.find('.btn-onetime').click(function () {
                    onetimeCharge().then(function () {
                        step0OnetimeValueHandle.tabShow();
                    });
                });
                domPane.find('.btn-monthly').click(function () {
                    membershipCharge().then(function () {
                        step0MembershipValueHandle.tabShow();
                    });
                });
            }
            var membershipCharge = function () {
                return step3CreateSeriesHandle.getId().then(function (id) {
                    return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: id}, sets: {strSeries_charge_type: 'monthly'}});
                });
            }
            var onetimeCharge = function () {
                return step3CreateSeriesHandle.getId().then(function (id) {
                    return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: id}, sets: {strSeries_charge_type: 'onetime'}});
                });
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-0-charge-type"]').tab('show');
            }
            this.init = function () {
                domPane = domPage.find('.step-0-charge-type-pane');
                bindEvents();
            }
        };
        var Step0MembershipValueClass = function () {
            var domPane;

            var bindEvents = function () {
                domPane.find('.next-wrap .btn-yes').click(function () {
                    saveValue().then(function () {
                        // step0AffiliateChargeHandle.tabShow();
                        step0StripeConnectHandle.tabShow();
                    });
                });
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-0-membership-value"]').tab('show');
            }
            var saveValue = function () {
                var v = domPane.find('[name="charge_per_month"]').val();
                step0AffiliateValueHandle.bootstrapSliderWith(v);
                return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: series.series_ID}, sets: {intSeries_price: v}});
            }
            this.init = function () {
                domPane = domPage.find('.step-0-membership-value-pane');
                domPane.find('[name="charge_per_month"]').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 45,
                    value: 20,
                });
                bindEvents();
            }
        }
        var Step0OnetimeValueClass = function () {
            var domPane;

            var bindEvents = function () {
                domPane.find('.next-wrap .btn-yes').click(function () {
                    saveValue().then(function () {
                        // step0AffiliateChargeHandle.tabShow();
                        step0StripeConnectHandle.tabShow();
                    });
                });
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-0-onetime-value"]').tab('show');
            }
            var saveValue = function () {
                var v = domPane.find('[name="intSeries_price"]').val();
                step0AffiliateValueHandle.bootstrapSliderWith(v);
                return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: series.series_ID}, sets: {intSeries_price: v}});
            }
            this.init = function () {
                domPane = domPage.find('.step-0-onetime-value-pane');
                domPane.find('[name="intSeries_price"]').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 45,
                    value: 20,
                });
                bindEvents();
            }
        }
        var Step0AffiliateChargeClass = function () {
            var domPane;
            var bindEvents = function () {
                domPane.find('.btn-yes').click(function () {
                    affiliateCharge().then(function () {
                        step0AffiliateValueHandle.tabShow();
                    });
                });
                domPane.find('.btn-no').click(function () {
                    affiliateUnCharge().then(function () {
                        step3CreateSeriesHandle.tabShow();
                    });
                });
            }
            var affiliateCharge = function () {
                return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: series.series_ID}, sets: {boolSeries_affiliated: 1}});
            }
            var affiliateUnCharge = function () {
                return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: series.series_ID}, sets: {boolSeries_affiliated: 0}});
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-0-affiliate-charge"]').tab('show');
            }
            this.init = function () {
                domPane = domPage.find('.step-0-affiliate-charge-pane');
                bindEvents();
            }
        };
        var Step0AffiliateValueClass = function () {
            var domPane;
            var chargeValue = 30;
            var youV, affiliateV = 0;
            var self = this;

            var bindEvents = function () {
                domPane.find('[name="charge_per_conversion"]').change(function (e) {
                    affiliateV = e.value.newValue;
                    youV = chargeValue - affiliateV;
                    bindEarnValue();
                });
                domPane.find('.next-wrap .btn-yes').click(function () {
                    saveValue().then(function () {
                        step0StripeConnectHandle.tabShow();
                    });
                });
            }
            var bindEarnValue = function () {
                domPane.find('.you-value').html('$' + youV);
                domPane.find('.affiliate-value').html('$' + affiliateV);
            }
            var saveValue = function () {
                return ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: {series_ID: series.series_ID}, sets: {intSeries_affiliate_price: affiliateV}});
            }
            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-0-affiliate-value"]').tab('show');
            }
            this.bootstrapSliderWith = function (maxV) {
                chargeValue = parseInt(maxV);
                affiliateV = 6;
                youV = chargeValue - affiliateV;
                domPane.find('[name="charge_per_conversion"]').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: maxV,
                    value: affiliateV,
                });
                bindEarnValue();
            }
            this.init = function () {
                domPane = domPage.find('.step-0-affiliate-value-pane');
                bindEvents();
            }
        }
        var Step0StripeConnectClass = function () {
            var domPane;

            this.tabShow = function () {
                domVirtualLinks.find('[href="#step-0-stripe-connect"]').tab('show');
            }
            this.init = function () {
                domPane = domPage.find('.step-0-stripe-connect-pane');
            }
        }
        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            domPageHeader = domPage.find('.content-header');
            domVirtualLinks = domPage.find('.virtual-links');

            step0MembershipChargeHandle = new Step0MembershipChargeClass();
            step0MembershipChargeHandle.init();

            step0ChargeTypeHandle = new Step0ChargeTypeClass();
            step0ChargeTypeHandle.init();

            step0MembershipValueHandle = new Step0MembershipValueClass();
            step0MembershipValueHandle.init();

            step0OnetimeValueHandle = new Step0OnetimeValueClass();
            step0OnetimeValueHandle.init();

            step0AffiliateChargeHandle = new Step0AffiliateChargeClass();
            step0AffiliateChargeHandle.init();

            step0AffiliateValueHandle = new Step0AffiliateValueClass();
            step0AffiliateValueHandle.init();

            step0StripeConnectHandle = new Step0StripeConnectClass();
            step0StripeConnectHandle.init();

            step3CreateSeriesHandle = new Step3CreateSeriesClass();
            step3CreateSeriesHandle.init();

            step1SelectFromHandle = new Step1SelectFromClass();
            step1SelectFromHandle.init();

            step1AddFromScratchHandle = new Step1AddFromScratchClass();
            step1AddFromScratchHandle.init();

            step1AddFromSearchHandle = new Step1AddFromSearchClass();
            step1AddFromSearchHandle.init();

            step1AddFromUploadHandle = new Step1AddFromUploadClass();
            step1AddFromUploadHandle.init();

            step2ArrangeHandle = new Step2ArrangeClass();
            step2ArrangeHandle.init();

            step4SelectSetHandle = new Step4SelectSetClass();
            step4SelectSetHandle.init();

            stepChargeHandle = new StepChargeClass();
            stepInviteHandle = new StepInviteClass();
            stepShareHandle = new StepShareClass();
            paneHistories.push('#step-0-membership-charge');
            bindEvents();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();
