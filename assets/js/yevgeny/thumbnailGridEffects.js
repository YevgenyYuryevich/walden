(function() {
	'use strict';
	// some dummy data to play with..

    var allImages, current_page_type;

    var series_pages = [], current_series_page_key = 'page1';
    var categories_pages = [], current_category_page_key = 'page1';

	$.ajax({
		url: '../../../yevgeny/action.php',
        data: {action: 'getCategories'},
		success: function (rlt) {
			allImages = rlt;
			categories_pages = rlt;
			current_category_page_key = 'page1';
            current_page_type = 'categories';
        },
        async: false,
		type: 'POST',
        dataType: 'json'
	});
	// http://coveroverflow.com/a/11381730/989439
	function mobilecheck() {
		var check = false;
		(function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	}

	var animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		// animation end event name
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		// event type (if mobile use touch events)
		eventtype = mobilecheck() ? 'touchstart' : 'click',
		// support for css animations
		support = Modernizr.cssanimations;

	function onAnimationEnd( elems, len, callback ) {
		var re_len = 0;
		elems.forEach(function (el) {
			if (el.hasChildNodes()){
				re_len++;
			}
        });
		if (re_len === 0){
		    callback.call();
        }
		var finished = 0,
			onEndFn = function() {
				this.removeEventListener( animEndEventName, onEndFn );
                ++finished;
				if( finished === re_len ) {
					callback.call();
				}
			};
		elems.forEach( function( el,i ) {
			if (el.querySelector('a') !== null){
                el.querySelector('a').addEventListener( animEndEventName, onEndFn );
			}
		});
	}

	function init() {
		[].forEach.call( document.querySelectorAll( '.tt-grid-wrapper' ), function( el ) {

			var grid = el.querySelector( '.tt-grid' ),
				items = [].slice.call( grid.querySelectorAll( 'li' ) ),
				navDots = [].slice.call( el.querySelectorAll( 'nav > a' ) ),
				isAnimating = false,
				current = 0;

			$('button.back-to-categories',el).click(function () {

                if (current_page_type == 'categories' || isAnimating === true){ return false; }
                loadNewSet('open_categories', current_category_page_key);
                current_series_page_key = 'page1';
            });

            $('.next-page', el).click(function () {
                if (isAnimating){ return ; }
                if (current_page_type == 'categories'){
                    var next_index = 'page' + (current_category_page_key.substr(4)*1 + 1);
                    if (typeof categories_pages[next_index] == 'undefined'){
                        return false;
                    }
                    loadNewSet('pagination', categories_pages[next_index]);
                    current_category_page_key = next_index;
                }
                else if (current_page_type == 'series'){
                    var next_index = 'page' + (current_series_page_key.substr(4)*1 + 1);
                    if (typeof series_pages[next_index] == 'undefined'){
                        return false;
                    }
                    loadNewSet('pagination', series_pages[next_index]);
                    current_series_page_key = next_index;
                }
            });

            $('.prev-page', el).click(function () {
                if (isAnimating){ return ; }
                if (current_page_type == 'categories'){
                    var pre_index = 'page' + (current_category_page_key.substr(4)*1 - 1);
                    if (typeof categories_pages[pre_index] == 'undefined'){
                        return false;
                    }
                    loadNewSet('pagination', categories_pages[pre_index]);
                    current_category_page_key = pre_index;
                }
                else if (current_page_type == 'series'){
                    var pre_index = 'page' + (current_series_page_key.substr(4)*1 - 1);
                    if (typeof series_pages[pre_index] == 'undefined'){
                        return false;
                    }
                    loadNewSet('pagination', series_pages[pre_index]);
                    current_series_page_key = pre_index;
                }
            });
			navDots.forEach( function( el, i ) {
				el.addEventListener( eventtype, function( ev ) {
					// if( isAnimating || current === i ) return false; // prev
					ev.preventDefault();

					updateCurrent( i );
					loadNewSet('page_nation', i );
				} );
			} );
			items.forEach(function (el, i) {
                if (items[i].querySelector('a:not(.tt-old) .object_button') !== null){
                    items[i].querySelector('a:not(.tt-old) .object_button').addEventListener(eventtype, function (el) {
                        loadNewSet('open_series', el.target.dataset.id);
                    });
                }
            });
			function updateCurrent( set ) {
				classie.remove( navDots[ current ], 'tt-current' );
				classie.add( navDots[ set ], 'tt-current' );
				current = set;
			}
			// this is just a way we can test this. You would probably get your images with an AJAX request...
			function loadNewSet(action, set ) {
			    if (isAnimating === true){
			        return ;
                }
                isAnimating = true;
				var newImages = allImages.page1;
				if (action === 'pagination'){
                    newImages = set;
                }
                else if (action == 'open_categories'){
				    newImages = categories_pages[set];
                    current_page_type = 'categories';
                    setupPaginationItems();
                }
                else if (action === 'open_series'){
					document.getElementsByClassName('loading')[0].style.display = 'block';
					setTimeout(function () {
                        $.ajax({
                            url: '../../../yevgeny/action.php',
                            data: {action: 'getSeries', category_id: set},
                            success: function (rlt) {
                                series_pages = rlt;
                                if (typeof rlt.page1 !== 'undefined'){
                                    newImages = rlt.page1;
                                }
                                else{
                                    newImages = false;
                                }
                                document.getElementsByClassName('loading')[0].style.display = 'none';
                                current_page_type = 'series';
                                setupPaginationItems();
                            },
                            async: false,
                            type: 'POST',
                            dataType: 'json'
                        })
                    });
				}
				else if(action === 'refresh_series'){
                    if(typeof set !== 'undefined'){
                        newImages = set;
                    }
                    else {
                        newImages = [];
                    }
                    setupPaginationItems();
                }
				items.forEach( function( el ) {
					var itemChild = el.querySelector( 'a' );
					// add class "tt-old" to the elements/images that are going to get removed
					if( itemChild ) {
						classie.add( itemChild, 'tt-old' );
					}
				} );

				// apply effect
				setTimeout( function() {

					// append new elements
					if(newImages){

                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el;
                            if (items[i].querySelector('a:not(.tt-old) .object_button') !== null){
                                items[i].querySelector('a:not(.tt-old) .object_button').addEventListener(eventtype, function (el) {
                                    loadNewSet('open_series', el.target.dataset.id);
                                });
                            }
                            addFuncToPreview(items[i]);
                            addFuncToJoinBtn(items[i]);
                            addFuncToTrashBtn(items[i], loadNewSet);
                        } );
                    }

					// add "effect" class to the grid
					classie.add( grid, 'tt-effect-active' );

					// wait that animations end
					var onEndAnimFn = function() {
						// remove old elements
						items.forEach( function( el ) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
							// remove class "tt-empty" from the empty items
							classie.remove( el, 'tt-empty' );
							// now apply that same class to the items that got no children (special case)
							if ( !el.hasChildNodes() ) {
								classie.add( el, 'tt-empty' );
							};
						} );
						// remove the "effect" class
						classie.remove( grid, 'tt-effect-active' );
						isAnimating = false;
					};

					if( support ) {

						onAnimationEnd( items, items.length, onEndAnimFn );
					}
					else {
						onEndAnimFn.call();
					}
				}, 25 );
			}
            var setupPaginationItems = function () {
                var items_wrapper = $('.pagination .page-items-wrapper');
                items_wrapper.empty();

                // var pages_len = typeof current_group !== 'undefined' ? current_group.pages.length : 0;
                var pages_len = 0;

                if (current_page_type === 'categories'){
                    for (var key in categories_pages){
                        if (categories_pages.hasOwnProperty(key)) {
                            pages_len++;
                        }
                    }

                    if (pages_len > 0){
                        items_wrapper.append('<a href="javascript:;" data-page_index="'+ 1 +'">' + (1 + 0) + '</a>');

                        for(var i = 1; i < pages_len; i ++){
                            items_wrapper.append('|<a href="javascript:;" data-page_index="'+ (i + 1) +'">' + (1 + i) + '</a>');
                        }
                    }
                }
                else {
                    for (var key in series_pages){
                        if (series_pages.hasOwnProperty(key)) {
                            pages_len++;
                        }
                    }

                    if (pages_len > 0){
                        items_wrapper.append('<a href="javascript:;" data-page_index="'+ 1 +'">' + (1 + 0) + '</a>');

                        for(var i = 1; i < pages_len; i ++){
                            items_wrapper.append('|<a href="javascript:;" data-page_index="'+ (i + 1) +'">' + (1 + i) + '</a>');
                        }
                    }
                }
                addFuncToPaginationItems();
            }

            var addFuncToPaginationItems = function () {
                var items_wrapper = $('.pagination .page-items-wrapper');
                $('a', items_wrapper).click(function () {
                    var page_index = $(this).data('page_index');
                    var pre_page_index;
                    if (isAnimating){ return ; }


                    if (current_page_type == 'categories'){
                        if (page_index == current_category_page_key.substr(4)*1){
                            return ;
                        }

                        pre_page_index = 'page' + page_index;
                        loadNewSet('pagination', categories_pages[pre_page_index]);
                        current_category_page_key = pre_page_index;
                    }
                    else{
                        if (page_index == current_series_page_key.substr(4)*1){
                            return ;
                        }
                        pre_page_index = 'page' + page_index;
                        loadNewSet('pagination', series_pages[pre_page_index]);
                        current_series_page_key = pre_page_index;
                    }
                });
            };
            setupPaginationItems();

		} );
		var addFuncToPreview = function (container) {
            if (container.querySelector('a:not(.tt-old) .action_button [data-link = "preview"]') !== null){
                container.querySelector('a:not(.tt-old) .action_button [data-link = "preview"]').addEventListener(eventtype, function (el) {
                    $('.loading').css('display', 'block');
                	$.ajax({
                        url: '../../../yevgeny/action.php',
                        data: {action: 'previewSeries', series_id: $(this).data('series_id')},
                        start: function () {
                        },
                        success: function (rlt) {
                            $('.preview-rightbox .preview-lightbox-wrapper .preview-lightbox-content').html(rlt);
                            $('.loading').css('display', 'none');
                            addFuncToLightBoxWhenOpen(container);
                        },
                        type: 'POST'
					});
                });
            }
        };
		var addFuncToLightBoxWhenOpen = function (container) {
            $(container).addClass('preview-state');
            setTimeout(function () {
                $('.preview-rightbox').css('display', 'block');
                var pos = $(container).offset();
                pos.top = pos.top - $(window).scrollTop();
                var width = $(container).outerWidth();
                var height = $(container).outerHeight();
                pos.right = $(window).width() - pos.left - width;
                pos.bottom = $(window).height() - pos.top - height;
                $('.preview-rightbox .preview-lightbox-wrapper').css(pos);
                $('.preview-rightbox .preview-lightbox-wrapper').animate({
                    top: '5vh',
                    right: '10vw',
                    left: '10vw',
                    bottom: '5vh',
                    fontSize: '10px'
                }, {
                    duration: 500,
                    complete: function () {
                        addAnimationToLightBoxContent($('.preview-rightbox .preview-lightbox-wrapper .preview-lightbox-content'));
                        addFuncToCloseLightBoxBtn($('.preview-rightbox .preview-lightbox-wrapper'), container);
                    }
                });
            }, 500);
        };
		var addFuncToCloseLightBoxBtn = function (light_box, container) {
            $('.close', light_box).off('click').on('click', function () {
                var pos = $(container).offset();
                pos.top = pos.top - $(window).scrollTop();
                var width = $(container).outerWidth();
                var height = $(container).outerHeight();
                pos.right = $(window).width() - pos.left - width;
                pos.bottom = $(window).height() - pos.top - height;
                var css = $.extend(pos, {opacity: 0, fontSize: '5px'});
                $('.preview-rightbox .preview-lightbox-wrapper').animate(
                    css,
                    {
                        duration: 500,
                        start: function () {
                        },
                        complete: function () {
                            $('.preview-rightbox .preview-lightbox-wrapper').removeAttr('style');
                            $('.preview-rightbox').css('display', 'none');
                            $(container).removeClass('preview-state');
                            $(container).addClass('restore-series-state');
                            $(container).bind(animEndEventName, function () {
                                $(container).removeClass('restore-series-state');
                            })
                        }
                    }
                )
            });
        };
        var addAnimationToLightBoxContent = function (lightbox_content) {
            initVars();
            listeScroll(lightbox_content);
            checkVisibility();
            sliderControlls();
            autoSlider();
            $('article', lightbox_content).readingTime({
                wordCountTarget: '.words',
                wordsPerMinute: 200,
            });
        };
        var addFuncToJoinBtn = function (container) {
            var timer = null;
            var self = $("a:not(.tt-old) .action_button [data-link = 'join'] button", container);
            var clicked = false;
            if (container.querySelector('a:not(.tt-old) .action_button [data-link = "join"]') !== null){
                container.querySelector('a:not(.tt-old) .action_button [data-link = "join"]').addEventListener(eventtype, function (el) {
                    if (clicked){
                        alert('you are already joined');
                        return false;
                    }
                    var clicked_container = $(container.querySelector('a:not(.tt-old) .action_button [data-link = "join"]'));
                    self.removeClass("filled");
                    self.addClass("circle");
                    self.html("");
                    $(".wrap  svg", clicked_container).css("display", "block");
                    $(".circle_2", clicked_container).attr("class", "circle_2 fill_circle");
                    var series_id = $(this).data('series_id');

                    timer = setInterval(
                        function tick() {
                            if(clicked){
                                self.removeClass("circle");
                                self.addClass("filled");
                                // self.html("Joined");
                                $(".wrap img", clicked_container).css("display", "block");
                                $("svg", clicked_container).css("display", "none");
                                clearInterval(timer);

                                /* update series_pages */
                                if (typeof series_pages[current_series_page_key] !== 'undefined'){
                                    series_pages[current_series_page_key].forEach(function (v, key, arr) {
                                        if(v.indexOf('data-series_id = "'+ series_id +'"') !== -1){
                                            v = v.replace('<button type="submit">Join</button>', '<button type="submit" class = "filled">Join</button>');
                                            v = v.replace('<img src=', '<img style = "display: block;" src=');
                                            arr[key] = v;
                                            return ;
                                        }
                                    });
                                }
                            }
                        }, 2000);
                    $.ajax({
                        url: '../../../yevgeny/action.php',
                        data: {action: 'joinSeries', series_id: series_id},
                        start: function () {
                        },
                        success: function (rlt) {

                            if (rlt == 'no_access'){
                                swal(
                                    'You have no access',
                                    'Please login first?'
                                );
                                self.removeAttr('class');
                                self.html("join");
                                $(".wrap  svg", clicked_container).css("display", "none");
                                $(".circle_2", clicked_container).attr("class", "");
                                return false;
                            }
                            else if (rlt == true){
                                clicked = true;
                            }
                            else{
                                swal(
                                    'Oops...',
                                    'Something went wrong! Try again later',
                                    'error'
                                );
                                self.removeAttr('class');
                                self.html("join");
                                $(".wrap  svg", clicked_container).css("display", "none");
                                $(".circle_2", clicked_container).attr("class", "");
                            }
                        },
                        type: 'POST',
                        dataType: 'json'
                    });
                });
            }
        };
        var addFuncToTrashBtn = function (container, loadNewSet) {
            if (container.querySelector('a:not(.tt-old) .action_button [data-link = "trash"]') !== null){
                container.querySelector('a:not(.tt-old) .action_button [data-link = "trash"]').addEventListener(eventtype, function (el) {
                    var category_id = $('>a', container).data('category_id');
                    var series_id = $(this).data('series_id');
                    $('.loading').css('display', 'block');
                    $.ajax({
                        url: '../../../yevgeny/action.php',
                        data: {action: 'trashSeries', series_id: series_id, category_id: category_id},
                        start: function () {
                        },
                        success: function (series) {
                            $('.loading').css('display', 'none');
                            if (series == 'no_access'){
                                swal(
                                    'You have no access',
                                    'Please login first?'
                                );
                                return false;
                            }
                            else if (series === false){
                                swal(
                                    'Oops...',
                                    'Something went wrong! Try again later',
                                    'error'
                                );
                            }
                            else{
                                $(container).addClass('trash-state');
                                setTimeout(function () {
                                    $(container).html('');
                                    $(container).addClass('tt-empty');
                                    $(container).removeClass('trash-state');
                                    loadNewSet('refresh_series', series.page1);
                                    series_pages = series;
                                    current_series_page_key = 'page1';
                                }, 1000);
                            }
                        },
                        type: 'POST',
                        dataType: 'json'
                    });
                });
            }
        }
	}
	init();
})();