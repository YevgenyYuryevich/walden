(function () {
    'use strict';

    var PageClass = function () {

        var previewHandle, codePenHandle, layoutHandle;

        var PreviewClass = function () {

            var domRoot;

            this.setCode = function (code) {
                domRoot.find('.item-content').empty();
                domRoot.find('.item-content').append(code);
            }
            this.dom = function () {
                return domRoot;
            }
            this.init = function () {
                domRoot = $('section.preview-embed');
            }
        }

        var CodePenClass = function () {
            var domRoot;

            var editorHandle;

            var bindEvents = function () {
                editorHandle.onReload(function (embedCode) {
                    previewHandle.setCode(embedCode);
                });
            }

            this.setHtml = function (html) {
                editorHandle.setHtml(html);
            }
            this.setCss = function (css) {
                editorHandle.setCss(css);
            }
            this.setJs = function (js) {
                editorHandle.setJs(js);
            }
            this.reloadPreview = function () {
                editorHandle.reloadPreview();
            }
            this.init = function () {
                domRoot = $('section.code-embed');
                editorHandle = new EmbedEditorClass(domRoot.find('.component.embed-editor'));
                editorHandle.init();
                bindEvents();
            }
        }

        var LayoutClass = function () {
            var self = this;

            var domRoot;

            var selectedLayout;

            var trackedEmbedIds = [];
            var newEmbedCnt = 0;
            var clicked = false;
            var receiveEmbedMessage = function(e) {
                if ( clicked && e.data.embedId && e.data.name && e.data.name == 'currentHeight') {
                    if (trackedEmbedIds.includes(e.data.embedId)) {
                        return ;
                    }
                    else {
                        trackedEmbedIds.push(e.data.embedId);
                        newEmbedCnt++;
                        if (newEmbedCnt === 2) {
                            $('.loading').hide();
                            newEmbedCnt = 0;
                            domRoot.find('.go-to-preview').addClass('active');
                        }
                    }
                }
            }

            var bindEvents = function () {
                domRoot.find('[data-layout]').click(function () {
                    var embedCode = self.select($(this).data('layout'));
                    $('.loading').show();
                    setTimeout(function () {
                        codePenHandle.setHtml(embedCode.html);
                        codePenHandle.setCss(embedCode.css);
                        codePenHandle.setJs(embedCode.js);
                        codePenHandle.reloadPreview();
                        clicked = true;
                    }, 100);
                });
                window.addEventListener("message", receiveEmbedMessage);
                domRoot.find('.go-to-preview').click(function () {
                    $('html').animate({scrollTop: previewHandle.dom().offset().top + 'px'}, 400);
                    $(this).removeClass('active');
                });
            }
            var generateEmbedCode = function (url, title, des) {
                var embedCode = '<blockquote class="walden-embedly-card">\n' +
                    '    <h4>\n' +
                    '        <a href="'+ url +'">'+ title +'</a>\n' +
                    '    </h4>\n' +
                    '    <p>' + des + '</p>\n' +
                    '</blockquote>\n' +
                    '<script async src="'+ BASE_URL +'/assets/js/yevgeny/embed-platform.js?version=1" charset="UTF-8"></script>';
                return embedCode.replace('&amp;', '&');
            }
            this.select = function (layoutType) {
                selectedLayout = layoutType;
                domRoot.find('.layout-item').removeClass('selected');
                domRoot.find('[data-layout="'+ selectedLayout +'"]').parents('.layout-item').addClass('selected');
                var viewVersion = 'right-panel';
                var css = '';
                var js = '';
                var html = '';
                switch (selectedLayout) {
                    case 'grid':
                        viewVersion = 'grid';
                        var url = BASE_URL + '/viewexperience_embed?viewVersion=grid&id='+ series.series_ID +'&postViewVersion=popup';
                        if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                            url += '&affiliate_id=' + AFFILIATE_ID;
                        }
                        html = '<blockquote class="walden-embedly-card">\n' +
                            '    <h4>\n' +
                            '<!-- set postViewVersion with one of popup, turbotax and default or you can set on Javascript Tab-->\n' +
                            '        <a href="'+ url +'">'+ series.strSeries_title +'</a>\n' +
                            '    </h4>\n' +
                            '    <p>'+ series.strSeries_description +'</p>\n' +
                            '</blockquote>\n' +
                            '<script async src="'+ BASE_URL + '/assets/js/yevgeny/embed-platform.js?version=1" charset="UTF-8"></script>';

                        css = '/* content wrapper */\n' +
                            'html body .site-wrapper main.main-content{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* top left welcome back */\n' +
                            'html body .site-wrapper main.main-content .content-inner .content-header .welcome{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid wrapper */\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid{\n' +
                            '    \n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid item */\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li{\n' +
                            '    \n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid item inner*/\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid item title*/\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .title-wrapper .item-title{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid item button wrapper*/\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .buttons-wrapper{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid item button*/\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .buttons-wrapper .btn.btn-circle{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* grid item left bottom line*/\n' +
                            'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner > aside .bottom-line{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* nav footer */\n' +
                            'html body .site-wrapper main.main-content .content-inner footer.content-footer{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer nav(back, continue) wrapper*/\n' +
                            'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer nav back*/\n' +
                            'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper .step-back{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer nav continue*/\n' +
                            'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper .step-next{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer powered by*/\n' +
                            'html body .site-wrapper main.main-content .content-inner footer.content-footer .powered-by{\n' +
                            '    \n' +
                            '}\n';
                        js = '// you can set range 1 ~ 5;\n' +
                            'const COLS_COUNT = 5;\n' +
                            '\n' +
                            '// you can set range 1+;\n' +
                            'const ROWS_COUNT = 2;\n' +
                            '\n' +
                            '// you can set one of popup, turbotax, default\n' +
                            '// var postViewVersion = \'popup\';';
                        break;
                    case 'list':
                        viewVersion = 'popup';
                        css = '/* site wrapper */\n' +
                            '.walden-site .site-wrapper {\n' +
                            '    background: white;\n' +
                            '}\n' +
                            '\n' +
                            '/* left panel */\n' +
                            '.walden-site .site-wrapper .left-panel{\n' +
                            '    background-color: white;\n' +
                            '}\n' +
                            '\n' +
                            '/* top left welcome back message */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .welcome{\n' +
                            '    color: inherit;\n' +
                            '}\n' +
                            '\n' +
                            '/* top left login btn */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .login-btn {\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* left panel tree list */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* left panel tree list row */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* left panel tree list row meta info*/\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row .post-meta-info{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* popup widget */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .view-widget{\n' +
                            '    background-color: transparent;\n' +
                            '}';
                        break;
                    case 'guided':
                        viewVersion = "right-panel";
                        css = '/* site wrapper */\n' +
                            '.walden-site .site-wrapper {\n' +
                            '    background: white;\n' +
                            '}\n' +
                            '\n' +
                            '/* left panel */\n' +
                            '.walden-site .site-wrapper .left-panel{\n' +
                            '    background-color: white;\n' +
                            '}\n' +
                            '\n' +
                            '/* top left welcome back message */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .welcome{\n' +
                            '    color: inherit;\n' +
                            '}\n' +
                            '\n' +
                            '/* top left login btn */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .login-btn {\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* left panel tree list */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* left panel tree list row */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* left panel tree list row meta info*/\n' +
                            '\n' +
                            '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row .post-meta-info{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '\n' +
                            '/* right panel */\n' +
                            '\n' +
                            '.walden-site .site-wrapper .right-panel{\n' +
                            '    background-color: #f8f7f9;\n' +
                            '}\n' +
                            '\n' +
                            '/* top right saved money wrapper */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* top right saved money amount */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved .money-amount{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* top right saved money text */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved .w-name{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel blog wrapper */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel blog image wrapper */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-image-wrapper{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel blog image */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-image-wrapper .post-img{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel blog title */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-title{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel blog body (description) */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-body{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer */\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer nav(back, continue) wrapper*/\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer nav back*/\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper .step-back{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer nav continue*/\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper .step-next{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* right panel footer powered by*/\n' +
                            '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .powered-by{\n' +
                            '    \n' +
                            '}';
                        break;
                    case 'gallery':
                        css = '/* content wrapper */\n' +
                            'html body .site-wrapper .main-content{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* top left welcome back */\n' +
                            'html body .site-wrapper .main-content .content-inner .content-header .welcome{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* gallery list wrapper */\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* gallery item */\n' +
                            '\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* gallery item inner*/\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* gallery item header */\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* gallery item header day*/\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header .day-wrapper{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '/* gallery item header title*/\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header .post-title{\n' +
                            '    \n' +
                            '}\n' +
                            '\n' +
                            '\n' +
                            '/* gallery item body(description)*/\n' +
                            'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-body{\n' +
                            '    \n' +
                            '}';
                        viewVersion = 'gallery';
                        break;
                }
                var url = BASE_URL + '/viewexperience_embed?viewVersion='+ viewVersion +'&id=' + series.series_ID;
                if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                    url += '&affiliate_id=' + AFFILIATE_ID;
                }
                html = html ? html : generateEmbedCode(url, series.strSeries_title, series.strSeries_description);
                return {html: html, css: css, js: js};
            }
            this.init = function () {
                domRoot = $('section.select-layout-type');
                bindEvents();
                var embedCode = self.select('guided');
                codePenHandle.setHtml(embedCode.html);
                codePenHandle.setCss(embedCode.css);
                codePenHandle.reloadPreview();
            }
        }

        this.init = function () {
            previewHandle = new PreviewClass();
            previewHandle.init();

            codePenHandle = new CodePenClass();
            codePenHandle.init();

            layoutHandle = new LayoutClass();
            layoutHandle.init();
        }
    }

    var pageHandle = new PageClass();
    pageHandle.init();
})();