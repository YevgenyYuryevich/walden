(function () {
    'use strict';

    var is_loading_controlled_in_local = 0;

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    };

    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('-');
    };

    String.prototype.stringToDate = function () {
        var yyyy = this.split('-')[0];
        var mm = this.split('-')[1] * 1 - 1;
        var dd = this.split('-')[2];
        return new Date(yyyy, mm, dd);
    };
    var userPageHandle;
    var UserPageClass = function () {
        var subscriptionsHandle, amazonProductsHandle, eventsSectionHandle, goalsSectionHandle;

        var SubscriptionsClass = function () {
            var domRoot = $('main.main-content');

            var series = initialSeries;

            var itemHandles = [];

            var audioCnt = 0;

            var itemClass = function (itemData) {
                var domItem, domHeader, domTitle, domBody, domUsedBody, domFooter, domCircleNavWrap, domVideo;
                var subscription;
                var metaData = [];
                var videoHandle = null;

                var self = this;
                var availableTypes = [0, 2, 8, 10];

                var PathClass = function (path) {
                    var domPath, pathNumber;

                    var bindEvents = function () {
                        domPath.click(function () {
                            select();
                        });
                    }

                    var select = function () {
                        ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'set_seek', purchasedId: itemData.purchased_ID, parentId: path.clientsubscription_ID}).then(function (res) {
                            if (res.status == true){
                                setSubscription(res.data);
                            }
                            else {
                                swal("Sorry!", "There is no post in this option that you clicked. You will go to next post.").then(function () {
                                    stepNext();
                                });
                            }
                        });
                    }

                    this.init = function () {

                        domPath = domUsedBody.find('.path-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUsedBody.find('.paths-list'));

                        pathNumber = domUsedBody.find('.paths-list > .path-col').length;

                        domPath.find('.img-wrapper img').attr('src', 'assets/images/global-icons/tree/path/'+ pathNumber +'.svg');


                        var displayTitle = path.strClientSubscription_title;
                        displayTitle = displayTitle.length > 20 ? displayTitle.substr(0, 20) + '...' : displayTitle.substr(0, 20);

                        var displayDes = path.strClientSubscription_body;
                        displayDes = displayDes.length > 20 ? displayDes.substr(0, 40) + '...' : displayDes.substr(0, 40);

                        domPath.find('.path-title').html(displayTitle);
                        domPath.find('.path-description').html(displayDes);
                        bindEvents();
                    }
                }
                var FormFieldClass = function (domField) {
                    var domValue;
                    var field, answer = false;

                    var bindEvents = function () {
                        switch (field.strFormField_type) {
                            case 'text':
                                domValue.blur(function () {
                                    updateAnswer();
                                });
                                break;
                            case 'checkbox':
                                domValue.find('input').change(function () {
                                    updateAnswer();
                                });
                                break;
                        }
                    }
                    var updateAnswer = function () {
                        var sets = {};
                        is_loading_controlled_in_local = true;
                        domField.addClass('status-loading');
                        switch (field.strFormField_type) {
                            case 'text':
                                sets.strFormFieldAnswer_answer = domValue.html();
                                break;
                            case 'checkbox':
                                sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                                break;
                        }
                        if (answer) {
                            ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                                domField.removeClass('status-loading');
                                domField.addClass('status-loading-finished');
                                domField.one(animEndEventName, function () {
                                    console.log('ani end');
                                    domField.removeClass('status-loading-finished');
                                });
                                answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                            });
                        }
                        else {
                            $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                            ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                                domField.removeClass('status-loading');
                                domField.addClass('status-loading-finished');
                                answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                                domField.one(animEndEventName, function () {
                                    console.log('ani end');
                                    domField.removeClass('status-loading-finished');
                                });
                            });
                        }
                        is_loading_controlled_in_local = false;
                    }
                    var bindData = function () {
                        itemData.formFields.forEach(function (formField) {
                            if (formField.formField_ID == domField.attr('data-form-field')) {
                                field = formField;
                            }
                        });
                        formFieldAnswers.forEach(function (formFieldAnswer) {
                            if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                answer = formFieldAnswer;
                            }
                        });
                        if (answer) {
                            switch (field.strFormField_type) {
                                case 'text':
                                    domValue.html(answer.strFormFieldAnswer_answer);
                                    break;
                                case 'checkbox':
                                    domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                                    break;
                            }
                        }
                    }
                    this.init = function () {
                        domValue = domField.find('.form-field-value');
                        domField.find('[data-toggle="popover"]').popover();
                        bindData();
                        bindEvents();
                    }
                }

                var makeBody = function () {
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    var type = parseInt(subscription.intClientSubscriptions_type);
                    type = availableTypes.indexOf(type) != -1 ? type : 'other';
                    metaData.type = type;
                    var newDomUsedBody;

                    if (subscription.strClientSubscription_nodeType == 'menu') {
                        domBody.find('.item-decoration-container .right-portion').addClass('disabled');
                        newDomUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        domUsedBody = newDomUsedBody;
                        domUsedBody.find('.menu-title').html(subscription.strClientSubscription_title);
                        domUsedBody.find('.menu-description').html(subscription.strClientSubscription_body);

                        subscription.paths.forEach(function (path) {
                            var pathHandle = new PathClass(path);
                            pathHandle.init();
                        });
                        domUsedBody.find('.paths-list').addClass('paths-count-' + subscription.paths.length);
                    }
                    else {
                        domBody.find('.item-decoration-container .right-portion').removeClass('disabled');
                        newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        switch (parseInt(type)){
                            case 0:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.audioplayer-tobe').attr('data-thumb', subscription.strClientSubscription_image);
                                domUsedBody.find('.audioplayer-tobe').attr('data-source', subscription.strClientSubscription_body);
                                domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                                var settings_ap = {
                                    disable_volume: 'off',
                                    disable_scrub: 'default',
                                    design_skin: 'skin-wave',
                                    skinwave_dynamicwaves: 'off'
                                };
                                dzsag_init('#audio-' + audioCnt, {
                                    'transition':'fade',
                                    'autoplay' : 'off',
                                    'settings_ap': settings_ap
                                });
                                audioCnt++;
                                break;
                            case 2:
                                domUsedBody = newDomUsedBody;
                                domVideo = domUsedBody.find('video');
                                if (subscription.body_apiType == 'youtube') {
                                    var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": subscription.strClientSubscription_body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                    domVideo.attr('data-setup', JSON.stringify(setup));
                                }
                                else {
                                    domVideo.append('<source src="'+ subscription.strClientSubscription_body +'" />');
                                }
                                videoHandle = videojs(domVideo.get(0), {
                                    height: domUsedBody.innerWidth() * 540 / 960,
                                });
                                break;
                            case 8:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.append(subscription.strClientSubscription_body);
                                break;
                            case 7:
                                domUsedBody = newDomUsedBody;
                                // domUsedBody.find('.img-wrapper').append('<img src="'+ subscription.strClientSubscription_image +'">');
                                domUsedBody.find('.item-description').append(subscription.strClientSubscription_body);
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var hdl = new FormFieldClass($(d));
                                    hdl.init();
                                });
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                break;
                            case 10:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.item-description').append(subscription.strClientSubscription_body);
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var hdl = new FormFieldClass($(d));
                                    hdl.init();
                                });
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                break;
                            default:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.item-description').append(subscription.strClientSubscription_body);
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var hdl = new FormFieldClass($(d));
                                    hdl.init();
                                });
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                break;
                        }
                    }
                }

                var stepNext = function () {
                    var ajaxData = {
                        action: 'step_next',
                        frequency: itemData.intSeries_frequency_ID,
                        purchasedId: itemData.purchased_ID,
                        id: subscription.clientsubscription_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                setSubscription(res.data);
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                var stepPrev = function () {
                    var ajaxData = {
                        action: 'step_prev',
                        frequency: itemData.intSeries_frequency_ID,
                        purchasedId: itemData.purchased_ID,
                        id: subscription.clientsubscription_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                setSubscription(res.data);
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                var makeFavorite = function () {
                    var ajaxData = {
                        action: 'make_favorite',
                        id: subscription.clientsubscription_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                subscription.favorite = res.data;
                                domFooter.find('.make-favorite').addClass('favorited');
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                var makeUnFavorite = function() {
                    var ajaxData = {
                        action: 'make_unFavorite',
                        id: subscription.favorite.favorite_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                subscription.favorite = false;
                                domFooter.find('.make-favorite').removeClass('favorited');
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                var trashPost = function () {
                    var ajaxData = {
                        action: 'trash_post',
                        id: subscription.clientsubscription_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                if (res.data){
                                    setSubscription(res.data);
                                }
                                else {
                                    itemHandles.forEach(function (itemHandle, i) {
                                        if (itemHandle.id() == self.id()){
                                            itemHandles.splice(i, 1);
                                        }
                                    });
                                    domItem.remove();
                                }
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                var removeSeries = function () {
                    var ajaxData = {
                        action: 'remove_series',
                        id: itemData.series_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                itemHandles.forEach(function (itemHandle, i) {
                                    if (itemHandle.id() == self.id()){
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                domItem.remove();

                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                var complete = function(){
                    var ajaxData = {
                        action: 'complete_subscription',
                        id: subscription.clientsubscription_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                subscription.isSubsCompleted = true;
                                domHeader.find('.toogle-complete').addClass('btn-success');
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                var unComplete = function () {
                    var ajaxData = {
                        action: 'unComplete_subscription',
                        id: subscription.clientsubscription_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajaxData,
                        success: function (res) {
                            if (res.status == true){
                                subscription.isSubsCompleted = false;
                                domHeader.find('.toogle-complete').removeClass('btn-success');
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                var viewPost = function () {
                    var cloneForm = $('<form action="view" method="post" target="_blank" hidden><input name="id" value="'+ subscription.clientsubscription_ID +'"><input name="from" value="subscription"/><input name="prevpage" value="userpage"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }

                var openSwitchPage = function () {
                    var postId = subscription.intClientSubscription_post_ID;
                    var subscriptionId = subscription.clientsubscription_ID;
                    var owner = itemData.intSeries_client_ID == CLIENT_ID ? 'me' : 'other';
                    $('<form action="switchsubscription" method="post" target="_blank" hidden><input name="owner" value="'+ owner +'"/><input name="postId" value="'+ postId +'" /><input name="subscriptionId" value="'+ subscriptionId +'" /></form>').appendTo('body').submit().remove();
                }
                var openEditSeries = function(){
                    var $form = $('<form method="get" action="editseries" target="_blank"></form>');
                    $form.append('<input name="id" value="'+ itemData.series_ID +'"/>');
                    $form.hide();
                    $form.appendTo('body').submit();
                    $form.remove();
                }
                var openCalendar = function () {
                    var cloneForm = $('<form action="calendar" method="post" target="_blank" hidden><input name="series_id" value="'+ itemData.series_ID +'"><input name="prevpage" value="userpage"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }

                var setSubscription = function (subs) {
                    itemData.susubscription = subscription = subs;
                    if (subscription.strClientSubscription_nodeType == 'menu') {
                        domTitle.html(itemData.strSeries_title + ' - Please choose from the following selections:');
                    }
                    else {
                        domTitle.html(itemData.strSeries_title + ' - ' + subscription.strClientSubscription_title);
                    }
                    subscription.isSubsCompleted ? domHeader.find('.toogle-complete').addClass('btn-success') : domHeader.find('.toogle-complete').removeClass('btn-success');
                    makeBody();
                    subscription.favorite ? domFooter.find('.make-favorite').addClass('favorited') : domFooter.find('.make-favorite').removeClass('favorited');
                    var socialLinks = self.socialLinks();
                    domCircleNavWrap.find('.facebook-link').attr('href', socialLinks.facebook);
                    domCircleNavWrap.find('.twitter-link').attr('href', socialLinks.twitter);
                    domCircleNavWrap.find('.google-link').attr('href', socialLinks.google);
                    domCircleNavWrap.find('.linkedin-link').attr('href', socialLinks.linkedIn);
                    domCircleNavWrap.find('.mail-link').attr('href', socialLinks.email);
                }

                var bindEvents = function () {
                    domFooter.find('.open-switch-page').click(function () {
                        openSwitchPage();
                    });
                    domBody.find('.item-decoration-container .next-post').click(function () {
                        stepNext();
                    });
                    domBody.find('.item-decoration-container .prev-post').click(function () {
                        stepPrev();
                    });
                    domFooter.find('.make-favorite').click(function () {
                        subscription.favorite ? makeUnFavorite() : makeFavorite();
                    });
                    domFooter.find('.view-post').click(function () {
                        viewPost();
                    });
                    domFooter.find('.open-calendar').click(function () {
                        openEditSeries();
                    });
                    domFooter.find('.trash-post').click(function () {
                        swal({
                            title: "Are you sure?",
                            icon: "warning",
                            dangerMode: true,
                            buttons: {
                                cancel: "Cancel!",
                                trashPost: {
                                    text: "Trash Post",
                                    value: "trash_post",
                                    className: 'swal-button--danger'
                                },
                                removeSeries: {
                                    text: 'Remove Series',
                                    value: 'remove_series',
                                    className: 'swal-button--danger'
                                },
                            },
                        }).then(function(value){
                            switch (value) {
                                case "trash_post":
                                    trashPost();
                                    break;
                                case "remove_series":
                                    removeSeries();
                                    break;
                                default:
                                    break;
                            }
                        });
                    })
                    domHeader.find('.toogle-complete').click(function(){
                        subscription.isSubsCompleted ? unComplete() : complete();
                    });
                }

                this.scrollFocus = function () {
                    var currentScrollTop = $('html').scrollTop();
                    var scrollTop = domItem.offset().top;
                    var dur = 200 + Math.abs(scrollTop  - currentScrollTop) / 10;
                    setTimeout(function () {
                        $('html').animate({scrollTop: scrollTop + 'px'}, dur);
                    }, 0);
                }

                this.id = function () {
                    return itemData.purchased_ID;
                }
                this.subscriptionId = function () {
                    return subscription.clientsubscription_ID;
                }
                this.socialLinks = function () {
                    var viewUri = encodeURIComponent(BASE_URL + '/view.php?id=' + subscription.intClientSubscription_post_ID);
                    var title = encodeURIComponent(subscription.strClientSubscription_title);
                    var subject = encodeURIComponent('Shared from Walden.ly - ' + subscription.strClientSubscription_title);
                    var shareLinks = {
                        facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + viewUri + '&title=' + title,
                        twitter: 'https://twitter.com/intent/tweet?url=' + viewUri,
                        google: 'https://plus.google.com/share?url=' + viewUri,
                        email: 'mailto:?subject=' + subject + '&body=' + encodeURIComponent('I thought you might like this: ') + viewUri,
                    }
                    return shareLinks;
                }
                this.init = function () {

                    domItem = domRoot.find('.subscription-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    domHeader = domItem.find('.item-header');
                    domTitle = domHeader.find('.item-title');
                    domBody = domItem.find('.item-body');
                    domFooter = domItem.find('.item-footer');
                    domCircleNavWrap = domItem.find('.circle-nav-wrapper');

                    domItem.appendTo(domRoot);
                    domItem.find('.share-post').circleNav(domItem.find('.circle-nav-wrapper'));

                    subscription = itemData.subscription;

                    if (subscription.strClientSubscription_nodeType == 'menu') {
                        domTitle.html(itemData.strSeries_title + ' - Please choose from the following selections:');
                    }
                    else {
                        domTitle.html(itemData.strSeries_title + ' - ' + subscription.strClientSubscription_title);
                    }

                    subscription.isSubsCompleted ? domHeader.find('.toogle-complete').addClass('btn-success') : domHeader.find('.toogle-complete').removeClass('btn-success');
                    makeBody();
                    subscription.favorite ? domFooter.find('.make-favorite').addClass('favorited') : domFooter.find('.make-favorite').removeClass('favorited');
                    var socialLinks = self.socialLinks();
                    domCircleNavWrap.find('.facebook-link').attr('href', socialLinks.facebook);
                    domCircleNavWrap.find('.twitter-link').attr('href', socialLinks.twitter);
                    domCircleNavWrap.find('.google-link').attr('href', socialLinks.google);
                    domCircleNavWrap.find('.linkedin-link').attr('href', socialLinks.linkedIn);
                    domCircleNavWrap.find('.mail-link').attr('href', socialLinks.email);

                    bindEvents();
                }
            }

            this.scrollTo = function (id) {
                itemHandles.forEach(function (hdl) {
                    if (hdl.id() == id){
                        hdl.scrollFocus();
                    }
                });
            }
            this.init = function () {
                series.forEach(function (sery) {
                    var handle = new itemClass(sery);
                    handle.init();
                    itemHandles.push(handle);
                });
                jQuery(document).ready(function () {
                });
            }
        }
        var EventsSectionClass = function () {

            var data = initialEvents;

            var domEventsSectionRoot = $('.right-panel .events-section');
            var personalEventsHandle = null;
            var todaysEventsHandle = null;
            var calendarForTodayHandle = null;

            var TodaysEventsClass = function () {
                var dom_todays_inspirations_root = $('main.main-content');

                var dom_inspiration = $('.decoration-container .inspiration');
                var dom_date_range_to_show = $('.decoration-container .date-range');

                var todaysEventRowHandles = [];
                var now_date = new Date().yyyymmdd();

                var start_date_to_show = now_date;
                var end_date_to_show = now_date;

                var TodaysEventRowClass = function (event_data) {
                    var dom_event_instance_root = $('.event-item.sample', dom_todays_inspirations_root).clone().removeAttr('hidden').removeClass('sample');
                    var dom_main_img = $('>.item-img', dom_event_instance_root);
                    var domEventHeader = $('.item-header', dom_event_instance_root);
                    var dom_event_description = $('.item-body .event-description', dom_event_instance_root);
                    var dom_event_location = $('.item-body .event-location', dom_event_instance_root);
                    var dom_make_favorite = $('.make-favorite', dom_event_instance_root);
                    var dom_trash_object = $('.trash-post', dom_event_instance_root);
                    var dom_switch_btn = $('.open-switch-lightbox', dom_event_instance_root);
                    var is_favorite = event_data.is_favorite;
                    var pos_in_parent = -1;

                    var start_date = event_data.start_date;
                    var start_time = event_data.start_time;

                    var updatePosInParent = function (new_pos) {
                        pos_in_parent = new_pos;
                    }

                    var stringDateToFormat = function (date_str, time_str) {

                        var date_time = date_str.stringToDate();
                        var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nobember', 'December'];
                        var time_parts = time_str.match(/\d+/g);
                        var hours = parseInt(time_parts[0]);
                        var dd = 'AM';
                        if (hours >= 12){
                            dd = 'PM';
                            if (hours > 12){
                                hours = hours - 12;
                            }
                        }
                        return month_name[date_time.getMonth()] + ' ' + date_time.getDate() + ', ' + date_time.getFullYear() + ' at ' + hours + ':' + parseInt(time_parts[1]) + dd;
                    };

                    var self = this;

                    var makeFavorite = function () {
                        var ajax_data = {
                            action: 'make_event_favorite',
                            event_id: event_data.event_id,
                            start_date: event_data.start_date,
                            is_rescheduled: 0
                        };
                        if (event_data.instance_type == 'rescheduled_instance'){
                            ajax_data.instance_id = event_data.id;
                            ajax_data.is_rescheduled = 1;
                        }
                        $.ajax({
                            url: API_ROOT_URL + '/Events.php',
                            data: ajax_data,
                            success: function (res) {
                                if (res.status == true){
                                    is_favorite = true;
                                    event_data.is_favorite = true;
                                    event_data.favorite_row = res.data;
                                    dom_make_favorite.addClass('favorited');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    var makeUnFavorite = function () {
                        var ajax_data = {
                            action: 'make_event_unfavorite',
                            id: event_data.favorite_row.id
                        };
                        $.ajax({
                            url: API_ROOT_URL + '/Events.php',
                            data: ajax_data,
                            success: function (res) {
                                if (res.status == true){
                                    is_favorite = false;
                                    event_data.is_favorite = false;
                                    event_data.favorite_row = null;
                                    dom_make_favorite.removeClass('favorited');
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        })
                    }
                    var cancelEventInstance = function (e) {
                        swal({
                            title: "Are you sure?",
                            text: "Do you want to cancel this event?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                var ajax_data = {};
                                if (event_data.instance_type == 'rescheduled_instance'){
                                    ajax_data.action = 'delete_rescheduled_event';
                                    ajax_data.id = event_data.id;
                                    $.ajax({
                                        url: API_ROOT_URL + '/Events.php',
                                        type: 'post',
                                        data: ajax_data,
                                        dataType: 'json',
                                        success: function (res) {
                                            if (res.status){

                                                var pos_tp = 0;
                                                event_data.origin_event.rescheduled_events.forEach(function (rescheduled_event, i) {
                                                    if (rescheduled_event.id == event_data.id){
                                                        pos_tp = i;
                                                    }
                                                });
                                                event_data.origin_event.rescheduled_events.splice(pos_tp, 1);
                                                todaysEventsHandle.replaceInstances();
                                                todaysEventsHandle.refresh();
                                            }
                                        }
                                    });
                                }
                                else if (event_data.instance_type == 'single_instance'){
                                    ajax_data.action = 'delete_event';
                                    ajax_data.id = event_data.event_id;
                                    $.ajax({
                                        url: API_ROOT_URL + '/Events.php',
                                        type: 'post',
                                        data: ajax_data,
                                        dataType: 'json',
                                        success: function (res) {
                                            if (res.status){

                                                var pos_tp = 0;
                                                for (var i = 0; i < data.length; i ++){
                                                    if (data[i].id == event_data.event_id){
                                                        pos_tp = i;
                                                        break;
                                                    }
                                                }
                                                data.splice(pos_tp, 1);
                                                todaysEventsHandle.replaceInstances();
                                                todaysEventsHandle.refresh();
                                            }
                                        }
                                    });
                                }
                                else {
                                    ajax_data.action = 'cancel_event_instance';
                                    ajax_data.data = {event_id: event_data.event_id, start_date: start_date, start_time: start_time};
                                    $.ajax({
                                        url: API_ROOT_URL + '/Events.php',
                                        type: 'post',
                                        data: ajax_data,
                                        dataType: 'json',
                                        success: function (res) {
                                            if (res.status){

                                                event_data.origin_event.cancelled_events.push(res.data);
                                                todaysEventsHandle.replaceInstances();
                                                todaysEventsHandle.refresh();
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }

                    if (is_favorite){
                        dom_make_favorite.addClass('favorited');
                    }


                    var bindEvent = function () {
                        dom_make_favorite.click(function () {
                            if (is_favorite){
                                makeUnFavorite();
                            }
                            else {
                                makeFavorite();
                            }
                        });
                        dom_trash_object.click(function (e) {
                            cancelEventInstance(e);
                        });
                        dom_switch_btn.click(function () {
                            console.log('pending');
                        })
                    }

                    if (event_data.event_images.length){
                        dom_main_img.attr('src', event_data.event_images[0].media_url);
                    }
                    domEventHeader.find('.event-title').html(event_data.event_title);
                    domEventHeader.find('.start-datetime').html(stringDateToFormat(start_date, start_time));

                    dom_event_description.html(event_data.event_description);
                    dom_event_location.html(event_data.event_location);

                    var applyChangeToInstance = function (new_data) {
                        if (new_data.event_title !== null){
                            event_data.event_title = new_data.event_title;
                            domEventHeader.html(new_data.event_title);
                        }
                        if (new_data.event_description !== null){
                            event_data.event_description = new_data.event_description != null ? new_data.event_description : event_data.event_description;
                            dom_event_description.html(new_data.event_description);
                        }
                        if (new_data.event_location !== null){
                            event_data.event_location = new_data.event_location != null ? new_data.event_location : event_data.event_location;
                            dom_event_location.html(event_data.event_location);
                        }
                        if (new_data.start_date !== null){
                            start_date = new_data.start_date;
                            event_data.start_date = new_data.start_date != null ? new_data.start_date : event_data.start_date;
                        }
                        if (new_data.start_time !== null){
                            start_time = new_data.start_time;
                            event_data.start_time = new_data.start_time != null ? new_data.start_time : event_data.start_time;
                        }
                        if (new_data.start_date !== null || new_data.start_time !== null){
                            domEventHeader.find('.start-datetime').html(stringDateToFormat(start_date, start_time));
                        }
                        if (event_data.event_images.length){
                            dom_main_img.attr('src', event_data.event_images[0].media_url);
                        }
                        else {
                            dom_main_img.attr('src', default_event_image);
                        }
                    }

                    this.show = function () {
                        dom_todays_inspirations_root.prepend(dom_event_instance_root);
                    }
                    this.hide = function () {
                        dom_event_instance_root.detach();
                    }
                    this.removeDom = function () {
                        dom_event_instance_root.remove();
                    }
                    this.getDateTime = function () {
                        return start_date + ' ' + start_time;
                    }
                    this.getTime = function () {
                        return new Date(start_date + ' ' + start_time).getTime();
                    }
                    this.getStartDate = function(){ return start_date; };
                    this.init = function () {
                        bindEvent();
                    }
                    this.updatePosInParent = updatePosInParent;
                    this.getPosInParent = function () {
                        return pos_in_parent;
                    }
                    this.dom_event_instance_root = function () {
                        return dom_event_instance_root;
                    }
                }

                var getInstancesHandlesOfEvent = function (event_data) {

                    var now_date = new Date().yyyymmdd();
                    var now_time = new Date().toTimeString().split(' ')[0];

                    var eventInstancesHanldes = [];

                    var i;

                    if (typeof event_data.rescheduled_events != 'undefined'){
                        event_data.rescheduled_events.forEach(function (rescheduled_event) {
                            rescheduled_event.instance_type = 'rescheduled_instance';
                            rescheduled_event.event_images = event_data.event_images;
                            var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                return event_favorite.is_rescheduled == true && event_favorite.instance_id == rescheduled_event.id;
                            });
                            rescheduled_event.is_favorite = favorite_rows.length;
                            rescheduled_event.favorite_row = rescheduled_event.is_favorite ? favorite_rows[0] : null;

                            if (rescheduled_event.start_date >= start_date_to_show && rescheduled_event.start_date <= end_date_to_show){
                                todaysEventsHandle.addInstanceRow(rescheduled_event);
                            }
                        });
                    }

                    if (event_data.is_recurring === '1'){
                        var step = 24 * 60 * 60 * 1000;

                        switch (event_data.recurring_pattern.recurring_type){
                            case 'd':
                                step *= (1 * event_data.recurring_pattern.separation_count);
                                break;
                            case 'w':
                                step *= (7 * event_data.recurring_pattern.separation_count);
                                break;
                            default:
                                break;
                        }
                        var distance = Math.ceil((new Date(start_date_to_show) - new Date(event_data.start_date)) / step);
                        if ( distance < 0 ) { distance = 0; }

                        for ( i = 0; ; i ++){
                            var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (distance + i)).yyyymmdd();
                            var instance_day = new Date(instance_date).getDay();
                            var isin = false;
                            if (instance_date > end_date_to_show){ break; }
                            for(var j = 0; j < event_data.cancelled_events.length; j++){
                                if (event_data.cancelled_events[j].start_date == instance_date){ isin = true; break; }
                            }
                            if (!isin && event_data.recurring_pattern.days_of_week[parseInt(instance_day)] == '1'){

                                var event_instance = {
                                    origin_event: event_data,
                                    event_id: event_data.id,
                                    event_title: event_data.event_title,
                                    event_description: event_data.event_description,
                                    event_location: event_data.event_location,
                                    start_date: instance_date,
                                    start_time: event_data.start_time,
                                    instance_type: 'recurring_instance',
                                    event_images: event_data.event_images
                                }
                                var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                    return event_favorite.is_rescheduled == 0 && event_favorite.start_date == instance_date;
                                });

                                event_instance.is_favorite = favorite_rows.length;
                                event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;
                                if (start_date_to_show <= event_instance.start_date && event_instance.start_date <= end_date_to_show){
                                    todaysEventsHandle.addInstanceRow(event_instance);
                                }
                            }
                        }
                    }
                    else {
                        var event_instance = {
                            origin_event: event_data,
                            event_id: event_data.id,
                            event_title: event_data.event_title,
                            event_description: event_data.event_description,
                            event_location: event_data.event_location,
                            start_date: event_data.start_date,
                            start_time: event_data.start_time,
                            instance_type: 'single_instance',
                            event_images: event_data.event_images
                        };

                        var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                            return event_favorite.is_rescheduled == 0;
                        });
                        event_instance.is_favorite = favorite_rows.length;
                        event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                        if (event_instance.start_date >= start_date_to_show && event_instance.start_date <= end_date_to_show){
                            todaysEventsHandle.addInstanceRow(event_instance);
                        }
                    }
                    return eventInstancesHanldes;
                }

                var convertDateFormat = function (date_to_convert) {
                    var datePart = date_to_convert.match(/\d+/g);
                    var yy = datePart[0].substring(2);
                    var m = parseInt(datePart[1]);
                    var d = parseInt(datePart[2]);
                    return m + '/' + d + '/' + yy;
                }

                this.addInstanceRow = function (event_data) {
                    var handle = new TodaysEventRowClass(event_data);
                    todaysEventRowHandles.push(handle);
                    handle.init();
                    return handle;
                }

                this.updateDateRangeToShow = function (s_date, e_date) {
                    start_date_to_show = s_date;
                    end_date_to_show = e_date;
                };

                this.replaceInstances = function () {
                    todaysEventRowHandles.forEach(function (todaysEventRowHandle) {
                        todaysEventRowHandle.removeDom();
                    });
                    todaysEventRowHandles = [];
                    data.forEach(function (event_row) {
                        getInstancesHandlesOfEvent(event_row);
                    });
                }

                this.refresh = function () {

                    dom_date_range_to_show.find('.start-date').html(convertDateFormat(start_date_to_show));
                    dom_date_range_to_show.find(('.end-date')).html(convertDateFormat(end_date_to_show));

                    if (start_date_to_show == now_date && end_date_to_show == now_date){
                        dom_inspiration.show();
                        dom_date_range_to_show.hide();
                    }
                    else {
                        dom_inspiration.hide();
                        dom_date_range_to_show.show();
                    }

                    todaysEventRowHandles.forEach(function (todaysEventRowHandle) {
                        todaysEventRowHandle.hide();
                    });
                    todaysEventRowHandles = todaysEventRowHandles.sort(function (a, b) {
                        if (a.getDateTime() > b.getDateTime()){
                            return -1;
                        }
                        else if (a.getDateTime() == b.getDateTime()){
                            return 0;
                        }
                        return 1;
                    });

                    todaysEventRowHandles.forEach(function (todaysEventRowHandle, i) {
                        todaysEventRowHandle.updatePosInParent(i);
                        if (start_date_to_show <= todaysEventRowHandle.getStartDate() && todaysEventRowHandle.getStartDate() <= end_date_to_show){
                            todaysEventRowHandle.show();
                        }
                    });
                }

                this.init = function () {

                    dom_date_range_to_show.find('.back-to-today').click(function () {
                        calendarForTodayHandle.clearDateRange();
                    });
                    this.replaceInstances();
                    this.refresh();
                }
            }

            var PersonalEventsClass = function () {
                var dom_personal_events_root = $('.personal-events', domEventsSectionRoot);
                var dom_event_list = $('ul.event-list', dom_personal_events_root);
                var dom_view_more = $('footer .view-more-event', dom_personal_events_root)
                var dom_add_event = $('footer .add-event', dom_personal_events_root);

                var events = data;
                var event_handles = [];
                var current_index = 0;

                var eventRowClass = function (event_row_data) {

                    var dom_event_row_root = $('li.sample', dom_event_list).clone().removeClass('sample').removeAttr('hidden');
                    var dom_favorite = $('.make-favorite', dom_event_row_root);
                    var dom_cancel_event = $('.cancel-event-instance', dom_event_row_root);
                    var dom_occurring_date = $('.occurring-datetime', dom_event_row_root);
                    var domEventHeader = $('.event-title', dom_event_row_root);
                    var dom_event_instance = $('.event-instance', dom_event_row_root);

                    var start_date = event_row_data.start_date;
                    var start_time = event_row_data.start_time;
                    var is_recurring = (event_row_data.is_recurring === '1' || event_row_data.is_recurring == true) ? true : false;
                    var is_favorite = event_row_data.is_favorite;
                    if (is_favorite){ dom_favorite.addClass('favorited'); }

                    var pos_in_parent = -1;

                    var stringDateToFormat = function (date_str, time_str) {

                        var date_time = date_str.stringToDate();
                        var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nobember', 'December'];
                        var time_parts = time_str.match(/\d+/g);
                        var hours = parseInt(time_parts[0]);
                        var dd = 'AM';
                        if (hours >= 12){
                            dd = 'PM';
                            if (hours > 12){
                                hours = hours - 12;
                            }
                        }
                        return month_name[date_time.getMonth()] + ' ' + date_time.getDate() + ', ' + date_time.getFullYear() + ' at ' + hours + ':' + parseInt(time_parts[1]) + dd;
                    };

                    var showOn = function (parent_dom) {
                        dom_event_row_root.appendTo(parent_dom);
                    }

                    var self = this;

                    var applyChangeToInstance = function (new_data) {
                        if (new_data.event_title !== null){
                            event_row_data.event_title = new_data.event_title;
                            domEventHeader.html(new_data.event_title);
                        }
                        if (new_data.event_description !== null){
                            event_row_data.event_description = new_data.event_description != null ? new_data.event_description : event_row_data.event_description;
                        }
                        if (new_data.event_location !== null){
                            event_row_data.event_location = new_data.event_location != null ? new_data.event_location : event_row_data.event_description;
                        }
                        if (new_data.start_date !== null){
                            start_date = new_data.start_date;
                            event_row_data.start_date = new_data.start_date != null ? new_data.start_date : event_row_data.start_date;
                        }
                        if (new_data.start_time !== null){
                            start_time = new_data.start_time;
                            event_row_data.start_time = new_data.start_time != null ? new_data.start_time : event_row_data.start_time;
                        }
                        dom_occurring_date.html(stringDateToFormat(event_row_data.start_date, event_row_data.start_time));
                    }

                    var makeFavorite = function () {
                        var ajax_data = {
                            action: 'make_event_favorite',
                            event_id: event_row_data.event_id,
                            start_date: event_row_data.start_date,
                            is_rescheduled: 0
                        };
                        if (event_row_data.instance_type == 'rescheduled_instance'){
                            ajax_data.instance_id = event_row_data.id;
                            ajax_data.is_rescheduled = 1;
                        }
                        $.ajax({
                            url: API_ROOT_URL + '/Events.php',
                            data: ajax_data,
                            success: function (res) {
                                if (res.status == true){
                                    is_favorite = true;
                                    event_row_data.is_favorite = true;
                                    event_row_data.favorite_row = res.data;
                                    dom_favorite.addClass('favorited');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    var makeUnFavorite = function () {
                        var ajax_data = {
                            action: 'make_event_unfavorite',
                            id: event_row_data.favorite_row.id
                        };
                        $.ajax({
                            url: API_ROOT_URL + '/Events.php',
                            data: ajax_data,
                            success: function (res) {
                                if (res.status == true){
                                    is_favorite = false;
                                    event_row_data.is_favorite = false;
                                    event_row_data.favorite_row = null;
                                    dom_favorite.removeClass('favorited');
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        })
                    }
                    var cancelEventInstance = function(){
                        var ajax_data = {};
                        if (event_row_data.instance_type == 'rescheduled_instance'){
                            ajax_data.action = 'delete_rescheduled_event';
                            ajax_data.id = event_row_data.id;
                            $.ajax({
                                url: API_ROOT_URL + '/Events.php',
                                type: 'post',
                                data: ajax_data,
                                dataType: 'json',
                                success: function (res) {
                                    if (res.status){

                                        var index = pos_in_parent;
                                        self.removeFromDom();
                                        event_handles.splice(index, 1);
                                        current_index--;
                                        refresh();
                                        var pos_tp = 0;
                                        event_row_data.origin_event.rescheduled_events.forEach(function (rescheduled_event, i) {
                                            if (rescheduled_event.id == event_row_data.id){
                                                pos_tp = i;
                                            }
                                        });
                                        event_row_data.origin_event.rescheduled_events.splice(pos_tp, 1);
                                        todaysEventsHandle.replaceInstances();
                                        todaysEventsHandle.refresh();
                                    }
                                }
                            });
                        }
                        else if (event_row_data.instance_type == 'single_instance'){
                            ajax_data.action = 'delete_event';
                            ajax_data.id = event_row_data.event_id;
                            $.ajax({
                                url: API_ROOT_URL + '/Events.php',
                                type: 'post',
                                data: ajax_data,
                                dataType: 'json',
                                success: function (res) {
                                    if (res.status){

                                        var index = pos_in_parent;
                                        self.removeFromDom();
                                        event_handles.splice(index, 1);
                                        current_index--;
                                        refresh();

                                        var pos_tp = 0;
                                        for (var i = 0; i < data.length; i ++){
                                            if (data[i].id == event_row_data.event_id){
                                                pos_tp = i;
                                                break;
                                            }
                                        }
                                        data.splice(pos_tp, 1);
                                        todaysEventsHandle.replaceInstances();
                                        todaysEventsHandle.refresh();
                                    }
                                }
                            });
                        }
                        else {
                            ajax_data.action = 'cancel_event_instance';
                            ajax_data.data = {event_id: event_row_data.event_id, start_date: start_date, start_time: start_time};
                            $.ajax({
                                url: API_ROOT_URL + '/Events.php',
                                type: 'post',
                                data: ajax_data,
                                dataType: 'json',
                                success: function (res) {
                                    if (res.status){

                                        var index = pos_in_parent;
                                        self.removeFromDom();
                                        event_handles.splice(index, 1);
                                        current_index--;
                                        refresh();

                                        event_row_data.origin_event.cancelled_events.push(res.data);
                                        todaysEventsHandle.replaceInstances();
                                        todaysEventsHandle.refresh();
                                    }
                                }
                            });
                        }
                    }
                    var removeEvent = function () {
                        var ajax_data = {};
                        ajax_data.action = 'delete_event';
                        ajax_data.id = event_row_data.event_id;
                        $.ajax({
                            url: API_ROOT_URL + '/Events.php',
                            type: 'post',
                            data: ajax_data,
                            dataType: 'json',
                            success: function (res) {
                                if (res.status){
                                    removeAllInstancesOfEvent(event_row_data.event_id);
                                    refresh();
                                    var pos_tp = 0;
                                    for (var i = 0; i < data.length; i ++){
                                        if (data[i].id == event_row_data.event_id){
                                            pos_tp = i;
                                            break;
                                        }
                                    }
                                    data.splice(pos_tp, 1);
                                    todaysEventsHandle.replaceInstances();
                                    todaysEventsHandle.refresh();
                                }
                            }
                        });
                    }

                    var bindEvent = function () {
                        dom_event_instance.click(function () {
                            console.log('pendding');
                        });
                        dom_favorite.click(function () {
                            if (is_favorite){
                                makeUnFavorite();
                            }
                            else {
                                makeFavorite();
                            }
                        });
                        dom_cancel_event.on('click', function (e) {
                            e.preventDefault();
                            swal({
                                title: "Are you sure?",
                                icon: "warning",
                                dangerMode: true,
                                buttons: {
                                    cancel: "Cancel",
                                    trashPost: {
                                        text: "Skep This Day",
                                        value: "trash_post",
                                        className: 'swal-button--danger'
                                    },
                                    removeSeries: {
                                        text: 'Delete This Event',
                                        value: 'remove_event',
                                        className: 'swal-button--danger'
                                    },
                                },
                            }).then(function(value){
                                switch (value) {
                                    case "trash_post":
                                        cancelEventInstance();
                                        break;
                                    case "remove_event":
                                        removeEvent();
                                        break;
                                    default:
                                        break;
                                }
                            });
                        });
                    }
                    this.showOnEventsList = function () {
                        dom_event_row_root.appendTo(dom_event_list);
                    }
                    this.removeFromDom = function () {
                        dom_event_row_root.detach();
                    }
                    this.getDateTime = function () {
                        return new Date(start_date + ' ' + start_time);
                    }
                    this.updatePosInParent = function (new_pos) {
                        pos_in_parent = new_pos;
                    }
                    this.getPosInParent = function () {
                        return pos_in_parent;
                    }
                    this.getEventId = function () {
                        return event_row_data.event_id;
                    }
                    this.applyChangeToInstance = applyChangeToInstance;
                    this.dom_event_row_root = function(){ return dom_event_row_root; };
                    this.init = function () {

                        dom_occurring_date.html(stringDateToFormat(event_row_data.start_date, event_row_data.start_time));
                        domEventHeader.html(event_row_data['event_title']);
                        bindEvent();
                    }
                }

                var getInstancesHandlesOfEvent = function (event_data) {
                    var now_date = new Date().yyyymmdd();
                    var now_time = new Date().toTimeString().split(' ')[0];

                    var eventInstancesHanldes = [];

                    var i;

                    if (typeof event_data.rescheduled_events != 'undefined'){
                        event_data.rescheduled_events.forEach(function (rescheduled_event) {
                            rescheduled_event.instance_type = 'rescheduled_instance';
                            rescheduled_event.event_images = event_data.event_images;
                            rescheduled_event.origin_event = event_data;
                            var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                return event_favorite.is_rescheduled == true && event_favorite.instance_id == rescheduled_event.id;
                            });
                            rescheduled_event.is_favorite = favorite_rows.length;
                            rescheduled_event.favorite_row = rescheduled_event.is_favorite ? favorite_rows[0] : null;

                            if (rescheduled_event.start_date > now_date){
                                var newHanlde = new eventRowClass(rescheduled_event);
                                newHanlde.init();
                                eventInstancesHanldes.push(newHanlde);
                            }
                        });
                    }

                    if (event_data.is_recurring === '1'){
                        var step = 24 * 60 * 60 * 1000;

                        switch (event_data.recurring_pattern.recurring_type){
                            case 'd':
                                step *= (1 * event_data.recurring_pattern.separation_count);
                                break;
                            case 'w':
                                step *= (7 * event_data.recurring_pattern.separation_count);
                                break;
                            default:
                                break;
                        }
                        var distance = Math.ceil((new Date(now_date) - new Date(event_data.start_date)) / step);
                        if ( distance < 0 ) { distance = 0; }
                        var event_flag = 0;
                        for ( i = 0; i < 365; i ++){
                            // var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (distance + i)).yyyymmdd();
                            var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (0 + i)).yyyymmdd();
                            var instance_day = new Date(instance_date).getDay();
                            var isin = false;
                            for(var j = 0; j < event_data.cancelled_events.length; j++){
                                if (event_data.cancelled_events[j].start_date == instance_date){ isin = true; break; }
                            }
                            if (!isin && event_data.recurring_pattern.days_of_week[parseInt(instance_day)] == '1'){
                                var event_instance = {
                                    origin_event: event_data,
                                    event_id: event_data.id,
                                    event_title: event_data.event_title,
                                    event_description: event_data.event_description,
                                    event_location: event_data.event_location,
                                    start_date: instance_date,
                                    start_time: event_data.start_time,
                                    instance_type: 'recurring_instance',
                                    event_images: event_data.event_images
                                }


                                var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                    return event_favorite.is_rescheduled == 0 && event_favorite.start_date == instance_date;
                                });
                                event_instance.is_favorite = favorite_rows.length;
                                event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                                if (!event_flag && event_instance.start_date > now_date){
                                    event_flag = 1;
                                    var newHandle = new eventRowClass(event_instance);
                                    newHandle.init();
                                    eventInstancesHanldes.push(newHandle);
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        var event_instance = {
                            origin_event: event_data,
                            event_id: event_data.id,
                            event_title: event_data.event_title,
                            event_description: event_data.event_description,
                            event_location: event_data.event_location,
                            start_date: event_data.start_date,
                            start_time: event_data.start_time,
                            instance_type: 'single_instance',
                            event_images: event_data.event_images
                        };

                        var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                            return event_favorite.is_rescheduled == 0;
                        });
                        event_instance.is_favorite = favorite_rows.length;
                        event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                        if (event_instance.start_date > now_date){
                            var newHandle = new eventRowClass(event_instance);
                            newHandle.init();
                            eventInstancesHanldes.push(newHandle);
                        }
                    }
                    return eventInstancesHanldes;
                }

                var updateAllInstancesOfEvent = function (event_id, data) {
                    var handles_to_update = event_handles.filter(function (event_handle) {
                        return event_handle.getEventId() == event_id;
                    });
                    data.start_date = null;
                    handles_to_update.forEach(function (handle_to_update) {
                        handle_to_update.applyChangeToInstance(data);
                    });
                };
                var removeAllInstancesOfEvent = function (event_id) {
                    var handles_to_remove = event_handles.filter(function (event_handle) {
                        return event_handle.getEventId() == event_id;
                    });
                    var minus = 0;
                    handles_to_remove.forEach(function(handle_to_remove){
                        handle_to_remove.removeFromDom();
                        var pos = handle_to_remove.getPosInParent() - minus;
                        event_handles.splice(pos, 1);
                        minus++;
                    });
                }

                var flipInitInstancesOfEvent = function (event_id) {
                    var handles_to_update = event_handles.filter(function (event_handle) {
                        return event_handle.getEventId() == event_id;
                    });
                };

                var viewMore = function () {
                    var next_page_index = current_index + 5;
                    for (var i = current_index; i < next_page_index && i < event_handles.length; i ++){
                        event_handles[i].showOnEventsList();
                        current_index++;
                    }
                }

                var refresh = function () {
                    var i;
                    for ( i = 0; i < current_index + 2 && i < event_handles.length; i ++){
                        event_handles[i].removeFromDom();
                    }
                    event_handles = event_handles.sort(function (a, b) {
                        if (parseInt(a.getDateTime().getTime()) > parseInt(b.getDateTime().getTime())){
                            return 1;
                        }
                        else if(parseInt(a.getDateTime().getTime()) == parseInt(b.getDateTime().getTime())){
                            return 0;
                        }
                        return -1;
                    });

                    for (i = 0; i < event_handles.length; i ++){
                        event_handles[i].updatePosInParent(i);
                    }

                    for (i = 0; i < current_index && i < event_handles.length; i ++){
                        event_handles[i].showOnEventsList();
                    }
                }

                var bindEvents = function () {
                    dom_add_event.click(function () {
                        console.log('pendding');
                    });
                    dom_view_more.click(function () {
                        viewMore();
                    });
                }

                this.refresh = refresh;

                this.init = function () {

                    events.forEach(function (event_data) {
                        var instanceHandlesOfEvents = getInstancesHandlesOfEvent(event_data);
                        instanceHandlesOfEvents = instanceHandlesOfEvents.sort(function (a, b) {
                            if (a.getDateTime().getTime() > b.getDateTime().getTime()){
                                return 1;
                            }
                            else if (a.getDateTime().getTime() == b.getDateTime().getTime()){
                                return 0;
                            }
                            return -1;
                        });
                        if (instanceHandlesOfEvents.length > 0){
                            event_handles = event_handles.concat(instanceHandlesOfEvents[0]);
                        }
                    });

                    refresh();
                    viewMore();
                    bindEvents();
                }
            }

            var CalendarForTodayClass = function () {
                var now_date = new Date().yyyymmdd();

                var start_date, end_date;
                var calendarInstance = $('#calendar_for_today').datepicker({
                    todayHighlight: true,
                    toggleActive: true,
                    multidate: true
                });

                calendarInstance.on('changeDate', function (e) {
                    var dates = calendarInstance.datepicker('getDates');
                    switch (dates.length){
                        case 0:
                            start_date = end_date = now_date;
                            todaysEventsHandle.updateDateRangeToShow(start_date, end_date);
                            todaysEventsHandle.replaceInstances();
                            todaysEventsHandle.refresh();
                            break;
                        case 1:
                            start_date = end_date = dates[0].yyyymmdd();
                            todaysEventsHandle.updateDateRangeToShow(start_date, end_date);
                            todaysEventsHandle.replaceInstances();
                            todaysEventsHandle.refresh();
                            break;
                        case 2:
                            start_date = dates[0].yyyymmdd();
                            end_date = dates[1].yyyymmdd();
                            start_date = start_date > end_date ? [end_date, end_date = start_date][0] : start_date;
                            todaysEventsHandle.updateDateRangeToShow(start_date, end_date);
                            todaysEventsHandle.replaceInstances();
                            todaysEventsHandle.refresh();
                            break;
                        case 3:
                            dates.splice(0, 1);
                            calendarInstance.datepicker('setDates', dates);
                            break;
                        default:
                            break;
                    }
                });
                this.clearDateRange = function () {
                    calendarInstance.datepicker('setDates', []);
                };
                this.init = function () {

                }
            }

            this.init = function () {
                todaysEventsHandle = new TodaysEventsClass();
                personalEventsHandle = new PersonalEventsClass();
                calendarForTodayHandle = new CalendarForTodayClass();
                todaysEventsHandle.init();
                personalEventsHandle.init();
                calendarForTodayHandle.init();
            }
        }
        var AmazonProductsClass = function () {

            var watchListHandle = null;
            var leftPanelHandle = null;

            var watchListClass = function () {
                var dom_watch_list_root = $('.amazon-products-watchlist-section');
                var dom_list_contiainer = $('.product-list', dom_watch_list_root);

                var getAsin = function (url) {
                    var regex = RegExp("https://www.amazon.com/([\\w-]+/)?(dp|gp/product)/(\\w+/)?(\\w{10})");
                    var m = url.match(regex);
                    if (m == null){
                        return false;
                    }
                    return m[4];
                };
                var self = this, loadingHandle;
                var listRowHandles = [];
                var keys_of_id = [];

                var createNewProductByAsin = function (asin) {
                    var ajaxData = {
                        action: 'create_new_product',
                        asin: asin
                    };

                    $.ajax({
                        url: API_ROOT_URL + '/Products.php',
                        type: 'post',
                        data: ajaxData,
                        dataType: 'json',
                        success: function (res) {
                            if (res.status){
                                var res_data = res.data;
                                res_data.price_rows.forEach(function (price_row) {
                                    price_row.intProductPrice_newprice = price_row.intProductPrice_newprice / 100;
                                    price_row.intProductPrice_usedprice = price_row.intProductPrice_usedprice / 100;
                                });

                                if (typeof keys_of_id[res_data.product_ID] == 'undefined'){
                                    var newHandle = self.addListRow(res_data);
                                    newHandle.show();
                                }
                                else {
                                    var currentHandle = listRowHandles[keys_of_id[res_data.product_ID]];
                                    currentHandle.updateInFront(res_data);
                                }
                            }
                            else {
                                alert('sorry, somethin is wrong');
                            }
                        }
                    });
                };

                var listRowClass = function (row_data) {

                    var pos_in_parent = -1;

                    var handleOfLeft = null;
                    var self = this;

                    var dom_list_row = dom_list_contiainer.find('.list-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    var dom_title = dom_list_row.find('.product-title');
                    var dom_price = dom_list_row.find('.product-price a');
                    var dom_close_product = dom_list_row.find('.close-product');

                    var dom_recommendation = dom_list_row.find('.recommendation');
                    var latest_price = row_data.price_rows[row_data.price_rows.length - 1];
                    var format_price = '$' + (latest_price.intProductPrice_newprice);

                    var is_show = false;
                    var bindEvent = function () {
                        dom_list_row.find('.amchar-icon-wrapper a').click(function () {
                            if (handleOfLeft !== null){
                                var scroll_top = handleOfLeft.getScrollTop();
                                setTimeout(function () {
                                    $('html').animate({scrollTop: scroll_top + 'px'}, 400);
                                }, 0);
                            }
                            else {
                                var newHandle = leftPanelHandle.addItem(row_data);
                                self.setLeftHandle(newHandle);
                                handleOfLeft.setRightHandle(self);
                                handleOfLeft.show();
                                var scroll_top = handleOfLeft.getScrollTop();
                                $('.loading').css('display', 'block');
                                setTimeout(function () {
                                    $('.loading').css('display', 'none');
                                    $('html').animate({scrollTop: scroll_top + 'px'}, 400);
                                }, 370);
                            }
                        });
                        dom_close_product.click(function () {

                            swal({
                                // title: "Are you sure?",
                                text: "Are you sure you want to delete this product?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    self.closeProduct();
                                }
                            });
                        });
                    }
                    this.closeProduct = function () {
                        var ajaxData = {
                            action: 'close_product',
                            product_id: row_data.product_ID
                        };
                        $.ajax({
                            url: API_ROOT_URL + '/Products.php',
                            type: 'post',
                            data: ajaxData,
                            dataType: 'json',
                            success: function (res) {
                                if (res.status){
                                    keys_of_id[row_data.product_ID] = undefined;
                                    dom_list_row.remove();
                                    listRowHandles.splice(pos_in_parent, 1);
                                    if (handleOfLeft !== null){
                                        handleOfLeft.removeRow()
                                    }
                                }
                                else {
                                    alert('Sorry, something went wrong');
                                }
                            }
                        });
                    }
                    this.updateInFront = function (new_data) {

                        row_data.strProduct_name = new_data.strProduct_name;
                        row_data.recommendation = new_data.recommendation;
                        row_data.price_rows = new_data.price_rows;

                        latest_price = row_data.price_rows[row_data.price_rows.length - 1];
                        format_price = '$' + (latest_price.intProductPrice_newprice);
                        dom_title.html(row_data.strProduct_name);
                        dom_price.html(format_price);
                        dom_recommendation.html(row_data.recommendation);
                        if (handleOfLeft !== null){
                            handleOfLeft.updateInFront(row_data);
                        }
                    }

                    this.setPosInParent = function (new_pos) {
                        pos_in_parent = new_pos;
                    }
                    this.setLeftHandle = function (newHandle) {
                        handleOfLeft = newHandle;
                    }
                    this.show = function () {
                        if (!is_show){
                            dom_list_row.appendTo(dom_list_contiainer);
                            is_show = true;
                        }
                    };
                    this.init = function () {
                        dom_title.html(row_data.strProduct_name);
                        dom_price.html(format_price);
                        dom_price.attr('href', row_data.strProduct_amazonURL);
                        dom_recommendation.html(row_data.recommendation);
                        bindEvent();
                    }
                }

                var bindEvent = function () {
                    dom_watch_list_root.find('.check-product').click(function () {
                        var url = dom_watch_list_root.find('.input-wrapper input[name="new_product"]').val();
                        var asin = getAsin(url);
                        if(asin){
                            createNewProductByAsin(asin);
                        }
                        else {
                            alert('invalid url');
                        }
                    });
                    dom_watch_list_root.find('.view-all').click(function () {
                        listRowHandles.forEach(function (listRowHandle) {
                            listRowHandle.show();
                        });
                    })
                };


                this.addListRow = function (row_data) {
                    var newHandle = new listRowClass(row_data);
                    newHandle.init();
                    keys_of_id[row_data.product_ID] = listRowHandles.length;
                    newHandle.setPosInParent(listRowHandles.length);
                    listRowHandles.push(newHandle);
                    return newHandle;
                };
                this.setInitData = function (data) {
                    data.forEach(function (row_data, i) {
                        keys_of_id[row_data.product_ID] = i;
                        var newHandle = self.addListRow(row_data);
                        newHandle.setPosInParent(i);
                    });
                    for(var i = 0; i < 5 && i < listRowHandles.length; i ++){
                        listRowHandles[i].show();
                    }
                }
                this.init = function () {
                    loadingHandle = new SiteLoadingHandle({
                        container: dom_watch_list_root.find('.watch-list-wrapper'),
                        type: 7,
                        loadingSize: 3,
                        css: {
                            height: '70px',
                        },
                    });
                    loadingHandle.init();
                    // amazone start

                    // loadingHandle.show();
                    // $.ajax({
                    //     url: API_ROOT_URL + '/Products.php',
                    //     type: 'post',
                    //     data: {action: 'get_products'},
                    //     dataType: 'json',
                    //     success: function (res) {
                    //         loadingHandle.hide();
                    //         is_loading_controlled_in_local = false;
                    //         if (res.status){
                    //             res.data.forEach(function (row_data) {
                    //                 row_data.price_rows.forEach(function (price_row) {
                    //                     price_row.intProductPrice_newprice = price_row.intProductPrice_newprice / 100;
                    //                     price_row.intProductPrice_usedprice = price_row.intProductPrice_usedprice / 100;
                    //                 });
                    //             });
                    //             self.setInitData(res.data);
                    //         }
                    //         else {
                    //             alert('sorry, somethin is wrong');
                    //         }
                    //     }
                    // });

                    // amazon end
                    is_loading_controlled_in_local = false;
                    bindEvent();
                }
            };

            var leftPanelClass = function () {
                var dom_item_rows_container = $('main.main-content');

                var itemRowHandles = [];

                var itemRowClass = function (row_data) {
                    var dom_item_row = dom_item_rows_container.find('.amazon-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    var dom_chart = dom_item_row.find('.chart-root').attr('id', 'chartDiv' + row_data.product_ID);
                    var dom_item_title = dom_item_row.find('.item-header');
                    var dom_item_image = dom_item_row.find('.product-basic-description-wrapper .image-wrapper img');
                    var dom_item_descriptoin = dom_item_row.find('.product-basic-description-wrapper .product-description');
                    var dom_prices_wrapper = dom_item_row.find('.prices-wrapper');
                    var dom_new_price = dom_prices_wrapper.find('.new-price-wrapper');
                    var dom_used_price = dom_prices_wrapper.find('.used-price-wrapper');
                    var dom_new_quantity = dom_prices_wrapper.find('.new-quantity-wrapper');
                    var dom_used_quantity = dom_prices_wrapper.find('.used-quantity-wrapper');

                    var latest_price_data = row_data.price_rows[row_data.price_rows.length - 1];
                    var amazonLargeImgLightboxHandle = null;

                    var pos_in_parent = -1;
                    var self = this;
                    var rightHandle = null;
                    var chart = null;
                    var current_chart_kind = 'new_price';

                    var origin_height = 0;

                    var clearChat = function () {
                        if (chart !== null){
                            chart.clear();
                            chart = null;
                        }
                    }
                    var makeChart = function (data_kind) {

                        var flag_dates = [];
                        var values_by_date = [];
                        var data_provider = [];

                        for (var i = 0; i < row_data.price_rows.length; i ++){
                            var price_row = row_data.price_rows[i];
                            if (flag_dates[price_row.dtProductPrice_date] == true){
                                continue ;
                            }
                            flag_dates[price_row.dtProductPrice_date] = true;
                            var vals_filtered =  row_data.price_rows.filter(function (pr) {
                                return (pr.dtProductPrice_date == price_row.dtProductPrice_date);
                            });
                            var sums = {intProductPrice_newprice: 0, intProductPrice_usedprice: 0, intProductPrice_newquantity: 0, intProductPrice_usedquantity: 0};
                            vals_filtered.forEach(function (val_flt) {
                                sums.intProductPrice_newprice += (100 * val_flt.intProductPrice_newprice);
                                sums.intProductPrice_usedprice += (100 * val_flt.intProductPrice_usedprice);
                                sums.intProductPrice_newquantity += (1.0 * val_flt.intProductPrice_newquantity);
                                sums.intProductPrice_usedquantity += (1.0 * val_flt.intProductPrice_usedquantity);
                            });
                            var average = {
                                intProductPrice_newprice: Math.round(sums.intProductPrice_newprice / vals_filtered.length) / 100,
                                intProductPrice_usedprice: Math.round(sums.intProductPrice_usedprice / vals_filtered.length) / 100,
                                intProductPrice_newquantity: sums.intProductPrice_newquantity / vals_filtered.length,
                                intProductPrice_usedquantity: sums.intProductPrice_usedquantity / vals_filtered.length
                            };
                            switch (data_kind){
                                case 'new_price':
                                    values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_newprice;
                                    break;
                                case 'used_price':
                                    values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_usedprice;
                                    break;
                                case 'new_quantity':
                                    values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_newquantity;
                                    break;
                                case 'used_quantity':
                                    values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_usedquantity;
                                    break;
                            }
                        }

                        for (var price_date in values_by_date){
                            data_provider.push({"date": price_date, "value": values_by_date[price_date]});
                        }

                        chart = AmCharts.makeChart(dom_chart.attr('id'), {
                            "path": "assets/plugins/amchart/amcharts/",
                            "type": "serial",
                            "theme": "light",
                            "marginRight": 40,
                            "marginLeft": 40,
                            "autoMarginOffset": 20,
                            "mouseWheelZoomEnabled":true,
                            "dataDateFormat": "YYYY-MM-DD",
                            "valueAxes": [{
                                "id": "v1",
                                "axisAlpha": 0,
                                "position": "left",
                                "ignoreAxisWidth":true
                            }],
                            "balloon": {
                                "borderThickness": 1,
                                "shadowAlpha": 0
                            },
                            "graphs": [{
                                "id": "g1",
                                "balloon":{
                                    "drop":true,
                                    "adjustBorderColor":false,
                                    "color":"#ffffff"
                                },
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "title": "red line",
                                "useLineColorForBulletBorder": true,
                                "valueField": "value",
                                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
                            }],
                            // "chartScrollbar": {
                            //     "graph": "g1",
                            //     "oppositeAxis":false,
                            //     "offset":30,
                            //     "scrollbarHeight": 80,
                            //     "backgroundAlpha": 0,
                            //     "selectedBackgroundAlpha": 0.1,
                            //     "selectedBackgroundColor": "#888888",
                            //     "graphFillAlpha": 0,
                            //     "graphLineAlpha": 0.5,
                            //     "selectedGraphFillAlpha": 0,
                            //     "selectedGraphLineAlpha": 1,
                            //     "autoGridCount":true,
                            //     "color":"#AAAAAA"
                            // },
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha":1,
                                "cursorColor":"#258cbb",
                                "limitToGraph":"g1",
                                "valueLineAlpha":0.2,
                                "valueZoomable":true
                            },
                            // "valueScrollbar":{
                            //     "oppositeAxis":false,
                            //     "offset":50,
                            //     "scrollbarHeight":10
                            // },
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true,
                                "dashLength": 1,
                                "minorGridEnabled": true
                            },
                            "export": {
                                "enabled": true
                            },
                            "dataProvider": data_provider
                        });
                        chart.addListener("rendered", zoomChart);

                        zoomChart();

                        function zoomChart() {
                            chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
                        }
                    }
                    var bindEvent = function () {
                        dom_new_price.click(function () {
                            dom_prices_wrapper.find('.price-item').removeClass('active');
                            dom_new_price.addClass('active');
                            clearChat();
                            makeChart('new_price');
                            current_chart_kind = 'new_price';
                        });
                        dom_new_quantity.click(function () {
                            dom_prices_wrapper.find('.price-item').removeClass('active');
                            dom_new_quantity.addClass('active');
                            clearChat();
                            makeChart('new_quantity');
                            current_chart_kind = 'new_quantity';
                        });
                        dom_used_price.click(function () {
                            dom_prices_wrapper.find('.price-item').removeClass('active');
                            dom_used_price.addClass('active');
                            clearChat();
                            makeChart('used_price');
                            current_chart_kind = 'used_price';
                        });
                        dom_used_quantity.click(function () {
                            dom_prices_wrapper.find('.price-item').removeClass('active');
                            dom_used_quantity.addClass('active');
                            clearChat();
                            makeChart('used_quantity');
                            current_chart_kind = 'used_quantity';
                        });
                        dom_item_row.find('.product-basic-description-wrapper .description-wrapper .view-more-wrapper .view-more').click(function () {

                            var clone_description = dom_item_descriptoin.clone(true).css({height: 'auto', position: 'absolute', maxHeight: 'none', width: '100%', top: '10000px'});
                            clone_description.appendTo(dom_item_row.find('.product-basic-description-wrapper .description-wrapper'));

                            origin_height = clone_description.height();
                            clone_description.remove();

                            dom_item_row.find('.product-basic-description-wrapper .description-wrapper').addClass('expanded');
                            dom_item_descriptoin.css({maxHeight: origin_height});
                        });
                        dom_item_row.find('.product-basic-description-wrapper .description-wrapper .view-more-wrapper .view-less').click(function () {
                            dom_item_row.find('.product-basic-description-wrapper .description-wrapper').removeClass('expanded');
                            dom_item_descriptoin.css({maxHeight: '160px'});
                            setTimeout(function () {
                                $('html').animate({scrollTop: self.getScrollTop() + 'px'}, 600);
                            }, 0);
                        });
                        dom_item_row.find('.product-basic-description-wrapper .image-wrapper img').click(function () {

                            amazonLargeImgLightboxHandle.open();
                        });
                    };

                    this.removeRow = function () {
                        dom_item_row.remove();
                        itemRowHandles.splice(pos_in_parent, 1);
                    }
                    this.setRightHandle = function (newHandle) {
                        rightHandle = newHandle;
                    }
                    this.show = function () {
                        dom_item_row.prependTo(dom_item_rows_container);
                        makeChart('new_price');
                    }
                    this.updateInFront = function(new_data){
                        row_data = new_data;
                        latest_price_data = row_data.price_rows[row_data.price_rows.length - 1];
                        dom_item_title.html(row_data.strProduct_name);
                        dom_new_price.find('.item-value').html('$' + latest_price_data.intProductPrice_newprice);
                        dom_used_price.find('.item-value').html('$' + latest_price_data.intProductPrice_usedprice);
                        dom_new_quantity.find('.item-value').html(latest_price_data.intProductPrice_newquantity);
                        dom_used_quantity.find('.item-value').html(latest_price_data.intProductPrice_usedquantity);
                        dom_item_row.find('.amazon-link-wrapper a').attr('href', row_data.strProduct_amazonURL);
                        clearChat();
                        makeChart(current_chart_kind);
                    }
                    this.getScrollTop = function () {
                        return dom_item_row.offset().top;
                    }
                    this.setPosInParent = function(new_pos){
                        pos_in_parent = new_pos;
                    }
                    this.init = function () {
                        dom_item_title.html(row_data.strProduct_name);
                        dom_item_image.attr('src', row_data.strProduct_image);
                        dom_item_descriptoin.html(row_data.strProduct_description);
                        dom_new_price.find('.item-value').html('$' + latest_price_data.intProductPrice_newprice);
                        dom_used_price.find('.item-value').html('$' + latest_price_data.intProductPrice_usedprice);
                        dom_new_quantity.find('.item-value').html(latest_price_data.intProductPrice_newquantity);
                        dom_used_quantity.find('.item-value').html(latest_price_data.intProductPrice_usedquantity);
                        dom_item_row.find('.amazon-link-wrapper a').attr('href', row_data.strProduct_amazonURL);

                        var clone_description = dom_item_descriptoin.clone(true).css({height: 'auto', position: 'absolute', maxHeight: 'none', width: '100%', top: '10000px'});
                        clone_description.appendTo(dom_item_row.find('.product-basic-description-wrapper .description-wrapper'));
                        dom_item_descriptoin.css({maxHeight: '160px'});
                        setTimeout(function () {
                            origin_height = clone_description.height();
                            if(origin_height <= 160){
                                dom_item_row.find('.product-basic-description-wrapper .description-wrapper .view-more-wrapper').remove();
                            }
                            clone_description.remove();
                        }, 1000);

                        amazonLargeImgLightboxHandle = new SiteLightboxClass({
                            domLightboxMainContent: $('<img src="'+ row_data.strProduct_large_img +'"/>'),
                            mainContentHdlClass: function (lightboxRootHandle, domLightboxContent) {this.init = function () {}},
                            domFrom: dom_item_image,
                            domClosingTo: dom_item_image,
                            openDuration: 300,
                            closeDuration: 400,
                            isFlexWidth: true,
                            lightboxType: 'cssMain'
                        });
                        amazonLargeImgLightboxHandle.init();
                        bindEvent();
                    }
                }

                this.addItem = function (row_data) {
                    var newHandle = new itemRowClass(row_data);
                    newHandle.init();
                    newHandle.setPosInParent(itemRowHandles.length);
                    itemRowHandles.push(newHandle);
                    return newHandle;
                }
                this.init = function () {

                }
            }

            this.init = function () {
                watchListHandle = new watchListClass();
                leftPanelHandle = new leftPanelClass();
                leftPanelHandle.init();
                watchListHandle.init();
            }
        }
        var GoalsSectionClass = function () {
            var domRoot = $('main.main-content');

            var goalsInitHandle, goalsMainHandle, goalsMorEvnHandle, goalsFirstPopupHdl;
            var GoalsInitClass = function () {
                var domInitRoot;
                var domGratefulForm, domNormalForm, domTrapForm;

                var flgGrate, flgNormal, flgTrap;

                var createGrateGoal = function () {
                    var text = domGratefulForm.find('textarea').val();
                    goalsMainHandle.grateGoalsHandle.insertGoal(text).then(function () {
                        flgGrate = true;
                        refresh();
                    });
                }
                var createNormalGoal = function () {
                    var text = domNormalForm.find('textarea').val();
                    goalsMainHandle.normalGoalsHandle.insertGoal(text).then(function () {
                        flgNormal = true;
                        refresh();
                    })
                }
                var createTrapGoal = function () {
                    var text = domTrapForm.find('textarea').val();
                    goalsMainHandle.trapGoalsHandle.insertGoal(text).then(function () {
                        flgTrap = true;
                        refresh();
                    })
                };

                var refresh = function () {
                    if (flgGrate){
                        domGratefulForm.remove();
                    }
                    if (flgNormal){
                        domNormalForm.remove();
                    }
                    if (flgTrap){
                        domTrapForm.remove();
                    }
                    if (flgGrate && flgNormal && flgTrap){
                        domInitRoot.remove();
                    }
                }

                var bindEvents = function () {
                    domGratefulForm.submit(function () {
                        createGrateGoal();
                        return false;
                    });
                    domNormalForm.submit(function () {
                        createNormalGoal();
                        return false;
                    });
                    domTrapForm.submit(function () {
                        createTrapGoal();
                        return false;
                    });

                }

                this.init = function () {
                    domInitRoot = domRoot.find('.goals-init');
                    domGratefulForm = domInitRoot.find('.step-1');
                    domNormalForm = domInitRoot.find('.step-2');
                    domTrapForm = domInitRoot.find('.step-3');

                    flgGrate = initialGoals.grateGoals.length > 0;
                    flgNormal = initialGoals.normalGoals.length > 0;
                    flgTrap = initialGoals.trapGoals.length > 0;

                    refresh();
                    if (!(flgTrap && flgGrate && flgNormal)){
                        domInitRoot.show();
                    }
                    bindEvents();
                }
            }

            var GoalsMainClass = function () {
                var domMainRoot = domRoot.find('.goals-main');

                var goalsData = initialGoals;
                var baseDate;
                var normalGoalsHandle, grateGoalsHandle, trapGoalsHandle, self = this;

                var beforeDate = function(baseDate){
                    var yesterday = baseDate.stringToDate();
                    yesterday.setDate(yesterday.getDate() - 1);
                    return yesterday.yyyymmdd();
                }
                var nextDate = function(baseDate){
                    var tomorrow = baseDate.stringToDate();
                    tomorrow.setDate(tomorrow.getDate() + 1);
                    return tomorrow.yyyymmdd();
                }
                var todayDate = function () {
                    // var today = new Date();
                    // return today.yyyymmdd();
                    return serverToday;
                }
                var formatDate = function (strDate) {
                    var d = strDate.stringToDate();
                    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var mm = d.getMonth() + 1; // getMonth() is zero-based
                    var dd = d.getDate();

                    return dayNames[d.getDay()] + ', ' + [
                        (mm>9 ? '' : '0') + mm,
                        (dd>9 ? '' : '0') + dd,
                        d.getFullYear()
                    ].join('/');
                }

                var NormalGoalsClass = function () {
                    var domBlockRoot, domItemList;
                    var domProgressRoot, domProgressList;

                    var goals = goalsData.normalGoals;

                    var itemHandles = [], self = this;

                    var ItemClass = function (itemData) {
                        var domItemRoot, domProgress;

                        var self = this;
                        var goalNumber = 1;

                        var bindEvents = function () {
                            domItemRoot.find('.delete-goal').click(function () {
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this goal?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.delete();
                                    }
                                });
                            });
                            domItemRoot.find('.edit-entry').click(function () {
                                domItemRoot.find('.goal-text-wrapper').addClass('edit-status');
                            });
                            domItemRoot.find('.back-to-origin').click(function () {
                                domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                            });
                            domItemRoot.find('form.update-form').submit(function () {
                                var text = domItemRoot.find('textarea[name=goal_text]').val();
                                var sets = {strGoal_text: text};
                                self.update(sets).then(function () {
                                    domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                                });
                                return false;
                            });
                            domItemRoot.find('.toogle-complete').click(function () {
                                var complete = itemData.boolGoal_complete ? 0 : 1;
                                var sets = {boolGoal_complete: complete};
                                self.update(sets);
                            });
                            domItemRoot.find('.for-tomorrow-wrapper svg').click(function () {

                                var today = todayDate();
                                var tomorrow = nextDate(today);

                                var sets;

                                if(tomorrow == itemData.dtGoal_date){
                                    sets = {dtGoal_date: today};
                                }
                                else {
                                    sets = {dtGoal_date: tomorrow};
                                }
                                self.update(sets);
                            })
                        }
                        this.id = function () {
                            return itemData.goal_ID;
                        }
                        this.order = function () {
                            return parseInt(itemData.intGoal_order);
                        }
                        this.setOrder = function (newOrder) {
                            itemData.intGoal_order = newOrder;
                        }
                        this.setGoalNumber = function (n) {
                            goalNumber = n;
                            domItemRoot.find('.goal-number').html(goalNumber);
                            domProgress.find('.goal-number').html(goalNumber);
                        }
                        this.appendProgress = function () {
                            domProgress.appendTo(domProgressList);
                        }
                        this.detachProgress = function(){
                            domProgress.detach();
                        }
                        this.appendDom = function () {
                            domItemRoot.appendTo(domItemList);
                        }
                        this.detachDom = function () {
                            domItemRoot.detach();
                        }
                        this.delete = function () {
                            var where = {
                                goal_ID: self.id(),
                            };
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'delete', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        itemHandles.forEach(function (hdl, i) {
                                            if (hdl.id() == self.id()){
                                                itemHandles.splice(i, 1);
                                                domItemRoot.remove();
                                                domProgress.remove();
                                            }
                                        });
                                        normalGoalsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.remove = function () {
                            domItemRoot.remove();
                            domProgress.remove();
                        }
                        this.refresh = function () {
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                            domItemRoot.find('textarea[name=goal_text]').html(itemData.strGoal_text);
                            if (itemData.boolGoal_complete){
                                domItemRoot.find('.toogle-complete').addClass('btn-success');
                                domProgress.addClass('completed');
                            }
                            else {
                                domItemRoot.find('.toogle-complete').removeClass('btn-success');
                                domProgress.removeClass('completed');
                            }

                            var nextD = nextDate(baseDate);
                            if (itemData.dtGoal_date == nextD){
                                domItemRoot.addClass('for-tomorrow');
                            }
                            else {
                                domItemRoot.removeClass('for-tomorrow');
                            }
                            domProgress.find('.goal-number').html(goalNumber);
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                        }
                        this.update = function(sets){
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: {goal_ID: self.id()}, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(itemData, sets);
                                        self.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.updateInFront = function (sets) {
                            $.extend(itemData, sets);
                            self.refresh();
                        }
                        this.init = function () {
                            domItemRoot = domBlockRoot.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemList);
                            domProgress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number"></span>.</li>'));
                            domItemRoot.data('controlHandle', self);
                            self.refresh();
                            bindEvents();
                        }
                    }

                    var updateItemsOrder = function () {

                        var hdls = [], orders = [];

                        domItemList.find('li.goal-item').each(function (i, dom) {
                            var hdl = $(dom).data('controlHandle');
                            orders.push(hdl.order());
                            hdls.push(hdl);
                        });
                        orders.sort(function (a, b) {
                            return a - b;
                        });
                        var updateSets = [];

                        for (var i = 0; i < hdls.length; i ++){
                            var newOrder = orders[i];
                            var handle = hdls[i];
                            if (newOrder !== handle.order()){
                                updateSets.push({where: handle.id(), sets: {intGoal_order: newOrder}});
                                handle.setOrder(newOrder);
                            }
                        }
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'update_goals', updateSets: updateSets},
                            success: function (res) {
                                if (res.status == true){
                                    self.refresh();
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvents = function () {
                        domBlockRoot.find('form.add-goal-form').submit(function () {
                            var text = domBlockRoot.find('form.add-goal-form textarea').val();
                            self.insertGoal(text).then(domBlockRoot.find('form.add-goal-form textarea').val(''));
                            return false;
                        });
                        domItemList.on('sortupdate', function (e, ui) {
                            updateItemsOrder();
                        });
                    }

                    this.setData = function (data) {

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                            domBlockRoot.find('.display-date').html('Today');
                            domBlockRoot.find('.display-title').html('I will...');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                            domBlockRoot.find('.display-date').html(formatDate(baseDate));
                            if (baseDate < todayDate()){
                                domBlockRoot.find('.display-title').html('I was...');
                            }
                            else {
                                domBlockRoot.find('.display-title').html('I will...');
                            }
                        }

                        goals = data.goals;
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });

                        itemHandles.forEach(function (hdl) {
                            hdl.remove();
                        });
                        itemHandles = [];
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                    }
                    this.refresh = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendProgress();
                        });
                    }
                    this.refreshAll = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachDom();
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendDom();
                            hdl.appendProgress();
                        });
                    }
                    this.insertGoal = function (goalText) {
                        var sets = {
                            strGoal_text: goalText,
                            intGoal_type: 2,
                            dtGoal_date: todayDate()
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'insert', sets: sets},
                            success: function (res) {
                                if (res.status == true){
                                    self.insertInFront(res.data);
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.insertInFront = function (goalData) {
                        var handle = new ItemClass(goalData);
                        handle.init();
                        handle.setGoalNumber(itemHandles.length + 1);
                        handle.appendProgress();
                        itemHandles.push(handle);
                    }
                    this.updateInFront = function (sets, id) {
                        itemHandles.forEach(function (hdl) {
                            if (id == hdl.id()){
                                hdl.updateInFront(sets);
                            }
                        });
                        normalGoalsHandle.refresh();
                    }
                    this.deleteInFront = function (id) {
                        itemHandles.forEach(function (hdl, i) {
                            if (id == hdl.id()){
                                hdl.remove();
                                itemHandles.splice(i, 1);
                            }
                        });
                        normalGoalsHandle.refresh();
                    }
                    this.init = function () {
                        domBlockRoot = domMainRoot.find('.normal-goals-wrapper');
                        domItemList = domBlockRoot.find('.item-list');
                        domProgressRoot = domBlockRoot.find('.goals-process');
                        domProgressList = domProgressRoot.find('.progress-indicator');

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                        }
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                        domItemList.sortable({
                            items: 'li.goal-item',
                            handle: '.drag-icon',
                            update: function (a, b) {
                            },
                            start: function(e, ui){
                                var height = parseInt(ui.helper.css('height')) - 2 + 'px';
                                ui.placeholder.height(height);
                            },
                            placeholder: 'ui-state-highlight',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            opacity: 0.9
                        });
                        bindEvents();
                    }
                }

                var GrateGoalsClass = function () {
                    var domBlockRoot, domItemList;
                    var domProgressRoot, domProgressList;

                    var goals = goalsData.grateGoals;

                    var itemHandles = [], self = this;

                    var ItemClass = function (itemData) {
                        var domItemRoot, domProgress;

                        var self = this;
                        var goalNumber = 1;

                        var bindEvents = function () {
                            domItemRoot.find('.delete-goal').click(function () {
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this goal?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.delete();
                                    }
                                });
                            });
                            domItemRoot.find('.edit-entry').click(function () {
                                domItemRoot.find('.goal-text-wrapper').addClass('edit-status');
                            });
                            domItemRoot.find('.back-to-origin').click(function () {
                                domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                            });
                            domItemRoot.find('form.update-form').submit(function () {
                                var text = domItemRoot.find('textarea[name=goal_text]').val();
                                var sets = {strGoal_text: text};
                                self.update(sets).then(function () {
                                    domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                                });
                                return false;
                            });
                            domItemRoot.find('.toogle-complete').click(function () {
                                var complete = itemData.boolGoal_complete ? 0 : 1;
                                var sets = {boolGoal_complete: complete};
                                self.update(sets);
                            });
                            domItemRoot.find('.for-tomorrow-wrapper svg').click(function () {

                                var today = todayDate();
                                var tomorrow = nextDate(today);

                                var sets;

                                if(tomorrow == itemData.dtGoal_date){
                                    sets = {dtGoal_date: today};
                                }
                                else {
                                    sets = {dtGoal_date: tomorrow};
                                }
                                self.update(sets);
                            })
                        }
                        this.id = function () {
                            return itemData.goal_ID;
                        }
                        this.order = function () {
                            return parseInt(itemData.intGoal_order);
                        }
                        this.setOrder = function (newOrder) {
                            itemData.intGoal_order = newOrder;
                        }
                        this.setGoalNumber = function (n) {
                            goalNumber = n;
                            domItemRoot.find('.goal-number').html(goalNumber);
                            domProgress.find('.goal-number').html(goalNumber);
                        }
                        this.appendProgress = function () {
                            domProgress.appendTo(domProgressList);
                        }
                        this.detachProgress = function(){
                            domProgress.detach();
                        }
                        this.delete = function () {
                            var where = {
                                goal_ID: self.id(),
                            };
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'delete', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        itemHandles.forEach(function (hdl, i) {
                                            if (hdl.id() == self.id()){
                                                itemHandles.splice(i, 1);
                                                domItemRoot.remove();
                                                domProgress.remove();
                                            }
                                        });
                                        normalGoalsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.remove = function () {
                            domItemRoot.remove();
                            domProgress.remove();
                        }
                        this.refresh = function () {
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                            domItemRoot.find('textarea[name=goal_text]').html(itemData.strGoal_text);
                            if (itemData.boolGoal_complete){
                                domItemRoot.find('.toogle-complete').addClass('btn-success');
                                domProgress.addClass('completed');
                            }
                            else {
                                domItemRoot.find('.toogle-complete').removeClass('btn-success');
                                domProgress.removeClass('completed');
                            }

                            var nextD = nextDate(baseDate);
                            if (itemData.dtGoal_date == nextD){
                                domItemRoot.addClass('for-tomorrow');
                            }
                            else {
                                domItemRoot.removeClass('for-tomorrow');
                            }
                            domProgress.find('.goal-number').html(goalNumber);
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                        }
                        this.update = function(sets){
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: {goal_ID: self.id()}, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(itemData, sets);
                                        self.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.init = function () {
                            domItemRoot = domBlockRoot.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemList);
                            domProgress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number"></span>.</li>'));
                            domItemRoot.data('controlHandle', self);
                            self.refresh();
                            bindEvents();
                        }
                    }

                    var updateItemsOrder = function () {

                        var hdls = [], orders = [];

                        domItemList.find('li.goal-item').each(function (i, dom) {
                            var hdl = $(dom).data('controlHandle');
                            orders.push(hdl.order());
                            hdls.push(hdl);
                        });
                        orders.sort(function (a, b) {
                            return a - b;
                        });
                        var updateSets = [];

                        for (var i = 0; i < hdls.length; i ++){
                            var newOrder = orders[i];
                            var handle = hdls[i];
                            if (newOrder !== handle.order()){
                                updateSets.push({where: handle.id(), sets: {intGoal_order: newOrder}});
                                handle.setOrder(newOrder);
                            }
                        }
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'update_goals', updateSets: updateSets},
                            success: function (res) {
                                if (res.status == true){
                                    self.refresh();
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvents = function () {
                        domBlockRoot.find('form.add-goal-form').submit(function () {
                            var text = domBlockRoot.find('form.add-goal-form textarea').val();
                            self.insertGoal(text).then(domBlockRoot.find('form.add-goal-form textarea').val(''));
                            return false;
                        });
                        domItemList.on('sortupdate', function (e, ui) {
                            updateItemsOrder();
                        });
                    }

                    this.setData = function (data) {

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                            domBlockRoot.find('.display-date').html('Today');
                            domBlockRoot.find('.display-title').html('I will be grateful for...');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                            domBlockRoot.find('.display-date').html(formatDate(baseDate));
                            if (baseDate < todayDate()){
                                domBlockRoot.find('.display-title').html('I was grateful for...');
                            }
                            else {
                                domBlockRoot.find('.display-title').html('I will be grateful for...');
                            }
                        }

                        goals = data.goals;
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });

                        itemHandles.forEach(function (hdl) {
                            hdl.remove();
                        });
                        itemHandles = [];
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                    }
                    this.refresh = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendProgress();
                        });
                    }
                    this.insertGoal = function (goalText) {
                        var sets = {
                            strGoal_text: goalText,
                            intGoal_type: 1,
                            dtGoal_date: todayDate()
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'insert', sets: sets},
                            success: function (res) {
                                if (res.status == true){
                                    var handle = new ItemClass(res.data);
                                    handle.init();
                                    handle.setGoalNumber(itemHandles.length + 1);
                                    handle.appendProgress();
                                    itemHandles.push(handle);
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.init = function () {
                        domBlockRoot = domMainRoot.find('.grate-goals-wrapper');
                        domItemList = domBlockRoot.find('.item-list');
                        domProgressRoot = domBlockRoot.find('.goals-process');
                        domProgressList = domProgressRoot.find('.progress-indicator');

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                        }
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                        domItemList.sortable({
                            items: 'li.goal-item',
                            handle: '.drag-icon',
                            update: function (a, b) {
                            },
                            start: function(e, ui){
                                var height = parseInt(ui.helper.css('height')) - 2 + 'px';
                                ui.placeholder.height(height);
                            },
                            placeholder: 'ui-state-highlight',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            opacity: 0.9
                        });
                        bindEvents();
                    }
                }

                var TrapGoalsClass = function () {
                    var domBlockRoot, domItemList;
                    var domProgressRoot, domProgressList;

                    var goals = goalsData.trapGoals;

                    var itemHandles = [], self = this;

                    var ItemClass = function (itemData) {
                        var domItemRoot, domProgress;

                        var self = this;
                        var goalNumber = 1;

                        var bindEvents = function () {
                            domItemRoot.find('.delete-goal').click(function () {
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this goal?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.delete();
                                    }
                                });
                            });
                            domItemRoot.find('.edit-entry').click(function () {
                                domItemRoot.find('.goal-text-wrapper').addClass('edit-status');
                            });
                            domItemRoot.find('.back-to-origin').click(function () {
                                domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                            });
                            domItemRoot.find('form.update-form').submit(function () {
                                var text = domItemRoot.find('textarea[name=goal_text]').val();
                                var sets = {strGoal_text: text};
                                self.update(sets).then(function () {
                                    domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                                });
                                return false;
                            });
                            domItemRoot.find('.toogle-complete').click(function () {
                                var complete = itemData.boolGoal_complete ? 0 : 1;
                                var sets = {boolGoal_complete: complete};
                                self.update(sets);
                            });
                            domItemRoot.find('.for-tomorrow-wrapper svg').click(function () {

                                var today = todayDate();
                                var tomorrow = nextDate(today);

                                var sets;

                                if(tomorrow == itemData.dtGoal_date){
                                    sets = {dtGoal_date: today};
                                }
                                else {
                                    sets = {dtGoal_date: tomorrow};
                                }
                                self.update(sets);
                            })
                        }
                        this.id = function () {
                            return itemData.goal_ID;
                        }
                        this.order = function () {
                            return parseInt(itemData.intGoal_order);
                        }
                        this.setOrder = function (newOrder) {
                            itemData.intGoal_order = newOrder;
                        }
                        this.setGoalNumber = function (n) {
                            goalNumber = n;
                            domItemRoot.find('.goal-number').html(goalNumber);
                            domProgress.find('.goal-number').html(goalNumber);
                        }
                        this.appendProgress = function () {
                            domProgress.appendTo(domProgressList);
                        }
                        this.detachProgress = function(){
                            domProgress.detach();
                        }
                        this.delete = function () {
                            var where = {
                                goal_ID: self.id(),
                            };
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'delete', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        itemHandles.forEach(function (hdl, i) {
                                            if (hdl.id() == self.id()){
                                                itemHandles.splice(i, 1);
                                                domItemRoot.remove();
                                                domProgress.remove();
                                            }
                                        });
                                        normalGoalsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.remove = function () {
                            domItemRoot.remove();
                            domProgress.remove();
                        }
                        this.refresh = function () {
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                            domItemRoot.find('textarea[name=goal_text]').html(itemData.strGoal_text);
                            if (itemData.boolGoal_complete){
                                domItemRoot.find('.toogle-complete').addClass('btn-success');
                                domProgress.addClass('completed');
                            }
                            else {
                                domItemRoot.find('.toogle-complete').removeClass('btn-success');
                                domProgress.removeClass('completed');
                            }

                            var nextD = nextDate(baseDate);
                            if (itemData.dtGoal_date == nextD){
                                domItemRoot.addClass('for-tomorrow');
                            }
                            else {
                                domItemRoot.removeClass('for-tomorrow');
                            }
                            domProgress.find('.goal-number').html(goalNumber);
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                        }
                        this.update = function(sets){
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: {goal_ID: self.id()}, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(itemData, sets);
                                        self.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.init = function () {
                            domItemRoot = domBlockRoot.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemList);
                            domProgress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number"></span>.</li>'));
                            domItemRoot.data('controlHandle', self);
                            self.refresh();
                            bindEvents();
                        }
                    }

                    var updateItemsOrder = function () {

                        var hdls = [], orders = [];

                        domItemList.find('li.goal-item').each(function (i, dom) {
                            var hdl = $(dom).data('controlHandle');
                            orders.push(hdl.order());
                            hdls.push(hdl);
                        });
                        orders.sort(function (a, b) {
                            return a - b;
                        });
                        var updateSets = [];

                        for (var i = 0; i < hdls.length; i ++){
                            var newOrder = orders[i];
                            var handle = hdls[i];
                            if (newOrder !== handle.order()){
                                updateSets.push({where: handle.id(), sets: {intGoal_order: newOrder}});
                                handle.setOrder(newOrder);
                            }
                        }
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'update_goals', updateSets: updateSets},
                            success: function (res) {
                                if (res.status == true){
                                    self.refresh();
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvents = function () {
                        domBlockRoot.find('form.add-goal-form').submit(function () {
                            var text = domBlockRoot.find('form.add-goal-form textarea').val();
                            self.insertGoal(text).then(domBlockRoot.find('form.add-goal-form textarea').val(''));
                            return false;
                        });
                        domItemList.on('sortupdate', function (e, ui) {
                            updateItemsOrder();
                        });
                    }

                    this.setData = function (data) {

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                            domBlockRoot.find('.display-date').html('Today');
                            domBlockRoot.find('.display-title').html('I will avoid the trap of...');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                            domBlockRoot.find('.display-date').html(formatDate(baseDate));
                            if (baseDate < todayDate()){
                                domBlockRoot.find('.display-title').html('I did avoid the trap of...');
                            }
                            else {
                                domBlockRoot.find('.display-title').html('I will avoid the trap of...');
                            }
                        }

                        goals = data.goals;
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });

                        itemHandles.forEach(function (hdl) {
                            hdl.remove();
                        });
                        itemHandles = [];
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                    }
                    this.refresh = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendProgress();
                        });
                    }
                    this.insertGoal = function (goalText) {
                        var sets = {
                            strGoal_text: goalText,
                            intGoal_type: 3,
                            dtGoal_date: todayDate()
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'insert', sets: sets},
                            success: function (res) {
                                if (res.status == true){
                                    var handle = new ItemClass(res.data);
                                    handle.init();
                                    handle.setGoalNumber(itemHandles.length + 1);
                                    handle.appendProgress();
                                    itemHandles.push(handle);
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.init = function () {
                        domBlockRoot = domMainRoot.find('.trap-goals-wrapper');
                        domItemList = domBlockRoot.find('.item-list');
                        domProgressRoot = domBlockRoot.find('.goals-process');
                        domProgressList = domProgressRoot.find('.progress-indicator');

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                        }
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                        domItemList.sortable({
                            items: 'li.goal-item',
                            handle: '.drag-icon',
                            update: function (a, b) {
                            },
                            start: function(e, ui){
                                var height = parseInt(ui.helper.css('height')) - 2 + 'px';
                                ui.placeholder.height(height);
                            },
                            placeholder: 'ui-state-highlight',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            opacity: 0.9
                        });
                        bindEvents();
                    }
                }

                var setBaseDate = function (newBaseDate) {
                    baseDate = newBaseDate;
                    $.ajax({
                        url: API_ROOT_URL + '/Goals.php',
                        data: {action: 'get_goals_data_by_date', date: baseDate},
                        success: function (res) {
                            if (res.status == true){
                                var data = res.data;
                                normalGoalsHandle.setData({goals: data.normalGoals, date: baseDate});
                                // grateGoalsHandle.setData({goals: data.grateGoals, date: baseDate});
                                // trapGoalsHandle.setData({goals: data.trapGoals, date: baseDate});
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                var bindEvents = function () {
                    domMainRoot.find('.step-next').click(function () {
                        var nextD = nextDate(baseDate);
                        setBaseDate(nextD);
                    });
                    domMainRoot.find('.step-before').click(function () {
                        var beforeD = beforeDate(baseDate);
                        setBaseDate(beforeD);
                    });
                }

                this.scrollFocus = function () {
                    var currentScrollTop = $('html').scrollTop();
                    var scrollTop = domMainRoot.offset().top - parseInt(siteHeaderHandle.height());
                    var dur = 200 + Math.abs(scrollTop  - currentScrollTop) / 10;
                    setTimeout(function () {
                        $('html').animate({scrollTop: scrollTop + 'px'}, dur);
                    }, 0);
                }

                this.init = function () {
                    baseDate = todayDate();
                    normalGoalsHandle = new NormalGoalsClass();
                    normalGoalsHandle.init();

                    // grateGoalsHandle = new GrateGoalsClass();
                    // grateGoalsHandle.init();
                    //
                    // trapGoalsHandle = new TrapGoalsClass();
                    // trapGoalsHandle.init();

                    self.normalGoalsHandle = normalGoalsHandle;
                    // self.grateGoalsHandle = grateGoalsHandle;
                    // self.trapGoalsHandle = trapGoalsHandle;

                    bindEvents();
                }
            }

            var GoalsMorEvnClass = function () {
                var domMorEvnRoot = domRoot.find('.goals-morevn');

                var morningHandle, eveningHandle;

                var formatDate = function(){

                    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nobember', 'December'];
                    var today = serverToday;
                    var dt = today.stringToDate();
                    var yyyy = dt.getFullYear();
                    var mm = monthNames[dt.getMonth()]; // getMonth() is zero-based
                    var dd = dt.getDate();
                    var dayName = dayNames[dt.getDay()];
                    return dayName + ', ' + mm + ' ' + dd + ', ' + yyyy;
                }

                var MorningClass = function () {
                    var domPage, domTextWrp, domText;

                    var morningGoal = initialGoals.morningGoal;

                    var self = this, popupHandle;

                    var PopupClass = function (lightboxRootHandle, domLightboxContent) {

                        var domForm = domLightboxContent.find('form.morevn-form');
                        var self = this;

                        var helperRow = false;

                        var bindEvents = function () {
                            domForm.submit(function () {
                                var text = domForm.find('textarea[name="morevn_text"]').val();
                                if (morningGoal){
                                    self.update({strGoal_text: text}).then(function () {
                                        lightboxRootHandle.cancelClose();
                                    });
                                }
                                else {
                                    self.insert({strGoal_text: text, intGoal_type: 4}).then(function () {
                                        lightboxRootHandle.cancelClose();
                                    });
                                }
                                return false;
                            });
                            domForm.find('.btn.cancel-btn').click(function () {
                                lightboxRootHandle.cancelClose();
                            });
                            domLightboxContent.find('.helper-tip-btn').click(function () {
                                self.makeRandHelper();
                            });
                        }
                        this.makeRandHelper = function () {
                            var where = {
                                strHelper_key: 'morning_page'
                            };
                            if (helperRow){
                                where['helper_ID !='] = helperRow.helper_ID;
                            }
                            return $.ajax({
                                url: API_ROOT_URL + '/Helpers.php',
                                data: {action: 'rand', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        if (res.data){
                                            helperRow = res.data;
                                            var helperText = res.data.strHelper_value;
                                            domLightboxContent.find('.helper-tip-text').html(helperText);
                                        }
                                        else {

                                        }
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.insert = function (sets) {
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'insert', sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        morningGoal = res.data;
                                        self.refresh();
                                        morningHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.update = function (sets) {
                            var where = {goal_ID: morningGoal.goal_ID};
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: where, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(morningGoal, sets);
                                        self.refresh();
                                        morningHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.refresh = function () {
                            domForm.find('textarea[name="morevn_text"]').html(morningGoal.strGoal_text);
                            domLightboxContent.find('.popup-content-header .display-date').html(formatDate());
                        }
                        this.init = function () {
                            self.refresh();
                            bindEvents();
                        }
                    };

                    var bindEvents = function () {
                        domPage.on('click', function(e) {
                            if (e.target.hasAttribute('data-readmore-toggle')){
                                return;
                            }
                            popupHandle.open();
                        });
                    }
                    this.refresh = function () {
                        if (morningGoal){
                            domTextWrp.find('.hint').remove();
                            domTextWrp.find('.page-text').html(morningGoal.strGoal_text);
                            domPage.find('.toogle-complete').show();
                        }
                        else {
                        }
                        domText.readmore({
                            speed: 300,
                            collapsedHeight: 204,
                            moreLink: '<a href="javascript:;">continue reading</a>',
                            lessLink: '<a href="javascript:;">View Less</a>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTextWrp.toggleClass('collapsed');
                            }
                        });
                    }
                    this.init = function () {
                        domPage = domMorEvnRoot.find('.morning-pages');
                        domTextWrp = domPage.find('.page-text-wrapper');
                        domText = domTextWrp.find('.page-text');
                        popupHandle = new SiteLightboxClass({
                            domLightboxMainContent: $('.goal-page-popup-content.sample').clone().removeClass('.sample').removeAttr('hidden'),
                            mainContentHdlClass: PopupClass,
                            lightboxType: 'cssMain',
                            openedCss: {
                                width: 1300
                            }
                        });
                        popupHandle.init();
                        bindEvents();
                        self.refresh();
                        domTextWrp.toggleClass('collapsed');
                    }
                }
                var EveningClass = function () {
                    var domPage, domTextWrp, domText;

                    var eveningGoal = initialGoals.eveningGoal;

                    var self = this, popupHandle;

                    var PopupClass = function (lightboxRootHandle, domLightboxContent) {

                        var domForm = domLightboxContent.find('form.morevn-form');
                        var self = this;

                        var helperRow = false;

                        var bindEvents = function () {
                            domForm.submit(function () {
                                var text = domForm.find('textarea[name="morevn_text"]').val();
                                if (eveningGoal){
                                    self.update({strGoal_text: text}).then(function () {
                                        lightboxRootHandle.cancelClose();
                                    });
                                }
                                else {
                                    self.insert({strGoal_text: text, intGoal_type: 5}).then(function () {
                                        lightboxRootHandle.cancelClose();
                                    });
                                }
                                return false;
                            });
                            domForm.find('.btn.cancel-btn').click(function () {
                                lightboxRootHandle.cancelClose();
                            });
                            domLightboxContent.find('.helper-tip-btn').click(function () {
                                self.makeRandHelper();
                            });
                        }
                        this.makeRandHelper = function () {
                            var where = {
                                strHelper_key: 'evening_page'
                            };
                            if (helperRow){
                                where['helper_ID !='] = helperRow.helper_ID;
                            }
                            return $.ajax({
                                url: API_ROOT_URL + '/Helpers.php',
                                data: {action: 'rand', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        if (res.data){
                                            helperRow = res.data;
                                            var helperText = res.data.strHelper_value;
                                            domLightboxContent.find('.helper-tip-text').html(helperText);
                                        }
                                        else {

                                        }
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.insert = function (sets) {
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'insert', sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        eveningGoal = res.data;
                                        self.refresh();
                                        eveningHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.update = function (sets) {
                            var where = {goal_ID: eveningGoal.goal_ID};
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: where, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(eveningGoal, sets);
                                        self.refresh();
                                        eveningHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.refresh = function () {
                            var hText = 'My Evening Reflection Page';
                            var tipText = 'The evening reflection is your chance to create closure for your day. You might reflect on your accomplishments, the things you want to do better, the things you want to get done tomorrow, or anything else that will close the day positively.';
                            domLightboxContent.find('.popup-content-header .display-title').html(hText);
                            domLightboxContent.find('.popup-content-header .display-date').html(formatDate());
                            domLightboxContent.find('.helper-tip-text').html(tipText);
                            domForm.find('textarea[name="morevn_text"]').html(eveningGoal.strGoal_text);
                        }
                        this.init = function () {
                            self.refresh();
                            bindEvents();
                        }
                    };

                    var bindEvents = function () {
                        domPage.on('click', function(e) {
                            if (e.target.hasAttribute('data-readmore-toggle')){
                                return;
                            }
                            popupHandle.open();
                        });
                    }
                    this.refresh = function () {
                        if (eveningGoal){
                            domTextWrp.find('.hint').remove();
                            domTextWrp.find('.page-text').html(eveningGoal.strGoal_text);
                            domPage.find('.toogle-complete').show();
                        }
                        else {
                        }
                        domText.readmore({
                            speed: 300,
                            collapsedHeight: 204,
                            moreLink: '<a href="javascript:;">continue reading</a>',
                            lessLink: '<a href="javascript:;">View Less</a>',
                            embedCSS: false,
                            beforeToggle: function () {
                            },
                            afterToggle: function () {
                                domTextWrp.toggleClass('collapsed');
                            }
                        });
                    }
                    this.init = function () {
                        domPage = domMorEvnRoot.find('.evening-pages');
                        domTextWrp = domPage.find('.page-text-wrapper');
                        domText = domTextWrp.find('.page-text');
                        popupHandle = new SiteLightboxClass({
                            domLightboxMainContent: $('.goal-page-popup-content.sample').clone().removeClass('.sample').removeAttr('hidden'),
                            mainContentHdlClass: PopupClass,
                            lightboxType: 'cssMain',
                            openedCss: {
                                width: 1300
                            }
                        });
                        popupHandle.init();
                        bindEvents();
                        self.refresh();
                        domTextWrp.toggleClass('collapsed');
                    }
                }

                this.init = function () {
                    morningHandle = new MorningClass();
                    morningHandle.init();
                    eveningHandle = new EveningClass();
                    eveningHandle.init();
                }
            }

            var GoalsFirstPopupContentClass = function (lightboxRootHandle, domLightboxContent) {
                var domMainRoot = domLightboxContent.find('.goals-main');

                var goalsData = initialGoals;
                var baseDate;
                var normalGoalsHandle, grateGoalsHandle, trapGoalsHandle, self = this;

                var beforeDate = function(baseDate){
                    var yesterday = baseDate.stringToDate();
                    yesterday.setDate(yesterday.getDate() - 1);
                    return yesterday.yyyymmdd();
                }
                var nextDate = function(baseDate){
                    var tomorrow = baseDate.stringToDate();
                    tomorrow.setDate(tomorrow.getDate() + 1);
                    return tomorrow.yyyymmdd();
                }
                var todayDate = function () {
                    // var today = new Date();
                    // return today.yyyymmdd();
                    return serverToday;
                }
                var formatDate = function (strDate) {
                    var d = strDate.stringToDate();
                    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var mm = d.getMonth() + 1; // getMonth() is zero-based
                    var dd = d.getDate();

                    return dayNames[d.getDay()] + ', ' + [
                        (mm>9 ? '' : '0') + mm,
                        (dd>9 ? '' : '0') + dd,
                        d.getFullYear()
                    ].join('/');
                }

                var NormalGoalsClass = function () {
                    var domBlockRoot, domItemList;
                    var domProgressRoot, domProgressList;

                    var goals = goalsData.normalGoals;

                    var itemHandles = [], self = this;

                    var ItemClass = function (itemData) {
                        var domItemRoot, domProgress;

                        var self = this;
                        var goalNumber = 1;

                        var bindEvents = function () {
                            domItemRoot.find('.delete-goal').click(function () {
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this goal?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.delete();
                                    }
                                });
                            });
                            domItemRoot.find('.edit-entry').click(function () {
                                domItemRoot.find('.goal-text-wrapper').addClass('edit-status');
                            });
                            domItemRoot.find('.back-to-origin').click(function () {
                                domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                            });
                            domItemRoot.find('form.update-form').submit(function () {
                                var text = domItemRoot.find('textarea[name=goal_text]').val();
                                var sets = {strGoal_text: text};
                                self.update(sets).then(function () {
                                    domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                                });
                                return false;
                            });
                            domItemRoot.find('.toogle-complete').click(function () {
                                var complete = itemData.boolGoal_complete ? 0 : 1;
                                var sets = {boolGoal_complete: complete};
                                self.update(sets);
                            });
                            domItemRoot.find('.for-tomorrow-wrapper svg').click(function () {

                                var today = todayDate();
                                var tomorrow = nextDate(today);

                                var sets;

                                if(tomorrow == itemData.dtGoal_date){
                                    sets = {dtGoal_date: today};
                                }
                                else {
                                    sets = {dtGoal_date: tomorrow};
                                }
                                self.update(sets);
                            })
                        }
                        this.id = function () {
                            return itemData.goal_ID;
                        }
                        this.order = function () {
                            return parseInt(itemData.intGoal_order);
                        }
                        this.setOrder = function (newOrder) {
                            itemData.intGoal_order = newOrder;
                        }
                        this.setGoalNumber = function (n) {
                            goalNumber = n;
                            domItemRoot.find('.goal-number').html(goalNumber);
                            domProgress.find('.goal-number').html(goalNumber);
                        }
                        this.appendProgress = function () {
                            domProgress.appendTo(domProgressList);
                        }
                        this.detachProgress = function(){
                            domProgress.detach();
                        }
                        this.delete = function () {
                            var where = {
                                goal_ID: self.id(),
                            };
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'delete', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        itemHandles.forEach(function (hdl, i) {
                                            if (hdl.id() == self.id()){
                                                itemHandles.splice(i, 1);
                                                domItemRoot.remove();
                                                domProgress.remove();
                                                goalsMainHandle.normalGoalsHandle.deleteInFront(self.id());
                                            }
                                        });
                                        normalGoalsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.remove = function () {
                            domItemRoot.remove();
                            domProgress.remove();
                        }
                        this.refresh = function () {
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                            domItemRoot.find('textarea[name=goal_text]').html(itemData.strGoal_text);
                            if (itemData.boolGoal_complete){
                                domItemRoot.find('.toogle-complete').addClass('btn-success');
                                domProgress.addClass('completed');
                            }
                            else {
                                domItemRoot.find('.toogle-complete').removeClass('btn-success');
                                domProgress.removeClass('completed');
                            }

                            var nextD = nextDate(baseDate);
                            if (itemData.dtGoal_date == nextD){
                                domItemRoot.addClass('for-tomorrow');
                            }
                            else {
                                domItemRoot.removeClass('for-tomorrow');
                            }
                            domProgress.find('.goal-number').html(goalNumber);
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                        }
                        this.update = function(sets){
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: {goal_ID: self.id()}, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(itemData, sets);
                                        self.refresh();
                                        goalsMainHandle.normalGoalsHandle.updateInFront(sets, self.id());
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.init = function () {
                            domItemRoot = domBlockRoot.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemList);
                            domProgress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number"></span>.</li>'));
                            domItemRoot.data('controlHandle', self);
                            self.refresh();
                            bindEvents();
                        }
                    }

                    var updateItemsOrder = function () {

                        var hdls = [], orders = [];

                        domItemList.find('li.goal-item').each(function (i, dom) {
                            var hdl = $(dom).data('controlHandle');
                            orders.push(hdl.order());
                            hdls.push(hdl);
                        });
                        orders.sort(function (a, b) {
                            return a - b;
                        });
                        var updateSets = [];

                        for (var i = 0; i < hdls.length; i ++){
                            var newOrder = orders[i];
                            var handle = hdls[i];
                            if (newOrder !== handle.order()){
                                updateSets.push({where: handle.id(), sets: {intGoal_order: newOrder}});
                                handle.setOrder(newOrder);
                                goalsMainHandle.normalGoalsHandle.updateInFront({intGoal_order: newOrder}, handle.id());
                            }
                        }
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'update_goals', updateSets: updateSets},
                            success: function (res) {
                                if (res.status == true){
                                    self.refresh();
                                    goalsMainHandle.normalGoalsHandle.refreshAll();
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvents = function () {
                        domBlockRoot.find('form.add-goal-form').submit(function () {
                            var text = domBlockRoot.find('form.add-goal-form textarea').val();
                            self.insertGoal(text).then(domBlockRoot.find('form.add-goal-form textarea').val(''));
                            return false;
                        });
                        domItemList.on('sortupdate', function (e, ui) {
                            updateItemsOrder();
                        });
                    }

                    this.setData = function (data) {

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                            domBlockRoot.find('.display-date').html('Today');
                            domBlockRoot.find('.display-title').html('I will...');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                            domBlockRoot.find('.display-date').html(formatDate(baseDate));
                            if (baseDate < todayDate()){
                                domBlockRoot.find('.display-title').html('I was...');
                            }
                            else {
                                domBlockRoot.find('.display-title').html('I will...');
                            }
                        }

                        goals = data.goals;
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });

                        itemHandles.forEach(function (hdl) {
                            hdl.remove();
                        });
                        itemHandles = [];
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                    }
                    this.refresh = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendProgress();
                        });
                    }
                    this.insertGoal = function (goalText) {
                        var sets = {
                            strGoal_text: goalText,
                            intGoal_type: 2,
                            dtGoal_date: todayDate()
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'insert', sets: sets},
                            success: function (res) {
                                if (res.status == true){
                                    var handle = new ItemClass(res.data);
                                    handle.init();
                                    handle.setGoalNumber(itemHandles.length + 1);
                                    handle.appendProgress();
                                    itemHandles.push(handle);
                                    goalsMainHandle.normalGoalsHandle.insertInFront(res.data);
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.init = function () {
                        domBlockRoot = domMainRoot.find('.normal-goals-wrapper');
                        domItemList = domBlockRoot.find('.item-list');
                        domProgressRoot = domBlockRoot.find('.goals-process');
                        domProgressList = domProgressRoot.find('.progress-indicator');

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                        }
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                        domItemList.sortable({
                            items: 'li.goal-item',
                            handle: '.drag-icon',
                            update: function (a, b) {
                            },
                            start: function(e, ui){
                                var height = parseInt(ui.helper.css('height')) - 2 + 'px';
                                ui.placeholder.height(height);
                            },
                            placeholder: 'ui-state-highlight',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            opacity: 0.9
                        });
                        bindEvents();
                    }
                }

                var GrateGoalsClass = function () {
                    var domBlockRoot, domItemList;
                    var domProgressRoot, domProgressList;

                    var goals = goalsData.grateGoals;

                    var itemHandles = [], self = this;

                    var ItemClass = function (itemData) {
                        var domItemRoot, domProgress;

                        var self = this;
                        var goalNumber = 1;

                        var bindEvents = function () {
                            domItemRoot.find('.delete-goal').click(function () {
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this goal?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.delete();
                                    }
                                });
                            });
                            domItemRoot.find('.edit-entry').click(function () {
                                domItemRoot.find('.goal-text-wrapper').addClass('edit-status');
                            });
                            domItemRoot.find('.back-to-origin').click(function () {
                                domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                            });
                            domItemRoot.find('form.update-form').submit(function () {
                                var text = domItemRoot.find('textarea[name=goal_text]').val();
                                var sets = {strGoal_text: text};
                                self.update(sets).then(function () {
                                    domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                                });
                                return false;
                            });
                            domItemRoot.find('.toogle-complete').click(function () {
                                var complete = itemData.boolGoal_complete ? 0 : 1;
                                var sets = {boolGoal_complete: complete};
                                self.update(sets);
                            });
                            domItemRoot.find('.for-tomorrow-wrapper svg').click(function () {

                                var today = todayDate();
                                var tomorrow = nextDate(today);

                                var sets;

                                if(tomorrow == itemData.dtGoal_date){
                                    sets = {dtGoal_date: today};
                                }
                                else {
                                    sets = {dtGoal_date: tomorrow};
                                }
                                self.update(sets);
                            })
                        }
                        this.id = function () {
                            return itemData.goal_ID;
                        }
                        this.order = function () {
                            return parseInt(itemData.intGoal_order);
                        }
                        this.setOrder = function (newOrder) {
                            itemData.intGoal_order = newOrder;
                        }
                        this.setGoalNumber = function (n) {
                            goalNumber = n;
                            domItemRoot.find('.goal-number').html(goalNumber);
                            domProgress.find('.goal-number').html(goalNumber);
                        }
                        this.appendProgress = function () {
                            domProgress.appendTo(domProgressList);
                        }
                        this.detachProgress = function(){
                            domProgress.detach();
                        }
                        this.delete = function () {
                            var where = {
                                goal_ID: self.id(),
                            };
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'delete', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        itemHandles.forEach(function (hdl, i) {
                                            if (hdl.id() == self.id()){
                                                itemHandles.splice(i, 1);
                                                domItemRoot.remove();
                                                domProgress.remove();
                                            }
                                        });
                                        normalGoalsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.remove = function () {
                            domItemRoot.remove();
                            domProgress.remove();
                        }
                        this.refresh = function () {
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                            domItemRoot.find('textarea[name=goal_text]').html(itemData.strGoal_text);
                            if (itemData.boolGoal_complete){
                                domItemRoot.find('.toogle-complete').addClass('btn-success');
                                domProgress.addClass('completed');
                            }
                            else {
                                domItemRoot.find('.toogle-complete').removeClass('btn-success');
                                domProgress.removeClass('completed');
                            }

                            var nextD = nextDate(baseDate);
                            if (itemData.dtGoal_date == nextD){
                                domItemRoot.addClass('for-tomorrow');
                            }
                            else {
                                domItemRoot.removeClass('for-tomorrow');
                            }
                            domProgress.find('.goal-number').html(goalNumber);
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                        }
                        this.update = function(sets){
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: {goal_ID: self.id()}, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(itemData, sets);
                                        self.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.init = function () {
                            domItemRoot = domBlockRoot.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemList);
                            domProgress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number"></span>.</li>'));
                            domItemRoot.data('controlHandle', self);
                            self.refresh();
                            bindEvents();
                        }
                    }

                    var updateItemsOrder = function () {

                        var hdls = [], orders = [];

                        domItemList.find('li.goal-item').each(function (i, dom) {
                            var hdl = $(dom).data('controlHandle');
                            orders.push(hdl.order());
                            hdls.push(hdl);
                        });
                        orders.sort(function (a, b) {
                            return a - b;
                        });
                        var updateSets = [];

                        for (var i = 0; i < hdls.length; i ++){
                            var newOrder = orders[i];
                            var handle = hdls[i];
                            if (newOrder !== handle.order()){
                                updateSets.push({where: handle.id(), sets: {intGoal_order: newOrder}});
                                handle.setOrder(newOrder);
                            }
                        }
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'update_goals', updateSets: updateSets},
                            success: function (res) {
                                if (res.status == true){
                                    self.refresh();
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvents = function () {
                        domBlockRoot.find('form.add-goal-form').submit(function () {
                            var text = domBlockRoot.find('form.add-goal-form textarea').val();
                            self.insertGoal(text).then(domBlockRoot.find('form.add-goal-form textarea').val(''));
                            return false;
                        });
                        domItemList.on('sortupdate', function (e, ui) {
                            updateItemsOrder();
                        });
                    }

                    this.setData = function (data) {

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                            domBlockRoot.find('.display-date').html('Today');
                            domBlockRoot.find('.display-title').html('I will be grateful for...');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                            domBlockRoot.find('.display-date').html(formatDate(baseDate));
                            if (baseDate < todayDate()){
                                domBlockRoot.find('.display-title').html('I was grateful for...');
                            }
                            else {
                                domBlockRoot.find('.display-title').html('I will be grateful for...');
                            }
                        }

                        goals = data.goals;
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });

                        itemHandles.forEach(function (hdl) {
                            hdl.remove();
                        });
                        itemHandles = [];
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                    }
                    this.refresh = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendProgress();
                        });
                    }
                    this.insertGoal = function (goalText) {
                        var sets = {
                            strGoal_text: goalText,
                            intGoal_type: 1,
                            dtGoal_date: todayDate()
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'insert', sets: sets},
                            success: function (res) {
                                if (res.status == true){
                                    var handle = new ItemClass(res.data);
                                    handle.init();
                                    handle.setGoalNumber(itemHandles.length + 1);
                                    handle.appendProgress();
                                    itemHandles.push(handle);
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.init = function () {
                        domBlockRoot = domMainRoot.find('.grate-goals-wrapper');
                        domItemList = domBlockRoot.find('.item-list');
                        domProgressRoot = domBlockRoot.find('.goals-process');
                        domProgressList = domProgressRoot.find('.progress-indicator');

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                        }
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                        domItemList.sortable({
                            items: 'li.goal-item',
                            handle: '.drag-icon',
                            update: function (a, b) {
                            },
                            start: function(e, ui){
                                var height = parseInt(ui.helper.css('height')) - 2 + 'px';
                                ui.placeholder.height(height);
                            },
                            placeholder: 'ui-state-highlight',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            opacity: 0.9
                        });
                        bindEvents();
                    }
                }

                var TrapGoalsClass = function () {
                    var domBlockRoot, domItemList;
                    var domProgressRoot, domProgressList;

                    var goals = goalsData.trapGoals;

                    var itemHandles = [], self = this;

                    var ItemClass = function (itemData) {
                        var domItemRoot, domProgress;

                        var self = this;
                        var goalNumber = 1;

                        var bindEvents = function () {
                            domItemRoot.find('.delete-goal').click(function () {
                                swal({
                                    title: "Are you sure?",
                                    text: "Do you want to delete this goal?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then(function (willDelete) {
                                    if (willDelete){
                                        self.delete();
                                    }
                                });
                            });
                            domItemRoot.find('.edit-entry').click(function () {
                                domItemRoot.find('.goal-text-wrapper').addClass('edit-status');
                            });
                            domItemRoot.find('.back-to-origin').click(function () {
                                domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                            });
                            domItemRoot.find('form.update-form').submit(function () {
                                var text = domItemRoot.find('textarea[name=goal_text]').val();
                                var sets = {strGoal_text: text};
                                self.update(sets).then(function () {
                                    domItemRoot.find('.goal-text-wrapper').removeClass('edit-status');
                                });
                                return false;
                            });
                            domItemRoot.find('.toogle-complete').click(function () {
                                var complete = itemData.boolGoal_complete ? 0 : 1;
                                var sets = {boolGoal_complete: complete};
                                self.update(sets);
                            });
                            domItemRoot.find('.for-tomorrow-wrapper svg').click(function () {

                                var today = todayDate();
                                var tomorrow = nextDate(today);

                                var sets;

                                if(tomorrow == itemData.dtGoal_date){
                                    sets = {dtGoal_date: today};
                                }
                                else {
                                    sets = {dtGoal_date: tomorrow};
                                }
                                self.update(sets);
                            })
                        }
                        this.id = function () {
                            return itemData.goal_ID;
                        }
                        this.order = function () {
                            return parseInt(itemData.intGoal_order);
                        }
                        this.setOrder = function (newOrder) {
                            itemData.intGoal_order = newOrder;
                        }
                        this.setGoalNumber = function (n) {
                            goalNumber = n;
                            domItemRoot.find('.goal-number').html(goalNumber);
                            domProgress.find('.goal-number').html(goalNumber);
                        }
                        this.appendProgress = function () {
                            domProgress.appendTo(domProgressList);
                        }
                        this.detachProgress = function(){
                            domProgress.detach();
                        }
                        this.delete = function () {
                            var where = {
                                goal_ID: self.id(),
                            };
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'delete', where: where},
                                success: function (res) {
                                    if (res.status == true){
                                        itemHandles.forEach(function (hdl, i) {
                                            if (hdl.id() == self.id()){
                                                itemHandles.splice(i, 1);
                                                domItemRoot.remove();
                                                domProgress.remove();
                                            }
                                        });
                                        normalGoalsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.remove = function () {
                            domItemRoot.remove();
                            domProgress.remove();
                        }
                        this.refresh = function () {
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                            domItemRoot.find('textarea[name=goal_text]').html(itemData.strGoal_text);
                            if (itemData.boolGoal_complete){
                                domItemRoot.find('.toogle-complete').addClass('btn-success');
                                domProgress.addClass('completed');
                            }
                            else {
                                domItemRoot.find('.toogle-complete').removeClass('btn-success');
                                domProgress.removeClass('completed');
                            }

                            var nextD = nextDate(baseDate);
                            if (itemData.dtGoal_date == nextD){
                                domItemRoot.addClass('for-tomorrow');
                            }
                            else {
                                domItemRoot.removeClass('for-tomorrow');
                            }
                            domProgress.find('.goal-number').html(goalNumber);
                            domItemRoot.find('.goal-text').html(itemData.strGoal_text);
                        }
                        this.update = function(sets){
                            return $.ajax({
                                url: API_ROOT_URL + '/Goals.php',
                                data: {action: 'update', where: {goal_ID: self.id()}, sets: sets},
                                success: function (res) {
                                    if (res.status == true){
                                        $.extend(itemData, sets);
                                        self.refresh();
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        }
                        this.init = function () {
                            domItemRoot = domBlockRoot.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemList);
                            domProgress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number"></span>.</li>'));
                            domItemRoot.data('controlHandle', self);
                            self.refresh();
                            bindEvents();
                        }
                    }

                    var updateItemsOrder = function () {

                        var hdls = [], orders = [];

                        domItemList.find('li.goal-item').each(function (i, dom) {
                            var hdl = $(dom).data('controlHandle');
                            orders.push(hdl.order());
                            hdls.push(hdl);
                        });
                        orders.sort(function (a, b) {
                            return a - b;
                        });
                        var updateSets = [];

                        for (var i = 0; i < hdls.length; i ++){
                            var newOrder = orders[i];
                            var handle = hdls[i];
                            if (newOrder !== handle.order()){
                                updateSets.push({where: handle.id(), sets: {intGoal_order: newOrder}});
                                handle.setOrder(newOrder);
                            }
                        }
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'update_goals', updateSets: updateSets},
                            success: function (res) {
                                if (res.status == true){
                                    self.refresh();
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvents = function () {
                        domBlockRoot.find('form.add-goal-form').submit(function () {
                            var text = domBlockRoot.find('form.add-goal-form textarea').val();
                            self.insertGoal(text).then(domBlockRoot.find('form.add-goal-form textarea').val(''));
                            return false;
                        });
                        domItemList.on('sortupdate', function (e, ui) {
                            updateItemsOrder();
                        });
                    }

                    this.setData = function (data) {

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                            domBlockRoot.find('.display-date').html('Today');
                            domBlockRoot.find('.display-title').html('I will avoid the trap of...');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                            domBlockRoot.find('.display-date').html(formatDate(baseDate));
                            if (baseDate < todayDate()){
                                domBlockRoot.find('.display-title').html('I did avoid the trap of...');
                            }
                            else {
                                domBlockRoot.find('.display-title').html('I will avoid the trap of...');
                            }
                        }

                        goals = data.goals;
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });

                        itemHandles.forEach(function (hdl) {
                            hdl.remove();
                        });
                        itemHandles = [];
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                    }
                    this.refresh = function () {
                        itemHandles.sort(function (a, b) {
                            return a.order() - b.order();
                        });
                        itemHandles.forEach(function (hdl) {
                            hdl.detachProgress();
                        });
                        itemHandles.forEach(function (hdl, i) {
                            hdl.setGoalNumber(i + 1);
                            hdl.appendProgress();
                        });
                    }
                    this.insertGoal = function (goalText) {
                        var sets = {
                            strGoal_text: goalText,
                            intGoal_type: 3,
                            dtGoal_date: todayDate()
                        };
                        return $.ajax({
                            url: API_ROOT_URL + '/Goals.php',
                            data: {action: 'insert', sets: sets},
                            success: function (res) {
                                if (res.status == true){
                                    var handle = new ItemClass(res.data);
                                    handle.init();
                                    handle.setGoalNumber(itemHandles.length + 1);
                                    handle.appendProgress();
                                    itemHandles.push(handle);
                                }
                                else {
                                    alert('something went wrong');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    this.init = function () {
                        domBlockRoot = domMainRoot.find('.trap-goals-wrapper');
                        domItemList = domBlockRoot.find('.item-list');
                        domProgressRoot = domBlockRoot.find('.goals-process');
                        domProgressList = domProgressRoot.find('.progress-indicator');

                        if (baseDate == todayDate()){
                            domBlockRoot.addClass('on-today');
                        }
                        else {
                            domBlockRoot.removeClass('on-today');
                        }
                        goals.sort(function (a, b) {
                            return parseInt(a.intGoal_order) - parseInt(b.intGoal_order);
                        });
                        goals.forEach(function (goal, i) {
                            var handle = new ItemClass(goal);
                            handle.init();
                            handle.setGoalNumber(i + 1);
                            itemHandles.push(handle);
                        });
                        self.refresh();
                        domItemList.sortable({
                            items: 'li.goal-item',
                            handle: '.drag-icon',
                            update: function (a, b) {
                            },
                            start: function(e, ui){
                                var height = parseInt(ui.helper.css('height')) - 2 + 'px';
                                ui.placeholder.height(height);
                            },
                            placeholder: 'ui-state-highlight',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            opacity: 0.9
                        });
                        bindEvents();
                    }
                }

                var setBaseDate = function (newBaseDate) {
                    baseDate = newBaseDate;
                    $.ajax({
                        url: API_ROOT_URL + '/Goals.php',
                        data: {action: 'get_goals_data_by_date', date: baseDate},
                        success: function (res) {
                            if (res.status == true){
                                var data = res.data;
                                normalGoalsHandle.setData({goals: data.normalGoals, date: baseDate});
                                // grateGoalsHandle.setData({goals: data.grateGoals, date: baseDate});
                                // trapGoalsHandle.setData({goals: data.trapGoals, date: baseDate});
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }

                var bindEvents = function () {
                    domMainRoot.find('.modal-footer .btn.btn-default').click(function () {
                        lightboxRootHandle.cancelClose().then(function () {
                            goalsMainHandle.scrollFocus();
                        });
                    });
                    domMainRoot.find('.step-next').click(function () {
                        var nextD = nextDate(baseDate);
                        setBaseDate(nextD);
                    });
                    domMainRoot.find('.step-before').click(function () {
                        var beforeD = beforeDate(baseDate);
                        setBaseDate(beforeD);
                    });
                }

                this.init = function () {
                    baseDate = todayDate();
                    normalGoalsHandle = new NormalGoalsClass();
                    normalGoalsHandle.init();

                    // grateGoalsHandle = new GrateGoalsClass();
                    // grateGoalsHandle.init();
                    //
                    // trapGoalsHandle = new TrapGoalsClass();
                    // trapGoalsHandle.init();

                    self.normalGoalsHandle = normalGoalsHandle;
                    // self.grateGoalsHandle = grateGoalsHandle;
                    // self.trapGoalsHandle = trapGoalsHandle;

                    bindEvents();
                }
            }

            this.init = function () {
                // goalsInitHandle = new GoalsInitClass();
                goalsMainHandle = new GoalsMainClass();
                goalsMorEvnHandle = new GoalsMorEvnClass();

                // goalsInitHandle.init();
                goalsMainHandle.init();
                goalsMorEvnHandle.init();
                if (isFirstVisit){
                    goalsFirstPopupHdl = new SiteLightboxClass({
                        domLightboxMainContent: $('.first-goals-popup').clone().removeClass('sample').removeAttr('hidden'),
                        mainContentHdlClass: GoalsFirstPopupContentClass,
                        openDuration: 600,
                        closeDuration: 400,
                        lightboxType: 'cssMain',
                        openedCss: {
                            width: 1500
                        }
                    });
                    goalsFirstPopupHdl.init();
                    goalsFirstPopupHdl.open();
                }
            }
        }

        this.init = function () {
            subscriptionsHandle = new SubscriptionsClass();
            subscriptionsHandle.init();

            amazonProductsHandle = new AmazonProductsClass();
            amazonProductsHandle.init();

            eventsSectionHandle = new EventsSectionClass();
            eventsSectionHandle.init();

            goalsSectionHandle = new GoalsSectionClass();
            goalsSectionHandle.init();
            jQuery(document).ready(function () {
                switch (initialScrollTo){
                    case 'subscription':
                        subscriptionsHandle.scrollTo(initialFocusId);
                        break;
                    default:
                        break;
                }
            });
            siteHeaderHandle.scrollFix({hides: []});
        }
    };
    userPageHandle = new UserPageClass();
    userPageHandle.init();
    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });
})();