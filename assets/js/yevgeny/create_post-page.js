(function () {

    'use strict';

    var PageClass = function () {

        var domPage;

        var createFromScratchHandle, createFromSearchHandle, createMenuDecisionHandle, bulkUploaderHandle, createSwitchToHandle, createWithFormHandle, createWithFormLoopHandle;
        var seriesTreeHandle;

        var bindData = function () {
            switch (createType) {
                case 'from_scratch':
                    domPage.find('.post-create-type').html('Create a unique post');
                    break;
                case 'from_search':
                    domPage.find('.post-create-type').html('Create post from search');
                    break;
                case 'menu_decision':
                    domPage.find('.post-create-type').html('Create menu / decision point');
                    break;
                case 'bulk_upload':
                    domPage.find('.post-create-type').html('Bulk upload');
                    break;
                case 'switch_to':
                    domPage.find('.post-create-type').html('Create "switch-to" point');
                    break;
            }
        }

        var CreateFromScratchClass = function () {
            var domRoot, domForm, domSortByDropDown;
            var searchTerms = [];
            var intPost_type = 7;
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domRoot.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domRoot.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domRoot.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    intPost_type = v;
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.submit(function () {
                    create();
                    return false;
                });
            }
            var create = function () {
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_series_ID = series.series_ID;
                sets.intPost_type = intPost_type;
                sets.strPost_keywords = searchTerms.join(',');
                sets = Object.assign(sets, seriesTreeHandle.postInsertPosition());
                ajaxData.append('sets', JSON.stringify(sets));
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                    ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                }
                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                    window.location.href = 'edit_series?id=' + series.series_ID;
                });
            }
            this.init = function () {
                domRoot = domPage.find('.create-from-scratch');
                domForm = domRoot.find('form');
                domSortByDropDown = domForm.find('.dropdown');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domRoot.find('[name="strPost_body"]').summernote({
                    height: 200,
                    tabsize: 2,
                });
                bindEvents();
            }
        }
        var CreateFromSearchClass = function () {

            var domRoot, domHeader, domSearchContainer, domFooter;

            var self = this, youtubeListHandle, podcastsListHandle, blogsListHandle, ideaboxListHandle, postsListHandle;
            var selectedStructureHandle;

            var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';

            var bindEvents = function () {
                domHeader.find('.search-input').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        domHeader.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domHeader.find('.terms-container'));
                        self.applySearch(v);
                    }
                });
            }

            this.applySearch = function (kwd) {
                self.searchByKeyword(kwd).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, kwd);
                    podcastsListHandle.setItems(res.data.spreaker, kwd);
                    blogsListHandle.setItems(res.data.blogs, kwd);
                    // ideaboxListHandle.setItems(res.data.ideabox, kwd);
                    // postsListHandle.setItems(res.data.posts, kwd);
                });
            }

            this.searchByKeyword = function (keyword) {
                var ajaxData = {
                    q: keyword,
                    size: 50,
                    items: [
                        {name: 'youtube', token: false},
                        {name: 'spreaker', token: false},
                        {name: 'blogs', token: false},
                        // {name: 'recipes', token: false},
                        // {name: 'ideabox', token: false},
                        // {name: 'posts', token: false},
                        // {name: 'rssbPosts', token: false},
                    ]
                };
                return ajaxAPiHandle.apiPost('SearchPosts.php', ajaxData);
            }

            var SearchRowClass = function (from) {
                var domSearchRow, domItemsList;

                var rowSelf = this, itemHandles = [], filteredHandles = [];

                var bindData = function () {
                    switch (from) {
                        case 'youtube':
                            domSearchRow.find('.from-name').html('Results from Youtube');
                            break;
                        case 'podcasts':
                            domSearchRow.find('.from-name').html('Results from Podcasts');
                            break;
                        case 'blog':
                            domSearchRow.find('.from-name').html('Results from Blogs');
                            break;
                        case 'recipes':
                            domSearchRow.find('.from-name').html('Results from Recipes');
                            break;
                        case 'ideabox':
                            domSearchRow.find('.from-name').html('Results from Ideabox');
                            break;
                        case 'posts':
                            domSearchRow.find('.from-name').html('Results from Posts');
                            break;
                        case 'rssb-posts':
                            domSearchRow.find('.from-name').html('Results from RSSBlogPost');
                            break;
                        default:
                            domSearchRow.find('.from-name').html('Results from Posts');
                            break;

                    }
                }

                var bindEvents = function () {
                    domItemsList.on('afterChange', function (e, slick, currentSlide) {
                        var loadIndex = currentSlide + 10;
                        for (var i = loadIndex; i < itemHandles.length && i < loadIndex + 5; i ++) {
                            if (itemHandles[i].isDataLoaded()) {
                                continue;
                            }
                            itemHandles[i].bindData();
                        }
                    });
                }

                var formatItemData = function(itemData){
                    var fData = {};
                    switch (from) {
                        case 'youtube':
                            fData.title = itemData.snippet.title;
                            fData.summary = itemData.snippet.description;
                            fData.body = helperHandle.makeYoutubeUrlById(itemData.id.videoId);
                            fData.image = itemData.snippet.thumbnails.high.url;
                            fData.type = 2;
                            break;
                        case 'podcasts':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = helperHandle.makeSpreakerUrlById(itemData.episode_id);
                            fData.image = itemData.image_url;
                            fData.type = 0;
                            break;
                        case 'blog':
                            fData.title = itemData.name;
                            fData.summary = '';
                            fData.body = itemData.body;
                            fData.image = itemData.image;
                            fData.type = 7;
                            break;
                        case 'recipes':
                            fData.title = itemData.title;
                            fData.summary = '';
                            fData.body = itemData.source_url;
                            fData.image = itemData.image_url;
                            fData.type = 7;
                            break;
                        case 'ideabox':
                            fData.title = itemData.strIdeaBox_title;
                            fData.summary = '';
                            fData.body = itemData.strIdeaBox_idea;
                            fData.image = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        case 'posts':
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                        case 'rssb-posts':
                            fData.title = itemData.strRSSBlogPosts_title;
                            fData.summary = itemData.strRSSBlogPosts_description;
                            fData.body = itemData.strRSSBlogPosts_content;
                            fData.image = DEFAULT_IMAGE;
                            fData.type = 7;
                            break;
                        default:
                            fData.title = itemData.strPost_title;
                            fData.summary = itemData.strPost_summary;
                            fData.body = itemData.strPost_body;
                            fData.image = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                            fData.type = itemData.intPost_type;
                            break;
                    }
                    fData.origin = itemData;
                    return fData;
                }

                var SearchItemClass = function (itemData) {

                    var dataLoaded = false, isAdded = false;
                    var domItem;

                    var self = this, selectedListItemHandle, selectedGridItemHandle;

                    var bindData = function () {
                        dataLoaded = true;
                        domItem.find('.item-img').attr('src', itemData.image);
                        domItem.find('.item-title').html(itemData.title);
                        helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                            domItem.find('.blog-duration').html(res);
                            itemData.duration = res;
                        });
                    }

                    var bindEvents = function () {
                        domItem.find('.added-icon-wrapper').click(function () {
                            if (isAdded) {
                                isAdded = false;
                                domItem.removeClass('added-item');
                                selectedListItemHandle.remove();
                                selectedGridItemHandle.remove();
                            }
                            else {
                                isAdded = true;
                                domItem.addClass('added-item');
                                var sHandles = selectedStructureHandle.add(itemData);
                                selectedListItemHandle = sHandles.list;
                                selectedGridItemHandle = sHandles.grid;

                                selectedListItemHandle.setSearchHandle(self);
                                selectedListItemHandle.setGridHandle(sHandles.grid);

                                selectedGridItemHandle.setSearchHandle(self);
                                selectedGridItemHandle.setListHandle(sHandles.list);
                            }
                        })
                    }
                    this.addToPost = function (seryId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.postFormat()
                        };
                        ajaxData.sets.intPost_series_ID = seryId;
                        return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }
                    this.addToSuscription = function (purchasedId, postId) {
                        var ajaxData = {
                            action: 'add',
                            sets: self.subscriptionFormat()
                        };
                        ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                        if (postId){
                            ajaxData.sets.intClientSubscription_post_ID = postId;
                        }
                        return ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                            if (res.status){
                                isAdded = true;
                                domItem.addClass('added-post');
                            }
                            return true;
                        });
                    }
                    this.subscriptionFormat = function () {
                        var f = {
                            strClientSubscription_title: itemData.title,
                            strClientSubscription_body: itemData.body,
                            strClientSubscription_image: itemData.image,
                            intClientSubscriptions_type: itemData.type,
                        }
                        if (itemData.duration) {
                            f.strSubscription_duration = itemData.duration;
                        }
                        return f;
                    }
                    this.postFormat = function () {
                        var f = {
                            strPost_title: itemData.title,
                            strPost_body: itemData.body,
                            strPost_summary: itemData.summary,
                            intPost_type: itemData.type,
                            strPost_featuredimage: itemData.image,
                        }
                        if (itemData.duration) {
                            f.strPost_duration = itemData.duration;
                        }
                        return f;
                    }

                    this.remove = function () {
                        domItemsList.slick('slickRemove', domItem.parent().find('>.slick-slide').index(domItem));
                    }
                    this.isAdded = function () {
                        return isAdded;
                    }
                    this.isDataLoaded = function () {
                        return dataLoaded;
                    }
                    this.bindData = bindData;
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domItemsList);
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.init = function () {
                        domItem = domSearchRow.find('.search-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        bindEvents();
                    }
                }

                this.setItems = function (itemsData, newKeyword) {
                    domItemsList.slick('unslick');
                    filteredHandles.forEach(function (value) {
                        value.remove();
                    });
                    domSearchRow.find('.found-value').html(itemsData.items.length);
                    filteredHandles = [];
                    itemsData.items.forEach(function (item, i) {
                        var itemHandle = new SearchItemClass(formatItemData(item));
                        itemHandle.init();
                        if (i < 15) {
                            itemHandle.bindData();
                        }
                        itemHandles.push(itemHandle);
                        filteredHandles.push(itemHandle);
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        centerMode: false,
                        arrows: true,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 4,
                                    slidesToScroll: 3,
                                }
                            },
                        ]
                    });
                }
                this.selectedItems = function () {
                    var sItems = [];
                    itemHandles.forEach(function (value) {
                        if (value.isAdded()) {
                            sItems.push(value.postFormat());
                        }
                    });
                    return sItems;
                }
                this.refreshItems = function () {
                    domItemsList.css('height', domItemsList.css('height'));
                    domItemsList.css('opacity', 0);
                    setTimeout(function () {
                        domItemsList.slick('unslick');
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        switch (sortBy) {
                            case 'audio':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 0;
                                });
                                break;
                            case 'video':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 2;
                                });
                                break;
                            case 'text':
                                filteredHandles = itemHandles.filter(function (value) {
                                    return value.data().type == 7 || value.data().type == 8 || value.data().type == 10;
                                });
                                break;
                            default:
                                filteredHandles = itemHandles;
                                break;
                        }
                        filteredHandles.forEach(function (value) {
                            value.append();
                        });
                        domItemsList.css('height', 'auto');
                        domItemsList.slick({
                            dots: false,
                            infinite: false,
                            centerMode: false,
                            arrows: true,
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            nextArrow: domSearchRow.find('.arrow-right'),
                            prevArrow: domSearchRow.find('.arrow-left'),
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 4,
                                        slidesToScroll: 3,
                                    }
                                },
                            ]
                        });
                        domItemsList.css('opacity', 1);
                    }, 300);
                }
                this.setSlickPosition = function () {
                    domItemsList.slick('setPosition');
                }
                this.init = function () {
                    domSearchRow = domSearchContainer.find('.search-row.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domSearchContainer);
                    domItemsList = domSearchRow.find('.search-items-list');
                    bindData();
                    domItemsList.slick({
                        dots: false,
                        infinite: false,
                        arrows: true,
                        centerMode: false,
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        nextArrow: domSearchRow.find('.arrow-right'),
                        prevArrow: domSearchRow.find('.arrow-left'),
                        responsive: [
                        ]
                    });
                    bindEvents();
                }
            }

            var SelectedStructureClass = function () {

                var domStructure;
                var structureListHandle, structureGridHandle;
                var currentTab = 'list';

                var postFields = {
                    id: 'post_ID',
                    title: 'strPost_title',
                    body: 'strPost_body',
                    image: 'strPost_featuredimage',
                    type: 'intPost_type',
                    order: 'intPost_order',
                    nodeType: 'strPost_nodeType',
                    status: 'strPost_status',
                    parent: 'intPost_parent',
                    free: 'boolPost_free',
                };

                var subscriptionFiels = {
                    id: 'clientsubscription_ID',
                    title: 'strClientSubscription_title',
                    body: 'strClientSubscription_body',
                    image: 'strClientSubscription_image',
                    type: 'intClientSubscriptions_type',
                    order: 'intClientSubscriptions_order',
                    nodeType: 'strClientSubscription_nodeType',
                    parent: 'intClientSubscriptions_parent'
                };

                var viewFormat = function (data) {
                    var fData = {};
                    var fk;

                    for (var k in data) {
                        if (isSeriesMine) {
                            fk = helperHandle.keyOfVal(postFields, k);
                            if (fk) { fData[fk] = data[k]; }
                        }
                        else {
                            fk = helperHandle.keyOfVal(subscriptionFiels, k);
                            if (fk) { fData[fk] = data[k]; }
                        }
                    }
                    fData.origin = data;
                    return fData;
                }

                var postFormat = function (data) {
                    var fData = {};
                    for (var k in data) {
                        if (postFields[k]) {
                            fData[postFields[k]] = data[k];
                        }
                    }
                    return fData;
                }

                var subscriptionFormat = function (data) {
                    var fData = {};
                    for (var k in data) {
                        if (subscriptionFiels[k]) {
                            fData[subscriptionFiels[k]] = data[k];
                        }
                    }
                    return fData;
                }

                var originFormat = function (data) {
                    if (isSeriesMine) {
                        return postFormat(data);
                    }
                    return subscriptionFormat(data);
                }
                var bindEvents = function () {
                    domStructure.find('.save-btn').click(function () {
                        if (currentTab === 'list') {
                            structureListHandle.save();
                        }
                        else {
                            structureGridHandle.save();
                        }
                    });
                    domStructure.find('[href="#structure-grid-tab"]').on('shown.bs.tab', function () {
                        currentTab = 'grid';
                        domStructure.removeClass('list-structure-mode');
                        domStructure.addClass('grid-structure-mode');
                    });
                    domStructure.find('[href="#structure-list-tab"]').on('shown.bs.tab', function () {
                        currentTab = 'list';
                        domStructure.removeClass('grid-structure-mode');
                        domStructure.addClass('list-structure-mode');
                    });
                }

                var StructureListClass = function () {
                    var domStructureList, domPostsList;
                    var self = this;

                    var PostRowClass = function (postData) {

                        var domRow, domInnerWrp;
                        var self = this, parentHandle = false, searchItemHandle, gridItemHandle;
                        var isRemoved = false;

                        var bindEvents = function () {
                            domInnerWrp.find('.delete-action').click(function () {
                                self.remove();
                                searchItemHandle.remove();
                                gridItemHandle.remove();
                            });
                        }

                        var bindData = function () {
                            domInnerWrp.find('.post-title').html(postData.title);
                            domInnerWrp.find('.item-img').attr('src', postData.image);
                            domInnerWrp.addClass('type-' + postData.type);
                            domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-post');
                        }
                        this.setSearchHandle = function (hdl) {
                            searchItemHandle = hdl;
                        }
                        this.setGridHandle = function (hdl) {
                            gridItemHandle = hdl;
                        }
                        this.isRemoved = function () {
                            return isRemoved;
                        }
                        this.getDomPosition = function () {
                            return domRow.index();
                        }
                        this.remove = function () {
                            isRemoved = true;
                            domRow.remove();
                        }
                        this.appendTo = function (domList) {
                            if (!domList) {
                                if (parentHandle) {
                                    domList = parentHandle.getDom().find('> ul');
                                }
                                else {
                                    domList = domPostsList;
                                }

                            }
                            setTimeout(function () {
                                domRow.addClass('appear');
                                domRow.appendTo(domList);
                            }, 200);
                            return new Promise(function (resolve, reject) {
                                setTimeout(function () {
                                    domRow.removeClass('appear');
                                    resolve();
                                }, 700);
                            });
                        }
                        this.data = function () {
                            return postData;
                        }
                        this.init = function () {
                            domRow = domStructureList.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                            domInnerWrp = domRow.find('> .post-row-inner');
                            bindData();
                            bindEvents();
                            domRow.data('controlHandle', self);
                        }
                    }

                    this.save = function () {
                        var items = self.getItems();
                        var fItems = [];
                        items.forEach(function (value) {
                            var f = postFormat(value);
                            f.intPost_series_ID = series.series_ID;
                            fItems.push(f);
                        });
                        if (fItems.length) {
                            return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: fItems}).then(function (res) {
                                window.location.href = 'edit_series?id=' + series.series_ID;
                                return res;
                            });
                        }
                    }
                    this.makeSortable = function () {
                        domPostsList.sortable({
                            placeholder: 'ui-state-highlight',
                            items: 'li',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300,
                            opacity: 0.9,
                            update: function (a, b) {}
                        });
                    }
                    this.add = function (post) {
                        var hdl = new PostRowClass(post);
                        hdl.init();
                        hdl.appendTo();
                        return hdl;
                    }
                    this.getItems = function () {
                        var rtn = [];
                        domPostsList.find('>.post-row').each(function (i) {
                            rtn.push($(this).data('controlHandle').data());
                        });
                        return rtn;
                    }
                    this.init = function () {
                        domStructureList = domStructure.find('#structure-list-tab');
                        domPostsList = domStructureList.find('.posts-list');
                        self.makeSortable();
                    }
                }

                var StructureGridClass = function () {
                    var domStructureGrid, domPostsList;

                    var itemHandles = [], self = this;

                    var PostItemClass = function (itemData) {

                        var domItem, domBody, domTypeBody;
                        var dataLoaded = false, isRemoved = false;

                        var self = this, searchItemHandle, listItemHandle;

                        var remove = function () {
                            isRemoved = true;
                            domItem.remove();
                        }
                        this.setSearchHandle = function (hdl) {
                            searchItemHandle = hdl;
                        }
                        this.setListHandle = function (hdl) {
                            listItemHandle = hdl;
                        }
                        this.isRemoved = function () {
                            return isRemoved;
                        }
                        var bindData = function () {
                            dataLoaded = true;
                            domItem.addClass('type-' + itemData.type);
                            switch (parseInt(itemData.type)) {
                                case 0:
                                    domTypeBody = domBody.find('.blog-type.type-0.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                    break;
                                case 2:
                                    domTypeBody = domBody.find('.blog-type.type-2.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                    domTypeBody.find('img').attr('src', itemData.image);
                                    break;
                                default:
                                    domTypeBody = domBody.find('.blog-type.type-default.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                                    domTypeBody.html(itemData.body);
                                    break;
                            }
                            domItem.find('.item-title').html(itemData.title);
                            if (itemData.duration) {
                                domBody.find('.blog-duration').html(itemData.duration);
                            }
                            else {
                                helperHandle.getBlogDuration(itemData.body, itemData.type).then(function (res) {
                                    domBody.find('.blog-duration').html(res);
                                    itemData.duration = res;
                                });
                            }
                        }
                        var bindEvents = function () {
                            domItem.find('.item-action.delete-action').click(function (e) {
                                remove();
                                searchItemHandle.remove();
                                listItemHandle.remove();
                            });
                        }
                        this.data = function () {
                            return itemData;
                        }
                        this.append = function () {
                            domItem.appendTo(domPostsList);
                        }
                        this.detach = function () {
                            domItem.detach();
                        }
                        this.bindData = bindData;
                        this.remove = remove;
                        this.isDataLoaded = function () {
                            return dataLoaded;
                        }
                        this.init = function () {
                            domItem = domStructureGrid.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                            domBody = domItem.find('.item-type-body');
                            domItem.data('controlHandle', self);
                            bindEvents();
                        }
                    }

                    this.getItems = function () {
                        var rtn = [];
                        domPostsList.find('>.post-item').each(function (i) {
                            rtn.push($(this).data('controlHandle').data());
                        });
                        return rtn;
                    }
                    this.save = function () {
                        var items = self.getItems();
                        var fItems = [];
                        items.forEach(function (value) {
                            var f = postFormat(value);
                            f.intPost_series_ID = series.series_ID;
                            fItems.push(f);
                        });
                        if (fItems.length) {
                            return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: fItems}).then(function (res) {
                                window.location.href = 'edit_series?id=' + series.series_ID;
                                return res;
                            });
                        }
                    }
                    this.add = function (post) {
                        var hdl = new PostItemClass(post);
                        hdl.init();
                        hdl.bindData();
                        hdl.append();
                        itemHandles.push(hdl);
                        return hdl;
                    }
                    this.init = function () {
                        domStructureGrid = domStructure.find('#structure-grid-tab');
                        domPostsList = domStructureGrid.find('.items-container');
                        domPostsList.sortable({
                            placeholder: 'ui-state-highlight',
                            items: 'article.post-item',
                            coneHelperSize: true,
                            forcePlaceholderSize: true,
                            tolerance: "pointer",
                            helper: "clone",
                            revert: 300, // animation in milliseconds
                            // handle: '.img-wrapper',
                            opacity: 0.9,
                            update: function (a, b) {}
                        });
                    }
                }

                this.add = function (post) {
                    var l = structureListHandle.add(post);
                    var g = structureGridHandle.add(post);
                    return {
                        list: l,
                        grid: g
                    };
                }
                this.init = function () {
                    domStructure = domRoot.find('.selected-items-wrapper');

                    structureListHandle = new StructureListClass();
                    structureListHandle.init();

                    structureGridHandle = new StructureGridClass();
                    structureGridHandle.init();
                    bindEvents();
                }
            }

            this.addToSeries = function (seriesId) {
                var items = self.data();
                items.forEach(function (item) {
                    item.intPost_series_ID = seriesId;
                });
                return ajaxAPiHandle.apiPost('Posts.php', {action: 'insert_many', items: items}).then(function (res) {
                    return res;
                });
            }
            this.data = function () {
                var items = [];
                items = items.concat(youtubeListHandle.selectedItems());
                items = items.concat(podcastsListHandle.selectedItems());
                items = items.concat(blogsListHandle.selectedItems());
                // items = items.concat(postsListHandle.selectedItems());
                // items = items.concat(ideaboxListHandle.selectedItems());
                return items;
            }
            this.init = function () {
                domRoot = domPage.find('.create-from-search');
                domHeader = domRoot.find('.pane-header');
                domFooter = domRoot.find('.pane-footer');

                domSearchContainer = domRoot.find('.search-container');

                youtubeListHandle = new SearchRowClass('youtube');
                youtubeListHandle.init();

                podcastsListHandle = new SearchRowClass('podcasts');
                podcastsListHandle.init();

                blogsListHandle = new SearchRowClass('blog');
                blogsListHandle.init();

                selectedStructureHandle = new SelectedStructureClass();
                selectedStructureHandle.init();
                // postsListHandle = new SearchRowClass('posts');
                // postsListHandle.init();
                //
                // ideaboxListHandle = new SearchRowClass('ideabox');
                // ideaboxListHandle.init();
                bindEvents();
            }
        }
        var CreateMenuDecisionClass = function () {
            var domRoot, domForm, domSortByDropDown, domOptionsContainer;
            var optionItemHandles = [];

            var bindEvents = function () {
                domForm.find('.options-cnt-input').change(function () {
                    setOptionCnt(parseInt($(this).val()));
                });
                domForm.submit(function () {
                    save();
                    return false;
                })
            }
            var setOptionCnt = function (v) {
                var len = optionItemHandles.length;
                console.log(v, len);
                for (var i = 0; i < v || i < len; i++) {
                    if (i >= v) {
                        var hdl = optionItemHandles[optionItemHandles.length - 1];
                        hdl.remove();
                        optionItemHandles.pop();
                    }
                    else if (i >= len) {
                        var hdl = new OptionItemClass();
                        hdl.init();
                        optionItemHandles.push(hdl);
                    }
                }
            }
            var OptionItemClass = function () {
                var domOption;
                var self = this;
                var updateCoverImage = function(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.cover-image-wrapper .img-wrapper img', domOption).attr('src', e.target.result).css('opacity', 1);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                var bindEvents = function () {
                    domOption.find('.cover-image-wrapper input[type="file"]').change(function () {
                        updateCoverImage(this);
                    });
                    domOption.find('.cover-image-wrapper .delete-action').click(function () {
                        domOption.find('.cover-image-wrapper input[type="file"]').val('');
                        domOption.find('.cover-image-wrapper .img-wrapper img').removeAttr('src');
                    });
                }
                this.data = function () {
                    var rtn = {
                        strPost_title: domOption.find('.title-input').val(),
                        strPost_body: domOption.find('.body-input').val(),
                        strPost_nodeType: 'path',
                        intPost_series_ID: series.series_ID,
                    };
                    if (domOption.find('.cover-image-wrapper input[type=file]').prop('files').length) {
                        rtn.strPost_featuredimage = domOption.find('.cover-image-wrapper input[type=file]').prop('files')[0];
                    }
                    return rtn;
                }
                this.remove = function () {
                    domOption.remove();
                }
                this.save = function (parentId) {
                    var sets = self.data();
                    sets.intPost_parent = parentId;
                    var ajaxData = new FormData();
                    ajaxData.append('action', 'insert');
                    if (sets.strPost_featuredimage) {
                        ajaxData.append('strPost_featuredimage', sets.strPost_featuredimage);
                        delete sets.strPost_featuredimage;
                    }
                    ajaxData.append('sets', JSON.stringify(sets));
                    return ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData, true, 100).then(function () {
                        return res.data;
                    });
                }
                this.init = function () {
                    domOption = domOptionsContainer.find('.option-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domOptionsContainer);
                    domOption.find('.option-number .number-value').html(optionItemHandles.length + 1);
                    bindEvents();
                }
            }

            var save = function () {
                var sets = helperHandle.formSerialize(domForm);
                sets.strPost_nodeType = 'menu';
                sets.intPost_series_ID = series.series_ID;
                sets = Object.assign(sets, seriesTreeHandle.postInsertPosition());
                ajaxAPiHandle.apiPost('Posts.php', {action: 'insert', sets: sets}, true, 100).then(function (res) {
                    if (res.status) {
                        var ps = [];
                        optionItemHandles.forEach(function (hdl) {
                            ps.push(hdl.save(res.data.post_ID));
                        });
                        Promise.all(ps).then(function () {
                            window.location.href = 'edit_series?id=' + series.series_ID;
                        });
                    }
                });
            }

            this.init = function () {
                domRoot = domPage.find('.create-menu-decision');
                domForm = domRoot.find('form');
                domOptionsContainer = domForm.find('.options-container');
                domSortByDropDown = domForm.find('.dropdown');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domRoot.find('[name="strPost_body"]').summernote({
                    height: 200,
                    tabsize: 2,
                });
                domForm.find('.options-cnt-input').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 5,
                    value: 2,
                });

                var hdl = new OptionItemClass();
                hdl.init();
                optionItemHandles.push(hdl);

                hdl = new OptionItemClass();
                hdl.init();
                optionItemHandles.push(hdl);

                bindEvents();
            }
        }
        var CreateFromBulkUpload = function () {
            var domRoot, domHeader, domFilesList, domFooter;
            var fileRowHandles = [];

            var key = 1;
            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent'
            };

            var postFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (postFields[k]) {
                        fData[postFields[k]] = data[k];
                    }
                }
                return fData;
            }

            var postTypeArr = {
                'video': 2,
                'audio': 0,
                'image': 8,
                'text': 7,
            }

            var FileRowClass = function (file) {

                var domRow;
                var postData = {};

                var self = this;
                var keyI;
                var bindData = function () {
                    postData.title = file.name;
                    postData.strType = helperHandle.extractTypeFromFileType(file.type);
                    postData.type = postTypeArr[postData.strType];
                    postData.fullType = file.type;
                    var dType = file.type;

                    if (file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        dType = 'application/docx';
                    } else if (file.type === 'application/msword') {
                        dType = 'application/doc';
                    } else if (file.type === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                        dType = 'application/ppt';
                    }

                    domRow.find('.item-title-input').attr('placeholder', postData.title).val(postData.title);
                    domRow.find('.file-type').html(dType);

                    switch (postData.strType) {
                        case 'video':
                            break;
                        case 'music':
                            break;
                        case 'image':
                            updateCoverImage(file);
                            break;
                        case 'application':
                        case 'text':
                            fileToText();
                            break;
                    }
                }
                var fileToText = function () {
                    var ajaxData = new FormData();
                    ajaxData.append('file', file);
                    return ajaxAPiHandle.multiPartApiPost('FileToHtml.php', ajaxData).then(function (res) {
                        postData.body = res.data;
                        return res.data;
                    });
                }
                var updateCoverImage = function(file) {
                    postData.imageFile = file;
                    if (helperHandle.extractTypeFromFileType(file.type) != 'image') {
                        swal("Not Image!", "Sorry, This is not image file, Please select image file.").then(function () {
                        });
                        return false;
                    }
                    postData.imageFile = file;
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        domRow.find('img.item-img').attr('src', e.target.result);
                        domRow.find('.item-img-wrapper').css('opacity', 1);
                    }
                    reader.readAsDataURL(file);
                }
                var bindEvents = function () {
                    domRow.find('.image-input').change(function () {
                        if (this.files && this.files[0]) {
                            updateCoverImage(this.files[0]);
                        }
                    });
                    domRow.find('input.item-title-input').change(function () {
                        var title = $(this).val();
                        postData.title = title ? title : $(this).attr('placeholder');
                    });
                    domRow.find('.delete-action').click(function () {
                        if (self.isUploaded()) {
                            self.delete();
                        }
                        else {
                            self.cancel();
                        }
                    });
                    domRow.find('.preview').click(function () {
                        self.preview();
                    });
                }
                this.preview = function () {
                    var wData = postData;
                    if (!wData.body) {
                        wData.body = window.URL.createObjectURL(file);
                    }
                    // viewWidgetHandle.view(wData);
                }
                this.key = function () {
                    return keyI;
                }
                this.isUploaded = function () {
                    return postData.id ? true : false;
                }
                this.delete = function () {
                    if (Array.isArray(postData.id)) {
                        postData.id.forEach(function (id) {
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: id}, true, 200).then(function (res) {
                            });
                        });
                        self.cancel();
                    }
                    else {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'delete', where: postData.id}).then(function (res) {
                            self.cancel();
                        });
                    }
                }
                this.cancel = function () {
                    fileRowHandles.forEach(function (hdl, i) {
                        if (hdl.key() === self.key()) {
                            fileRowHandles.splice(i, 1);
                            domRow.remove();
                        }
                    });
                }
                this.upload = function () {

                    var resPromise;

                    if (postData.strType === 'text' || postData.strType === 'application') {
                        resPromise = Promise.resolve();
                    }
                    else {
                        var ajaxData = new FormData();
                        ajaxData.append('file', file);
                        ajaxData.append('sets', JSON.stringify({where: 'assets/media/' + postData.strType, prefix: 'post'}));
                        resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 200).then(function (res) {
                            postData.body = res.data.abs_url;
                            return true;
                        });
                    }
                    resPromise.then(function () {
                        if (Array.isArray(postData.body)) {
                            postData.id = [];
                            postData.body.forEach(function (body, i) {
                                var ajaxPost = new FormData();
                                ajaxPost.append('action', 'insert');
                                var postFData = postFormat($.extend({}, postData, {body: body, title: postData.title + '('+ (i + 1) +')'}));
                                postFData.intPost_series_ID = series.series_ID;
                                delete postFData.post_ID;
                                ajaxPost.append('sets', JSON.stringify(postFData));
                                if (postData.imageFile) {
                                    ajaxPost.append('strPost_featuredimage', postData.imageFile);
                                }
                                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxPost, true, 200).then(function (res) {
                                    postData.id.push(res.data.post_ID);
                                    domRow.addClass('uploaded');
                                });
                            });
                        }
                        else {
                            var ajaxPost = new FormData();
                            ajaxPost.append('action', 'insert');
                            var postFData = postFormat(postData);
                            postFData.intPost_series_ID = series.series_ID;
                            ajaxPost.append('sets', JSON.stringify(postFData));
                            if (postData.imageFile) {
                                ajaxPost.append('strPost_featuredimage', postData.imageFile);
                            }
                            ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxPost, true, 200).then(function (res) {
                                postData.id = res.data.post_ID;
                                domRow.addClass('uploaded');
                            });
                        }
                    });
                }
                this.init = function () {
                    keyI = key++;
                    domRow = domFilesList.find('.upload-term-item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domFilesList);
                    bindData();
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }

            var bindEvents = function () {
                domHeader.find('.add-new-input').change(function () {
                    var files = $(this).prop('files');
                    for (var i = 0; i < files.length; i ++) {
                        var hdl = new FileRowClass(files[i]);
                        hdl.init();
                        fileRowHandles.push(hdl);
                    }
                    $(this).val('');
                });
                domFooter.find('.save-btn').click(function () {
                    domFilesList.find('>.upload-term-item:not(.sample)').each(function () {
                        var hdl = $(this).data('controlHandle');
                        hdl.upload();
                    });
                });
                domFooter.find('.cancel-all-btn').click(function () {
                    fileRowHandles.forEach(function (value) {
                        if (value.isUploaded()) {
                            value.delete();
                        }
                        else {
                            value.cancel();
                        }
                    });
                });

            }
            this.init = function () {
                domRoot = domPage.find('.create-from-bulk-upload');
                domHeader = domRoot.find('.pane-header');
                domFooter = domRoot.find('.pane-footer');
                domFilesList = domRoot.find('.items-container');
                domFilesList.sortable({
                    placeholder: 'ui-state-highlight',
                    items: '.upload-term-item',
                    coneHelperSize: true,
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    revert: 300, // animation in milliseconds
                    // handle: '.img-wrapper',
                    opacity: 0.9,
                    update: function (a, b) {}
                });
                bindEvents();
            }
        }
        var CreateSwitchToClass = function () {
            var domRoot, domForm, domSortByDropDown;

            var redirectModalHandle;

            var searchTerms = [];
            var intPost_type = 7;

            var RedirectToModalClass = function () {
                var domModal;
                var rPostsTreeHandle;
                var bindEvents = function () {
                    domModal.find('.choose-btn').click(function () {
                        choose();
                    })
                }
                var choose = function () {
                    var r_id = rPostsTreeHandle.postRedirectPosition();
                    if (r_id) {
                        domForm.find('.redirect-url-input').val(r_id).change();
                        domModal.modal('hide');
                    }
                    else {
                        swal('Please select node');
                    }
                }
                this.init = function () {
                    domModal = domRoot.find('.redirect-to-modal');
                    rPostsTreeHandle = new SeriesPostsTreeClass(series, domModal.find('.series-posts-tree'));
                    rPostsTreeHandle.init();
                    bindEvents();
                }
            }

            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domRoot.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domRoot.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domRoot.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    intPost_type = v;
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.submit(function () {
                    create();
                    return false;
                });
            }
            var create = function () {
                var rid = domForm.find('.redirect-url-input').val();
                if (!rid) {
                    swal('Please select node or insert url to redirect');
                    return false;
                }
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_series_ID = series.series_ID;
                sets.strPost_keywords = searchTerms.join(',');
                sets.intPost_type = helperHandle.isAbsUrl(rid) ? 11 : 9;
                sets.strPost_body = rid;
                sets = Object.assign(sets, seriesTreeHandle.postInsertPosition());
                ajaxData.append('sets', JSON.stringify(sets));
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                    ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                }
                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                    window.location.href = 'edit_series?id=' + series.series_ID;
                });
            }
            this.init = function () {
                domRoot = domPage.find('.create-switch-to');
                domForm = domRoot.find('form');
                domSortByDropDown = domForm.find('.dropdown');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domRoot.find('[name="strPost_body"]').summernote({
                    height: 200,
                    tabsize: 2,
                });
                redirectModalHandle = new RedirectToModalClass();
                redirectModalHandle.init();
                bindEvents();
            }
        }
        var CreateWithFormClass = function () {
            var domRoot, domForm, domSortByDropDown, domFieldsList, domDes;

            var fieldRowHandles = [];

            var searchTerms = [];
            var intPost_type = 10;
            var fieldCreateType = 'checkbox';
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result).css('opacity', 1);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            var bindEvents = function () {
                domRoot.find('[name="strPost_keywords"]').keyup(function (e) {
                    var code = e.which;
                    if (code === 13) {
                        var v = $(this).val();
                        if (v) {
                            $(this).val('');
                            domRoot.find('.terms-container .term-item.sample').clone().removeClass('sample').removeAttr('hidden').html('#' + v).appendTo(domRoot.find('.terms-container'));
                            searchTerms.push(v);
                        }
                    }
                });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    intPost_type = v;
                });
                domForm.find('.cover-image-wrapper [name="strPost_featuredimage"]').change(function () {
                    updateCoverImage(this);
                });
                domForm.find('.type-choose-item').click(function () {
                    domForm.find('.type-choose-item').removeClass('active-item');
                    $(this).addClass('active-item');
                    fieldCreateType = $(this).data('type');
                });
                domForm.find('.add-form-btn').click(function () {
                    createField().then(function (res) {
                        if (res) {
                            var hdl = new FieldRowClass(res);
                            hdl.init();
                            fieldRowHandles.push(hdl);
                        };
                    })
                });
                domDes.on('summernote.enter', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.focus', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.keydown', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.on('summernote.change', function(e) {
                    domDes.summernote('editor.saveRange');
                });
                domDes.parent().find('.note-editable').click(function () {
                    domDes.summernote('editor.saveRange');
                });
                domForm.submit(function () {
                    create();
                    return false;
                });
                domForm.find('.save-btn').click(function (e) {
                    create();
                    e.preventDefault();
                    return false;
                });
            }
            var create = function () {
                var ajaxData = new FormData();
                ajaxData.append('action', 'insert');
                var sets = helperHandle.formSerialize(domForm);
                sets.intPost_series_ID = series.series_ID;
                sets.intPost_type = intPost_type;
                sets.strPost_keywords = searchTerms.join(',');
                sets = Object.assign(sets, seriesTreeHandle.postInsertPosition());
                ajaxData.append('sets', JSON.stringify(sets));
                if (domForm.find('[name="strPost_featuredimage"]').prop('files').length) {
                    ajaxData.append('strPost_featuredimage', domForm.find('[name="strPost_featuredimage"]').prop('files')[0]);
                }
                ajaxAPiHandle.multiPartApiPost('Posts.php', ajaxData).then(function (res) {
                    window.location.href = 'edit_series?id=' + series.series_ID;
                });
            }

            var FieldRowClass = function (fieldData) {
                var domRow;
                var bindData = function () {
                    domRow.find('.item-name-input').html(fieldData.strFormField_name);
                    domRow.addClass('field-type-' + fieldData.strFormField_type);
                }
                var bindEvents = function () {
                    domRow.find('.delete-action').click(function () {
                        swal({
                            title: "Are you sure?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete) {
                                deleteRow();
                            }
                        });
                    });
                    domRow.find('.append-action.append-question').on('click', function () {
                        insertNode();
                    });
                    domRow.find('.append-action.append-answer').click(function () {
                        insertAnsweredNode();
                    });
                }
                var deleteRow = function () {
                    ajaxAPiHandle.apiPost('FormFields.php', {action: 'delete', where: fieldData.formField_ID}).then(function () {
                        domRow.remove();
                        fieldRowHandles.forEach(function (hdl, i) {
                            if (hdl.data().formField_ID === fieldData.formField_ID) {
                                fieldRowHandles.splice(i, 1);
                                return true;
                            }
                        });
                    });
                }
                var insertNode = function () {
                    var domNode = $('<div data-form-field="'+ fieldData.formField_ID +'" class="form-field-wrapper"></div>');
                    var domNodeInner = $('<div class="form-field-inner"></div>').appendTo(domNode);
                    domNodeInner.append('<label contenteditable="false" class="form-field-name">'+ fieldData.strFormField_name +':</label>');
                    switch (fieldData.strFormField_type) {
                        case 'text':
                            domNodeInner.append('<div class="form-field-value" contenteditable="true" placeholder="' + fieldData.strFormField_default + '"></div>');
                            break;
                        case 'checkbox':
                            domNodeInner.append('<div class="form-field-value" contenteditable="false"><input type="checkbox" ' + (parseInt(fieldData.strFormField_default) === 1 ? 'checked' : '') + ' /></div>');
                            break;
                    }
                    domNodeInner.append('<span class="form-field-helper" style="font-style: italic;" data-toggle="popover"  data-trigger="hover" contenteditable="false" data-content="'+ fieldData.strFormField_helper +'">i</span>');
                    insertNodeToBody(domNode.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (domNode.find('.form-field-value').html() == '<br>') {
                            domNode.find('.form-field-value').empty();
                        }
                    });
                }
                var insertAnsweredNode = function () {
                    var domNode = domForm.find('.sample.answered-form-field-wrapper').find('.answered-form-field').clone();
                    domNode.attr('data-answered-form-field', fieldData.formField_ID);
                    var answer = false;
                    formFieldAnswers.forEach(function (formFieldAnswer) {
                        if (formFieldAnswer.intFormFieldAnswer_field_ID == fieldData.formField_ID) {
                            answer = formFieldAnswer;
                        }
                    });
                    fieldData.answer = answer ? answer.strFormFieldAnswer_answer : false;
                    var hdl = new AnsweredFormFieldClass(domNode, fieldData);
                    hdl.init();
                    insertNodeToBody(domNode.get(0));
                    domDes.on('summernote.change', function(e) {
                        if (domNode.find('.form-field-value').html() == '<br>') {
                            domNode.find('.form-field-value').empty();
                        }
                    });
                }
                this.data = function () {
                    return fieldData;
                }
                this.init = function () {
                    domRow = domFieldsList.find('.form-field-item.sample').clone().removeAttr('hidden').removeClass('sample').appendTo(domFieldsList);
                    bindData();
                    bindEvents();
                }
            }
            var createField = function () {
                var sets = {};
                sets.strFormField_name = domForm.find('.form-question-input').val();
                if (!sets.strFormField_name) {
                    swal('please name your form field');
                    return Promise.resolve(false);
                }
                sets.strFormField_helper = domForm.find('.form-helper-input').val();
                sets.intFormField_series_ID = series.series_ID;
                sets.strFormField_type = fieldCreateType;
                switch (fieldCreateType) {
                    case 'text':
                        sets.strFormField_default = '';
                        break;
                    case 'checkbox':
                        sets.strFormField_default = 0;
                        break;
                }
                return ajaxAPiHandle.apiPost('FormFields.php', {action: 'insert', sets: sets}).then(function (res) {
                    return $.extend(sets, {formField_ID: res.data});
                });
            }
            var insertNodeToBody = function (domNode) {
                domDes.summernote('editor.restoreRange');
                domDes.summernote('editor.focus');
                domDes.summernote('insertNode', domNode);
                domDes.summernote('insertText', '\u00A0');
                makeDescriptionValid();
                $('[data-toggle="popover"]').popover();
            }
            var makeDescriptionValid = function () {
                var domEditor = domForm.find('.note-editor.note-frame .note-editable');
                var tpPreBr = false;
                domEditor.find('div>br, p>br').each(function (i, d) {
                    var p = $(d).parent();
                    if (p.html() === '<br>') {
                        if (tpPreBr) {
                            tpPreBr.remove();
                        }
                        tpPreBr = p;
                    }
                });
                domEditor.find('.form-field-wrapper').each(function (i, d) {
                    if ($(d).find('.form-field-name').length === 0) {
                        $(d).remove();
                    }
                });
            }
            this.init = function () {
                domRoot = domPage.find('.create-with-form');
                domForm = domRoot.find('form');
                domDes = domForm.find('textarea[name="strPost_body"]');
                domFieldsList = domForm.find('.form-fiels-container');
                domSortByDropDown = domForm.find('.dropdown');
                $.summernote.dom.emptyPara = "<div><br></div>";
                domRoot.find('[name="strPost_body"]').summernote({
                    height: 200,
                    tabsize: 2,
                });
                formFields.forEach(function (f) {
                    var hdl = new FieldRowClass(f);
                    hdl.init();
                    fieldRowHandles.push(hdl);
                });
                bindEvents();
            }
        }
        var CreateWithFormLoopClass = function () {
            var domRoot;
            var createLoopHandle;
            this.init = function () {
                domRoot = domPage.find('.create-with-form-loop');
                createLoopHandle = new FormLoopEditorClass(domRoot.find('.form-loop-editor-component'), {seriesId: 4});
                createLoopHandle.init();
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            seriesTreeHandle = new SeriesPostsTreeClass(series, domPage.find('.right-portion .series-posts-tree'));
            seriesTreeHandle.init();
            createFromScratchHandle = new CreateFromScratchClass();
            createFromScratchHandle.init();

            createFromSearchHandle = new CreateFromSearchClass();
            createFromSearchHandle.init();

            createMenuDecisionHandle = new CreateMenuDecisionClass();
            createMenuDecisionHandle.init();

            bulkUploaderHandle = new CreateFromBulkUpload();
            bulkUploaderHandle.init();

            createSwitchToHandle = new CreateSwitchToClass();
            createSwitchToHandle.init();

            createWithFormHandle = new CreateWithFormClass();
            createWithFormHandle.init();

            createWithFormLoopHandle = new CreateWithFormLoopClass();
            createWithFormLoopHandle.init();
            bindData();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();