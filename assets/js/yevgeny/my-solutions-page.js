(function () {

    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('-');
    };

    String.prototype.stringToDate = function () {
        var yyyy = this.split('-')[0];
        var mm = this.split('-')[1] * 1 - 1;
        var dd = this.split('-')[2];
        return new Date(yyyy, mm, dd);
    };

    var PageClass = function () {

        var domPage;
        var mainHandle, filterHandle, goalsHandle, whatDoFirstHandle, selectCategoriesHandle, selectSeriesHandle, postEditorHandle, gettingStartedHeaderHandle;

        var FilterClass = function () {

            var domRoot, domSortByDropDown, domCategoryFilterDropDown;

            var category = -1, filter = -1;

            var bindEvents = function () {
                // domRoot.find('.filter-category').click(function () {
                //     domRoot.find('.filter-category').removeClass('active');
                //     $(this).addClass('active');
                //     category = $(this).data('category');
                //     mainHandle.applyFilter(category, filter);
                // });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    filter = $(this).data('value');
                    mainHandle.applyFilter(category, filter);
                });
                domCategoryFilterDropDown.find('ul li a').click(function () {
                    domCategoryFilterDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('category');
                    var txt = $(this).text();
                    domCategoryFilterDropDown.data('value', v);
                    domCategoryFilterDropDown.find('button span').text(txt);
                    category = $(this).data('category');
                    mainHandle.applyFilter(category, filter);
                });
            }
            this.fillCategories = function () {
                categories.forEach(function (category) {
                    var dom = domCategoryFilterDropDown.find('ul li[hidden]').clone().removeAttr('hidden').appendTo(domCategoryFilterDropDown.find('ul'));
                    dom.find('a').removeClass('active').attr('data-category', category.category_ID).text(category.strCategory_name);
                    dom.find('a').click(function () {
                        domCategoryFilterDropDown.find('ul li').removeAttr('hidden');
                        $(this).parent().attr('hidden', true);
                        var v = $(this).data('category');
                        var txt = $(this).text();
                        domCategoryFilterDropDown.data('value', v);
                        domCategoryFilterDropDown.find('button span').text(txt);
                        category = v;
                        mainHandle.applyFilter(category, filter);
                    });
                });
            }
            this.show = function () {
                $('.filter-wrapper').show();
            }
            this.init = function () {
                domRoot = $('.filter-wrapper .filter-content');
                domSortByDropDown = domRoot.find('.sort-by .dropdown');
                domCategoryFilterDropDown = domRoot.find('.categories-filter-wrapper .dropdown');
                domRoot.find('[data-toggle="popover"]').popover({
                    trigger: 'hover',
                    placement: 'top',
                });
                bindEvents();
            }
        }

        var MainClass = function () {
            var domRoot;

            var itemHandles = [], filteredHandles = [];
            var availableTypes = [0, 2, 8, 10];
            var audioCnt = 0;

            var ItemClass = function (itemData) {

                var domItem, domItemHeader, domUsedBody, domBody, domVideo, domCircleNavWrap;
                var subscription, metaData = [];
                var self = this, videoHandle, notesHandle, listingHandle;

                var bindEvents = function () {
                    domItemHeader.find('.complete-action').click(function () {
                        setComplete(subscription.boolCompleted ? 0 : 1);
                    });
                    domItemHeader.find('.finish-btn').click(function (e) {
                        setComplete(subscription.boolCompleted ? 0 : 1);
                        e.preventDefault();
                        return false;
                    });
                    domItemHeader.find('.favorite-action').click(function () {
                        setFavorite(subscription.favorite ? 0 : 1);
                    });
                    domItemHeader.find('.favorite-btn').click(function (e) {
                        setFavorite(subscription.favorite ? 0 : 1);
                        e.preventDefault();
                        return false;
                    });
                    domItemHeader.find('.trash-action').click(function () {
                        swal({
                            title: "Are you sure?",
                            icon: "warning",
                            dangerMode: true,
                            buttons: {
                                cancel: "Cancel!",
                                trashPost: {
                                    text: "Trash Post",
                                    value: "trash_post",
                                    className: 'swal-button--danger'
                                },
                                removeSeries: {
                                    text: 'Remove Series',
                                    value: 'remove_series',
                                    className: 'swal-button--danger'
                                },
                            },
                        }).then(function(value){
                            switch (value) {
                                case "trash_post":
                                    trash();
                                    break;
                                case "remove_series":
                                    remove();
                                    break;
                                default:
                                    break;
                            }
                        });
                    });
                    domItemHeader.find('.trash-btn').click(function () {
                        swal({
                            title: "Are you sure?",
                            icon: "warning",
                            dangerMode: true,
                            buttons: {
                                cancel: "Cancel!",
                                trashPost: {
                                    text: "Trash Post",
                                    value: "trash_post",
                                    className: 'swal-button--danger'
                                },
                                removeSeries: {
                                    text: 'Remove Series',
                                    value: 'remove_series',
                                    className: 'swal-button--danger'
                                },
                            },
                        }).then(function(value){
                            switch (value) {
                                case "trash_post":
                                    trash();
                                    break;
                                case "remove_series":
                                    remove();
                                    break;
                                default:
                                    break;
                            }
                        });
                    });
                    domItem.find('.slider-nav.arrow-left').click(function () {
                        stepPrev();
                    });
                    domItem.find('.slider-nav.arrow-right').click(function () {
                        stepNext();
                    });
                    domItem.get(0).addEventListener('swl', function () {
                        stepNext();
                    }, false);
                    domItem.get(0).addEventListener('swr', function () {
                        stepPrev();
                    }, false);
                    domItemHeader.find('.listing-toggle').click(function () {
                        if (listingHandle) {
                            if (parseInt(itemData.permission) === 0) {
                                listingHandle.setMode('default');
                            } else {
                                listingHandle.setMode('edit');
                            }
                            listingHandle.open();
                        }
                        else {
                            if (parseInt(itemData.permission) === 0) {
                                listingHandle = new ListingSubscriptionClass(itemData, $('.listing-panel-component-wrap').find('.listing-subscription-component').clone().appendTo($('body')));
                                listingHandle.init();
                                listingHandle.afterLoad(function () {
                                    listingHandle.update(subscription.clientsubscription_ID, {current: true});
                                    listingHandle.open();
                                });
                                listingHandle.afterSelect(function (sub) {
                                    self.setSubscription(sub);
                                    listingHandle.close();
                                });
                            } else {
                                listingHandle = new ListingPostClass(itemData, $('.listing-panel-component-wrap').find('.listing-post-component').clone().appendTo($('body')));
                                listingHandle.init();
                                listingHandle.setMode('edit');
                                listingHandle.setSelected(subscription.intClientSubscription_post_ID);
                                listingHandle.open();

                                listingHandle.afterSelect(function (post) {
                                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_subscribe', end_point: {intClientSubscription_purchased_ID: itemData.purchased_ID, intClientSubscription_post_ID: post.post_ID}}, true, 200).then(function (res) {
                                        var sub = res.data;
                                        var parent = sub.strClientSubscription_nodeType == 'path' ? sub.clientsubscription_ID : sub.intClientSubscriptions_parent;
                                        ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: itemData.purchased_ID, parentId: parent, order: sub.intClientSubscriptions_order}).then(function (res) {
                                            if (res.status) {
                                                self.setSubscription(res.data);
                                                listingHandle.close();
                                            }
                                        });
                                    });
                                });
                                listingHandle.afterPathSelect(function (post) {
                                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_subscribe', end_point: {intClientSubscription_purchased_ID: itemData.purchased_ID, intClientSubscription_post_ID: post.post_ID}}, true, 200).then(function (res) {
                                        var sub = res.data;
                                        var parent = sub.strClientSubscription_nodeType == 'path' ? sub.clientsubscription_ID : sub.intClientSubscriptions_parent;
                                        ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: itemData.purchased_ID, parentId: parent, order: sub.intClientSubscriptions_order}).then(function (res) {
                                            if (res.status) {
                                                self.setSubscription(res.data);
                                                listingHandle.close();
                                            }
                                        });
                                    });
                                });
                            }
                        }
                    });
                    domItemHeader.find('.share-btn').click(function () {
                        if (listingHandle) {
                            listingHandle.setMode('share');
                            listingHandle.open();
                            var hdl = listingHandle.getHandle(parseInt(itemData.permission) === 0 ? subscription.clientsubscription_ID : subscription.intClientSubscription_post_ID);
                            if (hdl) {
                                hdl.domInnerWrap().find('.share-action').addClass('shared');
                                setTimeout(function () {
                                    hdl.dom().get(0).scrollIntoView();
                                }, 700);
                            }
                        }
                        else {
                            if (parseInt(itemData.permission) === 0) {
                                listingHandle = new ListingSubscriptionClass(itemData, $('.listing-panel-component-wrap').find('.listing-subscription-component').clone().appendTo($('body')));
                                listingHandle.init();
                                listingHandle.setMode('share');
                                listingHandle.afterLoad(function () {
                                    listingHandle.update(subscription.clientsubscription_ID, {current: true});
                                    listingHandle.open();
                                    var hdl = listingHandle.getHandle(subscription.clientsubscription_ID);
                                    if (hdl) {
                                        hdl.domInnerWrap().find('.share-action').addClass('shared');
                                        setTimeout(function () {
                                            hdl.dom().get(0).scrollIntoView();
                                        }, 700);
                                    }
                                });
                                listingHandle.afterSelect(function (sub) {
                                    self.setSubscription(sub);
                                    listingHandle.close();
                                });
                            } else {
                                listingHandle = new ListingPostClass(itemData, $('.listing-panel-component-wrap').find('.listing-post-component').clone().appendTo($('body')));
                                listingHandle.init();
                                listingHandle.setMode('share');
                                listingHandle.setSelected(subscription.intClientSubscription_post_ID);
                                listingHandle.open().then(function () {
                                    var hdl = listingHandle.getHandle(subscription.intClientSubscription_post_ID);
                                    if (hdl) {
                                        hdl.domInnerWrap().find('.share-action').addClass('shared');
                                        setTimeout(function () {
                                            hdl.dom().get(0).scrollIntoView();
                                        }, 700);
                                    }
                                });
                                listingHandle.afterSelect(function (post) {
                                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_subscribe', end_point: {intClientSubscription_purchased_ID: itemData.purchased_ID, intClientSubscription_post_ID: post.post_ID}}, true, 200).then(function (res) {
                                        var sub = res.data;
                                        var parent = sub.strClientSubscription_nodeType == 'path' ? sub.clientsubscription_ID : sub.intClientSubscriptions_parent;
                                        ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: itemData.purchased_ID, parentId: parent, order: sub.intClientSubscriptions_order}).then(function (res) {
                                            if (res.status) {
                                                self.setSubscription(res.data);
                                                listingHandle.close();
                                            }
                                        });
                                    });
                                });
                                listingHandle.afterPathSelect(function (post) {
                                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_subscribe', end_point: {intClientSubscription_purchased_ID: itemData.purchased_ID, intClientSubscription_post_ID: post.post_ID}}, true, 200).then(function (res) {
                                        var sub = res.data;
                                        var parent = sub.strClientSubscription_nodeType == 'path' ? sub.clientsubscription_ID : sub.intClientSubscriptions_parent;
                                        ajaxAPiHandle.pagePost('my_solutions', {action: 'set_seek', purchasedId: itemData.purchased_ID, parentId: parent, order: sub.intClientSubscriptions_order}).then(function (res) {
                                            if (res.status) {
                                                self.setSubscription(res.data);
                                                listingHandle.close();
                                            }
                                        });
                                    });
                                });
                            }
                        }
                    });
                    domItemHeader.find('.edit-btn').click(function () {
                        if (subscription.intClientSubscription_post_ID) {
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'get', where: {post_ID: subscription.intClientSubscription_post_ID}}).then(function (res) {
                                postEditorHandle.setData(res.data);
                                postEditorHandle.open();
                                postEditorHandle.afterSave(function (changes) {
                                    let fChanges = helperHandle.postToViewFormat(changes);
                                    let sChanges = helperHandle.subscriptionFormat(fChanges);
                                    subscription = Object.assign(subscription, sChanges);
                                    self.setSubscription(subscription);
                                });
                            });
                        }
                    })
                }

                var trash = function () {
                    var ajaxData = {
                        action: 'trash_post',
                        id: subscription.clientsubscription_ID
                    };
                    ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                        if (res.status == true){
                            if (res.data){
                                self.setSubscription(res.data);
                            }
                            else {
                                itemHandles.forEach(function (itemHandle, i) {
                                    if (itemHandle.data().purchased_ID === itemData.purchased_ID){
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                domItem.remove();
                            }
                        }
                        else {
                            alert('something went wrong');
                        }
                    })
                }
                var remove = function () {
                    var ajaxData = {
                        action: 'remove_series',
                        id: itemData.series_ID
                    };
                    ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                        if (res.status){
                            itemHandles.forEach(function (itemHandle, i) {
                                if (itemHandle.data().purchased_ID === itemData.purchased_ID){
                                    itemHandles.splice(i, 1);
                                }
                            });
                            domItem.remove();
                        }
                        else {
                            alert('something went wrong');
                        }
                    });
                }

                var updateBody = function () {
                    ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', where: subscription.clientsubscription_ID, sets: {strClientSubscription_body: domUsedBody.html()}})
                }

                var setComplete = function(v){
                    var ajaxData = {
                        action: 'set_complete',
                        sets: {
                            intSubscription_ID: subscription.clientsubscription_ID,
                            boolCompleted: v,
                        },
                    };
                    ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function () {
                        subscription.boolCompleted = v;
                        if (v) {
                            domItemHeader.find('.complete-action').addClass('completed');
                            domItemHeader.find('.finish-btn').addClass('completed');
                        }
                        else {
                            domItemHeader.find('.complete-action').removeClass('completed');
                            domItemHeader.find('.finish-btn').removeClass('completed');
                        }
                    });
                }
                var setFavorite = function (v) {
                    var ajaxData = {
                        action: 'set_favorite',
                        subscription_id: subscription.clientsubscription_ID,
                        value: v
                    };
                    ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function (res) {
                        if (v) {
                            subscription.favorite = res.data;
                            domItemHeader.find('.favorite-action').addClass('favorite');
                            domItemHeader.find('.favorite-btn').addClass('favorite');
                        }
                        else {
                            subscription.favorite = false;
                            domItemHeader.find('.favorite-action').removeClass('favorite');
                            domItemHeader.find('.favorite-btn').removeClass('favorite');
                        }
                    })
                }
                var selectPath = function (path) {
                    ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'set_seek', purchasedId: itemData.purchased_ID, parentId: path.id}).then(function (res) {
                        if (res.status == true && res.data){
                            self.setSubscription(res.data);
                        }
                        else {
                            stepNext();
                        }
                    });
                }
                var makeBody = function () {

                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    var type = parseInt(subscription.intClientSubscriptions_type);
                    type = availableTypes.indexOf(type) != -1 ? type : 'other';
                    metaData.type = type;
                    var newDomUsedBody;

                    if (subscription.strClientSubscription_nodeType == 'menu') {
                        domBody.find('.item-decoration-container .right-portion').addClass('disabled');
                        newDomUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        domUsedBody = newDomUsedBody;

                        var menuData = helperHandle.subscriptionToViewFormat(subscription);
                        menuData.paths = subscription.paths.map(function (path) {
                            return helperHandle.subscriptionToViewFormat(path);
                        });
                        var menuHandle = new MenuOptionsClass(domUsedBody.find('.menu-options-component'), menuData);
                        menuHandle.init();
                        menuHandle.onSelect(function (option) {
                            selectPath(option);
                        });
                    }
                    else {
                        domBody.find('.item-decoration-container .right-portion').removeClass('disabled');
                        newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        switch (parseInt(type)){
                            case 0:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.audioplayer-tobe').attr('data-thumb', subscription.strClientSubscription_image);
                                domUsedBody.find('.audioplayer-tobe').attr('data-source', subscription.strClientSubscription_body);
                                domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                                var settings_ap = {
                                    disable_volume: 'off',
                                    disable_scrub: 'default',
                                    design_skin: 'skin-wave',
                                    skinwave_dynamicwaves: 'off'
                                };
                                dzsag_init('#audio-' + audioCnt, {
                                    'transition':'fade',
                                    'autoplay' : 'off',
                                    'settings_ap': settings_ap
                                });
                                audioCnt++;
                                break;
                            case 2:
                                domUsedBody = newDomUsedBody;
                                domVideo = domUsedBody.find('video');

                                if (subscription.body_apiType == 'youtube') {
                                    var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": subscription.strClientSubscription_body}], "youtube": { "customVars": { "wmode": "transparent" }} };
                                    domVideo.attr('data-setup', JSON.stringify(setup));
                                }
                                else {
                                    domVideo.append('<source src="'+ subscription.strClientSubscription_body +'" />');
                                }

                                videoHandle = videojs(domVideo.get(0), {
                                    height: domUsedBody.innerWidth() * 540 / 960,
                                    children: [
                                        "mediaLoader",
                                        "posterImage",
                                        "textTrackDisplay",
                                        "loadingSpinner",
                                        "bigPlayButton",
                                        "controlBar",
                                        "errorDisplay",
                                        "textTrackSettings",
                                        "resizeManager",
                                    ]
                                }, function () {
                                    this.one('timeupdate', function() {
                                        domBody.find('.blog-duration').hide();
                                    });
                                });
                                break;
                            case 8:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.append(subscription.strClientSubscription_body);
                                break;
                            case 7:
                                domUsedBody = newDomUsedBody;
                                // domUsedBody.find('.img-wrapper').append('<img src="'+ subscription.strClientSubscription_image +'">');
                                domUsedBody.find('.item-description').append(subscription.strClientSubscription_body);
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new FormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var mode = $(d).hasClass('mode-answer') ? 'answer' : 'question';
                                    var hdl = new FormLoopClass($(d), loopId, {mode: mode});
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-question-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopQuestionClass($(d), loopId);
                                    hdl.init();
                                    hdl.afterChange(function () {
                                        updateBody();
                                    });
                                });
                                domUsedBody.find('.form-loop-answer-parag-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopAnswerParagClass($(d), loopId);
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-answer-table-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopAnswerTableClass($(d), loopId);
                                    hdl.init();
                                });
                                break;
                            case 10:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.item-description').append(subscription.strClientSubscription_body);
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new FormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var mode = $(d).hasClass('mode-answer') ? 'answer' : 'question';
                                    var hdl = new FormLoopClass($(d), loopId, {mode: mode});
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-question-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopQuestionClass($(d), loopId);
                                    hdl.init();
                                    hdl.afterChange(function () {
                                        updateBody();
                                    });
                                });
                                domUsedBody.find('.form-loop-answer-parag-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopAnswerParagClass($(d), loopId);
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-answer-table-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopAnswerTableClass($(d), loopId);
                                    hdl.init();
                                });
                                break;
                            default:
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.item-description').append(subscription.strClientSubscription_body);
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    itemData.formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new FormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var mode = $(d).hasClass('mode-answer') ? 'answer' : 'question';
                                    var hdl = new FormLoopClass($(d), loopId, {mode: mode});
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-question-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopQuestionClass($(d), loopId);
                                    hdl.init();
                                    hdl.afterChange(function () {
                                        updateBody();
                                    });
                                });
                                domUsedBody.find('.form-loop-answer-parag-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopAnswerParagClass($(d), loopId);
                                    hdl.init();
                                });
                                domUsedBody.find('.form-loop-answer-table-component').each(function (i, d) {
                                    var loopId = $(d).attr('data-form-loop');
                                    var hdl = new FormLoopAnswerTableClass($(d), loopId);
                                    hdl.init();
                                });
                                break;
                        }
                    }
                }
                var stepNext = function () {
                    var ajaxData = {
                        action: 'step_next',
                        frequency: itemData.intSeries_frequency_ID,
                        purchasedId: itemData.purchased_ID,
                        id: subscription.clientsubscription_ID
                    };
                    ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                        if (res.status == true){
                            self.setSubscription(res.data);
                        }
                        else {
                            alert('something went wrong');
                        }
                    });
                }
                var stepPrev = function () {
                    var ajaxData = {
                        action: 'step_prev',
                        frequency: itemData.intSeries_frequency_ID,
                        purchasedId: itemData.purchased_ID,
                        id: subscription.clientsubscription_ID
                    };
                    ajaxAPiHandle.pagePost(CURRENT_PAGE, ajaxData).then(function (res) {
                        if (res.status == true){
                            self.setSubscription(res.data);
                        }
                        else {
                            alert('something went wrong');
                        }
                    })
                }

                this.socialLinks = function () {
                    var viewUri = encodeURIComponent(BASE_URL + '/view_blog?id=' + subscription.intClientSubscription_post_ID);
                    var title = encodeURIComponent(subscription.strClientSubscription_title);
                    var subject = encodeURIComponent('Shared from Walden.ly - ' + subscription.strClientSubscription_title);
                    var shareLinks = {
                        facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + viewUri + '&title=' + title,
                        twitter: 'https://twitter.com/intent/tweet?url=' + viewUri,
                        google: 'https://plus.google.com/share?url=' + viewUri,
                        email: 'mailto:?subject=' + subject + '&body=' + encodeURIComponent('I thought you might like this: ') + viewUri,
                        linkedin: 'https://www.linkedin.com/shareArticle?mini=true&url='+ viewUri +'&title='+ title +'&summary='+ subject +'&source='
                    }
                    return shareLinks;
                }
                this.data = function () {
                    return itemData;
                }
                this.bindData = function () {
                    domItem.removeClass('type-0').removeClass('type-2').removeClass('type-7').removeClass('type-8').removeClass('type-10');
                    domItem.addClass('type-' + subscription.intClientSubscriptions_type);
                    domItem.find('.article-title').html(subscription.strClientSubscription_title);
                    if (subscription.intClientSubscription_post_ID && parseInt(itemData.permission)) {
                        domItem.find('.article-title').attr('href', 'view_blog?id=' + subscription.intClientSubscription_post_ID);
                    } else {
                        domItem.find('.article-title').attr('href', 'view_blog?id=' + subscription.clientsubscription_ID + '&from=subscription');
                    }
                    domItemHeader.find('.view-day').html(subscription.viewDay);
                    if (subscription.boolCompleted) {
                        domItemHeader.find('.complete-action').addClass('completed');
                        domItemHeader.find('.finish-btn').addClass('completed');
                    }
                    else {
                        domItemHeader.find('.complete-action').removeClass('completed');
                        domItemHeader.find('.finish-btn').removeClass('completed');
                    }
                    if (subscription.favorite) {
                        domItemHeader.find('.favorite-action').addClass('favorite');
                        domItemHeader.find('.favorite-btn').addClass('favorite');
                    }
                    else {
                        domItemHeader.find('.favorite-action').removeClass('favorite');
                        domItemHeader.find('.favorite-btn').removeClass('favorite');
                    }

                    var socialLinks = self.socialLinks();
                    domCircleNavWrap.find('.facebook-link').attr('href', socialLinks.facebook);
                    domCircleNavWrap.find('.twitter-link').attr('href', socialLinks.twitter);
                    domCircleNavWrap.find('.google-link').attr('href', socialLinks.google);
                    domCircleNavWrap.find('.linkedin-link').attr('href', socialLinks.linkedin);
                    domCircleNavWrap.find('.mail-link').attr('href', socialLinks.email);
                    if (subscription.strSubscription_duration) {
                        domBody.find('.blog-duration').html(subscription.strSubscription_duration);
                    }
                    else {
                        helperHandle.getBlogDuration(subscription.origin_body, subscription.intClientSubscriptions_type).then(function (res) {
                            domBody.find('.blog-duration').html(res);
                            subscription.strSubscription_duration = res;
                            ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', where: subscription.clientsubscription_ID, sets: {strSubscription_duration: res}}, false);
                        });
                    }
                    var postNotesData = {
                        postNotes: subscription.postNotes,
                        postId: subscription.intClientSubscription_post_ID,
                        subscriptionId: subscription.clientsubscription_ID,
                        options: {
                            mobileStatus: 'static',
                        }
                    };
                    if (notesHandle) {
                        notesHandle.setData(postNotesData);
                    }
                    else {
                        notesHandle = new PostNotesClass(domItem.find('.post-notes-component'), postNotesData);
                        notesHandle.init();
                    }

                    makeBody();
                }
                this.setSubscription = function (subs) {
                    if (listingHandle) {
                        listingHandle.update(subscription.clientsubscription_ID, {current: false});
                    }
                    itemData.susubscription = subscription = subs;
                    if (listingHandle) {
                        listingHandle.update(subscription.clientsubscription_ID, {current: true});
                    }
                    self.bindData();
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.append = function () {
                    domItem.appendTo(domRoot);
                }
                this.dom = function () {
                    return domItem;
                }
                this.makeBody = makeBody;
                this.init = function () {
                    domItem = domRoot.find('.blog-article.sample').clone().removeClass('sample').removeAttr('hidden');
                    domItemHeader = domItem.find('.article-header');
                    domCircleNavWrap = domItem.find('.circle-nav-wrapper');
                    domBody = domItem.find('.article-body');
                    subscription = itemData.subscription;
                    domItem.find('.share-post').circleNav(domItem.find('.circle-nav-wrapper'));
                    domItemHeader.find('.series-name').html(itemData.strSeries_title);
                    domItemHeader.find('.edit-action').attr('href', 'edit_series?id=' + itemData.series_ID);
                    domItemHeader.find('.embed-action').attr('href', 'edit_series?tab=embed&id=' + itemData.series_ID);
                    domItemHeader.find('.add-action').attr('href', 'add_posts?id=' + itemData.series_ID);
                    if (parseInt(itemData.unReadPostsCount)) {
                        domItemHeader.find('.listing-toggle').addClass('has-new').find('.unread-posts-count').html(itemData.unReadPostsCount);
                    }
                    if (parseInt(itemData.permission) === 0) {
                        domItemHeader.find('.edit-action').remove();
                        domItemHeader.find('.add-action').remove();
                    }
                    domItem.find('[data-toggle="popover"]').popover({
                        trigger: 'hover'
                    });
                    bindEvents();
                }
            }

            this.applyFilter = function (category, filter) {
                domRoot.css('height', domRoot.css('height'));
                domRoot.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (hdl) {
                        hdl.detach();
                    });
                    filteredHandles = itemHandles.filter(function (hdl) {
                        if (parseInt(category) === -1) {
                            return true;
                        }
                        else if (parseInt(category) === parseInt(hdl.data().intSeries_category)) {
                            return true;
                        }
                        return false;
                    });
                    filteredHandles = filteredHandles.filter(function (value) {
                        if (parseInt(filter) === -1) {
                            return true;
                        }
                        else if (parseInt(filter) === parseInt(value.data().subscription.intClientSubscriptions_type)) {
                            return true;
                        }
                        return false;
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                        if (parseInt(value.data().subscription.intClientSubscriptions_type) === 2) {
                            value.makeBody();
                        }
                    });
                    domRoot.css('opacity', 1);
                    domRoot.css('height', 'auto');
                }, 300);
            }

            this.init = function () {
                domRoot = domPage.find('.main-col section.articles-container');
                series.forEach(function (itemData) {
                    var hdl = new ItemClass(itemData);
                    hdl.init();
                    hdl.append();
                    hdl.bindData();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                });
            }
        }

        var GoalsClass = function () {
            var domRoot;

            var normalGoalsHandle, morningHandle, eveningHandle, addModalHandle;
            var activeDate = new Date().yyyymmdd();
            let today = new Date().yyyymmdd();

            var NormalGoalsClass = function () {

                var domNormal, domNormalList;

                var self = this;

                var itemHandles = [];

                var ItemClass = function (itemData) {
                    var domItem, domActions;
                    var self = this;
                    var bindEvents = function () {
                        domActions.find('.action-item.delete-action').click(function () {
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this goal?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    self.delete();
                                }
                            });
                        });
                        domActions.find('.complete-action').click(function () {
                            self.update({boolGoal_complete: itemData.boolGoal_complete ? 0 : 1});
                        });
                        domActions.find('.item-for-tomorrow').click(function () {
                            let currentDate = itemData.dtGoal_date.stringToDate();
                            currentDate.setDate(currentDate.getDate() + 1);
                            self.update({dtGoal_date: currentDate.yyyymmdd()}).then(function () {
                                normalGoalsHandle.refresh();
                            });
                        });
                        domActions.find('.item-goal-date input.active-date').change(function () {
                            $(this).parent().addClass('added');
                            var d = $(this).val();
                            $(this).parent().data('when', d);
                            $(this).datepicker('hide');
                            self.update({dtGoal_date: d}).then(function () {
                                normalGoalsHandle.refresh();
                            });
                        });
                    }
                    var bindData = function () {
                        domItem.find('.goal-text').html(itemData.strGoal_text);
                        domItem.find('.item-date').html(itemData.dtGoal_date);
                        if (itemData.boolGoal_complete) {
                            domItem.addClass('completed');
                        }
                        else {
                            domItem.removeClass('completed');
                        }
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.update = function (sets) {
                        return ajaxAPiHandle.apiPost('Goals.php', {action: 'update', where: itemData.goal_ID, sets: sets}).then(function (res) {
                            if (res.status) {
                                Object.assign(itemData, sets);
                                bindData();
                                return sets;
                            }
                        });
                    }
                    this.delete = function () {
                        ajaxAPiHandle.apiPost('Goals.php', {action: 'delete', where: itemData.goal_ID}).then(function (res) {
                            if (res.status) {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().goal_ID === itemData.goal_ID) {
                                        itemHandles.splice(i, 1);
                                        self.remove();
                                    }
                                });
                            }
                        })
                    }
                    this.remove = function () {
                        domItem.remove();
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domNormalList);
                    }
                    this.init = function () {
                        domItem = domNormal.find('.goal-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        domActions = domItem.find('.goal-actions');
                        domItem.find('.item-goal-date input.active-date').datepicker();
                        bindData();
                        bindEvents();

                    }
                }
                this.refresh = function () {
                    itemHandles.forEach(function (value) {
                        value.remove();
                    });
                    itemHandles = [];
                    goals.normalGoals = goals.normalGoals.sort(function (a, b) {
                        if (a.dtGoal_date > b.dtGoal_date) {
                            return 1;
                        }
                        if (a.dtGoal_date < b.dtGoal_date) {
                            return -1;
                        }
                        return 0;
                    });
                    goals.normalGoals.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        itemHandles.push(hdl);
                    });
                }
                this.insert = function (sets) {
                    var defaultSets = {
                        intGoal_type: 2,
                        dtGoal_date: activeDate,
                    }
                    sets = Object.assign(defaultSets, sets);
                    ajaxAPiHandle.apiPost('Goals.php', {action: 'insert', sets: sets}).then(function (res) {
                        if (res.status){
                            if (sets.dtGoal_date >= activeDate) {
                                goals.normalGoals.push(res.data);
                                normalGoalsHandle.refresh();
                            }
                            return res;
                        }
                        return false;
                    });
                }
                this.setDate = function (d) {
                    ajaxAPiHandle.apiPost('Goals.php', {action: 'get_goals_data_by_date', date: d}).then(function (res) {
                        if (res.status) {
                            activeDate = d;
                            goals.normalGoals = res.data.normalGoals;
                            self.refresh();
                        }
                    });
                }
                this.init = function () {
                    domNormal = domRoot.find('.goals-container');
                    domNormalList = domNormal.find('.goal--list');
                    goals.normalGoals = goals.normalGoals.sort(function (a, b) {
                        if (a.dtGoal_date > b.dtGoal_date) {
                            return 1;
                        }
                        if (a.dtGoal_date < b.dtGoal_date) {
                            return -1;
                        }
                        return 0;
                    });
                    goals.normalGoals.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        itemHandles.push(hdl);
                    });
                    domNormalList.sortable({
                        placeholder: 'ui-state-highlight',
                        items: '.goal-item:not(.sample)',
                        coneHelperSize: true,
                        forcePlaceholderSize: true,
                        tolerance: "pointer",
                        helper: "clone",
                        revert: 300, // animation in milliseconds
                        handle: '.drag-icon-wrapper',
                        opacity: 0.9,
                        update: function (a, b) {}
                    });
                }
            }

            var MorEvenClass = function (domMERoot, data) {
                var domContainer;

                var self = this;
                var itemHandles = [];

                var helperRow = false;
                var ItemClass = function (itemData) {
                    var domItem, domActions;
                    var self = this;
                    var displayTxt;
                    var bindEvents = function () {
                        domActions.find('.action-item.delete-action').click(function () {
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this goal?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    self.delete();
                                }
                            });
                        });
                        domActions.find('.complete-action').click(function () {
                            self.update({boolGoal_complete: itemData.boolGoal_complete ? 0 : 1});
                        });
                    }
                    var bindData = function () {
                        domItem.find('.goal-text').html(displayTxt);
                        if (itemData.boolGoal_complete) {
                            domItem.addClass('completed');
                        }
                        else {
                            domItem.removeClass('completed');
                        }
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.update = function (sets) {
                        ajaxAPiHandle.apiPost('Goals.php', {action: 'update', where: itemData.goal_ID, sets: sets}).then(function (res) {
                            if (res.status) {
                                Object.assign(itemData, sets);
                                bindData();
                            }
                        });
                    }
                    this.delete = function () {
                        ajaxAPiHandle.apiPost('Goals.php', {action: 'delete', where: itemData.goal_ID}).then(function (res) {
                            if (res.status) {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().goal_ID === itemData.goal_ID) {
                                        itemHandles.splice(i, 1);
                                        self.remove();
                                    }
                                });
                            }
                        });
                    }
                    this.remove = function () {
                        domItem.remove();
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.append = function () {
                        domItem.appendTo(domContainer);
                    }
                    this.init = function () {
                        domItem = domContainer.find('.mor-even-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        domActions = domItem.find('.goal-actions');
                        displayTxt = itemData.strGoal_text.length > 300 ? itemData.strGoal_text.substr(0, 300) + '...' : itemData.strGoal_text;
                        bindData();
                        bindEvents();

                    }
                }

                var bindEvents = function () {
                    domMERoot.find('.add-goal').mousedown(function () {
                        var txt = domMERoot.find('.goal-input').val();
                        if (txt) {
                            self.insert({strGoal_text: txt});
                            domMERoot.find('.goal-input').val('');
                        }
                    });
                    domMERoot.find('.need-help').click(function () {
                        fillRandPage();
                    });
                }

                var fillRandPage = function () {
                    var where = {
                        strHelper_key: data.type == 4 ? 'morning_page' : 'evening_page',
                    };
                    if (helperRow){
                        where['helper_ID !='] = helperRow.helper_ID;
                    }
                    ajaxAPiHandle.apiPost('Helpers.php', {action: 'rand', where: where}).then(function (res) {
                        if (res.status == true){
                            if (res.data){
                                helperRow = res.data;
                                var helperText = res.data.strHelper_value;
                                domMERoot.find('textarea.goal-input').html(helperText);
                            }
                        }
                    })
                }
                this.insert = function (sets) {
                    var defaultSets = {
                        intGoal_type: data.type
                    }
                    sets = Object.assign(defaultSets, sets);
                    ajaxAPiHandle.apiPost('Goals.php', {action: 'insert', sets: sets}).then(function (res) {
                        if (res.status){
                            var handle = new ItemClass(res.data);
                            handle.init();
                            handle.append();
                            itemHandles.push(handle);
                        }
                    });
                }

                this.init = function () {
                    domContainer = domMERoot.find('.mor-even-container');
                    data.items.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        itemHandles.push(hdl);
                    });
                    bindEvents();
                }
            }

            var AddGoalModalClass = function () {
                var domModal;
                var activeDate = new Date().yyyymmdd();

                var bindEvents = function () {
                    domModal.find('.goal-date').click(function () {
                        domModal.find('.goal-date').removeClass('active');
                        $(this).addClass('active');
                        var d = $(this).data('when');
                        if (d === 'calendar') {
                            return true;
                        }
                        activeDate = d;
                    });
                    domModal.find('.add-btn').click(function () {
                        var txt = domModal.find('.goal-input').val();
                        if (txt) {
                            normalGoalsHandle.insert({dtGoal_date: activeDate, strGoal_text: txt});
                            domModal.modal('hide');
                        }
                    });
                    domModal.find('.goal-date input.active-date').change(function () {
                        $(this).parent().addClass('added');
                        var d = $(this).val();
                        $(this).parent().data('when', d);
                        activeDate = d;
                    });
                }

                this.init = function () {
                    domModal = $('#add-goal-modal');
                    domModal.find('.goal-date input.active-date').datepicker();
                    bindEvents();
                }
            }

            var bindEvents = function () {
                domRoot.find('.goal-input[data-type="normal"]').keyup(function (e) {
                    var code = e.keyCode ? e.keyCode : e.which;
                    if (code === 13) {
                        normalGoalsHandle.insert({strGoal_text: $(this).val().replace(/\n$/, '')});
                        $(this).val('');
                    }
                });
                domRoot.find('.goal-date').click(function () {
                    domRoot.find('.goal-date').removeClass('active');
                    $(this).addClass('active');
                    var d = $(this).data('when');
                    if (d === 'calendar') {
                        return true;
                    }
                    goalsHandle.normalGoalsHandle().setDate(d);
                });
                domRoot.find('.goal-date input.active-date').change(function () {
                    $(this).parent().addClass('added');
                    var d = $(this).val();
                    $(this).parent().data('when', d);
                    goalsHandle.normalGoalsHandle().setDate(d);
                });
            }

            this.normalGoalsHandle = function () {
                return normalGoalsHandle;
            };


            this.init = function () {
                domRoot = domPage.find('.goals-col section.goals-section');
                domRoot.find('[data-toggle="popover"]').popover({
                    trigger: 'hover'
                });
                normalGoalsHandle = new NormalGoalsClass();
                normalGoalsHandle.init();

                morningHandle = new MorEvenClass(domRoot.find('.morning-section'), {items: goals.morningGoals, type: 4});
                morningHandle.init();

                eveningHandle = new MorEvenClass(domRoot.find('.evening-section'), {items: goals.eveningGoals, type: 5});
                eveningHandle.init();

                addModalHandle = new AddGoalModalClass();
                addModalHandle.init();
                domRoot.find('.goal-date input.active-date').datepicker();
                bindEvents();
            }
        }

        let GettingStartedHeaderClass = function() {
            let domRoot, domStepProgress;
            this.step = function (v) {
                domRoot.find('.title-wrapper .step-value').html(v);
                domStepProgress.find('.progress-item').removeClass('active');
                domStepProgress.find('.progress-item.step-' + v).addClass('active');
            }
            this.hide = function () {
                domRoot.hide();
            }
            this.init = function () {
                domRoot = domPage.find('.getting-started-header');
                domStepProgress = domRoot.find('.step-progress-wrapper');
            }
        }

        let WhatDoFirstClass = function () {
            let domRoot;
            let bindEvents = function () {
                domRoot.find('[href="#select-categories-tab"]').on('shown.bs.tab', function () {
                    selectCategoriesHandle = new SelectCategoriesClass();
                    selectCategoriesHandle.init();
                    gettingStartedHeaderHandle.step(2);
                });
            }
            this.init = function () {
                domRoot = domPage.find('.what-do-first-pane');
                bindEvents();
            }
        }

        var SelectCategoriesClass = function () {
            var domRoot, domItemsList;
            var itemHandles = [], filteredHandles = [];
            var boolSimplify = true, boolEnrich = true;
            var selectedCnt = 0, paneSeries = [];

            var bindEvents = function () {
                domRoot.find('.simplify-btn').click(function () {
                    $(this).toggleClass('selected');
                    boolSimplify = $(this).hasClass('selected');
                    applyFilter();
                });
                domRoot.find('.enrich-btn').click(function () {
                    $(this).toggleClass('selected');
                    boolEnrich = $(this).hasClass('selected');
                    applyFilter();
                });
                domRoot.find('.show-me-btn').click(function (e) {
                    e.preventDefault();
                    if ( !$(this).attr('disabled') ) {
                        showMe();
                    }
                    return false;
                });
                domRoot.find('.show-me-btn').on('shown.bs.tab', function (e) {
                    selectSeriesHandle = new SelectSeriesClass(paneSeries);
                    selectSeriesHandle.init();
                    gettingStartedHeaderHandle.step(3);
                    categories = itemHandles.filter(function (value) {
                        return value.isSelected();
                    }).map(function (value) {
                        return value.data();
                    });
                    setTimeout(function () {
                        $('html, body').stop().animate({
                            scrollTop: domPage.find('.select-series-pane').offset().top - 80
                        }, 300, 'swing');
                    }, 100);
                });
            }
            var showMe = function () {
                var ajaxData = {
                    action: 'get_many',
                    where: {
                        intSeries_category: [],
                    }
                };
                itemHandles.forEach(function (value) {
                    if (value.isSelected()) {
                        ajaxData.where.intSeries_category.push(value.data().category_ID);
                    }
                });
                ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                    paneSeries = res.data;
                    domRoot.find('.show-me-btn').tab('show');
                });
            }
            var ItemClass = function (itemData) {
                var domItem;
                var self = this;

                var bindEvents = function () {
                    domItem.find('.item-image-wrapper').click(function () {
                        domItem.toggleClass('selected');
                        selectedCnt += domItem.hasClass('selected') ? 1 : -1;
                        checkNextAvailable();
                    });
                }
                this.isSelected = function () {
                    return domItem.hasClass('selected');
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domItemsList);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = function () {
                    domItem.find('.item-image').css('background-image', 'url(' + itemData.strCategory_image + ')');
                    domItem.find('.child-count span').text(itemData.series_count);
                    domItem.find('.item-title').text(itemData.strCategory_name);
                }
                this.init = function () {
                    domItem = domItemsList.find('.category-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemsList);
                    self.bindData();
                    bindEvents();
                }
            }

            var applyFilter = function () {
                domItemsList.css('height', domItemsList.css('height'));
                domItemsList.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (hdl) {
                        hdl.detach();
                    });
                    filteredHandles = itemHandles.filter(function (hdl) {
                        var d = hdl.data();
                        if (boolSimplify && d.boolSimplify) {
                            return true;
                        }
                        if (boolEnrich && d.boolEnrich) {
                            return true;
                        }
                        return false;
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domItemsList.css('opacity', 1);
                    domItemsList.css('height', 'auto');
                }, 300);
            }

            var checkNextAvailable = function () {
                if (selectedCnt >= 3) {
                    domRoot.find('.show-me-btn').removeAttr('disabled');
                }
                else {
                    domRoot.find('.show-me-btn').attr('disabled', true);
                }
            }

            this.selectedCategories = function () {
                var rtn = [];
                itemHandles.forEach(function (value) {
                    if (value.isSelected()) {
                        rtn.push(value.data().category_ID);
                    }
                });
                return rtn;
            }
            this.init = function () {
                domRoot = domPage.find('.select-categories-pane');
                domItemsList = domRoot.find('.categories-list');
                allCategories.sort(function (a, b) {
                    var strA = a.strCategory_name.toLowerCase();
                    var strB = b.strCategory_name.toLowerCase();

                    if (strA < strB) {
                        return -1;
                    }
                    if (strA > strB) {
                        return 1;
                    }
                    return 0;
                });
                allCategories.forEach(function (itemData) {
                    var hdl = new ItemClass(itemData);
                    hdl.init();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                });
                bindEvents();
            }
        }

        var SelectSeriesClass = function (paneSeries) {

            var domRoot, domItemsList;

            var itemHandles = [];
            var selectedCnt = 0;
            let categories = [];
            let searched = false;
            let keyword = '';

            var bindEvents = function () {
                domRoot.find('.join-all-btn').click(function () {
                    joinAll();
                });
                domRoot.find('.let-go-btn').on('shown.bs.tab', function () {
                    gettingStartedHeaderHandle.hide();
                    filterHandle = new FilterClass();
                    filterHandle.init();
                    filterHandle.show();
                    filterHandle.fillCategories();
                    mainHandle = new MainClass();
                    mainHandle.init();
                    domPage.find('.experience-goal-switcher-wrapper').show();
                    setTimeout(function () {
                        $('html, body').stop().animate({scrollTop: '0px'}, 300);
                    }, 100);
                });
                domRoot.find('.let-go-btn').click(function (e) {
                    e.preventDefault();
                    if ( !$(this).attr('disabled') ) {
                        letGo();
                    }
                    return false;
                });
                domRoot.find('.load-more').click(function () {
                    loadMore();
                });
                domRoot.find('.search-input-wrapper').submit(function (e) {
                    e.preventDefault();
                    let keyword = domRoot.find('.search-input-wrapper input').val();
                    searchSeries(keyword);
                    return false;
                })
            }

            var ItemClass = function (itemData) {
                var domItem;
                var self = this, payHandle = null;

                var popupErroJoin = function () {
                    swal("Sorry, Something went wrong!", {
                        content: 'please try again later'
                    });
                }

                var openStripe = function () {
                    if (payHandle) {
                        payHandle.open({
                            name: itemData.strSeries_title,
                            description: itemData.strSeries_description,
                            currency: 'usd',
                            amount: parseFloat((itemData.intSeries_price * 100).toFixed(2))
                        });
                    }
                    else {
                        popupErroJoin();
                    }
                }

                var join = function (token) {
                    ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: itemData.series_ID, token: token}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = res.data;
                            domItem.addClass('purchased');
                            selectedCnt++;
                            if (selectedCnt >= 3) {
                                domRoot.find('.let-go-btn').removeAttr('disabled');
                            }
                        }
                    });
                }

                var unJoin = function () {
                    swal("You already joined this series!", {
                        content: 'Thanks!'
                    });
                    return false;

                    ajaxAPiHandle.apiPost('Series.php', {action: 'unJoin', id: itemData.purchased}).then(function (res) {
                        if (res.status == true){
                            itemData.purchased = 0;
                        }
                    });
                }
                var openStripeCard = function () {
                    stripeCardModalHandle.setSeries(itemData);
                    stripeCardModalHandle.open();
                    stripeCardModalHandle.afterJoin(function (res) {
                        if (res.status == true){
                            itemData.purchased = res.data;
                            domItem.addClass('purchased');
                            selectedCnt++;
                            if (selectedCnt >= 3) {
                                domRoot.find('.let-go-btn').removeAttr('disabled');
                            }
                        }
                    });
                }
                var bindEvents = function () {
                    domItem.find('.join-btn').click(function () {
                        if (parseInt(itemData.boolSeries_charge) === 1 && parseInt(itemData.purchased) == 0) {
                            openStripeCard();
                        }
                        else {
                            parseInt(itemData.purchased) ? unJoin() : join();
                        }
                    });
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domItemsList);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.bindData = function () {
                    if ( parseInt(itemData.purchased) ) {
                        domItem.addClass('purchased');
                    }
                    domItem.find('.item-image').css('background-image', 'url(' + itemData.strSeries_image + ')');
                    domItem.find('.child-count span').text(itemData.posts_count);
                    domItem.find('.item-title').text(itemData.strSeries_title);
                    domItem.find('.preview-btn').attr('href', 'preview_series?id=' + itemData.series_ID);
                    domItem.find('.item-category').attr('href', 'browse?id=' + itemData.category.category_ID).html(itemData.category.strCategory_name);
                }
                this.remove = function () {
                    domItem.remove();
                }
                this.init = function () {
                    domItem = domItemsList.find('.series-article.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domItemsList);
                    self.bindData();
                    if (itemData.boolSeries_charge == 1 && itemData.purchased == 0) {
                        if (stripeApi_pKey && itemData.stripe_account_id) {
                            payHandle = StripeCheckout.configure({
                                key: stripeApi_pKey,
                                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                locale: 'auto',
                                token: function(token) {
                                    join(token);
                                }
                            });
                        }
                    }
                    bindEvents();
                }
            }

            var joinAll = function () {
                selectedCnt = 0;
                var ajaxData = {
                    action: 'join_many',
                    items: []
                }
                itemHandles.forEach(function (value) {
                    var d = value.data();
                    if (!parseInt(d.boolSeries_charge)) {
                        selectedCnt++;
                        ajaxData.items.push(d.series_ID);
                    }
                });
                ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                    if (res.status) {
                        swal("SUCCESS!", 'You have successfully joined '+ selectedCnt +' series', "success", {
                            button: "OK"
                        }).then(function () {
                            letGo();
                        });
                    }
                    else {
                        swal("Sorry, Something went wrong!", {
                            content: 'please try again later'
                        });
                    }
                });
            }

            var loadMore = function () {
                domRoot.find('.load-more-status').addClass('status-loading');
                if (searched) {
                    let ajaxData = {
                        action: 'search',
                        keyword: keyword,
                        pageToken: itemHandles.length,
                        size: 20
                    };
                    ajaxAPiHandle.apiPost('Series.php', ajaxData, false).then(function (res) {
                        res.data.forEach(function (itemData) {
                            var hdl = new ItemClass(itemData);
                            hdl.init();
                            itemHandles.push(hdl);
                        });
                        domRoot.find('.load-more-status').removeClass('status-loading');
                        if (res.data.length < ajaxData.size) {
                            domRoot.find('.load-more-status').hide();
                        }
                    });
                } else {
                    var ajaxData = {
                        action: 'get_many',
                        where: {
                            intSeries_category: selectCategoriesHandle.selectedCategories()
                        },
                        pageToken: itemHandles.length,
                        size: 20,
                    };
                    ajaxAPiHandle.apiPost('Series.php', ajaxData, false).then(function (res) {
                        res.data.forEach(function (v) {
                            var hdl = new ItemClass(v);
                            hdl.init();
                            itemHandles.push(hdl);
                        });
                        domRoot.find('.load-more-status').removeClass('status-loading');
                        if (res.data.length < ajaxData.size) {
                            domRoot.find('.load-more-status').hide();
                        }
                    });
                }
            }

            var letGo = function () {
                return ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'get_today_experiences'}).then(function (res) {
                    var d = res.data;
                    series = d.series;
                    formFieldAnswers = d.formFieldAnswers;
                    categories = d.categories;
                    domRoot.find('.let-go-btn').tab('show');
                });
            }
            let searchSeries = function (kwd) {
                keyword = kwd;
                let ajaxData = {action: 'search', keyword: kwd, pageToken: 0, size: 20};
                ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                    itemHandles.forEach(function (handle) {
                        handle.remove();
                    });
                    itemHandles = [];
                    res.data.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        itemHandles.push(hdl);
                    });
                    searched = true;
                    domRoot.find('.load-more-status').show();
                });
            }
            this.init = function () {
                domRoot = domPage.find('.select-series-pane');
                domItemsList = domRoot.find('.series-list');
                paneSeries.forEach(function (itemData) {
                    var hdl = new ItemClass(itemData);
                    hdl.init();
                    itemHandles.push(hdl);
                });
                bindEvents();
            }
        }

        var bindEvents = function () {
            domPage.find('.experience-goal-switcher .experience-switch').click(function () {
                domPage.find('.experience-goal-switcher .switch-item').removeClass('active-item');
                $(this).addClass('active-item');
                domPage.find('.main-content').css('min-height', domPage.find('.main-content').css('height'));
                domPage.find('.main-content').css('opacity', 0);
                setTimeout(function () {
                    domPage.find('.main-content').removeClass('goals-active').addClass('experiences-active');
                    domPage.find('.main-content').css('min-height', '200px');
                    domPage.find('.main-content').css('opacity', 1);
                }, 300);
            });
            domPage.find('.experience-goal-switcher .goal-switch').click(function () {
                domPage.find('.experience-goal-switcher .switch-item').removeClass('active-item');
                $(this).addClass('active-item');
                domPage.find('.main-content').css('min-height', domPage.find('.main-content').css('height'));
                domPage.find('.main-content').css('opacity', 0);
                setTimeout(function () {
                    domPage.find('.main-content').removeClass('experiences-active').addClass('goals-active');
                    domPage.find('.main-content').css('min-height', '200px');
                    domPage.find('.main-content').css('opacity', 1);
                }, 300);
            });
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');

            if (series.length) {
                filterHandle = new FilterClass();
                filterHandle.init();
                mainHandle = new MainClass();
                mainHandle.init();
            } else {
                gettingStartedHeaderHandle = new GettingStartedHeaderClass();
                gettingStartedHeaderHandle.init();
                whatDoFirstHandle = new WhatDoFirstClass();
                whatDoFirstHandle.init();
            }
            goalsHandle = new GoalsClass();
            goalsHandle.init();

            postEditorHandle = new PostEditorClass($('.site-wrapper > .post-editor'));
            postEditorHandle.init();
            bindEvents();
        }
    }

    var pageHandle = new PageClass();
    pageHandle.init();
})();