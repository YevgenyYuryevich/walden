(function () {
    'use strict';

    var viewHandle;
    var is_loading_controlled_in_local = false;

    var ViewClass = function () {

        var domRoot;
        var domArticle;
        var domUsedBody, domBody, domVideo;
        var postData;
        var audioCnt = 0, metaData = {};

        var stickyHeaderHandle, stickyFooterHandle;
        var fontSizeChangeUnit = 1;
        var defaultFontSize = 24;
        var availableTypes = [0, 2, 8, 10];
        var self = this;

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            paths: 'paths',
            free: 'boolPost_free'
        };

        var subscriptionFiels = {
            id: 'clientsubscription_ID',
            title: 'strClientSubscription_title',
            body: 'strClientSubscription_body',
            image: 'strClientSubscription_image',
            type: 'intClientSubscriptions_type',
            order: 'intClientSubscriptions_order',
            nodeType: 'strClientSubscription_nodeType',
            parent: 'intClientSubscriptions_parent',
            paths: 'paths',
            completed: 'boolCompleted',
        };

        var videoHandle = null;
        var videoCnt = 1;
        var payBlogHandle = null;
        var stripeCardModalHandle = false;
        var FormFieldClass = function (domField) {
            var domValue;
            var field, answer = false;

            var bindEvents = function () {
                switch (field.strFormField_type) {
                    case 'text':
                        domValue.blur(function () {
                            updateAnswer();
                        });
                        break;
                    case 'checkbox':
                        domValue.find('input').change(function () {
                            updateAnswer();
                        });
                        break;
                }
            }
            var updateAnswer = function () {
                if (CLIENT_ID == -1) {
                    return false;
                }
                var sets = {};
                is_loading_controlled_in_local = true;
                domField.addClass('status-loading');
                switch (field.strFormField_type) {
                    case 'text':
                        sets.strFormFieldAnswer_answer = domValue.html();
                        break;
                    case 'checkbox':
                        sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                        break;
                }
                if (answer) {
                    ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                        domField.removeClass('status-loading');
                        domField.addClass('status-loading-finished');
                        domField.one(animEndEventName, function () {
                            console.log('ani end');
                            domField.removeClass('status-loading-finished');
                        });
                        answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                    });
                }
                else {
                    $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                    ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                        domField.removeClass('status-loading');
                        domField.addClass('status-loading-finished');
                        answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                        domField.one(animEndEventName, function () {
                            console.log('ani end');
                            domField.removeClass('status-loading-finished');
                        });
                    });
                }
                is_loading_controlled_in_local = false;
            }
            var bindData = function () {
                formFields.forEach(function (formField) {
                    if (formField.formField_ID == domField.attr('data-form-field')) {
                        field = formField;
                    }
                });
                formFieldAnswers.forEach(function (formFieldAnswer) {
                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                        answer = formFieldAnswer;
                    }
                });
                if (answer) {
                    switch (field.strFormField_type) {
                        case 'text':
                            domValue.html(answer.strFormFieldAnswer_answer);
                            break;
                        case 'checkbox':
                            domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                            break;
                    }
                }
            }
            this.init = function () {
                domValue = domField.find('.form-field-value');
                domField.find('[data-toggle="popover"]').popover();
                bindData();
                bindEvents();
            }
        }

        var viewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                if (from === 'post') {
                    fk = helperHandle.keyOfVal(postFields, k);
                    if (fk) { fData[fk] = data[k]; }
                }
                else {
                    fk = helperHandle.keyOfVal(subscriptionFiels, k);
                    if (fk) { fData[fk] = data[k]; }
                }
            }
            fData.origin = data;
            return fData;
        }

        var postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }

        var subscriptionFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (subscriptionFiels[k]) {
                    fData[subscriptionFiels[k]] = data[k];
                }
            }
            return fData;
        }

        var originFormat = function (data) {
            if (from === 'post') {
                return postFormat(data);
            }
            return subscriptionFormat(data);
        }

        var setReadingTime = function () {
            $('article.blog-wrapper').readingTime({
                wordCountTarget: '.words',
                wordsPerMinute: 200
            });
        }

        var StickyHeaderClass = function () {
            var domStickyHeader = domRoot.find('.sticky-header');
            var domCompleteMark = domStickyHeader.find('.complete-wrapper .toogle-complete');
            var appearStatus = false;
            var lastTop = 0;
            var delay = 100, isAnimating = false;
            var timeOut = false;
            var self = this;

            var embedHandle;

            var completePost = function () {
                if (from === 'subscription'){
                    var ajaxData = {};
                    ajaxData.action = 'complete_subscription';
                    ajaxData.id = postData.id;
                    return ajaxAPiHandle.pagePost('view', ajaxData).then(function (res) {
                        if (res.status){
                            postData.completed = 1;
                            domCompleteMark.addClass('btn-success');
                            return res;
                        }
                    });
                }
                else {

                }
            }

            var unCompletePost = function () {
                if (from === 'subscription'){
                    var ajaxData = {};
                    ajaxData.action = 'unComplete_subscription';
                    ajaxData.id = postData.id;
                    return ajaxAPiHandle.pagePost('view', ajaxData).then(function (res) {
                        if (res.status){
                            postData.completed = 0;
                            domCompleteMark.removeClass('btn-success');
                            return res;
                        }
                    });
                }
                else {

                }
            }

            var bindEvents = function () {
                domStickyHeader.find('.toogle-complete').click(function () {
                    (from === 'subscription' && postData.completed > 0) ? unCompletePost() : completePost();
                });
                $(window).scroll(function () {
                    var scrollTop = $(this).get(0).scrollY;
                    var dir = scrollTop - lastTop;

                    lastTop = scrollTop;
                    if (scrollTop > 60){
                        if (!isAnimating){
                            if (dir < 0){
                                if (appearStatus){
                                    clearTimeout(timeOut);
                                }
                                else {
                                    self.appear();
                                }
                                timeOut = setTimeout(function () {
                                    self.disappear();
                                }, 5000);
                            }
                            else if (dir > 0){
                                if (appearStatus){
                                    clearTimeout(timeOut);
                                    self.disappear();
                                }
                                else {
                                }
                            }
                        }
                    }
                    else {
                        domStickyHeader.removeClass('disappear');
                        domStickyHeader.removeClass('appear');
                        domArticle.find('.blog-header').removeClass('disappear');
                        domArticle.find('.blog-header').removeClass('appear');
                        appearStatus = true;
                        isAnimating = false;
                    }
                });
                domStickyHeader.on(animEndEventName, function () {
                    isAnimating = false;
                });
            }

            this.bindData = function () {
                domStickyHeader.find('.sticky-series-title').html(series.strSeries_title);
                domStickyHeader.find('.sticky-post-title').html('--- ' + postData.title + ' ---');
                if (purchased && postData.completed){
                    domCompleteMark.addClass('btn-success');
                }
                else {
                    domCompleteMark.removeClass('btn-success');
                }
                domStickyHeader.find('[href ^= "https://www.facebook.com/sharer"]').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + postData.origin.meta.viewUri + '&title=' + postData.origin.meta.uriTitle);
                domStickyHeader.find('[href ^= "https://twitter.com/intent"]').attr('href', 'https://twitter.com/intent/tweet?url=' + postData.origin.meta.viewUri);
                domStickyHeader.find('[href ^= "https://www.linkedin.com/shareArticle"]').attr('href', 'https://www.linkedin.com/shareArticle?mini=true&url=' + postData.origin.meta.viewUri + '&title=' + postData.origin.meta.uriTitle);
                domStickyHeader.find('[href ^= "https://plus.google.com/share"]').attr('href', 'https://plus.google.com/share?url=' + postData.origin.meta.viewUri);
                domStickyHeader.find('[href ^= "mailto:"]').attr('href', 'mailto:?&subject=' + 'Shared from Walden.ly - ' + postData.origin.meta.uriTitle + '&body=' + encodeURIComponent('I thought you might like this: ') + postData.origin.meta.viewUri);
                if (window.scrollY <= 60){
                    domStickyHeader.removeClass('disappear');
                    domStickyHeader.removeClass('appear');
                    domArticle.find('.blog-header').removeClass('disappear');
                    domArticle.find('.blog-header').removeClass('appear');
                    appearStatus = true;
                    isAnimating = false;
                }
                else {
                }
                embedHandle.bindData(postData);
            }
            this.appear = function () {
                appearStatus = true;
                domStickyHeader.removeClass('disappear');
                domStickyHeader.addClass('appear');
                domArticle.find('.blog-header').addClass('disappear');
                domArticle.find('.blog-header').removeClass('appear');
                isAnimating = true;
            }
            this.disappear = function () {
                appearStatus = false;
                domStickyHeader.removeClass('appear');
                domStickyHeader.addClass('disappear');
                domArticle.find('.blog-header').removeClass('disappear');
                domArticle.find('.blog-header').addClass('appear');
                isAnimating = true;
            }
            this.init = function () {

                if (from === 'post') {
                    domStickyHeader.find('.complete-wrapper').css('opacity', 0);
                }

                domStickyHeader.find('.circle-nav-wrapper .circle-nav-toggle').circleNav(domStickyHeader.find('.circle-nav-wrapper'), {isSelf: true});
                if (series.strSeries_title.length > 100 || postData.title.length > 100){
                    domStickyHeader.addClass('long-text');
                }
                lastTop = window.scrollY;
                if (window.scrollY > 60){
                    self.appear();
                }
                bindEvents();
                embedHandle = new PostCopyEmbedClass($('.component.post-copy-embed'), postData);
                embedHandle.init();
            }
        }

        var StickyFooterClass = function () {

            var domStickyFooter = domRoot.find('.sticky-footer');

            var self = this;

            var appearStatus = true, timeOut = false, isAnimating = false, isClickedAnimation = false;
            var speakerHandle;

            var lastTop = 0;

            var bindEvents = function () {
                domStickyFooter.find('.reduce-fontsize').click(function () {
                    var preFontSize = $('html').css('font-size');
                    var newFontSize = parseInt(preFontSize) - fontSizeChangeUnit;
                    newFontSize = newFontSize > 10 ? newFontSize : 10;
                    $('html').css('font-size', newFontSize + 'px');
                });
                domStickyFooter.find('.increase-fontsize').click(function () {
                    var preFontSize = $('html').css('font-size');
                    var newFontSize = parseInt(preFontSize) + fontSizeChangeUnit;
                    newFontSize = newFontSize < 40 ? newFontSize : 40;
                    $('html').css('font-size', newFontSize + 'px');
                });
                domStickyFooter.find('.default-fontsize').click(function () {
                    $('html').css('font-size', defaultFontSize);
                });
                domStickyFooter.on(animEndEventName, function () {
                    isAnimating = false;
                    isClickedAnimation = false;
                });
                $(window).scroll(function () {
                    var scrollTop = $(this).get(0).scrollY;

                    var dir = scrollTop - lastTop;
                    lastTop = scrollTop;
                    if (isAnimating){
                    }
                    else if (!isClickedAnimation){
                        if (dir > 0){
                            if (appearStatus){
                                clearTimeout(timeOut);
                            }
                            else {
                                self.appear();
                            }
                            timeOut = setTimeout(function () {
                                self.disappear();
                            }, 5000);
                        }
                        else if (dir < 0){
                            if (appearStatus){
                                clearTimeout(timeOut);
                                self.disappear();
                            }
                            else {
                            }
                        }
                    }
                });
                $(window).click(function (e) {
                    if (window.innerHeight - e.clientY < 100 && !isAnimating){
                        isClickedAnimation = true;
                        setTimeout(function () {
                            isClickedAnimation = false;
                        }, 100);
                        if (appearStatus){
                            clearTimeout(timeOut);
                        }
                        else {
                            self.appear();
                        }
                        timeOut = setTimeout(function () {
                            self.disappear();
                        }, 5000);
                    }
                })
            }

            var SpeakerClass = function () {

                var domSpeaker = domStickyFooter.find('.speaker-wrapper');

                var setupLanguage = function () {

                    if ('speechSynthesis' in window) {
                        // window.speechSynthesis.onvoiceschanged = function (ev) {
                        var voiceList = domSpeaker.find('[name="speaker_lang"]');
                        if(voiceList.find('option').length == 0) {
                            speechSynthesis.getVoices().forEach(function(voice, index) {
                                var $option = $('<option>')
                                    .val(index)
                                    .html(voice.name + (voice.default ? ' (default)' :''));
                                voiceList.append($option);
                            });
                            voiceList.material_select();
                        }
                        // }
                    }
                }

                var blockTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'div', 'p'];
                var msg = new SpeechSynthesisUtterance();
                var cleanText = function (dirtyText) {

                    blockTags.forEach(function (blockTag) {
                        dirtyText = dirtyText.replace('</' + blockTag + '>', '\n</' + blockTag + '>');
                    });
                    return $('<div>' + dirtyText + '</div>').text();
                }

                var speakText = function (text) {
                    setTimeout(function () {
                        speechSynthesis.cancel();
                        var voices = window.speechSynthesis.getVoices();
                        msg.voice = voices[domSpeaker.find('[name="speaker_lang"]').val()];
                        msg.rate = domSpeaker.find('[name="speaker_rate"]').val() / 10;
                        msg.pitch = domSpeaker.find('[name="speaker_pitch"]').val();
                        msg.text = text;
                        msg.volume = 1;
                        window.speechSynthesis.speak(msg);
                    }, 10);
                }

                var makePostText = function () {
                    var text = series.strSeries_title + '\n\n\n\n';
                    text += postData.title + '\n\n';
                    text += postData.body;
                    return cleanText(text);
                }

                var bindEvents = function () {
                    domSpeaker.find('.play-speaker').click(function () {
                        if (speechSynthesis.pending){
                            return false;
                        }

                        if (speechSynthesis.paused){
                            window.speechSynthesis.resume();
                        }
                        else {
                            var text = makePostText();
                            speakText(text);
                        }
                    });



                    var pendingFunc = function () {
                        domSpeaker.find('.pause-speaker').click(function () {
                            if (window.speechSynthesis.pending || window.speechSynthesis.paused){
                                return false;
                            }

                            window.speechSynthesis.pause();
                        });
                        return true;
                    }();

                    msg.onstart = function(event) {
                        console.log('stat', event);
                        domSpeaker.removeClass('pause');
                        domSpeaker.addClass('speaking');
                    };
                    msg.onpause = function (ev) {
                        console.log('paused', ev);
                        domSpeaker.removeClass('speaking');
                        domSpeaker.addClass('pause');
                    }
                    msg.onresume = function (ev) {
                        console.log('resume', ev);
                        domSpeaker.removeClass('pause');
                        domSpeaker.addClass('speaking');

                    }
                    msg.onend = function(event) {
                        console.log('end', event);
                        domSpeaker.removeClass('pause');
                        domSpeaker.removeClass('speaking');
                    };

                };

                this.init = function () {
                    setTimeout(function () {
                        setupLanguage();
                    }, 100);
                    window.speechSynthesis.cancel();
                    setTimeout(function () {
                        var voices = window.speechSynthesis.getVoices();
                        msg.voice = voices[0];
                        msg.volume = 0;
                        msg.text = '';
                        window.speechSynthesis.speak(msg);
                    }, 100);

                    bindEvents();
                }
            }

            this.appear = function () {
                if (appearStatus){
                    clearTimeout(timeOut);
                    timeOut = setTimeout(function () {
                        self.disappear();
                    }, 5000);
                }
                else {
                    appearStatus = true;
                    domStickyFooter.removeClass('disappear');
                    domStickyFooter.addClass('appear');
                    isAnimating = true;
                }
            }
            this.disappear = function () {
                appearStatus = false;
                domStickyFooter.removeClass('appear');
                domStickyFooter.addClass('disappear');
                isAnimating = true;
            }
            this.init = function () {
                $(document).ready(function () {
                    speakerHandle = new SpeakerClass();
                    speakerHandle.init();
                });
                lastTop = window.scrollY;
                bindEvents();
            }
        }

        var PathClass = function (path) {
            var domPath, pathNumber;

            var bindEvents = function () {
                domPath.click(function () {
                    select();
                });
            }

            var select = function () {
                var ajaxData = {};

                if (from === 'post') {
                    ajaxData.action = 'set_path';
                    ajaxData.pathId = path.id;
                    ajaxData.seriesId = series.series_ID;
                }
                else {
                    ajaxData.action = 'set_subPath';
                    ajaxData.pathId = path.id;
                    ajaxData.purchasedId = purchased.purchased_ID;
                }
                ajaxAPiHandle.pagePost('view', ajaxData).then(function (res) {
                    if (res.status === true){
                        self.bindData(viewFormat(res.data));
                    }
                    else {
                        swal("Sorry!", "There is no post in this option that you clicked. You will go to next post.").then(function () {
                            nextPost();
                        });
                    }
                });
            }

            this.init = function () {

                domPath = domUsedBody.find('.path-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUsedBody.find('.paths-list'));

                pathNumber = domUsedBody.find('.paths-list > .path-col').length;

                domPath.find('.img-wrapper img').attr('src', 'assets/images/global-icons/tree/path/'+ pathNumber +'.svg');


                var displayTitle = path.title;
                displayTitle = displayTitle.length > 20 ? displayTitle.substr(0, 50) + '...' : displayTitle.substr(0, 50);

                var displayDes = path.body;
                displayDes = displayDes.length > 20 ? displayDes.substr(0, 100) + '...' : displayDes.substr(0, 100);

                domPath.find('.path-title').html(displayTitle);
                domPath.find('.path-description').html(displayDes);
                bindEvents();
            }
        }

        var removePluginsEvents = function () {
            $('svg').mousedown(function (e) {
                return false;
            });
        }

        var makeBody = function () {

            var type = parseInt(postData.type);
            type = availableTypes.indexOf(type) != -1 ? type : 'other';
            var newDomUsedBody;
            if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                payBlogHandle = new PayBlogClass(domUsedBody, series, stripeCardModalHandle);
                payBlogHandle.onPaid(function (purchasedId) {
                    purchased = purchasedId;
                    makeBody();
                });
                removePluginsEvents();
                payBlogHandle.init();
            }
            else if (postData.nodeType === 'menu') {
                newDomUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                domUsedBody = newDomUsedBody;
                domUsedBody.find('.menu-title').html(postData.title);
                domUsedBody.find('.menu-description').html(postData.body);

                postData.paths.forEach(function (path) {
                    var pathHandle = new PathClass(viewFormat(path));
                    pathHandle.init();
                });
                domUsedBody.find('.paths-list').addClass('paths-count-' + postData.paths.length);
            }
            else {
                newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                switch (parseInt(type)){
                    case 0:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                        domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                        domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                        var settings_ap = {
                            disable_volume: 'off',
                            disable_scrub: 'default',
                            design_skin: 'skin-wave',
                            skinwave_dynamicwaves: 'off'
                        };
                        dzsag_init('#audio-' + audioCnt, {
                            'transition':'fade',
                            'autoplay' : 'off',
                            'settings_ap': settings_ap
                        });
                        audioCnt++;
                        break;
                    case 2:
                        if (videoHandle){

                        }
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domVideo = domUsedBody.find('video');
                        if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                            var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                            domVideo.attr('data-setup', JSON.stringify(setup));
                        }
                        else {
                            domVideo.append('<source src="'+ postData.body +'" />');
                        }
                        videoHandle = videojs(domVideo.get(0));
                        break;
                    case 8:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.append(postData.body);
                        setReadingTime();
                        break;
                    case 10:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        domUsedBody.append(postData.body);
                        domUsedBody.find('[data-form-field]').each(function (i, d) {
                            var hdl = new FormFieldClass($(d));
                            hdl.init();
                        });
                        domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                            var field = false;
                            var answer = false;
                            formFields.forEach(function (formField) {
                                if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                    field = formField;
                                }
                            });
                            formFieldAnswers.forEach(function (formFieldAnswer) {
                                if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                    answer = formFieldAnswer;
                                }
                            });
                            if (field) {
                                if (answer) {
                                    field.answer = answer.strFormFieldAnswer_answer;
                                }
                                else{
                                    field.answer = false;
                                }
                            }
                            var hdl = new AnsweredFormFieldClass($(d), field);
                            hdl.init();
                        });
                        setReadingTime();
                        break;
                    default:
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        // domUsedBody.find('.img-wrapper').append('<img src="'+ subscription.strClientSubscription_image +'">');
                        domUsedBody.append(postData.body);
                        domUsedBody.find('[data-form-field]').each(function (i, d) {
                            var hdl = new FormFieldClass($(d));
                            hdl.init();
                        });
                        domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                            var field = false;
                            var answer = false;
                            formFields.forEach(function (formField) {
                                if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                    field = formField;
                                }
                            });
                            formFieldAnswers.forEach(function (formFieldAnswer) {
                                if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                    answer = formFieldAnswer;
                                }
                            });
                            if (field) {
                                if (answer) {
                                    field.answer = answer.strFormFieldAnswer_answer;
                                }
                                else{
                                    field.answer = false;
                                }
                            }
                            var hdl = new AnsweredFormFieldClass($(d), field);
                            hdl.init();
                        });
                        setReadingTime();
                        break;
                }
            }
        }
        var nextPost = function () {
            var ajaxData;

            if (from === 'post') {
                ajaxData = {
                    action: 'next_post',
                    id: postData.id
                };

            }
            else {
                ajaxData = {
                    action: 'next_sub',
                    id: postData.id
                };
            }

            return ajaxAPiHandle.pagePost('view', ajaxData).then(function (res) {
                if (res.status){
                    self.bindData(viewFormat(res.data));
                }
            });
        }
        var prevPost = function () {

            var ajaxData;

            if (from === 'post') {
                ajaxData = {
                    action: 'prev_post',
                    id: postData.id
                };
            }
            else {
                ajaxData = {
                    action: 'prev_sub',
                    id: postData.id
                };
            }

            return ajaxAPiHandle.pagePost('view', ajaxData).then(function (res) {
                if (res.status){
                    self.bindData(viewFormat(res.data));
                }
            });
        }
        var bindEvents = function () {
            domRoot.find('.prev-post').click(function () {
                prevPost();
            });
            domRoot.find('.next-post').click(function () {
                nextPost();
            });
        }
        this.bindData = function (newData) {

            $('head title').html(postData.title);
            $('meta[name="Title"]').attr('content', postData.title);
            $('meta[name="Description"]').attr('content', postData.origin.meta.metaDes);
            $('meta[property="og:title"]').attr('content', postData.title);
            $('meta[property="og:image"]').attr('content', postData.image);
            $('meta[property="og:description"]').attr('content', postData.origin.meta.metaDes);

            postData = $.extend({}, postData, newData);

            for( var i = 0; i < 10; i ++ ){
                domRoot.find('.site-content').removeClass('post-type-' + i);
            }
            domRoot.find('.site-content').addClass('post-type-' + postData.type);
            domArticle.find('.series-title').html(series.strSeries_title);
            if (postData.nodeType === 'menu') {
                $('.site-wrapper .decoration-container .right-portion').addClass('disabled');
                domArticle.find('.post-title').html('Please choose from the following selections:');
            }
            else {
                $('.site-wrapper .decoration-container .right-portion').removeClass('disabled');
                domArticle.find('.post-title').html(postData.title);
            }
            makeBody();
            stickyHeaderHandle.bindData();
            stickyFooterHandle.appear();
        }
        this.init = function () {
            domRoot = $('.site-wrapper.page');
            domArticle = domRoot.find('article.blog-wrapper');
            domBody = domArticle.find('.blog-body');
            removePluginsEvents();
            if (series.boolSeries_charge == 1 && !purchased) {
                stripeCardModalHandle = new StripeCardModalClass(domRoot.find('.stripe-card-modal'), stripeApi_pKey);
                stripeCardModalHandle.init();
                stripeCardModalHandle.setSeries(series);
            }
            postData = viewFormat(initialPostData);

            stickyFooterHandle = new StickyFooterClass();
            stickyFooterHandle.init();
            stickyHeaderHandle = new StickyHeaderClass();
            stickyHeaderHandle.init();
            self.bindData(postData);

            bindEvents();
        }
    }

    viewHandle = new ViewClass();
    viewHandle.init();

    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });
})();