(function () {

    'use strict'

    $ = jQuery;
    var is_loading_controlled_in_local = false;
    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }
    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('-');
    };
    String.prototype.stringToDate = function () {
        var yyyy = this.split('-')[0];
        var mm = this.split('-')[1] * 1 - 1;
        var dd = this.split('-')[2];
        return new Date(yyyy, mm, dd);
    }
    function mobilecheck() {
        var check = false;
        (function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }

    var animEndEventNames = {
            'WebkitAnimation' : 'webkitAnimationEnd',
            'OAnimation' : 'oAnimationEnd',
            'msAnimation' : 'MSAnimationEnd',
            'animation' : 'animationend'
        },
        // animation end event name
        animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
        // event type (if mobile use touch events)
        eventtype = mobilecheck() ? 'touchstart' : 'click',
        // support for css animations
        support = Modernizr.cssanimations;

    function onAnimationEnd( elems, len, callback ) {
        var re_len = 0;
        elems.forEach(function (el) {
            if (el.hasChildNodes()){
                re_len++;
            }
        });
        if (re_len === 0){
            callback.call();
        }
        var finished = 0,
            onEndFn = function() {
                this.removeEventListener( animEndEventName, onEndFn );
                ++finished;
                if( finished === re_len ) {
                    callback.call();
                }
            };
        elems.forEach( function( el,i ) {
            if (el.querySelector('a') !== null){
                el.querySelector('a').addEventListener( animEndEventName, onEndFn );
            }
        });
    }

    var goalsHandle = null;

    var goalsClass = function (goals_data) {
        var dom_goals_container = $('main.main-content ul.timeline');
        var show_whats = {event: true, goal: true, ideabox: true};
        var selectHandle = null;
        var formatDate = function (date_obj) {
            var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nobember', 'December'];
            return month_name[date_obj.getMonth()] + ' ' + date_obj.getDate() + ', ' + date_obj.getFullYear()
        }
        var itemRowHandles = [];
        var itemRowClass = function (row_data, origin_type) {
            var dom_item_root = dom_goals_container.find('.timeline-item.sample').clone().removeClass('sample').removeAttr('hidden');
            var dom_img = null;

            var title, description, img, deadline, completed;
            var now_date = new Date().yyyymmdd().stringToDate();

            var editEventInstanceLightboxHandle = null;
            var applyChangeToInstance = function (new_data) {
                if (new_data.event_title !== null){
                    title = row_data.event_title = new_data.event_title;
                    dom_item_root.find('.timeline-title').html(new_data.event_title);
                }
                if (new_data.event_description !== null){
                    description = row_data.event_description = new_data.event_description != null ? new_data.event_description : row_data.event_description;
                    dom_item_root.find('.timeline-description').html(removeTags(description));

                    img = row_data.event_images.length > 0 ? row_data.event_images[0].media_url : DEFAULT_EVENT_IMAGE;
                    dom_img = $($.parseHTML('<img class="timeline-img" src="'+ img +'">'));
                    dom_img.prependTo(dom_item_root.find('.timeline-description'));
                }
                if (new_data.start_date !== null){
                    row_data.start_date = new_data.start_date != null ? new_data.start_date : row_data.start_date;
                    deadline = row_data.start_date.stringToDate();
                    dom_item_root.find('.timeline-info span').html(formatDate(deadline));
                }
            }
            var editEventInstanceLightboxClass = function () {
                var dom_lightbox_root = $('.edit-event-lightbox.sample').clone().removeClass('sample');
                var dom_lightbox_inner_wrapper = $('.edit-event-lightbox-inner-wrapper', dom_lightbox_root);
                var dom_lightbox_inner = $('.edit-event-lightbox-inner', dom_lightbox_inner_wrapper);
                var dom_close_btn = $('.close', dom_lightbox_inner_wrapper);
                var dom_open_from = dom_item_root;
                var dom_title_wrapper = $('.event-title-wrapper', dom_lightbox_inner);
                var dom_description_wrapper = $('.description-wrapper', dom_lightbox_inner);
                var dom_location_wrapper = $('.location-wrapper', dom_lightbox_inner);
                var dom_date_time_wrapper = $('.date-time-wrapper', dom_lightbox_inner);
                var dom_main_image = $('.event-main-image', dom_lightbox_inner);

                var event_instance_title = row_data.event_title;
                var event_instance_description = row_data.event_description;
                var event_instance_location = row_data.event_location;
                var event_instance_start_date = row_data.start_date;
                var event_instance_start_time = row_data.start_time;

                var eventImagesHandle = null;

                var startDateInstance = dom_date_time_wrapper.find('input.start-date').val(row_data.start_date).datepicker({
                    format: 'yyyy-mm-dd',
                    container: dom_lightbox_inner.find('.the-details')
                });
                var startTimeInstance = dom_date_time_wrapper.find('input.start-time').val(row_data.start_time).timepicker({
                    showMeridian: false
                });

                var eventIamgesClass = function () {

                    var dom_event_images_root = $('.event-images-container', dom_lightbox_inner);
                    var dom_grid_wrapper = $('.tt-grid-wrapper', dom_event_images_root);
                    var dom_list_wrapper = $('ul.tt-grid', dom_event_images_root);
                    var dom_previous = $('.previous-page', dom_event_images_root);
                    var dom_next = $('.next-page', dom_event_images_root);

                    var count_per_page = 5;

                    var pages = [];
                    var current_page_index = 0;
                    var pages_size = 0;
                    var keys_of_id = [];

                    row_data.event_images.forEach(function (post, i) {
                        keys_of_id[post.id] = i;
                        var page_index = parseInt(i / count_per_page);
                        post.html = '<a>' +
                            '<div class="event-image-item">' +
                            '<img class="delete-event-image-icon" data-id="' + post.id + '" src="assets/images/global-icons/close.png" title="delete this image"/>' +
                            '<img class="event-image" src="'+ post.media_url +'" />' +
                            '</div>' +
                            '</a>';

                        if (typeof pages[page_index] == 'undefined'){
                            pages[page_index] = [];
                        }
                        pages[page_index].push(post);
                        pages_size = page_index + 1;
                    });

                    var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                        items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                        isAnimating = false;


                    var removeIamgeFromEventInFront = function (id) {
                        row_data.event_images.splice(keys_of_id[id], 1);
                        flipInitInstancesOfEvent(row_data.event_id);
                    }
                    var removeIamgeFromEvent = function (id) {
                        swal({
                            // title: 'Are you sure?',
                            text: "Are you sure you want to delete this image?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, cancel it!',
                            cancelButtonText: 'No, cancel!',
                            confirmButtonClass: 'btn btn-success',
                            cancelButtonClass: 'btn btn-danger',
                            buttonsStyling: false
                        }).then(function () {
                            var ajax_data = {
                                action: 'delete_event_image',
                                id: id
                            };
                            $.ajax({
                                url: USER_ACTION_URL,
                                data: ajax_data,
                                success: function (res) {
                                    if (res.status == true){
                                        removeIamgeFromEventInFront(id);
                                    }
                                    else {
                                        alert('something went wrong');
                                    }
                                },
                                type: 'post',
                                dataType: 'json'
                            })

                        }, function (dismiss) {
                            // dismiss can be 'cancel', 'overlay',
                            // 'close', and 'timer'
                        });
                    }

                    var addFuncToEl = function (el) {
                        el .find(':not(.tt-old)').find('.delete-event-image-icon').click(function () {
                            removeIamgeFromEvent($(this).data('id'));
                        })
                    }

                    // this is just a way we can test this. You would probably get your images with an AJAX request...

                    function loadNewSet(action, set ) {
                        if (isAnimating === true){
                            return ;
                        }
                        isAnimating = true;

                        var newImages = pages[current_page_index];
                        if (action === 'pagination'){
                            newImages = pages[set];
                            current_page_index = set;
                        }
                        items.forEach( function( el ) {
                            var itemChild = el.querySelector( 'a' );
                            // add class "tt-old" to the elements/images that are going to get removed
                            if( itemChild ) {
                                classie.add( itemChild, 'tt-old' );
                            }
                        } );

                        // apply effect
                        setTimeout( function() {

                            // append new elements
                            if(newImages){

                                [].forEach.call( newImages, function( el, i ) {
                                    items[ i ].innerHTML += el.html;
                                } );
                            }

                            // add "effect" class to the grid
                            classie.add( grid, 'tt-effect-active' );


                            // wait that animations end
                            var onEndAnimFn = function() {
                                // remove old elements
                                items.forEach( function( el ) {
                                    // remove old elems
                                    var old = el.querySelector( '.tt-old' );
                                    if( old ) { el.removeChild( old ); }
                                    // remove class "tt-empty" from the empty items
                                    classie.remove( el, 'tt-empty' );
                                    // now apply that same class to the items that got no children (special case)
                                    if ( !el.hasChildNodes() ) {
                                        classie.add( el, 'tt-empty' );
                                    }
                                    addFuncToEl($(el));
                                } );
                                // remove the "effect" class
                                classie.remove( grid, 'tt-effect-active' );
                                isAnimating = false;
                            };

                            if( support ) {
                                onAnimationEnd( items, items.length, onEndAnimFn );
                            }
                            else {
                                onEndAnimFn.call();
                            }
                        }, 25 );
                    }

                    var previous = function () {
                        if (current_page_index > 0 && !isAnimating){
                            loadNewSet('pagination', current_page_index - 1);
                        }
                    }
                    var next = function () {
                        if (current_page_index < pages_size - 1 && !isAnimating){
                            loadNewSet('pagination', current_page_index + 1);
                        }
                    }

                    var flipInit = function() {
                        pages = [];
                        keys_of_id = [];
                        pages_size = 0;
                        row_data.event_images.forEach(function (post, i) {
                            keys_of_id[post.id] = i;
                            var page_index = parseInt(i / count_per_page);
                            if (typeof pages[page_index] == 'undefined'){
                                pages[page_index] = [];
                            }
                            pages[page_index].push(post);
                            pages_size = page_index + 1;
                        });
                        isAnimating = false;
                        loadNewSet();
                        if (pages_size > 0){
                            dom_main_image.attr('src', row_data.event_images[0].media_url);
                        }
                        else {
                            dom_main_image.attr('src', DEFAULT_EVENT_IMAGE);
                        }
                    }

                    var accept_image_type = ['image/png', 'image/jpeg'];
                    var fileType = function (dataTransfer) {

                        var dropped_url = dataTransfer.getData('URL');

                        if(dropped_url != ''){
                            return dropped_url;
                        }

                        if (dataTransfer.files.length < 1){
                            return false;
                        }
                        var type = dataTransfer.files[0].type;

                        if (accept_image_type.indexOf(type) > -1){
                            return 'image';
                        }else {
                            return false;
                        }
                    };

                    var addImageToEvent = function (event_image_data) {
                        event_image_data.html = '<a>' +
                            '<div class="event-image-item">' +
                            '<img class="delete-event-image-icon" data-id="' + event_image_data.id + '" src="assets/images/global-icons/close.png" title="delete this image"/>' +
                            '<img class="event-image" src="'+ event_image_data.media_url +'" />' +
                            '</div>' +
                            '</a>';
                        keys_of_id[event_image_data.id] = row_data.event_images.length;
                        row_data.event_images.push(event_image_data);
                        if (pages_size > 0){
                            if (pages[pages_size - 1].length == count_per_page){
                                pages[pages_size++] = [event_image_data];
                            }
                            else {
                                pages[pages_size - 1].push(event_image_data);
                            }
                        }
                        else {
                            pages[pages_size++] = [event_image_data];
                        }
                    }

                    dom_event_images_root.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                    })
                        .on('dragover dragenter', function() {
                            dom_event_images_root.addClass('is-dragover');
                        })
                        .on('dragleave dragend drop', function() {
                            dom_event_images_root.removeClass('is-dragover');
                        })
                        .on('drop', function(e) {
                            var file_type = fileType(e.originalEvent.dataTransfer);

                            if (file_type === 'image'){
                                dom_event_images_root.removeClass('is-dragover');
                                var ajaxData = new FormData();
                                var files = e.originalEvent.dataTransfer.files;
                                ajaxData.append('event_image', files[0]);
                                ajaxData.append('event_id', row_data.event_id);
                                ajaxData.append('action', 'add_image_to_event');

                                $.ajax({
                                    url: USER_ACTION_URL,
                                    type: 'post',
                                    data: ajaxData,
                                    dataType: 'json',
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    complete: function() {
                                    },
                                    success: function(res) {

                                        if (res.status === true){
                                            addImageToEvent(res.data);
                                            flipInitInstancesOfEvent(row_data.event_id);
                                        }
                                        else {
                                            alert('sorry something is wrong');
                                        }
                                    },
                                    error: function() {
                                        alert('sorry something is wrong');
                                        // Log the error, show an alert, whatever works for you
                                    }
                                });

                            }
                            else {
                                alert('this is unsupported format');
                            }
                        });
                    this.init = function () {
                        flipInit();
                        dom_previous.click(function () {
                            previous();
                        });
                        dom_next.click(function () {
                            next();
                        });
                    };
                    this.flipInit = flipInit;
                    this.loadNewSet = loadNewSet;
                    this.addImageToEvent = addImageToEvent;
                }

                eventImagesHandle = new eventIamgesClass();

                var bindEvent = function () {
                    dom_close_btn.click(function () {
                        closeLightBox();
                    });
                    dom_title_wrapper.find('.edit-entry').click(function () {
                        dom_title_wrapper.addClass('edit-status');
                        dom_title_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                    });
                    dom_title_wrapper.find('.save-entry').click(function () {
                        event_instance_title = dom_title_wrapper.find('.editable-content').html();
                        row_data.event_title = event_instance_title;
                        dom_title_wrapper.removeClass('edit-status');
                        dom_title_wrapper.find('.editable-content').removeAttr('contenteditable');
                    });
                    dom_title_wrapper.find('.back-to-origin').click(function () {
                        dom_title_wrapper.removeClass('edit-status');
                        dom_title_wrapper.find('.editable-content').html(event_instance_title);
                        dom_title_wrapper.find('.editable-content').removeAttr('contenteditable');
                    });
                    dom_description_wrapper.find('.edit-entry').click(function () {
                        dom_description_wrapper.addClass('edit-status');
                        dom_description_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                    });
                    dom_description_wrapper.find('.save-entry').click(function () {
                        event_instance_description = dom_description_wrapper.find('.editable-content').html();
                        row_data.event_description = event_instance_description;
                        dom_description_wrapper.removeClass('edit-status');
                        dom_description_wrapper.find('.editable-content').removeAttr('contenteditable');
                    });
                    dom_description_wrapper.find('.back-to-origin').click(function () {
                        dom_description_wrapper.removeClass('edit-status');
                        dom_description_wrapper.find('.editable-content').html(event_instance_description);
                        dom_description_wrapper.find('.editable-content').removeAttr('contenteditable');
                    });
                    dom_location_wrapper.find('.edit-entry').click(function () {
                        dom_location_wrapper.addClass('edit-status');
                        dom_location_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                    });
                    dom_location_wrapper.find('.save-entry').click(function () {
                        event_instance_location = dom_location_wrapper.find('.editable-content').html();
                        row_data.event_location = event_instance_location;
                        dom_location_wrapper.removeClass('edit-status');
                        dom_location_wrapper.find('.editable-content').removeAttr('contenteditable');
                    });
                    dom_location_wrapper.find('.back-to-origin').click(function () {
                        dom_location_wrapper.removeClass('edit-status');
                        dom_location_wrapper.find('.editable-content').html(event_instance_location);
                        dom_location_wrapper.find('.editable-content').removeAttr('contenteditable');
                    });
                    dom_date_time_wrapper.find('.edit-entry').click(function () {
                        dom_date_time_wrapper.addClass('edit-status');
                    });
                    dom_date_time_wrapper.find('.save-entry').click(function () {
                        dom_date_time_wrapper.removeClass('edit-status');
                        dom_date_time_wrapper.find('.for-display').find('.start-date').html(event_instance_start_date);
                        dom_date_time_wrapper.find('.for-display').find('.start-time').html(event_instance_start_time);
                    });
                    dom_date_time_wrapper.find('.back-to-origin').click(function () {
                        dom_date_time_wrapper.removeClass('edit-status');
                    });
                    startDateInstance.on('changeDate', function (e) {
                        event_instance_start_date = startDateInstance.val();
                    });
                    startTimeInstance.on('changeTime.timepicker', function (e) {
                        event_instance_start_time = startTimeInstance.val() + ':00';
                    });
                    dom_lightbox_inner.find('.apply-to-instance').click(function () {
                        var ajax_data = {
                            data: {
                                event_id: row_data.event_id,
                                event_title: event_instance_title,
                                event_description: event_instance_description,
                                event_location: event_instance_location,
                                start_date: event_instance_start_date,
                                start_time: event_instance_start_time
                            }
                        }
                        switch (row_data.instance_type){
                            case 'recurring_instance':
                                ajax_data.origin_start_date = row_data.start_date;
                                ajax_data.action = 'reschedule_instance';
                                break;
                            case 'single_instance':
                                ajax_data.action = 'update_event';
                                break;
                            case 'rescheduled_instance':
                                ajax_data.id = row_data.id;
                                ajax_data.action = 'update_reschduled_instance';
                                break;
                        }
                        $.ajax({
                            url: USER_ACTION_URL,
                            data: ajax_data,
                            success: function (res) {
                                if (res.status == true){
                                    if (ajax_data.action == 'reschedule_instance'){
                                        row_data.id = res.data.rescheduled_row.id;
                                        row_data.instance_type = 'rescheduled_instance';
                                    }
                                    applyChangeToInstance(ajax_data.data);
                                }
                                else {
                                    alert('something went wrong!');
                                }
                                closeLightBox();
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    });
                    dom_lightbox_inner.find('.cancel').click(function () {
                        closeLightBox();
                    })
                }

                var openLightbox = function () {

                    dom_lightbox_root.appendTo('body');
                    dom_open_from.addClass('open-lightbox-from-state');
                    dom_lightbox_inner.css('display', 'none');
                    var window_width = $(window).width();
                    setTimeout(function () {
                        dom_lightbox_root.css('display', 'block');
                        var pos = dom_open_from.offset();
                        pos.top = pos.top - $(window).scrollTop();
                        var width = dom_open_from.outerWidth();
                        var height = dom_open_from.outerHeight();
                        pos.right = $(window).width() - pos.left - width;
                        pos.bottom = $(window).height() - pos.top - height;
                        dom_lightbox_inner_wrapper.css(pos);

                        var to_css = {
                            top: '5vh',
                            right: '10vw',
                            left: '10vw',
                            bottom: '5vh',
                            fontSize: '10px',
                            padding: '50px'
                        };
                        if (window_width > 768 && window_width < 910){
                            to_css = {
                                top: '2vh',
                                right: '4vw',
                                left: '4vw',
                                bottom: '2vh',
                                fontSize: '10px',
                                padding: '20px'
                            }
                        }
                        else if (window_width < 768){
                            to_css = {
                                top: '1vh',
                                right: '2vw',
                                left: '2vw',
                                bottom: '1vh',
                                fontSize: '10px',
                                padding: '20px'
                            }
                        }

                        dom_lightbox_inner_wrapper.animate(to_css, {
                            duration: 500,
                            complete: function () {
                                dom_lightbox_inner_wrapper.css('padding', 'auto');
                                dom_lightbox_inner.css('display', 'block');
                                dom_lightbox_root.addClass('open-status');
                            }
                        });
                    }, 100);
                }
                var closeLightBox = function () {
                    dom_lightbox_root.removeClass('open-status');
                    dom_lightbox_inner.css('display', 'none');
                    dom_lightbox_inner_wrapper.css('padding', '0px');
                    var pos = dom_open_from.offset();
                    pos.top = pos.top - $(window).scrollTop();
                    var width = dom_open_from.outerWidth();
                    var height = dom_open_from.outerHeight();
                    pos.right = $(window).width() - pos.left - width;
                    pos.bottom = $(window).height() - pos.top - height;
                    dom_lightbox_root.removeClass('open-status');
                    var css = $.extend(pos, {});
                    dom_lightbox_inner_wrapper.animate(
                        css,
                        {
                            duration: 500,
                            start: function () {
                            },
                            complete: function () {
                                dom_lightbox_inner_wrapper.removeAttr('style');
                                dom_lightbox_root.css('display', 'none');
                                dom_open_from.removeClass('open-lightbox-from-state');
                                dom_lightbox_root.detach();
                            }
                        }
                    );
                }

                this.openLightbox = openLightbox;
                this.closeLightBox = closeLightBox;
                this.addImageToEvent = eventImagesHandle.addImageToEvent;
                this.eventImagesHandle = eventImagesHandle;

                this.init = function () {
                    dom_title_wrapper.find('.editable-content').html(event_instance_title);
                    dom_description_wrapper.find('.editable-content').html(event_instance_description);
                    dom_location_wrapper.find('.editable-content').html(event_instance_location);
                    dom_date_time_wrapper.find('.for-display').find('.start-date').html(event_instance_start_date);
                    dom_date_time_wrapper.find('.for-display').find('.start-time').html(event_instance_start_time);
                    eventImagesHandle.init();
                    bindEvent();
                };
            }

            title = description = img = deadline = completed = false;

            switch (origin_type){
                case 'event':
                    title = row_data.event_title;
                    description = row_data.event_description;
                    img = row_data.event_images.length > 0 ? row_data.event_images[0].media_url : DEFAULT_EVENT_IMAGE;
                    deadline = row_data.start_date.stringToDate();
                    completed = deadline.getTime() < now_date.getTime() ? true : false;

                    break;
                case 'ideabox':
                    title = row_data.strIdeaBox_title;
                    description = row_data.strIdeaBox_idea;
                    img = row_data.strIdeaBox_image || DEFAULT_IDEABOX_IMAGE;
                    deadline = row_data.dtIdeaBox_deadline.stringToDate();
                    completed = row_data.boolIdeaBox_done == '1' ? true : false;
                    break;
                case 'goal':
                    title = row_data.strGoal_text;
                    deadline = row_data.dtGoal_datecreated.stringToDate();
                    completed = row_data.boolGoal_complete == '1' ? true : false;
                    break;
            }
            function removeTags(str) {
                if (!str){ return ''; }
                var regex = /(<([^>]+)>)/ig;
                return str.replace(regex, "");
            }
            var bindEvent = function () {
                if (origin_type === 'ideabox'){
                    dom_item_root.find('.timeline-link-wrapper a.timeline-link').click(function () {
                        $($.parseHTML('<form action="idea/index.php" method="POST" target="_blank" hidden>' +
                            '<input type="hidden" name="id" value="' + row_data.ideabox_ID + '">' +
                            '<input name="visual" value="Edit" hidden/><input type="submit" value="send" hidden/></form>')).appendTo($(this)).submit();
                    });
                    dom_item_root.find('.timeline-description img.timeline-img').click(function () {
                        $($.parseHTML('<form action="idea/index.php" method="POST" target="_blank" hidden>' +
                            '<input type="hidden" name="id" value="' + row_data.ideabox_ID + '">' +
                            '<input name="visual" value="Edit" hidden/><input type="submit" value="send" hidden/></form>')).appendTo($(this)).submit();
                    });
                    dom_item_root.find('.timeline-title').click(function () {
                        $($.parseHTML('<form action="idea/index.php" method="POST" target="_blank" hidden>' +
                            '<input type="hidden" name="id" value="' + row_data.ideabox_ID + '">' +
                            '<input name="visual" value="Edit" hidden/><input type="submit" value="send" hidden/></form>')).appendTo($(this)).submit();
                    });
                }
                if (origin_type == 'event'){
                    dom_item_root.find('.timeline-description img.timeline-img').click(function () {
                        editEventInstanceLightboxHandle.openLightbox();
                    });
                    dom_item_root.find('.timeline-title').click(function () {
                        editEventInstanceLightboxHandle.openLightbox();
                    });
                }
            }

            this.getTime = function () {
                return deadline.getTime();
            }
            this.getId = function () {
                switch (origin_type){
                    case 'event':
                        return row_data.event_id;
                        break;
                    case 'ideabox':
                        return row_data.ideabox_ID;
                        break;
                    case 'goal':
                        return row_data.goal_ID;
                        break;
                }
            }
            this.getType = function () {
                return origin_type;
            }
            this.show = function () {
                dom_goals_container.append(dom_item_root);
            };
            this.hide = function () {
                dom_item_root.detach();
            };
            this.init = function () {
                dom_item_root.find('.timeline-info span').html(formatDate(deadline));
                dom_item_root.find('.timeline-title').html(title);
                dom_item_root.find('.timeline-description').html(removeTags(description));
                if (completed){
                    dom_item_root.find('.timeline-marker').addClass('completed');
                }
                if (img){
                    dom_img = $($.parseHTML('<img class="timeline-img" src="'+ img +'">'));
                    dom_img.prependTo(dom_item_root.find('.timeline-description'));
                }
                if (origin_type !== 'ideabox'){
                    dom_item_root.find('.timeline-link-wrapper').remove();
                }
                if (origin_type == 'event'){
                    editEventInstanceLightboxHandle = new editEventInstanceLightboxClass();
                    editEventInstanceLightboxHandle.init();
                    this.editEventInstanceLightboxHandle = editEventInstanceLightboxHandle;
                }
                bindEvent();
            }
        }

        var flipInitInstancesOfEvent = function (event_id) {
            var handles_to_update = itemRowHandles.filter(function (event_handle) {
                return event_handle.getId() == event_id && event_handle.getType() == 'event';
            });
            handles_to_update.forEach(function (handle_to_update) {
                handle_to_update.editEventInstanceLightboxHandle.eventImagesHandle.flipInit();
            });
        };

        var addItem = function (row_data, origin_type) {
            var newHandle = new itemRowClass(row_data, origin_type);
            newHandle.init();
            itemRowHandles.push(newHandle);
            return newHandle;
        }

        var selectClass = function () {
            var dom_list = $('.multi-select-wrapper .dropdown ul.dropdown-menu');
            var bindEvent = function () {
                dom_list.find('li').click(function () {
                    var checkbox = $(this).find('input');
                    checkbox.prop('checked', !checkbox.prop('checked')).change();
                    show_whats[$(this).data('what')] = checkbox.prop('checked');
                    goalsHandle.refreshGoals();
                    return false;
                });
            }
            this.init = function () {
                bindEvent();
            }
        }

        this.refreshGoals = function () {
            itemRowHandles.forEach(function (itemHandle) {
                itemHandle.hide();
            });
            itemRowHandles.sort(function (a, b) {
                if (a.getTime() > b.getTime()){
                    return 1;
                }
                else if (a.getTime() == b.getTime()){
                    return 0;
                }
                return -1;
            });

            itemRowHandles.forEach(function (itemHandle) {
                itemHandle.hide();
            });
            itemRowHandles.forEach(function (itemHandle) {
                if (show_whats[itemHandle.getType()]){
                    itemHandle.show();
                }
            });
        }
        this.makeItemHandles = function () {
            var makeInstancesHandlesOfEvent = function (event_data) {

                var now_date = new Date().yyyymmdd();
                var now_time = new Date().toTimeString().split(' ')[0];

                var eventInstancesHanldes = [];
                var start_date_to_show = event_data.start_date;
                var end_date_to_show = now_date;
                var i;

                if (typeof event_data.rescheduled_events != 'undefined'){
                    event_data.rescheduled_events.forEach(function (rescheduled_event) {
                        rescheduled_event.instance_type = 'rescheduled_instance';
                        rescheduled_event.event_images = event_data.event_images;
                        var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                            return event_favorite.is_rescheduled == true && event_favorite.instance_id == rescheduled_event.id;
                        });
                        rescheduled_event.is_favorite = favorite_rows.length;
                        rescheduled_event.favorite_row = rescheduled_event.is_favorite ? favorite_rows[0] : null;

                        if (rescheduled_event.start_date >= start_date_to_show && rescheduled_event.start_date <= end_date_to_show){
                            addItem(rescheduled_event, 'event');
                        }
                    });
                }

                if (event_data.is_recurring === '1'){
                    var step = 24 * 60 * 60 * 1000;

                    switch (event_data.recurring_pattern.recurring_type){
                        case 'd':
                            step *= (1 * event_data.recurring_pattern.separation_count);
                            break;
                        case 'w':
                            step *= (7 * event_data.recurring_pattern.separation_count);
                            break;
                        default:
                            break;
                    }
                    var distance = Math.ceil((new Date(start_date_to_show) - new Date(event_data.start_date)) / step);
                    if ( distance < 0 ) { distance = 0; }

                    for ( i = 0; ; i ++){
                        var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (distance + i)).yyyymmdd();
                        var instance_day = new Date(instance_date).getDay();
                        var isin = false;
                        if (instance_date > end_date_to_show){ break; }
                        for(var j = 0; j < event_data.cancelled_events.length; j++){
                            if (event_data.cancelled_events[j].start_date == instance_date){ isin = true; break; }
                        }
                        if (!isin && event_data.recurring_pattern.days_of_week[parseInt(instance_day)] == '1'){

                            var event_instance = {
                                origin_event: event_data,
                                event_id: event_data.id,
                                event_title: event_data.event_title,
                                event_description: event_data.event_description,
                                event_location: event_data.event_location,
                                start_date: instance_date,
                                start_time: event_data.start_time,
                                instance_type: 'recurring_instance',
                                event_images: event_data.event_images
                            }
                            var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                return event_favorite.is_rescheduled == 0 && event_favorite.start_date == instance_date;
                            });

                            event_instance.is_favorite = favorite_rows.length;
                            event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;
                            if (start_date_to_show <= event_instance.start_date && event_instance.start_date <= end_date_to_show){
                                addItem(event_instance, 'event');
                            }
                        }
                    }
                }
                else {
                    var event_instance = {
                        origin_event: event_data,
                        event_id: event_data.id,
                        event_title: event_data.event_title,
                        event_description: event_data.event_description,
                        event_location: event_data.event_location,
                        start_date: event_data.start_date,
                        start_time: event_data.start_time,
                        instance_type: 'single_instance',
                        event_images: event_data.event_images
                    };

                    var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                        return event_favorite.is_rescheduled == 0;
                    });
                    event_instance.is_favorite = favorite_rows.length;
                    event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                    if (event_instance.start_date >= start_date_to_show && event_instance.start_date <= end_date_to_show){
                        addItem(event_instance, 'event');
                    }
                }
            }
            goals_data.events.forEach(function (event_data) {
                makeInstancesHandlesOfEvent(event_data);
            });
            goals_data.ideabox.forEach(function (ideabox_row) {
                addItem(ideabox_row, 'ideabox');
            });
            goals_data.goals.forEach(function (goal) {
                addItem(goal, 'goal');
            });
        }

        selectHandle = new selectClass();

        this.init = function () {
            selectHandle.init();
            this.makeItemHandles();
            this.refreshGoals();
        }
    }
    jQuery(document).ready(function () {
        $(document).ajaxStart(function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        });
        $(document).ajaxComplete(function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        });
        $.ajax({
            url: ACTION_URL,
            type: 'post',
            data: {action: 'get_goals_data'},
            dataType: 'json',
            success: function (res) {
                if (!res.status){ alert('sorry, something wrong'); return false;}
                goalsHandle = new goalsClass(res.data);
                goalsHandle.init();
            }
        });
    })
})()