(function(){
    'use strict';

    var is_loading_controlled_in_local = false;

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    var EditSeriesClass = function () {

        var mainHandle;

        var MainClass = function () {

            var domRoot = $('.posts-container');
            var domPostsList = $('.posts-container .posts-list');

            var series = initialSeries, audioCnt = 1, videoCnt = 1;

            var postRowHandles = [], virtualRootHandle, virtualRootData;
            var self = this;

            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent'
            };

            var viewFormat = function (data) {
                var fData = {};
                var fk;

                for (var k in data) {
                    fk = helperHandle.keyOfVal(postFields, k);
                    if (fk) { fData[fk] = data[k]; }
                }
                fData.origin = data;
                return fData;
            }

            var postFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (postFields[k]) {
                        fData[postFields[k]] = data[k];
                    }
                }
                return fData;
            }

            var PostRowClass = function (postData) {

                var domRow, domChildList, domInnerWrp, domBody, domBodyInner, domUsedBody;
                var self = this, parentHandle = false;

                var isChildsLoaded = false, metaData = {};

                var childHandles = [];
                var bindEvents = function () {
                    domInnerWrp.find('.jstree-icon.jstree-ocl').click(function () {
                        if (domRow.hasClass('jstree-open') && childHandles.length && self.nodeType() !== 'post') {
                            self.collapseChild();
                        }
                        else {
                            self.expandChild();
                        }
                    });
                    if (postData.nodeType === 'post') {
                        domInnerWrp.find('.post-title').click(function () {
                            toggleBody();
                        });
                        domInnerWrp.find('.expand-post').click(function () {
                            toggleBody();
                        });
                    }
                }

                var loadChilds = function () {
                    childHandles = [];
                    domRow.addClass('jstree-loading');
                    is_loading_controlled_in_local = true;
                    domChildList.empty();

                    var resPromise, ajaxData;
                    ajaxData = {
                        action: 'get_items',
                        where: {intPost_series_ID: initialSeries.series_ID, intPost_parent: postData.id},
                        select: helperHandle.array_keys(postFormat(postData)),
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData, false);
                    return resPromise.then(function (res) {

                        var childPosts = res.data;
                        var promises = [];

                        childPosts.forEach(function (childPost) {
                            var fData = viewFormat(childPost);
                            promises.push(self.addChild(fData));
                        });

                        isChildsLoaded = true;
                        domRow.removeClass('jstree-loading');
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        is_loading_controlled_in_local = false;

                        if (!childHandles.length) {
                            domRow.addClass('jstree-leaf');
                        }
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve(true);
                            }, 200);
                        });
                    });
                }

                var bindData = function () {
                    domInnerWrp.find('.post-title').html(postData.title);

                    domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-' + postData.nodeType);
                    domRow.removeClass('jstree-leaf');
                    domRow.removeClass('mjs-nestedSortable-leaf');
                    domRow.removeClass('mjs-nestedSortable-branch');
                    domRow.removeClass('node-status-publish node-status-draft');

                    if (postData.nodeType == 'post') {
                        isChildsLoaded = true;
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-leaf');
                        self.expandChild();
                    }
                    else if (isChildsLoaded && !childHandles.length) {
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    else {
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    switch (postData.nodeType) {
                        case 'post':
                            domInnerWrp.find('.drag-icon.type-post').attr('src', postData.image);
                            break;
                        case 'menu':
                            domInnerWrp.find('.expand-col').remove();
                            domInnerWrp.find('.post-body').remove();
                            domInnerWrp.find('.drag-icon.type-menu').attr('src', 'assets/images/global-icons/tree/list.svg');
                            break;
                        case 'path':
                            domInnerWrp.find('.expand-col').remove();
                            domInnerWrp.find('.post-body').remove();
                            domInnerWrp.find('.drag-icon.type-path').attr('src', 'assets/images/global-icons/tree/path/'+ (postData.meta.domOrder) +'.svg');
                            break;
                        case 'root':
                            domInnerWrp.find('.expand-col').remove();
                            domInnerWrp.find('.post-body').remove();
                            domInnerWrp.find('.drag-icon.type-root').attr('src', postData.image);
                            break;
                    }
                }

                var makeBody = function () {
                    var availableTypes = [0, 2, 8];
                    var type = parseInt(postData.type);
                    type = availableTypes.indexOf(type) != -1 ? type : 'other';
                    var newDomUsedBody;
                    newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyInner);
                    switch (parseInt(type)){
                        case 0:
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                            domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                            domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                            var settings_ap = {
                                disable_volume: 'off',
                                disable_scrub: 'default',
                                design_skin: 'skin-wave',
                                skinwave_dynamicwaves: 'off'
                            };
                            dzsag_init('#audio-' + audioCnt, {
                                'transition':'fade',
                                'autoplay' : 'off',
                                'settings_ap': settings_ap
                            });
                            audioCnt++;

                            // return new Promise(function (resovle) {
                            //     setTimeout(function () {
                            //         resovle();
                            //     }, 2000);
                            // });

                            break;
                        case 2:
                            if (typeof metaData.videoId !== 'undefined'){
                                window.afterglow.destroyPlayer(metaData.videoId);
                            }
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.find('video').addClass('afterglow');
                            domUsedBody.find('video').attr('data-youtube-id', postData.body);
                            domUsedBody.find('video').attr('id', 'video_' + videoCnt);
                            metaData.videoId = 'video_' + videoCnt++;
                            if (window.afterglow && !window.afterglow.getPlayer(metaData.videoId)){
                                window.afterglow.init();
                            }
                            return new Promise(function (resolve) {
                                window.afterglow.on(metaData.videoId,'ready', function(){
                                    resolve();
                                });
                            });
                            break;
                        case 8:
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            break;
                        default:
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            break;
                    }
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 300);
                    });
                }

                var expandBody = function () {
                    if (postData.body !== undefined) {
                        domBody.collapse('show');
                        domInnerWrp.find('.expand-post').removeClass('collapsed').addClass('expanded');
                    }
                    else {
                        $('.loading').show();
                        ajaxAPiHandle.apiPost('Posts.php', {where: postData.id, action: 'get'}, false).then(function (res) {
                            postData.body = res.data.strPost_body;
                            if (parseInt(postData.type) === 0) {
                                domBody.collapse('show');
                            }
                            makeBody().then(function () {
                                    $('.loading').hide();
                                    if (parseInt(postData.type) === 0) {
                                    }
                                    else {
                                        domBody.collapse('show');
                                    }
                                    domInnerWrp.find('.expand-post').removeClass('collapsed').addClass('expanded');
                                });
                        });
                    }
                }

                var collapseBody = function () {
                    domBody.collapse('hide');
                    domInnerWrp.find('.expand-post').addClass('collapsed').removeClass('expanded');
                }

                var toggleBody = function () {
                    if (domBody.hasClass('in')) {
                        collapseBody();
                    }
                    else {
                        expandBody();
                    }
                }

                this.expandChild = function () {
                    if (!isChildsLoaded) {
                        loadChilds().then(function () {
                            domRow.removeClass('jstree-closed').addClass('jstree-open');
                            domRow.find('> ul').collapse('show');
                        });
                    }
                    else {
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        domRow.find('> ul').collapse('show');
                    }
                }

                this.collapseChild = function () {
                    domRow.removeClass('jstree-open');
                    domRow.find('> ul').collapse('hide');
                    domRow.find('> ul').off('hidden.bs.collapse').one('hidden.bs.collapse', function (e) {
                        domRow.addClass('jstree-closed');
                    });
                }

                this.addChildHandle = function (childHandle) {
                    childHandles.push(childHandle);
                    domRow.removeClass('jstree-leaf');
                }
                this.addChild = function (childPost) {
                    if (postData.nodeType === 'menu') {
                        childPost.meta = {domOrder: childHandles.length + 1};
                    }
                    var hdl = new PostRowClass(childPost);
                    hdl.init();
                    hdl.setParent(self);
                    return hdl.appendTo(domChildList);
                }
                this.setParent = function (parentHdl) {
                    parentHandle = parentHdl;
                    parentHandle.addChildHandle(self);
                }
                this.nodeType = function () {
                    return postData.nodeType;
                }
                this.makeTypeValid = function () {
                    var parentType = parentHandle ? parentHandle.nodeType() : 'path';
                    var myNodeType = self.nodeType(), newNodeType = myNodeType;
                    postData.strPost_nodeType = generateValidNodeType(parentType, myNodeType);
                    bindData();
                    childHandles.forEach(function (childHandle) {
                        childHandle.makeTypeValid();
                    });
                }
                this.setOrder = function (newOrder) {
                    postData.order = newOrder;
                }
                this.getOrder = function () {
                    return postData.order;
                }
                this.getDomPosition = function () {
                    return domRow.index();
                }
                this.getId = function () {
                    return postData.id;
                }
                this.removeDom = function () {
                    domRow.remove();
                }
                this.focus = function () {
                    domRow.addClass('focus');
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('focus');
                            resolve();
                        }, 700);
                    });
                }
                this.getDom = function () {
                    return domRow;
                }
                this.appendTo = function (domList) {
                    if (!domList) {
                        if (parentHandle) {
                            domList = parentHandle.getDom().find('> ul');
                        }
                        else {
                            domList = domPostsList;
                        }

                    }
                    setTimeout(function () {
                        domRow.addClass('appear');
                        domRow.appendTo(domList);
                    }, 200);
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('appear');
                            resolve();
                        }, 700);
                    });
                }
                this.disappear = function () {
                    setTimeout(function () {
                        domRow.addClass('disappear');
                    }, 200)
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('disappear');
                            domRow.detach();
                            resolve();
                        }, 700)
                    });
                }
                this.init = function () {
                    domRow = domRoot.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                    domInnerWrp = domRow.find('> .post-row-inner');
                    domBody = domInnerWrp.find('.post-body');
                    domBodyInner = domBody.find('.body-inner');
                    bindData();
                    domRow.attr('id', 'sortable-tree-item-' + self.getId());
                    if (self.nodeType() !== 'post') {
                        domRow.append('<ul class="collapse" style="height: 0px;"></ul>');
                        domChildList = domRow.find('> ul');
                    }
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }

            var generateValidNodeType = function(parentNodeType, myNodeType) {
                var newNodeType = myNodeType;

                switch (parentNodeType) {
                    case 'post':
                        break;
                    case 'menu':
                        newNodeType = 'path';
                        break;
                    case 'path':
                        if (myNodeType === 'path') {
                            newNodeType = 'menu';
                        }
                        break;
                }
                return newNodeType;
            }

            var bindEvents = function () {
                domPostsList.on('sortexpand', function (e, helper) {
                    var domT = domPostsList.find('li.mjs-nestedSortable-hovering');
                    var hdl = domT.data('controlHandle');
                    hdl.expandChild();
                });
            }

            this.addPostRow = function (post) {
                var postRowHandle = new PostRowClass(viewFormat(post));
                postRowHandle.init();
                postRowHandle.setParent(virtualRootHandle);
                postRowHandle.appendTo();
                postRowHandles.push(postRowHandle);
                return postRowHandle;
            }

            this.deleteInFront = function (id) {
                postRowHandles.forEach(function (hdl, i) {
                    if (hdl.getId() == id) {
                        hdl.removeDom();
                        postRowHandles.splice(i, 1);
                    }
                });
            }

            this.makeSortable = function () {
                var options = {
                    appendTo: $('.posts-container'),
                    placeholder: 'ui-state-highlight',
                    // containment: '.page.site-wrapper',
                    items: 'li',
                    protectRoot: true,
                    forcePlaceholderSize: true,
                    handle: '.drag-icon',
                    helper:	'clone',
                    opacity: .6,
                    revert: 250,
                    tabSize: 25,
                    tolerance: 'pointer',
                    toleranceElement: '> .post-row-inner',
                    maxLevels: 0,
                    isTree: true,
                    expandOnHover: 1000,
                    doNotClear: true,
                    startCollapsed: true,
                    listType: 'ul',
                    expandedClass: 'jstree-open',
                    collapsedClass: 'jstree-closed'
                };
                options.disableParentChange = true;
                domPostsList.nestedSortable(options);
            }

            this.init = function () {
                virtualRootData = {
                    id: 0,
                    title: series.strSeries_title,
                    image: series.strSeries_image,
                    type: 7,
                    order: 1,
                    nodeType: 'root',
                    parent: -1,
                    status: 'publish',
                    origin: {
                        intClientSubscription_post_ID: 0,
                    }
                };

                virtualRootHandle = new PostRowClass(virtualRootData);
                virtualRootHandle.init();
                virtualRootHandle.appendTo();
                virtualRootHandle.expandChild();
                // self.makeSortable();
                bindEvents();
            }
        }

        this.init = function () {

            mainHandle = new MainClass();
            mainHandle.init();
        }
    }

    var editSeriesHandle = new EditSeriesClass();
    editSeriesHandle.init();
})();