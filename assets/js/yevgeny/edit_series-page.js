(function () {

    'use strict';

    var PageClass = function () {

        var domPage, domPageHeader, domEmbedTypeDropDown;

        var seriesEditingHandle, seriesStructureHandle, seriesEmbedHandle, seriesConversationHandle;

        var bindEvents = function () {
            domPageHeader.find('[href="#series-structure-tab"]').on('shown.bs.tab', function () {
                domPage.removeClass('embed-mode');
                domPage.addClass('structure-mode');
                if (!seriesStructureHandle) {
                    seriesStructureHandle = new SeriesStructureClass();
                    seriesStructureHandle.init();
                }
            });
            domPageHeader.find('[href="#series-editing-tab"]').on('shown.bs.tab', function () {
                domPage.removeClass('structure-mode');
                domPage.removeClass('embed-mode');
            });
            domPageHeader.find('[href="#series-embed-tab"]').on('shown.bs.tab', function () {
                domPage.removeClass('structure-mode');
                domPage.addClass('embed-mode');
                if (!seriesEmbedHandle) {
                    seriesEmbedHandle = new SeriesEmbedClass();
                    seriesEmbedHandle.init();
                }
            });
        }

        var SeriesEditingClass = function () {
            var domForm, domCoverImgWrap;
            var chargeSeriesHandle;
            var coOwnersHandle;
            var imageState = 'initial';
            var isChanged = false;

            var bindEvents = function () {
                domForm.find('.cover-image-wrapper [name="strSeries_image"]').change(function () {
                    updateCoverImage(this);
                });
                domCoverImgWrap.dndhover().on('dndHoverStart', function (event) {
                    domCoverImgWrap.addClass('drag-over');
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                }).on('dndHoverEnd', function (event) {
                    domCoverImgWrap.removeClass('drag-over');
                    event.stopPropagation();
                    event.preventDefault();
                    return true;
                });
                domCoverImgWrap.on('drop', function(e) {
                    domCoverImgWrap.removeClass('drag-over');
                });
                domForm.find('input, select, textarea').change(function () {
                    if (!isChanged) {
                        swal('Remember to save your changes! None of the changes will take effect until you' +
                            'press the Save button on the bottom of this page.');
                        isChanged = true;
                    }
                });
                domForm.submit(function () {
                    save();
                    return false;
                });
                domPage.find('.invite-friend-form').submit(function () {
                    var v = $(this).find('[name="emails"]').val();
                    var emails = v.split(/\s*,+\s*/);
                    invite(emails);
                    return false;
                });
                domPage.find('.delete-series').click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "Do you want to delete this series? All posts of this series will be deleted",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete){
                            deleteSeries().then(function (res) {
                                if(res) {
                                    window.location = 'my_series';
                                }
                            });
                        }
                    });
                });
            }
            var bindData = function () {
                domForm.find('.day-item input').each(function (i) {
                    if (parseInt(series.available_days[i])) {
                        $(this).prop('checked', true).change();
                    }
                });
            }
            var deleteSeries = function () {
                return ajaxAPiHandle.apiPost('Series.php', {action: 'delete', where: series.series_ID}).then(function (res) {
                    return res.status;
                });
            }
            var updateCoverImage = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                    imageState = 'updated';
                }
            };

            var CoOwnersClass = function () {
                var domCoOwner, domItemsList;

                var filterInvitationStatus = 'accept';

                var itemHandles = [], filteredHandles = [];

                var ItemClass = function (itemData) {
                    var domItem;
                    var bindData = function () {
                        switch (parseInt(itemData.intSeriesOwner_role)) {
                            case 1:
                                domItem.find('.role-txt').html('Creator');
                                break;
                            case 2:
                                domItem.find('.role-txt').html('Admin');
                                break;
                            default:
                                domItem.find('.role-txt').html('Editor');
                                break;
                        }
                        if (itemData.user) {
                            if (itemData.user.image) {
                                domItem.find('.item-img-wrapper').css('background-image', 'url("' + itemData.user.image + '")').css('background-size', 'cover');
                            }
                            if (itemData.strSeriesInvitation_status == 'waiting') {
                                domItem.find('.user-name').html(itemData.strInvitation_email);
                            }
                            else {
                                domItem.find('.user-name').html(itemData.user.f_name);
                            }
                        }
                        else {
                            if (itemData.strSeriesInvitation_status == 'waiting') {
                                domItem.find('.user-name').html(itemData.strInvitation_email);
                            }
                        }
                    }
                    var bindEvents = function () {
                        domItem.find('.delete-action').click(function () {
                            if (itemData.intSeriesOwner_role == 1) {
                                return false;
                            }
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this owner?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteItem();
                                }
                            });
                        });
                    }
                    var deleteItem = function () {
                        if (itemData.strSeriesInvitation_status == 'accept') {
                            ajaxAPiHandle.apiPost('Series.php', {action: 'delete_owner', where: {intSeriesOwner_series_ID: itemData.intSeriesOwner_series_ID, intSeriesOwner_user_ID: itemData.intSeriesOwner_user_ID}}).then(function () {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().seriesOwner_ID == itemData.seriesOwner_ID) {
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                domItem.remove();
                            });
                        }
                        else {
                            ajaxAPiHandle.apiPost('Series.php', {action: 'delete_invitation', where: {intSeries_ID: itemData.intSeries_ID, strInvitation_email: itemData.strInvitation_email}}).then(function () {
                                itemHandles.forEach(function (value, i) {
                                    if (value.data().series_invitation_ID == itemData.series_invitation_ID) {
                                        itemHandles.splice(i, 1);
                                    }
                                });
                                domItem.remove();
                            });
                        }
                    }
                    this.data = function () {
                        return itemData;
                    }
                    this.append = function () {
                        domItem.insertBefore(domItemsList.find('.add-friend-card'));
                    }
                    this.detach = function () {
                        domItem.detach();
                    }
                    this.init = function () {
                        domItem = domItemsList.find('.owner-item.sample').clone().removeClass('sample').removeAttr('hidden');
                        bindData();
                        bindEvents();
                    }
                }
                var invite = function (emails) {
                    var ajaxData = {
                        action: 'invite',
                        id: series.series_ID,
                        emails: emails,
                    };
                    ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                        res.data.forEach(function (row) {
                            row.intSeriesOwner_role = 2;
                            var hdl = new ItemClass(row);
                            hdl.init();
                            itemHandles.push(hdl);
                            if (filterInvitationStatus == 'waiting') {
                                hdl.append();
                                filteredHandles.push(hdl);
                            }
                        });
                        swal({
                            icon: "success",
                            title: 'SUCCESS!',
                            text: 'Your invitation are sent successfully',
                        });
                    });
                }
                var bindEvents = function () {
                    domCoOwner.find('.owner-switch').change(function () {
                        filterInvitationStatus = $(this).prop('checked') ? 'waiting' : 'accept';
                        applyFilter();
                    });
                    domCoOwner.find('.add-input').keydown(function (e) {
                        var code = e.which;
                        if (code === 13) {
                            e.preventDefault();
                            var v = $(this).val();
                            var emails = v.split(/\s*,+\s*/);
                            invite(emails);
                            $(this).val('');
                            return false;
                        }
                    });
                }
                var applyFilter = function () {
                    domItemsList.css('min-height', domItemsList.css('height'));
                    domItemsList.css('opacity', 0);
                    setTimeout(function () {
                        filteredHandles.forEach(function (value) {
                            value.detach();
                        });
                        filteredHandles = itemHandles.filter(function (value) {
                            return value.data().strSeriesInvitation_status === filterInvitationStatus;
                        });
                        filteredHandles.forEach(function (value) {
                            value.append();
                        })
                        domItemsList.css('min-height', '0px');
                        domItemsList.css('opacity', 1);
                    }, 300);
                }
                this.init = function () {
                    domCoOwner = domForm.find('.co-owners-wrapper');
                    domItemsList = domCoOwner.find('.owners-container');
                    seriesOwners.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        hdl.append();
                        itemHandles.push(hdl);
                        filteredHandles.push(hdl);
                    });
                    invitations.forEach(function (itemData) {
                        var hdl = new ItemClass(itemData);
                        hdl.init();
                        itemHandles.push(hdl);
                    });
                    bindEvents();
                }
            }

            var ChargeSeriesClass = function () {
                var domChargeWrap;
                var chargeValue = parseInt(series.intSeries_price);
                var affiliateV = 6;
                var youV = chargeValue - affiliateV;
                var self = this;

                var bindEvents = function () {
                    domChargeWrap.find('input').change(function () {
                        var type = $(this).attr('type');
                        var v;
                        if (type === 'checkbox') {
                            v = $(this).prop('checked') ? 1 : 0;
                        }
                        else {
                            v = $(this).val();
                        }
                        series[$(this).attr('name')] = v;
                        chargeValue = parseInt(series.intSeries_price);
                        affiliateV = parseInt(series.intSeries_affiliate_price);
                        if (affiliateV > chargeValue) {
                            affiliateV = parseInt(chargeValue / 3);
                        }
                        youV = chargeValue - affiliateV;
                        bindData();
                    });
                }
                var bindData = function () {

                    if (series.boolSeries_charge) {
                        domChargeWrap.find('[name="boolSeries_charge"]').prop('checked', true);
                        domChargeWrap.find('[name="intSeries_price"]').bootstrapSlider('enable');
                        domChargeWrap.find('[name="intSeries_price"]').parents('.value-wrap').removeClass('disable');
                        if (series.boolSeries_affiliated) {
                            domChargeWrap.find('[name="intSeries_affiliate_price"]').parents('.value-wrap').removeClass('disable');
                            domChargeWrap.find('[name="intSeries_affiliate_price"]').parents('.value-wrap').removeClass('no-affiliate');
                            affiliateBootstrapSliderWith(chargeValue);
                            domChargeWrap.find('[name="boolSeries_affiliated"]').prop('checked', true);
                            domChargeWrap.find('[name="intSeries_affiliate_price"]').bootstrapSlider('enable');
                        }
                        else {
                            affiliateV = 0;
                            youV = chargeValue;
                            domChargeWrap.find('[name="intSeries_affiliate_price"]').parents('.value-wrap').addClass('no-affiliate');
                            domChargeWrap.find('[name="boolSeries_affiliated"]').prop('checked', false);
                            domChargeWrap.find('[name="intSeries_affiliate_price"]').bootstrapSlider('disable');
                        }
                    }
                    else {
                        domChargeWrap.find('[name="intSeries_price"]').parents('.value-wrap').addClass('disable');
                        domChargeWrap.find('[name="intSeries_affiliate_price"]').parents('.value-wrap').addClass('disable');
                        domChargeWrap.find('[name="boolSeries_charge"]').prop('checked', false);
                        domChargeWrap.find('[name="intSeries_price"]').bootstrapSlider('disable');
                        domChargeWrap.find('[name="intSeries_affiliate_price"]').bootstrapSlider('disable');
                        domChargeWrap.find('[name="boolSeries_affiliated"]').prop('checked', false);
                    }
                    bindEarnValue();
                }
                var affiliateBootstrapSliderWith = function (maxV) {
                    chargeValue = parseInt(maxV);
                    youV = chargeValue - affiliateV;
                    domChargeWrap.find('[name="intSeries_affiliate_price"]').bootstrapSlider ({
                        handle: 'square',
                        min: 0,
                        max: maxV,
                        value: affiliateV,
                    });
                }
                var bindEarnValue = function () {
                    domChargeWrap.find('.you-value').html('$' + youV);
                    domChargeWrap.find('.affiliate-value').html('$' + affiliateV);
                }
                this.data = function () {
                    var sets = {
                        boolSeries_charge: series.boolSeries_charge,
                        boolSeries_affiliated: series.boolSeries_affiliated,
                    };
                    if (series.intSeries_price) {
                        sets.intSeries_price = series.intSeries_price;
                    }
                    if (series.intSeries_affiliate_price) {
                        sets.intSeries_affiliate_price = series.intSeries_affiliate_price;
                    }
                    return sets;
                }
                this.dom = function () {
                    return domChargeWrap;
                }
                this.init = function () {
                    series.boolSeries_charge = parseInt(series.boolSeries_charge);
                    series.boolSeries_affiliated = parseInt(series.boolSeries_affiliated);
                    series.intSeries_price = series.intSeries_price ? series.intSeries_price : '';
                    series.intSeries_affiliate_price = series.intSeries_affiliate_price ? parseInt(series.intSeries_affiliate_price) : 6;
                    chargeValue = series.intSeries_price;
                    affiliateV = series.intSeries_affiliate_price;
                    youV = chargeValue - affiliateV;
                    domChargeWrap = domForm.find('.charge-series-wrapper');
                    domChargeWrap.find('[name="intSeries_price"]').bootstrapSlider ({
                        handle: 'square',
                        min: 0,
                        max: 45,
                        value: chargeValue
                    });
                    domChargeWrap.find('[name="intSeries_affiliate_price"]').bootstrapSlider ({
                        handle: 'square',
                        min: 0,
                        max: chargeValue,
                        value: affiliateV,
                    });
                    bindEarnValue();
                    bindData();
                    bindEvents();
                }
            }
            var invite = function (emails) {
                var ajaxData = {
                    action: 'invite',
                    id: series.series_ID,
                    emails: emails,
                };
                ajaxAPiHandle.apiPost('Series.php', ajaxData).then(function (res) {
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Your invitation are sent successfully',
                    }).then(function (value) {
                    });
                });
            }

            var save = function () {
                var sets = helperHandle.formSerialize(domForm);
                var availableDays = '';
                domForm.find('.day-item input').each(function (i) {
                    availableDays += $(this).prop('checked') ? '1' : '0';
                    sets.available_days = availableDays;
                });
                if (!sets.boolSeries_charge) {
                    sets.boolSeries_charge = 0;
                }
                if (!sets.boolSeries_affiliated) {
                    sets.boolSeries_affiliated = 0;
                }
                var ajaxData = new FormData();
                ajaxData.append('action', 'update');
                ajaxData.append('where', series.series_ID);
                ajaxData.append('sets', JSON.stringify(sets));
                if (imageState == 'updated') {
                    ajaxData.append('strSeries_image', domForm.find('.cover-image-wrapper [name="strSeries_image"]').prop('files')[0]);
                }
                return ajaxAPiHandle.multiPartApiPost('Series.php', ajaxData).then(function (res) {
                    swal("Success!", "Your changes have been saved", "success");
                });
            }

            this.init = function () {
                domForm = domPage.find('.edit-series-form');
                domCoverImgWrap = domForm.find('.cover-image-wrapper');
                domForm.find('[name="intSeries_category"]').select2();
                bindData();
                bindEvents();
                chargeSeriesHandle = new ChargeSeriesClass();
                chargeSeriesHandle.init();
                coOwnersHandle = new CoOwnersClass();
                coOwnersHandle.init();
            }
        }

        var SeriesStructureClass = function () {

            var domStructure;
            var postsStructureHandle, structureExplainerHandle;

            var bindEvents = function () {
            }

            this.init = function () {
                domStructure = domPage.find('#series-structure-tab');

                structureExplainerHandle = new PostsStructureExplainerClass(domStructure.find('.posts-structure-explainer-component'));
                structureExplainerHandle.init();

                postsStructureHandle = new PostsStructureClass(domStructure.find('.posts-structure-component'), series.series_ID);
                postsStructureHandle.init();
                postsStructureHandle.setMode('default');
                bindEvents();
            }
        }

        var SeriesEmbedClass = function () {
            var domRoot, domPreview;

            var editorHandle;

            var trackedEmbedIds = [];
            var newEmbedCnt = 0;
            var clicked = false;
            var receiveEmbedMessage = function(e) {
                if ( clicked && e.data.embedId && e.data.name && e.data.name == 'currentHeight') {
                    if (trackedEmbedIds.includes(e.data.embedId)) {
                        return ;
                    }
                    else {
                        trackedEmbedIds.push(e.data.embedId);
                        newEmbedCnt++;
                        if (newEmbedCnt === 2) {
                            $('.loading').hide();
                            newEmbedCnt = 0;
                            domRoot.find('.go-to-preview').addClass('active');
                        }
                    }
                }
            }

            var bindEvents = function () {
                editorHandle.onReload(function (embedCode) {
                    domPreview.empty();
                    domPreview.append(embedCode);
                });
                domEmbedTypeDropDown.find('ul li a').click(function () {
                    domEmbedTypeDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domEmbedTypeDropDown.data('value', v);
                    domEmbedTypeDropDown.find('button span').text(txt);
                    domRoot.find('.embed-code-header span').text(txt);
                    domRoot.find('.embed-style-item').removeClass('active-item');
                    domRoot.find('.embed-style-item[data-type="'+ v +'"]').addClass('active-item');
                    var embedCode = helperHandle.seriesEmbedCode(series, v);
                    $('.loading').show();
                    setTimeout(function () {
                        editorHandle.setHtml(embedCode.html);
                        editorHandle.setCss(embedCode.css);
                        editorHandle.setJs(embedCode.js);
                        editorHandle.reloadPreview();
                        clicked = true;
                    }, 100);
                });
                domRoot.find('.embed-style-item').click(function () {

                    var v = $(this).data('type');
                    domRoot.find('.embed-style-item').removeClass('active-item');
                    $(this).addClass('active-item');

                    domEmbedTypeDropDown.find('ul li').removeAttr('hidden');
                    domEmbedTypeDropDown.find('ul li a[data-value="'+ v +'"]').parent().attr('hidden', true);
                    var txt = domEmbedTypeDropDown.find('ul li a[data-value="'+ v +'"]').text();
                    domEmbedTypeDropDown.find('button span').text(txt);
                    domRoot.find('.embed-code-header span').text(txt);

                    var embedCode = helperHandle.seriesEmbedCode(series, v);
                    $('.loading').show();
                    setTimeout(function () {
                        editorHandle.setHtml(embedCode.html);
                        editorHandle.setCss(embedCode.css);
                        editorHandle.setJs(embedCode.js);
                        editorHandle.reloadPreview();
                        clicked = true;
                    }, 100);
                });
                domRoot.find('.get-code-btn').click(function () {
                    editorHandle.copyEmbed();
                    $(this).html('copied');
                    $(this).addClass('copied');
                    setTimeout(function () {
                        domRoot.find('.get-code-btn').html('Get code');
                        domRoot.find('.get-code-btn').removeClass('copied');
                    }, 1000);
                });
                window.addEventListener("message", receiveEmbedMessage);
            }
            this.init = function () {
                domRoot = domPage.find('#series-embed-tab');
                domPreview = domRoot.find('.embed-preview-wrapper');
                editorHandle = new EmbedEditorClass(domRoot.find('.component.embed-editor'));
                editorHandle.init();
                bindEvents();
                $('.loading').show();
                var embedCode = helperHandle.seriesEmbedCode(series, 'guided');
                setTimeout(function () {
                    editorHandle.setHtml(embedCode.html);
                    editorHandle.setCss(embedCode.css);
                    editorHandle.setJs(embedCode.js);
                    editorHandle.reloadPreview();
                    clicked = true;
                }, 100);
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            domPageHeader = domPage.find('.content-header');
            domEmbedTypeDropDown = domPageHeader.find('.select-embed-type-wrapper .dropdown');

            $.fn.dndhover = function(options) {
                return this.each(function() {

                    var self = $(this);
                    var collection = $();

                    self.on('dragenter', function(event) {
                        if (collection.length === 0) {

                            self.trigger('dndHoverStart');
                        }
                        collection = collection.add(event.target);
                    });

                    self.on('dragleave', function(event) {
                        /*
                         * Firefox 3.6 fires the dragleave event on the previous element
                         * before firing dragenter on the next one so we introduce a delay
                         */
                        setTimeout(function() {
                            collection = collection.not(event.target);
                            if (collection.length === 0) {
                                self.trigger('dndHoverEnd');
                            }
                        }, 100);
                    });
                });
            };

            seriesEditingHandle = new SeriesEditingClass();
            seriesEditingHandle.init();

            // copyEmbedHandle = new SeriesCopyEmbedClass(domPage.find('.component.series-copy-embed'), series);
            // copyEmbedHandle.init();

            if (tab === 'series') {
                domPage.removeClass('structure-mode');
            }
            else if (tab === 'structure') {
                seriesStructureHandle = new SeriesStructureClass();
                seriesStructureHandle.init();
                domPage.addClass('structure-mode');
            }
            else {
                seriesEmbedHandle = new SeriesEmbedClass();
                seriesEmbedHandle.init();
                domPage.addClass('embed-mode');
            }
            bindEvents();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();