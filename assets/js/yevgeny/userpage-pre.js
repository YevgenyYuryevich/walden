(function () {
    'use strict'
    var is_loading_controlled_in_local = 0;
    var count_per_page_on_switch_lightbox = 4;
    var used_post_types = [0, 2, 5, 8];

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
        ].join('-');
    };

    String.prototype.stringToDate = function () {
        var yyyy = this.split('-')[0];
        var mm = this.split('-')[1] * 1 - 1;
        var dd = this.split('-')[2];
        return new Date(yyyy, mm, dd);
    }

    function mobilecheck() {
        var check = false;
        (function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }

    var animEndEventNames = {
            'WebkitAnimation' : 'webkitAnimationEnd',
            'OAnimation' : 'oAnimationEnd',
            'msAnimation' : 'MSAnimationEnd',
            'animation' : 'animationend'
        },
        // animation end event name
        animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
        // event type (if mobile use touch events)
        eventtype = mobilecheck() ? 'touchstart' : 'click',
        // support for css animations
        support = Modernizr.cssanimations;

    function onAnimationEnd( elems, len, callback ) {
        var re_len = 0;
        elems.forEach(function (el) {
            if (el.hasChildNodes()){
                re_len++;
            }
        });
        if (re_len === 0){
            callback.call();
        }
        var finished = 0,
            onEndFn = function() {
                this.removeEventListener( animEndEventName, onEndFn );
                ++finished;
                if( finished === re_len ) {
                    callback.call();
                }
            };
        elems.forEach( function( el,i ) {
            if (el.querySelector('a') !== null){
                el.querySelector('a').addEventListener( animEndEventName, onEndFn );
            }
        });
    }

    var userpageHandle = function () {

        var post_objects = [];

        var switchLightboxHandle = function (series_id, post_id, dom_open_from, subscription_id, purchased_id, userHandleObj) {

            var root_dom = $('.switch-lightbox-wrapper .switch-lightbox').clone().appendTo('body');
            var dom_lightbox_wrapper = $('.switch-wrapper', root_dom);
            var dom_close_button = $('.close', root_dom);
            var ajaxResolve = $.Deferred();

            var newPostsSectionObject = null;
            var favoritePostsSectionObject = null;
            var ideaboxPostsSectionObject = null;

            var ideaboxes = [];
            var favorites = [];
            var new_posts = [];

            var getDataFromServer = function () {
                var ajaxData = {
                    action: 'get_switch_lightbox_data',
                    series_id: series_id,
                    post_id: post_id
                };

                $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    success: function (res) {
                        ideaboxes = res.data.ideaboxes;
                        favorites = res.data.favorites;
                        new_posts = res.data.new_posts;
                        ajaxResolve.resolve();
                    }
                });
            }
            getDataFromServer();

            var ready = function(callbackFunction) {

                $.when(ajaxResolve).done(function () {
                    callbackFunction();
                });
            }

            var openLightBox = function(){
                dom_open_from.addClass('open-lightbox-from-state');

                var window_width = $(window).width();

                setTimeout(function () {
                    root_dom.css('display', 'block');
                    var pos = dom_open_from.offset();
                    pos.top = pos.top - $(window).scrollTop();
                    var width = dom_open_from.outerWidth();
                    var height = dom_open_from.outerHeight();
                    pos.right = $(window).width() - pos.left - width;
                    pos.bottom = $(window).height() - pos.top - height;
                    dom_lightbox_wrapper.css(pos);

                    var to_css = {
                        top: '5vh',
                        right: '10vw',
                        left: '10vw',
                        bottom: '5vh',
                        fontSize: '10px',
                        padding: '50px'
                    };
                    if (window_width > 768 && window_width < 910){
                        to_css = {
                            top: '2vh',
                            right: '4vw',
                            left: '4vw',
                            bottom: '2vh',
                            fontSize: '10px',
                            padding: '20px'
                        }
                    }
                    else if (window_width < 768){
                        to_css = {
                            top: '1vh',
                            right: '2vw',
                            left: '2vw',
                            bottom: '1vh',
                            fontSize: '10px',
                            padding: '20px'
                        }
                    }

                    dom_lightbox_wrapper.animate(to_css, {
                        duration: 500,
                        complete: function () {
                            if (newPostsSectionObject == null){
                                newPostsSectionObject = new newPostsSectionHandle();
                            }
                            newPostsSectionObject.init();
                            if (favoritePostsSectionObject == null){
                                favoritePostsSectionObject = new favoritePostsSectionHandle();
                            }
                            favoritePostsSectionObject.init();
                            if (ideaboxPostsSectionObject == null){
                                ideaboxPostsSectionObject = new ideaboxPostsSectionHandle();
                            }
                            ideaboxPostsSectionObject.init();
                            root_dom.addClass('open-status');
                        }
                    });
                }, 100);
            }
            var closeLightBox = function () {
                root_dom.removeClass('open-status');
                newPostsSectionObject.clean();
                favoritePostsSectionObject.clean();
                ideaboxPostsSectionObject.clean();

                var pos = dom_open_from.offset();
                pos.top = pos.top - $(window).scrollTop();
                var width = dom_open_from.outerWidth();
                var height = dom_open_from.outerHeight();
                pos.right = $(window).width() - pos.left - width;
                pos.bottom = $(window).height() - pos.top - height;

                var css = $.extend(pos, {opacity: 0, fontSize: '5px', padding: '0px'});
                dom_lightbox_wrapper.animate(
                    css,
                    {
                        duration: 500,
                        start: function () {
                        },
                        complete: function () {
                            dom_lightbox_wrapper.removeAttr('style');
                            root_dom.css('display', 'none');
                            dom_open_from.removeClass('open-lightbox-from-state');
                        }
                    }
                );
            };

            var newPostsSectionHandle = function () {

                var root_dom_new_posts = $('.new-posts', root_dom);
                var dom_grid_wrapper = $('.tt-grid-wrapper', root_dom_new_posts);
                var dom_list_wrapper = $('ul.tt-grid', root_dom_new_posts);
                var dom_previous = $('.previous-page', root_dom_new_posts);
                var dom_next = $('.next-page', root_dom_new_posts);

                var pages = [];
                var current_page_index = 0;
                var pages_size = 0;

                new_posts.forEach(function (post, i) {
                    var page_index = parseInt(i / count_per_page_on_switch_lightbox);
                    post.html = '<a>' +
                        '<div class="column">' +
                        '<div class="column_object">' +
                        '<div class="image-wrapper has-advanced-upload"><img src="' + post.strPost_featuredimage + '" alt="" class="object_image" /></div>' +
                        '<div class="object_title">' + post.strPost_title + '</div>' +
                        '<div class="buttons-wrapper">' +
                        '<div class="select-post-button" data-post_id="' + post.post_ID + '">' +
                        '<div class="wrap"><button type="submit">Select</button><img src="../assets/images/check_arrow_2.svg" alt=""><svg width="42px" height="42px"><circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle></svg></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="object_line"></div>' +
                        '</div>' +
                        '</div>' +
                        '</a>';

                    if (typeof pages[page_index] == 'undefined'){
                        pages[page_index] = [];
                    }
                    pages[page_index].push(post);
                    pages_size = page_index + 1;
                });

                var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var selectPost = function (post_id) {
                    var ajaxData = {
                        action: 'set_post_as_subscription',
                        subscription_ID: subscription_id,
                        purchased_ID: purchased_id,
                        post_ID: post_id
                    };

                    $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: ajaxData,
                        dataType: 'json',
                        success: function (res) {
                            var res_data = res.data;
                            userHandleObj.updateInFront(res_data);
                            closeLightBox();
                        }
                    });
                }

                var addFuncToEl = function (el) {
                    el .find(':not(.tt-old)').find('.buttons-wrapper .select-post-button').click(function () {
                        selectPost($(this).data('post_id'));
                    })
                }

                // this is just a way we can test this. You would probably get your images with an AJAX request...

                function loadNewSet(action, set ) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    var newImages = pages[current_page_index];
                    if (action === 'pagination'){
                        newImages = pages[set];
                        current_page_index = set;
                    }
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );

                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){

                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html;
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el ) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !el.hasChildNodes() ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                addFuncToEl($(el));
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                var previous = function () {
                    if (current_page_index > 0 && !isAnimating){
                        loadNewSet('pagination', current_page_index - 1);
                    }
                }
                var next = function () {
                    if (current_page_index < pages_size - 1 && !isAnimating){
                        loadNewSet('pagination', current_page_index + 1);
                    }
                }

                var flipInit = function() {
                    grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    isAnimating = false;
                    loadNewSet();
                }

                return {
                    pages: pages,
                    init: function () {
                        dom_list_wrapper.html('');
                        for(var i = 0; i < count_per_page_on_switch_lightbox; i++){
                            dom_list_wrapper.append('<li class="tt-empty"></li>');
                        }
                        flipInit();
                        dom_previous.click(function () {
                            previous();
                        });
                        dom_next.click(function () {
                            next();
                        });
                    },
                    clean: function () {
                        dom_list_wrapper.html('');
                    }
                };
            }
            var favoritePostsSectionHandle = function () {

                var root_dom_favorite_posts = $('.favorite-posts', root_dom);
                var dom_grid_wrapper = $('.tt-grid-wrapper', root_dom_favorite_posts);
                var dom_list_wrapper = $('ul.tt-grid', root_dom_favorite_posts);
                var dom_previous = $('.previous-page', root_dom_favorite_posts);
                var dom_next = $('.next-page', root_dom_favorite_posts);

                var pages = [];
                var current_page_index = 0;
                var pages_size = 0;

                favorites.forEach(function (post, i) {
                    var page_index = parseInt(i / count_per_page_on_switch_lightbox);
                    post.html = '<a>' +
                        '<div class="column">' +
                        '<div class="column_object">' +
                        '<div class="image-wrapper has-advanced-upload"><img src="' + post.strPost_featuredimage + '" alt="" class="object_image" /></div>' +
                        '<div class="object_title">' + post.strPost_title + '</div>' +
                        '<div class="buttons-wrapper">' +
                        '<div class="select-post-button" data-post_id="' + post.post_ID + '">' +
                        '<div class="wrap"><button type="submit">Select</button><img src="../assets/images/check_arrow_2.svg" alt=""><svg width="42px" height="42px"><circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle></svg></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="object_line"></div>' +
                        '</div>' +
                        '</div>' +
                        '</a>';

                    if (typeof pages[page_index] == 'undefined'){
                        pages[page_index] = [];
                    }
                    pages[page_index].push(post);
                    pages_size = page_index + 1;
                });

                var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var selectPost = function (post_id) {
                    var ajaxData = {
                        action: 'set_post_as_subscription',
                        subscription_ID: subscription_id,
                        purchased_ID: purchased_id,
                        post_ID: post_id
                    };

                    $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: ajaxData,
                        dataType: 'json',
                        success: function (res) {
                            var res_data = res.data;
                            userHandleObj.updateInFront(res_data);
                            closeLightBox();
                        }
                    });
                }

                var addFuncToEl = function (el) {
                    el.find(':not(.tt-old)').find('.buttons-wrapper .select-post-button').click(function () {
                        selectPost($(this).data('post_id'));
                    })
                }

                // this is just a way we can test this. You would probably get your images with an AJAX request...

                function loadNewSet(action, set ) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    var newImages = pages[current_page_index];
                    if (action === 'pagination'){
                        newImages = pages[set];
                        current_page_index = set;
                    }
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );

                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){

                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html;
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el ) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !el.hasChildNodes() ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                addFuncToEl($(el));
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                var previous = function () {
                    if (current_page_index > 0 && !isAnimating){
                        loadNewSet('pagination', current_page_index - 1);
                    }
                }
                var next = function () {
                    if (current_page_index < pages_size - 1 && !isAnimating){
                        loadNewSet('pagination', current_page_index + 1);
                    }
                }

                var flipInit = function() {
                    grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    isAnimating = false;
                    loadNewSet();
                }

                return {
                    pages: pages,
                    init: function () {
                        dom_list_wrapper.html('');
                        for(var i = 0; i < count_per_page_on_switch_lightbox; i++){
                            dom_list_wrapper.append('<li class="tt-empty"></li>');
                        }
                        flipInit();
                        dom_previous.click(function () {
                            previous();
                        });
                        dom_next.click(function () {
                            next();
                        });
                    },
                    clean: function () {
                        dom_list_wrapper.html('');
                    }
                };
            }
            var ideaboxPostsSectionHandle = function () {

                var root_dom_ideabox_posts = $('.ideabox-posts', root_dom);
                var dom_grid_wrapper = $('.tt-grid-wrapper', root_dom_ideabox_posts);
                var dom_list_wrapper = $('ul.tt-grid', root_dom_ideabox_posts);
                var dom_previous = $('.previous-page', root_dom_ideabox_posts);
                var dom_next = $('.next-page', root_dom_ideabox_posts);
                var dom_back = $('.back-to-top-level', root_dom_ideabox_posts);

                // need
                var allItems = ideaboxes;
                var current_subcategory_id = 0;
                var allGroups = [], current_group, current_page_index = 0;
                var keys_of_id = [];

                var refreshItems = function () {
                    keys_of_id = [];
                    allGroups = [];
                    allItems.forEach(function (post, i) {
                        keys_of_id[post.ideabox_ID] = i;
                        post.html = '<a>' +
                            '<div class="column">' +
                            '<div class="column_object">' +
                            '<div class="image-wrapper has-advanced-upload"><img src="' + (post.strIdeaBox_image ? post.strIdeaBox_image : DEFAULT_IDEABOX_IMAGE) + '" alt="" class="object_image" /></div>' +
                            '<div class="object_title">' + post.strIdeaBox_title + '</div>' +
                            '<div class="buttons-wrapper">' +
                            '<div data-id = "' + post.ideabox_ID + '" class="explorer-ideabox-button" data-link = "load-group"><div><img src="assets/images/global-icons/ideabox.png"></div>Explore</div>' +
                            '<div class="select-post-button" data-ideabox_id="' + post.ideabox_ID + '">' +
                            '<div class="wrap"><button type="submit">Select</button><img src="../assets/images/check_arrow_2.svg" alt=""><svg width="42px" height="42px"><circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle></svg></div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="object_line"></div>' +
                            '</div>' +
                            '</div>' +
                            '</a>';
                    });

                    var flags = [];
                    allItems.forEach(function (item) {
                        var subcategory_id = item.intIdeaBox_subcategory;

                        if (flags[subcategory_id] === 1){ return; }

                        allGroups[subcategory_id] = allItems.filter(function (item) {
                            return item.intIdeaBox_subcategory === subcategory_id;
                        });
                        flags[subcategory_id] = 1;
                    });

                    allGroups.forEach(function (group) {
                        var len = group.length;

                        group.pages = [];
                        for (var i = 0; i*count_per_page_on_switch_lightbox < len; i ++ ){
                            group.pages[i] = [];
                            group.pages_size = i + 1;
                            for(var j = i*count_per_page_on_switch_lightbox; j < (i + 1)*count_per_page_on_switch_lightbox && j < len; j++){
                                group.pages[i].push(group[j].html);
                            }
                        }
                    });
                };

                refreshItems();
                current_group = allGroups[current_subcategory_id];

                var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var updateSubscriptionContent = function (title, body, image) {
                    if (!image){
                        image = DEFAULT_IDEABOX_IMAGE;
                    }
                    var ajaxData = {
                        action: 'update_subscription_content',
                        subscription_ID: subscription_id,
                        title: title,
                        body: body,
                        image: image,
                        type: 7
                    };

                    $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: ajaxData,
                        dataType: 'json',
                        success: function (res) {
                            if (res.status){
                                userHandleObj.updatePostType(7, false);
                                userHandleObj.updateThumnailImage(image, false, false);
                                userHandleObj.updateTitle(false, '', title);
                                userHandleObj.updateBody(null, body);
                            }
                            closeLightBox();
                        }
                    });
                }

                var back = function () {
                    if (current_subcategory_id == 0){ return; }
                    var parent = allItems[keys_of_id[current_subcategory_id]];
                    loadNewSet('load_setted_group', {subcategory_id: parent.intIdeaBox_subcategory})
                }

                var addFuncToEl = function (el) {
                    el.find(':not(.tt-old)').find('.buttons-wrapper .select-post-button').click(function () {
                        var ideabox_id = $(this).data('ideabox_id');
                        var title = allItems[keys_of_id[ideabox_id]].strIdeaBox_title;
                        var body = allItems[keys_of_id[ideabox_id]].strIdeaBox_idea;
                        var image = allItems[keys_of_id[ideabox_id]].strIdeaBox_image;
                        updateSubscriptionContent(title, body, image);
                    });
                    el.find(':not(.tt-old)').find('.buttons-wrapper .explorer-ideabox-button').click(function () {
                        loadNewSet('load_setted_group', {subcategory_id: $(this).data('id')});
                    });
                }

                // this is just a way we can test this. You would probably get your images with an AJAX request...

                function loadNewSet(action, set ) {

                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    var newImages = current_page_index > -1 ? current_group.pages[current_page_index] : [];

                    if (action === 'pagination'){
                        current_page_index = set;
                        newImages = current_group.pages[current_page_index];
                    }
                    else if (action === 'load_setted_group'){
                        current_page_index = 0;
                        current_subcategory_id = set.subcategory_id;
                        current_group = allGroups[current_subcategory_id];

                        if (typeof current_group !== 'undefined'){
                            newImages = current_group.pages[current_page_index];
                        }
                        else {
                            current_page_index = -1;
                            newImages = [];
                        }
                    }
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );

                    // apply effect
                    setTimeout( function() {

                        // append new elements
                        if(newImages){

                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el;
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el ) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !el.hasChildNodes() ) {
                                    classie.add( el, 'tt-empty' );
                                }
                                addFuncToEl($(el));
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                var previous = function () {
                    if (current_page_index > 0 && !isAnimating){
                        loadNewSet('pagination', current_page_index - 1);
                    }
                }
                var next = function () {
                    if (current_page_index > -1 && current_page_index < current_group.pages_size - 1 && !isAnimating){
                        loadNewSet('pagination', current_page_index + 1);
                    }
                }

                var flipInit = function() {
                    grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' );
                    items = [].slice.call( grid.querySelectorAll( 'li' ) );
                    isAnimating = false;
                    loadNewSet();
                }

                return {
                    allGroups: allGroups,
                    init: function () {
                        dom_list_wrapper.html('');
                        for(var i = 0; i < count_per_page_on_switch_lightbox; i++){
                            dom_list_wrapper.append('<li class="tt-empty"></li>');
                        }
                        flipInit();
                        dom_previous.click(function () {
                            previous();
                        });
                        dom_next.click(function () {
                            next();
                        });
                        dom_back.click(function () {
                            back();
                        });
                    },
                    clean: function () {
                        dom_list_wrapper.html('');
                    }
                };
            }

            return {
                root_dom: root_dom,
                openLightBox: openLightBox,
                init: function () {
                    dom_close_button.click(function () {
                        closeLightBox();
                    });
                },
                ready: ready
            };
        }

        var userObject = function (el) {

            var jq_dom = el;
            var dom_step_next = $('.step-next', jq_dom);
            var dom_step_before = $('.step-back', jq_dom);
            var dom_title = $('.uod_title', jq_dom);
            var dom_body = $('.uod_description', jq_dom);
            var dom_make_favorite = $('.make-favorite', jq_dom);
            var domViewLink = $('.view-post', jq_dom);
            var dom_open_switch_lightbox = $('.open-switch-lightbox', jq_dom);
            var dom_trash_post = $('.trash-post', jq_dom);
            var domComplete = jq_dom.find('.complete-wrapper .toogle-complete');

            var self = this;

            var subscription_id = jq_dom.data('clientsubscription_id');
            var purchased_id = jq_dom.data('purchased_id');
            var post_id = jq_dom.data('post_id');
            var series_id = jq_dom.data('series_id');
            var frequency_id = jq_dom.data('frequency_id');
            var video_id = dom_body.data('video_id');
            var switchLightboxObject = null;
            var favorite_id = jq_dom.data('favorite_id');

            var series_title = jq_dom.data('sereis_title');
            var post_type = jq_dom.data('post_type');
            var subscription_type = jq_dom.data('subscription_type');
            var use_post_type = subscription_type != null && subscription_type.toString().length ? subscription_type : post_type;
            var post_featuredimage = jq_dom.data('post_image');
            var series_image =  jq_dom.data('series_image');
            var subscription_image = jq_dom.data('subscription_image');
            var isComplete = jq_dom.data('post_complete') == '1' ? true : false;

            var updatePostType = function (new_subscription_type, new_post_type) {

                if (new_subscription_type !== false){
                    subscription_type = new_subscription_type ? parseInt(new_subscription_type) : null;
                }
                if (new_post_type !== false){
                    post_type = new_post_type ? parseInt(new_post_type) : null;
                }
                use_post_type = subscription_type != null && subscription_type.toString().length ? subscription_type : post_type;
            }
            var updateThumnailImage = function(strSubscription_image, strPost_featuredimage, strSeries_image) {

                subscription_image = strSubscription_image !== false ? strSubscription_image : subscription_image;
                post_featuredimage = strPost_featuredimage !== false ? strPost_featuredimage : post_featuredimage;
                series_image = strSeries_image !== false ? strSeries_image : series_image;

                var myimage = "assets/images/meditation.jfif";

                if (subscription_image == null || !subscription_image.length){
                    if(post_featuredimage == null || !post_featuredimage.length){
                        if(series_image != null && series_image.length){
                            myimage = strSeries_image;
                        }
                    }
                    else{
                        myimage = post_featuredimage;
                    }
                }
                else {
                    myimage = subscription_image;
                }
                $('> img.user_object_image', jq_dom).remove();
                if ($.inArray(parseInt(use_post_type), used_post_types) == -1){
                    jq_dom.prepend('<img class="user_object_image" src="'  + myimage + '">');
                }
            }
            var updateTitle = function (strSeries_title, strPost_title, strClientSubscription_title) {

                if (strSeries_title){ series_title = strSeries_title; }
                var use_title = strClientSubscription_title != null ? strClientSubscription_title : strPost_title;
                if (strClientSubscription_title){
                    dom_title.html('<span>' + use_title);
                }else {
                    dom_title.html('<span>' + series_title + ' - ' + use_title);
                }
            }
            var updateBody = function (strPost_body, strClientSubscription_body, postImage) {

                var use_body = strClientSubscription_body != null && strClientSubscription_body.length ? strClientSubscription_body : strPost_body;
                var inner_html = '';
                switch (parseInt(use_post_type)){
                    case 8:
                        var myembed = use_body.substr(0, use_body.indexOf("</section>"));
                        inner_html = myembed;
                        dom_body.html(inner_html);
                        break;
                    case 2:
                        var myembed = use_body.substr(use_body.indexOf("embed") + 6);
                        inner_html = '<video class="afterglow" data-autoresize="fit" id="video_' + myembed + '" width="960" height="540" data-youtube-id="' + myembed + '"></video>';
                        window.afterglow.destroyPlayer(video_id);
                        dom_body.html(inner_html);
                        window.afterglow.init();
                        video_id = 'video_' + myembed;
                        break;
                    case 0:
                        if (typeof postImage == 'undefined'){
                            postImage = subscription_image || post_featuredimage || series_image || 'assets/images/beautifulideas.jpg';
                        }
                        audiocounter++;
                        inner_html = '<div id="ag' + audiocounter + '" class="audiogallery">' +
                            '<div class="items">' +
                            '<div class="audioplayer-tobe" data-thumb="'+ postImage +'" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="' + use_body + '" data-sourceogg="sounds/adg3.ogg">;</div>' +
                            '</div>' +
                            '</div>';
                        dom_body.html(inner_html);
                        dzsag_init('#ag' + audiocounter,{
                            'transition':'fade'
                            ,'autoplay' : 'off'
                            ,'settings_ap': settings_ap
                        });

                        break;
                    case 5:
                        inner_html = '';
                        break;
                    default:
                        dom_body.html(use_body);
                        window.afterglow.init();
                        break;
                }
            }
            var updateFavorite = function(new_favorite_id){
                favorite_id = new_favorite_id;

                if (favorite_id){
                    dom_make_favorite.addClass('favorited');
                }
                else {
                    dom_make_favorite.removeClass('favorited');
                }
            }
            var updateViewLink = function (newLink) {
                domViewLink.attr('href', newLink);
            }
            var updateIsComplete = function (newIsComplete) {
                isComplete = newIsComplete === '1' || newIsComplete === true ? true : false;
                isComplete ? domComplete.addClass('btn-success') : domComplete.removeClass('btn-success');
            }

            var stepNext = function () {
                var ajaxData = {
                    action: 'step_next',
                    purchased_ID: purchased_id,
                    subscription_ID: subscription_id,
                    post_ID: post_id
                };

                $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    success: function (res) {
                        var res_data = res.data;
                        post_id = res_data.post_ID;
                        subscription_id = res_data.clientsubscription_ID;
                        favorite_id = res_data.favorite_id;
                        updatePostType(res_data.intClientSubscriptions_type, res_data.intPost_type);
                        updateThumnailImage(res_data.strClientSubscription_image, res_data.strPost_featuredimage, res_data.strSeries_image);
                        updateTitle(res_data.strSeries_title, res_data.strPost_title, res_data.strClientSubscription_title);
                        updateBody(res_data.strPost_body, res_data.strClientSubscription_body);
                        updateFavorite(favorite_id);
                        updateViewLink('view.php?id=' + post_id + '&sid=' + subscription_id + '&prepage=userpage');
                        updateIsComplete(res_data.isSubsCompleted);
                        switchLightboxObject = null;
                    }
                });
            }
            var stepBefore = function () {
                var ajaxData = {
                    action: 'step_before',
                    clientsubscription_ID: subscription_id,
                    purchased_ID: purchased_id,
                    frequency_ID: frequency_id,
                    series_ID: series_id
                };

                $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    success: function (res) {
                        var res_data = res.data;
                        post_id = res_data.post_ID;
                        subscription_id = res_data.clientsubscription_ID;
                        updatePostType(res_data.intClientSubscriptions_type, res_data.intPost_type);
                        updateThumnailImage(res_data.strClientSubscription_image, res_data.strPost_featuredimage, res_data.strSeries_image);
                        updateTitle(res_data.strSeries_title, res_data.strPost_title, res_data.strClientSubscription_title);
                        updateBody(res_data.strPost_body, res_data.strClientSubscription_body);
                        updateFavorite(res_data.favorite_id);
                        updateViewLink('view.php?id=' + post_id + '&sid=' + subscription_id);
                        updateIsComplete(res_data.isSubsCompleted);
                        switchLightboxObject = null;
                    }
                });
            }
            var makeFavorite = function () {
                var ajaxData = {
                    action: 'make_favorite',
                    series_id: series_id,
                    post_id: post_id,
                    subscriptionId: subscription_id
                };
                $.ajax({
                    url: ACTION_URL,
                    data: ajaxData,
                    success: function (res) {
                        if (res.status){
                            favorite_id = res.data;
                            dom_make_favorite.addClass('favorited');
                        }
                    },
                    type: 'post',
                    dataType: 'json'
                })
            }
            var makeUnFavorite = function () {
                var ajaxData = {
                    action: 'make_unfavorite',
                    favorite_id: favorite_id,
                };
                $.ajax({
                    url: ACTION_URL,
                    data: ajaxData,
                    success: function (res) {
                        if (res.status){
                            favorite_id = false;
                            dom_make_favorite.removeClass('favorited');
                        }
                    },
                    type: 'post',
                    dataType: 'json'
                })
            };

            var toggleFavorite = function () {
                if (favorite_id){
                    makeUnFavorite();
                }else {
                    makeFavorite();
                }
            }

            var updateInFront = function (post) {

                series_id = post.series_ID;
                post_id = post.post_ID;
                subscription_id = post.clientsubscription_ID;
                favorite_id = post.favorite_id;
                updatePostType(post.intClientSubscriptions_type, post.intPost_type);
                updateThumnailImage(post.strClientSubscription_image, post.strPost_featuredimage, post.strSeries_image);
                updateTitle(post.strSeries_title, post.strPost_title, post.strClientSubscription_title);
                updateBody(post.strPost_body, post.strClientSubscription_body);
                updateFavorite(favorite_id);
                updateViewLink('view.php?id=' + post_id + '&sid=' + subscription_id);
                updateIsComplete(post.isSubsCompleted);
                switchLightboxObject = null;
            }

            var openSwitchLightbox = function(){
                if (switchLightboxObject == null){
                    switchLightboxObject = new switchLightboxHandle(series_id, post_id, dom_open_switch_lightbox, subscription_id, purchased_id, self);
                    switchLightboxObject.init();
                }

                switchLightboxObject.ready(function () {
                    switchLightboxObject.openLightBox();
                });
            }

            var completePost = function () {
                var ajaxData = {};
                ajaxData.action = 'complete_subscription';
                ajaxData.id = subscription_id;
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    success: function (res) {
                        if (res.status){
                            isComplete = true;
                            domComplete.addClass('btn-success');
                        }
                    }
                });
            }

            var unCompletePost = function () {
                return $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: {action: 'unComplete_subscription', id: subscription_id},
                    dataType: 'json',
                    success: function (res) {
                        if (res.status){
                            isComplete = false;
                            domComplete.removeClass('btn-success');
                        }
                    }
                });
            }

            var bindEvents = function () {
                dom_step_next.click(function () {
                    stepNext();
                });
                dom_step_before.click(function () {
                    stepBefore();
                });
                dom_make_favorite.click(function () {
                    toggleFavorite();
                });
                dom_open_switch_lightbox.click(function () {
                    openSwitchLightbox();
                });
                dom_trash_post.click(function () {
                    trashPost();
                });
                domComplete.click(function () {
                    isComplete ? unCompletePost() : completePost();
                });
            }

            var trashPost = function(){
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: {
                        cancel: "Cancel!",
                        trashPost: {
                            text: "Trash Post",
                            value: "trash_post",
                            className: 'swal-button--danger'
                        },
                        removeSeries: {
                            text: 'Remove Series',
                            value: 'remove_series',
                            className: 'swal-button--danger'
                        },
                    },
                }).then(function(value){
                    switch (value) {
                        case "trash_post":
                            var ajaxData = {
                                action: 'trash_post',
                                purchased_id: purchased_id,
                                post_id: post_id
                            };
                            $.ajax({
                                url: ACTION_URL,
                                type: 'post',
                                data: ajaxData,
                                dataType: 'json',
                                success: function (res) {
                                    stepNext();
                                }
                            });
                            break;
                        case "remove_series":
                            self.removeSeries();
                            break;
                        default:
                            break;
                    }
                });
            }
            this.getId = function () {
                return subscription_id;
            }
            this.removeSeries = function () {
                $.ajax({
                    url: ACTION_URL,
                    data: {action: 'remove_series', purchasedId: purchased_id},
                    success: function (res) {
                        if (res.status){
                            for (var i = 0; i < post_objects.length; i++){
                                if (post_objects[i].getId() == subscription_id){
                                    post_objects.splice(i, 1);
                                }
                            }
                            jq_dom.remove();
                        }
                    },
                    type: 'post',
                    dataType: 'json'
                });
            }
            this.getDom = function () { return jq_dom; }
            this.stepNext = stepNext;
            this.stepBefore = stepBefore;

            this.init = function () {
                jq_dom.find('.share-post').circleNav(jq_dom.find('.circle-nav-wrapper'));
                bindEvents();
            };
            this.updateTitle = updateTitle;
            this.updateBody = updateBody;
            this.updatePostType = updatePostType;
            this.updateThumnailImage = updateThumnailImage;
            this.updateInFront = updateInFront;
        };

        return {
            init: function () {
                var objects = $('.user_objects > .user_object:not(.event-object)');
                objects.each(function (k, v) {
                    var tp = new userObject($(v));
                    tp.init();
                    post_objects.push(tp);
                });
            }
        }
    }();

    var eventsSectionClass = function (data) {

        var dom_events_section_root = $('.events-section');
        var personalEventsHandle = null;
        var todaysEventsHandle = null;
        var calendarForTodayHandle = null;

        var todaysEventsClass = function () {
            var dom_todays_inspirations_root = $('.insider .main_row .user_objects');

            var dom_inspiration = $('.insider .today-events .inspiration');
            var dom_date_range_to_show = $('.insider .today-events .date-range');

            var todaysEventRowHandles = [];
            var now_date = new Date().yyyymmdd();

            var start_date_to_show = now_date;
            var end_date_to_show = now_date;

            var todaysEventRowClass = function (event_data) {
                var dom_event_instance_root = $('.event-object.sample', dom_todays_inspirations_root).clone().removeAttr('hidden').removeClass('sample');
                var dom_main_img = $('>.user_object_image', dom_event_instance_root);
                var dom_event_title = $('.uod_title', dom_event_instance_root);
                var dom_event_description = $('.uod_description .event-description', dom_event_instance_root);
                var dom_event_location = $('.uod_description .event-location', dom_event_instance_root);
                var dom_make_favorite = $('.make-favorite', dom_event_instance_root);
                var dom_trash_object = $('.trash-post', dom_event_instance_root);
                var dom_switch_btn = $('.open-switch-lightbox', dom_event_instance_root);
                var is_favorite = event_data.is_favorite;
                var pos_in_parent = -1;

                var editEventInstanceLightboxHandle = null;

                var start_date = event_data.start_date;
                var start_time = event_data.start_time;

                var updatePosInParent = function (new_pos) {
                    pos_in_parent = new_pos;
                }

                var stringDateToFormat = function (date_str, time_str) {

                    var date_time = date_str.stringToDate();
                    var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nobember', 'December'];
                    var time_parts = time_str.match(/\d+/g);
                    var hours = parseInt(time_parts[0]);
                    var dd = 'AM';
                    if (hours >= 12){
                        dd = 'PM';
                        if (hours > 12){
                            hours = hours - 12;
                        }
                    }
                    return month_name[date_time.getMonth()] + ' ' + date_time.getDate() + ', ' + date_time.getFullYear() + ' at ' + hours + ':' + parseInt(time_parts[1]) + dd;
                };

                var self = this;

                var makeFavorite = function () {
                    var ajax_data = {
                        action: 'make_event_favorite',
                        event_id: event_data.event_id,
                        start_date: event_data.start_date,
                        is_rescheduled: 0
                    };
                    if (event_data.instance_type == 'rescheduled_instance'){
                        ajax_data.instance_id = event_data.id;
                        ajax_data.is_rescheduled = 1;
                    }
                    $.ajax({
                        url: ACTION_URL,
                        data: ajax_data,
                        success: function (res) {
                            if (res.status == true){
                                is_favorite = true;
                                event_data.is_favorite = true;
                                event_data.favorite_row = res.data;
                                dom_make_favorite.addClass('favorited');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                var makeUnFavorite = function () {
                    var ajax_data = {
                        action: 'make_event_unfavorite',
                        id: event_data.favorite_row.id
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajax_data,
                        success: function (res) {
                            if (res.status == true){
                                is_favorite = false;
                                event_data.is_favorite = false;
                                event_data.favorite_row = null;
                                dom_make_favorite.removeClass('favorited');
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    })
                }
                var cancelEventInstance = function (e) {
                    swal({
                        title: "Are you sure?",
                        text: "Do you want to cancel this event?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then(function (willDelete) {
                        if (willDelete){
                            var ajax_data = {};
                            if (event_data.instance_type == 'rescheduled_instance'){
                                ajax_data.action = 'delete_rescheduled_event';
                                ajax_data.id = event_data.id;
                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajax_data,
                                    dataType: 'json',
                                    success: function (res) {
                                        if (res.status){

                                            var pos_tp = 0;
                                            event_data.origin_event.rescheduled_events.forEach(function (rescheduled_event, i) {
                                                if (rescheduled_event.id == event_data.id){
                                                    pos_tp = i;
                                                }
                                            });
                                            event_data.origin_event.rescheduled_events.splice(pos_tp, 1);
                                            todaysEventsHandle.replaceInstances();
                                            todaysEventsHandle.refresh();
                                        }
                                    }
                                });
                            }
                            else if (event_data.instance_type == 'single_instance'){
                                ajax_data.action = 'delete_event';
                                ajax_data.id = event_data.event_id;
                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajax_data,
                                    dataType: 'json',
                                    success: function (res) {
                                        if (res.status){

                                            var pos_tp = 0;
                                            for (var i = 0; i < data.length; i ++){
                                                if (data[i].id == event_data.event_id){
                                                    pos_tp = i;
                                                    break;
                                                }
                                            }
                                            data.splice(pos_tp, 1);
                                            todaysEventsHandle.replaceInstances();
                                            todaysEventsHandle.refresh();
                                        }
                                    }
                                });
                            }
                            else {
                                ajax_data.action = 'cancel_event_instance';
                                ajax_data.data = {event_id: event_data.event_id, start_date: start_date, start_time: start_time};
                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajax_data,
                                    dataType: 'json',
                                    success: function (res) {
                                        if (res.status){

                                            event_data.origin_event.cancelled_events.push(res.data);
                                            todaysEventsHandle.replaceInstances();
                                            todaysEventsHandle.refresh();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }

                if (is_favorite){
                    dom_make_favorite.addClass('favorited');
                }


                var bindEvent = function () {
                    dom_make_favorite.click(function () {
                        console.log(is_favorite);
                        if (is_favorite){
                            makeUnFavorite();
                        }
                        else {
                            makeFavorite();
                        }
                    });
                    dom_trash_object.click(function (e) {
                        cancelEventInstance(e);
                    });
                    dom_switch_btn.click(function () {
                        editEventInstanceLightboxHandle.openLightbox();
                    })
                }

                if (event_data.event_images.length){
                    dom_main_img.attr('src', event_data.event_images[0].media_url);
                }
                dom_event_title.find('.event-title').html(event_data.event_title);
                dom_event_title.find('.start-datetime').html(stringDateToFormat(start_date, start_time));

                dom_event_description.html(event_data.event_description);
                dom_event_location.html(event_data.event_location);

                var applyChangeToInstance = function (new_data) {
                    if (new_data.event_title !== null){
                        event_data.event_title = new_data.event_title;
                        dom_event_title.html(new_data.event_title);
                    }
                    if (new_data.event_description !== null){
                        event_data.event_description = new_data.event_description != null ? new_data.event_description : event_data.event_description;
                        dom_event_description.html(new_data.event_description);
                    }
                    if (new_data.event_location !== null){
                        event_data.event_location = new_data.event_location != null ? new_data.event_location : event_data.event_location;
                        dom_event_location.html(event_data.event_location);
                    }
                    if (new_data.start_date !== null){
                        start_date = new_data.start_date;
                        event_data.start_date = new_data.start_date != null ? new_data.start_date : event_data.start_date;
                    }
                    if (new_data.start_time !== null){
                        start_time = new_data.start_time;
                        event_data.start_time = new_data.start_time != null ? new_data.start_time : event_data.start_time;
                    }
                    if (new_data.start_date !== null || new_data.start_time !== null){
                        dom_event_title.find('.start-datetime').html(stringDateToFormat(start_date, start_time));
                    }
                    if (event_data.event_images.length){
                        dom_main_img.attr('src', event_data.event_images[0].media_url);
                    }
                    else {
                        dom_main_img.attr('src', default_event_image);
                    }
                }

                var editEventInstanceLightboxClass = function () {
                    var dom_lightbox_root = $('.edit-event-lightbox.sample').clone().removeClass('sample');
                    var dom_lightbox_inner_wrapper = $('.edit-event-lightbox-inner-wrapper', dom_lightbox_root);
                    var dom_lightbox_inner = $('.edit-event-lightbox-inner', dom_lightbox_inner_wrapper);
                    var dom_close_btn = $('.close', dom_lightbox_inner_wrapper);
                    var dom_open_from = dom_event_instance_root;
                    var dom_title_wrapper = $('.event-title-wrapper', dom_lightbox_inner);
                    var dom_description_wrapper = $('.description-wrapper', dom_lightbox_inner);
                    var dom_location_wrapper = $('.location-wrapper', dom_lightbox_inner);
                    var dom_date_time_wrapper = $('.date-time-wrapper', dom_lightbox_inner);
                    var dom_main_image = $('.event-main-image', dom_lightbox_inner);

                    var event_instance_title = event_data.event_title;
                    var event_instance_description = event_data.event_description;
                    var event_instance_location = event_data.event_location;
                    var event_instance_start_date = event_data.start_date;
                    var event_instance_start_time = event_data.start_time;

                    var eventImagesHandle = null;
                    var startDateInstance = dom_date_time_wrapper.find('input.start-date').val(start_date).datepicker({
                        format: 'yyyy-mm-dd',
                        container: dom_lightbox_inner.find('.the-details')
                    });
                    var startTimeInstance = dom_date_time_wrapper.find('input.start-time').val(start_time).timepicker({
                        showMeridian: false
                    });

                    var eventIamgesClass = function () {

                        var dom_event_images_root = $('.event-images-container', dom_lightbox_inner);
                        var dom_grid_wrapper = $('.tt-grid-wrapper', dom_event_images_root);
                        var dom_list_wrapper = $('ul.tt-grid', dom_event_images_root);
                        var dom_previous = $('.previous-page', dom_event_images_root);
                        var dom_next = $('.next-page', dom_event_images_root);

                        var count_per_page = 5;

                        var pages = [];
                        var current_page_index = 0;
                        var pages_size = 0;
                        var keys_of_id = [];

                        event_data.event_images.forEach(function (post, i) {
                            keys_of_id[post.id] = i;
                            var page_index = parseInt(i / count_per_page);
                            post.html = '<a>' +
                                '<div class="event-image-item">' +
                                '<img class="delete-event-image-icon" data-id="' + post.id + '" src="assets/images/global-icons/close.png" title="delete this image"/>' +
                                '<img class="event-image" src="'+ post.media_url +'" />' +
                                '</div>' +
                                '</a>';

                            if (typeof pages[page_index] == 'undefined'){
                                pages[page_index] = [];
                            }
                            pages[page_index].push(post);
                            pages_size = page_index + 1;
                        });

                        var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                            items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                            isAnimating = false;


                        var removeIamgeFromEventInFront = function (id) {
                            event_data.event_images.splice(keys_of_id[id], 1);
                            flipInit();
                        }
                        var removeIamgeFromEvent = function (id) {
                            swal({
                                // title: "Are you sure?",
                                text: "Are you sure you want to delete this image?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    var ajax_data = {
                                        action: 'delete_event_image',
                                        id: id
                                    };
                                    $.ajax({
                                        url: ACTION_URL,
                                        data: ajax_data,
                                        success: function (res) {
                                            if (res.status == true){
                                                removeIamgeFromEventInFront(id);
                                            }
                                            else {
                                                alert('something went wrong');
                                            }
                                        },
                                        type: 'post',
                                        dataType: 'json'
                                    })
                                }
                            });
                        }

                        var addFuncToEl = function (el) {
                            el .find(':not(.tt-old)').find('.delete-event-image-icon').click(function () {
                                removeIamgeFromEvent($(this).data('id'));
                            })
                        }

                        // this is just a way we can test this. You would probably get your images with an AJAX request...

                        function loadNewSet(action, set ) {
                            if (isAnimating === true){
                                return ;
                            }
                            isAnimating = true;

                            var newImages = pages[current_page_index];
                            if (action === 'pagination'){
                                newImages = pages[set];
                                current_page_index = set;
                            }
                            items.forEach( function( el ) {
                                var itemChild = el.querySelector( 'a' );
                                // add class "tt-old" to the elements/images that are going to get removed
                                if( itemChild ) {
                                    classie.add( itemChild, 'tt-old' );
                                }
                            } );

                            // apply effect
                            setTimeout( function() {

                                // append new elements
                                if(newImages){

                                    [].forEach.call( newImages, function( el, i ) {
                                        items[ i ].innerHTML += el.html;
                                    } );
                                }

                                // add "effect" class to the grid
                                classie.add( grid, 'tt-effect-active' );


                                // wait that animations end
                                var onEndAnimFn = function() {
                                    // remove old elements
                                    items.forEach( function( el ) {
                                        // remove old elems
                                        var old = el.querySelector( '.tt-old' );
                                        if( old ) { el.removeChild( old ); }
                                        // remove class "tt-empty" from the empty items
                                        classie.remove( el, 'tt-empty' );
                                        // now apply that same class to the items that got no children (special case)
                                        if ( !el.hasChildNodes() ) {
                                            classie.add( el, 'tt-empty' );
                                        }
                                        addFuncToEl($(el));
                                    } );
                                    // remove the "effect" class
                                    classie.remove( grid, 'tt-effect-active' );
                                    isAnimating = false;
                                };

                                if( support ) {
                                    onAnimationEnd( items, items.length, onEndAnimFn );
                                }
                                else {
                                    onEndAnimFn.call();
                                }
                            }, 25 );
                        }

                        var previous = function () {
                            if (current_page_index > 0 && !isAnimating){
                                loadNewSet('pagination', current_page_index - 1);
                            }
                        }
                        var next = function () {
                            if (current_page_index < pages_size - 1 && !isAnimating){
                                loadNewSet('pagination', current_page_index + 1);
                            }
                        }

                        var flipInit = function() {
                            pages = [];
                            keys_of_id = [];
                            pages_size = 0;
                            event_data.event_images.forEach(function (post, i) {
                                keys_of_id[post.id] = i;
                                var page_index = parseInt(i / count_per_page);
                                if (typeof pages[page_index] == 'undefined'){
                                    pages[page_index] = [];
                                }
                                pages[page_index].push(post);
                                pages_size = page_index + 1;
                            });
                            isAnimating = false;
                            loadNewSet();
                            if (pages_size > 0){
                                dom_main_image.attr('src', event_data.event_images[0].media_url);
                            }
                            else {
                                dom_main_image.attr('src', default_event_image);
                            }
                        }

                        var accept_image_type = ['image/png', 'image/jpeg'];
                        var fileType = function (dataTransfer) {

                            var dropped_url = dataTransfer.getData('URL');

                            if(dropped_url != ''){
                                return dropped_url;
                            }

                            if (dataTransfer.files.length < 1){
                                return false;
                            }
                            var type = dataTransfer.files[0].type;

                            if (accept_image_type.indexOf(type) > -1){
                                return 'image';
                            }else if (accept_doc_types.indexOf(type) > -1){
                                return 'document';
                            } else {
                                return false;
                            }
                        };

                        var addImageToEvent = function (event_image_data) {
                            event_image_data.html = '<a>' +
                                '<div class="event-image-item">' +
                                '<img class="delete-event-image-icon" data-id="' + event_image_data.id + '" src="assets/images/global-icons/close.png" title="delete this image"/>' +
                                '<img class="event-image" src="'+ event_image_data.media_url +'" />' +
                                '</div>' +
                                '</a>';
                            keys_of_id[event_image_data.id] = event_data.event_images.length;
                            event_data.event_images.push(event_image_data);
                            if (pages_size > 0){
                                if (pages[pages_size - 1].length == count_per_page){
                                    pages[pages_size++] = [event_image_data];
                                }
                                else {
                                    pages[pages_size - 1].push(event_image_data);
                                }
                            }
                            else {
                                pages[pages_size++] = [event_image_data];
                            }
                        }

                        dom_event_images_root.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                        })
                            .on('dragover dragenter', function() {
                                dom_event_images_root.addClass('is-dragover');
                            })
                            .on('dragleave dragend drop', function() {
                                dom_event_images_root.removeClass('is-dragover');
                            })
                            .on('drop', function(e) {
                                var file_type = fileType(e.originalEvent.dataTransfer);

                                if (file_type === 'image'){
                                    dom_event_images_root.removeClass('is-dragover');
                                    var ajaxData = new FormData();
                                    var files = e.originalEvent.dataTransfer.files;
                                    ajaxData.append('event_image', files[0]);
                                    ajaxData.append('event_id', event_data.event_id);
                                    ajaxData.append('action', 'add_image_to_event');

                                    $.ajax({
                                        url: ACTION_URL,
                                        type: 'post',
                                        data: ajaxData,
                                        dataType: 'json',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        complete: function() {
                                        },
                                        success: function(res) {

                                            if (res.status === true){
                                                addImageToEvent(res.data);
                                                flipInit();
                                            }
                                            else {
                                                alert('sorry something is wrong');
                                            }
                                        },
                                        error: function() {
                                            alert('sorry something is wrong');
                                            // Log the error, show an alert, whatever works for you
                                        }
                                    });

                                }
                                else {
                                    alert('this is unsupported format');
                                }
                            });
                        this.init = function () {
                            flipInit();
                            dom_previous.click(function () {
                                previous();
                            });
                            dom_next.click(function () {
                                next();
                            });
                        };
                        this.flipInit = flipInit;
                        this.loadNewSet = loadNewSet;
                        this.addImageToEvent = addImageToEvent;
                    }

                    eventImagesHandle = new eventIamgesClass();

                    var bindEvent = function () {
                        dom_close_btn.click(function () {
                            closeLightBox();
                        });
                        dom_title_wrapper.find('.edit-entry').click(function () {
                            dom_title_wrapper.addClass('edit-status');
                            dom_title_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                        });
                        dom_title_wrapper.find('.save-entry').click(function () {
                            event_instance_title = dom_title_wrapper.find('.editable-content').html();
                            event_data.event_title = event_instance_title;
                            dom_title_wrapper.removeClass('edit-status');
                            dom_title_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_title_wrapper.find('.back-to-origin').click(function () {
                            dom_title_wrapper.removeClass('edit-status');
                            dom_title_wrapper.find('.editable-content').html(event_instance_title);
                            dom_title_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_description_wrapper.find('.edit-entry').click(function () {
                            dom_description_wrapper.addClass('edit-status');
                            dom_description_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                        });
                        dom_description_wrapper.find('.save-entry').click(function () {
                            event_instance_description = dom_description_wrapper.find('.editable-content').html();
                            event_data.event_description = event_instance_description;
                            dom_description_wrapper.removeClass('edit-status');
                            dom_description_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_description_wrapper.find('.back-to-origin').click(function () {
                            dom_description_wrapper.removeClass('edit-status');
                            dom_description_wrapper.find('.editable-content').html(event_instance_description);
                            dom_description_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_location_wrapper.find('.edit-entry').click(function () {
                            dom_location_wrapper.addClass('edit-status');
                            dom_location_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                        });
                        dom_location_wrapper.find('.save-entry').click(function () {
                            event_instance_location = dom_location_wrapper.find('.editable-content').html();
                            event_data.event_location = event_instance_location;
                            dom_location_wrapper.removeClass('edit-status');
                            dom_location_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_location_wrapper.find('.back-to-origin').click(function () {
                            dom_location_wrapper.removeClass('edit-status');
                            dom_location_wrapper.find('.editable-content').html(event_instance_location);
                            dom_location_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_date_time_wrapper.find('.edit-entry').click(function () {
                            dom_date_time_wrapper.addClass('edit-status');
                        });
                        dom_date_time_wrapper.find('.save-entry').click(function () {
                            dom_date_time_wrapper.removeClass('edit-status');
                            dom_date_time_wrapper.find('.for-display').find('.start-date').html(event_instance_start_date);
                            dom_date_time_wrapper.find('.for-display').find('.start-time').html(event_instance_start_time);
                        });
                        dom_date_time_wrapper.find('.back-to-origin').click(function () {
                            dom_date_time_wrapper.removeClass('edit-status');
                        });
                        startDateInstance.on('changeDate', function (e) {
                            event_instance_start_date = startDateInstance.val();
                        });
                        startTimeInstance.on('changeTime.timepicker', function (e) {
                            event_instance_start_time = startTimeInstance.val() + ':00';
                        });
                        dom_lightbox_inner.find('.apply-to-instance').html('SAVE').click(function () {
                            var ajax_data = {
                                data: {
                                    event_id: event_data.event_id,
                                    event_title: event_instance_title,
                                    event_description: event_instance_description,
                                    event_location: event_instance_location,
                                    start_date: event_instance_start_date,
                                    start_time: event_instance_start_time
                                }
                            }
                            switch (event_data.instance_type){
                                case 'recurring_instance':
                                    ajax_data.origin_start_date = event_data.start_date;
                                    ajax_data.action = 'reschedule_instance';
                                    break;
                                case 'single_instance':
                                    ajax_data.action = 'update_event';
                                    break;
                                case 'rescheduled_instance':
                                    ajax_data.id = event_data.id;
                                    ajax_data.action = 'update_reschduled_instance';
                                    break;
                            }
                            $.ajax({
                                url: ACTION_URL,
                                data: ajax_data,
                                success: function (res) {
                                    if (res.status == true){
                                        applyChangeToInstance(ajax_data.data);
                                        if (ajax_data.action == 'reschedule_instance'){
                                            event_data.id = res.data;
                                            event_data.instance_type = 'rescheduled_instance';
                                        }
                                        todaysEventsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong!');
                                    }
                                    closeLightBox();
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        });
                        dom_lightbox_inner.find('.apply-to-all-instance').hide().click(function () {
                            var ajax_data = {
                                data: {
                                    event_id: event_data.event_id,
                                    event_title: event_instance_title,
                                    event_description: event_instance_description,
                                    event_location: event_instance_location,
                                    start_date: event_instance_start_date,
                                    start_time: event_instance_start_time
                                }
                            };
                            switch (event_data.instance_type){
                                case 'recurring_instance':
                                    ajax_data.origin_start_date = event_data.start_date;
                                    ajax_data.action = 'update_all_instances_of_event';
                                    ajax_data.doReschedule = event_data.start_date != event_instance_start_date ? 1 : 0;
                                    break;
                                case 'single_instance':
                                    ajax_data.action = 'update_event';
                                    break;
                                case 'rescheduled_instance':
                                    ajax_data.id = event_data.id;
                                    ajax_data.doReschedule = 2;
                                    ajax_data.action = 'update_all_instances_of_event';
                                    break;
                            }
                            $.ajax({
                                url: ACTION_URL,
                                data: ajax_data,
                                success: function (res) {
                                    if (res.status == true){

                                        applyChangeToInstance(ajax_data.data);
                                        if (ajax_data.action == 'update_all_instances_of_event'){
                                            updateAllInstancesOfEvent(event_data.event_id, ajax_data.data);
                                            if (ajax_data.doReschedule == 1){
                                                event_data.instance_type = 'rescheduled_instance';
                                                event_data.id = res.data;
                                            }
                                            todaysEventsHandle.refresh();
                                        }
                                    }
                                    else {
                                        alert('something went wrong!');
                                    }
                                    closeLightBox();
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        });
                        dom_lightbox_inner.find('.cancel').click(function () {
                            closeLightBox();
                        })
                    }

                    var is_open = false;

                    var openLightbox = function () {
                        is_open = true;
                        var window_width = $(window).width();

                        dom_lightbox_root.appendTo('body');

                        dom_open_from.addClass('open-lightbox-from-state');
                        dom_lightbox_inner.css('display', 'none');
                        setTimeout(function () {
                            dom_lightbox_root.css('display', 'block');
                            var pos = dom_open_from.offset();
                            pos.top = pos.top - $(window).scrollTop();
                            var width = dom_open_from.outerWidth();
                            var height = dom_open_from.outerHeight();
                            pos.right = $(window).width() - pos.left - width;
                            pos.bottom = $(window).height() - pos.top - height;
                            dom_lightbox_inner_wrapper.css(pos);

                            var to_css = {
                                top: '5vh',
                                right: '10vw',
                                left: '10vw',
                                bottom: '5vh',
                                fontSize: '10px',
                                padding: '50px'
                            };
                            if (window_width > 768 && window_width < 910){
                                to_css = {
                                    top: '2vh',
                                    right: '4vw',
                                    left: '4vw',
                                    bottom: '2vh',
                                    fontSize: '10px',
                                    padding: '20px'
                                }
                            }
                            else if (window_width < 768){
                                to_css = {
                                    top: '1vh',
                                    right: '2vw',
                                    left: '2vw',
                                    bottom: '1vh',
                                    fontSize: '10px',
                                    padding: '20px'
                                }
                            }

                            dom_lightbox_inner_wrapper.animate(
                                to_css, {
                                    duration: 500,
                                    complete: function () {
                                        dom_lightbox_inner_wrapper.css('padding', 'auto');
                                        dom_lightbox_inner.css('display', 'block');
                                        dom_lightbox_root.addClass('open-status');
                                    }
                                });
                        }, 100);
                    }
                    var closeLightBox = function () {
                        dom_lightbox_root.removeClass('open-status');
                        is_open = false;
                        dom_lightbox_inner.css('display', 'none');
                        dom_lightbox_inner_wrapper.css('padding', '0px');
                        var pos = dom_open_from.offset();
                        pos.top = pos.top - $(window).scrollTop();
                        var width = dom_open_from.outerWidth();
                        var height = dom_open_from.outerHeight();
                        pos.right = $(window).width() - pos.left - width;
                        pos.bottom = $(window).height() - pos.top - height;

                        var css = $.extend(pos, {});
                        dom_lightbox_inner_wrapper.animate(
                            css,
                            {
                                duration: 500,
                                start: function () {
                                },
                                complete: function () {
                                    dom_lightbox_inner_wrapper.removeAttr('style');
                                    dom_lightbox_root.css('display', 'none');
                                    dom_open_from.removeClass('open-lightbox-from-state');
                                    dom_lightbox_root.detach();
                                }
                            }
                        );
                    }

                    this.openLightbox = openLightbox;
                    this.closeLightBox = closeLightBox;
                    this.addImageToEvent = eventImagesHandle.addImageToEvent;
                    this.init = function () {
                        dom_title_wrapper.find('.editable-content').html(event_instance_title);
                        dom_description_wrapper.find('.editable-content').html(event_instance_description);
                        dom_location_wrapper.find('.editable-content').html(event_instance_location);
                        dom_date_time_wrapper.find('.for-display').find('.start-date').html(event_instance_start_date);
                        dom_date_time_wrapper.find('.for-display').find('.start-time').html(event_instance_start_time);
                        eventImagesHandle.init();
                        bindEvent();
                    };
                    this.eventImagesHandle = eventImagesHandle;
                }

                editEventInstanceLightboxHandle = new editEventInstanceLightboxClass();

                this.show = function () {
                    dom_todays_inspirations_root.prepend(dom_event_instance_root);
                }
                this.hide = function () {
                    dom_event_instance_root.detach();
                }
                this.removeDom = function () {
                    dom_event_instance_root.remove();
                }
                this.getDateTime = function () {
                    return start_date + ' ' + start_time;
                }
                this.getTime = function () {
                    return new Date(start_date + ' ' + start_time).getTime();
                }
                this.getStartDate = function(){ return start_date; };
                this.init = function () {
                    bindEvent();
                    editEventInstanceLightboxHandle.init();
                }
                this.updatePosInParent = updatePosInParent;
                this.getPosInParent = function () {
                    return pos_in_parent;
                }
                this.dom_event_instance_root = function () {
                    return dom_event_instance_root;
                }
            }

            var getInstancesHandlesOfEvent = function (event_data) {

                var now_date = new Date().yyyymmdd();
                var now_time = new Date().toTimeString().split(' ')[0];

                var eventInstancesHanldes = [];

                var i;

                if (typeof event_data.rescheduled_events != 'undefined'){
                    event_data.rescheduled_events.forEach(function (rescheduled_event) {
                        rescheduled_event.instance_type = 'rescheduled_instance';
                        rescheduled_event.event_images = event_data.event_images;
                        var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                            return event_favorite.is_rescheduled == true && event_favorite.instance_id == rescheduled_event.id;
                        });
                        rescheduled_event.is_favorite = favorite_rows.length;
                        rescheduled_event.favorite_row = rescheduled_event.is_favorite ? favorite_rows[0] : null;

                        if (rescheduled_event.start_date >= start_date_to_show && rescheduled_event.start_date <= end_date_to_show){
                            todaysEventsHandle.addInstanceRow(rescheduled_event);
                        }
                    });
                }

                if (event_data.is_recurring === '1'){
                    var step = 24 * 60 * 60 * 1000;

                    switch (event_data.recurring_pattern.recurring_type){
                        case 'd':
                            step *= (1 * event_data.recurring_pattern.separation_count);
                            break;
                        case 'w':
                            step *= (7 * event_data.recurring_pattern.separation_count);
                            break;
                        default:
                            break;
                    }
                    var distance = Math.ceil((new Date(start_date_to_show) - new Date(event_data.start_date)) / step);
                    if ( distance < 0 ) { distance = 0; }

                    for ( i = 0; ; i ++){
                        var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (distance + i)).yyyymmdd();
                        var instance_day = new Date(instance_date).getDay();
                        var isin = false;
                        if (instance_date > end_date_to_show){ break; }
                        for(var j = 0; j < event_data.cancelled_events.length; j++){
                            if (event_data.cancelled_events[j].start_date == instance_date){ isin = true; break; }
                        }
                        if (!isin && event_data.recurring_pattern.days_of_week[parseInt(instance_day)] == '1'){

                            var event_instance = {
                                origin_event: event_data,
                                event_id: event_data.id,
                                event_title: event_data.event_title,
                                event_description: event_data.event_description,
                                event_location: event_data.event_location,
                                start_date: instance_date,
                                start_time: event_data.start_time,
                                instance_type: 'recurring_instance',
                                event_images: event_data.event_images
                            }
                            var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                return event_favorite.is_rescheduled == 0 && event_favorite.start_date == instance_date;
                            });

                            event_instance.is_favorite = favorite_rows.length;
                            event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;
                            if (start_date_to_show <= event_instance.start_date && event_instance.start_date <= end_date_to_show){
                                todaysEventsHandle.addInstanceRow(event_instance);
                            }
                        }
                    }
                }
                else {
                    var event_instance = {
                        origin_event: event_data,
                        event_id: event_data.id,
                        event_title: event_data.event_title,
                        event_description: event_data.event_description,
                        event_location: event_data.event_location,
                        start_date: event_data.start_date,
                        start_time: event_data.start_time,
                        instance_type: 'single_instance',
                        event_images: event_data.event_images
                    };

                    var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                        return event_favorite.is_rescheduled == 0;
                    });
                    event_instance.is_favorite = favorite_rows.length;
                    event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                    if (event_instance.start_date >= start_date_to_show && event_instance.start_date <= end_date_to_show){
                        todaysEventsHandle.addInstanceRow(event_instance);
                    }
                }
                return eventInstancesHanldes;
            }

            var convertDateFormat = function (date_to_convert) {
                var datePart = date_to_convert.match(/\d+/g);
                var yy = datePart[0].substring(2);
                var m = parseInt(datePart[1]);
                var d = parseInt(datePart[2]);
                return m + '/' + d + '/' + yy;
            }

            this.addInstanceRow = function (event_data) {
                var handle = new todaysEventRowClass(event_data);
                todaysEventRowHandles.push(handle);
                handle.init();
                return handle;
            }

            this.updateDateRangeToShow = function (s_date, e_date) {
                start_date_to_show = s_date;
                end_date_to_show = e_date;
            };

            this.replaceInstances = function () {
                todaysEventRowHandles.forEach(function (todaysEventRowHandle) {
                    todaysEventRowHandle.removeDom();
                });
                todaysEventRowHandles = [];
                data.forEach(function (event_row) {
                    getInstancesHandlesOfEvent(event_row);
                });
            }

            this.refresh = function () {

                dom_date_range_to_show.find('.start-date').html(convertDateFormat(start_date_to_show));
                dom_date_range_to_show.find(('.end-date')).html(convertDateFormat(end_date_to_show));

                if (start_date_to_show == now_date && end_date_to_show == now_date){
                    dom_inspiration.show();
                    dom_date_range_to_show.hide();
                }
                else {
                    dom_inspiration.hide();
                    dom_date_range_to_show.show();
                }

                todaysEventRowHandles.forEach(function (todaysEventRowHandle) {
                    todaysEventRowHandle.hide();
                });
                todaysEventRowHandles = todaysEventRowHandles.sort(function (a, b) {
                    if (a.getDateTime() > b.getDateTime()){
                        return -1;
                    }
                    else if (a.getDateTime() == b.getDateTime()){
                        return 0;
                    }
                    return 1;
                });

                todaysEventRowHandles.forEach(function (todaysEventRowHandle, i) {
                    todaysEventRowHandle.updatePosInParent(i);
                    if (start_date_to_show <= todaysEventRowHandle.getStartDate() && todaysEventRowHandle.getStartDate() <= end_date_to_show){
                        todaysEventRowHandle.show();
                    }
                });
            }

            this.init = function () {

                dom_date_range_to_show.find('.back-to-today').click(function () {
                    calendarForTodayHandle.clearDateRange();
                });
                this.replaceInstances();
                this.refresh();
            }
        }
        todaysEventsHandle = new todaysEventsClass();

        var personalEventsClass = function () {
            var dom_personal_events_root = $('.personal-events', dom_events_section_root);
            var dom_event_list = $('ul.event-list', dom_personal_events_root);
            var dom_view_more = $('footer .view-more-event', dom_personal_events_root)
            var dom_add_event = $('footer .add-event', dom_personal_events_root);
            var editEventLightboxHandle = null;

            var events = data;
            var event_handles = [];
            var current_index = 0;

            var eventRowClass = function (event_row_data) {

                var dom_event_row_root = $('li.sample', dom_event_list).clone().removeClass('sample').removeAttr('hidden');
                var dom_favorite = $('.make-favorite', dom_event_row_root);
                var dom_cancel_event = $('.cancel-event-instance', dom_event_row_root);
                var dom_occurring_date = $('.occurring-datetime', dom_event_row_root);
                var dom_event_title = $('.event-title', dom_event_row_root);
                var dom_event_instance = $('.event-instance', dom_event_row_root);

                var start_date = event_row_data.start_date;
                var start_time = event_row_data.start_time;
                var is_recurring = (event_row_data.is_recurring === '1' || event_row_data.is_recurring == true) ? true : false;
                var is_favorite = event_row_data.is_favorite;
                if (is_favorite){ dom_favorite.addClass('favorited'); }

                var editEventInstanceLightboxHandle = null;

                var pos_in_parent = -1;

                var stringDateToFormat = function (date_str, time_str) {

                    var date_time = date_str.stringToDate();
                    var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Nobember', 'December'];
                    var time_parts = time_str.match(/\d+/g);
                    var hours = parseInt(time_parts[0]);
                    var dd = 'AM';
                    if (hours >= 12){
                        dd = 'PM';
                        if (hours > 12){
                            hours = hours - 12;
                        }
                    }
                    return month_name[date_time.getMonth()] + ' ' + date_time.getDate() + ', ' + date_time.getFullYear() + ' at ' + hours + ':' + parseInt(time_parts[1]) + dd;
                };

                var showOn = function (parent_dom) {
                    dom_event_row_root.appendTo(parent_dom);
                }

                var self = this;

                var applyChangeToInstance = function (new_data) {
                    if (new_data.event_title !== null){
                        event_row_data.event_title = new_data.event_title;
                        dom_event_title.html(new_data.event_title);
                    }
                    if (new_data.event_description !== null){
                        event_row_data.event_description = new_data.event_description != null ? new_data.event_description : event_row_data.event_description;
                    }
                    if (new_data.event_location !== null){
                        event_row_data.event_location = new_data.event_location != null ? new_data.event_location : event_row_data.event_description;
                    }
                    if (new_data.start_date !== null){
                        start_date = new_data.start_date;
                        event_row_data.start_date = new_data.start_date != null ? new_data.start_date : event_row_data.start_date;
                    }
                    if (new_data.start_time !== null){
                        start_time = new_data.start_time;
                        event_row_data.start_time = new_data.start_time != null ? new_data.start_time : event_row_data.start_time;
                    }
                    dom_occurring_date.html(stringDateToFormat(event_row_data.start_date, event_row_data.start_time));
                }

                var editEventInstanceLightboxClass = function () {
                    var dom_lightbox_root = $('.edit-event-lightbox.sample').clone().removeClass('sample');
                    var dom_lightbox_inner_wrapper = $('.edit-event-lightbox-inner-wrapper', dom_lightbox_root);
                    var dom_lightbox_inner = $('.edit-event-lightbox-inner', dom_lightbox_inner_wrapper);
                    var dom_close_btn = $('.close', dom_lightbox_inner_wrapper);
                    var dom_open_from = dom_event_row_root;
                    var dom_title_wrapper = $('.event-title-wrapper', dom_lightbox_inner);
                    var dom_description_wrapper = $('.description-wrapper', dom_lightbox_inner);
                    var dom_location_wrapper = $('.location-wrapper', dom_lightbox_inner);
                    var dom_date_time_wrapper = $('.date-time-wrapper', dom_lightbox_inner);
                    var dom_main_image = $('.event-main-image', dom_lightbox_inner);

                    var event_instance_title = event_row_data.event_title;
                    var event_instance_description = event_row_data.event_description;
                    var event_instance_location = event_row_data.event_location;
                    var event_instance_start_date = event_row_data.start_date;
                    var event_instance_start_time = event_row_data.start_time;

                    var eventImagesHandle = null;

                    var startDateInstance = dom_date_time_wrapper.find('input.start-date').val(start_date).datepicker({
                        format: 'yyyy-mm-dd',
                        container: dom_lightbox_inner.find('.the-details')
                    });
                    var startTimeInstance = dom_date_time_wrapper.find('input.start-time').val(start_time).timepicker({
                        showMeridian: false
                    });

                    var eventIamgesClass = function () {

                        var dom_event_images_root = $('.event-images-container', dom_lightbox_inner);
                        var dom_grid_wrapper = $('.tt-grid-wrapper', dom_event_images_root);
                        var dom_list_wrapper = $('ul.tt-grid', dom_event_images_root);
                        var dom_previous = $('.previous-page', dom_event_images_root);
                        var dom_next = $('.next-page', dom_event_images_root);

                        var count_per_page = 5;

                        var pages = [];
                        var current_page_index = 0;
                        var pages_size = 0;
                        var keys_of_id = [];

                        event_row_data.event_images.forEach(function (post, i) {
                            keys_of_id[post.id] = i;
                            var page_index = parseInt(i / count_per_page);
                            post.html = '<a>' +
                                '<div class="event-image-item">' +
                                '<img class="delete-event-image-icon" data-id="' + post.id + '" src="assets/images/global-icons/close.png" title="delete this image"/>' +
                                '<img class="event-image" src="'+ post.media_url +'" />' +
                                '</div>' +
                                '</a>';

                            if (typeof pages[page_index] == 'undefined'){
                                pages[page_index] = [];
                            }
                            pages[page_index].push(post);
                            pages_size = page_index + 1;
                        });

                        var grid = dom_grid_wrapper.get(0).querySelector( '.tt-grid' ),
                            items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                            isAnimating = false;


                        var removeIamgeFromEventInFront = function (id) {
                            event_row_data.event_images.splice(keys_of_id[id], 1);
                            flipInitInstancesOfEvent(event_row_data.event_id);
                        }
                        var removeIamgeFromEvent = function (id) {
                            swal({
                                // title: "Are you sure?",
                                text: "Are you sure you want to delete this image?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    var ajax_data = {
                                        action: 'delete_event_image',
                                        id: id
                                    };
                                    $.ajax({
                                        url: ACTION_URL,
                                        data: ajax_data,
                                        success: function (res) {
                                            if (res.status == true){
                                                removeIamgeFromEventInFront(id);
                                            }
                                            else {
                                                alert('something went wrong');
                                            }
                                        },
                                        type: 'post',
                                        dataType: 'json'
                                    })
                                }
                            });
                        }

                        var addFuncToEl = function (el) {
                            el .find(':not(.tt-old)').find('.delete-event-image-icon').click(function () {
                                removeIamgeFromEvent($(this).data('id'));
                            })
                        }

                        // this is just a way we can test this. You would probably get your images with an AJAX request...

                        function loadNewSet(action, set ) {
                            if (isAnimating === true){
                                return ;
                            }
                            isAnimating = true;

                            var newImages = pages[current_page_index];
                            if (action === 'pagination'){
                                newImages = pages[set];
                                current_page_index = set;
                            }
                            items.forEach( function( el ) {
                                var itemChild = el.querySelector( 'a' );
                                // add class "tt-old" to the elements/images that are going to get removed
                                if( itemChild ) {
                                    classie.add( itemChild, 'tt-old' );
                                }
                            } );

                            // apply effect
                            setTimeout( function() {

                                // append new elements
                                if(newImages){

                                    [].forEach.call( newImages, function( el, i ) {
                                        items[ i ].innerHTML += el.html;
                                    } );
                                }

                                // add "effect" class to the grid
                                classie.add( grid, 'tt-effect-active' );


                                // wait that animations end
                                var onEndAnimFn = function() {
                                    // remove old elements
                                    items.forEach( function( el ) {
                                        // remove old elems
                                        var old = el.querySelector( '.tt-old' );
                                        if( old ) { el.removeChild( old ); }
                                        // remove class "tt-empty" from the empty items
                                        classie.remove( el, 'tt-empty' );
                                        // now apply that same class to the items that got no children (special case)
                                        if ( !el.hasChildNodes() ) {
                                            classie.add( el, 'tt-empty' );
                                        }
                                        addFuncToEl($(el));
                                    } );
                                    // remove the "effect" class
                                    classie.remove( grid, 'tt-effect-active' );
                                    isAnimating = false;
                                };

                                if( support ) {
                                    onAnimationEnd( items, items.length, onEndAnimFn );
                                }
                                else {
                                    onEndAnimFn.call();
                                }
                            }, 25 );
                        }

                        var previous = function () {
                            if (current_page_index > 0 && !isAnimating){
                                loadNewSet('pagination', current_page_index - 1);
                            }
                        }
                        var next = function () {
                            if (current_page_index < pages_size - 1 && !isAnimating){
                                loadNewSet('pagination', current_page_index + 1);
                            }
                        }

                        var flipInit = function() {
                            pages = [];
                            keys_of_id = [];
                            pages_size = 0;
                            event_row_data.event_images.forEach(function (post, i) {
                                keys_of_id[post.id] = i;
                                var page_index = parseInt(i / count_per_page);
                                if (typeof pages[page_index] == 'undefined'){
                                    pages[page_index] = [];
                                }
                                pages[page_index].push(post);
                                pages_size = page_index + 1;
                            });
                            isAnimating = false;
                            loadNewSet();
                            if (pages_size > 0){
                                dom_main_image.attr('src', event_row_data.event_images[0].media_url);
                            }
                            else {
                                dom_main_image.attr('src', default_event_image);
                            }
                        }

                        var accept_image_type = ['image/png', 'image/jpeg'];
                        var fileType = function (dataTransfer) {

                            var dropped_url = dataTransfer.getData('URL');

                            if(dropped_url != ''){
                                return dropped_url;
                            }

                            if (dataTransfer.files.length < 1){
                                return false;
                            }
                            var type = dataTransfer.files[0].type;

                            if (accept_image_type.indexOf(type) > -1){
                                return 'image';
                            }else if (accept_doc_types.indexOf(type) > -1){
                                return 'document';
                            } else {
                                return false;
                            }
                        };

                        var addImageToEvent = function (event_image_data) {
                            event_image_data.html = '<a>' +
                                '<div class="event-image-item">' +
                                '<img class="delete-event-image-icon" data-id="' + event_image_data.id + '" src="assets/images/global-icons/close.png" title="delete this image"/>' +
                                '<img class="event-image" src="'+ event_image_data.media_url +'" />' +
                                '</div>' +
                                '</a>';
                            keys_of_id[event_image_data.id] = event_row_data.event_images.length;
                            event_row_data.event_images.push(event_image_data);
                            if (pages_size > 0){
                                if (pages[pages_size - 1].length == count_per_page){
                                    pages[pages_size++] = [event_image_data];
                                }
                                else {
                                    pages[pages_size - 1].push(event_image_data);
                                }
                            }
                            else {
                                pages[pages_size++] = [event_image_data];
                            }
                        }

                        dom_event_images_root.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                        })
                            .on('dragover dragenter', function() {
                                dom_event_images_root.addClass('is-dragover');
                            })
                            .on('dragleave dragend drop', function() {
                                dom_event_images_root.removeClass('is-dragover');
                            })
                            .on('drop', function(e) {
                                var file_type = fileType(e.originalEvent.dataTransfer);

                                if (file_type === 'image'){
                                    dom_event_images_root.removeClass('is-dragover');
                                    var ajaxData = new FormData();
                                    var files = e.originalEvent.dataTransfer.files;
                                    ajaxData.append('event_image', files[0]);
                                    ajaxData.append('event_id', event_row_data.event_id);
                                    ajaxData.append('action', 'add_image_to_event');

                                    $.ajax({
                                        url: ACTION_URL,
                                        type: 'post',
                                        data: ajaxData,
                                        dataType: 'json',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        complete: function() {
                                        },
                                        success: function(res) {

                                            if (res.status === true){
                                                addImageToEvent(res.data);
                                                flipInitInstancesOfEvent(event_row_data.event_id);
                                            }
                                            else {
                                                alert('sorry something is wrong');
                                            }
                                        },
                                        error: function() {
                                            alert('sorry something is wrong');
                                            // Log the error, show an alert, whatever works for you
                                        }
                                    });

                                }
                                else {
                                    alert('this is unsupported format');
                                }
                            });
                        this.init = function () {
                            flipInit();
                            dom_previous.click(function () {
                                previous();
                            });
                            dom_next.click(function () {
                                next();
                            });
                        };
                        this.flipInit = flipInit;
                        this.loadNewSet = loadNewSet;
                        this.addImageToEvent = addImageToEvent;
                    }

                    eventImagesHandle = new eventIamgesClass();

                    var bindEvent = function () {
                        dom_close_btn.click(function () {
                            closeLightBox();
                        });
                        dom_title_wrapper.find('.edit-entry').click(function () {
                            dom_title_wrapper.addClass('edit-status');
                            dom_title_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                        });
                        dom_title_wrapper.find('.save-entry').click(function () {
                            event_instance_title = dom_title_wrapper.find('.editable-content').html();
                            event_row_data.event_title = event_instance_title;
                            dom_title_wrapper.removeClass('edit-status');
                            dom_title_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_title_wrapper.find('.back-to-origin').click(function () {
                            dom_title_wrapper.removeClass('edit-status');
                            dom_title_wrapper.find('.editable-content').html(event_instance_title);
                            dom_title_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_description_wrapper.find('.edit-entry').click(function () {
                            dom_description_wrapper.addClass('edit-status');
                            dom_description_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                        });
                        dom_description_wrapper.find('.save-entry').click(function () {
                            event_instance_description = dom_description_wrapper.find('.editable-content').html();
                            event_row_data.event_description = event_instance_description;
                            dom_description_wrapper.removeClass('edit-status');
                            dom_description_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_description_wrapper.find('.back-to-origin').click(function () {
                            dom_description_wrapper.removeClass('edit-status');
                            dom_description_wrapper.find('.editable-content').html(event_instance_description);
                            dom_description_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_location_wrapper.find('.edit-entry').click(function () {
                            dom_location_wrapper.addClass('edit-status');
                            dom_location_wrapper.find('.editable-content').attr('contenteditable', true).focus();
                        });
                        dom_location_wrapper.find('.save-entry').click(function () {
                            event_instance_location = dom_location_wrapper.find('.editable-content').html();
                            event_row_data.event_location = event_instance_location;
                            dom_location_wrapper.removeClass('edit-status');
                            dom_location_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_location_wrapper.find('.back-to-origin').click(function () {
                            dom_location_wrapper.removeClass('edit-status');
                            dom_location_wrapper.find('.editable-content').html(event_instance_location);
                            dom_location_wrapper.find('.editable-content').removeAttr('contenteditable');
                        });
                        dom_date_time_wrapper.find('.edit-entry').click(function () {
                            dom_date_time_wrapper.addClass('edit-status');
                        });
                        dom_date_time_wrapper.find('.save-entry').click(function () {
                            dom_date_time_wrapper.removeClass('edit-status');
                            dom_date_time_wrapper.find('.for-display').find('.start-date').html(event_instance_start_date);
                            dom_date_time_wrapper.find('.for-display').find('.start-time').html(event_instance_start_time);
                        });
                        dom_date_time_wrapper.find('.back-to-origin').click(function () {
                            dom_date_time_wrapper.removeClass('edit-status');
                        });
                        startDateInstance.on('changeDate', function (e) {
                            event_instance_start_date = startDateInstance.val();
                        });
                        startTimeInstance.on('changeTime.timepicker', function (e) {
                            event_instance_start_time = startTimeInstance.val() + ':00';
                        });
                        dom_lightbox_inner.find('.apply-to-instance').click(function () {
                            var ajax_data = {
                                data: {
                                    event_id: event_row_data.event_id,
                                    event_title: event_instance_title,
                                    event_description: event_instance_description,
                                    event_location: event_instance_location,
                                    start_date: event_instance_start_date,
                                    start_time: event_instance_start_time
                                }
                            }
                            switch (event_row_data.instance_type){
                                case 'recurring_instance':
                                    ajax_data.origin_start_date = event_row_data.start_date;
                                    ajax_data.action = 'reschedule_instance';
                                    break;
                                case 'single_instance':
                                    ajax_data.action = 'update_event';
                                    break;
                                case 'rescheduled_instance':
                                    ajax_data.id = event_row_data.id;
                                    ajax_data.action = 'update_reschduled_instance';
                                    break;
                            }
                            $.ajax({
                                url: ACTION_URL,
                                data: ajax_data,
                                success: function (res) {
                                    if (res.status == true){
                                        if (ajax_data.action == 'reschedule_instance'){
                                            event_row_data.id = res.data.rescheduled_row.id;
                                            event_row_data.instance_type = 'rescheduled_instance';
                                        }
                                        applyChangeToInstance(ajax_data.data);

                                        var now_date = new Date().yyyymmdd();
                                        if (event_instance_start_date <= now_date){
                                            self.removeFromDom();
                                            event_handles.splice(pos_in_parent, 1);
                                            current_index--;
                                            refresh();
                                        }
                                        else {
                                            refresh();
                                        }
                                        switch (ajax_data.action){
                                            case 'reschedule_instance':
                                                event_row_data.origin_event.cancelled_events.push(res.data.cancelled_row);
                                                event_row_data.origin_event.rescheduled_events.push(res.data.rescheduled_row);
                                                break;
                                            case 'update_event':
                                                event_row_data.origin_event.event_title = event_instance_title;
                                                event_row_data.origin_event.event_description = event_instance_description;
                                                event_row_data.origin_event.event_location = event_instance_location;
                                                event_row_data.origin_event.start_date = event_instance_start_date;
                                                event_row_data.origin_event.start_time = event_instance_start_time;
                                                break;
                                            case 'update_reschduled_instance':
                                                var my_row = (event_row_data.origin_event.rescheduled_events.filter(function (v) {
                                                    return v.id == event_row_data.id;
                                                }))[0];
                                                my_row.event_title = event_instance_title;
                                                my_row.event_description = event_instance_description;
                                                my_row.event_location = event_instance_location;
                                                my_row.start_date = event_instance_start_date;
                                                my_row.start_time = event_instance_start_time;
                                                break;
                                        }
                                        todaysEventsHandle.replaceInstances();
                                        todaysEventsHandle.refresh();
                                    }
                                    else {
                                        alert('something went wrong!');
                                    }
                                    closeLightBox();
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        });
                        dom_lightbox_inner.find('.apply-to-all-instance').click(function () {
                            var ajax_data = {
                                data: {
                                    event_id: event_row_data.event_id,
                                    event_title: event_instance_title,
                                    event_description: event_instance_description,
                                    event_location: event_instance_location,
                                    start_date: event_instance_start_date,
                                    start_time: event_instance_start_time
                                }
                            };
                            switch (event_row_data.instance_type){
                                case 'recurring_instance':
                                    ajax_data.origin_start_date = event_row_data.start_date;
                                    ajax_data.action = 'update_all_instances_of_event';
                                    ajax_data.doReschedule = event_row_data.start_date != event_instance_start_date ? 1 : 0;
                                    break;
                                case 'single_instance':
                                    ajax_data.action = 'update_all_instances_of_event';
                                    ajax_data.doReschedule = 0;
                                    break;
                                case 'rescheduled_instance':
                                    ajax_data.id = event_row_data.id;
                                    ajax_data.doReschedule = 2;
                                    ajax_data.action = 'update_all_instances_of_event';
                                    break;
                            }
                            $.ajax({
                                url: ACTION_URL,
                                data: ajax_data,
                                success: function (res) {
                                    if (res.status == true){

                                        if (ajax_data.action == 'update_all_instances_of_event'){
                                            updateAllInstancesOfEvent(event_row_data.event_id, ajax_data.data);
                                            if (ajax_data.doReschedule == 1){
                                                event_row_data.instance_type = 'rescheduled_instance';
                                                event_row_data.id = res.data.rescheduled_row.id;
                                            }
                                        }
                                        applyChangeToInstance(ajax_data.data);

                                        var now_date = new Date().yyyymmdd();
                                        if (event_instance_start_date <= now_date){
                                            self.removeFromDom();
                                            event_handles.splice(pos_in_parent, 1);
                                            current_index--;
                                            refresh();
                                        }
                                        else {
                                            refresh();
                                        }

                                        event_row_data.origin_event.event_title = event_instance_title;
                                        event_row_data.origin_event.event_description = event_instance_description;
                                        event_row_data.origin_event.event_location = event_instance_location;
                                        event_row_data.origin_event.start_time = event_instance_start_time;
                                        event_row_data.origin_event.rescheduled_events.forEach(function (rescheduled_event) {
                                            rescheduled_event.event_title = event_instance_title;
                                            rescheduled_event.event_description = event_instance_description;
                                            rescheduled_event.event_location = event_instance_location;
                                            rescheduled_event.start_time = event_instance_start_time;
                                        });

                                        switch (ajax_data.doReschedule){
                                            case 0:
                                                break;
                                            case 1:
                                                event_row_data.origin_event.cancelled_events.push(res.data.cancelled_row);
                                                event_row_data.origin_event.rescheduled_events.push(res.data.rescheduled_row);
                                                break;
                                            case 2:
                                                var my_row = (event_row_data.origin_event.rescheduled_events.filter(function (v) {
                                                    return v.id == event_row_data.id;
                                                }))[0];
                                                my_row.start_date = event_instance_start_date;
                                                break;
                                        }
                                        todaysEventsHandle.replaceInstances();
                                        todaysEventsHandle.refresh();

                                    }
                                    else {
                                        alert('something went wrong!');
                                    }
                                    closeLightBox();
                                },
                                type: 'post',
                                dataType: 'json'
                            });
                        });
                        dom_lightbox_inner.find('.cancel').click(function () {
                            closeLightBox();
                        })
                    }

                    var openLightbox = function () {

                        dom_lightbox_root.appendTo('body');
                        dom_open_from.addClass('open-lightbox-from-state');
                        dom_lightbox_inner.css('display', 'none');
                        var window_width = $(window).width();
                        setTimeout(function () {
                            dom_lightbox_root.css('display', 'block');
                            var pos = dom_open_from.offset();
                            pos.top = pos.top - $(window).scrollTop();
                            var width = dom_open_from.outerWidth();
                            var height = dom_open_from.outerHeight();
                            pos.right = $(window).width() - pos.left - width;
                            pos.bottom = $(window).height() - pos.top - height;
                            dom_lightbox_inner_wrapper.css(pos);

                            var to_css = {
                                top: '5vh',
                                right: '10vw',
                                left: '10vw',
                                bottom: '5vh',
                                fontSize: '10px',
                                padding: '50px'
                            };
                            if (window_width > 768 && window_width < 910){
                                to_css = {
                                    top: '2vh',
                                    right: '4vw',
                                    left: '4vw',
                                    bottom: '2vh',
                                    fontSize: '10px',
                                    padding: '20px'
                                }
                            }
                            else if (window_width < 768){
                                to_css = {
                                    top: '1vh',
                                    right: '2vw',
                                    left: '2vw',
                                    bottom: '1vh',
                                    fontSize: '10px',
                                    padding: '20px'
                                }
                            }

                            dom_lightbox_inner_wrapper.animate(to_css, {
                                duration: 500,
                                complete: function () {
                                    dom_lightbox_inner_wrapper.css('padding', 'auto');
                                    dom_lightbox_inner.css('display', 'block');
                                    dom_lightbox_root.addClass('open-status');
                                }
                            });
                        }, 100);
                    }
                    var closeLightBox = function () {
                        dom_lightbox_root.removeClass('open-status');
                        dom_lightbox_inner.css('display', 'none');
                        dom_lightbox_inner_wrapper.css('padding', '0px');
                        var pos = dom_open_from.offset();
                        pos.top = pos.top - $(window).scrollTop();
                        var width = dom_open_from.outerWidth();
                        var height = dom_open_from.outerHeight();
                        pos.right = $(window).width() - pos.left - width;
                        pos.bottom = $(window).height() - pos.top - height;
                        dom_lightbox_root.removeClass('open-status');
                        var css = $.extend(pos, {});
                        dom_lightbox_inner_wrapper.animate(
                            css,
                            {
                                duration: 500,
                                start: function () {
                                },
                                complete: function () {
                                    dom_lightbox_inner_wrapper.removeAttr('style');
                                    dom_lightbox_root.css('display', 'none');
                                    dom_open_from.removeClass('open-lightbox-from-state');
                                    dom_lightbox_root.detach();
                                }
                            }
                        );
                    }

                    this.openLightbox = openLightbox;
                    this.closeLightBox = closeLightBox;
                    this.addImageToEvent = eventImagesHandle.addImageToEvent;
                    this.init = function () {
                        dom_title_wrapper.find('.editable-content').html(event_instance_title);
                        dom_description_wrapper.find('.editable-content').html(event_instance_description);
                        dom_location_wrapper.find('.editable-content').html(event_instance_location);
                        dom_date_time_wrapper.find('.for-display').find('.start-date').html(event_instance_start_date);
                        dom_date_time_wrapper.find('.for-display').find('.start-time').html(event_instance_start_time);
                        eventImagesHandle.init();
                        bindEvent();
                    };
                    this.eventImagesHandle = eventImagesHandle;
                }

                editEventInstanceLightboxHandle = new editEventInstanceLightboxClass();

                var makeFavorite = function () {
                    var ajax_data = {
                        action: 'make_event_favorite',
                        event_id: event_row_data.event_id,
                        start_date: event_row_data.start_date,
                        is_rescheduled: 0
                    };
                    if (event_row_data.instance_type == 'rescheduled_instance'){
                        ajax_data.instance_id = event_row_data.id;
                        ajax_data.is_rescheduled = 1;
                    }
                    $.ajax({
                        url: ACTION_URL,
                        data: ajax_data,
                        success: function (res) {
                            if (res.status == true){
                                is_favorite = true;
                                event_row_data.is_favorite = true;
                                event_row_data.favorite_row = res.data;
                                dom_favorite.addClass('favorited');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                }
                var makeUnFavorite = function () {
                    var ajax_data = {
                        action: 'make_event_unfavorite',
                        id: event_row_data.favorite_row.id
                    };
                    $.ajax({
                        url: ACTION_URL,
                        data: ajax_data,
                        success: function (res) {
                            if (res.status == true){
                                is_favorite = false;
                                event_row_data.is_favorite = false;
                                event_row_data.favorite_row = null;
                                dom_favorite.removeClass('favorited');
                            }
                            else {
                                alert('something went wrong');
                            }
                        },
                        type: 'post',
                        dataType: 'json'
                    })
                }
                var cancelEventInstance = function (e) {
                    swal({
                        title: "Are you sure?",
                        icon: "warning",
                        dangerMode: true,
                        buttons: {
                            cancel: "Cancel",
                            trashPost: {
                                text: "Skep This Day",
                                value: "trash_post",
                                className: 'swal-button--danger'
                            },
                            removeSeries: {
                                text: 'Delete This Event',
                                value: 'remove_series',
                                className: 'swal-button--danger'
                            },
                        },
                    }).then(function(value){
                        switch (value) {
                            case "trash_post":
                                var ajax_data = {};
                                if (event_row_data.instance_type == 'rescheduled_instance'){
                                    ajax_data.action = 'delete_rescheduled_event';
                                    ajax_data.id = event_row_data.id;
                                    $.ajax({
                                        url: ACTION_URL,
                                        type: 'post',
                                        data: ajax_data,
                                        dataType: 'json',
                                        success: function (res) {
                                            if (res.status){

                                                var index = pos_in_parent;
                                                self.removeFromDom();
                                                event_handles.splice(index, 1);
                                                current_index--;
                                                refresh();
                                                var pos_tp = 0;
                                                event_row_data.origin_event.rescheduled_events.forEach(function (rescheduled_event, i) {
                                                    if (rescheduled_event.id == event_row_data.id){
                                                        pos_tp = i;
                                                    }
                                                });
                                                event_row_data.origin_event.rescheduled_events.splice(pos_tp, 1);
                                                todaysEventsHandle.replaceInstances();
                                                todaysEventsHandle.refresh();
                                            }
                                        }
                                    });
                                }
                                else if (event_row_data.instance_type == 'single_instance'){
                                    ajax_data.action = 'delete_event';
                                    ajax_data.id = event_row_data.event_id;
                                    $.ajax({
                                        url: ACTION_URL,
                                        type: 'post',
                                        data: ajax_data,
                                        dataType: 'json',
                                        success: function (res) {
                                            if (res.status){

                                                var index = pos_in_parent;
                                                self.removeFromDom();
                                                event_handles.splice(index, 1);
                                                current_index--;
                                                refresh();

                                                var pos_tp = 0;
                                                for (var i = 0; i < data.length; i ++){
                                                    if (data[i].id == event_row_data.event_id){
                                                        pos_tp = i;
                                                        break;
                                                    }
                                                }
                                                data.splice(pos_tp, 1);
                                                todaysEventsHandle.replaceInstances();
                                                todaysEventsHandle.refresh();
                                            }
                                        }
                                    });
                                }
                                else {
                                    ajax_data.action = 'cancel_event_instance';
                                    ajax_data.data = {event_id: event_row_data.event_id, start_date: start_date, start_time: start_time};
                                    $.ajax({
                                        url: ACTION_URL,
                                        type: 'post',
                                        data: ajax_data,
                                        dataType: 'json',
                                        success: function (res) {
                                            if (res.status){

                                                var index = pos_in_parent;
                                                self.removeFromDom();
                                                event_handles.splice(index, 1);
                                                current_index--;
                                                refresh();

                                                event_row_data.origin_event.cancelled_events.push(res.data);
                                                todaysEventsHandle.replaceInstances();
                                                todaysEventsHandle.refresh();
                                            }
                                        }
                                    });
                                }
                                break;
                            case "remove_series":
                                var ajax_data = {};
                                ajax_data.action = 'delete_event';
                                ajax_data.id = event_row_data.event_id;
                                $.ajax({
                                    url: ACTION_URL,
                                    type: 'post',
                                    data: ajax_data,
                                    dataType: 'json',
                                    success: function (res) {
                                        if (res.status){
                                            removeAllInstancesOfEvent(event_row_data.event_id);
                                            refresh();
                                            var pos_tp = 0;
                                            for (var i = 0; i < data.length; i ++){
                                                if (data[i].id == event_row_data.event_id){
                                                    pos_tp = i;
                                                    break;
                                                }
                                            }
                                            data.splice(pos_tp, 1);
                                            todaysEventsHandle.replaceInstances();
                                            todaysEventsHandle.refresh();
                                        }
                                    }
                                });
                                break;
                            default:
                                break;
                        }
                    });
                }

                var bindEvent = function () {
                    dom_event_instance.click(function () {
                        editEventInstanceLightboxHandle.openLightbox();
                    });
                    dom_favorite.click(function () {
                        if (is_favorite){
                            makeUnFavorite();
                        }
                        else {
                            makeFavorite();
                        }
                    });
                }

                this.addImageToEvent = editEventInstanceLightboxHandle.addImageToEvent;
                this.init = function () {

                    dom_occurring_date.html(stringDateToFormat(event_row_data.start_date, event_row_data.start_time));
                    dom_event_title.html(event_row_data['event_title']);
                    dom_cancel_event.on('click', function (e) {
                        e.preventDefault();
                        cancelEventInstance(e);
                    });
                    bindEvent();
                    editEventInstanceLightboxHandle.init();
                }
                this.showOnEventsList = function () {
                    dom_event_row_root.appendTo(dom_event_list);
                }
                this.removeFromDom = function () {
                    dom_event_row_root.detach();
                }
                this.getDateTime = function () {
                    return new Date(start_date + ' ' + start_time);
                }
                this.updatePosInParent = function (new_pos) {
                    pos_in_parent = new_pos;
                }
                this.getPosInParent = function () {
                    return pos_in_parent;
                }
                this.getEventId = function () {
                    return event_row_data.event_id;
                }
                this.applyChangeToInstance = applyChangeToInstance;
                this.editEventInstanceLightboxHandle = editEventInstanceLightboxHandle;
                this.dom_event_row_root = function(){ return dom_event_row_root; };
            }

            var getInstancesHandlesOfEvent = function (event_data) {
                var now_date = new Date().yyyymmdd();
                var now_time = new Date().toTimeString().split(' ')[0];

                var eventInstancesHanldes = [];

                var i;

                if (typeof event_data.rescheduled_events != 'undefined'){
                    event_data.rescheduled_events.forEach(function (rescheduled_event) {
                        rescheduled_event.instance_type = 'rescheduled_instance';
                        rescheduled_event.event_images = event_data.event_images;
                        rescheduled_event.origin_event = event_data;
                        var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                            return event_favorite.is_rescheduled == true && event_favorite.instance_id == rescheduled_event.id;
                        });
                        rescheduled_event.is_favorite = favorite_rows.length;
                        rescheduled_event.favorite_row = rescheduled_event.is_favorite ? favorite_rows[0] : null;

                        if (rescheduled_event.start_date > now_date){
                            var newHanlde = new eventRowClass(rescheduled_event);
                            newHanlde.init();
                            eventInstancesHanldes.push(newHanlde);
                        }
                    });
                }

                if (event_data.is_recurring === '1'){
                    var step = 24 * 60 * 60 * 1000;

                    switch (event_data.recurring_pattern.recurring_type){
                        case 'd':
                            step *= (1 * event_data.recurring_pattern.separation_count);
                            break;
                        case 'w':
                            step *= (7 * event_data.recurring_pattern.separation_count);
                            break;
                        default:
                            break;
                    }
                    var distance = Math.ceil((new Date(now_date) - new Date(event_data.start_date)) / step);
                    if ( distance < 0 ) { distance = 0; }
                    var event_flag = 0;
                    for ( i = 0; i < 365; i ++){
                        // var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (distance + i)).yyyymmdd();
                        var instance_date = new Date(event_data.start_date.stringToDate().getTime() + step * (0 + i)).yyyymmdd();
                        var instance_day = new Date(instance_date).getDay();
                        var isin = false;
                        for(var j = 0; j < event_data.cancelled_events.length; j++){
                            if (event_data.cancelled_events[j].start_date == instance_date){ isin = true; break; }
                        }
                        if (!isin && event_data.recurring_pattern.days_of_week[parseInt(instance_day)] == '1'){
                            var event_instance = {
                                origin_event: event_data,
                                event_id: event_data.id,
                                event_title: event_data.event_title,
                                event_description: event_data.event_description,
                                event_location: event_data.event_location,
                                start_date: instance_date,
                                start_time: event_data.start_time,
                                instance_type: 'recurring_instance',
                                event_images: event_data.event_images
                            }


                            var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                                return event_favorite.is_rescheduled == 0 && event_favorite.start_date == instance_date;
                            });
                            event_instance.is_favorite = favorite_rows.length;
                            event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                            if (!event_flag && event_instance.start_date > now_date){
                                event_flag = 1;
                                var newHandle = new eventRowClass(event_instance);
                                newHandle.init();
                                eventInstancesHanldes.push(newHandle);
                                break;
                            }
                        }
                    }
                }
                else {
                    var event_instance = {
                        origin_event: event_data,
                        event_id: event_data.id,
                        event_title: event_data.event_title,
                        event_description: event_data.event_description,
                        event_location: event_data.event_location,
                        start_date: event_data.start_date,
                        start_time: event_data.start_time,
                        instance_type: 'single_instance',
                        event_images: event_data.event_images
                    };

                    var favorite_rows = event_data.event_favorites.filter(function (event_favorite) {
                        return event_favorite.is_rescheduled == 0;
                    });
                    event_instance.is_favorite = favorite_rows.length;
                    event_instance.favorite_row = event_instance.is_favorite ? favorite_rows[0] : null;

                    if (event_instance.start_date > now_date){
                        var newHandle = new eventRowClass(event_instance);
                        newHandle.init();
                        eventInstancesHanldes.push(newHandle);
                    }
                }
                return eventInstancesHanldes;
            }

            events.forEach(function (event_data) {
                var instanceHandlesOfEvents = getInstancesHandlesOfEvent(event_data);
                instanceHandlesOfEvents = instanceHandlesOfEvents.sort(function (a, b) {
                    if (a.getDateTime().getTime() > b.getDateTime().getTime()){
                        return 1;
                    }
                    else if (a.getDateTime().getTime() == b.getDateTime().getTime()){
                        return 0;
                    }
                    return -1;
                });
                if (instanceHandlesOfEvents.length > 0){
                    event_handles = event_handles.concat(instanceHandlesOfEvents[0]);
                }
            });

            var updateAllInstancesOfEvent = function (event_id, data) {
                var handles_to_update = event_handles.filter(function (event_handle) {
                    return event_handle.getEventId() == event_id;
                });
                data.start_date = null;
                handles_to_update.forEach(function (handle_to_update) {
                    handle_to_update.applyChangeToInstance(data);
                });
            };
            var removeAllInstancesOfEvent = function (event_id) {
                var handles_to_remove = event_handles.filter(function (event_handle) {
                    return event_handle.getEventId() == event_id;
                });
                var minus = 0;
                handles_to_remove.forEach(function(handle_to_remove){
                    handle_to_remove.removeFromDom();
                    var pos = handle_to_remove.getPosInParent() - minus;
                    event_handles.splice(pos, 1);
                    minus++;
                });
            }

            var flipInitInstancesOfEvent = function (event_id) {
                var handles_to_update = event_handles.filter(function (event_handle) {
                    return event_handle.getEventId() == event_id;
                });
                handles_to_update.forEach(function (handle_to_update) {
                    handle_to_update.editEventInstanceLightboxHandle.eventImagesHandle.flipInit();
                });
            };

            var viewMore = function () {
                var next_page_index = current_index + 5;
                for (var i = current_index; i < next_page_index && i < event_handles.length; i ++){
                    event_handles[i].showOnEventsList();
                    current_index++;
                }
            }

            var refresh = function () {
                var i;
                for ( i = 0; i < current_index + 2 && i < event_handles.length; i ++){
                    event_handles[i].removeFromDom();
                }
                event_handles = event_handles.sort(function (a, b) {
                    if (parseInt(a.getDateTime().getTime()) > parseInt(b.getDateTime().getTime())){
                        return 1;
                    }
                    else if(parseInt(a.getDateTime().getTime()) == parseInt(b.getDateTime().getTime())){
                        return 0;
                    }
                    return -1;
                });

                for (i = 0; i < event_handles.length; i ++){
                    event_handles[i].updatePosInParent(i);
                }

                for (i = 0; i < current_index && i < event_handles.length; i ++){
                    event_handles[i].showOnEventsList();
                }
            }

            var addInstanceRow = function (event_row_data) {
                var newHandle = new eventRowClass(event_row_data);
                newHandle.init();
                event_handles.push(newHandle);
                current_index++;
                return newHandle;
            }

            var createEvent = function (event_data, callbackOnFinish) {

                var copyObject = jQuery.extend(true, {}, event_data);

                if (copyObject.is_recurring){
                    copyObject.recurring_pattern_data.additional_event_dates.forEach(function (date, i) {
                        copyObject.recurring_pattern_data.additional_event_dates[i] = date.yyyymmdd();
                    });

                    copyObject.recurring_pattern_data.omit_dates.forEach(function (date, i) {
                        copyObject.recurring_pattern_data.omit_dates[i] = date.yyyymmdd();
                    });
                }

                var ajax_data = {
                    action: 'create_event',
                    data: copyObject
                }
                $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajax_data,
                    dataType: 'json',
                    success: function (res) {
                        if (res.status){
                            data.push(res.data)
                            var instanceHandlesOfEvent = getInstancesHandlesOfEvent(res.data);
                            instanceHandlesOfEvent = instanceHandlesOfEvent.sort(function (a, b) {
                                return parseInt(a.getDateTime().getTime()) - parseInt(b.getDateTime().getTime());
                            });
                            if (instanceHandlesOfEvent.length > 0){
                                event_handles = event_handles.concat(instanceHandlesOfEvent[0]);
                                current_index += 1;
                            }
                            refresh();
                            todaysEventsHandle.replaceInstances();
                            todaysEventsHandle.refresh();
                        }
                        callbackOnFinish();
                    }
                });
            }
            var editEventLightboxClass = function () {
                var dom_lightbox_root = $('.create-event-lightbox');
                var dom_lightbox_inner_wrapper = $('.create-event-lightbox-inner-wrapper', dom_lightbox_root);
                var dom_lightbox_inner = $('.create-event-lightbox-inner', dom_lightbox_inner_wrapper);
                var dom_close_btn = $('.close', dom_lightbox_root);
                var dom_form = $('form', dom_lightbox_inner);
                var dom_is_recurring = $('#is_recurring', dom_form);
                var dom_create_btn = $('.create-event-btn', dom_form);
                var dom_title = $('input[name=event_title]', dom_form);
                var dom_description = $('textarea[name=event_description]', dom_form);
                var dom_location = $('input[name=location]', dom_form);

                var event_title = '';
                var event_description = '';
                var event_location = '';
                var start_date = '';
                var start_time = '';
                var now = new Date();

                var is_recurring = true;

                var bindEvent = function () {

                    dom_title.change(function () {
                        event_title = dom_title.val();
                    });
                    dom_description.change(function () {
                        event_description = dom_description.val();
                    });
                    dom_location.change(function () {
                        event_location = dom_location.val();
                    });
                }

                var tp_dom_open_from = null;

                var startDateHandle = null;
                var startTimeHanlde = null;
                var recurringPatternHanlde = null;

                startDateHandle = dom_form.find('input[name="start_date"]').datepicker({
                    container: dom_lightbox_inner_wrapper
                });

                startDateHandle.on('changeDate', function (e) {
                    start_date = e.date;
                    recurringPatternHanlde.refresh();
                });

                startTimeHanlde =  dom_form.find('input[name=start_time]').timepicker({
                    container: dom_lightbox_inner_wrapper,
                    defaultTime: now.getHours() + ':' + now.getMinutes(),
                    showMeridian: false
                });
                startTimeHanlde.on('changeTime.timepicker', function (e) {
                    start_time = e.time;
                });

                var recurringPatternClass = function () {
                    var dom_recurring_pattern_root = $('.recurring-pattern', dom_form);
                    var dom_recurring_type = $('select[name=recurring_type]', dom_recurring_pattern_root);

                    var dailyRecurringHandle = null;
                    var weeklyRecurringHandle = null;

                    var dailyRecurringClass = function () {
                        var dom_daily_recurring_root = $('.daily-recurring-wrapper', dom_recurring_pattern_root);
                        var dom_separation_count = $('.separation-count-wrapper input', dom_daily_recurring_root);

                        var separation_count = dom_separation_count.val();

                        var dates_on_display = [];
                        var additional_event_dates = [];
                        var omit_dates = [];
                        var days_of_week = '1111111';
                        var event_dates_on_display = [];
                        var is_month_changed = true;
                        var is_days_of_week_changed = false;

                        function containsObject(obj, list) {
                            var x;
                            for (x in list) {
                                if (list[x].getTime() === obj.getTime()) {
                                    return (x * 1 + 1);
                                }
                            }
                            return false;
                        }

                        var isFilledPosWithBasic = function (date) {
                            if (date.getTime() < start_date.getTime()){
                                return false;
                            }

                            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                            var firstDate = start_date;
                            var secondDate = date;
                            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

                            if (diffDays % separation_count > 0){ return false; }
                            if (days_of_week[date.getDay()] == '0'){ return false; }

                            return true;
                        };

                        var isEventDate = function(date) {

                            if (date.getTime() < start_date.getTime()){
                                return false;
                            }
                            if (containsObject(date, additional_event_dates)){ return true; }

                            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                            var firstDate = start_date;
                            var secondDate = date;
                            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

                            if (diffDays % separation_count > 0){ return false; }
                            if (containsObject(date, omit_dates)){ return false; }
                            if (days_of_week[date.getDay()] == '0'){ return false; }

                            return true;
                        }

                        var dailyRecurringInstance = dom_daily_recurring_root.find('.daily-recurring').datepicker({
                            toggleActive: true,
                            multidate: true,
                            beforeShowDay: function (date) {
                                if (!containsObject(date, dates_on_display)){
                                    dates_on_display.push(date);
                                }
                                return true;
                            }
                        });

                        var refreshEventDates = function () {
                            event_dates_on_display = [];
                            var current_month = dates_on_display[20].getMonth();
                            var i, pos;

                            additional_event_dates.forEach(function (date) {
                                if (isFilledPosWithBasic(date)){
                                    pos = containsObject(date, additional_event_dates);
                                    additional_event_dates.splice(pos - 1, 1);
                                }
                            });

                            omit_dates.forEach(function (date) {
                                if (!isFilledPosWithBasic(date)){
                                    pos = containsObject(date, omit_dates);
                                    omit_dates.splice(pos - 1, 1);
                                }
                            });


                            for(i = 0; i < 42; i ++){
                                if (i > 20 && dates_on_display[i].getMonth() != current_month){ break; }
                                if(isEventDate(dates_on_display[i])){
                                    event_dates_on_display.push((dates_on_display[i]));
                                }
                            }
                            dailyRecurringInstance.datepicker('setDates', event_dates_on_display);
                        }

                        dailyRecurringInstance.on('changeMonth', function (e) {
                            dates_on_display = [];
                            setTimeout(function () {
                                is_month_changed = true;
                                refreshEventDates();
                            }, 100);
                        });

                        var get_changed_date = function (new_dates, old_dates) {

                            if (new_dates.length > old_dates.length){
                                for (var i = 0; i < new_dates.length; i ++){
                                    if (!containsObject(new_dates[i], old_dates)){
                                        return new_dates[i];
                                    }
                                }
                            }
                            else {
                                for (var i = 0; i < old_dates.length; i ++){
                                    if (!containsObject(old_dates[i], new_dates)){
                                        return old_dates[i];
                                    }
                                }
                            }
                            return false;
                        }

                        dailyRecurringInstance.on('changeDate', function (e) {
                            if (is_month_changed || is_days_of_week_changed){
                                is_month_changed = false;
                                is_days_of_week_changed = false;
                            }
                            else {

                                var changed_date = get_changed_date(e.dates, event_dates_on_display);
                                if (!changed_date){ return ; }
                                if (changed_date.getTime() < start_date.getTime()){
                                    alert('sorry, this is less than start date');
                                    dailyRecurringInstance.datepicker('setDates', event_dates_on_display);
                                    return ;
                                }
                                if (event_dates_on_display.length < e.dates.length){
                                    var pos = containsObject(changed_date, omit_dates);
                                    if (pos){
                                        omit_dates.splice(pos - 1, 1);
                                    }
                                    else {
                                        additional_event_dates.push(changed_date);
                                    }
                                    event_dates_on_display.push(changed_date);
                                }
                                else {
                                    var pos = containsObject(changed_date, additional_event_dates);
                                    if(pos){
                                        additional_event_dates.splice(pos - 1, 1);
                                    }
                                    else {
                                        omit_dates.push(changed_date);
                                    }
                                    pos = containsObject(changed_date, event_dates_on_display);
                                    event_dates_on_display.splice(pos - 1, 1);
                                }
                            }
                        })
                        var dom_week_row = $('.datepicker-days table thead tr:last-child', dom_daily_recurring_root);

                        $('th', dom_week_row).each(function (i) {
                            $(this).click(function () {
                                $(this).toggleClass('omit');
                                if ($(this).hasClass('omit')){
                                    days_of_week = days_of_week.replaceAt(i, '0');
                                    if (days_of_week == '0000000'){
                                        alert('Sorry, Please Select At Least One Day');
                                    }
                                }
                                else {
                                    days_of_week = days_of_week.replaceAt(i, '1');
                                }
                                is_days_of_week_changed = true;
                                refreshEventDates();
                            });
                        });

                        this.show = function () {
                            dom_daily_recurring_root.show();
                        }
                        this.hide = function () {
                            dom_daily_recurring_root.hide();
                        }
                        this.refresh = function () {
                            refreshEventDates();
                        }
                        this.getDailyRecurringData = function () {
                            return {
                                recurring_type: 'd',
                                additional_event_dates: additional_event_dates,
                                omit_dates: omit_dates,
                                days_of_week: days_of_week,
                                separation_count: separation_count
                            }
                        }
                        this.init = function () {
                            dom_separation_count.bind('keyup mouseup change', function () {
                                separation_count = dom_separation_count.val();
                                refreshEventDates();
                            });
                            refreshEventDates();
                        }
                    }

                    dailyRecurringHandle = new dailyRecurringClass();

                    var weeklyRecurringClass = function () {

                        var dom_weekly_recurring_root = $('.weekly-recurring-wrapper', dom_recurring_pattern_root);
                        var dom_separation_count = $('.separation-count-wrapper input', dom_weekly_recurring_root);

                        var separation_count = dom_separation_count.val();

                        var dates_on_display = [];
                        var additional_event_dates = [];
                        var omit_dates = [];
                        var event_dates_on_display = [];
                        var is_month_changed = true;

                        function containsObject(obj, list) {
                            var x;
                            for (x in list) {
                                if (list[x].getTime() === obj.getTime()) {
                                    return (x * 1 + 1);
                                }
                            }
                            return false;
                        }

                        var isFilledPosWithBasic = function (date) {
                            if (date.getTime() < start_date.getTime()){
                                return false;
                            }

                            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                            var firstDate = start_date;
                            var secondDate = date;
                            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

                            if (diffDays % (separation_count * 7) > 0){ return false; }

                            return true;
                        };

                        var isEventDate = function(date) {

                            if (date.getTime() < start_date.getTime()){
                                return false;
                            }
                            if (containsObject(date, additional_event_dates)){ return true; }

                            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                            var firstDate = start_date;
                            var secondDate = date;
                            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
                            if (diffDays % (separation_count * 7) > 0){ return false; }
                            if (containsObject(date, omit_dates)){ return false; }

                            return true;
                        }

                        var weeklyRecurringInstance = dom_weekly_recurring_root.find('.weekly-recurring').datepicker({
                            toggleActive: true,
                            multidate: true,
                            beforeShowDay: function (date) {
                                if (!containsObject(date, dates_on_display)){
                                    dates_on_display.push(date);
                                }
                                return true;
                            }
                        });

                        var refreshEventDates = function () {
                            event_dates_on_display = [];
                            var current_month = dates_on_display[20].getMonth();
                            var i, pos;

                            additional_event_dates.forEach(function (date) {
                                if (isFilledPosWithBasic(date)){
                                    pos = containsObject(date, additional_event_dates);
                                    additional_event_dates.splice(pos - 1, 1);
                                }
                            });

                            omit_dates.forEach(function (date) {
                                if (!isFilledPosWithBasic(date)){
                                    pos = containsObject(date, omit_dates);
                                    omit_dates.splice(pos - 1, 1);
                                }
                            });


                            for(i = 0; i < 42; i ++){
                                if (i > 20 && dates_on_display[i].getMonth() != current_month){ break; }

                                if(isEventDate(dates_on_display[i])){
                                    event_dates_on_display.push((dates_on_display[i]));
                                }
                            }
                            weeklyRecurringInstance.datepicker('setDates', event_dates_on_display);
                        }

                        weeklyRecurringInstance.on('changeMonth', function (e) {
                            dates_on_display = [];
                            setTimeout(function () {
                                is_month_changed = true;
                                refreshEventDates();
                            }, 100);
                        });

                        var get_changed_date = function (new_dates, old_dates) {

                            if (new_dates.length > old_dates.length){
                                for (var i = 0; i < new_dates.length; i ++){
                                    if (!containsObject(new_dates[i], old_dates)){
                                        return new_dates[i];
                                    }
                                }
                            }
                            else {
                                for (var i = 0; i < old_dates.length; i ++){
                                    if (!containsObject(old_dates[i], new_dates)){
                                        return old_dates[i];
                                    }
                                }
                            }
                            return false;
                        }

                        weeklyRecurringInstance.on('changeDate', function (e) {
                            if (is_month_changed){
                                is_month_changed = false;
                            }
                            else {

                                var changed_date = get_changed_date(e.dates, event_dates_on_display);
                                if (!changed_date){ return ; }
                                if (changed_date.getTime() < start_date.getTime()){
                                    alert('sorry, this is less than start date');
                                    weeklyRecurringInstance.datepicker('setDates', event_dates_on_display);
                                    return ;
                                }
                                if (event_dates_on_display.length < e.dates.length){
                                    var pos = containsObject(changed_date, omit_dates);
                                    if (pos){
                                        omit_dates.splice(pos - 1, 1);
                                    }
                                    else {
                                        additional_event_dates.push(changed_date);
                                    }
                                    event_dates_on_display.push(changed_date);
                                }
                                else {
                                    var pos = containsObject(changed_date, additional_event_dates);
                                    if(pos){
                                        additional_event_dates.splice(pos - 1, 1);
                                    }
                                    else {
                                        omit_dates.push(changed_date);
                                    }
                                    pos = containsObject(changed_date, event_dates_on_display);
                                    event_dates_on_display.splice(pos - 1, 1);
                                }
                            }
                        })

                        this.show = function () {
                            dom_weekly_recurring_root.show();
                        }
                        this.hide = function () {
                            dom_weekly_recurring_root.hide();
                        }
                        this.refresh = function () {
                            refreshEventDates();
                        }
                        this.getWeeklyRecurringData = function () {
                            return {
                                recurring_type: 'w',
                                additional_event_dates: additional_event_dates,
                                omit_dates: omit_dates,
                                separation_count: separation_count,
                                is_recurring: true
                            }
                        }
                        this.init = function () {
                            dom_separation_count.bind('keyup mouseup change', function () {
                                separation_count = dom_separation_count.val();
                                refreshEventDates();
                            });
                            refreshEventDates();
                        }
                    }

                    weeklyRecurringHandle = new weeklyRecurringClass();
                    weeklyRecurringHandle.hide();

                    this.hide = function(){ dom_recurring_pattern_root.hide(); }
                    this.show = function(){ dom_recurring_pattern_root.show(); }
                    this.refresh = function () {
                        dailyRecurringHandle.refresh();
                        weeklyRecurringHandle.refresh();
                    }

                    this.getRecurringData = function () {
                        switch (dom_recurring_type.val()){
                            case 'd':
                                return dailyRecurringHandle.getDailyRecurringData();
                                break;
                            case 'w':
                                return weeklyRecurringHandle.getWeeklyRecurringData();
                                break;
                        }
                        return false;
                    }

                    this.init = function () {
                        dailyRecurringHandle.init();
                        weeklyRecurringHandle.init();
                        dom_recurring_type.change(function () {
                            switch (dom_recurring_type.val()){
                                case 'd':
                                    dailyRecurringHandle.show();
                                    weeklyRecurringHandle.hide();
                                    break;
                                case 'w':
                                    dailyRecurringHandle.hide();
                                    weeklyRecurringHandle.show();
                                    break;
                            }

                        })
                    }
                }
                recurringPatternHanlde = new recurringPatternClass();

                var openLightbox = function (dom_open_from) {
                    tp_dom_open_from = dom_open_from;
                    dom_open_from.addClass('open-lightbox-from-state');
                    dom_lightbox_inner.css('display', 'none');

                    var window_width = $(window).width();

                    setTimeout(function () {
                        dom_lightbox_root.css('display', 'block');
                        var pos = dom_open_from.offset();
                        pos.top = pos.top - $(window).scrollTop();
                        var width = dom_open_from.outerWidth();
                        var height = dom_open_from.outerHeight();
                        pos.right = $(window).width() - pos.left - width;
                        pos.bottom = $(window).height() - pos.top - height;
                        dom_lightbox_inner_wrapper.css(pos);

                        var to_css = {
                            top: '5vh',
                            right: '10vw',
                            left: '10vw',
                            bottom: '5vh',
                            fontSize: '10px',
                            padding: '50px'
                        };
                        if (window_width > 768 && window_width < 910){
                            to_css = {
                                top: '2vh',
                                right: '4vw',
                                left: '4vw',
                                bottom: '2vh',
                                fontSize: '10px',
                                padding: '20px'
                            }
                        }
                        else if (window_width < 768){
                            to_css = {
                                top: '1vh',
                                right: '2vw',
                                left: '2vw',
                                bottom: '1vh',
                                fontSize: '10px',
                                padding: '20px'
                            }
                        }

                        dom_lightbox_inner_wrapper.animate(to_css, {
                            duration: 500,
                            complete: function () {
                                dom_lightbox_root.addClass('open-status');
                                dom_lightbox_inner_wrapper.css('padding', 'auto');
                                dom_lightbox_inner.css('display', 'block');
                                startTimeHanlde.timepicker('setTime', new Date().getHours() + ':' + new Date().getMinutes());
                                dom_lightbox_root.addClass('open-status');
                            }
                        });
                    }, 100);
                }
                var closeLightBox = function (dom_open_from) {
                    dom_lightbox_root.removeClass('open-status');
                    dom_lightbox_inner.css('display', 'none');
                    dom_lightbox_inner_wrapper.css('padding', '0px');
                    var pos = dom_open_from.offset();
                    pos.top = pos.top - $(window).scrollTop();
                    var width = dom_open_from.outerWidth();
                    var height = dom_open_from.outerHeight();
                    pos.right = $(window).width() - pos.left - width;

                    pos.bottom = $(window).height() - pos.top - height;
                    var css = $.extend(pos, {});
                    dom_lightbox_inner_wrapper.animate(
                        css,
                        {
                            duration: 500,
                            start: function () {
                            },
                            complete: function () {
                                dom_lightbox_inner_wrapper.removeAttr('style');
                                dom_lightbox_root.css('display', 'none');
                                dom_open_from.removeClass('open-lightbox-from-state');
                            }
                        }
                    );
                }

                this.openLightbox = openLightbox;
                this.closeLightBox = closeLightBox;

                this.init = function () {

                    dom_close_btn.click(function () {
                        closeLightBox(tp_dom_open_from);
                    });
                    startDateHandle.datepicker('setDate', new Date(now.getYear() + 1900, now.getMonth(), now.getDate()));
                    dom_is_recurring.change(function () {
                        is_recurring = dom_is_recurring.prop('checked');
                        is_recurring ? recurringPatternHanlde.show() : recurringPatternHanlde.hide();
                    });
                    recurringPatternHanlde.init();
                    bindEvent();
                    dom_create_btn.click(function () {
                        var event_data = {
                            event_title: event_title,
                            event_description: event_description,
                            event_location: event_location,
                            start_date: start_date.yyyymmdd(),
                            start_time: start_time.value + ':' + '00',
                            is_recurring: is_recurring
                        }
                        if (is_recurring){
                            event_data.recurring_pattern_data = recurringPatternHanlde.getRecurringData();
                        }
                        createEvent(event_data, function(){ closeLightBox(tp_dom_open_from); });
                    });
                }
            }

            editEventLightboxHandle = new editEventLightboxClass();
            this.addInstanceRow = addInstanceRow;
            this.refresh = refresh;
            this.init = function () {
                editEventLightboxHandle.init();
                dom_add_event.click(function () {
                    editEventLightboxHandle.openLightbox(dom_add_event);
                });
                refresh();
                dom_view_more.click(function () {
                    viewMore();
                });
                viewMore();
            }
        }
        personalEventsHandle = new personalEventsClass();

        var calendarForTodayClass = function () {
            var now_date = new Date().yyyymmdd();

            var start_date, end_date;
            var calendarInstance = $('#calendar_for_today').datepicker({
                todayHighlight: true,
                toggleActive: true,
                multidate: true
            });

            calendarInstance.on('changeDate', function (e) {
                var dates = calendarInstance.datepicker('getDates');
                switch (dates.length){
                    case 0:
                        start_date = end_date = now_date;
                        todaysEventsHandle.updateDateRangeToShow(start_date, end_date);
                        todaysEventsHandle.replaceInstances();
                        todaysEventsHandle.refresh();
                        break;
                    case 1:
                        start_date = end_date = dates[0].yyyymmdd();
                        todaysEventsHandle.updateDateRangeToShow(start_date, end_date);
                        todaysEventsHandle.replaceInstances();
                        todaysEventsHandle.refresh();
                        break;
                    case 2:
                        start_date = dates[0].yyyymmdd();
                        end_date = dates[1].yyyymmdd();
                        start_date = start_date > end_date ? [end_date, end_date = start_date][0] : start_date;
                        todaysEventsHandle.updateDateRangeToShow(start_date, end_date);
                        todaysEventsHandle.replaceInstances();
                        todaysEventsHandle.refresh();
                        break;
                    case 3:
                        dates.splice(0, 1);
                        calendarInstance.datepicker('setDates', dates);
                        break;
                    default:
                        break;
                }
            });
            this.clearDateRange = function () {
                calendarInstance.datepicker('setDates', []);
            };
            this.init = function () {

            }
        }
        calendarForTodayHandle = new calendarForTodayClass();

        this.init = function () {
            personalEventsHandle.init();
            todaysEventsHandle.init();
            calendarForTodayHandle.init();
        }
    }

    var amazonProductsClass = function (data) {

        var watchListHandle = null;
        var leftPanelHandle = null;

        data.forEach(function (row_data) {
            row_data.price_rows.forEach(function (price_row) {
                price_row.intProductPrice_newprice = price_row.intProductPrice_newprice / 100;
                price_row.intProductPrice_usedprice = price_row.intProductPrice_usedprice / 100;
            });
        });

        var watchListClass = function () {
            var dom_watch_list_root = $('.amazon-products-watchlist-section');
            var dom_list_contiainer = $('.product-list', dom_watch_list_root);

            var getAsin = function (url) {
                var regex = RegExp("https://www.amazon.com/([\\w-]+/)?(dp|gp/product)/(\\w+/)?(\\w{10})");
                var m = url.match(regex);
                if (m == null){
                    return false;
                }
                return m[4];
            };
            var self = this;
            var listRowHandles = [];
            var keys_of_id = [];

            var createNewProductByAsin = function (asin) {
                var ajaxData = {
                    action: 'create_new_product',
                    asin: asin
                };

                $.ajax({
                    url: ACTION_URL,
                    type: 'post',
                    data: ajaxData,
                    dataType: 'json',
                    success: function (res) {
                        if (res.status){
                            var res_data = res.data;
                            res_data.price_rows.forEach(function (price_row) {
                                price_row.intProductPrice_newprice = price_row.intProductPrice_newprice / 100;
                                price_row.intProductPrice_usedprice = price_row.intProductPrice_usedprice / 100;
                            });

                            if (typeof keys_of_id[res_data.product_ID] == 'undefined'){
                                var newHandle = self.addListRow(res_data);
                                newHandle.show();
                            }
                            else {
                                var currentHandle = listRowHandles[keys_of_id[res_data.product_ID]];
                                currentHandle.updateInFront(res_data);
                            }
                        }
                        else {
                            alert('sorry, somethin is wrong');
                        }
                    }
                });
            };

            var listRowClass = function (row_data) {

                var pos_in_parent = -1;

                var handleOfLeft = null;
                var self = this;

                var dom_list_row = dom_list_contiainer.find('.list-item.sample').clone().removeClass('sample').removeAttr('hidden');
                var dom_title = dom_list_row.find('.product-title');
                var dom_price = dom_list_row.find('.product-price a');
                var dom_close_product = dom_list_row.find('.close-product');

                var dom_recommendation = dom_list_row.find('.recommendation');
                var latest_price = row_data.price_rows[row_data.price_rows.length - 1];
                var format_price = '$' + (latest_price.intProductPrice_newprice);

                var is_show = false;
                var bindEvent = function () {
                    dom_list_row.find('.amchar-icon-wrapper a').click(function () {
                        if (handleOfLeft !== null){
                            var scroll_top = handleOfLeft.getScrollTop();
                            setTimeout(function () {
                                $('html').animate({scrollTop: scroll_top + 'px'}, 400);
                            }, 0);
                        }
                        else {
                            var newHandle = leftPanelHandle.addItem(row_data);
                            self.setLeftHandle(newHandle);
                            handleOfLeft.setRightHandle(self);
                            handleOfLeft.show();
                            var scroll_top = handleOfLeft.getScrollTop();
                            $('.loading').css('display', 'block');
                            setTimeout(function () {
                                $('.loading').css('display', 'none');
                                $('html').animate({scrollTop: scroll_top + 'px'}, 400);
                            }, 370);
                        }
                    });
                    dom_close_product.click(function () {

                        swal({
                            // title: "Are you sure?",
                            text: "Are you sure you want to delete this product?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                self.closeProduct();
                            }
                        });
                    });
                }
                this.closeProduct = function () {
                    var ajaxData = {
                        action: 'close_product',
                        product_id: row_data.product_ID
                    };
                    $.ajax({
                        url: ACTION_URL,
                        type: 'post',
                        data: ajaxData,
                        dataType: 'json',
                        success: function (res) {
                            if (res.status){
                                keys_of_id[row_data.product_ID] = undefined;
                                dom_list_row.remove();
                                listRowHandles.splice(pos_in_parent, 1);
                                if (handleOfLeft !== null){
                                    handleOfLeft.removeRow()
                                }
                            }
                            else {
                                alert('Sorry, something went wrong');
                            }
                        }
                    });
                }
                this.updateInFront = function (new_data) {

                    row_data.strProduct_name = new_data.strProduct_name;
                    row_data.recommendation = new_data.recommendation;
                    row_data.price_rows = new_data.price_rows;

                    latest_price = row_data.price_rows[row_data.price_rows.length - 1];
                    format_price = '$' + (latest_price.intProductPrice_newprice);
                    dom_title.html(row_data.strProduct_name);
                    dom_price.html(format_price);
                    dom_recommendation.html(row_data.recommendation);
                    if (handleOfLeft !== null){
                        handleOfLeft.updateInFront(row_data);
                    }
                }

                this.setPosInParent = function (new_pos) {
                    pos_in_parent = new_pos;
                }
                this.setLeftHandle = function (newHandle) {
                    handleOfLeft = newHandle;
                }
                this.show = function () {
                    if (!is_show){
                        dom_list_row.appendTo(dom_list_contiainer);
                        is_show = true;
                    }
                };
                this.init = function () {
                    dom_title.html(row_data.strProduct_name);
                    dom_price.html(format_price);
                    dom_price.attr('href', row_data.strProduct_amazonURL);
                    dom_recommendation.html(row_data.recommendation);
                    bindEvent();
                }
            }

            var bindEvent = function () {
                dom_watch_list_root.find('.check-product').click(function () {
                    var url = dom_watch_list_root.find('.input-wrapper input[name="new_product"]').val();
                    var asin = getAsin(url);
                    if(asin){
                        createNewProductByAsin(asin);
                    }
                    else {
                        alert('invalid url');
                    }
                });
                dom_watch_list_root.find('.view-all').click(function () {
                    listRowHandles.forEach(function (listRowHandle) {
                        listRowHandle.show();
                    });
                })
            };

            this.addListRow = function (row_data) {
                var newHandle = new listRowClass(row_data);
                newHandle.init();
                keys_of_id[row_data.product_ID] = listRowHandles.length;
                newHandle.setPosInParent(listRowHandles.length);
                listRowHandles.push(newHandle);
                return newHandle;
            };
            this.init = function () {
                data.forEach(function (row_data, i) {
                    keys_of_id[row_data.product_ID] = i;
                    var newHandle = self.addListRow(row_data);
                    newHandle.setPosInParent(i);
                });
                for(var i = 0; i < 5 && i < listRowHandles.length; i ++){
                    listRowHandles[i].show();
                }
                bindEvent();
            }
        };
        watchListHandle = new watchListClass();

        var leftPanelClass = function () {
            var dom_item_rows_container = $('.insider .main_row .user_objects');

            var itemRowHandles = [];

            var itemRowClass = function (row_data) {
                var dom_item_row = dom_item_rows_container.find('.amazon-object.sample').clone().removeClass('sample').removeAttr('hidden');
                var dom_chart = dom_item_row.find('.chart-root').attr('id', 'chartDiv' + row_data.product_ID);
                var dom_item_title = dom_item_row.find('.uod_title');
                var dom_item_image = dom_item_row.find('.product-basic-description-wrapper .image-wrapper img');
                var dom_item_descriptoin = dom_item_row.find('.product-basic-description-wrapper .product-description');
                var dom_prices_wrapper = dom_item_row.find('.prices-wrapper');
                var dom_new_price = dom_prices_wrapper.find('.new-price-wrapper');
                var dom_used_price = dom_prices_wrapper.find('.used-price-wrapper');
                var dom_new_quantity = dom_prices_wrapper.find('.new-quantity-wrapper');
                var dom_used_quantity = dom_prices_wrapper.find('.used-quantity-wrapper');

                var latest_price_data = row_data.price_rows[row_data.price_rows.length - 1];
                var amazonLargeImgLightboxHandle = null;

                var pos_in_parent = -1;
                var self = this;
                var rightHandle = null;
                var chart = null;
                var current_chart_kind = 'new_price';

                var origin_height = 0;

                var clearChat = function () {
                    if (chart !== null){
                        chart.clear();
                        chart = null;
                    }
                }
                var makeChart = function (data_kind) {

                    var flag_dates = [];
                    var values_by_date = [];
                    var data_provider = [];

                    for (var i = 0; i < row_data.price_rows.length; i ++){
                        var price_row = row_data.price_rows[i];
                        if (flag_dates[price_row.dtProductPrice_date] == true){
                            continue ;
                        }
                        flag_dates[price_row.dtProductPrice_date] = true;
                        var vals_filtered =  row_data.price_rows.filter(function (pr) {
                            return (pr.dtProductPrice_date == price_row.dtProductPrice_date);
                        });
                        var sums = {intProductPrice_newprice: 0, intProductPrice_usedprice: 0, intProductPrice_newquantity: 0, intProductPrice_usedquantity: 0};
                        vals_filtered.forEach(function (val_flt) {
                            sums.intProductPrice_newprice += (100 * val_flt.intProductPrice_newprice);
                            sums.intProductPrice_usedprice += (100 * val_flt.intProductPrice_usedprice);
                            sums.intProductPrice_newquantity += (1.0 * val_flt.intProductPrice_newquantity);
                            sums.intProductPrice_usedquantity += (1.0 * val_flt.intProductPrice_usedquantity);
                        });
                        var average = {
                            intProductPrice_newprice: Math.round(sums.intProductPrice_newprice / vals_filtered.length) / 100,
                            intProductPrice_usedprice: Math.round(sums.intProductPrice_usedprice / vals_filtered.length) / 100,
                            intProductPrice_newquantity: sums.intProductPrice_newquantity / vals_filtered.length,
                            intProductPrice_usedquantity: sums.intProductPrice_usedquantity / vals_filtered.length
                        };
                        switch (data_kind){
                            case 'new_price':
                                values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_newprice;
                                break;
                            case 'used_price':
                                values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_usedprice;
                                break;
                            case 'new_quantity':
                                values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_newquantity;
                                break;
                            case 'used_quantity':
                                values_by_date[price_row.dtProductPrice_date] = average.intProductPrice_usedquantity;
                                break;
                        }
                    }

                    for (var price_date in values_by_date){
                        data_provider.push({"date": price_date, "value": values_by_date[price_date]});
                    }

                    chart = AmCharts.makeChart(dom_chart.attr('id'), {
                        "path": "assets/plugins/amchart/amcharts/",
                        "type": "serial",
                        "theme": "light",
                        "marginRight": 40,
                        "marginLeft": 40,
                        "autoMarginOffset": 20,
                        "mouseWheelZoomEnabled":true,
                        "dataDateFormat": "YYYY-MM-DD",
                        "valueAxes": [{
                            "id": "v1",
                            "axisAlpha": 0,
                            "position": "left",
                            "ignoreAxisWidth":true
                        }],
                        "balloon": {
                            "borderThickness": 1,
                            "shadowAlpha": 0
                        },
                        "graphs": [{
                            "id": "g1",
                            "balloon":{
                                "drop":true,
                                "adjustBorderColor":false,
                                "color":"#ffffff"
                            },
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "bulletSize": 5,
                            "hideBulletsCount": 50,
                            "lineThickness": 2,
                            "title": "red line",
                            "useLineColorForBulletBorder": true,
                            "valueField": "value",
                            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
                        }],
                        // "chartScrollbar": {
                        //     "graph": "g1",
                        //     "oppositeAxis":false,
                        //     "offset":30,
                        //     "scrollbarHeight": 80,
                        //     "backgroundAlpha": 0,
                        //     "selectedBackgroundAlpha": 0.1,
                        //     "selectedBackgroundColor": "#888888",
                        //     "graphFillAlpha": 0,
                        //     "graphLineAlpha": 0.5,
                        //     "selectedGraphFillAlpha": 0,
                        //     "selectedGraphLineAlpha": 1,
                        //     "autoGridCount":true,
                        //     "color":"#AAAAAA"
                        // },
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha":1,
                            "cursorColor":"#258cbb",
                            "limitToGraph":"g1",
                            "valueLineAlpha":0.2,
                            "valueZoomable":true
                        },
                        // "valueScrollbar":{
                        //     "oppositeAxis":false,
                        //     "offset":50,
                        //     "scrollbarHeight":10
                        // },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true
                        },
                        "export": {
                            "enabled": true
                        },
                        "dataProvider": data_provider
                    });
                    chart.addListener("rendered", zoomChart);

                    zoomChart();

                    function zoomChart() {
                        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
                    }
                }
                var bindEvent = function () {
                    dom_new_price.click(function () {
                        dom_prices_wrapper.find('.price-item').removeClass('active');
                        dom_new_price.addClass('active');
                        clearChat();
                        makeChart('new_price');
                        current_chart_kind = 'new_price';
                    });
                    dom_new_quantity.click(function () {
                        dom_prices_wrapper.find('.price-item').removeClass('active');
                        dom_new_quantity.addClass('active');
                        clearChat();
                        makeChart('new_quantity');
                        current_chart_kind = 'new_quantity';
                    });
                    dom_used_price.click(function () {
                        dom_prices_wrapper.find('.price-item').removeClass('active');
                        dom_used_price.addClass('active');
                        clearChat();
                        makeChart('used_price');
                        current_chart_kind = 'used_price';
                    });
                    dom_used_quantity.click(function () {
                        dom_prices_wrapper.find('.price-item').removeClass('active');
                        dom_used_quantity.addClass('active');
                        clearChat();
                        makeChart('used_quantity');
                        current_chart_kind = 'used_quantity';
                    });
                    dom_item_row.find('.product-basic-description-wrapper .description-wrapper .view-more-wrapper .view-more').click(function () {
                        dom_item_row.find('.product-basic-description-wrapper .description-wrapper').addClass('expanded');
                        dom_item_descriptoin.css({maxHeight: origin_height});
                    });
                    dom_item_row.find('.product-basic-description-wrapper .description-wrapper .view-more-wrapper .view-less').click(function () {
                        dom_item_row.find('.product-basic-description-wrapper .description-wrapper').removeClass('expanded');
                        dom_item_descriptoin.css({maxHeight: '160px'});
                        setTimeout(function () {
                            $('html').animate({scrollTop: self.getScrollTop() + 'px'}, 600);
                        }, 0);
                    });
                    dom_item_row.find('.product-basic-description-wrapper .image-wrapper img').click(function () {
                        //marked
                        amazonLargeImgLightboxHandle.openLightbox();
                    });
                };
                var amazonLargeImgLightbox = function () {
                    var dom_lightbox_root = $('.amazon-large-img-lightbox.sample').clone().removeClass('sample');
                    var dom_lightbox_inner_wrapper = $('.my-lightbox-inner-wrapper', dom_lightbox_root);
                    var dom_lightbox_inner = $('.my-lightbox-inner', dom_lightbox_inner_wrapper);
                    var dom_close_btn = $('.close', dom_lightbox_inner_wrapper);
                    var dom_open_from = dom_item_row.find('.product-basic-description-wrapper .image-wrapper img');
                    dom_lightbox_inner.find('.amazon-large-img').attr('src', row_data.strProduct_large_img);

                    var openLightbox = function () {

                        dom_lightbox_root.appendTo('body');
                        dom_open_from.addClass('open-lightbox-from-state');
                        dom_lightbox_inner.css('display', 'none');
                        var window_width = $(window).width();
                        setTimeout(function () {
                            dom_lightbox_root.css('display', 'block');
                            var pos = dom_open_from.offset();
                            pos.top = pos.top - $(window).scrollTop();
                            var width = dom_open_from.outerWidth();
                            var height = dom_open_from.outerHeight();
                            pos.right = $(window).width() - pos.left - width;
                            pos.bottom = $(window).height() - pos.top - height;
                            dom_lightbox_inner_wrapper.css(pos);

                            var to_css = {
                                top: '5vh',
                                right: '10vw',
                                left: '10vw',
                                bottom: '5vh',
                                fontSize: '10px',
                                padding: '50px'
                            };
                            if (window_width > 768 && window_width < 910){
                                to_css = {
                                    top: '2vh',
                                    right: '4vw',
                                    left: '4vw',
                                    bottom: '2vh',
                                    fontSize: '10px',
                                    padding: '20px'
                                }
                            }
                            else if (window_width < 768){
                                to_css = {
                                    top: '1vh',
                                    right: '2vw',
                                    left: '2vw',
                                    bottom: '1vh',
                                    fontSize: '10px',
                                    padding: '20px'
                                }
                            }

                            dom_lightbox_inner_wrapper.animate(to_css, {
                                duration: 500,
                                complete: function () {
                                    dom_lightbox_inner_wrapper.css('padding', 'auto');
                                    dom_lightbox_inner.css('display', 'block');
                                    dom_lightbox_root.addClass('open-status');
                                }
                            });
                        }, 100);
                    }
                    var closeLightBox = function () {
                        dom_lightbox_root.removeClass('open-status');
                        dom_lightbox_inner.css('display', 'none');
                        dom_lightbox_inner_wrapper.css('padding', '0px');
                        var pos = dom_open_from.offset();
                        pos.top = pos.top - $(window).scrollTop();
                        var width = dom_open_from.outerWidth();
                        var height = dom_open_from.outerHeight();
                        pos.right = $(window).width() - pos.left - width;
                        pos.bottom = $(window).height() - pos.top - height;
                        dom_lightbox_root.removeClass('open-status');
                        var css = $.extend(pos, {});
                        dom_lightbox_inner_wrapper.animate(
                            css,
                            {
                                duration: 500,
                                start: function () {
                                },
                                complete: function () {
                                    dom_lightbox_inner_wrapper.removeAttr('style');
                                    dom_lightbox_root.css('display', 'none');
                                    dom_open_from.removeClass('open-lightbox-from-state');
                                    dom_lightbox_root.detach();
                                }
                            }
                        );
                    }
                    dom_close_btn.click(function () {
                        closeLightBox();
                    });

                    this.openLightbox = openLightbox;
                    this.closeLightBox = closeLightBox;
                }
                amazonLargeImgLightboxHandle = new amazonLargeImgLightbox();

                this.removeRow = function () {
                    dom_item_row.remove();
                    itemRowHandles.splice(pos_in_parent, 1);
                }
                this.setRightHandle = function (newHandle) {
                    rightHandle = newHandle;
                }
                this.show = function () {
                    dom_item_row.prependTo(dom_item_rows_container);
                    makeChart('new_price');
                }
                this.updateInFront = function(new_data){
                    row_data = new_data;
                    latest_price_data = row_data.price_rows[row_data.price_rows.length - 1];
                    dom_item_title.html(row_data.strProduct_name);
                    dom_new_price.find('.item-value').html('$' + latest_price_data.intProductPrice_newprice);
                    dom_used_price.find('.item-value').html('$' + latest_price_data.intProductPrice_usedprice);
                    dom_new_quantity.find('.item-value').html(latest_price_data.intProductPrice_newquantity);
                    dom_used_quantity.find('.item-value').html(latest_price_data.intProductPrice_usedquantity);
                    dom_item_row.find('.amazon-link-wrapper a').attr('href', row_data.strProduct_amazonURL);
                    clearChat();
                    makeChart(current_chart_kind);
                }
                this.getScrollTop = function () {
                    return dom_item_row.offset().top;
                }
                this.setPosInParent = function(new_pos){
                    pos_in_parent = new_pos;
                }
                this.init = function () {
                    dom_item_title.html(row_data.strProduct_name);
                    dom_item_image.attr('src', row_data.strProduct_image);
                    dom_item_descriptoin.html(row_data.strProduct_description);
                    dom_new_price.find('.item-value').html('$' + latest_price_data.intProductPrice_newprice);
                    dom_used_price.find('.item-value').html('$' + latest_price_data.intProductPrice_usedprice);
                    dom_new_quantity.find('.item-value').html(latest_price_data.intProductPrice_newquantity);
                    dom_used_quantity.find('.item-value').html(latest_price_data.intProductPrice_usedquantity);
                    dom_item_row.find('.amazon-link-wrapper a').attr('href', row_data.strProduct_amazonURL);

                    var clone_description = dom_item_descriptoin.clone(true).css({height: 'auto', position: 'absolute', maxHeight: 'none', width: '100%', top: '10000px'}).appendTo(dom_item_row.find('.product-basic-description-wrapper .description-wrapper'));
                    setTimeout(function () {
                        origin_height = clone_description.height();
                        dom_item_descriptoin.css({maxHeight: '160px'});
                        if(origin_height <= 160){
                            dom_item_row.find('.product-basic-description-wrapper .description-wrapper .view-more-wrapper').remove();
                        }
                        clone_description.remove();
                    }, 0);
                    bindEvent();
                }
            }

            this.addItem = function (row_data) {
                var newHandle = new itemRowClass(row_data);
                newHandle.init();
                newHandle.setPosInParent(itemRowHandles.length);
                itemRowHandles.push(newHandle);
                return newHandle;
            }
            this.init = function () {

            }
        }

        leftPanelHandle = new leftPanelClass();

        this.init = function () {
            leftPanelHandle.init();
            watchListHandle.init();
        }
    }

    var personGoalsClass = function (data) {

        var popupHandle = null;
        var leftPanelHandle = null;

        var popupClass = function () {
            var dom_modal_root = $('#popup-for-new-goals');
            var dom_close_modal = dom_modal_root.find('.close-modal-wrppaer button');
            dom_modal_root.modal({
                backdrop: false,
                show: false
            });

            var goals = [];

            var sendGoal = function (callback) {
                var ajax_data = {
                    action: 'create_goal',
                    goal_text: dom_modal_root.find('form textarea').val()
                };

                $.ajax({
                    url: ACTION_URL,
                    data: ajax_data,
                    success: function (res) {
                        if (res.status){
                            var not_show_again_val = dom_modal_root.find('#not-again-checkbox').prop('checked');
                            goals.push(res.data);
                            if (not_show_again_val){
                                notShowAgain();
                                dom_modal_root.modal('hide');
                            }
                            else {
                                dom_modal_root.find('.process-message').html('Goal added! Would you like to set another goal?');
                                dom_modal_root.find('form textarea').val('');
                                dom_modal_root.find('form textarea').attr('placeholder', 'Set another goal...');
                                dom_close_modal.html('My Goals are Set!');
                            }
                            if (callback !== false){
                                callback();
                            }
                        }
                        else {
                            alert('sorry, something went wrong');
                        }
                    },
                    type: 'post',
                    dataType: 'json'
                });
            }
            var notShowAgain = function () {
                var ajax_data = {
                    action: 'not_new_goal_again'
                };
                $.ajax({
                    url: ACTION_URL,
                    data: ajax_data,
                    success: function (res) {
                        if (res.status){
                        }
                        else {
                            alert('sorry, something went wrong');
                        }
                    },
                    type: 'post',
                    dataType: 'json'
                });
            }
            var bindEvent = function () {
                dom_modal_root.find('form').submit(function () {
                    sendGoal(false);
                    return false;
                });
                dom_modal_root.on('hide.bs.modal', function () {
                    if (goals.length){
                        leftPanelHandle.addItem(goals);
                    }
                });
                dom_close_modal.click(function () {
                    var not_show_again_val = dom_modal_root.find('#not-again-checkbox').prop('checked');
                    if (not_show_again_val){
                        notShowAgain();
                    }
                    var goal_text = dom_modal_root.find('.new-goal-wrapper textarea').val();
                    if (goal_text !== ''){
                        sendGoal(function () {
                            dom_modal_root.modal('hide');
                        });
                    }
                    else {
                        dom_modal_root.modal('hide');
                    }
                    return false;
                })
            }
            this.init = function () {
                bindEvent();
                if (SHOW_GOAL_POPUP && IS_FIRST_VISIT_TODAY){
                    dom_modal_root.modal('show');
                }
            }
        }
        popupHandle = new popupClass();

        var leftPanelClass = function () {
            var dom_item_rows_container = $('.insider .main_row .user_objects');

            var itemRowHandles = [];

            var itemRowClass = function (row_data) {
                var dom_item_root = dom_item_rows_container.find('.goal-object.sample').clone().removeClass('sample').removeAttr('hidden');
                var dom_goals_container = dom_item_root.find('.goals-container ul');
                var dom_progress_indicator = dom_item_root.find('.progress-indicator');

                var goalHandles = [];
                var self = this;

                var goalItemClass = function (goal_data) {
                    var dom_goal_root = dom_goals_container.find('li.sample').clone().removeClass('sample').removeAttr('hidden');
                    var dom_progress = $($.parseHTML('<li><span class="bubble"></span> Goal <span class="goal-number">1</span>.</li>'));
                    var goal_number = 1;
                    var pos_in_parent = goalHandles.length;

                    var updateGoal = function (new_text, new_complete) {

                        new_text = new_text === false ? goal_data.strGoal_text : new_text;
                        new_complete = new_complete === false ? goal_data.boolGoal_complete : new_complete;
                        var ajax_data = {
                            action: 'update_goal',
                            id: goal_data.goal_ID,
                            text: new_text,
                            complete: new_complete
                        }
                        $.ajax({
                            url: ACTION_URL,
                            data: ajax_data,
                            success: function (res) {
                                if (res.status){
                                    dom_goal_root.removeClass('edit-status');
                                    goal_data.strGoal_text = new_text;
                                    goal_data.boolGoal_complete = new_complete;
                                    dom_goal_root.find('.goal-text').html(goal_data.strGoal_text);
                                    dom_goal_root.find('textarea[name="goal_text"]').val(goal_data.strGoal_text);

                                    if (goal_data.boolGoal_complete === '1'){
                                        dom_goal_root.find('.toogle-complete').addClass('btn-success');
                                        dom_progress.addClass('completed');
                                    }
                                    else {
                                        dom_goal_root.find('.toogle-complete').removeClass('btn-success');
                                        dom_progress.removeClass('completed');
                                    }
                                }
                                else{
                                    alert('Sorry, something went wrong!');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }
                    var deleteGoal = function () {
                        var ajax_data = {
                            action: 'delete_goal',
                            id: goal_data.goal_ID
                        };
                        $.ajax({
                            url: ACTION_URL,
                            data: ajax_data,
                            success: function (res) {
                                if (res.status){
                                    dom_goal_root.remove();
                                    dom_progress.remove();
                                    goalHandles.splice(pos_in_parent, 1);
                                    self.refresh();
                                }
                                else{
                                    alert('Sorry, something went wrong!');
                                }
                            },
                            type: 'post',
                            dataType: 'json'
                        });
                    }

                    var bindEvent = function () {
                        dom_goal_root.find('.action-container .edit-entry').click(function () {
                            dom_goal_root.addClass('edit-status');
                        });
                        dom_goal_root.find('.action-container .save-entry').click(function () {
                            updateGoal(dom_goal_root.find('textarea[name="goal_text"]').val(), false);
                        });
                        dom_goal_root.find('.action-container .back-to-origin').click(function () {
                            dom_goal_root.removeClass('edit-status');
                        });
                        dom_goal_root.find('.goal-complete-wrapper .toogle-complete').click(function () {
                            var new_complete = goal_data.boolGoal_complete === '1' ? '0' : '1';
                            updateGoal(false, new_complete);
                        });
                        dom_goal_root.find('.delete-goal-wrapper .delete-goal').click(function () {
                            swal({
                                // title: "Are you sure?",
                                text: "Are you sure you want to delete this goal?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteGoal();
                                }
                            });
                        });
                    }

                    this.updatePosInParent = function (new_pos) {
                        pos_in_parent = new_pos;
                        goal_number = 1 + 1 * new_pos;
                        dom_goal_root.find('.goal-number-wrapper .goal-number').html(goal_number);
                        dom_progress.find('.goal-number').html(goal_number);
                    }
                    this.init = function () {
                        dom_goal_root.appendTo(dom_goals_container);
                        dom_progress.appendTo(dom_progress_indicator);
                        goal_number = goalHandles.length;
                        dom_goal_root.find('.goal-number-wrapper .goal-number').html(goal_number);
                        dom_progress.find('.goal-number').html(goal_number);
                        dom_goal_root.find('.goal-text').html(goal_data.strGoal_text);
                        dom_goal_root.find('textarea[name="goal_text"]').val(goal_data.strGoal_text);
                        if (goal_data.boolGoal_complete === '1'){
                            dom_goal_root.find('.toogle-complete').addClass('btn-success');
                            dom_progress.addClass('completed');
                        }
                        else {
                            dom_goal_root.find('.toogle-complete').removeClass('btn-success');
                            dom_progress.removeClass('completed');
                        }
                        bindEvent();
                    }
                }

                this.addGoal = function (goal_data) {
                    var newHandle = new goalItemClass(goal_data);
                    goalHandles.push(newHandle);
                    newHandle.init();
                    return newHandle;
                }
                this.refresh = function () {
                    goalHandles.forEach(function (goalHandle, i) {
                        goalHandle.updatePosInParent(i);
                    });
                }
                this.init = function () {
                    dom_item_root.prependTo(dom_item_rows_container);
                    row_data.forEach(function (goal_data) {
                        self.addGoal(goal_data);
                    });
                }
            }
            this.addItem = function (row_data) {
                var newHandle = new itemRowClass(row_data);
                newHandle.init();
                itemRowHandles.push(newHandle);
            }
            this.init = function () {
                if (data.length){
                    this.addItem(data);
                }
            }
        }
        leftPanelHandle = new leftPanelClass();

        this.init = function () {
            leftPanelHandle.init();
            popupHandle.init();
        }
    }

    var amazonProductsHandle = null;
    var eventsSectionHandle = null;
    var personGoalsHandle = null;

    is_loading_controlled_in_local = true;
    $('.loading').css('display', 'block');
    jQuery(document).ready(function ($) {

        var ajaxData = {
            action: 'getEvents'
        };

        $.ajax({
            url: ACTION_URL,
            type: 'post',
            data: ajaxData,
            dataType: 'json',
            success: function (res) {
                if (res.status){
                    eventsSectionHandle = new eventsSectionClass(res.data);
                    eventsSectionHandle.init();
                }
                else {
                    alert('sorry, somethin is wrong');
                }
            }
        });

        ajaxData = {action: 'get_amazon_data'};
        $.ajax({
            url: ACTION_URL,
            type: 'post',
            data: ajaxData,
            dataType: 'json',
            success: function (res) {
                if (res.status){
                    amazonProductsHandle = new amazonProductsClass(res.data);
                    amazonProductsHandle.init();
                    $('.loading').css('display', 'none');
                    is_loading_controlled_in_local = false;
                }
                else {
                    alert('sorry, somethin is wrong');
                }
            }
        });

        ajaxData = {
            action: 'get_goals'
        };
        $.ajax({
            url: ACTION_URL,
            type: 'post',
            data: ajaxData,
            dataType: 'json',
            success: function (res) {
                if (res.status){
                    personGoalsHandle = new personGoalsClass(res.data);
                    personGoalsHandle.init();
                    $('.loading').css('display', 'none');
                }
                else {
                    alert('sorry, somethin is wrong');
                }
            }
        });
        userpageHandle.init();
    });

    $(document).ajaxStart(function () {

        if(!is_loading_controlled_in_local) {
            $('.loading').css('display', 'block');
        }
    });
    $(document).ajaxComplete(function () {
        if(!is_loading_controlled_in_local){
            $('.loading').css('display', 'none');
        }
    });
})()