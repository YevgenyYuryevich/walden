(function(){
    'use strict';

    var is_loading_controlled_in_local = false;
    var accept_doc_types = ['application/pdf','text/plain'];
    var accept_image_type = ['image/png', 'image/jpeg'];
    var accept_audio_types = ['audio/mp3'];
    var accept_video_types = ['video/mp4', 'video/avi'];

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    var EditSeriesClass = function () {

        var mainHandle, addPostsHandle;
        var isAdvancedUpload;

        var MainClass = function () {

            var domRoot = $('#main-tab');
            var domAddLinks = domRoot.find('.add-to-links-section');
            var domPostsList = $('.posts-container .posts-list');

            var series = initialSeries;
            var purchase = initialPurchase;
            var availableDays = purchase.available_days;

            var postRowHandles = [], virtualRootHandle, virtualRootData;
            var footerHandle;
            var self = this;
            var isFirstAddPostOpen = true;
            var postFields = {
                id: 'post_ID',
                title: 'strPost_title',
                body: 'strPost_body',
                image: 'strPost_featuredimage',
                type: 'intPost_type',
                order: 'intPost_order',
                nodeType: 'strPost_nodeType',
                status: 'strPost_status',
                parent: 'intPost_parent',
                free: 'boolPost_free',
            };

            var subscriptionFiels = {
                id: 'clientsubscription_ID',
                title: 'strClientSubscription_title',
                body: 'strClientSubscription_body',
                image: 'strClientSubscription_image',
                type: 'intClientSubscriptions_type',
                order: 'intClientSubscriptions_order',
                nodeType: 'strClientSubscription_nodeType',
                parent: 'intClientSubscriptions_parent'
            };

            var viewFormat = function (data) {
                var fData = {};
                var fk;

                for (var k in data) {
                    if (isSeriesMine) {
                        fk = helperHandle.keyOfVal(postFields, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                    else {
                        fk = helperHandle.keyOfVal(subscriptionFiels, k);
                        if (fk) { fData[fk] = data[k]; }
                    }
                }
                fData.origin = data;
                return fData;
            }

            var postFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (postFields[k]) {
                        fData[postFields[k]] = data[k];
                    }
                }
                return fData;
            }

            var subscriptionFormat = function (data) {
                var fData = {};
                for (var k in data) {
                    if (subscriptionFiels[k]) {
                        fData[subscriptionFiels[k]] = data[k];
                    }
                }
                return fData;
            }

            var originFormat = function (data) {
                if (isSeriesMine) {
                    return postFormat(data);
                }
                return subscriptionFormat(data);
            }

            var PostRowClass = function (postData) {

                var domRow, domChildList, domInnerWrp;
                var self = this, parentHandle = false;

                var isChildsLoaded = false;

                var childHandles = [];
                var editModalHandle = null;

                var metaData = {};

                var openEditModal = function () {
                    var edited = false;
                    var resP;
                    if (typeof postData.body !== 'undefined') {
                        resP = Promise.resolve(postData.body);
                    }
                    else {
                        if (isSeriesMine) {
                            resP = ajaxAPiHandle.apiPost('Posts.php', {action: 'get', where: self.getId()}).then(function (res) {
                                postData.body = res.data.strPost_body;
                                return postData.body;
                            });
                        }
                        else {
                            resP = ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'get', where: self.getId()}).then(function (res) {
                                postData.body = res.data.strClientSubscription_body;
                                return postData.body;
                            });
                        }
                    }
                    resP.then(function () {
                        postData.series = initialSeries;
                        editModalHandle = new EditPostModalClass(domRoot.find('.modal-edit-entry-wrapper .modal-edit-post').clone(), postData);
                        editModalHandle.init();
                        editModalHandle.afterUpdate(function (sets) {
                            edited = true;
                            bindData();
                        });
                        editModalHandle.modalHandle().onClosed(function () {
                            if (edited) {
                                self.focus();
                            }
                        });
                        editModalHandle.modalHandle().open();
                    });
                }
                var bindEvents = function () {
                    domInnerWrp.find('.delete-post img').click(function () {
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover this!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                self.deletePost();
                            }
                        });
                    });
                    domInnerWrp.find('.clone-post img').click(function () {
                        self.clonePost();
                    });
                    domInnerWrp.find('.edit-post').click(function () {
                        openEditModal();
                    });
                    domInnerWrp.find('.view-post').click(function () {
                        openView();
                    });
                    domInnerWrp.find('.comeback-post').click(function () {
                        bindData();
                        makeNormalStatus();
                    });
                    domInnerWrp.find('.publish-post .inactive-icon').click(function () {
                        var sets = {
                            status: 'publish',
                        };
                        self.update(sets).then(function () {
                            makeNormalStatus();
                        });
                    });
                    domInnerWrp.find('.publish-post .active-icon').click(function () {
                        var sets = {
                            status: 'draft',
                        };
                        self.update(sets).then(function () {
                            makeNormalStatus();
                        });
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'deep_delete', where: {intClientSubscription_post_ID: self.getId()}});
                    });
                    domInnerWrp.find('.save-post').click(function () {
                        var sets = {
                            title: domInnerWrp.find('.post-title').html(),
                        };
                        self.update(sets).then(function () {
                            makeNormalStatus();
                        });
                    });
                    domInnerWrp.find('.jstree-icon.jstree-ocl').click(function () {
                        if (domRow.hasClass('jstree-open') && childHandles.length && self.nodeType() !== 'post') {
                            self.collapseChild();
                        }
                        else {
                            self.expandChild();
                        }
                    });
                    domInnerWrp.find('.post-title').keypress(function (e) {
                        var key = e.which;
                        if(key == 13)  // the enter key code
                        {
                            e.preventDefault();
                            domInnerWrp.find('.save-post').click();
                        }
                    });
                    if (isSeriesMine) {
                        domInnerWrp.find('[name="boolPost_free"]').change(function () {
                            var v = $(this).prop('checked') ? 1 : 0;
                            self.update({free: v});
                        });
                    }
                }
                var openView = function () {
                    var from = isSeriesMine ? 'post' : 'subscription';
                    var cloneForm = $('<form action="view" method="get" target="_blank" hidden><input name="id" value="'+ postData.id +'"><input name="from" value="'+ from +'" /><input name="prevpage" value="editseries"></form>').appendTo('body').submit();
                    cloneForm.remove();
                }
                var loadChilds = function () {
                    childHandles = [];
                    domRow.addClass('jstree-loading');
                    is_loading_controlled_in_local = true;
                    domChildList.empty();

                    var resPromise, ajaxData;
                    if (isSeriesMine) {
                        ajaxData = {
                            action: 'get_items',
                            where: {intPost_series_ID: initialSeries.series_ID, intPost_parent: postData.id},
                            select: helperHandle.array_keys(postFormat(postData)),
                        };
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData, false);
                    }
                    else {
                        var select = helperHandle.array_keys(subscriptionFormat(postData));
                        select = select.concat('intClientSubscription_post_ID');

                        ajaxData = {
                            action: 'get_items',
                            where: {intClientSubscription_purchased_ID: initialPurchase.purchased_ID, intClientSubscriptions_parent: postData.id},
                            select: select,
                        };
                        if (postData.origin.intClientSubscription_post_ID !== null) {
                            ajaxData.meta = {
                                pre_action: 'subscribe',
                                where: {
                                    intPost_series_ID: initialSeries.series_ID,
                                    intPost_parent: postData.origin.intClientSubscription_post_ID
                                },
                                intClientSubscription_purchased_ID: initialPurchase.purchased_ID
                            };
                        }
                        resPromise = ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData, false);
                    }
                    return resPromise.then(function (res) {

                        var childPosts = res.data;
                        var promises = [];

                        childPosts.forEach(function (childPost) {
                            var fData = viewFormat(childPost);
                            promises.push(self.addChild(fData));
                        });
                        isChildsLoaded = true;
                        domRow.removeClass('jstree-loading');
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        is_loading_controlled_in_local = false;

                        if (!childHandles.length) {
                            domRow.addClass('jstree-leaf');
                        }
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve(true);
                            }, 200);
                        });
                    });
                }

                var bindData = function () {
                    domInnerWrp.find('.post-title').html(postData.title);

                    domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-' + postData.nodeType);
                    domRow.removeClass('jstree-leaf');
                    domRow.removeClass('mjs-nestedSortable-leaf');
                    domRow.removeClass('mjs-nestedSortable-branch');
                    domRow.removeClass('node-status-publish node-status-draft');

                    if (postData.nodeType == 'post') {
                        isChildsLoaded = true;
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-leaf');
                        self.expandChild();
                    }
                    else if (isChildsLoaded && !childHandles.length) {
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    else {
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    if (isSeriesMine) {
                        domRow.addClass('node-status-' + postData.status);
                        domInnerWrp.find('[name="boolPost_free"]').prop('checked', parseInt(postData.free));
                    }
                }

                var makeEditStatus = function () {
                    domRow.addClass('edit-status');
                    domInnerWrp.find('.post-title').attr('contenteditable', true);
                }

                var makeNormalStatus = function () {
                    domRow.removeClass('edit-status');
                    domInnerWrp.find('.post-title').removeAttr('contenteditable');
                }

                this.getPath = function () {
                    if (!parentHandle) {
                        return [self.getId()];
                    }
                    var parentPath = parentHandle.getPath();
                    return parentPath.push(self.getId());
                }

                this.expandChild = function () {
                    if (!isChildsLoaded) {
                        loadChilds().then(function (res) {
                            res.then(function () {
                                domRow.removeClass('jstree-closed').addClass('jstree-open');
                                domRow.find('> ul').collapse('show');
                            });
                        });
                    }
                    else {
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        domRow.find('> ul').collapse('show');
                    }
                }

                this.collapseChild = function () {
                    domRow.removeClass('jstree-open');
                    domRow.find('> ul').collapse('hide');
                    domRow.find('> ul').off('hidden.bs.collapse').one('hidden.bs.collapse', function (e) {
                        domRow.addClass('jstree-closed');
                    });
                }

                this.childLoaded = function () {
                    return isChildsLoaded;
                }
                this.reOrderChilds = function () {
                    var updateSets = [];
                    childHandles.forEach(function (childHdl) {
                        var newOrder = childHdl.getDomPosition() + 1;
                        updateSets.push({where: childHdl.getId(), sets: originFormat({order: newOrder})});
                        childHdl.setOrder(newOrder);
                    });
                    if (isSeriesMine) {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'update_posts', updateSets: updateSets});
                    }
                    else {
                        ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update_items', updateSets: updateSets});
                    }
                }
                this.spliceChild = function (id) {
                    childHandles.forEach(function (childHandle, i) {
                        if (childHandle.getId() === id) {
                            childHandles.splice(i, 1);
                        }
                    });
                    if (isChildsLoaded && !childHandles.length) {
                        domRow.removeClass('jstree-open').addClass('jstree-leaf');
                    }
                }
                this.addChildHandle = function (childHandle) {
                    childHandles.push(childHandle);
                    domRow.removeClass('jstree-leaf');
                }
                this.addChild = function (childPost) {
                    var hdl = new PostRowClass(childPost);
                    hdl.init();
                    hdl.setParent(self);
                    return hdl.appendTo(domChildList);
                }
                this.setParent = function (parentHdl) {
                    parentHandle = parentHdl;
                    parentHandle.addChildHandle(self);
                }
                this.updateParent = function (parentHdl) {
                    parentHandle.spliceChild(self.getId());
                    var parentType = parentHdl ? parentHdl.nodeType() : 'path';
                    var myNodeType = self.nodeType(), newNodeType = myNodeType;

                    parentHandle = parentHdl;
                    parentHandle.addChildHandle(self);
                    self.makeTypeValid();

                    var sets = {};
                    sets.parent = parentHdl.getId();

                    sets.nodeType = generateValidNodeType(parentType, myNodeType);

                    return self.update(sets).then(function (res) {
                        return res;
                    });
                }
                this.nodeType = function () {
                    return postData.nodeType;
                }
                this.makeTypeValid = function () {
                    var parentType = parentHandle ? parentHandle.nodeType() : 'path';
                    var myNodeType = self.nodeType(), newNodeType = myNodeType;
                    postData.strPost_nodeType = generateValidNodeType(parentType, myNodeType);
                    bindData();
                    childHandles.forEach(function (childHandle) {
                        childHandle.makeTypeValid();
                    });
                }
                this.update = function (sets) {

                    var resPromise;

                    if (isSeriesMine) {
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', {action: 'update', sets: originFormat(sets), where: self.getId()});
                    }
                    else {
                        resPromise = ajaxAPiHandle.apiPost('Subscriptions.php', {action: 'update', sets: originFormat(sets), where: self.getId()});
                    }
                    return resPromise.then(function (res) {
                        $.extend(postData, sets);
                        if (sets.body) {
                            metaData.body = sets.body;
                        }
                        bindData();
                        return res;
                    });
                }
                this.clonePost = function () {
                    var resPromiss;
                    if (isSeriesMine){
                        resPromiss = ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'clone_post', where: self.getId()});
                    }
                    else {
                        resPromiss = ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'clone_subscription', where: self.getId()});
                    }
                    return resPromiss.then(function (res) {
                        if (res.status){
                            var newHandle = new PostRowClass(viewFormat(res.data));
                            newHandle.init();
                            newHandle.setParent(parentHandle);
                            newHandle.appearAfter(domRow);
                            return res;
                        }
                    })
                }
                this.setOrder = function (newOrder) {
                    postData.order = newOrder;
                }
                this.getOrder = function () {
                    return postData.order;
                }
                this.getDomPosition = function () {
                    return domRow.index();
                }
                this.getId = function () {
                    return postData.id;
                }
                this.deletePost = function () {
                    var resPromise = ajaxAPiHandle.pagePost(CURRENT_PAGE, {action: 'delete_post', where: self.getId(), meta: initialSeries.series_ID});

                    resPromise.then(function () {
                        self.disappear().then(function () {
                            domRow.remove();
                            parentHandle.spliceChild(self.getId());
                        });
                    });
                }
                this.removeDom = function () {
                    domRow.remove();
                }
                this.focus = function () {
                    domRow.addClass('focus');
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('focus');
                            resolve();
                        }, 700);
                    });
                }
                this.appearAfter = function (domAfter) {
                    var newDom;
                    if (domAfter){
                        setTimeout(function () {
                            domRow.addClass('appear');
                            newDom = domRow.insertAfter(domAfter);
                        }, 200);
                    }
                    else {
                        setTimeout(function () {
                            domRow.addClass('appear');
                            newDom = domRow.appendTo(domPostsList);
                        }, 200)
                    }
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('appear');
                            resolve();
                        }, 700);
                    });
                }
                this.getDom = function () {
                    return domRow;
                }
                this.appendTo = function (domList) {
                    if (!domList) {
                        if (parentHandle) {
                            domList = parentHandle.getDom().find('> ul');
                        }
                        else {
                            domList = domPostsList;
                        }

                    }
                    setTimeout(function () {
                        domRow.addClass('appear');
                        domRow.appendTo(domList);
                    }, 200);
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('appear');
                            resolve();
                        }, 700);
                    });
                }
                this.disappear = function () {
                    setTimeout(function () {
                        domRow.addClass('disappear');
                    }, 200)
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('disappear');
                            domRow.detach();
                            resolve();
                        }, 700)
                    });
                }
                this.init = function () {
                    domRow = domRoot.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                    domInnerWrp = domRow.find('> .post-row-inner');
                    bindData();
                    domRow.attr('id', 'sortable-tree-item-' + self.getId());
                    if (self.nodeType() !== 'post') {
                        domRow.append('<ul class="collapse" style="height: 0px;"></ul>');
                        domChildList = domRow.find('> ul');
                    }
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }

            var generateValidNodeType = function(parentNodeType, myNodeType) {
                var newNodeType = myNodeType;

                switch (parentNodeType) {
                    case 'post':
                        break;
                    case 'menu':
                        newNodeType = 'path';
                        break;
                    case 'path':
                        if (myNodeType === 'path') {
                            newNodeType = 'menu';
                        }
                        break;
                }
                return newNodeType;
            }

            var toggleAvailableDay = function (dayName, domTarget) {
                var dayNames = ['mon', 'tus', 'wed', 'thi', 'fri', 'sat', 'sun'];
                var dayKey = dayNames.indexOf(dayName);

                availableDays = availableDays.replaceAt(dayKey, availableDays[dayKey] == '1' ? '0' : '1');

                ajaxAPiHandle.post(ACTION_URL, {action: 'set_available_days', seriesId: series.series_ID, purchasedId: purchase.purchased_ID, days: availableDays}).then(function (res) {
                    if (res.status == true){
                        domTarget.toggleClass('active-day');
                    }
                    else {
                        availableDays = availableDays.replaceAt(dayKey, availableDays[dayKey] == '1' ? '0' : '1');
                    }
                })
            }
            var updateCoverImage = function (input) {
                if (input.files && input.files[0]) {
                    var sendData = new FormData();
                    sendData.append('action', 'update_cover_image');
                    sendData.append('seriesId', series.series_ID);
                    sendData.append('src', input.files[0]);

                    ajaxAPiHandle.multiPartPost(ACTION_URL, sendData).then(function (res) {
                        if (res.status){
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    });
                }
            }
            var bindEvents = function () {
                domRoot.find('.available-days-list > li > .icon-wrapper').click(function () {
                    toggleAvailableDay($(this).data('day'), $(this).parent());
                });
                domPostsList.on('sortupdate', function (e, helper) {
                    var currentHdl = helper.item.data('controlHandle');
                    var parentHdl = helper.item.parents('li').data('controlHandle');
                    parentHdl = parentHdl ? parentHdl : virtualRootHandle;
                    currentHdl.updateParent(parentHdl);
                    parentHdl.reOrderChilds();
                });
                domPostsList.on('sortexpand', function (e, helper) {
                    var domT = domPostsList.find('li.mjs-nestedSortable-hovering');
                    var hdl = domT.data('controlHandle');
                    hdl.expandChild();
                });
                domRoot.find('.open-add-items').click(function () {
                    openCreateSeriesContent({});
                });
                domRoot.find('.add-from-search').click(function () {
                    if (isFirstAddPostOpen) {
                        addPostsHandle.setSearchTerm(initialSeries.strSeries_title).then(function () {
                            $('.nav.nav-tabs li:last-child a[href="#add-items-tab"]').tab('show');
                            isFirstAddPostOpen = false;
                        });
                    }
                    else {
                        $('.nav.nav-tabs li:last-child a[href="#add-items-tab"]').tab('show');
                    }
                });
                domRoot.find('.cover-image-wrapper [name="cover_img"]').change(function () {
                    updateCoverImage(this);
                });
                domAddLinks.find('[data-node-type]').click(function () {
                    var sets = {};
                    sets.nodeType = $(this).attr('data-node-type');
                    if (sets.nodeType == 'post') {
                        sets.type = $(this).attr('data-post-type');
                    }
                    openCreateSeriesContent(sets);
                });
                domAddLinks.find('[data-tab]').click(function () {
                    var sets = {};
                    sets.tab = $(this).attr('data-tab');
                    openCreateSeriesContent(sets);
                });
                domRoot.find('.charge-helper a').click(function (e) {
                    localStorage.setItem('walden_start_page_stripe_connect', 'editseries?id=' + series.series_ID);
                });

                domRoot.find('[name="boolSeries_charge"]').change(function () {
                    if (isStripeAvailable) {
                        var checked = $(this).prop('checked');
                        var v = checked ? 1 : 0;
                        ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: series.series_ID, sets: {boolSeries_charge: v}}).then(function () {
                            if (checked) {
                                domRoot.find('.series-charge-wrapper').addClass('charged');
                                domRoot.find('.charge-price').removeAttr('disabled');
                                domRoot.find('.posts-container.jstree').addClass('charged');
                                series.boolSeries_charge = 1;
                            }
                            else {
                                domRoot.find('.series-charge-wrapper').removeClass('charged');
                                domRoot.find('.charge-price').attr('disabled', true);
                                domRoot.find('.posts-container.jstree').removeClass('charged');
                                series.boolSeries_charge = 0;
                            }
                            bindData();
                        });
                    }
                    else {
                        if ($(this).prop('checked')) {
                            $(this).prop('checked', false).change();
                        }
                        else {
                            swal("Connect your account");
                        }
                    }
                });
                domRoot.find('[name="boolSeries_affiliated"]').change(function () {
                    var charged = domRoot.find('[name="boolSeries_charge"]').prop('checked');
                    if (charged) {
                        var checked = $(this).prop('checked');
                        var v = checked ? 1 : 0;
                        ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: series.series_ID, sets: {boolSeries_affiliated: v}}).then(function () {
                            if (checked) {
                                domRoot.find('.affiliate-settings').addClass('affiliated');
                                domRoot.find('[name="intSeries_affiliate_percent"]').removeAttr('disabled');
                                domRoot.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('enable');
                                series.boolSeries_affiliated = 1;
                            }
                            else {
                                domRoot.find('.affiliate-settings').removeClass('affiliated');
                                domRoot.find('[name="intSeries_affiliate_percent"]').attr('disabled', true);
                                domRoot.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                                series.boolSeries_affiliated = 0;
                            }
                            bindData();
                        });
                    }
                    else {
                        domRoot.find('[name="intSeries_affiliate_percent"]').attr('disabled', true);
                        domRoot.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                        if ($(this).prop('checked')) {
                            $(this).prop('checked', false).change();
                            swal("Please charge for this series");
                        }
                    }
                });
                domRoot.find('[name="intSeries_price"]').change(function () {
                    var v = $(this).val();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: series.series_ID, sets: {intSeries_price: v}}).then(function () {
                        series.intSeries_price = v;
                        bindData();
                    })
                });
                domRoot.find('[name="intSeries_affiliate_percent"]').change(function () {
                    var v = $(this).val();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'update', where: series.series_ID, sets: {intSeries_affiliate_percent: v}}).then(function () {
                        series.intSeries_affiliate_percent = v;
                        bindData();
                    })
                });
                domRoot.find('[name="strSeries_description"]').change(function () {
                    var v = $(this).val();
                    ajaxAPiHandle.apiPost('Series.php', {action: 'update', sets: {strSeries_description: v}, where: series.series_ID});
                });
            }

            var openCreateSeriesContent = function (sets) {
                var vForm = $('<form action="createseriescontent" method="post" target="_blank"></form>').hide().appendTo('body');
                vForm.append('<input name="id" value="'+ initialSeries.series_ID +'">');
                var inputs = '';
                for (var k in sets) {
                    inputs += '<input name="'+ k +'" value="' + sets[k] +'">';
                }
                vForm.append($(inputs));
                vForm.submit().remove();
            }

            var FooterClass = function () {

                var domFooter;
                var layout;
                var fields;
                var viewVersion = "right-panel";
                var html = '';
                var css = '';
                var js = '';
                var editorHandle;

                var bindEvents = function () {
                    domFooter.find('#select-layout .item-icon-wrapper').click(function () {
                        layout = $(this).data('layout');
                        $('[data-toggle="tab"][href="#information-display"]').tab('show');
                        switch (layout) {
                            case 'grid':
                                viewVersion = 'grid';
                                var uri = BASE_URL +'/viewexperience_embed?viewVersion=grid&id='+ initialSeries.series_ID +'&postViewVersion=popup';
                                if (typeof AFFILIATE_ID !== "undefined" && AFFILIATE_ID !== -1) {
                                    uri += '&affiliate_id=' + AFFILIATE_ID;
                                }
                                html = '<blockquote class="walden-embedly-card">\n' +
                                    '    <h4>\n' +
                                    '<!-- set postViewVersion with one of popup, turbotax and default or you can set on Javascript Tab-->\n' +
                                    '        <a href="'+ uri +'">'+ initialSeries.strSeries_title +'</a>\n' +
                                    '    </h4>\n' +
                                    '    <p>'+ initialSeries.strSeries_description +'</p>\n' +
                                    '</blockquote>\n' +
                                    '<script async src="'+ BASE_URL + '/assets/js/yevgeny/embed-platform.js?version=1" charset="UTF-8"></script>';
                                css = '/* content wrapper */\n' +
                                    'html body .site-wrapper main.main-content{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top left welcome back */\n' +
                                    'html body .site-wrapper main.main-content .content-inner .content-header .welcome{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid wrapper */\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid{\n' +
                                    '    \n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid item */\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li{\n' +
                                    '    \n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid item inner*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid item title*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .title-wrapper .item-title{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid item button wrapper*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .buttons-wrapper{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid item button*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner .buttons-wrapper .btn.btn-circle{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* grid item left bottom line*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner .posts-grid .tt-grid-wrapper ul.tt-grid li .item-inner > aside .bottom-line{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* nav footer */\n' +
                                    'html body .site-wrapper main.main-content .content-inner footer.content-footer{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer nav(back, continue) wrapper*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer nav back*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper .step-back{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer nav continue*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner footer.content-footer .nav-wrapper .step-next{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer powered by*/\n' +
                                    'html body .site-wrapper main.main-content .content-inner footer.content-footer .powered-by{\n' +
                                    '    \n' +
                                    '}\n';
                                js = '// you can set range 1 ~ 5;\n' +
                                    'const COLS_COUNT = 5;\n' +
                                    '\n' +
                                    '// you can set range 1+;\n' +
                                    'const ROWS_COUNT = 2;\n' +
                                    '\n' +
                                    '// you can set one of popup, turbotax, default\n' +
                                    '// var postViewVersion = \'popup\';';
                                break;
                            case 'list':
                                viewVersion = 'popup';
                                css = '/* site wrapper */\n' +
                                    '.walden-site .site-wrapper {\n' +
                                    '    background: white;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel */\n' +
                                    '.walden-site .site-wrapper .left-panel{\n' +
                                    '    background-color: white;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top left welcome back message */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .welcome{\n' +
                                    '    color: inherit;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top left login btn */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .login-btn {\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel tree list */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel tree list row */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel tree list row meta info*/\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row .post-meta-info{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* popup widget */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .view-widget{\n' +
                                    '    background-color: transparent;\n' +
                                    '}';
                                break;
                            case 'gallery':
                                css = '/* content wrapper */\n' +
                                    'html body .site-wrapper .main-content{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top left welcome back */\n' +
                                    'html body .site-wrapper .main-content .content-inner .content-header .welcome{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* gallery list wrapper */\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* gallery item */\n' +
                                    '\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* gallery item inner*/\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* gallery item header */\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* gallery item header day*/\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header .day-wrapper{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* gallery item header title*/\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-header .post-title{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '\n' +
                                    '/* gallery item body(description)*/\n' +
                                    'html body .site-wrapper .main-content .content-inner .posts-section .items-container .slick-list .item.slick-slide .item-inner .item-body{\n' +
                                    '    \n' +
                                    '}';
                                viewVersion = 'gallery';
                                break;
                            case 'guided':
                                viewVersion = "right-panel";
                                css = '/* site wrapper */\n' +
                                    '.walden-site .site-wrapper {\n' +
                                    '    background: white;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel */\n' +
                                    '.walden-site .site-wrapper .left-panel{\n' +
                                    '    background-color: white;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top left welcome back message */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .welcome{\n' +
                                    '    color: inherit;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top left login btn */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .login-btn {\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel tree list */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel tree list row */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* left panel tree list row meta info*/\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .left-panel .posts-container.jstree .posts-list .post-row .post-meta-info{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '\n' +
                                    '/* right panel */\n' +
                                    '\n' +
                                    '.walden-site .site-wrapper .right-panel{\n' +
                                    '    background-color: #f8f7f9;\n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top right saved money wrapper */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top right saved money amount */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved .money-amount{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* top right saved money text */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .money-saved .w-name{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel blog wrapper */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel blog image wrapper */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-image-wrapper{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel blog image */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-image-wrapper .post-img{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel blog title */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-title{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel blog body (description) */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .blog-content-wrapper .post-body{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer */\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer nav(back, continue) wrapper*/\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer nav back*/\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper .step-back{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer nav continue*/\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .nav-wrapper .step-next{\n' +
                                    '    \n' +
                                    '}\n' +
                                    '\n' +
                                    '/* right panel footer powered by*/\n' +
                                    '.walden-site .site-wrapper .right-panel .right-panel-inner .right-panel-footer .powered-by{\n' +
                                    '    \n' +
                                    '}';
                                break;
                        }
                    });
                    domFooter.find('#information-display ul li .icon-wrapper').click(function () {
                        $(this).parents('li').toggleClass('active-item');
                    });
                    domFooter.find('.go-to-codepen').click(function () {
                        $('[data-toggle="tab"][href="#code-editor"]').tab('show');
                    });
                    $('[data-toggle="tab"][href="#code-editor"]').on('shown.bs.tab', function () {
                        var url = BASE_URL + '/viewexperience_embed?viewVersion='+ viewVersion +'&id=' + initialSeries.series_ID;
                        if (typeof AFFILIATE_ID !== 'undefined' && AFFILIATE_ID !== -1) {
                            url += '&affiliate_id=' + AFFILIATE_ID;
                        }
                        var embedCode = html ? html : generateEmbedCode(url, initialSeries.strSeries_title, initialSeries.strSeries_description);
                        editorHandle.setHtml(embedCode);
                        editorHandle.setCss(css);
                        editorHandle.setJs(js);
                        // editorHandle.reloadPreview();
                    });
                }

                var generateEmbedCode = function (url, title, des) {
                    var embedCode = '<blockquote class="walden-embedly-card">\n' +
                        '    <h4>\n' +
                        '        <a href="'+ url +'">'+ title +'</a>\n' +
                        '    </h4>\n' +
                        '    <p>' + des + '</p>\n' +
                        '</blockquote>\n' +
                        '<script async src="'+ BASE_URL +'/assets/js/yevgeny/embed-platform.js?version=1.0" charset="UTF-8"></script>';
                    return embedCode.replace(/&amp;/g, '&');
                }

                this.init = function () {
                    domFooter = domRoot.find('.main-content-footer');
                    fields = {
                        sTitle: true,
                        sDescription: true,
                        sImage: true,
                        pTitles: true,
                        pPreview: true,
                        pFullText: true,
                        pImage: true,
                    };
                    editorHandle = new EmbedEditorClass(domFooter.find('.component.embed-editor'));
                    editorHandle.init();
                    bindEvents();
                }
            }

            var bindData = function () {
                if (series.boolSeries_charge) {
                    if (series.boolSeries_affiliated) {
                        var ownerMoney = series.intSeries_price * (100 - parseInt(series.intSeries_affiliate_percent)) / 100;
                        ownerMoney = parseInt(ownerMoney);
                        domRoot.find('.money-expects .charge-field.field-value').html('$' + ownerMoney);

                        var affiliateMoney = series.intSeries_price - ownerMoney;
                        affiliateMoney = parseInt(affiliateMoney);
                        domRoot.find('.money-expects .affiliate-field.field-value').html('$' + affiliateMoney);
                    }
                    else {
                        domRoot.find('.money-expects .charge-field.field-value').html('$' + series.intSeries_price);
                        domRoot.find('.money-expects .affiliate-field.field-value').html('NO Affiliated');
                    }
                }
                else {
                    domRoot.find('.money-expects .charge-field.field-value').html('No Charged');
                    domRoot.find('.money-expects .affiliate-field.field-value').html('NO Affiliated');
                }
            }

            this.addPostRow = function (post) {
                var postRowHandle = new PostRowClass(viewFormat(post));
                postRowHandle.init();
                postRowHandle.setParent(virtualRootHandle);
                postRowHandle.appendTo();
                postRowHandles.push(postRowHandle);
                return postRowHandle;
            }

            this.deleteInFront = function (id) {
                postRowHandles.forEach(function (hdl, i) {
                    if (hdl.getId() == id) {
                        hdl.removeDom();
                        postRowHandles.splice(i, 1);
                    }
                });
            }

            this.makeSortable = function () {

                var isAllowed = function(placeholder, placeholderParent, currentItem) {
                    var currentHandle = currentItem.data('controlHandle');
                    var parentHandle = placeholderParent ? placeholderParent.data('controlHandle') : false;

                    if (isSeriesMine) {
                        switch (currentHandle.nodeType()) {
                            case 'post':
                                if (parentHandle) {
                                    return parentHandle.nodeType() !== 'post';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'menu':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'path' || parentHandle.nodeType() === 'root';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'path':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'menu';
                                }
                                else {
                                    return false;
                                }
                                break;
                        }
                        return true;
                    }
                    else {
                        switch (currentHandle.nodeType()) {
                            case 'post':
                                if (parentHandle) {
                                    return parentHandle.nodeType() !== 'post';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'menu':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'path' || parentHandle.nodeType() === 'root';
                                }
                                else {
                                    return true;
                                }
                                break;
                            case 'path':
                                if (parentHandle) {
                                    return parentHandle.nodeType() === 'menu';
                                }
                                else {
                                    return false;
                                }
                                break;
                        }
                        return true;
                    }
                }
                var options = {
                    appendTo: domRoot.find('.posts-container'),
                    placeholder: 'ui-state-highlight',
                    // containment: '.page.site-wrapper',
                    items: 'li',
                    protectRoot: true,
                    forcePlaceholderSize: true,
                    handle: '.drag-icon',
                    helper:	'clone',
                    opacity: .6,
                    revert: 250,
                    tabSize: 25,
                    tolerance: 'pointer',
                    toleranceElement: '> .post-row-inner',
                    maxLevels: 0,
                    isTree: true,
                    expandOnHover: 1000,
                    doNotClear: true,
                    startCollapsed: true,
                    listType: 'ul',
                    expandedClass: 'jstree-open',
                    collapsedClass: 'jstree-closed',
                    isAllowed: isAllowed,
                };
                if (!isSeriesMine) {
                    options.disableParentChange = true;
                }
                domPostsList.nestedSortable(options);
            }

            this.init = function () {
                virtualRootData = {
                    id: 0,
                    title: '',
                    image: '',
                    type: 7,
                    order: 1,
                    nodeType: 'root',
                    parent: -1,
                    status: 'publish',
                    free: 0,
                    origin: {
                        intClientSubscription_post_ID: 0,
                    }
                };

                virtualRootHandle = new PostRowClass(virtualRootData);
                virtualRootHandle.init();
                virtualRootHandle.appendTo();

                // posts.forEach(function (post) {
                //     self.addPostRow(post);
                // });
                self.makeSortable();

                footerHandle = new FooterClass();
                footerHandle.init();

                series.boolSeries_charge = parseInt(series.boolSeries_charge);
                series.boolSeries_affiliated = parseInt(series.boolSeries_affiliated);
                bindData();
                domRoot.find('[name="intSeries_affiliate_percent"]').bootstrapSlider ({
                    handle: 'square',
                    min: 0,
                    max: 100,
                    value: parseInt(series.intSeries_affiliate_percent),
                });
                if (series.boolSeries_affiliated) {
                    domRoot.find('.affiliate-settings').addClass('affiliated');
                    domRoot.find('[name="intSeries_affiliate_percent"]').removeAttr('disabled');
                    domRoot.find('[name="intSeries_affiliate_percent"]').val(parseInt(series.intSeries_affiliate_percent));
                }
                else {
                    domRoot.find('.affiliate-settings').removeClass('affiliated');
                    domRoot.find('[name="intSeries_affiliate_percent"]').attr('disabled', true);
                    domRoot.find('[name="intSeries_affiliate_percent"]').bootstrapSlider('disable');
                }
                domRoot.find('[data-toggle="popover"]').popover();
                bindEvents();
            }
        }

        this.init = function () {

            mainHandle = new MainClass();
            mainHandle.init();

            addPostsHandle = new SeriesSearchPostsClass($('.series-search-posts.component'), {isSeriesMine: isSeriesMine, series: initialSeries, purchased: initialPurchase});
            addPostsHandle.init();

            addPostsHandle.afterAdd(function (data) {
                mainHandle.addPostRow(data);
            });
            addPostsHandle.afterDelete(function (id) {
                mainHandle.deleteInFront(id);
            });

            if (PANEL == 'addNew'){
                addPostsHandle.setSearchTerm(initialSeries.strSeries_title);
            }
            else {
            }
        }
    }

    var editSeriesHandle = new EditSeriesClass();
    editSeriesHandle.init();
})();