(function(){
    'use strict';

    var is_loading_controlled_in_local = false;

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    var ViewSeriesClass = function () {

        var domLeftPanel;
        var mainHandle, authHandle, rightHandle;
        var afterGlowReady, leftAfterGlowReady, rightAfterGlowReady;
        var videoCnt = 1;

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            paths: 'paths',
            free: 'boolPost_free'
        };

        var subscriptionFiels = {
            id: 'clientsubscription_ID',
            title: 'strClientSubscription_title',
            body: 'strClientSubscription_body',
            image: 'strClientSubscription_image',
            type: 'intClientSubscriptions_type',
            order: 'intClientSubscriptions_order',
            nodeType: 'strClientSubscription_nodeType',
            parent: 'intClientSubscriptions_parent',
            paths: 'paths',
            completed: 'boolCompleted',
        };

        var viewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                if (from === 'post') {
                    fk = helperHandle.keyOfVal(postFields, k);
                    if (fk) { fData[fk] = data[k]; }
                }
                else {
                    fk = helperHandle.keyOfVal(subscriptionFiels, k);
                    if (fk) { fData[fk] = data[k]; }
                }
            }
            fData.origin = data;
            return fData;
        }
        var postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }

        var bindEvents = function () {
            domLeftPanel.find('.collapse-wrapper').click(function () {
                domLeftPanel.toggleClass('collapsed');
            });
        }

        var MainClass = function () {

            var domRoot = $('.posts-container');
            var domPostsList = $('.posts-container .posts-list');

            var series = initialSeries, audioCnt = 1;

            var viewWidgetHandle;
            var postRowHandles = [], virtualRootHandle, virtualRootData;
            var self = this;

            var FormFieldClass = function (domField) {
                var domValue;
                var field, answer = false;

                var bindEvents = function () {
                    switch (field.strFormField_type) {
                        case 'text':
                            domValue.blur(function () {
                                updateAnswer();
                            });
                            break;
                        case 'checkbox':
                            domValue.find('input').change(function () {
                                updateAnswer();
                            });
                            break;
                    }
                }
                var updateAnswer = function () {
                    if (CLIENT_ID == -1) {
                        return false;
                    }
                    var sets = {};
                    is_loading_controlled_in_local = true;
                    domField.addClass('status-loading');
                    switch (field.strFormField_type) {
                        case 'text':
                            sets.strFormFieldAnswer_answer = domValue.html();
                            break;
                        case 'checkbox':
                            sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                            break;
                    }
                    if (answer) {
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                            answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                        });
                    }
                    else {
                        $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                        });
                    }
                    is_loading_controlled_in_local = false;
                }
                var bindData = function () {
                    formFields.forEach(function (formField) {
                        if (formField.formField_ID == domField.attr('data-form-field')) {
                            field = formField;
                        }
                    });
                    formFieldAnswers.forEach(function (formFieldAnswer) {
                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                            answer = formFieldAnswer;
                        }
                    });
                    if (answer) {
                        switch (field.strFormField_type) {
                            case 'text':
                                domValue.html(answer.strFormFieldAnswer_answer);
                                break;
                            case 'checkbox':
                                domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                                break;
                        }
                    }
                }
                this.init = function () {
                    domValue = domField.find('.form-field-value');
                    domField.find('[data-toggle="popover"]').popover({placement: 'bottom'});
                    bindData();
                    bindEvents();
                }
            }

            var PostRowClass = function (postData) {

                var domRow, domChildList, domInnerWrp, domBody, domBodyInner, domUsedBody, domThumb, domTypeThumb, domMeta;
                var domLoadMore;
                var self = this, parentHandle = false;

                var isChildsLoaded = false, metaData = {};

                var childHandles = [];
                var bindEvents = function () {
                    domInnerWrp.find('.jstree-icon.jstree-ocl').click(function () {
                        if (domRow.hasClass('jstree-open') && childHandles.length && self.nodeType() !== 'post') {
                            self.collapseChild();
                        }
                        else {
                            self.expandChild();
                        }
                    });
                    if (postData.nodeType === 'post') {
                        domInnerWrp.find('.post-title').click(function () {
                            // viewWidgetHandle.view(postData);
                            if (viewVersion === 'right-panel') {
                                rightHandle.setData(postData);
                            }
                            else {
                                viewWidgetHandle.view(postData);
                            }
                        });
                        switch (parseInt(postData.type)) {
                            case 0:
                                domTypeThumb.find('.thumb_play').click(function () {
                                    if (viewVersion === 'right-panel') {
                                        rightHandle.setData(postData);
                                    }
                                    else {
                                        viewWidgetHandle.view(postData);
                                    }
                                });
                                break;
                            case 2:
                                domTypeThumb.find('.thumb_play').click(function () {
                                    if (viewVersion === 'right-panel') {
                                        rightHandle.setData(postData);
                                    }
                                    else {
                                        viewWidgetHandle.view(postData);
                                    }
                                });
                                break;
                            case 8:
                                domTypeThumb.click(function () {
                                    if (viewVersion === 'right-panel') {
                                        rightHandle.setData(postData);
                                    }
                                    else {
                                        viewWidgetHandle.view(postData);
                                    }
                                });
                                break;
                            case 7:
                                domTypeThumb.click(function () {
                                    if (viewVersion === 'right-panel') {
                                        rightHandle.setData(postData);
                                    }
                                    else {
                                        viewWidgetHandle.view(postData);
                                    }
                                });
                                break;
                        }
                    }
                    else {
                        domLoadMore.click(function () {
                            loadMore();
                        })
                    }
                }

                var loadChilds = function () {
                    childHandles = [];
                    domRow.addClass('jstree-loading');
                    is_loading_controlled_in_local = true;
                    // domChildList.empty();

                    var resPromise, ajaxData;
                    ajaxData = {
                        action: 'get_serial_items',
                        where: {intPost_series_ID: initialSeries.series_ID, intPost_parent: postData.id},
                        limit: 10,
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData, false);
                    return resPromise.then(function (res) {

                        var childPosts = res.data;
                        var promises = [];

                        if (childPosts.length < ajaxData.limit) {
                            domLoadMore.remove();
                        }

                        childPosts.forEach(function (childPost) {
                            var fData = viewFormat(childPost);
                            promises.push(self.addChild(fData));
                        });

                        isChildsLoaded = true;
                        domRow.removeClass('jstree-loading');
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        is_loading_controlled_in_local = false;

                        if (!childHandles.length) {
                            domRow.addClass('jstree-leaf');
                        }
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve(true);
                            }, 200);
                        });
                    });
                }

                var loadMore = function () {
                    domLoadMore.addClass('jstree-loading');
                    is_loading_controlled_in_local = true;
                    var lastChild = childHandles[childHandles.length - 1].data();
                    var ajaxData = {
                        action: 'get_serial_items',
                        where: {intPost_series_ID: initialSeries.series_ID, intPost_parent: lastChild.parent, 'intPost_order >': lastChild.order},
                        select: helperHandle.array_keys(postFormat(postData)),
                        limit: 10,
                    };
                    var resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData, false);
                    return resPromise.then(function (res) {

                        var childPosts = res.data;
                        var promises = [];
                        if (childPosts.length < ajaxData.limit) {
                            domLoadMore.remove();
                        }
                        childPosts.forEach(function (childPost) {
                            var fData = viewFormat(childPost);
                            promises.push(self.addChild(fData));
                        });

                        isChildsLoaded = true;
                        domLoadMore.removeClass('jstree-loading');
                        is_loading_controlled_in_local = false;

                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve(true);
                            }, 200);
                        });
                    });
                }

                var bindData = function () {
                    var displayTitle = postData.title;
                    // displayTitle = displayTitle.length > 20 ? displayTitle.substr(0, 20) + '...' : displayTitle.substr(0, 20);
                    domInnerWrp.find('.post-title').html(displayTitle);

                    domRow.removeClass('node-type-post node-type-path node-type-menu').addClass('node-type-' + postData.nodeType);
                    domRow.removeClass('jstree-leaf');
                    domRow.removeClass('mjs-nestedSortable-leaf');
                    domRow.removeClass('mjs-nestedSortable-branch');
                    domRow.removeClass('node-status-publish node-status-draft');

                    if (postData.nodeType == 'post') {
                        isChildsLoaded = true;
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-leaf');
                        self.expandChild();
                    }
                    else if (isChildsLoaded && !childHandles.length) {
                        domRow.addClass('jstree-leaf');
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    else {
                        domRow.addClass('mjs-nestedSortable-branch');
                    }
                    switch (postData.nodeType) {
                        case 'post':
                            switch ( parseInt(postData.type) ) {
                                case 0:
                                    domTypeThumb.css('background-image', 'url("'+ postData.image +'")');
                                    domMeta.find('.post-type-info').html('Audio');
                                    getAudioDuration().then(function (v) {
                                        domMeta.find('.post-dyn-info').html(formatTimeFromSeconds(v));
                                    });
                                    break;
                                case 2:
                                    domTypeThumb.css('background-image', 'url("'+ postData.image +'")');
                                    if (helperHandle.getApiTypeFromUrl(postData.body) === 'local') {
                                        domMeta.find('.post-type-info').html('Video');
                                    }
                                    else {
                                        domMeta.find('.post-type-info').html('Youtube');
                                    }
                                    getVideoDuration().then(function (v) {
                                        domMeta.find('.post-dyn-info').html(formatTimeFromSeconds(v));
                                    });
                                    break;
                                case 8:
                                    domTypeThumb.css('background-image', 'url("'+ postData.image +'")');
                                    domMeta.find('.post-type-info').html('Image');
                                    break;
                                case 7:
                                    domTypeThumb.css('background-image', 'url("'+ postData.image +'")');
                                    domMeta.find('.post-type-info').html('Text');
                                    break;
                                case 10:
                                    domTypeThumb.css('background-image', 'url("'+ postData.image +'")');
                                    domMeta.find('.post-type-info').html('Form Fields');
                                    break;
                            }
                            break;
                        case 'menu':
                            domInnerWrp.find('.expand-col').remove();
                            domInnerWrp.find('.post-body').remove();
                            domInnerWrp.find('.item_thumb.type-menu').attr('src', BASE_URL + '/assets/images/global-icons/tree/list.svg');
                            break;
                        case 'path':
                            domInnerWrp.find('.expand-col').remove();
                            domInnerWrp.find('.post-body').remove();
                            domInnerWrp.find('.item_thumb.type-path').attr('src', BASE_URL + '/assets/images/global-icons/tree/path/'+ (postData.meta.domOrder) +'.svg');
                            break;
                        case 'root':
                            domInnerWrp.find('.expand-col').remove();
                            domInnerWrp.find('.post-body').remove();
                            if (helperHandle.isAbsUrl(postData.image)) {
                                domInnerWrp.find('.item_thumb.type-root').attr('src', postData.image);
                            }
                            else {
                                domInnerWrp.find('.item_thumb.type-root').attr('src', BASE_URL + '/' + postData.image);
                            }
                            break;
                    }
                }

                var formatTimeFromSeconds = function (ss) {
                    var d = new Date(ss * 1000);
                    var h = d.getHours() - 1;
                    var m = d.getMinutes();
                    var s = d.getSeconds();
                    if (h) {
                        return h + ':' + m + ':' + s;
                    }
                    return m + ':' + s;
                }

                var getVideoDuration = function () {
                    var resolved = false;
                    return new Promise(function (resolve) {
                        var tpDom = $('<video class="video-js" autoplay height="100" width="100" style="position: fixed; left: -1000px; bottom: -1000px; opacity: 0;"></video>').appendTo('body');
                        if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                            var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                            tpDom.attr('data-setup', JSON.stringify(setup));
                        }
                        else {
                            tpDom.append('<source src="'+ postData.body +'" />');
                        }
                        videojs(tpDom.get(0)).ready(function(){
                            var myPlayer = this;
                            if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                                myPlayer.on('timeupdate', function () {
                                    if (resolved) {
                                        return;
                                    }
                                    var d = myPlayer.duration();
                                    resolved = true;
                                    resolve(d);
                                    setTimeout(function () {
                                        myPlayer.dispose();
                                    }, 100);
                                });
                            }
                            else {
                                myPlayer.on('loadeddata', function () {
                                    if (resolved) {
                                        return;
                                    }
                                    var d = myPlayer.duration();
                                    resolved = true;
                                    resolve(d);
                                    setTimeout(function () {
                                        myPlayer.dispose();
                                    }, 100);
                                });
                            }
                        });
                    });
                }

                var getAudioDuration = function () {
                    var tpDom = $('<audio src="'+ postData.body +'" controls style="position: fixed; left: -1000px; bottom: -1000px; opacity: 1;"></audio>').appendTo('body');
                    return new Promise(function (resolve) {
                        tpDom.on('loadedmetadata', function () {
                            resolve(tpDom.prop('duration'));
                            tpDom.remove();
                        });
                    });
                }

                this.data = function () {
                    return postData;
                }

                this.expandChild = function () {
                    if (!isChildsLoaded) {
                        loadChilds().then(function () {
                            domRow.removeClass('jstree-closed').addClass('jstree-open');
                            domRow.find('> ul').collapse('show');
                        });
                    }
                    else {
                        domRow.removeClass('jstree-closed').addClass('jstree-open');
                        domRow.find('> ul').collapse('show');
                    }
                }

                this.collapseChild = function () {
                    domRow.removeClass('jstree-open');
                    domRow.find('> ul').collapse('hide');
                    domRow.find('> ul').off('hidden.bs.collapse').one('hidden.bs.collapse', function (e) {
                        domRow.addClass('jstree-closed');
                    });
                }

                this.addChildHandle = function (childHandle) {
                    childHandles.push(childHandle);
                    domRow.removeClass('jstree-leaf');
                }
                this.addChild = function (childPost) {
                    if (postData.nodeType === 'menu') {
                        childPost.meta = {domOrder: childHandles.length + 1};
                    }
                    var hdl = new PostRowClass(childPost);
                    hdl.init();
                    hdl.setParent(self);
                    return hdl.appendTo(domChildList);
                }
                this.setParent = function (parentHdl) {
                    parentHandle = parentHdl;
                    parentHandle.addChildHandle(self);
                }
                this.nodeType = function () {
                    return postData.nodeType;
                }
                this.makeTypeValid = function () {
                    var parentType = parentHandle ? parentHandle.nodeType() : 'path';
                    var myNodeType = self.nodeType();
                    postData.strPost_nodeType = generateValidNodeType(parentType, myNodeType);
                    bindData();
                    childHandles.forEach(function (childHandle) {
                        childHandle.makeTypeValid();
                    });
                }
                this.setOrder = function (newOrder) {
                    postData.order = newOrder;
                }
                this.getOrder = function () {
                    return postData.order;
                }
                this.getDomPosition = function () {
                    return domRow.index();
                }
                this.getId = function () {
                    return postData.id;
                }
                this.removeDom = function () {
                    domRow.remove();
                }
                this.focus = function () {
                    domRow.addClass('focus');
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('focus');
                            resolve();
                        }, 700);
                    });
                }
                this.getDom = function () {
                    return domRow;
                }
                this.appendTo = function (domList) {
                    if (!domList) {
                        if (parentHandle) {
                            domList = parentHandle.getDom().find('> ul');
                        }
                        else {
                            domList = domPostsList;
                        }
                    }
                    setTimeout(function () {
                        domRow.addClass('appear');
                        if (domList.find('> .post-load-more').length) {
                            domRow.insertBefore(domList.find('> .post-load-more'));
                        }
                        else {
                            domRow.appendTo(domList);
                        }
                    }, 200);
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('appear');
                            resolve();
                        }, 700);
                    });
                }
                this.disappear = function () {
                    setTimeout(function () {
                        domRow.addClass('disappear');
                    }, 200);
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            domRow.removeClass('disappear');
                            domRow.detach();
                            resolve();
                        }, 700)
                    });
                }
                this.init = function () {
                    domRow = domRoot.find('li.post-row.sample').clone().removeClass('sample').removeAttr('hidden');
                    domInnerWrp = domRow.find('> .post-row-inner');
                    domThumb = domInnerWrp.find('.item_thumb_wrap .item_thumb.type-' + postData.nodeType);
                    domMeta = domInnerWrp.find('.post-meta-info');
                    if (postData.nodeType === 'post') {
                        switch (parseInt(postData.type)) {
                            case 0:
                                domInnerWrp.addClass('data-type-audio');
                                domThumb.addClass('data-type-audio');
                                domTypeThumb = domThumb.find('.audio-post');
                                break;
                            case 2:
                                domInnerWrp.addClass('data-type-video');
                                domThumb.addClass('data-type-video');
                                domTypeThumb = domThumb.find('.video-post');
                                break;
                            case 7:
                                domInnerWrp.addClass('data-type-text');
                                domThumb.addClass('data-type-text');
                                domTypeThumb = domThumb.find('.text-post');
                                break;
                            case 8:
                                domInnerWrp.addClass('data-type-image');
                                domThumb.addClass('data-type-image');
                                domTypeThumb = domThumb.find('.image-post');
                                break;
                            case 10:
                                domInnerWrp.addClass('data-type-form');
                                domThumb.addClass('data-type-form');
                                domTypeThumb = domThumb.find('.form-post');
                                break;
                        }
                    }
                    domBody = domInnerWrp.find('.post-body');
                    domBodyInner = domBody.find('.body-inner');
                    bindData();
                    domRow.attr('id', 'sortable-tree-item-' + self.getId());

                    if (viewVersion == 'right-panel') {
                        if (self.nodeType() !== 'root' && !rightHandle.getPostData()) {
                            if (postData.nodeType == 'menu' || postData.nodeType == 'path') {
                            }
                            else {
                                rightHandle.bindData(postData);
                            }
                        }
                    }
                    if (self.nodeType() !== 'post') {
                        domRow.append('<ul class="collapse" style="height: 0px;"></ul>');
                        domChildList = domRow.find('> ul');
                        domLoadMore = domRoot.find('li.sample.post-load-more').clone().removeClass('sample').removeAttr('hidden');
                        domChildList.append(domLoadMore);
                    }
                    bindEvents();
                    domRow.data('controlHandle', self);
                }
            }

            var ViewWidgetClass = function () {
                var domViewWidget, domUsedBody, domHeader, domBodyWrp, domFooter, domTopCtrl;
                var postData, metaData;
                var audioCnt;

                var videoHandle;

                var bindEvents = function () {
                    domHeader.find('.mv_min_control_max, .post-title').parent().click(function () {
                        maximize();
                    });
                    domHeader.find('.mv_min_control_close').parent().click(function () {
                        close();
                    });
                    domTopCtrl.find('.mv_minimize_icon').parent().click(function () {
                        minimize();
                    });
                    domTopCtrl.find('.mv_close_icon').parent().click(function () {
                        close();
                    });
                    domViewWidget.click(function (e) {
                        if (e.target !== this) return;
                        close();
                    });
                };

                var close = function () {
                    destoryBody();
                    domViewWidget.removeClass('active');
                    $('body').css('padding-right', '0px');
                    $('body').css('overflow', 'auto');
                }

                var destoryBody = function () {
                    if (!domUsedBody || !domUsedBody.length) {
                        return false;
                    }
                    switch (parseInt(postData.type)) {
                        case 0:
                            domViewWidget.removeClass('audio-type');
                            break;
                        case 2:
                            videoHandle.dispose();
                            break;
                        case 8:
                            domViewWidget.removeClass('image-type');
                            break;
                        default:
                            domViewWidget.removeClass('text-type');
                            break;
                    }
                    domUsedBody.remove();
                    return true;
                }

                var maximize = function () {
                    domViewWidget.addClass('maximize-view');
                    $('body').css('padding-right', window.innerWidth - $("body").prop("clientWidth") + 'px');
                    $('body').css('overflow', 'hidden');
                }

                var minimize = function () {
                    $('body').css('overflow', 'auto');
                    $('body').css('padding-right', '0px');
                    domViewWidget.removeClass('maximize-view');
                }

                this.view = function (newPostData) {
                    if (domViewWidget.hasClass('active')){
                        destoryBody();
                    }
                    domViewWidget.addClass('active');
                    postData = newPostData;
                    domHeader.find('.post-title').html(postData.title);
                    domFooter.find('.post-title').html(postData.title);
                    if (domViewWidget.hasClass('maximize-view')){
                        maximize();
                    }
                    switch (parseInt(postData.type)) {
                        case 0:
                            domViewWidget.addClass('audio-type');
                            domUsedBody = domViewWidget.find('.audio-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.find('.audioplayer-tobe').attr('data-thumb', helperHandle.makeAbsUrl(postData.image));
                            domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                            domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);

                            var settings_ap = {
                                disable_volume: 'off',
                                disable_scrub: 'default',
                                design_skin: 'skin-wave',
                                skinwave_dynamicwaves: 'on',
                                skinwave_mode: 'alternate',
                            };
                            dzsag_init('#audio-' + audioCnt, {
                                'transition':'fade',
                                'autoplay' : 'on',
                                'settings_ap': settings_ap
                            });
                            audioCnt++;
                            break;
                        case 2:
                            domViewWidget.addClass('video-type');
                            domUsedBody = domViewWidget.find('.video-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            var domVideo = domUsedBody.find('video');
                            if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                                var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                domVideo.attr('data-setup', JSON.stringify(setup));
                            }
                            else {
                                domVideo.append('<source src="'+ postData.body +'" />');
                            }
                            videoHandle = videojs(domVideo.get(0), {
                                width: domUsedBody.innerWidth(),
                                height: domUsedBody.innerWidth() * 540 / 960,
                            });
                            break;
                        case 8:
                            domViewWidget.addClass('image-type');
                            domUsedBody = domViewWidget.find('.image-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.append(postData.body);
                            break;
                        case 10:
                            domViewWidget.addClass('form-type');
                            domUsedBody = domViewWidget.find('.form-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });
                            break;
                        default:
                            domViewWidget.addClass('text-type');
                            domUsedBody = domViewWidget.find('.text-widget.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domBodyWrp);
                            domUsedBody.append(postData.body);
                            break;
                    }
                }
                this.init = function () {
                    domViewWidget = $('.view-widget');
                    domHeader = domViewWidget.find('header.widget-header');
                    domBodyWrp = domViewWidget.find('.widget-body');
                    domFooter = domViewWidget.find('footer.widget-footer');
                    domTopCtrl = domViewWidget.find('aside.widget-top-control');
                    metaData = {};
                    bindEvents();
                }
            }

            var generateValidNodeType = function(parentNodeType, myNodeType) {
                var newNodeType = myNodeType;

                switch (parentNodeType) {
                    case 'post':
                        break;
                    case 'menu':
                        newNodeType = 'path';
                        break;
                    case 'path':
                        if (myNodeType === 'path') {
                            newNodeType = 'menu';
                        }
                        break;
                }
                return newNodeType;
            }

            var bindEvents = function () {
                domPostsList.on('sortexpand', function (e, helper) {
                    var domT = domPostsList.find('li.mjs-nestedSortable-hovering');
                    var hdl = domT.data('controlHandle');
                    hdl.expandChild();
                });
            };

            this.addPostRow = function (post) {
                var postRowHandle = new PostRowClass(viewFormat(post));
                postRowHandle.init();
                postRowHandle.setParent(virtualRootHandle);
                postRowHandle.appendTo();
                postRowHandles.push(postRowHandle);
                return postRowHandle;
            }

            this.deleteInFront = function (id) {
                postRowHandles.forEach(function (hdl, i) {
                    if (hdl.getId() == id) {
                        hdl.removeDom();
                        postRowHandles.splice(i, 1);
                    }
                });
            }

            this.init = function () {
                virtualRootData = {
                    id: 0,
                    title: series.strSeries_title,
                    body: '',
                    image: series.strSeries_image,
                    type: 7,
                    order: 1,
                    nodeType: 'root',
                    parent: -1,
                    status: 'publish',
                    origin: {
                        intClientSubscription_post_ID: 0,
                    }
                };
                afterGlowReady = Promise.resolve(true);
                leftAfterGlowReady = Promise.resolve(true);
                virtualRootHandle = new PostRowClass(virtualRootData);
                virtualRootHandle.init();
                virtualRootHandle.appendTo();
                virtualRootHandle.expandChild();
                viewWidgetHandle = new ViewWidgetClass();
                viewWidgetHandle.init();
                bindEvents();
            }
        }

        var AuthClass = function () {
            var domAuth, domLoginForm, domRegisterForm;

            function showRegisterForm(){
                $('.loginBox').fadeOut('fast',function(){
                    $('.registerBox').fadeIn('fast');
                    $('.login-footer').fadeOut('fast',function(){
                        $('.register-footer').fadeIn('fast');
                    });
                    $('.modal-title').html('Register with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function showLoginForm(){
                $('#loginModal .registerBox').fadeOut('fast',function(){
                    $('.loginBox').fadeIn('fast');
                    $('.register-footer').fadeOut('fast',function(){
                        $('.login-footer').fadeIn('fast');
                    });

                    $('.modal-title').html('Login with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function openLoginModal(){
                showLoginForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);
            }
            function openRegisterModal(){
                showRegisterForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);

            }

            function shakeModal(errorMsg){
                if (!errorMsg) {
                    errorMsg = 'Invalid email/password combination';
                }
                $('#loginModal .modal-dialog').addClass('shake');
                $('.error').addClass('alert alert-danger').html(errorMsg);
                $('input[type="password"]').val('');
                setTimeout( function(){
                    $('#loginModal .modal-dialog').removeClass('shake');
                }, 1000 );
            }
            var login = function (username, password) {
                $('.loading').show();
                ajaxAPiHandle.pagePost('login.php', {function: 'signin', username: username, password: password}, false).then(function (res) {
                    if (res.result.success) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        ajaxAPiHandle.apiPost('Purchased.php', {action: 'get', where: {intPurchased_series_ID: series.series_ID}}, false).then(function (pRes) {
                            purchased = pRes.data;
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            rightHandle.showWelcome();
                        });
                    }
                    else {
                        shakeModal();
                    }
                });
            }
            var register = function (sets) {
                $('.loading').show();
                var ajaxData = {function: 'register'};
                $.extend(ajaxData, sets);
                ajaxAPiHandle.pagePost('login.php', ajaxData, false).then(function (res) {
                    if (res.result.registered) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        setTimeout(function () {
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            rightHandle.showWelcome();
                        }, 300);
                    }
                    else {
                        shakeModal('something is wrong');
                    }
                });
            }
            var bindEvents = function () {
                $('.big-login').click(function () {
                    openLoginModal();
                });
                $('.show-register').click(function () {
                    showRegisterForm();
                });
                $('.show-login').click(function () {
                    showLoginForm();
                });
                domLoginForm.submit(function () {
                    var username = domLoginForm.find('[name="email"]').val();
                    var password = domLoginForm.find('[name="password"]').val();
                    login(username, password);
                    return false;
                });
                domRegisterForm.submit(function () {
                    var sets = {};
                    sets.email = domRegisterForm.find('[name="email"]').val();
                    sets.f_name = domRegisterForm.find('[name="first_name"]').val();
                    sets.l_name = domRegisterForm.find('[name="last_name"]').val();
                    sets.password = domRegisterForm.find('[name="password"]').val();
                    var confirmPassword = domRegisterForm.find('[name="password_confirmation"]').val();
                    if (sets.password === confirmPassword) {
                        register(sets);
                    }
                    else {
                        shakeModal("Passwords don't match");
                    }
                    return false;
                });
            }
            this.init = function () {
                domAuth = $('#loginModal');
                domLoginForm = domAuth.find('.loginBox form');
                domRegisterForm = domAuth.find('.registerBox form');
                bindEvents();
            }
        }

        var RightPanelClass = function () {

            var domRight;
            var domUsedBody, domBody, domFooter, domVideo;
            var videoHandle;

            var audioCnt = 0, metaData = {};
            var availableTypes = [0, 2, 8, 10];
            var postData, prevPostData;
            var isWelcome = false;

            var self = this, payBlogHandle = null;

            var FormFieldClass = function (domField) {
                var domValue;
                var field, answer = false;

                var bindEvents = function () {
                    switch (field.strFormField_type) {
                        case 'text':
                            domValue.blur(function () {
                                updateAnswer();
                            });
                            break;
                        case 'checkbox':
                            domValue.find('input').change(function () {
                                updateAnswer();
                            });
                            break;
                    }
                }
                var updateAnswer = function () {
                    if (CLIENT_ID == -1) {
                        return false;
                    }
                    var sets = {};
                    is_loading_controlled_in_local = true;
                    domField.addClass('status-loading');
                    switch (field.strFormField_type) {
                        case 'text':
                            sets.strFormFieldAnswer_answer = domValue.html();
                            break;
                        case 'checkbox':
                            sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                            break;
                    }
                    if (answer) {
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                            answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                        });
                    }
                    else {
                        $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                        });
                    }
                    is_loading_controlled_in_local = false;
                }
                var bindData = function () {
                    formFields.forEach(function (formField) {
                        if (formField.formField_ID == domField.attr('data-form-field')) {
                            field = formField;
                        }
                    });
                    formFieldAnswers.forEach(function (formFieldAnswer) {
                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                            answer = formFieldAnswer;
                        }
                    });
                    if (answer) {
                        switch (field.strFormField_type) {
                            case 'text':
                                domValue.html(answer.strFormFieldAnswer_answer);
                                break;
                            case 'checkbox':
                                domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                                break;
                        }
                    }
                }
                this.init = function () {
                    domValue = domField.find('.form-field-value');
                    domField.find('[data-toggle="popover"]').popover();
                    bindData();
                    bindEvents();
                }
            }

            var PathClass = function (path) {
                var domPath, pathNumber;

                var bindEvents = function () {
                    domPath.click(function () {
                        select();
                    });
                }

                var select = function () {
                    var resPromise;
                    var ajaxData = {};

                    if (from === 'post') {
                        if (CLIENT_ID !== -1) {
                            ajaxData.action = 'set_seek';
                            ajaxData.where = clientSeriesView.clientSeriesView_ID;
                            ajaxData.sets = {
                                intCSView_seekParent: path.id,
                            };
                        }
                        else {
                            ajaxData.action = 'select_path';
                            ajaxData.id = path.id;
                        }
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                    }
                    else {
                        ajaxData.action = 'set_subPath';
                        ajaxData.pathId = path.id;
                        ajaxData.purchasedId = purchased.purchased_ID;
                        resPromise = ajaxAPiHandle.pagePost('view', ajaxData);

                    }
                    resPromise.then(function (res) {
                        if (res.status === true && res.data){
                            self.bindData(viewFormat(res.data));
                        }
                        else {
                            swal("Sorry!", "There is no post in this option that you clicked. You will go to next post.").then(function () {
                                if (CLIENT_ID !== -1) {
                                    seekNext();
                                }
                                else {
                                    nextPost();
                                }
                            });
                        }
                    });
                }

                this.init = function () {

                    domPath = domUsedBody.find('.path-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUsedBody.find('.paths-list'));

                    pathNumber = domUsedBody.find('.paths-list > .path-col').length;

                    domPath.find('.img-wrapper img').attr('src', BASE_URL + '/assets/images/global-icons/tree/path/'+ pathNumber +'.svg');


                    var displayTitle = path.title;
                    displayTitle = displayTitle.length > 20 ? displayTitle.substr(0, 50) + '...' : displayTitle.substr(0, 50);

                    var displayDes = path.body;
                    displayDes = displayDes.length > 20 ? displayDes.substr(0, 100) + '...' : displayDes.substr(0, 100);

                    domPath.find('.path-title').html(displayTitle);
                    domPath.find('.path-description').html(displayDes);
                    bindEvents();
                }
            }
            var nextPost = function () {

                var resPromise;
                var ajaxData;

                if (from === 'post') {
                    ajaxData = {
                        action: 'next_post',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                }
                else {
                    ajaxData = {
                        action: 'next_sub',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.pagePost('view', ajaxData);
                }

                return resPromise.then(function (res) {
                    if (res.status){
                        self.bindData(viewFormat(res.data));
                    }
                });
            }
            var prevPost = function () {
                var resPromise;
                var ajaxData;

                if (from === 'post') {
                    ajaxData = {
                        action: 'prev_post',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                }
                else {
                    ajaxData = {
                        action: 'prev_sub',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.pagePost('view', ajaxData);
                }

                return resPromise.then(function (res) {
                    if (res.status){
                        self.bindData(viewFormat(res.data));
                    }
                });
            }

            var seekNext = function () {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_next', CSViewId: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                    self.bindData(viewFormat(res.data));
                });
            }

            var seekPrev = function () {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_prev', CSViewId: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                    self.bindData(viewFormat(res.data));
                });
            }

            var showPrevPost = function () {
                isWelcome = false;
                self.bindData(prevPostData);
            }

            var makeBody = function () {
                var type = parseInt(postData.type);
                type = availableTypes.indexOf(type) !== -1 ? type : 'other';
                var newDomUsedBody;
                domBody.find('> .blog-type:not(.sample)').remove();
                if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                    newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    payBlogHandle = new PayBlogClass(domUsedBody, series);
                    payBlogHandle.onPaid(function (purchasedId) {
                        purchased = purchasedId;
                        makeBody();
                    });
                    payBlogHandle.init();
                }
                else if (postData.nodeType === 'menu') {
                    newDomUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    domUsedBody = newDomUsedBody;
                    // domUsedBody.find('.menu-title').html(postData.title);
                    domUsedBody.find('.menu-description').html(postData.body);

                    postData.paths.forEach(function (path) {
                        var pathHandle = new PathClass(viewFormat(path));
                        pathHandle.init();
                    });
                    domUsedBody.find('.paths-list').addClass('paths-count-' + postData.paths.length);
                }
                else {
                    newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    switch (parseInt(type)){
                        case 0:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.find('.audioplayer-tobe').attr('data-thumb', helperHandle.makeAbsUrl(postData.image));
                            domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                            domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                            var settings_ap = {
                                disable_volume: 'off',
                                disable_scrub: 'default',
                                design_skin: 'skin-wave',
                                skinwave_dynamicwaves: 'off'
                            };
                            dzsag_init('#audio-' + audioCnt, {
                                'transition':'fade',
                                'autoplay' : 'off',
                                'settings_ap': settings_ap
                            });
                            audioCnt++;
                            break;
                        case 2:
                            // domBody.find('> .blog-type:not(.sample)').remove();
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domVideo = domUsedBody.find('video');
                            if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                                var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                domVideo.attr('data-setup', JSON.stringify(setup));
                            }
                            else {
                                domVideo.append('<source src="'+ postData.body +'" />');
                            }
                            videoHandle = videojs(domVideo.get(0), {
                                width: domUsedBody.innerWidth(),
                                height: domUsedBody.innerWidth() * 540 / 960,
                            });
                            break;
                        case 8:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            break;
                        case 10:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });

                            domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                var field = false;
                                var answer = false;
                                formFields.forEach(function (formField) {
                                    if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                        field = formField;
                                    }
                                });
                                formFieldAnswers.forEach(function (formFieldAnswer) {
                                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                        answer = formFieldAnswer;
                                    }
                                });
                                if (field) {
                                    if (answer) {
                                        field.answer = answer.strFormFieldAnswer_answer;
                                    }
                                    else{
                                        field.answer = false;
                                    }
                                }
                                var hdl = new AnsweredFormFieldClass($(d), field);
                                hdl.init();
                            });

                            break;
                        default:
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });

                            domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                var field = false;
                                var answer = false;
                                formFields.forEach(function (formField) {
                                    if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                        field = formField;
                                    }
                                });
                                formFieldAnswers.forEach(function (formFieldAnswer) {
                                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                        answer = formFieldAnswer;
                                    }
                                });
                                if (field) {
                                    if (answer) {
                                        field.answer = answer.strFormFieldAnswer_answer;
                                    }
                                    else{
                                        field.answer = false;
                                    }
                                }
                                var hdl = new AnsweredFormFieldClass($(d), field);
                                hdl.init();
                            });
                            break;
                    }
                }
            }

            var bindEvents = function () {
                domFooter.find('.step-next').click(function () {
                    if (isWelcome) {
                        showPrevPost();
                    }
                    else if (CLIENT_ID !== -1) {
                        seekNext();
                    }
                    else {
                        nextPost();
                    }
                });
                domFooter.find('.step-back').click(function () {
                    if (isWelcome) {
                        showPrevPost();
                    }
                    else if (CLIENT_ID !== -1) {
                        seekPrev();
                    }
                    else {
                        prevPost();
                    }
                })
            }

            var insertCSViewAs = function () {
                var sets = {
                    intCSView_client_ID: CLIENT_ID,
                    intCSView_series_ID: initialSeries.series_ID,
                    intCSView_seekParent: postData.parent,
                    intCSView_seekOrder: postData.order
                };
                return ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'insert', sets: sets}, false).then(function (res) {
                    clientSeriesView = res.data;
                    $('.loading').hide();
                    return res.data;
                });
            }
            this.setData = function (data) {
                if (CLIENT_ID !== -1) {
                    var sets = {
                        intCSView_seekParent: data.parent,
                        intCSView_seekOrder: data.order,
                    };
                    ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'update', where: clientSeriesView.clientSeriesView_ID, sets: sets}).then(function (res) {
                        self.bindData(data);
                    })
                }
                else {
                    self.bindData(data);
                }
            }
            this.getPostData = function () {
                return postData ? postData : false;
            }
            this.showWelcome = function () {
                isWelcome = true;
                prevPostData = postData;
                welcomePost.boolPost_free = 1;
                self.bindData(viewFormat(welcomePost));
                var title = domRight.find('.post-title').html();
                domRight.find('.post-title').html(title.replace('name', clientName));

                var where = {intCSView_client_ID: CLIENT_ID, intCSView_series_ID: initialSeries.series_ID};
                ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'get', where: where}, false).then(function (res) {
                    if (!res.data) {
                        insertCSViewAs();
                    }
                    else {
                        clientSeriesView = res.data;
                        $('.loading').hide();
                    }
                });
                console.log(domUsedBody.find('a[href="http://linktouserlocation.com"]'));
                domUsedBody.find('a[href="http://linktouserlocation.com"]').removeAttr('target').attr('href', 'javascript:;').click(function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_post', CSView: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                        isWelcome = false;
                        self.bindData(viewFormat(res.data));
                    });
                });
            }
            this.bindData = function (data) {
                if (postData) {
                    postData.image = helperHandle.makeAbsUrl(postData.image);
                    domRight.removeClass('content-type-' + postData.type);
                }
                postData = $.extend({}, postData, data);
                if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                    domRight.find('.blog-content-wrapper').hide();
                    domRight.find('.pay-blog-wrapper').show();
                }
                else {
                    domRight.find('.blog-content-wrapper').show();
                    domRight.find('.pay-blog-wrapper').hide();

                    domRight.find('.post-img').attr('src', postData.image);
                    domRight.find('.post-title').html(postData.title);
                    domRight.addClass('content-type-' + postData.type);
                    makeBody();
                }
            }
            this.init = function () {
                domRight = $('.right-panel');
                domFooter = domRight.find('.right-panel-footer');
                domBody = domRight.find('.post-body');
                rightAfterGlowReady = Promise.resolve();
                if (seekPost) {
                    self.bindData(viewFormat(seekPost));
                }
                payBlogHandle = new PayBlogClass(domRight.find('.pay-blog-wrapper .pay-blog'), series);
                payBlogHandle.onPaid(function (purchasedId) {
                    purchased = purchasedId;
                    self.bindData(postData);
                });
                payBlogHandle.init();
                bindEvents();
            }
        }

        this.init = function () {
            domLeftPanel = $('.left-panel');
            mainHandle = new MainClass();
            authHandle = new AuthClass();

            mainHandle.init();

            if (viewVersion == 'right-panel') {
                rightHandle = new RightPanelClass();
                rightHandle.init();
            }
            authHandle.init();
            bindEvents();
            setInterval(function () {
                window.parent.postMessage({name: 'currentHeight', height: $('body').prop('scrollHeight'), embedId: embedId}, '*');
            }, 200);
        }
    }

    var editSeriesHandle = new ViewSeriesClass();
    editSeriesHandle.init();
})();