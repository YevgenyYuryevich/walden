(function () {
    'use strict';
    var ProfileClass = function () {

        var domRoot;
        var newImageFile = false;
        var isSecretChecked = false;

        var bindEvents = function () {
            domRoot.find('[name="image"]').change(function () {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        domRoot.find('.cover-image-wrapper img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                    newImageFile = input.files[0];
                }
            });
            domRoot.find('[name="username"]').keyup(function () {
                domRoot.find('[name="username"]').parents('.form-group').removeClass('wrong-info');
            });
            domRoot.find('[name="hash"]').keyup(function () {
                domRoot.find('[name="hash"]').parents('.form-group').removeClass('wrong-info');
            });
            domRoot.find('[name="username"]').change(function () {
                checkUsernameValid();
            });
            domRoot.find('[name="hash"]').change(function () {
                if ($(this).val()) {
                    checkPasswordValid($(this).val());
                }
            });
            domRoot.find('[name="new_hash"]').keyup(function () {
                if ($(this).val()) {
                    if (!domRoot.find('[name="hash"]').val()) {
                        domRoot.find('[name="hash"]').parents('.form-group').addClass('wrong-info');
                    }
                }
                else if (!domRoot.find('[name="hash"]').val()) {
                    domRoot.find('[name="hash"]').parents('.form-group').removeClass('wrong-info');
                }
            });
            domRoot.find('[name="new_hash"]').change(function () {
                if ($(this).val()) {
                    if (!domRoot.find('[name="hash"]').val()) {
                        domRoot.find('[name="hash"]').parents('.form-group').addClass('wrong-info');
                    }
                }
                else {
                    domRoot.find('[name="hash"]').parents('.form-group').removeClass('wrong-info');
                }
            });
            domRoot.submit(function () {
                update();
                return false;
            });
            domRoot.find('button.cancel').click(function () {
                bindData();
            })
        }

        var checkUsernameValid = function () {
            var v = domRoot.find('[name="username"]').val();
            var ajaxData = {
                action: 'get',
                where: {
                    'id !=': user.id,
                    username: v,
                }
            };
            ajaxAPiHandle.apiPost('User.php', ajaxData).then(function (res) {
                if (res.data) {
                    domRoot.find('[name="username"]').parents('.form-group').addClass('wrong-info');
                }
                else {
                    domRoot.find('[name="username"]').parents('.form-group').removeClass('wrong-info');
                }
            });
        }

        var checkPasswordValid = function (password) {
            var ajaxData = {
                action: 'get_secret',
                where: {
                    id: user.id,
                    hash: password,
                }
            };
            ajaxAPiHandle.apiPost('User.php', ajaxData).then(function (res) {
                if (res.data) {
                    isSecretChecked = true;
                    $.extend(user, res.data);
                    domRoot.find('[name="hash"]').parents('.form-group').addClass('secret-valid');
                    domRoot.find('[name="hash"]').parents('.form-group').removeClass('wrong-info');
                    bindSecretData();
                }
                else {
                    domRoot.find('[name="hash"]').parents('.form-group').addClass('wrong-info');
                }
            });
        }

        var bindData = function () {
            if (user.image) {
                domRoot.find('.profile-avatar').attr('src', user.image);
            }
            domRoot.find('[name="username"]').val(user.username);
            domRoot.find('[name="f_name"]').val(user.f_name);
            domRoot.find('[name="l_name"]').val(user.l_name);
            domRoot.find('[name="phone"]').val(user.phone);
            domRoot.find('[name="email"]').val(user.email);
        }

        var bindSecretData = function () {
        }

        var update = function () {
            var sets = helperHandle.formSerialize(domRoot);
            if (isSecretChecked) {
                if (sets.new_hash) {
                    sets.hash = sets.new_hash;
                }
                else {
                    delete sets.hash;
                }
            }
            else {
                delete sets.hash;
                delete sets.stripeApi_pKey;
                delete sets.stripeApi_cUrl;
            }
            delete sets.new_hash;
            var resPromise = Promise.resolve();
            if (newImageFile) {
                var ajaxData = new FormData();
                ajaxData.append('file', newImageFile);
                ajaxData.append('sets', JSON.stringify({prefix: 'userProfile'}));
                resPromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 100).then(function (res) {
                    sets.image = res.data.url;
                    return true;
                });
            }
            resPromise.then(function () {
                ajaxAPiHandle.apiPost('User.php', {action: 'update', sets: sets}).then(function () {
                    $.extend(user, sets);
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Changes are saved successfully',
                        closeOnClickOutside: false,
                    });
                });
            });
        }

        this.init = function () {
            domRoot = $('form.form-horizontal');
            bindData();
            bindEvents();
        }
    }
    var pageHandle = new ProfileClass();
    pageHandle.init();
})();