var is_loading_controlled_in_local = false;

(function() {
    var viewExpereincePageHandle;
    var ViewExperiencePageClass = function () {
        var postsSectionHandle = null;
        var seriesSectionHandle = null;
        var embedHandle = null;

        var SeriesSectionClass = function () {
            var domRoot = $('.series-section');

            var diamondsHandle;

            var self = this;

            var seriesData = initSeries;

            this.joined = false;
            this.purchased_ID = false;

            var DiamondsClass = function () {
                // from https://uigradients.com

                var gradients = [
                    'linear-gradient(to right, #005aa7, #fffde4)',
                    'linear-gradient(to right, #02111d, #037bb5, #02111d)',
                    'linear-gradient(to right, #ffe259, #ffa751)',
                    'linear-gradient(to top, #acb6e5, #86fde8)',
                    'linear-gradient(to top, #536976, #292e49)',
                    'linear-gradient(to right, #bbd2c5, #536976, #292e49)',
                    'linear-gradient(to right, #b79891, #94716b)',
                    'linear-gradient(to right, #bbd2c5, #536976)',
                    'linear-gradient(to left, #076585, #fff)',
                    'linear-gradient(to left, #00467f, #a5cc82)',
                    'linear-gradient(to left, #1488cc, #2b32b2)',
                    'linear-gradient(to top, #ec008c, #fc6767)',
                    'linear-gradient(to top, #cc2b5e, #753a88)',
                    'linear-gradient(to top, #2193b0, #6dd5ed)',
                    'linear-gradient(to bottom, #e65c00, #f9d423)',
                    'linear-gradient(to top, #2b5876, #4e4376)',
                    'linear-gradient(to right, #314755, #26a0da)',
                    'linear-gradient(to right, #77a1d3, #79cbca, #e684ae)',
                    'linear-gradient(to left, #ff6e7f, #bfe9ff)',
                    'linear-gradient(to right, #e52d27, #b31217)',
                    'linear-gradient(to top, #603813, #b29f94)',
                    'linear-gradient(to top, #16a085, #f4d03f)',
                    'linear-gradient(to left, #d31027, #ea384d)',
                    'linear-gradient(to left, #ede574, #e1f5c4)',
                    'linear-gradient(to left, #02aab0, #00cdac)',
                    'linear-gradient(to top, #da22ff, #9733ee)',
                    'linear-gradient(to left, #348f50, #56b4d3)',
                    'linear-gradient(to bottom, #3ca55c, #b5ac49)',
                    'linear-gradient(to left, #cc95c0, #dbd4b4, #7aa1d2)',
                    'linear-gradient(to bottom, #003973, #e5e5be)',
                    'linear-gradient(to bottom, #e55d87, #5fc3e4)',
                    'linear-gradient(to right, #403b4a, #e7e9bb)',
                    'linear-gradient(to top, #f09819, #edde5d)',
                    'linear-gradient(to top, #ff512f, #dd2476)',
                    'linear-gradient(to right, #aa076b, #61045f)',
                    'linear-gradient(to right, #1a2980, #26d0ce)',
                    'linear-gradient(to bottom, #ff512f, #f09819)',
                    'linear-gradient(to bottom, #1d2b64, #f8cdda)',
                    'linear-gradient(to right, #1fa2ff, #12d8fa, #a6ffcb)',
                    'linear-gradient(to right, #4cb8c4, #3cd3ad)'
                ];

                var domDiamondsWrapper = domRoot.find('.diamonds-wrapper');
                var domDiamonds = domDiamondsWrapper.find('.diamonds-container');

                var posts = initPosts;
                var screenKind = 1;
                var fillCount = 27;
                var size = 300;

                var makeVirtualDiamonds = function(size){
                    var cloneDoms = [];
                    while (size > 0){
                        var cloneDom = domDiamondsWrapper.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                        cloneDom.attr('href', 'javascript:;');
                        var randKey = Math.floor(Math.random() * gradients.length);
                        cloneDom.css('background-image', gradients[randKey]);
                        cloneDoms.push(cloneDom);
                        size--;
                    }
                    return cloneDoms;
                }
                var bindEvents = function () {
                    $(window).resize(function () {
                        var newScreenKind = getScreenKind();
                        if (newScreenKind !== screenKind){
                            domDiamonds.diamonds("destroy");
                            domDiamonds.empty();
                            fillMainDiamonds();
                            fillSides();
                        }
                    });
                }

                var getScreenKind = function () {
                    var windowWidth = $(window).width();

                    if (windowWidth >= 1637){
                        return 1;
                    }
                    else if (windowWidth >= 1320){
                        return 2;
                    }
                    else if (windowWidth >= 1020){
                        return 3;
                    }
                    else if (windowWidth >= 768){
                        return 4;
                    }
                    return 5;
                }
                var shuffle = function(array) {
                    var currentIndex = array.length, temporaryValue, randomIndex;

                    // While there remain elements to shuffle...
                    while (0 !== currentIndex) {

                        // Pick a remaining element...
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex -= 1;

                        // And swap it with the current element.
                        temporaryValue = array[currentIndex];
                        array[currentIndex] = array[randomIndex];
                        array[randomIndex] = temporaryValue;
                    }

                    return array;
                }

                var fillMainDiamonds = function () {
                    screenKind = getScreenKind();
                    switch (screenKind){
                        case 1:
                            size = 300;
                            fillCount = 27;
                            break;
                        case 2:
                            size = 300;
                            fillCount = 21;
                            break;
                        case 3:
                            size = 300;
                            fillCount = 20;
                            break;
                        case 4:
                            size = 200;
                            fillCount = 20;
                            break;
                        case 5:
                            size = 200;
                            fillCount = 15;
                        default:
                            break;
                    }
                    var cloneDoms = [];
                    for (var i = 0; i < fillCount && i < posts.length; i++){
                        var post = posts[i];
                        var cloneDom = domDiamondsWrapper.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                        cloneDom.attr('href', 'view.php?id=' + post.post_ID + '&prevpage=viewexperience');
                        cloneDom.css('background-image', 'url("'+ post.strPost_featuredimage +'")');
                        cloneDoms.push(cloneDom);
                    }

                    var virtualDoms = makeVirtualDiamonds(fillCount - posts.length);
                    cloneDoms = cloneDoms.concat(virtualDoms);

                    cloneDoms = shuffle(cloneDoms);

                    cloneDoms.forEach(function (cloneDom) {
                        cloneDom.appendTo(domDiamonds);
                    });
                    domDiamonds.diamonds({
                        size: size,
                        gap: 5,
                        hideIncompleteRow: false,
                        autoReDraw: false,
                        itemSelector: '.item'
                    });
                }

                var getCloneDomByPos = function (rowNumber, isUpper, offset) {
                    var domRow = domDiamonds.find('.diamond-row-wrap').eq(rowNumber - 1);
                    var domLayer = isUpper ? domRow.find('.diamond-row-upper') : domRow.find('.diamond-row-lower');
                    return domLayer.find('.diamond-box-wrap').eq(offset - 1).clone();
                }

                var fillSides = function () {
                    domDiamondsWrapper.find('.diamond-box-wrap.for-side:not(.sample)').remove();
                    var rowsCount = domDiamonds.find('>.diamonds').find('.diamond-row-wrap').length;

                    var cloneDoms = [];
                    var size = rowsCount * 2;
                    while (size > 0){
                        var cloneDom = domDiamondsWrapper.find('.diamond-box-wrap.for-side.sample').clone().removeClass('sample').removeAttr('hidden');
                        cloneDom.find('.item').attr('href', 'javascript:;');
                        var randKey = Math.floor(Math.random() * gradients.length);
                        cloneDom.find('.item').css('background-image', gradients[randKey]);
                        cloneDoms.push(cloneDom);
                        size--;
                    }

                    var restPosts = posts.slice(fillCount, fillCount + rowsCount * 2);

                    restPosts.forEach(function (post) {
                        var cloneDom = domDiamondsWrapper.find('.diamond-box-wrap.for-side.sample').clone().removeClass('sample').removeAttr('hidden');
                        cloneDom.find('.item').attr('href', 'view.php?id=' + post.post_ID + '&prevpage=viewexperience');
                        cloneDom.find('.item').css('background-image', 'url("'+ post.strPost_featuredimage +'")');
                        cloneDoms.push(cloneDom);
                    });

                    switch (screenKind){
                        case 1:
                            cloneDoms.push(getCloneDomByPos(1, true, 3));
                            cloneDoms.push(getCloneDomByPos(1, true, 4));
                            cloneDoms.push(getCloneDomByPos(1, false, 3));

                            cloneDoms.push(getCloneDomByPos(1, false, 1));
                            cloneDoms.push(getCloneDomByPos(2, true, 1));
                            cloneDoms.push(getCloneDomByPos(2, true, 2));
                            cloneDoms.push(getCloneDomByPos(2, false, 1));

                            cloneDoms.push(getCloneDomByPos(2, false, 4));
                            cloneDoms.push(getCloneDomByPos(3, true, 4));
                            cloneDoms.push(getCloneDomByPos(3, true, 5));
                            cloneDoms.push(getCloneDomByPos(3, false, 4));

                            break;
                        case 2:
                            cloneDoms.push(getCloneDomByPos(1, true, 3));
                            cloneDoms.push(getCloneDomByPos(1, true, 4));
                            cloneDoms.push(getCloneDomByPos(1, false, 3));

                            cloneDoms.push(getCloneDomByPos(1, false, 1));
                            cloneDoms.push(getCloneDomByPos(2, true, 1));
                            cloneDoms.push(getCloneDomByPos(2, true, 2));
                            cloneDoms.push(getCloneDomByPos(2, false, 1));

                            cloneDoms.push(getCloneDomByPos(2, false, 3));
                            cloneDoms.push(getCloneDomByPos(3, true, 3));
                            cloneDoms.push(getCloneDomByPos(3, true, 4));
                            cloneDoms.push(getCloneDomByPos(3, false, 3));

                            break;
                        case 3:
                            cloneDoms.push(getCloneDomByPos(1, true, 2));
                            cloneDoms.push(getCloneDomByPos(1, true, 3));
                            cloneDoms.push(getCloneDomByPos(1, false, 2));

                            cloneDoms.push(getCloneDomByPos(2, false, 1));
                            cloneDoms.push(getCloneDomByPos(3, true, 1));
                            cloneDoms.push(getCloneDomByPos(3, true, 2));
                            cloneDoms.push(getCloneDomByPos(3, false, 1));

                            cloneDoms.push(getCloneDomByPos(3, false, 2));
                            cloneDoms.push(getCloneDomByPos(4, true, 2));
                            cloneDoms.push(getCloneDomByPos(4, true, 3));
                            cloneDoms.push(getCloneDomByPos(4, false, 2));

                            break;
                        case 4:
                            cloneDoms.push(getCloneDomByPos(1, true, 2));
                            cloneDoms.push(getCloneDomByPos(1, true, 3));
                            cloneDoms.push(getCloneDomByPos(1, false, 2));

                            cloneDoms.push(getCloneDomByPos(2, false, 1));
                            cloneDoms.push(getCloneDomByPos(3, true, 1));
                            cloneDoms.push(getCloneDomByPos(3, true, 2));
                            cloneDoms.push(getCloneDomByPos(3, false, 1));

                            cloneDoms.push(getCloneDomByPos(3, false, 2));
                            cloneDoms.push(getCloneDomByPos(4, true, 2));
                            cloneDoms.push(getCloneDomByPos(4, true, 3));
                            cloneDoms.push(getCloneDomByPos(4, false, 2));
                            break;
                        case 5:
                            cloneDoms.push(getCloneDomByPos(1, true, 1));
                            cloneDoms.push(getCloneDomByPos(1, true, 2));
                            cloneDoms.push(getCloneDomByPos(1, false, 1));

                            cloneDoms.push(getCloneDomByPos(2, false, 1));
                            cloneDoms.push(getCloneDomByPos(3, true, 1));
                            cloneDoms.push(getCloneDomByPos(3, true, 2));
                            cloneDoms.push(getCloneDomByPos(3, false, 1));

                            cloneDoms.push(getCloneDomByPos(4, false, 1));
                            cloneDoms.push(getCloneDomByPos(5, true, 1));
                            cloneDoms.push(getCloneDomByPos(5, true, 2));
                            cloneDoms.push(getCloneDomByPos(5, false, 1));
                            break;
                        default:
                            cloneDoms.push(getCloneDomByPos(1, true, 3));
                            cloneDoms.push(getCloneDomByPos(1, true, 4));
                            cloneDoms.push(getCloneDomByPos(1, false, 3));

                            cloneDoms.push(getCloneDomByPos(1, false, 1));
                            cloneDoms.push(getCloneDomByPos(2, true, 1));
                            cloneDoms.push(getCloneDomByPos(2, true, 2));
                            cloneDoms.push(getCloneDomByPos(2, false, 1));

                            cloneDoms.push(getCloneDomByPos(2, false, 4));
                            cloneDoms.push(getCloneDomByPos(3, true, 4));
                            cloneDoms.push(getCloneDomByPos(3, true, 5));
                            cloneDoms.push(getCloneDomByPos(3, false, 4));
                            break;
                    }

                    cloneDoms.sort(function (a, b) {
                        var aKind = a.find('.item').attr('href');
                        var bKind = b.find('.item').attr('href');
                        if (aKind !== 'javascript:;'){
                            return 1;
                        }
                        else if (bKind !== 'javascript:;'){
                            return -1;
                        }
                        return 0;
                    });

                    for (var i = 0; i < rowsCount; i ++){
                        var cloneDom = cloneDoms.pop();
                        cloneDom.addClass('left-side for-side layer-' + (i + 1));
                        domDiamondsWrapper.append(cloneDom);

                        cloneDom = cloneDoms.pop();
                        cloneDom.addClass('right-side for-side layer-' + (i + 1));
                        domDiamondsWrapper.append(cloneDom);
                    }
                }

                this.init = function () {
                    fillMainDiamonds();
                    fillSides();
                    bindEvents();
                }
            }

            diamondsHandle = new DiamondsClass();

            var bindEvent = function () {
                domRoot.find('.join-series-wrapper .wrap').click(function () {
                    !self.joined ? self.joinSeries() : self.unJoinSeries();
                });
            }
            var viewUserPage = function (data) {
                $('<form action="userpage" method="post" hidden><input name="purchasedId" value="'+ data +'"><input name="scrollTo" value="subscription"></form>').appendTo('body').submit().remove();
            }
            var popupThanksJoin = function (data) {
                swal({
                    icon: "success",
                    title: 'SUCCESS!',
                    text: 'Thanks for joining! Would you like to go to your experiences?',
                    buttons: {
                        returnHome: {
                            text: "Go to my Experiences",
                            value: 'return_home',
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        customize: {
                            text: "Not yet",
                            value: 'customize',
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    },
                    closeOnClickOutside: false,
                }).then(function (value) {
                    if (value == 'return_home'){
                        viewUserPage(data);
                    }
                    else {
                    }
                });
                var domSwalRoot, domDisableCheckbox;
                var disablePopup = function () {
                    localStorage.setItem('thegreyshirt_show_popup_every_join', '0');
                }
                var enabelPopup = function () {
                    localStorage.removeItem('thegreyshirt_show_popup_every_join');
                }
                var bindEvents = function () {
                    domDisableCheckbox.change(function () {
                        $(this).prop('checked') ? disablePopup() : enabelPopup();
                    })
                }
                var init = function () {
                    setTimeout(function () {
                        domSwalRoot = $('.swal-overlay--show-modal .swal-modal');
                        domSwalRoot.addClass('thanks-join');
                        domSwalRoot.find('.swal-footer').append('<div class="disable-wrap"><input type="checkbox" id="disable_popup" name="disable_popup" value="1" /><label for="disable_popup">Do not Show</label></div>');
                        domDisableCheckbox = domSwalRoot.find('#disable_popup');
                        bindEvents();
                    }, 100);
                }();
            }
            this.joinSeries = function () {
                var btn = domRoot.find('.join-series-wrapper .wrap .join-series');
                btn.removeClass("filled");
                btn.addClass("circle");
                btn.html("");
                $(".wrap  svg", domRoot).css("display", "block");
                $(".circle_2", domRoot).attr("class", "circle_2 fill_circle");

                return new Promise(function (resolve, reject) {
                    var timer = setInterval(
                        function tick() {
                            if (self.joined){
                                btn.removeClass("circle");
                                btn.addClass("filled");
                                $(".wrap img", domRoot).css("display", "block");
                                $("svg", domRoot).css("display", "none");
                                clearInterval(timer);
                                setTimeout(function () {
                                    var s = localStorage.getItem('thegreyshirt_show_popup_every_join');
                                    if (s == null || s === '1'){
                                        popupThanksJoin(self.purchased_ID);
                                    }
                                }, 500);
                                resolve(self.purchased_ID);
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    $.ajax({
                        url: ACTION_URL,
                        data: {action: 'join_series', seriesId: seriesData.series_ID},
                        success: function (res) {
                            if (res.status){
                                self.joined = true;
                                self.purchased_ID = res.data;
                            }
                            is_loading_controlled_in_local = false;
                        },
                        type: 'post',
                        dataType: 'json'
                    });
                });
            }
            this.unJoinSeries = function () {
                var btn = domRoot.find('.join-series-wrapper .wrap .join-series');
                btn.removeClass("filled");
                btn.addClass("circle");
                $(".wrap  svg", domRoot).css("display", "block");
                $(".circle_2", domRoot).attr("class", "circle_2 fill_circle");
                return new Promise(function (resove, reject) {
                    var timer = setInterval(
                        function tick() {
                            if (!self.joined){
                                btn.removeClass("circle");
                                btn.html("Join");
                                $(".wrap img", domRoot).css("display", "none");
                                $("svg", domRoot).css("display", "none");
                                clearInterval(timer);
                                resove();
                            }
                        }, 2000);
                    is_loading_controlled_in_local = true;
                    $.ajax({
                        url: ACTION_URL,
                        data: {action: 'unjoin_series', seriesId: seriesData.series_ID},
                        success: function (res) {
                            if (res.status){
                                self.joined = false;
                                is_loading_controlled_in_local = false;
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                });
            }
            this.init = function () {
                self.purchased_ID = initPurchasedId;
                self.joined = initPurchasedId ? true : false;
                domRoot.find('.share-series').circleNav(domRoot.find('.circle-nav-wrapper'));
                diamondsHandle.init();
                bindEvent();
            }
        }

        var PostsSectionClass = function () {

            var totalPosts = initPosts;
            var currentIndex = 0;
            var blockSize = 15;
            var postsBlockHandles = [];

            var domRoot = $('.posts-section');
            var PostsBlockClass = function (posts) {
                var domGrid = domRoot.find('.tt-grid.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domRoot.find('.tt-grid-wrapper'));

                var grid = domGrid.get(0),
                    items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                    isAnimating = false;

                var itemHandles = [];

                var ItemClass = function (itemData) {
                    var domItemRoot = null;
                    var domTitle = null;

                    this.html = function () {
                        var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                        cloneDom.find('.image-wrapper img').attr('src', itemData.strPost_featuredimage);
                        cloneDom.find('.title-wrapper .object_title').html(itemData.strPost_title);
                        return cloneDom.html();
                    }
                    this.bindDom = function (domItem) {
                        domItemRoot = domItem;

                        domItemRoot.find('.view-post').click(function () {
                            window.open(BASE_URL + '/view.php?id=' + itemData.post_ID + '&prevpage=viewexperience');
                        });

                        domTitle = domItemRoot.find('.object_title');
                        var clone_title = domTitle.clone().css({height: 'auto', position: 'absolute', maxHeight: '1000px', width: '100%', top: '10000px'}).appendTo(domItemRoot.find('.title-wrapper'));
                        var origin_height = clone_title.height();

                        domTitle.css({maxHeight: '105px'});
                        if(origin_height <= 86){
                            domItemRoot.find('.title-wrapper .view-more-wrapper').remove();
                        }
                        clone_title.remove();
                        domItemRoot.find('.title-wrapper .view-more-wrapper .view-more').click(function () {
                            domItemRoot.parent().css('z-index', '1');
                            domItemRoot.find('.title-wrapper').addClass('expanded');
                            domTitle.css({maxHeight: origin_height * 1 + 20});
                        });
                        domItemRoot.find('.title-wrapper .view-more-wrapper .view-less').click(function () {
                            domItemRoot.parent().css('z-index', '0');
                            domItemRoot.find('.title-wrapper').removeClass('expanded');
                            domTitle.css({maxHeight: '105px'});
                        });
                    }
                }
                var self = this;
                var loadNewSet = function(set) {
                    if (isAnimating === true){
                        return ;
                    }
                    isAnimating = true;

                    var newImages = set;
                    items.forEach( function( el ) {
                        var itemChild = el.querySelector( 'a' );
                        // add class "tt-old" to the elements/images that are going to get removed
                        if( itemChild ) {
                            classie.add( itemChild, 'tt-old' );
                        }
                    } );

                    // apply effect
                    setTimeout( function() {
                        // append new elements
                        if(newImages){
                            [].forEach.call( newImages, function( el, i ) {
                                items[ i ].innerHTML += el.html();
                                el.bindDom($(items[ i ].querySelector( 'a:last-child' )));
                            } );
                        }

                        // add "effect" class to the grid
                        classie.add( grid, 'tt-effect-active' );


                        // wait that animations end
                        var onEndAnimFn = function() {
                            // remove old elements
                            items.forEach( function( el , i) {
                                // remove old elems
                                var old = el.querySelector( '.tt-old' );
                                if( old ) { el.removeChild( old ); }
                                // remove class "tt-empty" from the empty items
                                classie.remove( el, 'tt-empty' );
                                // now apply that same class to the items that got no children (special case)
                                if ( !$(el).children().length ) {
                                    classie.add( el, 'tt-empty' );
                                    $(el).remove();
                                }
                                else {
                                    // newImages[i].bindDom($(el));
                                }
                            } );
                            // remove the "effect" class
                            classie.remove( grid, 'tt-effect-active' );
                            isAnimating = false;
                        };

                        if( support ) {
                            onAnimationEnd( items, items.length, onEndAnimFn );
                        }
                        else {
                            onEndAnimFn.call();
                        }
                    }, 25 );
                }

                this.init = function () {
                    posts.forEach(function (post) {
                        var itemHandle = new ItemClass(post);
                        itemHandles.push(itemHandle);
                    });
                    loadNewSet(itemHandles);
                }
            }

            var bindEvent = function () {
                $('.site-wrapper.page').scroll(function (e) {
                    if ($('.site-wrapper.page').hasClass('header-search-opened')){
                        return true;
                    }
                    var elem = $(e.currentTarget);
                    if (elem[0].scrollHeight - elem.scrollTop() <= elem.outerHeight() + 10){
                        addPostsBlock();
                    }
                })
            }

            var addPostsBlock = function () {
                var blockPosts = totalPosts.slice(currentIndex, currentIndex + blockSize);
                currentIndex += blockPosts.length;
                if (!blockPosts.length){
                    return false;
                }
                var postsBlockHandle = new PostsBlockClass(blockPosts);
                postsBlockHandle.init();
                postsBlockHandles.push(postsBlockHandle);
                return postsBlockHandle;
            }
            this.init = function () {
                addPostsBlock();
                bindEvent();
            }
        }

        this.init = function () {
            postsSectionHandle = new PostsSectionClass();
            postsSectionHandle.init();

            seriesSectionHandle = new SeriesSectionClass();
            seriesSectionHandle.init();
            siteHeaderHandle.scrollFix({
                container: $('.site-wrapper.page'),
                domReplace: $('.scroll-fix-rplc').clone().removeAttr('hidden'),
            });
            embedHandle = new SeriesCopyEmbedClass($('.component.series-copy-embed'), initSeries);
            embedHandle.init();
        }
    }
    viewExpereincePageHandle = new ViewExperiencePageClass();
    viewExpereincePageHandle.init();
})();

$(document).ajaxStart(function () {
    if(!is_loading_controlled_in_local) {
        $('.loading').css('display', 'block');
    }
});
$(document).ajaxComplete(function () {
    if(!is_loading_controlled_in_local){
        $('.loading').css('display', 'none');
    }
});