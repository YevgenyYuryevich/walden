(function () {
    'use strict';

    var PageClass = function () {
        var domRoot, domHeader;
        var domUsedBody, domBody, domVideo;
        var audioCnt = 0;
        var availableTypes = [0, 2, 8];
        var self = this;

        var videoHandle = null;

        var makeBody = function () {
            var type = parseInt(postData.type);
            type = availableTypes.indexOf(type) != -1 ? type : 'other';
            var newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
            switch (parseInt(type)){
                case 0:
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    domUsedBody.find('.audioplayer-tobe').attr('data-thumb', postData.image);
                    domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                    domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                    var settings_ap = {
                        disable_volume: 'off',
                        disable_scrub: 'default',
                        design_skin: 'skin-wave',
                        skinwave_dynamicwaves: 'off'
                    };
                    dzsag_init('#audio-' + audioCnt, {
                        'transition':'fade',
                        'autoplay' : 'off',
                        'settings_ap': settings_ap
                    });
                    audioCnt++;
                    break;
                case 2:
                    if (videoHandle){

                    }
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    domVideo = domUsedBody.find('video');
                    if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                        var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}] , "customControlsOnMobile": true };
                        domVideo.attr('data-setup', JSON.stringify(setup));
                    }
                    else {
                        domVideo.append('<source src="'+ postData.body +'" />');
                    }
                    videoHandle = videojs(domVideo.get(0));
                    break;
                case 8:
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    domUsedBody.append(postData.body);
                    break;
                default:
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    domUsedBody.append(postData.body);
                    break;
            }
        }
        var bindEvents = function () {
        }

        this.bindData = function (newData) {

            postData = $.extend({}, postData, newData);

            $('head title').html(postData.title);

            for( var i = 0; i < 10; i ++ ){
                domRoot.find('.site-content').removeClass('post-type-' + i);
            }
            domRoot.find('.blog-title').html(postData.title);
            domBody.find('.blog-duration').html('waiting...');
            if (postData.duration) {
                domBody.find('.blog-duration').html(postData.duration);
            }
            else {
                helperHandle.getBlogDuration(postData.body, postData.type).then(function (res) {
                    postData.duration = res;
                    domBody.find('.blog-duration').html(res);
                });
            }
            makeBody();
        }
        this.init = function () {
            domRoot = $('.site-wrapper .site-content');
            domHeader = domRoot.find('header.preview-blog-header');
            domBody = domRoot.find('.blog-wrapper');
            self.bindData(postData);
            bindEvents();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();