(function () {
    'use strict';

    var pageHandle;

    var PageClass = function () {

        var domPage;

        var listingPostsHandle, viewHandle;

        var postFields = {
            id: 'post_ID',
            title: 'strPost_title',
            body: 'strPost_body',
            image: 'strPost_featuredimage',
            type: 'intPost_type',
            order: 'intPost_order',
            nodeType: 'strPost_nodeType',
            status: 'strPost_status',
            parent: 'intPost_parent',
            paths: 'paths',
            free: 'boolPost_free'
        };

        var subscriptionFiels = {
            id: 'clientsubscription_ID',
            title: 'strClientSubscription_title',
            body: 'strClientSubscription_body',
            image: 'strClientSubscription_image',
            type: 'intClientSubscriptions_type',
            order: 'intClientSubscriptions_order',
            nodeType: 'strClientSubscription_nodeType',
            parent: 'intClientSubscriptions_parent',
            paths: 'paths',
            completed: 'boolCompleted',
        };

        var viewFormat = function (data) {
            var fData = {};
            var fk;

            for (var k in data) {
                if (from === 'post') {
                    fk = helperHandle.keyOfVal(postFields, k);
                    if (fk) { fData[fk] = data[k]; }
                }
                else {
                    fk = helperHandle.keyOfVal(subscriptionFiels, k);
                    if (fk) { fData[fk] = data[k]; }
                }
            }
            fData.origin = data;
            return fData;
        }
        var postFormat = function (data) {
            var fData = {};
            for (var k in data) {
                if (postFields[k]) {
                    fData[postFields[k]] = data[k];
                }
            }
            return fData;
        }

        var ListingPostsClass = function () {

            var domListingPanel, domListing, domList;
            var itemHandles = [], self = this;

            var bindEvents = function () {
                domListing.find('.section-close').click(function () {
                    self.close();
                });
                domListingPanel.click(function (e) {
                    if (e.target === this) {
                        self.close();
                    }
                });
            }

            var ItemClass = function (itemData) {
                var domItem;
                var self = this;
                var bindEvents = function () {
                    domItem.find('.item-title').click(function () {
                        ajaxAPiHandle.apiPost('Posts.php', {action: 'get', where: itemData.post_ID}).then(function (res) {
                            viewHandle.setData(viewFormat(res.data));
                            listingPostsHandle.close();
                        });
                    });
                }
                var bindData = function () {
                    domItem.find('.item-img').attr('src', itemData.strPost_featuredimage);
                    domItem.find('.item-title').html(itemData.strPost_title);
                    domItem.find('.media-icon-wrapper').addClass('type-' + itemData.intPost_type);
                    if (itemData.strPost_duration) {
                        domItem.find('.item-duration').html(itemData.strPost_duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                            domItem.find('.item-duration').html(res);
                            itemData.strPost_duration = res;
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                        });
                    }
                    domItem.find('.item-view-states-wrapper').removeClass('state-complete').removeClass('state-current').removeClass('state-normal');
                    if (parseInt(itemData.boolCompleted)) {
                        domItem.find('.item-view-states-wrapper').addClass('state-complete');
                    }
                    else if (itemData.current) {
                        domItem.find('.item-view-states-wrapper').addClass('state-current');
                    }
                    else {
                        domItem.find('.item-view-states-wrapper').addClass('state-normal');
                    }
                }
                this.setData = function (sets) {
                    itemData = Object.assign(itemData, sets);
                    bindData();
                }
                this.data = function () {
                    return itemData;
                }
                this.init = function () {
                    domItem = domList.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domList);
                    bindData();
                    bindEvents();
                }
            }
            this.dom = function () {
                return domListing;
            }
            this.toggle = function () {
                domListing.toggleClass('collapsed');
                $('body').toggleClass('listing-opened');
            }
            this.open = function () {
                $('body').addClass('listing-opened');
                setTimeout(function () {
                    domListing.removeClass('collapsed');
                }, 10);
            }
            this.close = function () {
                domListing.addClass('collapsed');
                setTimeout(function () {
                    $('body').removeClass('listing-opened');
                }, 300);
            }
            this.update = function (id, sets) {
                itemHandles.forEach(function (value) {
                    if (value.data().post_ID == id) {
                        value.setData(sets);
                    }
                });
            }
            this.init = function () {
                domListingPanel = domPage.find('.listing-panel');
                domListing = domPage.find('#listing-posts');
                domList = domListing.find('.listing-posts-content .posts-list');

                posts.forEach(function (v) {
                    var hdl = new ItemClass(v);
                    hdl.init();
                    itemHandles.push(hdl);
                });
                bindEvents();
            }
        }

        var ViewPanelClass = function () {

            var domViewPanel;
            var domUsedBody, domBody, domFooter, domVideo;
            var videoHandle;

            var audioCnt = 0, metaData = {};
            var availableTypes = [0, 2, 8, 10];
            var postData, prevPostData;
            var isWelcome = false;

            var self = this, payBlogHandle = null;

            var FormFieldClass = function (domField) {
                var domValue;
                var field, answer = false;

                var bindEvents = function () {
                    switch (field.strFormField_type) {
                        case 'text':
                            domValue.blur(function () {
                                updateAnswer();
                            });
                            break;
                        case 'checkbox':
                            domValue.find('input').change(function () {
                                updateAnswer();
                            });
                            break;
                    }
                }
                var updateAnswer = function () {
                    if (CLIENT_ID == -1) {
                        return false;
                    }
                    var sets = {};
                    is_loading_controlled_in_local = true;
                    domField.addClass('status-loading');
                    switch (field.strFormField_type) {
                        case 'text':
                            sets.strFormFieldAnswer_answer = domValue.html();
                            break;
                        case 'checkbox':
                            sets.strFormFieldAnswer_answer = domValue.find('input').prop('checked') ? 1 : 0;
                            break;
                    }
                    if (answer) {
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'update', sets: sets, where: answer.formFieldAnswer_ID}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                            answer.strFormFieldAnswer_answer = sets.strFormFieldAnswer_answer;
                        });
                    }
                    else {
                        $.extend(sets, {intFormFieldAnswer_field_ID: field.formField_ID, intFormFieldAnswer_client_ID: CLIENT_ID});
                        ajaxAPiHandle.apiPost('FormFieldAnswers.php', {action: 'insert', sets: sets}, false).then(function (res) {
                            domField.removeClass('status-loading');
                            domField.addClass('status-loading-finished');
                            answer = $.extend(sets, {formFieldAnswer_ID: res.data});
                            domField.one(animEndEventName, function () {
                                console.log('ani end');
                                domField.removeClass('status-loading-finished');
                            });
                        });
                    }
                    is_loading_controlled_in_local = false;
                }
                var bindData = function () {
                    formFields.forEach(function (formField) {
                        if (formField.formField_ID == domField.attr('data-form-field')) {
                            field = formField;
                        }
                    });
                    formFieldAnswers.forEach(function (formFieldAnswer) {
                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                            answer = formFieldAnswer;
                        }
                    });
                    if (answer) {
                        switch (field.strFormField_type) {
                            case 'text':
                                domValue.html(answer.strFormFieldAnswer_answer);
                                break;
                            case 'checkbox':
                                domValue.find('input').prop('checked', parseInt(answer.strFormFieldAnswer_answer));
                                break;
                        }
                    }
                }
                this.init = function () {
                    domValue = domField.find('.form-field-value');
                    domField.find('[data-toggle="popover"]').popover();
                    bindData();
                    bindEvents();
                }
            }

            var PathClass = function (path) {
                var domPath, pathNumber;

                var bindEvents = function () {
                    domPath.click(function () {
                        select();
                    });
                }

                var select = function () {
                    var resPromise;
                    var ajaxData = {};

                    if (from === 'post') {
                        if (CLIENT_ID !== -1) {
                            ajaxData.action = 'set_seek';
                            ajaxData.where = clientSeriesView.clientSeriesView_ID;
                            ajaxData.sets = {
                                intCSView_seekParent: path.id,
                            };
                        }
                        else {
                            ajaxData.action = 'select_path';
                            ajaxData.id = path.id;
                        }
                        resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                    }
                    else {
                        ajaxData.action = 'set_subPath';
                        ajaxData.pathId = path.id;
                        ajaxData.purchasedId = purchased.purchased_ID;
                        resPromise = ajaxAPiHandle.pagePost('view', ajaxData);

                    }
                    resPromise.then(function (res) {
                        if (res.status === true && res.data){
                            self.bindData(viewFormat(res.data));
                        }
                        else {
                            swal("Sorry!", "There is no post in this option that you clicked. You will go to next post.").then(function () {
                                if (CLIENT_ID !== -1) {
                                    seekNext();
                                }
                                else {
                                    nextPost();
                                }
                            });
                        }
                    });
                }

                this.init = function () {

                    domPath = domUsedBody.find('.path-col.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domUsedBody.find('.paths-list'));

                    pathNumber = domUsedBody.find('.paths-list > .path-col').length;

                    domPath.find('.img-wrapper img').attr('src', BASE_URL + '/assets/images/global-icons/tree/path/'+ pathNumber +'.svg');


                    var displayTitle = path.title;
                    displayTitle = displayTitle.length > 20 ? displayTitle.substr(0, 50) + '...' : displayTitle.substr(0, 50);

                    var displayDes = path.body;
                    displayDes = displayDes.length > 20 ? displayDes.substr(0, 100) + '...' : displayDes.substr(0, 100);

                    domPath.find('.path-title').html(displayTitle);
                    domPath.find('.path-description').html(displayDes);
                    bindEvents();
                }
            }
            var nextPost = function () {

                var resPromise;
                var ajaxData;

                if (from === 'post') {
                    ajaxData = {
                        action: 'next_post',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                }
                else {
                    ajaxData = {
                        action: 'next_sub',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.pagePost('view', ajaxData);
                }

                return resPromise.then(function (res) {
                    if (res.status){
                        self.bindData(viewFormat(res.data));
                    }
                });
            }
            var prevPost = function () {
                var resPromise;
                var ajaxData;

                if (from === 'post') {
                    ajaxData = {
                        action: 'prev_post',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.apiPost('Posts.php', ajaxData);
                }
                else {
                    ajaxData = {
                        action: 'prev_sub',
                        id: postData.id
                    };
                    resPromise = ajaxAPiHandle.pagePost('view', ajaxData);
                }

                return resPromise.then(function (res) {
                    if (res.status){
                        self.bindData(viewFormat(res.data));
                    }
                });
            }

            var seekNext = function () {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_next', CSViewId: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                    self.bindData(viewFormat(res.data));
                });
            }

            var seekPrev = function () {
                ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_prev', CSViewId: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                    self.bindData(viewFormat(res.data));
                });
            }

            var showPrevPost = function () {
                isWelcome = false;
                self.bindData(prevPostData);
            }

            var makeBody = function () {
                var type = parseInt(postData.type);
                type = availableTypes.indexOf(type) !== -1 ? type : 'other';
                var newDomUsedBody;
                domBody.find('> .blog-type:not(.sample)').remove();
                if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                    newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                    domUsedBody = newDomUsedBody;
                    payBlogHandle = new PayBlogClass(domUsedBody, series);
                    payBlogHandle.onPaid(function (purchasedId) {
                        purchased = purchasedId;
                        makeBody();
                    });
                    payBlogHandle.init();
                }
                else if (postData.nodeType === 'menu') {
                    newDomUsedBody = domBody.find('.sample.type-menu').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    domUsedBody = newDomUsedBody;
                    // domUsedBody.find('.menu-title').html(postData.title);
                    domUsedBody.find('.menu-description').html(postData.body);

                    postData.paths.forEach(function (path) {
                        var pathHandle = new PathClass(viewFormat(path));
                        pathHandle.init();
                    });
                    domUsedBody.find('.paths-list').addClass('paths-count-' + postData.paths.length);
                }
                else {
                    newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                    switch (parseInt(type)){
                        case 0:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.find('.audioplayer-tobe').attr('data-thumb', helperHandle.makeAbsUrl(postData.image));
                            domUsedBody.find('.audioplayer-tobe').attr('data-source', postData.body);
                            domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                            var settings_ap = {
                                disable_volume: 'off',
                                disable_scrub: 'default',
                                design_skin: 'skin-wave',
                                skinwave_dynamicwaves: 'off'
                            };
                            dzsag_init('#audio-' + audioCnt, {
                                'transition':'fade',
                                'autoplay' : 'off',
                                'settings_ap': settings_ap
                            });
                            audioCnt++;
                            break;
                        case 2:
                            // domBody.find('> .blog-type:not(.sample)').remove();
                            typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domVideo = domUsedBody.find('video');
                            if (helperHandle.getApiTypeFromUrl(postData.body) === 'youtube') {
                                var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": postData.body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                domVideo.attr('data-setup', JSON.stringify(setup));
                            }
                            else {
                                domVideo.append('<source src="'+ postData.body +'" />');
                            }
                            videoHandle = videojs(domVideo.get(0), {
                                width: domUsedBody.innerWidth(),
                                height: domUsedBody.innerWidth() * 540 / 960,
                            });
                            break;
                        case 8:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            break;
                        case 10:
                            // typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });

                            domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                var field = false;
                                var answer = false;
                                formFields.forEach(function (formField) {
                                    if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                        field = formField;
                                    }
                                });
                                formFieldAnswers.forEach(function (formFieldAnswer) {
                                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                        answer = formFieldAnswer;
                                    }
                                });
                                if (field) {
                                    if (answer) {
                                        field.answer = answer.strFormFieldAnswer_answer;
                                    }
                                    else{
                                        field.answer = false;
                                    }
                                }
                                var hdl = new AnsweredFormFieldClass($(d), field);
                                hdl.init();
                            });

                            break;
                        default:
                            domUsedBody = newDomUsedBody;
                            domUsedBody.append(postData.body);
                            domUsedBody.find('[data-form-field]').each(function (i, d) {
                                var hdl = new FormFieldClass($(d));
                                hdl.init();
                            });

                            domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                var field = false;
                                var answer = false;
                                formFields.forEach(function (formField) {
                                    if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                        field = formField;
                                    }
                                });
                                formFieldAnswers.forEach(function (formFieldAnswer) {
                                    if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                        answer = formFieldAnswer;
                                    }
                                });
                                if (field) {
                                    if (answer) {
                                        field.answer = answer.strFormFieldAnswer_answer;
                                    }
                                    else{
                                        field.answer = false;
                                    }
                                }
                                var hdl = new AnsweredFormFieldClass($(d), field);
                                hdl.init();
                            });
                            break;
                    }
                }
            }

            var bindEvents = function () {
                domFooter.find('.step-next').click(function () {
                    if (isWelcome) {
                        showPrevPost();
                    }
                    else if (CLIENT_ID !== -1) {
                        seekNext();
                    }
                    else {
                        nextPost();
                    }
                });
                domFooter.find('.step-back').click(function () {
                    if (isWelcome) {
                        showPrevPost();
                    }
                    else if (CLIENT_ID !== -1) {
                        seekPrev();
                    }
                    else {
                        prevPost();
                    }
                });
                domViewPanel.find('.listing-toggle').click(function () {
                    listingPostsHandle.open();
                });
            }

            var insertCSViewAs = function () {
                var sets = {
                    intCSView_client_ID: CLIENT_ID,
                    intCSView_series_ID: initialSeries.series_ID,
                    intCSView_seekParent: postData.parent,
                    intCSView_seekOrder: postData.order
                };
                return ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'insert', sets: sets}, false).then(function (res) {
                    clientSeriesView = res.data;
                    $('.loading').hide();
                    return res.data;
                });
            }
            this.setData = function (data) {
                if (CLIENT_ID !== -1) {
                    var sets = {
                        intCSView_seekParent: data.parent,
                        intCSView_seekOrder: data.order,
                    };
                    ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'update', where: clientSeriesView.clientSeriesView_ID, sets: sets}).then(function (res) {
                        self.bindData(data);
                    })
                }
                else {
                    self.bindData(data);
                }
            }
            this.getPostData = function () {
                return postData ? postData : false;
            }
            this.showWelcome = function () {
                isWelcome = true;
                prevPostData = postData;
                welcomePost.boolPost_free = 1;
                self.bindData(viewFormat(welcomePost));
                var title = domViewPanel.find('.post-title').html();
                domViewPanel.find('.post-title').html(title.replace('name', clientName));

                var where = {intCSView_client_ID: CLIENT_ID, intCSView_series_ID: initialSeries.series_ID};
                ajaxAPiHandle.apiPost('ClientSeriesView.php', {action: 'get', where: where}, false).then(function (res) {
                    if (!res.data) {
                        insertCSViewAs();
                    }
                    else {
                        clientSeriesView = res.data;
                        $('.loading').hide();
                    }
                });
                domUsedBody.find('a[href="http://linktouserlocation.com"]').removeAttr('target').attr('href', 'javascript:;').click(function () {
                    ajaxAPiHandle.apiPost('Posts.php', {action: 'seek_post', CSView: clientSeriesView.clientSeriesView_ID}).then(function (res) {
                        isWelcome = false;
                        self.bindData(viewFormat(res.data));
                    });
                });
            }
            this.bindData = function (data) {
                if (postData) {
                    postData.image = helperHandle.makeAbsUrl(postData.image);
                    domViewPanel.removeClass('content-type-' + postData.type);
                    listingPostsHandle.update(postData.id, {current: false});
                }
                postData = $.extend({}, postData, data);
                listingPostsHandle.update(postData.id, {current: true});
                if (series.boolSeries_charge == 1 && !purchased && postData.free == 0) {
                    domViewPanel.find('.blog-content-wrapper').hide();
                    domViewPanel.find('.pay-blog-wrapper').show();
                }
                else {
                    domViewPanel.find('.blog-content-wrapper').show();
                    domViewPanel.find('.pay-blog-wrapper').hide();

                    domViewPanel.find('.post-img').attr('src', postData.image);
                    domViewPanel.find('.post-title').html(postData.title);
                    domViewPanel.addClass('content-type-' + postData.type);
                    makeBody();
                }
            }
            this.init = function () {
                domViewPanel = domPage.find('.view-panel');
                domFooter = domViewPanel.find('.view-panel-footer');
                domBody = domViewPanel.find('.post-body');
                if (seekPost) {
                    self.bindData(viewFormat(seekPost));
                }
                else  {
                    self.bindData(viewFormat(posts[0]));
                }
                payBlogHandle = new PayBlogClass(domViewPanel.find('.pay-blog-wrapper .pay-blog'), series);
                payBlogHandle.onPaid(function (purchasedId) {
                    purchased = purchasedId;
                    self.bindData(postData);
                });
                payBlogHandle.init();
                bindEvents();
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper');

            listingPostsHandle = new ListingPostsClass();
            listingPostsHandle.init();

            viewHandle = new ViewPanelClass();
            viewHandle.init();
            var currentHeight = 100;
            setInterval(function () {
                var nH = $('body').prop('scrollHeight');
                if (nH && currentHeight !== nH) {
                    currentHeight = nH;
                    window.parent.postMessage({name: 'currentHeight', height: currentHeight, embedId: embedId}, '*');
                }
            }, 200);
        }
    }
    pageHandle = new PageClass();
    pageHandle.init();
})();