(function () {
    'use strict';
    var PageClass = function () {
        var domForm, domCoverImgWrap;
        var imageState = 'initial';

        var updateCoverImage = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.cover-image-wrapper .img-wrapper img', domForm).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                imageState = 'updated';
            }
        };
        var checkPasswordValid = function (password) {
            var ajaxData = {
                action: 'get_secret',
                where: {
                    id: user.id,
                    hash: password,
                }
            };
            return ajaxAPiHandle.apiPost('User.php', ajaxData).then(function (res) {
                if (res.data) {
                    return true;
                }
                else {
                    return false;
                }
            });
        }
        var save = function () {
            var formData = new FormData(domForm.get(0));
            var sets = helperHandle.formSerialize(domForm);
            if (formData.get('new_password')) {
                sets.hash = sets.new_password;
                formData.append('hash', formData.get('new_password'));
            }

            formData.delete('current_password');
            delete sets.current_password;
            formData.delete('new_password');
            delete sets.new_password;

            var imageUpdatePromise = Promise.resolve();
            if (imageState == 'initial') {
                formData.delete('image');
                delete sets.image;
            }
            else if (imageState == 'removed') {
                formData.delete('image');
                sets.image = '';
                formData.append('image', '');
            }
            else {
                var ajaxData = new FormData();
                ajaxData.append('file', formData.get('image'));
                ajaxData.append('sets', JSON.stringify({prefix: 'userProfile'}));
                imageUpdatePromise = ajaxAPiHandle.multiPartApiPost('Uploads.php', ajaxData, true, 100).then(function (res) {
                    formData.set('image', res.data.url);
                    sets.image = res.data.url;
                    return true;
                });
            }
            imageUpdatePromise.then(function () {
                ajaxAPiHandle.apiPost('User.php', {action: 'update', sets: sets}).then(function () {
                    $.extend(user, sets);
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Changes are saved successfully',
                        closeOnClickOutside: false,
                    });
                });
            });
        };
        var bindEvents = function () {
            domForm.find('.cover-image-wrapper [name="image"]').change(function () {
                updateCoverImage(this);
            });
            domCoverImgWrap.dndhover().on('dndHoverStart', function (event) {
                domCoverImgWrap.addClass('drag-over');
                event.stopPropagation();
                event.preventDefault();
                return true;
            }).on('dndHoverEnd', function (event) {
                domCoverImgWrap.removeClass('drag-over');
                event.stopPropagation();
                event.preventDefault();
                return true;
            });
            domCoverImgWrap.on('drop', function(e) {
                domCoverImgWrap.removeClass('drag-over');
            });
            domForm.find('.cover-image-wrapper .delete-action').click(function () {
                domForm.find('.cover-image-wrapper [name="image"]').val('');
                domForm.find('.user-image').attr('src', 'assets/images/global-icons/user-avatar.svg');
                imageState = 'removed';
            });
            domForm.find('.cancel-btn').click(function () {
                bindData();
            });
            domForm.submit(function () {

                checkPasswordValid(domForm.find('[name="current_password"]').val()).then(function (res) {
                    if (res) {
                        save();
                    }
                    else {
                        swal("Your should type current password to update your account");
                    }
                });
                return false;
            });
            domForm.find('.submit-btn').click(function () {
                checkPasswordValid(domForm.find('[name="current_password"]').val()).then(function (res) {
                    if (res) {
                        save();
                    }
                    else {
                        swal("Your should type current password to update your account");
                    }
                });
            });
            domForm.find('input').keyup(function (e) {
                var code = e.which;
                if (code === 13) {
                    checkPasswordValid(domForm.find('[name="current_password"]').val()).then(function (res) {
                        if (res) {
                            save();
                        }
                        else {
                            swal("Your should type current password to update your account");
                        }
                    });
                }
            });
        };
        var bindData = function () {
            domForm.find('[name="f_name"]').val(user.f_name);
            domForm.find('[name="l_name"]').val(user.l_name);
            domForm.find('[name="email"]').val(user.email);
            if (user.image) {
                domForm.find('.user-image').attr('src', user.image);
            }
            else {
                domForm.find('.user-image').attr('src', 'assets/images/global-icons/man-with-short-hair-profile-avatar.svg');
            }
            domForm.find('[name="about_me"]').val(user.about_me);
            domForm.find('[name="social_facebook"]').val(user.social_facebook);
            domForm.find('[name="social_twitter"]').val(user.social_twitter);
            domForm.find('[name="social_pinterest"]').val(user.social_pinterest);
            domForm.find('[name="social_linkedin"]').val(user.social_linkedin);
            domForm.find('[name="current_password"]').val('');
            domForm.find('[name="new_password"]').val('');
            imageState = 'initial';
        };
        this.init = function () {
            domForm = $('form.profile-form');
            domCoverImgWrap = domForm.find('.cover-image-wrapper');
            $.fn.dndhover = function(options) {
                return this.each(function() {

                    var self = $(this);
                    var collection = $();

                    self.on('dragenter', function(event) {
                        if (collection.length === 0) {

                            self.trigger('dndHoverStart');
                        }
                        collection = collection.add(event.target);
                    });

                    self.on('dragleave', function(event) {
                        /*
                         * Firefox 3.6 fires the dragleave event on the previous element
                         * before firing dragenter on the next one so we introduce a delay
                         */
                        setTimeout(function() {
                            collection = collection.not(event.target);
                            if (collection.length === 0) {
                                self.trigger('dndHoverEnd');
                            }
                        }, 100);
                    });
                });
            };
            bindData();
            bindEvents();
        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();