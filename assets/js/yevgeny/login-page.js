(function () {
    'use strict';
    var AuthClass = function () {
        var domRoot, domLoginForm, domSignupForm, domResetForm;

        var login = function (username, password) {
            ajaxAPiHandle.pagePost('login.php', {function: 'signin', username: username, password: password}).then(function (res) {
                if (res.status) {
                    CLIENT_ID = res.result.data.id;
                    window.location.href = prevPage;
                }
                else {
                    domLoginForm.find('.alert.alert-danger').show();
                }
            });
        }
        var register = function (sets) {
            var ajaxData = {function: 'register'};
            $.extend(ajaxData, sets);
            ajaxAPiHandle.pagePost('login.php', ajaxData).then(function (res) {
                if (res.result.registered) {
                    CLIENT_ID = res.result.data.id;
                    window.location.href = 'simplify?first_visit=1';
                }
                else {
                    domSignupForm.find('.alert.alert-danger').show();
                }
            });
        }
        var resetPassword = function (email) {
            return ajaxAPiHandle.pagePost('login.php', {'function': 'reset_send', username: email}).then(function (res) {
                if (res.status) {
                    swal("SUCCESS!", 'We will send message to your email soon', "success", {
                        button: "OK"
                    });
                }
                else {
                    swal("Sorry!", 'username or email does not exist', "error", {
                        button: "OK"
                    });
                }
                return res.status;
            });
        }
        var sendResetPassword = function (sets) {
            sets['function'] = 'reset_pw';
            return ajaxAPiHandle.pagePost(CURRENT_PAGE, sets).then(function (res) {
                return res.status;
            });
        }
        var bindEvents = function () {
            domLoginForm.submit(function () {
                var username = domLoginForm.find('input#login-username').val();
                var password = domLoginForm.find('input#login-password').val();
                login(username, password);
                return false;
            });
            domLoginForm.find('.reset-password').click(function () {
                var username = domLoginForm.find('#login-reset-password').val();
                resetPassword(username);
            });
            domSignupForm.submit(function () {
                var sets = {};
                sets.email = domSignupForm.find('[name="email"]').val();
                sets.f_name = domSignupForm.find('[name="f_name"]').val();
                sets.password = domSignupForm.find('[name="password"]').val();
                register(sets);
                return false;
            });
            domResetForm.submit(function () {
                var sets = helperHandle.formSerialize(domResetForm);
                delete sets.re_password;
                sendResetPassword(sets).then(function (res) {
                    if (res) {
                        window.location.href = prevPage;
                    }
                    else {
                        domResetForm.find('.alert.alert-danger').show();
                    }
                });
            });
        }
        this.init = function () {
            domRoot = $('.auth-wrapper');
            domLoginForm = domRoot.find('form.login-form');
            domSignupForm = domRoot.find('form.signup-form');
            domResetForm = domRoot.find('form.reset-pwd-form');
            bindEvents();
        }
    }

    var authHandle = new AuthClass();
    authHandle.init();
})();