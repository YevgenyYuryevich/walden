(function () {

    'use strict';

    var is_loading_controlled_in_local = false;
    var createIdeaboxHandle;

    var accept_doc_types = ['application/pdf','text/plain'];
    var accept_image_type = ['image/png', 'image/jpeg'];

    var CreateIdeaboxClass = function () {

        var domRoot, domDescription, domRefLink;

        var getDataInfo = function (dataTransfer) {

            var droppedUrl = dataTransfer.getData('URL');

            if(droppedUrl != ''){
                return {url: droppedUrl, type: 'url'}
            }

            if (dataTransfer.files.length < 1){
                return {type: 'other'};
            }

            var type = dataTransfer.files[0].type;

            if (accept_image_type.indexOf(type) > -1){
                return {type: 'image', file: dataTransfer.files[0]};
            }else if (accept_doc_types.indexOf(type) > -1){
                return {type: 'document', file: dataTransfer.files[0]};
            } else {
                return {type: 'other'};
            }
        }
        var uploadRef = function (file) {
            var ajaxData = new FormData();
            ajaxData.append('action', 'upload_ref');
            ajaxData.append('ref_file', file);
            return $.ajax({
                url: ACTION_URL,
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res) {
                    if (res.status){
                        domRefLink.val(res.data);
                    }
                    else {
                        alert('Sorry. Something wrong!');
                    }
                },
                error: function() {}
            });
        }
        var createIdeabox = function () {
            var sets = {
                strIdeaBox_title: domRoot.find('[name="title"]').val(),
                strIdeaBox_idea: domRoot.find('[name="description"]').val(),
                intIdeaBox_private: domRoot.find('[name="isPrivate"]:checked').val(),
                intIdeaBox_subcategory: parentId
            }
            var refLink = domRoot.find('[name="refLink"]').val();
            if (refLink){
                var refText = domRoot.find('[name="refText"]').val();
                refText = refText ? refText : 'View Doc';
                sets.strIdeaBox_reference_link = refLink;
                sets.strIdeaBox_reference_text = refText;
            }
            var ajaxData = new FormData();
            ajaxData.append('action', 'insert');
            ajaxData.append('sets', JSON.stringify(sets));
            if (domRoot.find('[name="cover_img"]').prop('files').length){
                ajaxData.append('image', domRoot.find('[name="cover_img"]').prop('files')[0]);
            }
            return $.ajax({
                url: API_ROOT_URL + '/Ideabox.php',
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Your new ideabox are saved.',
                        buttons: {
                            returnHome: {
                                text: "RETURN HOME",
                                value: 'return_home',
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            customize: {
                                text: "CUSTOMIZE",
                                value: 'customize',
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        },
                        closeOnClickOutside: false,
                    }).then(function (value) {
                        if (value == 'return_home'){
                            viewUserPage();
                        }
                        else {
                            viewIdeabox(parentId);
                        }
                    });
                }
            });
        }
        var updateIdeabox = function () {
            var sets = {
                strIdeaBox_title: domRoot.find('[name="title"]').val(),
                strIdeaBox_idea: domRoot.find('[name="description"]').val(),
                intIdeaBox_private: domRoot.find('[name="isPrivate"]:checked').val(),
                intIdeaBox_subcategory: parentId
            }
            var refLink = domRoot.find('[name="refLink"]').val();
            if (refLink){
                var refText = domRoot.find('[name="refText"]').val();
                refText = refText ? refText : 'View Doc';
                sets.strIdeaBox_reference_link = refLink;
                sets.strIdeaBox_reference_text = refText;
            }
            var ajaxData = new FormData();
            ajaxData.append('action', 'update');
            ajaxData.append('sets', JSON.stringify(sets));
            ajaxData.append('where', JSON.stringify({ideabox_ID: inital_ideabox.ideabox_ID}));

            if (domRoot.find('[name="cover_img"]').prop('files').length){
                ajaxData.append('image', domRoot.find('[name="cover_img"]').prop('files')[0]);
            }
            return $.ajax({
                url: API_ROOT_URL + '/Ideabox.php',
                type: 'post',
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    swal({
                        icon: "success",
                        title: 'SUCCESS!',
                        text: 'Current ideabox are updated.',
                        buttons: {
                            returnHome: {
                                text: "RETURN HOME",
                                value: 'return_home',
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            customize: {
                                text: "CUSTOMIZE",
                                value: 'customize',
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        },
                        closeOnClickOutside: false,
                    }).then(function (value) {
                        if (value == 'return_home'){
                            viewUserPage();
                        }
                        else {
                            viewIdeabox(parentId);
                        }
                    });
                }
            });
        }
        var updateCoverImage = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.cover-image-wrapper .img-wrapper img', domRoot).addClass('touched');
                    $('.cover-image-wrapper .img-wrapper img', domRoot).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        var bindEvents = function () {
            domRoot.submit(function () {

                if (inital_ideabox){
                    updateIdeabox();
                }
                else {
                    createIdeabox();
                }
                return false;
            });
            domRefLink.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('dragenter', function (e) {
                domRefLink.addClass('is-dragover');
            }).on('dragleave', function (e) {
                domRefLink.removeClass('is-dragover');
            }).on('drop', function(e) {
                domRefLink.removeClass('is-dragover');
                var dataInfo = getDataInfo(e.originalEvent.dataTransfer);
                switch (dataInfo.type){
                    case 'url':
                        domRefLink.val(dataInfo.url);
                        break;
                    case 'other':
                        break;
                    default:
                        uploadRef(dataInfo.file);
                        break;
                }
            });
            domRoot.find('.cover-image-wrapper [name="cover_img"]').change(function () {
                updateCoverImage(this);
            });
        }
        var viewUserPage = function () {
            $('<form action="userpage" method="post" hidden></form>').appendTo('body').submit().remove();
        }
        var viewIdeabox = function (id) {
            $('<form action="ideaboxcards" method="post" hidden><input name="parentId" value="'+ id +'"></form>').appendTo('body').submit().remove();
        }
        this.init = function () {
            domRoot = $('#ideabox-form');
            domDescription = domRoot.find('[name="description"]');
            domRefLink = domRoot.find('[name="refLink"]');
            domDescription.summernote({
                height: 250,
                tabsize: 2
            });
            bindEvents();
        }
    }

    createIdeaboxHandle = new CreateIdeaboxClass();
    createIdeaboxHandle.init();

    $.ajaxSetup({
        beforeSend: function () {
            if(!is_loading_controlled_in_local) {
                $('.loading').css('display', 'block');
            }
        },
        complete: function () {
            if(!is_loading_controlled_in_local){
                $('.loading').css('display', 'none');
            }
        }
    });
})();