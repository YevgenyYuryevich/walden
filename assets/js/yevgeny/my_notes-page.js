(function () {
    'use strict';

    var PageClass = function () {
        var domPage, domCurrentSeries;

        var filterCategory = -1, filterSeries = -1, sortBy = -1;
        var categoryTitle;
        var filterHandle, seriesListHandle, postsListHandle, noteEditorHandle;

        var FilterClass = function () {

            var domRoot, domSortByDropDown, domCategoryDropDown;
            var bindEvents = function () {
                domRoot.find('.filter-category').click(function () {
                    domRoot.find('.filter-category').removeClass('active');
                    $(this).addClass('active');
                    filterSeries = -1;

                    filterCategory = $(this).data('category');
                    seriesListHandle.refresh();
                    postsListHandle.refresh();

                    seriesListHandle.dom().find('.item').removeClass('active');
                    seriesListHandle.dom().find('.item:nth-child(2)').addClass('active');
                });
                domSortByDropDown.find('ul li a').click(function () {
                    domSortByDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('value');
                    var txt = $(this).text();
                    domSortByDropDown.data('value', v);
                    domSortByDropDown.find('button span').text(txt);
                    sortBy = v;
                    //    handle actions here
                    postsListHandle.refresh();
                });
                domCategoryDropDown.find('ul li a').click(function () {
                    domCategoryDropDown.find('ul li').removeAttr('hidden');
                    $(this).parent().attr('hidden', true);
                    var v = $(this).data('category');
                    var txt = $(this).text();
                    domCategoryDropDown.data('value', v);
                    domCategoryDropDown.find('button span').text(txt);
                    if (v == -1) {
                        domCurrentSeries.text('All Series');
                    }
                    else {
                        domCurrentSeries.text('All Series of ' + txt);
                        categoryTitle = txt;
                    }
                });
            }
            this.init = function () {
                domRoot = domPage.find('.filter-wrapper');
                domSortByDropDown = domRoot.find('.sort-by .dropdown');
                domCategoryDropDown = domRoot.find('.categories-container-mobile .dropdown');
                bindEvents();
            }
        }
        var SeriesListClass = function () {
            var domRoot;
            var itemHandles = [], filteredHandles = [];

            var ItemClass = function (itemData) {
                var domItem;
                var bindEvents = function () {
                    domItem.click(function () {
                        filterSeries = itemData.series_ID;
                        domRoot.find('.item').removeClass('active');
                        domItem.addClass('active');
                        if (filterCategory == -1) {
                            domCurrentSeries.text(itemData.strSeries_title);
                        }
                        else if (filterSeries == -1) {
                            domCurrentSeries.text('All series of ' + categoryTitle);
                        }
                        else {
                            domCurrentSeries.text(itemData.strSeries_title);
                        }
                        postsListHandle.refresh();
                    });
                }
                var bindData = function () {
                    domItem.find('span').html(itemData.strSeries_title);
                    domItem.addClass('level-' + itemData.boolSeries_level);
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domRoot);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.init = function () {
                    domItem = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden');
                    if (itemData.series_ID == -1) {
                        domItem.addClass('all-item');
                    }
                    bindData();
                    bindEvents();
                }
            }
            this.dom = function () {
                return domRoot;
            }
            this.refresh = function () {
                domRoot.css('height', domRoot.css('height'));
                domRoot.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (value) {
                        value.detach();
                    });
                    filteredHandles = itemHandles.filter(function (value) {
                        if (filterCategory == -1) {
                            return true;
                        }
                        if (value.data().intSeries_category == -1 || value.data().intSeries_category == filterCategory) {
                            return true;
                        }
                        return false;
                    });
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domRoot.css('height', 'auto');
                    domRoot.css('opacity', 1);
                }, 300);
            }
            this.init = function () {
                series.sort(function (a, b) {
                    if (b.series_ID == -1) {
                        return 1;
                    }
                    if (a.boolSeries_level > b.boolSeries_level) {
                        return 1;
                    }
                    else if (a.boolSeries_level === b.boolSeries_level) {
                        return 0;
                    }
                    return -1;
                });
                domRoot = domPage.find('.main-content .series-col .series-list');
                series.forEach(function (value) {
                    var hdl = new ItemClass(value);
                    hdl.init();
                    hdl.append();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                });
                domRoot.find('.item:nth-child(2)').addClass('active');
            }
        }

        var postsListClass = function () {

            var domRoot;

            var itemHandles = [], filteredHandles = [];

            var ItemClass = function (itemData) {
                var domItem, domItemInner, domNotesList;
                var noteItemHandles = [];
                var NoteItemClass = function (note) {
                    var domNoteItem;

                    var self = this;

                    var bindData = function () {
                        domNoteItem.find('.note-content').html(note.postNote_content);
                    }

                    var bindEvents = function () {
                        domNoteItem.find('.delete-action').click(function () {
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this note?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    deleteNote();
                                }
                            });
                        });
                        domNoteItem.find('.edit-action').click(function () {
                            noteEditorHandle.setData(note);
                            noteEditorHandle.dom().modal('show');
                            noteEditorHandle.afterSave(function (sets) {
                                Object.assign(note, sets);
                                bindData();
                            });
                        });
                    }
                    var deleteNote = function () {
                        ajaxAPiHandle.apiPost('PostNotes.php', {action: 'delete', where: note.postNote_ID}).then(function (res) {
                            noteItemHandles.forEach(function (value, i) {
                                if (value.data().postNote_ID === itemData.postNote_ID) {
                                    noteItemHandles.splice(i, 1);
                                }
                            });
                            domNoteItem.remove();
                        });
                    }
                    this.data = function () {
                        return note;
                    }
                    this.set = function (key, v) {
                        note[key] = v;
                    }
                    this.init = function () {
                        domNoteItem = domNotesList.find('.note-item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domNotesList);
                        bindData();
                        bindEvents();
                        domNoteItem.data('controlHandle', self);
                    }
                }

                var bindData = function () {
                    domItemInner.addClass('type-' + itemData.intPost_type);
                    switch (parseInt(itemData.intPost_type)) {
                        case 0:
                            domItemInner.find('.to-value').html('audio');
                            break;
                        case 2:
                            domItemInner.find('.to-value').html('video');
                            break;
                        default:
                            domItemInner.find('.to-value').html('article');
                            break;
                    }
                    domItemInner.find('.item-img').attr('src', itemData.strPost_featuredimage);
                    domItemInner.find('.item-title').html(itemData.strPost_title).attr('href', BASE_URL + '/view_blog?id=' + itemData.post_ID);

                    var index = series.findIndex(function(s) {
                        return s.series_ID == itemData.intPost_series_ID;
                    });

                    domItemInner.find('.item-series').html(series[index].strSeries_title).attr('href', 'preview_series?id=' + series[index].series_ID);

                    if (itemData.strPost_duration) {
                        domItemInner.find('.blog-duration').html(itemData.strPost_duration);
                    }
                    else {
                        helperHandle.getBlogDuration(itemData.strPost_body, itemData.intPost_type).then(function (res) {
                            domItemInner.find('.blog-duration').html(res);
                            itemData.strPost_duration = res;
                            ajaxAPiHandle.apiPost('Posts.php', {action: 'update', where: itemData.post_ID, sets: {strPost_duration: res}}, false);
                        });
                    }
                    itemData.notes.forEach(function (note) {
                        var hdl = new NoteItemClass(note);
                        hdl.init();
                        noteItemHandles.push(hdl);
                    });
                }
                var bindEvents = function () {
                    domNotesList.on('sortupdate', function () {
                        reOrderNotes();
                    });
                }
                var reOrderNotes = function () {
                    var updates = [];
                    domNotesList.find('> .note-item:not(.sample)').each(function (i) {
                        var hdl = $(this).data('controlHandle');
                        if (i + 1 != hdl.data().intPostNote_order) {
                            hdl.set('intPostNote_order', i + 1);
                            updates.push({
                                where: hdl.data().postNote_ID,
                                sets: {
                                    intPostNote_order: i + 1
                                }
                            });
                        }
                    });
                    ajaxAPiHandle.apiPost('PostNotes.php', {action: 'update_many', updates: updates});
                }
                this.data = function () {
                    return itemData;
                }
                this.append = function () {
                    domItem.appendTo(domRoot);
                }
                this.detach = function () {
                    domItem.detach();
                }
                this.init = function () {
                    domItem = domRoot.find('.post-item.sample').clone().removeClass('sample').removeAttr('hidden');
                    domItemInner = domItem.find('.item-inner');
                    domNotesList = domItem.find('> .notes-list');
                    itemData.notes.sort(function (a, b) {
                        var aOrder = parseInt(a.intPostNote_order);
                        var bOrder = parseInt(b.intPostNote_order);
                        return aOrder - bOrder;
                    });
                    bindData();
                    domNotesList.sortable({
                        placeholder: 'ui-state-highlight',
                        items: '.note-item:not(.sample)',
                        coneHelperSize: true,
                        forcePlaceholderSize: true,
                        tolerance: "pointer",
                        helper: "clone",
                        revert: 300, // animation in milliseconds
                        handle: '.drag-icon-wrapper',
                        opacity: 0.9,
                        update: function (a, b) {}
                    });
                    bindEvents();
                }
            }
            this.refresh = function () {
                domRoot.css('height', domRoot.css('height'));
                domRoot.css('opacity', 0);
                setTimeout(function () {
                    filteredHandles.forEach(function (value) {
                        value.detach();
                    });
                    filteredHandles = itemHandles.filter(function (value) {
                        if (filterCategory === -1) {
                            if (filterSeries === -1) {
                                return true;
                            }
                            else if (filterSeries === value.data().intPost_series_ID) {
                                return true;
                            }
                        }
                        else {
                            if (filterSeries === -1) {
                                return value.data().category == filterCategory;
                            }
                            else {
                                return value.data().intPost_series_ID == filterSeries;
                            }
                        }
                        return false;
                    });
                    switch (sortBy) {
                        case 'audio':
                            filteredHandles = filteredHandles.filter(function (value) {
                                return value.data().intPost_type == 0;
                            });
                            break;
                        case 'video':
                            filteredHandles = filteredHandles.filter(function (value) {
                                return value.data().intPost_type == 2;
                            });
                            break;
                        case 'A-z':
                            filteredHandles.sort(function (a, b) {
                                if (a.data().strPost_title < b.data().strPost_title) {
                                    return 1;
                                }
                                else if (a.data().strPost_title == b.data().strPost_title) {
                                    return 0;
                                }
                                return -1;
                            });
                            break;
                        case 'Z-a':
                            filteredHandles.sort(function (a, b) {
                                if (a.data().strPost_title > b.data().strPost_title) {
                                    return 1;
                                }
                                else if (a.data().strPost_title == b.data().strPost_title) {
                                    return 0;
                                }
                                return -1;
                            });
                            break;
                        default:
                            break;
                    }
                    filteredHandles.forEach(function (value) {
                        value.append();
                    });
                    domRoot.css('height', 'auto');
                    domRoot.css('opacity', 1);
                }, 300);
            }
            this.init = function () {
                domRoot = domPage.find('.main-content .posts-col .posts-container');
                posts.forEach(function (value) {
                    var hdl = new ItemClass(value);
                    hdl.init();
                    hdl.append();
                    itemHandles.push(hdl);
                    filteredHandles.push(hdl);
                })
            }
        }

        var NoteEditorClass = function () {

            var domRoot, domForm;

            var note;

            var callbackAfterSave = function () {};

            var bindEvents = function () {
                domForm.submit(function () {
                    save();
                    return false;
                });
                domRoot.on('shown.bs.modal', function () {
                    domForm.find('[name="postNote_content"]').focus();
                });
            }

            var save = function () {
                var txt = domForm.find('[name="postNote_content"]').val();
                var sets = {
                    postNote_content: txt,
                };
                return ajaxAPiHandle.apiPost('PostNotes.php', {action: 'update', where: note.postNote_ID, sets: sets}).then(function (res) {
                    callbackAfterSave(sets);
                    domRoot.modal('hide');
                });
            }

            this.setData = function (n) {
                note = n;
                domForm.find('[name="postNote_content"]').val(note.postNote_content);
            }

            this.dom = function () {
                return domRoot;
            }
            this.afterSave = function (fn) {
                callbackAfterSave = fn;
            }
            this.init = function () {
                domRoot = domPage.find('.note-editor.modal');
                domForm = domRoot.find('form');
                bindEvents();
            }
        }

        this.init = function () {
            domPage = $('.site-wrapper .site-content');
            domCurrentSeries = domPage.find('.current-series');
            series.push({
                series_ID: -1,
                intSeries_category: -1,
                boolSeries_level: 0,
                strSeries_title: 'All Series',
                posts: [],
            });
            filterHandle = new FilterClass();
            filterHandle.init();

            seriesListHandle = new SeriesListClass();
            seriesListHandle.init();

            postsListHandle = new postsListClass();
            postsListHandle.init();

            noteEditorHandle = new NoteEditorClass();
            noteEditorHandle.init();

        }
    }
    var pageHandle = new PageClass();
    pageHandle.init();
})();