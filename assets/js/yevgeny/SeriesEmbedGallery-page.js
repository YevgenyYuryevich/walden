(function () {
    'use strict';

    var PageClass = function(){

        var payHandle;

        var domPageRoot;

        var postsSectionHandle, authHandle, embedGlobalHandle;

        var popupErroJoin = function () {
            swal("Sorry, Something went wrong!", {
                content: 'please try again later'
            });
        }
        var openStripe = function () {
            if (payHandle) {
                payHandle.open({
                    name: series.strSeries_title,
                    description: series.strSeries_description,
                    currency: 'usd',
                    amount: parseFloat((series.intSeries_price * 100).toFixed(2))
                });
            }
            else {
                popupErroJoin();
            }
        }
        var selectPost = function (token) {
            ajaxAPiHandle.apiPost('Series.php', {action: 'join', id: series.series_ID, token: token}, false).then(function (res) {
                if (res.status == true){
                    series.purchased = res.data;
                    if (token) {
                        postsSectionHandle.refreshBody();
                    }
                }
            });
        }
        var PostsSectionClass = function () {

            var domRoot, domContainer;
            var itemHandles = [];

            var availableTypes = [0, 2, 8], audioCnt = 1;

            var ItemClass = function (itemData) {
                var payBlogHandle;
                var domItemRoot, domUsedBody, domBody, domVideo;

                var metaData = {};


                var bindData = function () {
                    domItemRoot.find('.day-wrapper .item-day').html(itemData.day);
                    domItemRoot.find('.post-title').html(itemData.strPost_title);
                    makeBody();
                    metaData.isDataLoaded = true;
                }

                var makeBody = function () {
                    var type = parseInt(itemData.intPost_type);
                    type = availableTypes.indexOf(type) != -1 ? type : 'other';
                    if (series.boolSeries_charge == 1 && !purchased && itemData.boolPost_free == 0) {
                        newDomUsedBody = domBody.find('.sample.type-pay').clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                        domUsedBody = newDomUsedBody;
                        payBlogHandle = new PayBlogClass(domUsedBody, series);
                        payBlogHandle.onPaid(function (purchasedId) {
                            purchased = purchasedId;
                            postsSectionHandle.refreshBody();
                        });
                        payBlogHandle.init();
                    }
                    else {
                        var newDomUsedBody = domBody.find('.sample.type-' + type).clone().removeClass('sample').removeAttr('hidden').appendTo(domBody);
                        switch (parseInt(type)){
                            case 0:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domUsedBody.find('.audioplayer-tobe').attr('data-thumb', itemData.strPost_featuredimage);
                                domUsedBody.find('.audioplayer-tobe').attr('data-source', itemData.strPost_body);
                                domUsedBody.find('.audiogallery').attr('id', 'audio-' + audioCnt);
                                var settings_ap = {
                                    disable_volume: 'off',
                                    disable_scrub: 'default',
                                    design_skin: 'skin-wave',
                                    skinwave_dynamicwaves: 'off'
                                };
                                dzsag_init('#audio-' + audioCnt, {
                                    'transition':'fade',
                                    'autoplay' : 'off',
                                    'settings_ap': settings_ap
                                });
                                audioCnt++;
                                break;
                            case 2:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domVideo = domUsedBody.find('video');
                                setTimeout(function () {
                                    if (helperHandle.getApiTypeFromUrl(itemData.strPost_body) === 'youtube') {
                                        var setup = { "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": itemData.strPost_body}], "youtube": { "customVars": { "wmode": "transparent" } } };
                                        domVideo.attr('data-setup', JSON.stringify(setup));
                                    }
                                    else {
                                        domVideo.append('<source src="'+ itemData.strPost_body +'" />');
                                    }
                                    domVideo.attr('width', domUsedBody.innerWidth());
                                    domVideo.attr('height', domUsedBody.innerWidth() * 540 / 960);
                                    videojs(domVideo.get(0));
                                }, 100);
                                break;
                            case 8:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domUsedBody.append(itemData.strPost_body);
                                break;
                            default:
                                typeof domUsedBody !== 'undefined' ? domUsedBody.remove() : false;
                                domUsedBody = newDomUsedBody;
                                domUsedBody.append(itemData.strPost_body);
                                domUsedBody.find('[data-answered-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-answered-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer.strFormFieldAnswer_answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new AnsweredFormFieldClass($(d), field);
                                    hdl.init();
                                });
                                domUsedBody.find('[data-form-field]').each(function (i, d) {
                                    var field = false;
                                    var answer = false;
                                    formFields.forEach(function (formField) {
                                        if (formField.formField_ID == $(d).attr('data-form-field')) {
                                            field = formField;
                                        }
                                    });
                                    formFieldAnswers.forEach(function (formFieldAnswer) {
                                        if (formFieldAnswer.intFormFieldAnswer_field_ID == field.formField_ID) {
                                            answer = formFieldAnswer;
                                        }
                                    });
                                    if (field) {
                                        if (answer) {
                                            field.answer = answer;
                                        }
                                        else{
                                            field.answer = false;
                                        }
                                    }
                                    var hdl = new FormFieldClass($(d), field);
                                    hdl.init();
                                });
                                break;
                        }
                    }
                }
                this.makeBody = makeBody;
                this.isDataLoaded = function () {
                    return metaData.isDataLoaded;
                }
                this.bindData = bindData;
                this.init = function () {
                    domItemRoot = domRoot.find('.item.sample').clone().removeClass('sample').removeAttr('hidden').appendTo(domContainer);
                    domBody = domItemRoot.find('.item-body');
                    metaData.isDataLoaded = false;
                };
            }

            var bindEvents = function () {
                domContainer.on('afterChange', function (e, slick, currentSlide) {
                    // stop at last slider
                    if (currentSlide == itemHandles.length - 1){
                        // domContainer.slick('draggable', false);
                    }
                    var loadIndex = currentSlide + 2;
                    if (loadIndex < itemHandles.length && !itemHandles[loadIndex].isDataLoaded()) {
                        itemHandles[loadIndex].bindData();
                    }

                });
            }
            this.refreshBody = function () {
                itemHandles.forEach(function (hdl) {
                    if (hdl.isDataLoaded()) {
                        hdl.makeBody();
                    }
                });
            }
            this.init = function () {
                domRoot = domPageRoot.find('section.posts-section');
                domContainer = domRoot.find('.items-container');


                initialPosts.forEach(function (postData, i) {
                    postData.day = i + 1;
                    var handle = new ItemClass(postData);
                    handle.init();
                    if (i < 3){
                        handle.bindData();
                    }
                    itemHandles.push(handle);
                });

                domContainer.slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: false,
                    autoplaySpeed: 5000,
                    centerPadding: '20%',
                    nextArrow: $('.next-pagging'),
                    prevArrow: $('.prev-pagging'),
                    responsive: [
                    ]
                });
                bindEvents();
            }
        }
        var AuthClass = function () {
            var domAuth, domLoginForm, domRegisterForm;

            function showRegisterForm(){
                $('.loginBox').fadeOut('fast',function(){
                    $('.registerBox').fadeIn('fast');
                    $('.login-footer').fadeOut('fast',function(){
                        $('.register-footer').fadeIn('fast');
                    });
                    $('.modal-title').html('Register with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function showLoginForm(){
                $('#loginModal .registerBox').fadeOut('fast',function(){
                    $('.loginBox').fadeIn('fast');
                    $('.register-footer').fadeOut('fast',function(){
                        $('.login-footer').fadeIn('fast');
                    });

                    $('.modal-title').html('Login with');
                });
                $('.error').removeClass('alert alert-danger').html('');
            }
            function openLoginModal(){
                showLoginForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);
            }
            function openRegisterModal(){
                showRegisterForm();
                setTimeout(function(){
                    $('#loginModal').modal('show');
                }, 230);

            }

            function shakeModal(errorMsg){
                if (!errorMsg) {
                    errorMsg = 'Invalid email/password combination';
                }
                $('#loginModal .modal-dialog').addClass('shake');
                $('.error').addClass('alert alert-danger').html(errorMsg);
                $('input[type="password"]').val('');
                setTimeout( function(){
                    $('#loginModal .modal-dialog').removeClass('shake');
                }, 1000 );
            }
            var login = function (username, password) {
                ajaxAPiHandle.pagePost('login.php', {function: 'signin', username: username, password: password}, true, 200).then(function (res) {
                    if (res.result.success) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        ajaxAPiHandle.apiPost('Purchased.php', {action: 'get', where: {intPurchased_series_ID: series.series_ID}}).then(function (pRes) {
                            purchased = pRes.data;
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            if (purchased) {
                                postsSectionHandle.refreshBody();
                            }
                        });
                    }
                    else {
                        shakeModal();
                    }
                });
            }
            var register = function (sets) {
                $('.loading').show();
                var ajaxData = {function: 'register'};
                $.extend(ajaxData, sets);
                ajaxAPiHandle.pagePost('login.php', ajaxData, false).then(function (res) {
                    if (res.result.registered) {
                        CLIENT_ID = res.result.data.id;
                        domAuth.modal('hide');
                        setTimeout(function () {
                            $('.display-name').html(res.result.data.f_name);
                            clientName = res.result.data.f_name;
                            $('.site-wrapper').addClass('logged-in');
                            postsSectionHandle.showWelcome();
                        }, 300);
                    }
                    else {
                        shakeModal('something is wrong');
                    }
                });
            }
            var bindEvents = function () {
                $('.big-login').click(function () {
                    openLoginModal();
                });
                $('.show-register').click(function () {
                    showRegisterForm();
                });
                $('.show-login').click(function () {
                    showLoginForm();
                });
                domLoginForm.submit(function () {
                    var username = domLoginForm.find('[name="email"]').val();
                    var password = domLoginForm.find('[name="password"]').val();
                    login(username, password);
                    return false;
                });
                domRegisterForm.submit(function () {
                    var sets = {};
                    sets.email = domRegisterForm.find('[name="email"]').val();
                    sets.f_name = domRegisterForm.find('[name="first_name"]').val();
                    sets.l_name = domRegisterForm.find('[name="last_name"]').val();
                    sets.password = domRegisterForm.find('[name="password"]').val();
                    var confirmPassword = domRegisterForm.find('[name="password_confirmation"]').val();
                    if (sets.password === confirmPassword) {
                        register(sets);
                    }
                    else {
                        shakeModal("Passwords don't match");
                    }
                    return false;
                });
            }
            this.init = function () {
                domAuth = $('#loginModal');
                domLoginForm = domAuth.find('.loginBox form');
                domRegisterForm = domAuth.find('.registerBox form');
                bindEvents();
            }
        }
        this.init = function () {
            domPageRoot = $('.site-wrapper');
            if (series.boolSeries_charge == 1 && !purchased) {
                if (series.stripeApi_pKey && series.stripe_account_id) {
                    payHandle = StripeCheckout.configure({
                        key: series.stripeApi_pKey,
                        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                        locale: 'auto',
                        token: function(token) {
                            selectPost(token);
                        }
                    });
                }
            }

            postsSectionHandle = new PostsSectionClass();

            postsSectionHandle.init();

            authHandle = new AuthClass();
            authHandle.init();

            embedGlobalHandle = new EmbedPageGlobalClass();
            embedGlobalHandle.init();
        }
    }

    var previewSeriesPageHandle = new PageClass();

    $(document).ready(function () {
        previewSeriesPageHandle.init();
    });
})();