<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 03.01.2019
 * Time: 23:33
 */
function baseUrl(){
    return sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
}
function isAbsUrl($url){
    return preg_match('/^https?\:\/\//', $url, $matches);
}
function absUrl($url) {
    return isAbsUrl($url) ? $url : baseUrl() . '/' . $url;
}
define('BASE_URL', baseUrl());
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <!--    <link rel="stylesheet" href="css/experience_invited.css?version=--><?php //echo time();?><!--" />-->
    <style type="text/css">
        @font-face {
            font-family: 'Font-Muli';
            font-style: normal;
            font-weight: 400;
            src: local("Muli"), url("https://walden.ly/assets/fonts/Muli/Muli-Regular.ttf") format("truetype"); }
        html body {
            margin: 0px; }
        html body .walden-wrapper {
            background-color: #ebecee;
            font-family: 'Font-Muli';
            color: #747762; }
        html body .walden-wrapper .walden-content {
            width: 600px;
            max-width: 100%;
            background-color: white; }
        html body .walden-wrapper .walden-content .walden-content-inner {
            background-color: white; }
        html body .walden-wrapper .walden-content .walden-content-inner .walden-header .walden-logo-wrapper {
            width: 260px; }
        html body .walden-wrapper .walden-content .walden-content-inner .hero-section {
            background-color: #a4c1e3; }
        html body .walden-wrapper .walden-content .walden-content-inner .hero-section .you-invited .you-invited-txt {
            color: white;
            font-size: 30px; }
        html body .walden-wrapper .walden-content .walden-content-inner .hero-section .hero-image-table {
            width: 215px; }
        html body .walden-wrapper .walden-content .walden-content-inner .walden-footer {
            background-color: #1fcd98; }
        html body .walden-wrapper .walden-content .walden-content-inner .walden-footer .walden-menu a {
            text-decoration: none; }
        html body .walden-wrapper a {
            text-decoration: none; }

        .margin-0-auto {
            max-width: 100%;
            margin: 0px auto; }

        /*# sourceMappingURL=experience_invited.css.map */
    </style>
</head>
<body>
<table cellspacing="0" border="0" width="100%" class="walden-wrapper" style="background-color: #ebecee;">
    <tr>
        <td align="center" valign="top" style="padding-top: 20px; padding-bottom: 20px;">
            <table cellspacing="0" border="0" class="walden-content" width="750" style="background-color: white;">
                <tbody>
                <tr>
                    <td align="center" valign="top" class="walden-content-inner">
                        <?php require_once __DIR__ . '/common/email_header.php';?>
                        <table cellspacing="0" border="0" width="100%" class="hero-section" style="background-color: #71e29e;">
                            <tbody>
                            <tr>
                                <td align="center" valign="center" style="padding: 40px 40px">
                                    <table cellspacing="0" border="0" width="425" style="margin: 0px auto 30px;">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="center">
                                                <div style="text-align: center; color: white; font-size: 35px; font-weight: bold; letter-spacing: 1px;">
                                                    There's something new in <?php echo $_GET['series_title'];?>!
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table cellspacing="0" width="100%" border="0" class="">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="center">
                                                <img width="100%" src="<?php echo BASE_URL; ?>/assets/email-templates/images/add_posts/add_postsArtboard-4.png" alt="">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="">
                            <tbody>
                            <tr>
                                <td align="center" valign="center" style="padding: 30px">
                                    <div style="font-size: 30px; font-weight: bold; text-align: center; line-height: 50px; color: #7b7661;">
                                        <?php echo $_GET['name'];?> just added this great content!<br>
                                        Take a look...
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="series-posts" style="">
                            <tbody>
                            <tr>
                                <td align="left" valign="center" style="padding: 10px 30px;">
                                    <table cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td valign="center" align="left" style="font-size: 25px; font-weight: bold; padding-left: 10px; padding-right: 15px; color: #737661;"><?php echo $_GET['series_title_short'];?></td>
                                            <td valign="center" align="left" style="padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; background-color: #c8cbba; border-radius: 10px;">
                                                <table style="color: white; vertical-align: middle;">
                                                    <tbody>
                                                    <tr>
                                                        <td><?php echo $_GET['posts_count'];?> posts</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" cellpadding="0" cellspacing="10" border="0" style="border-collapse: separate;">
                                        <tbody>
                                        <tr>
                                            <?php foreach ($_GET['posts'] as $key => $post): ?>
                                                <td valign="top" align="center" width="160">
                                                    <table width="100%" cellspacing="0" border="0" style="">
                                                        <tr>
                                                            <td valign="center" align="left" width="17">
                                                                <table cellpadding="0" border="0" style="table-layout: fixed;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="center" align="center" width="17">
                                                                            <img width="100%" style="vertical-align: middle;" src="<?php echo (int) $post['intPost_type'] === 0 ? absUrl('assets/email-templates/images/series-you-invited/volume.svg') : ((int) $post['intPost_type'] === 2 ? absUrl('assets/email-templates/images/series-you-invited/youtube-logo.svg') : absUrl('assets/email-templates/images/series-you-invited/text-file.svg'));?>" alt="" />
                                                                        </td>
                                                                        <td valign="center" align="center">
                                                                            <span style="vertical-align: middle; color: #868686;">day <?php echo (int) $post['viewDay'];?></span>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="100" style="height: 100px;">
                                                                <img width="100%" height="100%" src="<?php echo absUrl($post['strPost_featuredimage']);?>" alt="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            <span style="color: #868686;">
                                                                <?php echo $post['strPost_title'];?>
                                                            </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            <?php endforeach; ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table cellspacing="10" border="0" width="100%" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td valign="center" align="center" style="padding-bottom: 40px;">
                                                <table cellspacing="10" border="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td width="30" valign="center" align="center">
                                                            <a href="<?php echo BASE_URL;?>/preview_series?id=<?php echo $_GET['series_id'];?>" style="color: #c8cbba; text-decoration: underline; font-size: 18px;"><img width="100%" style="vertical-align: middle;" src="<?php echo BASE_URL;?>/assets/email-templates/images/series-you-invited/windows-8.svg" alt=""></a>
                                                        </td>
                                                        <td valign="center" align="center">
                                                            <a href="<?php echo BASE_URL;?>/preview_series?id=<?php echo $_GET['series_id'];?>" style="color: #c8cbba; text-decoration: underline; font-size: 18px;">View all of the content currently in the experience</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td align="center" style="padding-bottom: 20px;">
                                                <table cellspacing="0" width="360" border="0" class="accept-table" style="max-width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="center" style="padding: 15px ; border-radius: 30px; background-color: #20cd97;">
                                                            <a class="accept-btn" href="<?php echo BASE_URL;?>/my_solutions" style="font-size: 24px; color: white; text-decoration: none !important;; text-decoration: none;">View my content</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="i-believe" style="background-color: #ffda70;">
                            <tbody>
                            <tr>
                                <td align="center" valign="center" style="padding: 20px 5px;">
                                    <table width="500" cellpadding="0" border="0" style="margin: 0px auto;">
                                        <tbody>
                                        <tr>
                                            <td valign="top" align="left" style="padding-right: 10px;">
                                                <span style="color: #ffd241; font-size: 100px; font-family: serif;">“</span>
                                            </td>
                                            <td valign="center" align="left" style="padding-right: 20px; padding-top: 10px;">
                                                <span style="color: white; font-size: 20px; line-height: 36px; font-style: italic;">It is the ultimate luxury to combine passion and contribution.<br>
                                                    It's also a very clear path to happiness.</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php require_once __DIR__ . '/common/email_footer.php';?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
