<table cellspacing="0" width="100%" border="0" class="walden-footer content-section" style="background-color: #1fcd98">
    <tbody>
    <tr>
        <td align="center" style="padding: 15px 0px;">
            <table>
                <tr>
                    <td align="center" valign="center" style="padding-bottom: 5px;">
                        <a href="<?php echo BASE_URL; ?>" style="color: white; font-size: 30px; font-weight: bold;">Walden.ly</a>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="center" style="padding-bottom: 5px;">
                        <span style="color: white;">copyright 2018. All rights reserved.</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="center" style="padding-bottom: 0px;">
                        <span style="color: white;">Click here to </span> <a href="javascript:;" style="color: white; text-decoration: none;">unsubscribe</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>