<table cellspacing="0" border="0" class="walden-header content-section">
    <tbody>
    <tr>
        <td align="center" valign="top" class="section-inner" style="padding: 25px 0px;">
            <table cellspacing="0" border="0" width="260" class="walden-logo-wrapper margin-0-auto">
                <tbody>
                <tr>
                    <td height="40">
                        <img class="walden-logo" width="100%" height="40"
                             src="<?php echo BASE_URL; ?>/assets/images/global-icons/site-logo.png"
                             alt="" />
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>