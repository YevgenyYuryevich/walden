<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 26.11.2018
 * Time: 05:08
 */

function baseUrl(){
    return sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
}
function isAbsUrl($url){
    return preg_match('/^https?\:\/\//', $url, $matches);
}
function absUrl($url) {
    return isAbsUrl($url) ? $url : baseUrl() . '/' . $url;
}
define('BASE_URL', baseUrl());
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <!--    <link rel="stylesheet" href="css/experience_invited.css?version=--><?php //echo time();?><!--" />-->
    <style type="text/css">
        @font-face {
            font-family: 'Font-Muli';
            font-style: normal;
            font-weight: 400;
            src: local("Muli"), url("https://walden.ly/assets/fonts/Muli/Muli-Regular.ttf") format("truetype"); }
        html body {
            margin: 0px; }
        html body .walden-wrapper {
            background-color: #ebecee;
            font-family: 'Font-Muli';
            color: #747762; }
        html body .walden-wrapper .walden-content {
            width: 600px;
            max-width: 100%;
            background-color: white; }
        html body .walden-wrapper .walden-content .walden-content-inner {
            background-color: white; }
        html body .walden-wrapper .walden-content .walden-content-inner .walden-header .walden-logo-wrapper {
            width: 260px; }
        html body .walden-wrapper .walden-content .walden-content-inner .hero-section {
            background-color: #a4c1e3; }
        html body .walden-wrapper .walden-content .walden-content-inner .hero-section .you-invited .you-invited-txt {
            color: white;
            font-size: 30px; }
        html body .walden-wrapper .walden-content .walden-content-inner .hero-section .hero-image-table {
            width: 215px; }
        html body .walden-wrapper .walden-content .walden-content-inner .walden-footer {
            background-color: #1fcd98; }
        html body .walden-wrapper .walden-content .walden-content-inner .walden-footer .walden-menu a {
            text-decoration: none; }
        html body .walden-wrapper a {
            text-decoration: none; }

        .margin-0-auto {
            max-width: 100%;
            margin: 0px auto; }

        /*# sourceMappingURL=experience_invited.css.map */
    </style>
</head>
<body>
<table cellspacing="0" border="0" width="100%" class="walden-wrapper" style="background-color: #ebecee;">
    <tr>
        <td align="center" valign="top" style="padding-top: 20px; padding-bottom: 20px;">
            <table cellspacing="0" border="0" class="walden-content" width="600" style="background-color: white;">
                <tbody>
                <tr>
                    <td align="center" valign="top" class="walden-content-inner">
                        <table cellspacing="0" border="0" class="walden-header content-section">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" class="section-inner" style="padding: 25px 0px;">
                                    <table cellspacing="0" border="0" width="260" class="walden-logo-wrapper margin-0-auto">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img class="walden-logo" width="100%" height="40"
                                                     src="<?php echo BASE_URL; ?>/assets/images/global-icons/site-logo.png"
                                                     alt="" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0"  width="100%" border="0" class="hero-section content-section" style="background-color: #a4c1e3;">
                            <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" border="0" width="350" class="you-invited">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 20px 0px;">
                                                <div class="you-invited-txt" style="color: white; font-size: 30px; line-height: 45px; font-weight: bold; text-align: center;">
                                                    Will you join <?php echo $_GET['name'];?> and contribute to <?php echo $_GET['series_title'];?>?
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" border="0" width="300" class="hero-image-table">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img width="100%"
                                                     src="<?php echo BASE_URL;?>/assets/email-templates/images/series-you-invited/email_main_image.svg"
                                                     alt="">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="make-life-section content-section">
                            <tbody>
                            <tr>
                                <td align="center" valign="center" style="padding-bottom: 20px;">
                                    <table>
                                        <tr>
                                            <td align="center" style="padding-top: 30px;">
                                                <div style="font-size: 38px; font-weight: bold; text-align: center; line-height: 45px; color: #747762;">
                                                    Help create a great experience!
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 10px 40px 20px;">
                                                <div style="font-size: 20px; line-height: 36px; color: #494949;">
                                                    <p>
                                                    <span><?php echo $_GET['name'];?></span> has created an online experienced called <span style="color: #747762;"><?php echo $_GET['series_title'];?></span> and thinks that your knowledge and interests make you the perfect person to contribute to this experience. Contributing is as easy as dragging and dropping content or webpages.  We hope you will join your friend's efforts by adding exciting, engaging, and high-quality content to this online experience.
                                                    </p>
                                                    <p>
                                                    <strong>Here is a preview of the online experience.</strong></p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="series-posts" style="">
                            <tbody>
                            <tr>
                                <td align="left" valign="center" style="padding: 10px 30px;">
                                    <table cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td valign="center" align="left" style="font-size: 25px; font-weight: bold; padding-left: 10px; padding-right: 15px; color: #737661;"><?php echo $_GET['series_title_short'];?></td>
                                            <td valign="center" align="left">
                                                <table style="padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; background-color: #c8cbba; color: white; vertical-align: middle; border-radius: 10px;">
                                                    <tbody>
                                                    <tr>
                                                        <td><?php echo $_GET['posts_count'];?> posts</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" cellpadding="0" cellspacing="10" border="0" style="border-collapse: separate;">
                                        <tbody>
                                        <tr>
                                            <?php foreach ($_GET['posts'] as $key => $post): ?>
                                                <td valign="top" align="center" width="160">
                                                    <table width="100%" cellspacing="0" border="0" style="">
                                                        <tr>
                                                            <td valign="center" align="left" width="17">
                                                                <table cellpadding="0" border="0" style="table-layout: fixed;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="center" align="center" width="17">
                                                                            <img width="100%" style="vertical-align: middle;" src="<?php echo (int) $post['intPost_type'] === 0 ? absUrl('assets/email-templates/images/series-you-invited/volume.svg') : ((int) $post['intPost_type'] === 2 ? absUrl('assets/email-templates/images/series-you-invited/youtube-logo.svg') : absUrl('assets/email-templates/images/series-you-invited/text-file.svg'));?>" alt="" />
                                                                        </td>
                                                                        <td valign="center" align="center">
                                                                            <span style="vertical-align: middle; color: #868686;">day <?php echo ($key + 1);?></span>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="100" style="height: 100px;">
                                                                <img width="100%" height="100%" src="<?php echo absUrl($post['strPost_featuredimage']);?>" alt="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            <span style="color: #868686;">
                                                                <?php echo $post['strPost_title'];?>
                                                            </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            <?php endforeach; ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table cellspacing="10" border="0" width="100%" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td valign="center" align="center" style="padding-bottom: 40px;">
                                                <table cellspacing="10" border="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td width="30" valign="center" align="center">
                                                            <a href="<?php echo BASE_URL;?>/preview_series?id=<?php echo $_GET['series_id'];?>" style="color: #c8cbba; text-decoration: underline; font-size: 18px;"><img width="100%" style="vertical-align: middle;" src="<?php echo BASE_URL;?>/assets/email-templates/images/series-you-invited/windows-8.svg" alt=""></a>
                                                        </td>
                                                        <td valign="center" align="center">
                                                            <a href="<?php echo BASE_URL;?>/preview_series?id=<?php echo $_GET['series_id'];?>" style="color: #c8cbba; text-decoration: underline; font-size: 18px;">View all of the content currently in the experience</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td align="center" style="padding-bottom: 20px;">
                                                <table cellspacing="0" width="360" border="0" class="accept-table" style="max-width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="center" style="padding: 15px ; border-radius: 30px; background-color: #20cd97;">
                                                            <a class="accept-btn" href="<?php echo BASE_URL;?>/series_invitation_manager?action=accept&id=<?php echo $_GET['id'];?>" style="font-size: 24px; color: white; text-decoration: none !important;; text-decoration: none;">Accept <?php echo $_GET['name'];?>&apos;s Invitation</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="i-believe" style="background-color: #ffda70;">
                            <tbody>
                            <tr>
                                <td align="center" valign="center" style="padding: 5px;">
                                    <table width="400" cellpadding="0" border="0" style="margin: 0px auto;">
                                        <tbody>
                                        <tr>
                                            <td valign="top" align="left" style="padding-right: 10px;">
                                                <span style="color: #ffd241; font-size: 100px; font-family: serif;">“</span>
                                            </td>
                                            <td valign="center" align="left" style="padding-right: 20px; padding-top: 10px;">
                                                <span style="color: white; font-size: 20px; line-height: 36px; font-style: italic;">The shape and solutions of the future rely totally on the collective effort of people working together.  We are all an integral part of the web of life.</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="how-works-section content-section">
                            <tbody>
                            <tr>
                                <td align="center" style="padding-top: 25px;">
                                    <table width="100%" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="center" style="padding-bottom: 30px;">
                                                <div style="font-size: 35px; font-weight: bold; color: #747762;">How Walden.ly works</div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="center" style="padding-bottom: 50px;">
                                    <table cellspacing="0" width="510" style="max-width: 100%; table-layout: fixed; margin: 0px auto;" border="0" class="">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="center" style="width: 210px; padding: 20px 0px;" width="210px">
                                                <img width="100%" src="<?php echo BASE_URL;?>/assets/email-templates/images/series-you-invited/shutterstock_1044482239.svg" />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td align="center" valign="center" style="width: 210px; padding: 20px 0px;" width="210px">
                                                <table cellspacing="0" width="100%" border="0" class="">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="center" style="padding-bottom: 5px;">
                                                            <div style="text-decoration: none; font-size: 20px; line-height: 1.5; font-weight: bold; color: #747762;">At Walden.ly, </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="center">
                                                            <div style="font-size: 20px; line-height: 1.5; color: #747762;">
                                                               people work together to create great online experiences.
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="center" style="padding-top: 10px;">
                                                            <img
                                                                    src="<?php echo BASE_URL; ?>/assets/email-templates/images/web-search.png"
                                                                    alt="">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="center" style="width: 210px; padding: 20px 0px;" width="210px">
                                                <table cellspacing="0" width="100%" border="0" class="">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="center" style="padding-bottom: 5px;">
                                                            <div style="font-size: 20px; font-weight: bold; color: #747762;">When you join your friend,</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="center">
                                                            <div style="font-size: 20px; color: #747762;">
                                                                you can easily add and organize great content from across the web to create a powerful online experience together.
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td align="center" valign="center" style="width: 210px; padding: 20px 0px;" width="210px">
                                                <img width="100%" src="<?php echo BASE_URL;?>/assets/email-templates/images/series-you-invited/shutterstock_1054175036.svg" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="center" style="width: 210px; padding: 20px 0px;" width="210px">
                                                <img width="100%" src="<?php echo BASE_URL;?>/assets/email-templates/images/series-you-invited/shutterstock_1106231384.svg" />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td align="center" valign="center" style="width: 210px; padding: 20px 0px;" width="210px">
                                                <table cellspacing="0" width="100%" border="0" class="">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="center" style="padding-bottom: 5px;">
                                                            <div style="font-size: 20px; font-weight: bold; color: #747762;">Your work can be shared or kept private</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="center">
                                                            <div style="font-size: 20px; color: #747762;">
                                                                You can keep your series private between friends or family or share it with the world. The choice is yours.
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding-bottom: 20px;">
                                    <table cellspacing="0" width="450" border="0" class="accept-table" style="max-width: 100%;">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="center" style="padding: 15px ; border-radius: 30px; background-color: #20cd97;">
                                                <a class="accept-btn" href="<?php echo BASE_URL;?>/series_invitation_manager?action=accept&id=<?php echo $_GET['id'];?>" style="text-decoration: none !important;; text-decoration: none;font-size: 24px; color: white;">Join and begin adding great content</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding-bottom: 20px;">
                                    <!---
                                    <table cellspacing="0" width="180" border="0" class="decline-table">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="center" style="border: 1px solid #20cd97; padding: 10px; background-color: white; border-radius: 20px;">
                                                <a class="decline-btn" href="<?php echo BASE_URL;?>/series_invitation_manager?action=decline&id=<?php echo $_GET['id'];?>" style="color: #747762; text-decoration: none; font-size: 18px;">Decline invitation</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
--->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" width="100%" border="0" class="walden-footer content-section" style="background-color: #1fcd98">
                            <tbody>
                            <tr>
                                <td align="center" style="padding: 15px 0px;">
                                    <table>
                                        <tr>
                                            <td align="center" valign="center" style="padding-bottom: 5px;">
                                                <a href="<?php echo BASE_URL; ?>" style="color: white; font-size: 30px; font-weight: bold;">Walden.ly</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="center" style="padding-bottom: 5px;">
                                                <span style="color: white;">copyright 2018. All rights reserved.</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="center" style="padding-bottom: 0px;">
                                                <span style="color: white;">Click here to </span> <a href="javascript:;" style="color: white; text-decoration: none;">unsubscribe</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
</body>
</html>