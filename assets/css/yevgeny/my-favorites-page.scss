@import "../../config/css/colors.scss";

html{
    body{
        .site-wrapper.page{
            .site-content{
                text-align: left;
                .filter-wrapper{
                    padding: 50px 70px 30px;
                    background-color: $lightGreen;
                    position: relative;
                    &:after{
                        content: '';
                        display: block;
                        position: absolute;
                        bottom: 0px;
                        left: 0px;
                        width: 100%;
                        height: 1px;
                        background-image: linear-gradient(to right, #1fcd98 1px, transparent 0%);
                        background-size: 5px 1px;
                        background-repeat: repeat-x;
                    }
                    .filter-content{
                        max-width: 100%;
                        margin: 0px auto;
                        text-align: left;
                        .my-favorites{
                            .filter-name{
                                margin: 0px 0px 15px;
                                font-size: 3rem;
                                font-weight: bold;
                            }
                            .filter-category{
                                display: block;
                                text-decoration: none;
                                position: relative;
                                margin: 0px 5px 10px;
                                padding-bottom: 1px;
                                font-size: 1.5rem;
                                color: $contentColor;
                                opacity: .6;
                                &:after{
                                    content: '';
                                    display: block;
                                    position: absolute;
                                    bottom: 0px;
                                    left: 0px;
                                    width: 100%;
                                    height: 1px;
                                    background-color: $contentColor;
                                }
                                &:hover{
                                    opacity: 1;
                                }
                                &.active{
                                    opacity: 1;
                                }
                            }
                        }
                        .sort-by{
                            text-align: left;
                            .filter-name{
                                margin: 0px 0px 15px;
                                font-size: 1.5em;
                            }
                            .dropdown{
                                display: inline-block;
                                button{
                                    background-color: transparent;
                                    color: inherit;
                                    border-radius: 9px;
                                    span{
                                        display: inline-block;
                                        width: 80px;
                                        color: inherit;
                                        text-align: left;
                                    }
                                    svg{
                                        width: 15px;
                                        vertical-align: middle;
                                    }
                                    &:focus{
                                        outline: none;
                                    }
                                }
                                ul{
                                    width: 100%;
                                    min-width: 0px;
                                    margin-top: 0px;
                                    border-top: 0px;
                                    border-radius: 9px;
                                    border-top-left-radius: 0px;
                                    border-top-right-radius: 0px;
                                    background-color: #f6fffc;
                                    li{
                                        a{
                                            color: inherit;
                                            &:hover{
                                                background-color: $contentColor;
                                                color: white;
                                            }
                                        }
                                    }
                                }
                                &.open{
                                    button{
                                        border-bottom-left-radius: 0px;
                                        border-bottom-right-radius: 0px;
                                    }
                                }
                            }
                        }
                        .categories-container-mobile{
                            .dropdown{
                                display: inline-block;
                                button{
                                    background-color: transparent;
                                    color: inherit;
                                    border-radius: 9px;
                                    span{
                                        display: inline-block;
                                        width: 150px;
                                        color: inherit;
                                        text-align: left;
                                        overflow: hidden;
                                        text-overflow: ellipsis;
                                        vertical-align: middle;
                                    }
                                    svg{
                                        width: 15px;
                                        vertical-align: middle;
                                    }
                                    &:focus{
                                        outline: none;
                                    }
                                }
                                ul{
                                    width: 100%;
                                    min-width: 0px;
                                    margin-top: 0px;
                                    border-top: 0px;
                                    border-radius: 9px;
                                    border-top-left-radius: 0px;
                                    border-top-right-radius: 0px;
                                    background-color: #f6fffc;
                                    li{
                                        a{
                                            color: inherit;
                                            overflow: hidden;
                                            text-overflow: ellipsis;
                                            &:hover{
                                                background-color: $contentColor;
                                                color: white;
                                            }
                                        }
                                    }
                                }
                                &.open{
                                    button{
                                        border-bottom-left-radius: 0px;
                                        border-bottom-right-radius: 0px;
                                    }
                                }
                            }
                        }
                    }
                    @media all and (max-width: 768px){
                        padding: 30px 10px 20px;
                        .filter-content{
                            .my-favorites{
                                .filter-name{
                                    margin: 10px 0px;
                                }
                            }
                        }
                        &:after{
                            display: none;
                        }
                    }
                }
                main.main-content{
                    max-width: 100%;
                    margin: 0px auto;
                    padding: 20px 75px;
                    .series-col{
                        flex-basis: 200px;
                        ul.series-list{
                            list-style: none;
                            padding-left: 0px;
                            transition: all .3s ease-out;

                            li{
                                margin-bottom: 5px;
                                font-size: 1.6rem;
                                a{
                                    color: inherit;
                                    text-decoration: none;
                                    i{
                                        font-size: .8rem;
                                        font-weight: bold;
                                        top: 0px;
                                        left: 0px;
                                        transition: all .1s ease-out;
                                    }
                                    span{
                                        margin-left: 10px;
                                    }
                                    &:hover{
                                        color: black;
                                        i{
                                            left: 5px;
                                        }
                                    }
                                }
                                &.level-0{
                                    i{
                                        color: $greenColor;
                                    }
                                }
                                &.level-1{
                                    i{
                                        color: $redColor;
                                    }
                                }
                                &.active{
                                    a{
                                        color: black;
                                        i{
                                            left: 5px;
                                        }
                                    }
                                }
                            }
                            @media all and (max-width: 768px){
                                overflow: auto;
                                padding: 15px 0px;
                                background-color: $greenColor;
                                overflow-scrolling: touch;
                                -webkit-overflow-scrolling: touch;
                                margin: 0px -20px 0px;
                                li{
                                    padding: 0px 15px;
                                    font-size: 1.7rem;
                                    line-height: 1.1;
                                    color: white;
                                    border-left: 1px solid white;
                                    text-decoration: none;
                                    a{
                                        color: inherit;
                                        text-decoration: none;
                                        white-space: pre;
                                        span{
                                            margin-left: 10px;
                                        }
                                        &:hover{
                                            color: white;
                                        }
                                    }
                                    &.active{
                                        a{
                                            color: white;
                                            font-weight: bold;
                                        }
                                    }
                                    &.all-item{
                                        border-left: none;
                                    }
                                }
                            }
                        }
                    }
                    .current-series{
                        margin: 0px -20px;
                        padding: 30px 40px 20px;
                        background-color: $lightGreen;
                        font-size: 2.5rem;
                    }
                    .posts-col{
                        margin-left: 50px;
                        .posts-container{
                            transition: all .3s ease-out;
                            .item{
                                flex-basis: calc(100% / 4);
                                margin-bottom: 20px;
                                padding: 0px 15px;
                                font-size: 1.5rem;
                                .item-header{
                                    .media-icon-wrapper{
                                        i.fa{
                                            display: none;
                                            vertical-align: middle;
                                        }
                                    }
                                    .fav-star{
                                        display: block;
                                        line-height: 0;
                                        svg{
                                            width: 20px;
                                            g, path{
                                                fill: $greenColor;
                                            }
                                        }
                                    }
                                }
                                .item-type-body{
                                    width: 100%;
                                    height: calc(100px + 5vw);
                                    position: relative;
                                    margin-bottom: 15px;
                                    .blog-type{
                                        position: absolute;
                                        top: 0px;
                                        left: 0px;
                                        width: 100%;
                                        height: 100%;
                                        overflow: hidden;
                                        img{
                                            width: 100%;
                                            height: 100%;
                                        }
                                        &.type-0{
                                            background-image: url("../../images/global-icons/audio-spectr.png");
                                            background-position: center center;
                                            background-repeat: no-repeat;
                                        }
                                    }
                                    aside.blog-duration{
                                        position: absolute;
                                        bottom: 8px;
                                        right: 9px;
                                        padding: 7px 20px;
                                        z-index: 1;
                                        background: rgba(227, 228, 220, .8);
                                        border-radius: 10px;
                                        font-size: 1.2rem;
                                        line-height: 1.1;
                                    }
                                }
                                .item-title{
                                    display: block;
                                    width: 100%;
                                    color: $contentColor;
                                }
                                .item-series-wrapper{
                                    .item-series{
                                        color: $lightGrey;
                                        text-decoration: underline $lightGrey;
                                        &:hover{
                                            color: black;
                                            text-decoration-color: black;
                                        }
                                    }
                                }
                                &.type-0{
                                    .item-header{
                                        .media-icon-wrapper{
                                            i.fa.audio-icon{
                                                display: initial;
                                                vertical-align: middle;
                                            }
                                        }
                                    }
                                }
                                &.type-2{
                                    .item-header{
                                        .media-icon-wrapper{
                                            i.fa.video-icon{
                                                display: initial;
                                                vertical-align: middle;
                                            }
                                        }
                                    }
                                }
                                &.type-7, &.type-8, &.type-10{
                                    .item-header{
                                        .media-icon-wrapper{
                                            i.fa.text-icon{
                                                display: initial;
                                                vertical-align: middle;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        @media all and (max-width: 768px) {
                            margin-left: 0px;
                            .posts-container{
                                width: 380px;
                                max-width: 100%;
                                margin: 0px auto;
                                .item{
                                    flex-basis: calc(100% / 1);
                                    margin-bottom: 20px;
                                    padding: 0px 15px;
                                    font-size: 1.5rem;
                                    .item-header{
                                        .media-icon-wrapper{
                                            i.fa{
                                                display: none;
                                                vertical-align: middle;
                                            }
                                        }
                                        .fav-star{
                                            display: block;
                                            line-height: 0;
                                            svg{
                                                width: 20px;
                                                g, path{
                                                    fill: $greenColor;
                                                }
                                            }
                                        }
                                    }
                                    .item-type-body{
                                        height: calc(150px + 5vw);
                                        margin-bottom: 15px;
                                    }
                                    .item-title{
                                    }
                                    .item-series-wrapper{
                                        .item-series{
                                        }
                                    }
                                }
                            }
                        }
                    }
                    @media all and (max-width: 768px) {
                        padding: 0px 20px 20px;
                    }
                }
                .no-content-wrapper{
                    display: none;
                    width: 1650px;
                    max-width: 90%;
                    margin: 100px auto;
                    background-color: $redColor;
                    padding: 50px 30px 20px;
                    border-radius: 25px;
                    color: white;
                    .no-content-txt-wrapper{
                        text-align: center;
                        .you-not-txt{
                            font-weight: bold;
                            font-size: 3.3rem;
                            line-height: 1.5;
                            margin-bottom: 50px;
                        }
                        .to-add-txt{
                            width: 540px;
                            max-width: 100%;
                            margin: 0px auto 20px;
                            font-size: 3rem;
                            line-height: 1.5;
                        }
                    }
                    .no-content-img-wrapper{
                        position: relative;
                        text-align: center;
                        .background-img{
                            max-width: 100%;
                        }
                        .to-add-icons-wrapper{
                            position: absolute;
                            top: 0px;
                            left: 0px;
                            width: 100%;
                            height: 100%;
                            padding-bottom: 50px;
                            .action-item{
                                background-color: transparent;
                                line-height: 0;
                                border-radius: 50%;
                                border: 2px solid $greenColor;
                                height: 100px;
                                width: 100px;
                                box-sizing: border-box;
                                svg{
                                    width: 60px;
                                    max-height: 60px;
                                    g, path, polygon, circle{
                                        fill: $greenColor !important;
                                    }
                                }
                                &.complete-action{
                                    &.completed{
                                        background-color: $greenColor;
                                        svg{
                                            g, path, polygon{
                                                fill: white !important;
                                            }
                                        }
                                    }
                                }
                                &.favorite-action{
                                    border-color: $yellowColor;
                                    svg{
                                        g, path, polygon{
                                            fill: $yellowColor !important;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    .to-add-txt-mobile{
                        text-align: center;
                        width: 540px;
                        max-width: 100%;
                        margin: 20px auto 0px;
                        font-size: 2rem;
                        line-height: 1.5;
                    }
                    @media all and (max-width: 768px) {
                        max-width: calc(100% - 20px);
                        margin: 30px auto;
                        padding: 30px 10px 20px;
                        .no-content-txt-wrapper{
                            .you-not-txt{
                                font-size: 2.5rem;
                                line-height: 1.5;
                                margin-bottom: 30px;
                            }
                        }
                        .no-content-img-wrapper{
                            .background-img{

                            }
                            .to-add-icons-wrapper{
                                padding-bottom: 50px;
                                .action-item{
                                    width: calc(100px - 60 * (768px - 100vw) / 768);
                                    height: calc(100px - 60 * (768px - 100vw) / 768);
                                    svg{
                                        width: 60%;
                                        max-height: 60%;
                                    }
                                }
                            }
                        }
                    }
                }
                &.no-content{
                    .no-content-wrapper{
                        display: block;
                    }
                    .d-no-content-none{
                        display: none !important;
                    }
                }
            }
        }
    }
}