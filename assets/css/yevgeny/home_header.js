(function () {
    'use strict';

    var HeaderClass = function () {

        var domRoot;

        var bindEvents = function () {
        }

        this.init = function () {
            domRoot = $('header.home-header');
            bindEvents();
        }
    }
    var HeaderSearchClass = function () {


        var flgOpend = false;
        var domRoot = $('#header-search-results');
        var domHeaderSearchForm = $('.home-header form.search-for-inspiration');
        var domFooterSearchForm = $('.home-footer form.search-for-inspiration');
        var domSearchForm = domRoot.find('.search-term-wrapper form');
        var domSearch = domSearchForm.find('input');

        var DEFAULT_IMAGE = 'assets/images/beautifulideas.jpg';
        var series = [];

        var youtubeListHandle = null;
        var podcastsListHandle = null;
        var blogsListHandle = null;
        var recipesListHandle = null;
        var ideaboxListHandle = null;
        var postsListHandle = null;
        var rssbPostsListHandle = null;
        var self = this;

        var searchPosts = function (kwd, size, items) {
            return ajaxAPiHandle.apiPost('SearchPosts.php', {q: kwd, size: size, items: items});
        }

        var bindEvents = function () {
            domHeaderSearchForm.submit(function () {
                if (!flgOpend){
                    $('html').addClass('header-search-open');
                    flgOpend = true;
                }
                domSearch.val(domHeaderSearchForm.find('input').val());
                domSearchForm.submit();
                return false;
            });
            domFooterSearchForm.submit(function () {
                if (!flgOpend){
                    $('html').addClass('header-search-open');
                    flgOpend = true;
                }
                domSearch.val(domFooterSearchForm.find('input').val());
                domSearchForm.submit();
                return false;
            });
            domSearchForm.submit(function () {

                var items = [
                    {name: 'youtube'},
                    {name: 'spreaker'},
                    {name: 'recipes'},
                    {name: 'blogs'},
                    {name: 'ideabox'},
                    {name: 'posts'},
                    {name: 'rssbPosts'}
                ];
                searchPosts(domSearch.val(), 10, items).then(function (res) {
                    youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                    podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                    blogsListHandle.setItems(res.data.blogs, domSearch.val());
                    recipesListHandle.setItems(res.data.recipes, domSearch.val());
                    ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                    postsListHandle.setItems(res.data.posts, domSearch.val());
                    rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
                });
                return false;
            });
            domSearchForm.find('.clear-search-term').click(function () {
                domSearch.val('');
                domSearchForm.submit();
            });
            domRoot.find('.show-more').click(function () {
                self.showMore();
            });
            domRoot.find('[name="from"]').change(function () {
                var val = $(this).val();
                var checked = $(this).prop('checked');
                var handle = null;
                switch (val){
                    case 'youtube':
                        handle = youtubeListHandle;
                        break;
                    case 'spreaker':
                        handle = podcastsListHandle;
                        break;
                    case 'twingly':
                        handle = blogsListHandle;
                        break;
                    case 'recipes':
                        handle = recipesListHandle;
                        break;
                    case 'ideabox':
                        handle = ideaboxListHandle;
                        break;
                    case 'posts':
                        handle = postsListHandle;
                        break;
                    case 'rssbposts':
                        handle = rssbPostsListHandle;
                        break;
                    default:
                        handle = youtubeListHandle;
                        break;
                }
                checked ? handle.show() : handle.hide();
            });
            domRoot.find('.i-am-done').click(function () {
                self.IAmDone();
            });
            domRoot.find('.results-from').find('.dropdown-menu').find('li').click(function () {
                var checkbox = $(this).find('input');
                checkbox.prop('checked', !checkbox.prop('checked')).change();
                return false;
            });
        }

        var YoutubeListClass = function () {
            var domResultsRoot = domRoot.find('.youtube-grid.tt-grid-wrapper');

            var self = this;
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];

            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intSeries_client_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {

                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                }

                this.viewPost = function () {
                    var data = self.getFormatData();
                    var sery = series[domSelectSery.val()];
                    var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                    $form.append('<input name="action" value="view"/>');
                    $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                    $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                    $form.append('<input name="type" value="'+ data.type +'"/>');
                    $form.append('<input name="title" value="'+ data.title +'"/>');
                    $form.append('<input name="body" value="'+ data.body +'"/>');
                    $form.append('<input name="img" value="'+ data.image +'"/>');
                    $form.submit();
                    $form.remove();
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }

                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.title = itemData.snippet.title;
                this.summary = itemData.snippet.description;
                this.body = 'https://www.youtube.com/embed/' + itemData.id.videoId;
                this.featuredImage = itemData.snippet.thumbnails.high.url;
                this.type = 2;
                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }
                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domItemRoot.find('.remove-wrapper').remove();
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }
            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }

            this.keyword = '';
            this.nextPageToken = false;
            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.init = function () {
            }
        }

        var PodcastsListClass = function () {
            var domResultsRoot = domRoot.find('.podcasts-grid.tt-grid-wrapper');

            var self = this;
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];

            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intSeries_client_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {

                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                }

                this.viewPost = function () {
                    var data = self.getFormatData();
                    var sery = series[domSelectSery.val()];
                    var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                    $form.append('<input name="action" value="view"/>');
                    $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                    $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                    $form.append('<input name="type" value="'+ data.type +'"/>');
                    $form.append('<input name="title" value="'+ data.title +'"/>');
                    $form.append('<input name="body" value="'+ data.body +'"/>');
                    $form.append('<input name="img" value="'+ data.image +'"/>');
                    $form.submit();
                    $form.remove();
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }

                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.title = itemData.title;
                this.summary = '';
                this.body = itemData.episode_id;
                this.body = 'https://api.spreaker.com/v2/episodes/' + itemData.episode_id + '/play';
                this.featuredImage = itemData.image_url;
                this.type = 0;
                this.selected = false;

                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }

                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domItemRoot.find('.remove-wrapper').remove();
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }

            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.keyword = '';
            this.nextPageToken = false;

            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.init = function () {
            }
        }

        var BlogsListClass = function () {
            var domResultsRoot = domRoot.find('.blogs-grid.tt-grid-wrapper');

            var self = this;
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];

            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intSeries_client_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {

                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                }

                this.viewPost = function () {
                    var data = self.getFormatData();
                    var sery = series[domSelectSery.val()];
                    var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                    $form.append('<input name="action" value="view"/>');
                    $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                    $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                    $form.append('<input name="type" value="'+ data.type +'"/>');
                    $form.append('<input name="title" value="'+ data.title +'"/>');
                    $form.append('<input name="body" value="'+ data.body +'"/>');
                    $form.append('<input name="img" value="'+ data.image +'"/>');
                    $form.submit();
                    $form.remove();
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }

                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.title = itemData.name;
                this.summary = '';
                this.body = itemData.body;
                this.featuredImage = itemData.image;
                this.type = 7;
                this.selected = false;

                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }

                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domItemRoot.find('.remove-wrapper').remove();
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }

            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.keyword = '';
            this.nextPageToken = false;

            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.init = function () {
            }
        }

        var RecipesListClass = function () {
            var domResultsRoot = domRoot.find('.recipes-grid.tt-grid-wrapper');

            var self = this;
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];

            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intSeries_client_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {

                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                }

                this.viewPost = function () {
                    var data = self.getFormatData();
                    var sery = series[domSelectSery.val()];
                    var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                    $form.append('<input name="action" value="view"/>');
                    $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                    $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                    $form.append('<input name="type" value="'+ data.type +'"/>');
                    $form.append('<input name="title" value="'+ data.title +'"/>');
                    $form.append('<input name="body" value="'+ data.body +'"/>');
                    $form.append('<input name="img" value="'+ data.image +'"/>');
                    $form.submit();
                    $form.remove();
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return ajaxAPiHandle.apiPost('Recipe3rd.php', {action: 'get', id: itemData.recipe_id}, true, 300).then(function (res) {
                        ajaxData.sets.strPost_body = res.data.body;
                        return ajaxAPiHandle.apiPost('Posts.php', ajaxData).then(function () {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                                return true;
                            };
                            return false;
                        });
                    });
                }

                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }
                    return ajaxAPiHandle.apiPost('Recipe3rd.php', {action: 'get', id: itemData.recipe_id}, true, 300).then(function (res) {
                        ajaxData.sets.strPost_body = res.data.body;
                        return ajaxAPiHandle.apiPost('Subscriptions.php', ajaxData).then(function () {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                                return true;
                            };
                            return false;
                        });
                    });
                }

                this.title = itemData.title;
                this.summary = '';
                this.body = itemData.source_url;
                this.featuredImage = itemData.image_url;
                this.type = 7;
                this.selected = false;

                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }

                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domItemRoot.find('.remove-wrapper').remove();
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }

            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.keyword = '';
            this.nextPageToken = false;

            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.init = function () {
            }
        }

        var IdeaboxListClass = function () {
            var domResultsRoot = domRoot.find('.ideabox-grid.tt-grid-wrapper');
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];
            var self = this;

            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        if (itemData.intIdeaBox_contractor_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {

                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                    domItemRoot.find('.remove-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intIdeaBox_contractor_ID == CLIENT_ID){
                            swal({
                                title: "Are you sure?",
                                text: "Do you want to delete this ideabox?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (willDelete) {
                                if (willDelete){
                                    self.deletePost();
                                }
                            });
                        }
                        else {
                            alert('Sorry. This is not created by you.')
                        }
                    });
                }

                this.deletePost = function () {
                    var ajaxData = {
                        action: 'delete',
                        id: self.id
                    };
                    return $.ajax({
                        url: API_ROOT_URL + '/Ideabox.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                itemHandles.forEach(function (itemHandle, i) {
                                    if (itemHandle.id == self.id){
                                        itemHandles.splice(i, 1);
                                        loadNewSet(itemHandles);
                                    }
                                })
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.viewPost = function () {
                    var $form = $('<form action="ideaboxcards" method="post" target="_blank" hidden></form>').appendTo('body');
                    $form.submit();
                    $form.remove();
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }

                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.id = itemData.ideabox_ID;
                this.title = itemData.strIdeaBox_title;
                this.summary = '';
                this.body = itemData.strIdeaBox_idea;
                this.featuredImage = itemData.strIdeaBox_image || DEFAULT_IMAGE;
                this.type = 7;
                this.selected = false;

                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }

                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }

            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.keyword = '';
            this.nextPageToken = false;
            this.init = function () {
            }
        }

        var postsListClass = function () {
            var domResultsRoot = domRoot.find('.posts-grid.tt-grid-wrapper');
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];
            var self = this;

            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intSeries_client_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {

                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                    domItemRoot.find('.remove-post').click(function () {
                        swal({
                            title: "Are you sure?",
                            text: "Do you want to delete this post?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then(function (willDelete) {
                            if (willDelete){
                                self.deletePost();
                                self.deleteSubscription();
                            }
                        });
                    });
                }

                this.viewPost = function () {
                    window.open(BASE_URL + '/view.php?id=' + itemData.post_ID + '&prevpage=' + CURRENT_PAGE);
                }

                this.deletePost = function () {
                    var ajaxData = {
                        action: 'delete',
                        id: self.id
                    };
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                itemHandles.forEach(function (itemHandle, i) {
                                    if (itemHandle.id == self.id){
                                    }
                                })
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }
                this.deleteSubscription = function () {
                    var ajaxData = {
                        action: 'delete_by_postId',
                        postId: self.id
                    };
                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                itemHandles.forEach(function (itemHandle, i) {
                                    if (itemHandle.id == self.id){
                                        itemHandles.splice(i, 1);
                                        loadNewSet(itemHandles);
                                    }
                                })
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }
                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }

                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.id = itemData.post_ID;
                this.title = itemData.strPost_title;
                this.summary = itemData.strPost_summary;
                this.body = itemData.strPost_body;
                this.featuredImage = itemData.strPost_featuredimage || DEFAULT_IMAGE;
                this.type = itemData.intPost_type;
                this.selected = false;

                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }

                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }
            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.keyword = '';
            this.nextPageToken = false;
            this.init = function () {
            }
        }

        var rssbPostsListClass = function () {

            var domResultsRoot = domRoot.find('.rssb-posts-grid.tt-grid-wrapper');
            var pageHistory = [];
            var itemHandles = [];
            var allItemHandles = [];
            var selectedItems = [];
            var self = this;
            var ItemClass = function (itemData) {

                var domItemRoot, domTitleWrp, domSelectSery;

                var isAdded = false;

                var self = this;

                var bindEvents = function () {
                    domItemRoot.find('.add-to-series-wrapper .add-post').click(function () {
                        var sery = series[domSelectSery.val()];
                        if (sery.intSeries_client_ID == CLIENT_ID){
                            self.addToPost(sery.series_ID).then(function (res) {
                            });
                        }
                        else {
                            self.addToSuscription(sery.purchased_ID, false);
                        }
                    });
                    domItemRoot.find('.view-post').click(function () {
                        self.viewPost();
                    });
                }

                this.viewPost = function () {
                    var data = self.getFormatData();
                    var sery = series[domSelectSery.val()];
                    var $form = $('<form action="viewpost" method="post" target="_blank" hidden></form>').appendTo('body');
                    $form.append('<input name="action" value="view"/>');
                    $form.append('<input name="prevPage" value="'+ CURRENT_PAGE +'"/>');
                    $form.append('<input name="seriesTitle" value="'+ sery.strSeries_title +'"/>');
                    $form.append('<input name="type" value="'+ data.type +'"/>');
                    $form.append('<input name="title" value="'+ data.title +'"/>');
                    $form.append('<input name="body" value="'+ data.body +'"/>');
                    $form.append('<input name="img" value="'+ data.image +'"/>');
                    $form.submit();
                    $form.remove();
                }

                this.addToPost = function (seryId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.postFormat()
                    };
                    ajaxData.sets.intPost_series_ID = seryId;
                    return $.ajax({
                        url: API_ROOT_URL + '/Posts.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.addToSuscription = function (purchasedId, postId) {
                    var ajaxData = {
                        action: 'add',
                        sets: self.subscriptionFormat()
                    };
                    ajaxData.sets.intClientSubscription_purchased_ID = purchasedId;
                    if (postId){
                        ajaxData.sets.intClientSubscription_post_ID = postId;
                    }

                    return $.ajax({
                        url: API_ROOT_URL + '/Subscriptions.php',
                        data: ajaxData,
                        success: function (res) {
                            if (res.status){
                                isAdded = true;
                                domItemRoot.addClass('added-post');
                            }
                        },
                        type: 'post',
                        dataType: 'json',
                    });
                }

                this.title = itemData.strRSSBlogPosts_title;
                this.summary = itemData.strRSSBlogPosts_description;
                this.body = itemData.strRSSBlogPosts_content;
                this.featuredImage = DEFAULT_IMAGE;
                this.type = 7;
                this.selected = false;

                this.html = function () {
                    var cloneDom = domRoot.find('.item-sample-wrapper').clone();
                    cloneDom.find('.img-wrapper img').attr('src', self.featuredImage);
                    cloneDom.find('.title-wrapper .item-title').html(self.title);
                    var tpHtml = cloneDom.html();
                    cloneDom.remove();
                    return tpHtml;
                }

                this.bindDom = function (domItem) {
                    domItemRoot = domItem;
                    domTitleWrp = domItemRoot.find('.title-wrapper');
                    domSelectSery = domItemRoot.find('.add-to-series-wrapper select');
                }
                this.selected = false;
                this.getFormatData = function () {
                    return {
                        title: self.title,
                        body: self.body,
                        summary: self.summary,
                        type: self.type,
                        image: self.featuredImage
                    }
                }
                this.subscriptionFormat = function () {
                    var data = self.getFormatData();

                    return {
                        strClientSubscription_title: data.title,
                        strClientSubscription_body: data.body,
                        strClientSubscription_image: data.image,
                        intClientSubscriptions_type: data.type
                    }
                }
                this.postFormat = function () {
                    var data = self.getFormatData();
                    return {
                        strPost_title: data.title,
                        strPost_body: data.body,
                        strPost_summary: data.summary,
                        intPost_type: data.type,
                        strPost_featuredimage: data.image
                    }
                }
                this.init = function () {
                    domItemRoot.find('.remove-wrapper').remove();
                    domTitleWrp.find('.item-title').readmore({
                        collapsedHeight: 47,
                        moreLink: '<span>Read More</span>',
                        lessLink: '<span>View Less</span>',
                        embedCSS: false,
                        beforeToggle: function () {
                        },
                        afterToggle: function () {
                            domTitleWrp.toggleClass('collapsed');
                        }
                    });
                    domTitleWrp.addClass('collapsed');
                    domSelectSery.select2();
                    bindEvents();
                }
            }
            var grid = domResultsRoot.get(0).querySelector( '.tt-grid' ),
                items = [].slice.call( grid.querySelectorAll( 'li' ) ),
                isAnimating = false;

            var loadNewSet = function(set) {
                if (isAnimating === true){
                    return ;
                }
                isAnimating = true;

                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );

                var newImages = set;
                items.forEach( function( el ) {
                    var itemChild = el.querySelector( 'a' );
                    // add class "tt-old" to the elements/images that are going to get removed
                    if( itemChild ) {
                        classie.add( itemChild, 'tt-old' );
                    }
                } );
                for (var i = 0; i < set.length - items.length; i++){
                    grid.innerHTML += '<li class="tt-empty"></li>';
                }
                grid = domResultsRoot.get(0).querySelector( '.tt-grid' );
                items = [].slice.call( grid.querySelectorAll( 'li' ) );
                // apply effect
                setTimeout( function() {

                    // append new elements
                    if(newImages){
                        [].forEach.call( newImages, function( el, i ) {
                            items[ i ].innerHTML += el.html();
                            el.bindDom($(items[ i ]).find('a:last-child'));
                            el.init();
                        } );
                    }

                    // add "effect" class to the grid
                    classie.add( grid, 'tt-effect-active' );

                    // wait that animations end
                    var onEndAnimFn = function() {
                        // remove old elements
                        items.forEach( function( el , i) {
                            // remove old elems
                            var old = el.querySelector( '.tt-old' );
                            if( old ) { el.removeChild( old ); }
                            // remove class "tt-empty" from the empty items
                            classie.remove( el, 'tt-empty' );
                            // now apply that same class to the items that got no children (special case)
                            if ( !$(el).children().length ) {
                                classie.add( el, 'tt-empty' );
                                $(el).remove();
                            }
                            else {
                                // newImages[i].bindDom($(el));
                            }
                        } );
                        // remove the "effect" class
                        classie.remove( grid, 'tt-effect-active' );
                        isAnimating = false;
                    };

                    if( support ) {
                        onAnimationEnd( items, items.length, onEndAnimFn );
                    }
                    else {
                        onEndAnimFn.call();
                    }
                }, 25 );
            }

            this.setItems = function (itemsData, newKeyword) {
                self.keyword = newKeyword;
                itemHandles = [];
                self.nextPageToken = itemsData.nextPageToken;
                itemsData.items.forEach(function (item) {
                    var itemHandle = new ItemClass(item);
                    itemHandles.push(itemHandle);
                    allItemHandles.push(itemHandle);
                });
                loadNewSet(itemHandles);
                pageHistory.push({origin: itemsData, handles: itemHandles, kwd: newKeyword});
            }
            this.show = function () {
                domResultsRoot.show();
            }
            this.hide = function () {
                domResultsRoot.hide();
            }
            this.getSelectedItems = function () {
                selectedItems = [];
                allItemHandles.forEach(function (itemHandle) {
                    if (itemHandle.selected){
                        selectedItems.push(itemHandle.getFormatData());
                    }
                });
                return selectedItems;
            }
            this.keyword = '';
            this.nextPageToken = false;
            this.init = function () {
            }
        }

        this.IAmDone = function () {
            flgOpend = false;
            $('html').removeClass('header-search-open');
        }
        this.viewPage = function(id){
            $('<form action="viewexperience" method="post" hidden><input name="id" value="'+ id +'"></form>').appendTo('body').submit();
        }
        this.showMore = function () {
            var items = [
                {name: 'youtube', token: youtubeListHandle.nextPageToken},
                {name: 'spreaker', token: podcastsListHandle.nextPageToken},
                {name: 'blogs', token: blogsListHandle.nextPageToken},
                {name: 'recipes', token: recipesListHandle.nextPageToken},
                {name: 'ideabox', token: ideaboxListHandle.nextPageToken},
                {name: 'posts',   token: postsListHandle.nextPageToken},
                {name: 'rssbPosts', token: rssbPostsListHandle.nextPageToken}
            ];
            searchPosts(domSearch.val(), 10, items).then(function (res) {
                youtubeListHandle.setItems(res.data.youtube, domSearch.val());
                podcastsListHandle.setItems(res.data.spreaker, domSearch.val());
                blogsListHandle.setItems(res.data.blogs, domSearch.val());
                recipesListHandle.setItems(res.data.recipes, domSearch.val());
                ideaboxListHandle.setItems(res.data.ideabox, domSearch.val());
                postsListHandle.setItems(res.data.posts, domSearch.val());
                rssbPostsListHandle.setItems(res.data.rssbPosts, domSearch.val());
            });
        }
        this.setSearchTerm = function (new_term) {
        }

        this.init = function () {
            youtubeListHandle = new YoutubeListClass();
            podcastsListHandle = new PodcastsListClass();
            blogsListHandle = new BlogsListClass();
            recipesListHandle = new RecipesListClass();
            ideaboxListHandle = new IdeaboxListClass();
            postsListHandle = new postsListClass();
            rssbPostsListHandle = new rssbPostsListClass();

            bindEvents();
            youtubeListHandle.init();
            podcastsListHandle.init();
            blogsListHandle.init();
            recipesListHandle.init();
            ideaboxListHandle.init();
            postsListHandle.init();
            rssbPostsListHandle.init();

            domRoot.find('[name="from"]').each(function () {
                var val = $(this).val();
                var checked = $(this).prop('checked');
                var handle = null;
                switch (val){
                    case 'youtube':
                        handle = youtubeListHandle;
                        break;
                    case 'spreaker':
                        handle = podcastsListHandle;
                        break;
                    case 'twingly':
                        handle = blogsListHandle;
                        break;
                    case 'recipes':
                        handle = recipesListHandle;
                        break;
                    case 'ideabox':
                        handle = ideaboxListHandle;
                        break;
                    case 'posts':
                        handle = postsListHandle;
                        break;
                    case 'rssbposts':
                        handle = rssbPostsListHandle;
                        break;
                    default:
                        handle = youtubeListHandle;
                        break;
                }
                checked ? handle.show() : handle.hide();
            });

            $.ajax({
                url: API_ROOT_URL + '/Series.php',
                data: {},
                success: function (res) {
                    res.data.forEach(function (sery) {
                        series[sery.series_ID] = sery;
                        var optHtml = '<option value="'+ sery.series_ID +'">'+ sery.strSeries_title +'</option>';
                        domRoot.find('.item-sample-wrapper .add-to-series-wrapper select').append(optHtml);
                    });
                },
                type: 'post',
                dataType: 'json',
            });
        }
    }

    var headerSearchHandle = new HeaderSearchClass();
    headerSearchHandle.init();

    var headerHandle = new HeaderClass();
    headerHandle.init();
})();