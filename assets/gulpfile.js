var apps = require('./page-css-script.json');

var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var del = require('del');
var runSequence = require('run-sequence');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var filesExist = require('files-exist');
const terser = require('gulp-terser');

var bundleCss = function (css_files, product_file_name) {
    gulp.src(filesExist(css_files))
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(concat(product_file_name))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('css/yevgeny'));
}
var bundleJs = function (js_files, product_file_name) {
    gulp.src(filesExist(js_files))
        .pipe(concat(product_file_name))
        .pipe(terser())
        .pipe(gulp.dest('js/yevgeny'));
}

var bundleCssWhere = function (css_files, where, product_file_name) {
    gulp.src(css_files)
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(concat(product_file_name))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest(where));
}
var bundleJsWhere = function (js_files, where, product_file_name) {
    gulp.src(js_files)
        .pipe(concat(product_file_name))
        .pipe(terser())
        .pipe(gulp.dest(where));
}

gulp.task('test', function () {
   var js_files = apps.test.js;
    bundleJs(js_files, 'test.js');
});

gulp.task('shared', function () {
    var css_files = apps.shared.css;
    var js_files = apps.shared.js;
    bundleCssWhere(css_files, 'shared/primary', 'app-global.css');
    bundleJsWhere(js_files, 'shared/primary', 'app-global.js');
});

gulp.task('shared_common', function () {
    var css_files = apps.app_common.css;
    var js_files = apps.app_common.js;
    bundleCssWhere(css_files, 'shared/primary', 'app-common.css');
    bundleJsWhere(js_files, 'shared/primary', 'app-common.js');
});

gulp.task('home_global', function () {
    var css_files = apps.home_global.css;
    var js_files = apps.home_global.js;
    bundleCssWhere(css_files, 'shared/primary', 'home-global.css');
    bundleJsWhere(js_files, 'shared/primary', 'home-global.js');
});

gulp.task('embedplatform', function () {
    var js_files = apps.embedPlatform.js;
    bundleJsWhere(js_files, 'js/yevgeny', 'embed-platform.js');
});



gulp.task('viewexperienceembed', function () {
    var css_files = apps.viewExperienceEmbed.css;
    var js_files = apps.viewExperienceEmbed.js;
    bundleCss(css_files, 'viewExperienceEmbed-global.css');
    bundleJs(js_files, 'viewExperienceEmbed-global.js');
});


gulp.task('calendar', function () {
    var css_files = apps.calendar.css;
    var js_files = apps.calendar.js;
    bundleCss(css_files, 'calendar-global.css');
    bundleJs(js_files, 'calendar-global.js');
});

gulp.task('user', function () {
    var css_files = apps.user.css;
    var js_files = apps.user.js;
    bundleCss(css_files, 'user-global.css');
    bundleJs(js_files, 'user-global.js');
});

gulp.task('goals', function () {
    var css_files = apps.goals.css;
    var js_files = apps.goals.js;
    bundleCss(css_files, 'goals-global.css');
    bundleJs(js_files, 'goals-global.js');
});
gulp.task('createyourexperience', function () {
    var css_files = apps.createYourExperience.css;
    var js_files = apps.createYourExperience.js;
    bundleCss(css_files, 'createYourExperience-global.css');
    bundleJs(js_files, 'createYourExperience-global.js');
});
gulp.task('createseries', function () {
    var css_files = apps.createSeries.css;
    var js_files = apps.createSeries.js;
    bundleCss(css_files, 'createSeries-global.css');
    bundleJs(js_files, 'createSeries-global.js');
});
gulp.task('viewexperience', function () {
    var css_files = apps.viewExperience.css;
    var js_files = apps.viewExperience.js;
    bundleCss(css_files, 'viewExperience-global.css');
    bundleJs(js_files, 'viewExperience-global.js');
});
gulp.task('exploreinterests', function () {
    var css_files = apps.exploreInterests.css;
    var js_files = apps.exploreInterests.js;
    bundleCss(css_files, 'exploreInterests-global.css');
    bundleJs(js_files, 'exploreInterests-global.js');
});

gulp.task('explore', function () {
    var css_files = apps.explore.css;
    var js_files = apps.explore.js;
    bundleCss(css_files, 'explore-global.css');
    bundleJs(js_files, 'explore-global.js');
});

gulp.task('editseries', function () {
    var css_files = apps.editSeries.css;
    var js_files = apps.editSeries.js;
    bundleCss(css_files, 'editSeries-global.css');
    bundleJs(js_files, 'editSeries-global.js');
});

gulp.task('view', function () {
    var css_files = apps.view.css;
    var js_files = apps.view.js;
    bundleCss(css_files, 'view-global.css');
    bundleJs(js_files, 'view-global.js');
});

gulp.task('viewembed', function () {
    var css_files = apps.viewembed.css;
    var js_files = apps.viewembed.js;
    bundleCss(css_files, 'viewEmbed-global.css');
    bundleJs(js_files, 'viewEmbed-global.js');
});

gulp.task('viewpost', function () {
    var css_files = apps.view.css;
    var js_files = apps.view.js;
    bundleCss(css_files, 'viewpost-global.css');
    bundleJs(js_files, 'viewpost-global.js');
});

gulp.task('invitation', function () {
    var css_files = apps.invitation.css;
    var js_files = apps.invitation.js;
    bundleCss(css_files, 'invitation-global.css');
    bundleJs(js_files, 'invitation-global.js');
});

gulp.task('login', function () {
    var css_files = apps.login.css;
    var js_files = apps.login.js;
    bundleCss(css_files, 'login-global.css');
    bundleJs(js_files, 'login-global.js');
});

gulp.task('signin', function () {
    var css_files = apps.signIn.css;
    var js_files = apps.signIn.js;
    bundleCss(css_files, 'signIn-global.css');
    bundleJs(js_files, 'signIn-global.js');
});

gulp.task('ideaboxcards', function () {
    var css_files = apps.ideaboxcards.css;
    var js_files = apps.ideaboxcards.js;
    bundleCss(css_files, 'ideaboxcards-global.css');
    bundleJs(js_files, 'ideaboxcards-global.js');
});

gulp.task('favorites', function () {
    var css_files = apps.favorites.css;
    var js_files = apps.favorites.js;
    bundleCss(css_files, 'favorites-global.css');
    bundleJs(js_files, 'favorites-global.js');
});

gulp.task('myseries', function () {
    var css_files = apps.myseries.css;
    var js_files = apps.myseries.js;
    bundleCss(css_files, 'myseries-global.css');
    bundleJs(js_files, 'myseries-global.js');
});

gulp.task('createrecurringevent', function () {
    var css_files = apps.createRecurringEvent.css;
    var js_files = apps.createRecurringEvent.js;
    bundleCss(css_files, 'createRecurringEvent-global.css');
    bundleJs(js_files, 'createRecurringEvent-global.js');
});

gulp.task('createsingleevent', function () {
    var css_files = apps.createSingleEvent.css;
    var js_files = apps.createSingleEvent.js;
    bundleCss(css_files, 'createSingleEvent-global.css');
    bundleJs(js_files, 'createSingleEvent-global.js');
});

gulp.task('editideabox', function () {
    var css_files = apps.editIdeabox.css;
    var js_files = apps.editIdeabox.js;
    bundleCss(css_files, 'editIdeabox-global.css');
    bundleJs(js_files, 'editIdeabox-global.js');
});

gulp.task('createevent', function () {
    var css_files = apps.createEvent.css;
    var js_files = apps.createEvent.js;
    bundleCss(css_files, 'createEvent-global.css');
    bundleJs(js_files, 'createEvent-global.js');
});

gulp.task('switchsubscription', function () {
    var css_files = apps.switchSubscription.css;
    var js_files = apps.switchSubscription.js;
    bundleCss(css_files, 'switchSubscription-global.css');
    bundleJs(js_files, 'switchSubscription-global.js');
});
gulp.task('index', function () {
    var css_files = apps.index.css;
    var js_files = apps.index.js;
    bundleCss(css_files, 'index-global.css');
    bundleJs(js_files, 'index-global.js');
});

gulp.task('previewseries', function () {
    var css_files = apps.previewSeries.css;
    var js_files = apps.previewSeries.js;
    bundleCss(css_files, 'previewSeries-global.css');
    bundleJs(js_files, 'previewSeries-global.js');
});

gulp.task('preview-series-embed', function () {
    var css_files = apps.previewSeriesEmbed.css;
    var js_files = apps.previewSeriesEmbed.js;
    bundleCss(css_files, 'previewseriesembed-global.css');
    bundleJs(js_files, 'previewseriesembed-global.js');
});

gulp.task('createseriescontent', function () {
    var css_files = apps.createSeriesContent.css;
    var js_files = apps.createSeriesContent.js;
    bundleCss(css_files, 'createSeriesContent-global.css');
    bundleJs(js_files, 'createSeriesContent-global.js');
});
gulp.task('bulkuploader', function () {
    var css_files = apps.bulkUploader.css;
    var js_files = apps.bulkUploader.js;
    bundleCss(css_files, 'bulkUploader-global.css');
    bundleJs(js_files, 'bulkUploader-global.js');
});
gulp.task('series-embed-gallery', function () {
    var css_files = apps.seriesEmbedGallery.css;
    var js_files = apps.seriesEmbedGallery.js;
    bundleCss(css_files, 'seriesEmbedGallery-global.css');
    bundleJs(js_files, 'seriesEmbedGallery-global.js');
});
gulp.task('series-embed-grid', function () {
    var css_files = apps.seriesEmbedGrid.css;
    var js_files = apps.seriesEmbedGrid.js;
    bundleCss(css_files, 'seriesEmbedGrid-global.css');
    bundleJs(js_files, 'seriesEmbedGrid-global.js');
});
gulp.task('home', function () {
    var css_files = apps.home.css;
    var js_files = apps.home.js;
    bundleCss(css_files, 'home-global.css');
    bundleJs(js_files, 'home-global.js');
});

gulp.task('browse', function () {
    var css_files = apps.browse.css;
    var js_files = apps.browse.js;
    bundleCss(css_files, 'browse-global.css');
    bundleJs(js_files, 'browse-global.js');
});
gulp.task('my_solutions', function () {
    var css_files = apps.my_solutions.css;
    var js_files = apps.my_solutions.js;
    bundleCss(css_files, 'my_solutions-global.css');
    bundleJs(js_files, 'my_solutions-global.js');
});
gulp.task('view_blog', function () {
    var css_files = apps.view_blog.css;
    var js_files = apps.view_blog.js;
    bundleCss(css_files, 'view_blog-global.css');
    bundleJs(js_files, 'view_blog-global.js');
});
gulp.task('series_embed_guided', function () {
    var css_files = apps.series_embed_guided.css;
    var js_files = apps.series_embed_guided.js;
    bundleCss(css_files, 'series_embed_guided-global.css');
    bundleJs(js_files, 'series_embed_guided-global.js');
});
gulp.task('preview_series', function () {
    var css_files = apps.preview_series.css;
    var js_files = apps.preview_series.js;
    bundleCss(css_files, 'preview_series-global.css');
    bundleJs(js_files, 'preview_series-global.js');
});
gulp.task('my_series', function () {
    var css_files = apps.my_series.css;
    var js_files = apps.my_series.js;
    bundleCss(css_files, 'my_series-global.css');
    bundleJs(js_files, 'my_series-global.js');
});

gulp.task('create_series', function () {
    var css_files = apps.create_series.css;
    var js_files = apps.create_series.js;
    bundleCss(css_files, 'create_series-global.css');
    bundleJs(js_files, 'create_series-global.js');
});

gulp.task('add_posts', function () {
    var css_files = apps.add_posts.css;
    var js_files = apps.add_posts.js;
    bundleCss(css_files, 'add_posts-global.css');
    bundleJs(js_files, 'add_posts-global.js');
});

gulp.task('series_invitation_accepted', function () {
    var css_files = apps.series_invitation_accepted.css;
    var js_files = apps.series_invitation_accepted.js;
    bundleCss(css_files, 'series_invitation_accepted-global.css');
    bundleJs(js_files, 'series_invitation_accepted-global.js');
});

gulp.task('edit_series', function () {
    var css_files = apps.edit_series.css;
    var js_files = apps.edit_series.js;
    bundleCss(css_files, 'edit_series-global.css');
    bundleJs(js_files, 'edit_series-global.js');
});

gulp.task('create_post', function () {
    var css_files = apps.create_post.css;
    var js_files = apps.create_post.js;
    bundleCss(css_files, 'create_post-global.css');
    bundleJs(js_files, 'create_post-global.js');
});

gulp.task('my_favorites', function () {
    var css_files = apps.my_favorites.css;
    var js_files = apps.my_favorites.js;
    bundleCss(css_files, 'my_favorites-global.css');
    bundleJs(js_files, 'my_favorites-global.js');
});
gulp.task('my_notes', function () {
    var css_files = apps.my_notes.css;
    var js_files = apps.my_notes.js;
    bundleCss(css_files, 'my_notes-global.css');
    bundleJs(js_files, 'my_notes-global.js');
});
gulp.task('shared_posts', function () {
    var css_files = apps.shared_posts.css;
    var js_files = apps.shared_posts.js;
    bundleCss(css_files, 'shared-posts-global.css');
    bundleJs(js_files, 'shared-posts-global.js');
});
gulp.task('user_profile', function () {
    var css_files = apps.user_profile.css;
    var js_files = apps.user_profile.js;
    bundleCss(css_files, 'user_profile-global.css');
    bundleJs(js_files, 'user_profile-global.js');
});
var pages = [
    'shared',
    'calendar',
    'user',
    'goals',
    'createyourexperience',
    'createseries',
    'viewexperience',
    'exploreinterests',
    'explore',
    'editseries',
    'view',
    'viewpost',
    'invitation',
    'login',
    'ideaboxcards',
    'favorites',
    'myseries',
    'createevent',
    'createrecurringevent',
    'createsingleevent',
    'editideabox',
    'switchsubscription',
    'index',
    'previewseries',
    'createseriescontent',
    'viewexperienceembed',
    'bulkuploader'
];
gulp.task('all', pages, function () {});
