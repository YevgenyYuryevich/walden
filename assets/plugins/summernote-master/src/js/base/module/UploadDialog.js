import $ from 'jquery';
import env from '../core/env';
import key from '../core/key';

export default class UploadDialog {
  constructor(context) {
    this.context = context;
    this.ui = $.summernote.ui;
    this.$body = $(document.body);
    this.$editor = context.layoutInfo.editor;
    this.options = context.options;
    this.lang = this.options.langInfo;
    this.urlUploaded = false;
  }

  initialize() {
    const $container = this.options.dialogsInBody ? this.$body : this.$editor;

    let imageLimitation = '';
    if (this.options.maximumImageFileSize) {
      const unit = Math.floor(Math.log(this.options.maximumImageFileSize) / Math.log(1024));
      const readableSize = (this.options.maximumImageFileSize / Math.pow(1024, unit)).toFixed(2) * 1 +
        ' ' + ' KMGTP'[unit] + 'B';
      imageLimitation = `<small>${this.lang.image.maximumFileSize + ' : ' + readableSize}</small>`;
    }

    const body = [
      '<div class="form-group note-form-group note-group-select-from-files">',
      '<label class="note-form-label">' + this.lang.upload.selectFromFiles + '</label>',
      '<input class="note-file-input note-form-control note-input" ',
      ' type="file" name="file" />',
      imageLimitation,
      '</div>',
      '<div class="progress" style="visibility: hidden;" id="upload-progress">',
      '<div class="progress-bar" role="progressbar" style="min-width: 2em; width: 0%;">0%</div>',
      '</div>',
      '<div class="form-group note-group-file-text" style="overflow:auto;">',
      '<label class="note-form-label">' + this.lang.upload.text + '</label>',
      '<input class="note-file-text form-control note-form-control note-input ',
      ' col-md-12" type="text" />',
      '</div>'
    ].join('');
    const buttonClass = 'btn btn-primary note-btn note-btn-primary note-file-btn';
    const footer = `<button type="submit" href="#" class="${buttonClass}" disabled>${this.lang.upload.insertLink}</button>`;

    this.$dialog = this.ui.dialog({
      title: this.lang.upload.insert,
      fade: this.options.dialogsFade,
      body: body,
      footer: footer
    }).render().appendTo($container);
  }

  destroy() {
    this.ui.hideDialog(this.$dialog);
    this.$dialog.remove();
  }

  bindEnterKey($input, $btn) {
    $input.on('keypress', (event) => {
      if (event.keyCode === key.code.ENTER) {
        event.preventDefault();
        $btn.trigger('click');
      }
    });
  }

  show() {
    this.context.invoke('editor.saveRange');
    this.showUploadDialog().then((linkInfo) => {
      // [workaround] hide dialog before restore range for IE range focus
      this.ui.hideDialog(this.$dialog);
      this.context.invoke('editor.restoreRange');
      this.context.invoke('editor.createLink', linkInfo);
    }).fail(() => {
      this.context.invoke('editor.restoreRange');
    });
  }

  /**
   * show image dialog
   *
   * @param {jQuery} $dialog
   * @return {Promise}
   */
  showUploadDialog() {
    return $.Deferred((deferred) => {
      const $fileInput = this.$dialog.find('.note-file-input');
      const $fileText = this.$dialog.find('.note-file-text');
      const $addLinkBtn = this.$dialog.find('.note-file-btn');
      this.ui.onDialogShown(this.$dialog, () => {
        this.context.triggerEvent('dialog.shown');

        // Cloning imageInput to clear element.
        $fileInput.replaceWith($fileInput.clone().on('change', (event) => {
          if (event.target.files.length) {
            const file = event.target.files[0];
            this.uploadFile(file).then(res => {
              $fileText.val(file.name);
              this.urlUploaded = res.data.abs_url;
              this.ui.toggleBtn($addLinkBtn, file.name && this.urlUploaded);
            });
          }
        }).val(''));

        $addLinkBtn.click((event) => {
          event.preventDefault();

          deferred.resolve({
            text: $fileText.val(),
            url: this.urlUploaded
          });
        });

        $fileText.on('keyup paste', () => {
          const text = $fileText.val();
          this.ui.toggleBtn($addLinkBtn, text && this.urlUploaded);
        }).val('');

        if (!env.isSupportTouch) {
          $fileText.trigger('focus');
        }
        this.bindEnterKey($fileText, $addLinkBtn);
      });
      const $progress = this.$dialog.find('#upload-progress');
      const $progressbar = $progress.find('.progress-bar');
      this.ui.onDialogHidden(this.$dialog, () => {
        $fileInput.off('change');
        $fileText.off('keyup paste keypress');
        $addLinkBtn.off('click');
        $progress.css('visibility', 'hidden');
        $progressbar.css('width', 0 + '%');
        if (deferred.state() === 'pending') {
          deferred.reject();
        }
      });

      this.ui.showDialog(this.$dialog);
    });
  }
  uploadFile(file) {
    if (file) {
      const formData = new FormData();
      formData.append('file', file);
      formData.append('sets', JSON.stringify({
        where: 'assets/media/other'
      }));
      const $progress = this.$dialog.find('#upload-progress');
      const $progressbar = $progress.find('.progress-bar');
      $progress.css('visibility', 'visible');
      var progressCallBack = (e) => {
        let percent = Math.round(100 * e.loaded / e.total);
        console.log(e.loaded, e.total, percent);
        percent = percent < 90 ? percent : 90;
        $progressbar.css('width', percent + '%');
        $progressbar.text(percent + '%');
      };
      return $.ajax({
        url: '/yevgeny/api/Uploads.php',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        xhr: () => {
          var myXhr = $.ajaxSettings.xhr();
          if (myXhr.upload) {
            // For handling the progress of the upload
            myXhr.upload.addEventListener('progress', progressCallBack, false);
          }
          return myXhr;
        }
      }).then(res => {
        $progressbar.css('width', 100 + '%');
        $progressbar.text(100 + '%');
        return res;
      });
    } else {
      throw new Error('Invalid Media');
    }
  }
}
