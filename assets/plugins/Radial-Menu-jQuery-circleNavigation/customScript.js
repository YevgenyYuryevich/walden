// jQuery-circleNav - v1.0

function whichTransitionEvent(){
    var t,
        el = document.createElement("fakeelement");

    var transitions = {
        "transition"      : "transitionend",
        "OTransition"     : "oTransitionEnd",
        "MozTransition"   : "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
    }

    for (t in transitions){
        if (el.style[t] !== undefined){
            return transitions[t];
        }
    }
}

(function ($, window, document, undefined) {
    // Plugin definition
    $.fn.circleNav = function (domTarget, options) {

        // Default settings
        var settings = $.extend({}, $.fn.circleNav.settings, options);

        // Plugin code
        return this.each( function () {

            var $container = typeof domTarget == 'string' ? $(domTarget) : domTarget; /* Navigation container: #circle-nav-wrapper */
            var $toggle = $('.circle-nav-toggle', $container);
            var $panel = $('.circle-nav-panel', $container);
            var $menu = $('.circle-nav-menu', $container);
            // breakpointChk();

            // Overlay
            if (settings.overlay) {
                if ($('.circle-nav-overlay').length == 0) {
                    $('body').append("<div class='circle-nav-overlay'></div>");
                    $(".circle-nav-overlay").css({
                        "top": "0",
                        "right": "0",
                        "bottom": "0",
                        "left": "0",
                        "position": "fixed",
                        "background-color": settings.overlayColor,
                        "opacity": settings.overlayOpacity,
                        "z-index": "-1",
                        "display": "none"
                    });
                }
            }

            // Toggle click event
            $(this).on('click', function () {
                $container.css('display', 'block');
                setTimeout(function () {
                    $container.stop().toggleClass('circle-nav-open');
                    $toggle.stop().toggleClass('circle-nav-open');
                    $panel.stop().toggleClass('circle-nav-open');
                    $menu.stop().toggleClass('circle-nav-open');
                    $('.circle-nav-overlay').fadeToggle();

                    if ( $('body').css("overflow") ) {
                        $('body, html').css("overflow", "");
                    } else {
                        $('body, html').css("overflow", "hidden");
                    }
                }, 10);
            });
            if (settings.isSelf){
            }
            else {
                $container.find('.circle-nav-toggle').click(function () {
                    $container.one(whichTransitionEvent(), function () {
                        $container.css('display', 'none');
                    });
                    $container.stop().toggleClass('circle-nav-open');
                    $toggle.stop().toggleClass('circle-nav-open');
                    $panel.stop().toggleClass('circle-nav-open');
                    $menu.stop().toggleClass('circle-nav-open');
                    $('.circle-nav-overlay').fadeToggle();

                    if ( $('body').css("overflow") ) {
                        $('body, html').css("overflow", "");
                    } else {
                        $('body, html').css("overflow", "hidden");
                    }
                });
            }

            $('.circle-nav-overlay').click(function () {
                $container.stop().toggleClass('circle-nav-open');
                $toggle.stop().toggleClass('circle-nav-open');
                $panel.stop().toggleClass('circle-nav-open');
                $menu.stop().toggleClass('circle-nav-open');
                $('.circle-nav-overlay').fadeToggle();

                if ( $('body').css("overflow") ) {
                    $('body, html').css("overflow", "");
                } else {
                    $('body, html').css("overflow", "hidden");
                }
            });

            // OnResize Events
            // var resizeTimer;
            // $(window).resize(function () {
            //     clearTimeout(resizeTimer);
            //     resizeTimer = setTimeout(function () {
            //         breakpointChk();
            //     }, 500);
            // });


            /** Functions **/

            // Checks screen size
            // function breakpointChk() {
            //     if ($toggle.is(":visible")) {
            //         isBreakpoint = true;
            //     } else {
            //         isBreakpoint = false;
            //     }
            // }

        });
    };

    // Default settings
    $.fn.circleNav.settings = {
        overlay: true,
        overlayColor: "#fff",
        overlayOpacity: ".7",
        isSelf: false
    };

})(jQuery, window, document);