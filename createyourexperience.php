<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 30.05.2018
 * Time: 08:15
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class CreateYourExperience extends \Core\Controller_core
{
    private $optionModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Options_m');
        $this->optionModel = new \Models\api\Options_m();
    }
    public function index() {
        $usersCanEditSeries = $this->optionModel->get(['strOption_name' => 'users_can_edit_series']);
        $usersCanEditSeries = $usersCanEditSeries ? (int) $usersCanEditSeries['strOption_value'] : false;
        $this->load->view('CreateYourExperience_v', ['usersCanEditSeries' => $usersCanEditSeries]);
    }
}
$handle = new CreateYourExperience();
$handle->index();