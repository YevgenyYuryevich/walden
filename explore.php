<?php

require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/Explore_m.php';
require_once 'guardian/class/userauth.class.php';

use Core\Controller_core;
use Models\Explore_m;

class Explore extends Controller_core
{
    public function __construct()
    {
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = 0;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->model = new Explore_m();
    }
    public function index(){
        $categories = $this->searchCategoriesByKeyword('');
        $isFirstVisit = isset($_GET['first_visit']) && $_GET['first_visit'] == '1' ? true : false;
        if ($isFirstVisit && $this->isLoggedIn){
            $row = $this->model->getVisitedPageHistory('explore');
            if ($row){
                $isFirstVisit = false;
            }
            else {
                $this->model->insertVisitedPageHistory('explore');
            }
        }
        require_once 'yevgeny/views/Explore_v.php';
    }
    public function searchCategoriesByKeyword($keyword){
        $searchedCategories = [];
        $categories = $this->model->getCategories();
        foreach ($categories as $category){
            $searchedSeries = $this->searchSeriesByKeywordAndCateId($keyword, $category['category_ID']);
            if ($searchedSeries){
                $category['series'] = $searchedSeries;
                array_push($searchedCategories, $category);
            }
        }
        return $searchedCategories;
    }
    public function searchSeriesByKeywordAndCateId($keyword, $categoryId){
        $searchedSeries = $this->model->searchSeriesByKeywordAndCateId($keyword, $categoryId);
        $unSearchedSeries = $this->model->unSearchSeriesByKeywordAndCateId($keyword, $categoryId);
        foreach ($unSearchedSeries as $unSearchedSery){
            $searchedPosts = $this->model->searchPostsByKeywordAndSrId($keyword, $unSearchedSery['series_ID']);
            if ($searchedPosts){ array_push($searchedSeries, $unSearchedSery); }
        }
        return $searchedSeries;
    }
    public function test(){
        $this->model->test();
    }
}

$exploreHandle = new Explore();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'search_categories_by_keyword':
            $keyword = $_POST['keyword'];
            $searchedCategories = $exploreHandle->searchCategoriesByKeyword($keyword);
            echo json_encode(['status' => true, 'data' => $searchedCategories]);
            break;
        default:
            $exploreHandle->index();
            break;
    }
}
else{
    $exploreHandle->index();
//    $exploreHandle->test();
}