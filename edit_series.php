<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 23.11.2018
 * Time: 11:39
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class Edit_Series extends \Core\Controller_core
{

    private $seriesHandle;
    private $model;
    private $categoryModel;
    private $optionModel;
    private $seriesInvitationsModel;
    private $seriesOwnersModel;
    private $userHandle;

    public function __construct()
    {
        parent::__construct();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();

        $this->model = $this->seriesHandle->model;


        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();


        $this->load->model('api_m/Options_m');
        $this->optionModel = new \Models\api\Options_m();

        $this->load->model('api_m/Series_Invitations_m');
        $this->seriesInvitationsModel = new \Models\api\Series_Invitations_m();

        $this->load->model('api_m/Series_Owners_m');
        $this->seriesOwnersModel = new \Models\api\Series_Owners_m();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();
    }
    public function index() {

        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $tab = isset($_POST['tab']) ? $_POST['tab'] : (isset($_GET['tab']) ? $_GET['tab'] : 'series');

        if (!$id) {
            echo 'no id';
            die();
        }

        $series = $this->model->get($id);
        $series = \Helpers\utf8Encode($series);
        if (!$this->seriesHandle->getRole($series) && (int)$_SESSION['client_ID'] !== 4) {
            echo "you don't have permission for this series.";
            die();
        }

        $stripeClientRow = $this->optionModel->get(['strOption_name' => 'stripe_client_id']);
        $stripeClientId = $stripeClientRow ? $stripeClientRow['strOption_value'] : '';
        $categories = $this->categoryModel->getRows();

        $seriesOwners = $this->seriesOwnersModel->getRows(['intSeriesOwner_series_ID' => $id]);

        foreach ($seriesOwners as &$seriesOwner) {
            $u = $this->userHandle->get($seriesOwner['intSeriesOwner_user_ID']);
            $seriesOwner['user'] = $u;
            $seriesOwner['strSeriesInvitation_status'] = 'accept';
        }
        $creator = $this->userHandle->get($series['intSeries_client_ID']);
        $creatorOwner = [
            'strSeriesInvitation_status' => 'accept',
            'intSeriesOwner_role' => 1,
            'user' => $creator,
        ];
        $seriesOwners = array_merge([$creatorOwner], $seriesOwners);

        $invitations = $this->seriesInvitationsModel->getRows(['intSeries_ID' => $id, 'strSeriesInvitation_status' => 'waiting']);

        foreach ($invitations as &$invitation) {
            $u = $this->userHandle->get(['email' => $invitation['strInvitation_email']]);
            $invitation['user'] = $u;
            $invitation['intSeriesOwner_role'] = 2;
        }
        $isSeriesMine = $series['intSeries_client_ID'] == $_SESSION['client_ID'] || true;
        $this->load->view('Edit_Series_v', ['categories' => $categories, 'stripeClientId' => $stripeClientId, 'series' => $series, 'seriesOwners' => $seriesOwners, 'invitations' => $invitations, 'isSeriesMine' => $isSeriesMine, 'tab' => $tab]);
    }
}


$handle = new Edit_Series();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}