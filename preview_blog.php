<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.12.2018
 * Time: 20:49
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Preview_Blog extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
    }
    public function index(){
        $data = [
            'type' => isset($_POST['type']) ? $_POST['type'] : 2,
            'img' => isset($_POST['img']) ? $_POST['img'] : false,
            'title' => isset($_POST['title']) ? $_POST['title'] : 'untitled',
            'body' => isset($_POST['body']) ? $_POST['body'] : 'no body',
            'duration' => isset($_POST['duration']) ? $_POST['duration'] : 'waiting...',
        ];
        $this->load->view('Preview_Blog_v', ['postData' => $data]);
    }
}
$hdl = new Preview_Blog();
$hdl->index();