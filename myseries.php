<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 26.02.2018
 * Time: 18:27
 */
require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/MySeries_m.php';
require_once 'yevgeny/models/Favorites_m.php';
require_once 'yevgeny/models/Ideaboxcards_m.php';
require_once 'yevgeny/models/api_m/Subscriptions_m.php';
require_once 'yevgeny/models/api_m/Purchased_m.php';

use Core\Controller_core;
use Models\MySeries_m;
use Models\Favorites_m;
use Models\Ideaboxcards_m;
use Models\api\Subscriptions_m;
use Models\api\Purchased_m;

class MySeries extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new MySeries_m();
        $this->favoriteModel = new Favorites_m();
        $this->ideaboxModel = new Ideaboxcards_m();
        $this->load->controller('Subscriptions_c');
        $this->subsHandle = new \Controllers\Subscriptions_c();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
        $this->subsModel = new Subscriptions_m();
        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();
        $this->purchasedModel = new Purchased_m();

        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();
    }
    public function index() {
        $mySeries = $this->seriesModel->getSeries(['intSeries_client_ID' => $_SESSION['client_ID']]);

        foreach ($mySeries as & $mySery) {
            $mySery['posts'] = $this->postsHandle->getSerialItems(['intPost_series_ID' => $mySery['series_ID'], 'intPost_parent' => 0], 'post_ID, intPost_series_ID, strPost_title, strPost_featuredimage, intPost_type, intPost_order, intPost_parent, strPost_nodeType', ['intPost_order', 'post_ID'], 50);
        }
        $mySeries = \Helpers\utf8Encode($mySeries);
        $this->load->view('MySeries_v', ['series' => $mySeries]);
    }
    public function subscribeSeries($purchasedId){
        $purchased = $this->purchasedModel->get($purchasedId);
        $seriesId = $purchased['intPurchased_series_ID'];
        $unSubsPosts = $this->model->getUnSubsPosts($seriesId, $purchasedId);

        foreach ($unSubsPosts as $unSubsPost){
            $sets = [
                'intClientSubscription_purchased_ID' => $purchasedId,
                'intClientSubscription_post_ID' => $unSubsPost['post_ID'],
            ];
            $this->subsModel->insert($sets);
        }
    }
    public function ajax_updateWithSubs(){
        $id = $_POST['id'];
        $withId = $_POST['withId'];
        $subs = $this->subsModel->get($withId);
        $subs = $this->model->formatSubscription($subs);

        $sets = [
            'strClientSubscription_body' => $subs['strClientSubscription_body'],
            'strClientSubscription_title' => $subs['strClientSubscription_title'],
            'strClientSubscription_image' => $subs['strClientSubscription_image'],
            'intClientSubscriptions_type' => $subs['intClientSubscriptions_type'],
        ];
        if (!is_null($subs['intClientSubscription_post_ID'])){
            $sets['intClientSubscription_post_ID'] = $subs['intClientSubscription_post_ID'];
        }
        $this->subsModel->update($id, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateWithIdea(){
        $ideaId = $_POST['ideaId'];
        $id = $_POST['id'];
        $ideabox = $this->ideaboxModel->getIdeabox($ideaId);
        $sets = [
            'strClientSubscription_body' => $ideabox['strIdeaBox_idea'],
            'strClientSubscription_title' => $ideabox['strIdeaBox_title'],
            'strClientSubscription_image' => $ideabox['strIdeaBox_image'],
            'intClientSubscriptions_type' => 7,
        ];
        $this->subsModel->update($id, $sets);
        echo json_encode(['status' => true]);
        die();
    }

    public function ajax_updateOrders(){
        $updateSets = $_POST['updateSets'];
        foreach ($updateSets as $updateSet){
            $id = $updateSet['id'];
            $sets = ['intClientSubscriptions_order' => $updateSet['order']];
            $this->subsModel->update($id, $sets);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function test(){
        $rlt = $this->model->test();
        echo '<pre>';
        var_dump($rlt);
    }
}

$handle = new MySeries();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'update_orders':
            $handle->ajax_updateOrders();
            break;
        case 'update_with_subs':
            $handle->ajax_updateWithSubs();
            break;
        case 'update_with_idea':
            $handle->ajax_updateWithIdea();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
//    $handle->test();
}