<?php

require_once("guardian/access.php");

include_once("config.php");
include_once("dbconnect.php");

$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',  $_SERVER['SERVER_NAME'] );
$action_url = $base_url . '/yevgeny/goals_page_action.php';
$user_action_url = $base_url . '/yevgeny/userpage_action.php';
$current_user = $_SESSION['client_ID'];
$default_ideabox_image = 'assets/images/beautifulideas.jpg';
$default_event_image = 'assets/images/slide_1.jpg';

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="My Growth and Experiences on thegreyshirt">
    <meta name="Description" content="View the things that you have experienced and accomplished on thegreyshirt">
    <title>Home</title>

    <link rel="stylesheet" href="assets/css/yevgeny/goals-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/yevgeny/goals-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-goals">
    <?php require_once 'yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    My Goals
                </div>
            </div>
            <header class="content-header">
                <div class="multi-select-wrapper">
                    <span>View Multiple</span>
                    <div class="dropdown">
                        <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select To View <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li data-what = 'event'><a href="javascript:;"><input id="show-events" type="checkbox" checked/><label for="show-events">Show Events</label></a></li>
                            <li data-what = 'ideabox'><a href="javascript:;"><input id="show-ideabox" type="checkbox" checked/><label for="show-ideabox">Show Ideabox</label></a></li>
                            <li data-what = 'goal'><a href="javascript:;"><input id="show-goals" type="checkbox" checked/><label for="show-events">Show Goals</label></a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <ul class="timeline timeline-centered">
                    <li class="timeline-item sample" hidden>
                        <div class="timeline-info">
                            <span>March 12, 2016</span>
                        </div>
                        <div class="timeline-marker">
                            <button class="btn-success btn btn-block"><i class="glyphicon glyphicon-ok"></i></button>
                        </div>
                        <div class="timeline-content">
                            <h3 class="timeline-title">Event Title</h3>
                            <div class="timeline-description">
                                Nullam vel sem. Nullam vel sem. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Donec vitae sapien ut libero venenatis faucibus. ullam dictum felis
                                eu pede mollis pretium. Pellentesque ut neque.
                            </div>
                            <div class="timeline-link-wrapper">
                                <a href="javascript:;" class="timeline-link">Link</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </main>
        </div>
    </div>
    <?php require_once 'yevgeny/views/_templates/_footer.php';?>
</div>

<div class="edit-event-lightbox my-lightbox sample">
    <div class="edit-event-lightbox-inner-wrapper my-lightbox-inner-wrapper">
        <div class="edit-event-lightbox-inner my-lightbox-inner">
            <div class="event-title-wrapper editable-wrapper">
                <span class="event-title editable-content"></span>
                <span class="action-wrapper">
                    <a href="javascript:;" class="edit-entry"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="javascript:;" class="save-entry"><i class="glyphicon glyphicon-check"></i></a>
                    <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                </span>
            </div>
            <div class="event-detail-wrapper">
                <img class="event-main-image" src="assets/images/slide_1.jpg" />
                <div class="description-wrapper editable-wrapper">
                    <div class="top-mark">The Event <span>“</span></div>
                    <div class="event-description editable-content">event description</div>
                    <span class="action-wrapper">
                        <a href="javascript:;" class="edit-entry"><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="javascript:;" class="save-entry"><i class="glyphicon glyphicon-check"></i></a>
                        <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                    </span>
                </div>
                <div class="the-details">
                    <h2>The Details</h2>
                    <div class="location-wrapper editable-wrapper">
                        <h3>Location</h3>
                        <div class="editable-content"></div>
                        <span class="action-wrapper">
                            <a href="javascript:;" class="edit-entry"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="javascript:;" class="save-entry"><i class="glyphicon glyphicon-check"></i></a>
                            <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                        </span>
                    </div>
                    <div class="date-time-wrapper editable-wrapper">
                        <h3>Date and Time</h3>
                        <div>
                            <div class="for-display"><span class="start-date"></span>&nbsp;&nbsp;<span class="start-time"></span></div>
                            <div class="for-edit"><input class="start-date"> <input class="start-time"></div>
                            <span class="action-wrapper">
                                <a href="javascript:;" class="edit-entry"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="javascript:;" class="save-entry"><i class="glyphicon glyphicon-check"></i></a>
                                <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="event-images-container">
                <section class="tt-grid-wrapper">
                    <span class="previous-page"><img src="assets/images/global-icons/previous.png" /></span>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                    <span class="next-page"><img src="assets/images/global-icons/next.png" /></span>
                    <div class="clearfix"></div>
                </section>
            </div>
            <div class="actions-wrapper">
                <button class="btn btn-success apply-to-instance">Apply</button>
                <button class="btn btn-default cancel">Cancel</button>
            </div>
            <button class="close">X</button>
        </div>
    </div>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    const ACTION_URL = '<?php echo $action_url;?>';
    const USER_ACTION_URL = '<?php echo $user_action_url;?>';
    const CURRENT_USER = '<?php $current_user;?>';
    const DEFAULT_IDEABOX_IMAGE = "<?php echo $default_ideabox_image;?>";
    const DEFAULT_EVENT_IMAGE = "<?php echo $default_event_image;?>";
    var initial_selected_sery = <?php echo isset($_GET['series_id']) ? $_GET['series_id'] : 0;?>;
</script>

<script src="assets/js/yevgeny/goals-global.js?version=<?php echo time();?>"></script>
<script src="assets/js/yevgeny/goals-page.js?version=<?php echo time();?>"></script>

</body>
</html>