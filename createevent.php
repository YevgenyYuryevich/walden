<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 03:40
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

use Core\Controller_core;

class CreateEvent extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        $this->load->view('CreateEvent_v');
    }
}
$handle = new CreateEvent();

$handle->index();