<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 24.12.2018
 * Time: 18:54
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Add_Posts extends \Core\Controller_core
{
    private $isLoggedIn;
    private $seriesModel;

    public function __construct()
    {
        parent::__construct();

        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
    }
    public function index() {
        $id = $_GET['id'];
        $series = $this->seriesModel->get($id);
        $this->load->view('Add_Posts_v', ['series' => $series]);
    }
}
$handle = new Add_Posts();
$handle->index();
