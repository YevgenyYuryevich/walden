<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.11.2018
 * Time: 08:23
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class Create_Post extends \Core\Controller_core
{
    private $seriesModel;
    private $formFieldModel;
    private $formFieldAnswerModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
        $this->load->model('api_m/FormField_m');
        $this->load->model('api_m/FormFieldAnswer_m');
        $this->formFieldModel = new \Models\api\FormField_m();
        $this->formFieldAnswerModel = new \Models\api\FormFieldAnswer_m();
    }
    public function index() {
        $id = isset($_POST['series_id']) ? $_POST['series_id'] : (isset($_GET['series_id']) ? $_GET['series_id'] : false);
        $create_type = isset($_POST['create_type']) ? $_POST['create_type'] : (isset($_GET['create_type']) ? $_GET['create_type'] : 'from_scratch');
        $series = $this->seriesModel->get($id);

        $formFields = $this->formFieldModel->getRows(['intFormField_series_ID' => $id]);
        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);

        $this->load->view('Create_Post_v', ['series' => $series, 'create_type' => $create_type, 'formFields' => $formFields, 'formFieldAnswers' => $formFieldAnswers]);
    }
}


$handle = new Create_Post();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}