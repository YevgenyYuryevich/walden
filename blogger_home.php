<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 19.09.2018
 * Time: 15:30
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Blogger_Home extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
    }
    public function index() {
        $this->load->view('Blogger_Home_v');
    }
}
$handle = new Blogger_Home();
$handle->index();