<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 30.03.2018
 * Time: 23:27
 */

require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/Explore_m.php';
require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/models/api_m/Search_m.php';
require_once 'yevgeny/models/api_m/Purchased_m.php';
require_once 'yevgeny/models/api_m/ClientMeta_m.php';

use Core\Controller_core;
use Models\Explore_m;
use Models\api\{Search_m, Purchased_m, ClientMeta_m};
use function Helpers\utf8Encode;

class Enrich extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
            $_SESSION['affiliate_id'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $_SESSION['affiliate_id'] = $_SESSION['guardian']['affiliate_id'];
            $this->isLoggedIn = true;
        }
        $this->model = new Explore_m();
        $this->searchModel = new Search_m();
        $this->purchasedModel = new Purchased_m();
        $this->clientMetaModel = new ClientMeta_m();
        $this->load->model('api_m/User_m');
        $this->userModel = new \Models\api\User_m();
        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();
    }
    public function index(){
        $categories = $this->searchCategoriesByKeyword('');
        $isFirstVisit = isset($_GET['first_visit']) && $_GET['first_visit'] == '1' ? true : false;

        if ($isFirstVisit && $this->isLoggedIn){
            $row = $this->model->getVisitedPageHistory('explore');
            if ($row){
                $isFirstVisit = false;
            }
            else {
                $this->model->insertVisitedPageHistory('explore');
            }
        }
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';
        $this->load->view('Enrich_v', ['categories' => $categories, 'isFirstVisit' => $isFirstVisit, 'stripeApi_pKey' => $stripeApi_pKey]);
    }
    public function searchCategoriesByKeyword($keyword){
        $searchedCategories = [];
        $categories = $this->model->getCategories();
        foreach ($categories as $category){
            $searchedSeries = $this->searchModel->searchSeries($keyword, 50, 0, ['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 1, 'boolSeries_isPublic' => 1, 'boolSeries_approved' => 1]);

            foreach ($searchedSeries as & $searchedSery){
                $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $searchedSery['series_ID']]);
                $searchedSery['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;
                $owner = $this->userModel->get($searchedSery['intSeries_client_ID']);
                $searchedSery['stripe_account_id'] = $owner['stripe_account_id'];
            }
            if ($searchedSeries){
                $category['series'] = $searchedSeries;
                array_push($searchedCategories, $category);
            }
        }
        return utf8Encode($searchedCategories);
    }
    public function test(){
        $r2 = $this->searchModel->searchSeries('Mindful', 50, 0, ['intSeries_category' => 2]);
        echo '<pre>';
        var_dump($r2);

    }
}

$handle = new Enrich();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'search_categories_by_keyword':
            $keyword = $_POST['keyword'];
            $searchedCategories = $handle->searchCategoriesByKeyword($keyword);
            echo json_encode(['status' => true, 'data' => $searchedCategories]);
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
//    $handle->test();
}