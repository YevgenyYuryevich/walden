<?php

require_once("guardian/access.php");

include_once("config.php");
include_once("dbconnect.php");

$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',  $_SERVER['SERVER_NAME'] );
$current_user = $_SESSION['client_ID'];
$default_ideabox_image = 'assets/images/beautifulideas.jpg';


if( isset($_POST['rssfeed']) ){
	
	
	$rssfeed = filter_var($_POST['rssfeed'], FILTER_SANITIZE_URL);

// Validate url
if (!filter_var($rssfeed, FILTER_VALIDATE_URL) === false) {
	
$query="INSERT INTO `tblRSSFeeds` (`strRssFeeds_URL`, `intRssFeeds_client_ID`, `intRssFeeds_approved`)
VALUES (:rss, :clientID, 0)";
$stmt = $myconnection->prepare ( $query );
try{
	

	$stmt->execute(array(':rss'=>$rssfeed, ':clientID'=>$current_user));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	catch(Exception $e){
		
	}
	
	
	
$showstring="Thank you for adding the RSS feed.  We will review the feed and begin showing results from it as soon as it is approved.";
} else {
$showstring="We were unable to read the RSS feed.  Please resubmit the feed.<br>If you continue to have this problem, please contact us at <a href='mailto:rss@thegreyshirt.com'>rss@thegreyshirt.com</a>";
}
}else{
	
	$showstring="Please submit your blog's RSS feed<br>to include your work in people's daily routines.";
	
}


?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Submit your work for inclusion on the grey shirt</title>

    <link rel="stylesheet" href="assets/css/yevgeny/calendar-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/yevgeny/calendar.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-calendar">
    <header class="site-header">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img class="logo-image" src="assets/images/logo.png"><span> the grey shirt</span><span> [</span><span>simplify daily life</span><span>]</span></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="userpage">My Solutions</a></li>
                        <li><a href="freeyourself">Explore</a></li>
                        <li><a href="createyourexperience">Create</a></li>
                        <li><a href="ideaboxcards">ideaBox</a></li>
                    </ul>
                    <form class="navbar-form navbar-right">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <i class="fa fa-bars humburger_button" style="color:grey;" aria-hidden="true"></i>
                    </form>
                </div>
            </div>
        </nav>
    </header>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Include your work
                </div>
            </div>
            <header class="content-header" style="padding-bottom:25px;">
                <div class="multi-select-series-wrapper">
                    <span><!--- reserved for content on the top right side ---></span>
                    
                </div>
            </header>
            <main class="main-content">
                <div class="content-row-sample">
                    <section class="content-row" style="min-height:550px;">
                        <header class="content-row-title"></header>
                        <div class="content-row-body">
                            <section class="tt-grid-wrapper after-clearfix">
                                <h2>Submit your Blog</h2>
                                <div style="font-size:22px;">
                                <p style="padding-top:25px; padding-bottom:25px; color:#337ab7;"><?php echo $showstring; ?></p>
                                <p>
                                	<form method="POST" action="submittedpost.php">
                                	<label>URL of RSS Feed:</label>
                                		<input style="width:350px;" type="text" placeholder="http://" name="rssfeed"/>
                                		<button class="buttoner">Submit</button>
                                	</form>
                                	
                                </p>
                                </div>
                                <p style="padding-top:10px; padding-bottom:25px;">(The RSS feed is typically located at http://yourwork.com/feed or http://yourwork.com/rss).<br>By submitting, you acknowledge that you agree to our terms and conditions.<br>For more information about the terms and conditions, as well as the procedure<br> that we use to approve blogs and display blog posts, please click here.</p>
                                <h3>Thank you for contributing to the community!</h3>
                                
                               
                               
                            </section>
                        </div>
                    </section>
                </div>
            </main>
        </div>
    </div>
    <footer class="site-footer">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="brand-footer"><a href="#"><img class="logo-image" src="assets/images/logo2.png"><span> the grey shirt</span><span> {</span><span>simplify daily life</span><span>}</span></a></div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <ul class="footer-menu">
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Terms</a></li>
                    <li><a href="#">Jobs</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="footer-copyright">Copyright 2017. All rights reserved.</div>
            </div>
        </div>
    </footer>
</div>



</body>
</html>