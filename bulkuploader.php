<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.06.2018
 * Time: 23:37
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class BulkUploader extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if (!$id) {
            echo 'no id';
            die();
        }
        $viewParams = [
            'seriesId' => $id,
        ];
        $this->load->view('BulkUploader_v', $viewParams);
    }
}

$handle = new BulkUploader();

$handle->index();