<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.04.2018
 * Time: 08:26
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class PreviewSeries extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = 0;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->load->model('api_m/Posts_m');
        $this->load->model('api_m/Series_m');
        $this->load->model('api_m/Purchased_m');
        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();
        $this->model = new \Models\api\Posts_m();
        $this->seriesModel = new \Models\api\Series_m();
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();
    }
    public function index(){
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if (!$id){ echo 'no id'; die(); }

        $series = $this->seriesModel->get($id);

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        $series['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;

//        $posts = $this->model->getPosts(['intPost_series_ID' => $id, 'intPost_parent' => 0], '*', 'intPost_order, post_ID', 100);
        $firstPost = $this->model->get(['intPost_series_ID' => $id, 'intPost_parent' => 0], '*', 'intPost_order, post_ID');
        $nextPosts = $this->postsHandle->getNextPosts($firstPost, 100);
        $posts = array_merge([$firstPost], $nextPosts);
        $viewUri = rawurlencode(BASE_URL . '/viewexperience?id=' . $id . '&affiliate_id=' . $_SESSION['affiliate_id']);
        $uriTitle = rawurlencode($series['strSeries_title']);
        $isLoggedIn = $this->isLoggedIn;

        $owner = $this->userHandle->get($series['intSeries_client_ID']);
        $series['stripe_account_id'] = $owner['stripe_account_id'];
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';

        $this->load->view('PreviewSeries_v', ['posts' => $posts, 'series' => $series, 'isLoggedIn' => $isLoggedIn, 'viewUri' => $viewUri, 'uriTitle' => $uriTitle, 'stripeApi_pKey' => $stripeApi_pKey]);
    }
}

$handle = new PreviewSeries();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}