<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.12.2018
 * Time: 11:02
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Series_Embed extends \Core\Controller_core
{
    private $seriesModel;
    private $postsHandle;
    private $postsModel;
    private $formFieldAnswerModel;
    private $formFieldsModel;
    private $CSViewModel;
    private $purchasedModel;
    private $optionsModel;
    private $userHandle;
    private $isLoggedIn;
    private $seriesHandle;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $_SESSION['f_name'] = $_SESSION['guardian']['f_name'];
            $this->isLoggedIn = true;
        }



        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();

        $this->load->model('api_m/Posts_m');
        $this->load->model('api_m/FormFieldAnswer_m');
        $this->load->model('api_m/FormField_m');
        $this->load->model('api_m/ClientSeriesView_m');
        $this->load->model('api_m/Purchased_m');

        $this->postsModel = new \Models\api\Posts_m();
        $this->formFieldAnswerModel = new \Models\api\FormFieldAnswer_m();
        $this->formFieldsModel = new \Models\api\FormField_m();
        $this->CSViewModel = new \Models\api\ClientSeriesView_m();
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();
    }
    public function index() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $inlineStyle = isset($_POST['inlineStyle']) ? $_POST['inlineStyle'] : (isset($_GET['inlineStyle']) ? $_GET['inlineStyle'] : false);
        $externalStyle = isset($_POST['externalStyle']) ? $_POST['externalStyle'] : (isset($_GET['externalStyle']) ? $_GET['externalStyle'] : false);

        $inlineScript = isset($_POST['inlineScript']) ? $_POST['inlineScript'] : (isset($_GET['inlineScript']) ? $_GET['inlineScript'] : false);
        $externalScript = isset($_POST['externalScript']) ? $_POST['externalScript'] : (isset($_GET['externalScript']) ? $_GET['externalScript'] : false);

        $embedId = isset($_POST['embedId']) ? $_POST['embedId'] : (isset($_GET['embedId']) ? $_GET['embedId'] : 1);

        $viewVersion = isset($_POST['viewVersion']) ? $_POST['viewVersion'] : (isset($_GET['viewVersion']) ? $_GET['viewVersion'] : 'guided');

        $introduced_affiliate = isset($_POST['affiliate_id']) ? $_POST['affiliate_id'] : (isset($_GET['affiliate_id']) ? $_GET['affiliate_id'] : false);

        if ($introduced_affiliate) {
            $_SESSION['introduced_affiliate'] = $introduced_affiliate;
        }

        $series = $this->seriesModel->get($id);
        $formFields = $this->formFieldsModel->getRows(['intFormField_series_ID' => $series['series_ID']]);
        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);
        $totalMoneySaved = $this->postsModel->sum('intPost_savedMoney', ['intPost_series_ID' => $id]);
        $welcomePost = $this->postsModel->get(77846);
//        $welcomePost = $this->postsModel->get(12062);
        $clientSeriesView = false;
        $seekPost = false;
        $purchased = false;

        if ($this->isLoggedIn) {
            $where = ['intCSView_client_ID' => $_SESSION['client_ID'], 'intCSView_series_ID' => $id];
            $clientSeriesView = $this->CSViewModel->get($where);
            if (!$clientSeriesView) {
                $this->CSViewModel->insert($where);
                $clientSeriesView = $this->CSViewModel->get($where);
            }
            $seekPost = $this->postsHandle->seekPost($clientSeriesView);
            $seekPost = $this->postsHandle->format($seekPost);
            $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        }

        $owner = $this->userHandle->get($series['intSeries_client_ID']);
        $series['stripe_account_id'] = $owner['stripe_account_id'];
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $series['stripeApi_pKey'] = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';
        $postsCount = $this->postsModel->countPosts(['intPost_series_ID' => $series['series_ID']]);
        $params = [
            'inlineStyle' => $inlineStyle,
            'externalStyle' => $externalStyle,
            'inlineScript' => $inlineScript,
            'externalScript' => $externalScript,
            'embedId' => $embedId,
            'series' => $series,
            'isLoggedIn' => $this->isLoggedIn,
            'formFields' => $formFields,
            'formFieldAnswers' => $formFieldAnswers,
            'totalMoneySaved' => $totalMoneySaved,
            'viewVersion' => $viewVersion,
            'welcomePost' => $welcomePost,
            'clientSeriesView' => $clientSeriesView,
            'purchased' => $purchased,
            'postsCount' => $postsCount,
            'seekPost' => $seekPost];
        $params['posts'] = $this->postsHandle->getSerialItems(['intPost_parent' => 0, 'intPost_series_ID' => $params['series']['series_ID']], '*', ['intPost_order', 'post_ID'], 200);
        $params['posts'] = \Helpers\utf8Encode($params['posts']);
        $this->seriesHandle->filterViewRecord($series['series_ID']);
        $this->seriesHandle->filterAnyViewRecord($series['series_ID']);
        switch ($params['viewVersion']) {
            case 'popup':
                $this->load->view('ViewExperience_embed_v', $params);
                break;
            case 'guided':
                $this->load->view('Series_Embed_Guided_v', $params);
                break;
            case 'gallery':
                $this->load->view('SeriesEmbedGallery_v', $params);
                break;
            case 'grid':
                $params['postViewVersion'] = isset($_POST['postViewVersion']) ? $_POST['postViewVersion'] : (isset($_GET['postViewVersion']) ? $_GET['postViewVersion'] : 'popup');
                $this->load->view('SeriesEmbedGrid_v', $params);
                break;
        }
    }
}

$handle = new Series_Embed();

if (isset($_POST['action']) || isset($_GET['action'])){
    $action = isset($_POST['action']) ? $_POST['action'] : $_GET['action'];
    switch ($action) {
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}