<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.07.2018
 * Time: 16:08
 */


require_once 'yevgeny/core/Controller_core.php';
require_once 'guardian/class/userauth.class.php';

class Login extends \Core\Controller_core
{
    private $authHandle;
    public function __construct()
    {
        parent::__construct();
        $this->authHandle = new UserAuthentication();
        $this->load->model('Invitation_m');
        $this->load->model('api_m/Options_m');
        $this->invitationModel = new \Models\Invitation_m();
        $this->optionsModel = new \Models\api\Options_m();
        if ($this->isLoggedIn()) {
            $prevPage = isset($_SESSION['uri']) && $_SESSION['uri'] ? $_SESSION['uri'] : '/my_solutions';
            $prevPage = $prevPage == '/index' || $prevPage == '/index.php' ? '/my_solutions' : $prevPage;
            header('Location:' . $prevPage);
        }

    }
    public function index() {
        $prevPage = isset($_SESSION['uri']) && $_SESSION['uri'] ? $_SESSION['uri'] : '/my_solutions';
        $prevPage = $prevPage == '/index' || $prevPage == '/index.php' ? '/my_solutions' : $prevPage;
        if ($this->isLoggedIn() && false) {
            header('Location:' . $prevPage);
        }
        else {
            $viewParams = ['prevPage' => $prevPage];
            $viewParams['type'] = 'signin';
            $viewParams['temper'] = '';
            $viewParams['token'] = '';
            if (isset($_GET['reset_pw'])) {
                $viewParams['type'] = 'rest_pw';
                $viewParams['temper'] = $_GET['temper'];
            }
            else if (isset($_GET['register'])){
                $viewParams['type'] = 'register';
            }
            if (isset($_GET['token'])){
                $viewParams['token'] = $_GET['token'];
                $viewParams['invitationRow'] = $this->invitationModel->get(['strToken' => $_GET['token']]);
            }
            else {
                $viewParams['invitationRow'] = false;
            }

            $allowToRegisterRow = $this->optionsModel->get(['strOption_name' => 'users_can_register']);
            $viewParams['canRegister'] = $allowToRegisterRow ? (int)$allowToRegisterRow['strOption_value'] : false;
            $viewParams['canRegister'] = $viewParams['canRegister'] ? $viewParams['canRegister'] : ($viewParams['invitationRow'] ? true : false);
//            $this->load->view('SignIn_v', $viewParams);
            $this->load->view('Login_v', $viewParams);
        }
    }
    public function ajax_signIn() {
        $info = [];
        $info['result'] = $this->authHandle->signin();
        $info['status'] = isset($info['result']['data']) ? true : false;
        exit(json_encode($info));
    }
    public function ajax_register(){
        $info = [];
        $info['result'] = $this->authHandle->register();
        $info['status'] = $info['result'] ? true : false;
        exit(json_encode($info));
    }
    public function ajax_sendEmail(){
        $info = [];
        $info['result'] = $this->authHandle->ajax_sendEmail();
        $info['status'] = isset($info['result']['data']) ? true : false;
        exit(json_encode($info));
    }
    public function ajax_resetSend(){
        $info = [];
        $info['result'] = $this->authHandle->reset_send();
        $info['status'] = isset($info['result']['success']) ? true : false;
        exit(json_encode($info));
    }
    public function ajax_resetPassword(){
        $info = [];
        $info['result'] = $this->authHandle->reset_pw();
        $info['status'] = isset($info['result']['success']) ? true : false;
        exit(json_encode($info));
    }
    public function ajax_check(){
        $info = [];
        $info['result'] = $this->authHandle->check();
        $info['status'] = $info['result'];
        exit(json_encode($info));
    }
    public function isLoggedIn(){
        return $this->authHandle->isLoggedIn();
    }
}

$handle = new Login();

if (isset($_POST['check'])){
    $handle->ajax_check();
}
else if (isset($_POST['function'])){
    switch ($_POST['function']){
        case 'signin':
            $handle->ajax_signIn();
            break;
        case 'register':
            $handle->ajax_register();
            break;
        case 'reset_send':
            $handle->ajax_resetSend();
            break;
        case 'reset_pw':
            $handle->ajax_resetPassword();
            break;
        case 'send_email':
            $handle->ajax_sendEmail();
            break;
        case 'check':
            $handle->ajax_check();
            break;
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}