<?php
include_once("config.php");
include_once("dbconnect.php");

//CLEAN GET VARS
$myid=$_GET['id'];
$myid = stripslashes($myid);
$myid=trim($myid);
$myid=htmlspecialchars($myid);

$myimage="";
$series=0;


$query="SELECT post_ID, dtPost_due, strSeriesFrequency_name, strCategory_name, strSeries_description, strSeries_title, strSeries_writernotes, series_ID, strPost_title, strPost_body, strPost_summary, strPost_keywords, intPost_days, strPost_featuredimage   FROM `tblpost` INNER JOIN tblseries ON tblpost.intPost_series_ID=tblseries.series_ID
INNER JOIN tblseriesfrequency ON tblseries.intSeries_frequency_ID=tblseriesfrequency.seriesfrequency_ID
INNER JOIN tblcategories ON tblseries.intSeries_category=tblcategories.category_ID
WHERE post_ID=:post
LIMIT 1";

$stmt = $myconnection->prepare ( $query );

$stmt->execute(array( ':post' => $myid ));
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($rows as $row){

    $time = strtotime($row['dtPost_due']);
    $date = date("m/d/y", $time);

    $title=stripslashes($row['strSeries_title']);
    $description=$row['strSeries_description'];
    $category=$row['strCategory_name'];
    $writersnotes=$row['strSeries_writernotes'];
    $posttitle=stripslashes($row['strPost_title']);
    //$postbody=htmlentities($row['strPost_body']);
    $postbody=stripslashes(htmlspecialchars_decode($row['strPost_body']));


    $postkeywords=$row['strPost_keywords'];
    $postsummary=$row['strPost_summary'];
    $postdays=$row['intPost_days'];
    $series=$row['series_ID'];
    $myimage=$row['strPost_featuredimage'];
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Home</title>
    <!--Css contection!-->
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi|EB+Garamond" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main_blog.css">
    <link rel="stylesheet" href="assets/css/adaptive_blog.css">


    <!--- audio player --->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="assets/audioplayer/audioplayer.js" type="text/javascript"></script>
    <link rel='stylesheet' type="text/css" href="assets/audioplayer/audioplayer.css"/>

    <!--- end audio player --->

</head>
<body>
<div class="content_container">
    <div class="header">
        <div class="line_container"> <img src="assets/images/line.png" alt="line"> </div>
        <div class="text_container">
            <div class="title" style="text-transform: uppercase;">

                <?php

                echo $title;

                ?>

            </div>
            <div class="subtitle">

                <?php
                //$posttitle=str_replace(" by ", "<p>by</p>", $posttitle);

                $myend=strrpos($posttitle, " - ");
                $posttitle=substr($posttitle, 0, $myend);

                echo $posttitle;

                ?>


            </div>

        </div>
        <div class="line_container" style="margin-top:20px;"> <img src="assets/images/line.png" alt="line"> </div>
        <div id="header_1" class="animated subtext">
            <div style="color:grey; padding-bottom:20px; padding-top:10px; font-size:20px;">Read time: <div class="eta" style=" display: inline-block;"></div></div>


            <article style="line-height: 48px; font-size:24px; padding-left:20px; padding-right:60px;">
                <?php
                echo $postbody;
                ?>


            </article>
        </div>
    </div>

    <div class="footer">

    </div>
</div>
<!--Script contection!-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
<script src="assets/js/animation.js"></script>

<script src="assets/js/readingTime.min.js"></script>

<script>

    $(function() {

        $('article').readingTime({
            wordCountTarget: '.words',
            wordsPerMinute: 200,
        });

    });

</script>

<!--Script contection!-->

</body>
</html>
