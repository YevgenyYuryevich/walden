<?php
include_once("config.php");
include_once("dbconnect.php");

session_start();
$client_id = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;

$query="SELECT series_ID, strSeries_title, strSeries_description, strSeries_image
    FROM tblseries
    WHERE intSeries_category = :category_id
    AND series_ID NOT IN (
    SELECT intTrash_series_ID AS series
    FROM tbltrash
    WHERE intTrash_client_ID = :client_id
    UNION
    SELECT intPurchased_series_ID AS series
    FROM tblpurchased
    WHERE intPurchased_client_ID = :client_id
    AND boolPurchased_active=1)";

$myconnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);;
$stmt = $myconnection->prepare ( $query );
$stmt->bindValue(':category_id', $_GET['category_id']);
$stmt->bindValue(':client_id', $client_id);
$rlt = $stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Home</title>

    <!--Css contection!-->
    <link rel="stylesheet" href="assets/css/materialize.css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi|EB+Garamond|Montserrat" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/adaptive.css">
    <link rel="stylesheet" href="assets/css/category.css">
    <link rel="stylesheet" href="assets/css/category_adaptive.css">
    <link rel="stylesheet" href="assets/css/yevgeny_custom.css" />
    <link rel="stylesheet" href="assets/css/yevgeny/component.css" />
    <link rel="stylesheet" href="assets/css/yevgeny/demo.css" />
    <link rel="stylesheet" href="assets/css/yevgeny/normalize.css" />
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Reem+Kufi" rel="stylesheet">
    <script src="assets/js/yevgeny/modernizr.custom.js"></script>
</head>

<body>
<div class="category_content category_content_1">
    <div class="menu">
        <div class="row" style="margin: 0;">
            <div class="col-xs-4 col-md-4 com-sm-4 left_part">
                <img width="50px" src="assets/images/logo.png"> <font style="color:grey; font-family:'EB Garamond', sans-serif;">the grey shirt</font> <font style="font-size:18px;color:grey">[</font><font style="font-size:18px;color:#9e9e9e; font-family:'EB Garamond', sans-serif;">simplify daily life</font><font style="font-size:18px;color:grey">]</font>
            </div>
            <div class="col-xs-5 col-md-5 com-sm-5 center_part">
                <div class="col sp1" style="padding-left: 6px; padding-right:18px; border-right:1px solid grey;">
                    <a class="main_menu" href="userpage.html">My Solutions</a>
                </div>
                <div class="col sp1" style=" padding-left: 18px; padding-right:18px; border-right:1px solid grey;">
                    <a class="main_menu" href="category.html">Explore</a>
                </div>
                <div class="col sp1" style=" padding-left: 18px; padding-right:18px; border-right:1px solid grey;">
                    <a class="main_menu" href="userpage.html">Create</a>
                </div>
                <div class="col sp1" style="padding-left: 18px; padding-right:6px;">
                    <a class="main_menu" href="ideabox.html">ideaBox</a>
                </div>
            </div>
            <div class="col-xs-3 col-md-3 com-sm-3 right_part">
                <div style="display:inline-block; width:calc(100% - 28px); position:relative;">
                    <input type="text" name="" value="" class="search_input">
                    <i class="fa fa-search seatch_icon" aria-hidden="true"></i>
                </div>
                <i class="fa fa-bars humburger_button" style="color:grey;" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    <div><div class="row"></div></div>
    <div class="events">
        Today's Events
        <i class="material-icons">arrow_drop_down_circle</i>
    </div>
    <div class="insider insider_1" >
        <div class="top_line"> </div>
        <div class="bottom_line"> </div>
        <div class="right_line"> </div>
        <div class="left_line"> </div>
        <div class="rectangle" style="top:-10px; right:-10px;"></div>
        <div class="rectangle" style="bottom:-10px; right:-10px;"></div>
        <div class="row main_row" style="height:100%; border-top:2px solid black;">
            <div class="col-xs-0 col-sm-6 col-md-4 col-lg-3 category_rows">
                <div class="big_rectangle"><i class="fa fa-bicycle" aria-hidden="true"></i>&nbsp; Free Yourself<i class="fa fa-caret-down drop_down" aria-hidden="true"></i> </div>
                <div class="first_column">
                    <div class="category_column">
                        <img src="assets\images\free3.jpg" alt="" class="first_column_image">
                        <div class="first_column_data">

                            <div class="header_first_column"></div>
                            <div class="header_first_quotes">&rdquo;</div>
                            <div class="header_first_text">
                                "Instead of trying to make your life perfect, give yourself the freedom to make it an adventure, and go ever upward."
                                <br>
                                <div class="author">-Drew Houston</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
                <div class=" row project_rows">
                    <?php foreach ($rows as $row){
                        $rowid=htmlspecialchars_decode($row['series_ID']);
                        $rowimage=htmlspecialchars_decode($row['strSeries_image']);
                        $rowtitle=htmlspecialchars_decode($row['strSeries_title']);
                        $rowbody=htmlspecialchars_decode($row['strSeries_description']);
                        ?>
                        <div class="col-md-12 col-lg-4 parent_column">
                            <div class="column_overflow">
                                <div id="first_column" class="column" style="padding-top:30px;">
                                    <div class="column_object" title= '<?php  echo strip_tags($rowbody);  ?>'>
                                        <img src="<?php echo $rowimage; ?>" alt="" class="object_image">
                                        <div class="object_title"><?php echo $rowtitle; ?></div>
                                        <div class="action_button"><a href="javascript:;"> Join </a></div>
                                        <div class="action_button"><a href="javascript:;"> Trash </a></div>
                                        <div class="action_button"><a href="javascript:;"> Preview </a></div>
                                        <div class="object_line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_end">
    <div class="row footer_container">
        <div class="col-md-4">
            <div class="forma_header" style="margin-top: 15px;font-size: 26px;">
                <img src="assets/images/logo2.png" width="40px">
                <font style="color:grey;">the grey shirt</font> <font style="font-size:18px;color:grey">[</font><font style="font-size:18px;">simplify daily life</font><font style="font-size:18px;color:grey">]</font>
                <br>
            </div>
        </div>
        <div class="col-md-4" style="text-align: center">
            <div class="col-md-2 menuer" style="text-align: center"><span class="text">About</span></div>
            <div class="col-md-2 menuer" style="text-align: center"><span class="text">Contact</span></div>
            <div class="col-md-2 menuer" style="text-align: center"><span class="text">Privacy</span></div>
            <div class="col-md-2 menuer" style="text-align: center"><span class="text">Terms</span></div>
            <div class="col-md-2 menuer" style="text-align: center"><span class="text">Jobs</span></div>
        </div>
        <div class="col-md-4" style="text-align: right">
            <div class="" style="padding-top:25px; font-size: 14px;">
                Copyright 2017.  All rights reserved.
            </div>
        </div>
    </div>
</div>
<!--Script contection!-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
<script src="assets/js/yevgeny_custom.js"></script>
<script src="assets/js/category.js"></script>
<script src="assets/js/yevgeny/classie.js"></script>
<script src="assets/js/yevgeny/thumbnailGridEffects.js"></script>

</body>

</html>
