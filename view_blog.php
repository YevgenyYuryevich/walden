<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 02.11.2018
 * Time: 10:44
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

use Models\View_m;
use Models\api\Posts_m;

class View_Blog extends \Core\Controller_core
{
    private $isLoggedIn;
    private $model;
    private $postModel;
    private $subsModel;
    private $purchasedModel;
    private $seriesModel;
    private $subsHandle;
    private $postsHandle;
    private $userHandle;
    private $CSViewModel;
    private $optionsModel;
    private $formFieldAnswerModel;
    private $formFieldsModel;
    private $postNotesModel;
    private $favoriteModel;
    private $subsCpltModel;
    private $seriesHandle;

    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->load->model('View_m');
        $this->load->model('api_m/Posts_m');
        $this->load->model('api_m/Subscriptions_m');
        $this->load->model('api_m/Purchased_m');
        $this->load->model('api_m/Series_m');
        $this->load->controller('Subscriptions_c');
        $this->load->controller('Posts_c');

        $this->load->model('api_m/FormFieldAnswer_m');
        $this->load->model('api_m/FormField_m');
        $this->load->model('api_m/ClientSeriesView_m');

        $this->formFieldAnswerModel = new \Models\api\FormFieldAnswer_m();
        $this->formFieldsModel = new \Models\api\FormField_m();

        $this->model = new View_m();
        $this->postModel = new Posts_m();
        $this->subsModel = new \Models\api\Subscriptions_m();
        $this->purchasedModel = new \Models\api\Purchased_m();
        $this->seriesModel = new \Models\api\Series_m();
        $this->subsHandle = new \Controllers\Subscriptions_c();
        $this->postsHandle = new \Controllers\Posts_c();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();

        $this->CSViewModel = new \Models\api\ClientSeriesView_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->model('api_m/PostNotes_m');
        $this->postNotesModel = new \Models\api\PostNotes_m();

        $this->load->model('api_m/Favorite_m');
        $this->favoriteModel = new \Models\api\Favorite_m();

        $this->subsCpltModel = $this->subsHandle->subsCpltModel;

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();
    }
    public function index() {

        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $from = isset($_POST['from']) ? $_POST['from'] : (isset($_GET['from']) ? $_GET['from'] : 'post');

        $prevPage = isset($_POST['prevpage']) ? $_POST['prevpage'] : (isset($_GET['prevpage']) ? $_GET['prevpage'] : 'viewexperience');

        if (!$id) { die('no id'); }

        if ($from === 'subscription') {
            $postData = $this->subsHandle->get($id);
            $purchased = $this->purchasedModel->get($postData['intClientSubscription_purchased_ID']);
            $series = $this->seriesModel->get($purchased['intPurchased_series_ID']);
        }
        else {
            $postData = $this->postModel->get($id);
            $postData = $this->postsHandle->filterRedirect($postData);
            $series = $this->seriesModel->get($postData['intPost_series_ID']);
            $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        }
        if (!$postData){ die('no id'); }


        if (!$this->isLoggedIn){
            $visited_ids = isset($_SESSION['visited_ids']) ? $_SESSION['visited_ids'] : [];
            $visitCnt_left = isset($_SESSION['visitCnt-left']) ? $_SESSION['visitCnt-left'] : 10;

            if (!in_array($id, $visited_ids)){
                $_SESSION['visited_ids'][] = $id;
                $_SESSION['visitCnt-left'] = $visitCnt_left = $visitCnt_left - 1;
            }

            if ($visitCnt_left < 0){
                $_SESSION['visitCnt-left'] = 0;
                $_SESSION['uri'] = $_SERVER['REQUEST_URI'];
                header('Location:/login');
            }
            else{
                $_SESSION['visitCnt-left'] = $visitCnt_left;
            }
        }
        else{
            $visitCnt_left = 10;
            if (isset($_SESSION['visited_ids'])){
                unset($_SESSION['visited_ids']);
            }
            if (isset($_SESSION['visitCnt-left'])){
                unset($_SESSION['visitCnt-left']);
            }
        }

        $postData = $this->formatPost($postData, $from);

        switch ($prevPage){
            case 'calendar':
                $prevPage = $prevPage . '?series_id=' . $series['series_ID'];
                break;
            default:
                $prevPage = $prevPage . '?id=' . $series['series_ID'];
                break;
        }

        $isLoggedIn = $this->isLoggedIn;

        $formFields = $this->formFieldsModel->getRows(['intFormField_series_ID' => $series['series_ID']]);
        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);

        $owner = $this->userHandle->get($series['intSeries_client_ID']);
        $series['stripe_account_id'] = $owner['stripe_account_id'];

        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $series['stripeApi_pKey'] = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';
        $postsCount = $this->postModel->countPosts(['intPost_series_ID' => $series['series_ID']]);
        $stripeApi_pKey = $series['stripeApi_pKey'];
        $postData = \Helpers\utf8Encode($postData);
        $series = \Helpers\utf8Encode($series);
        $series['unreadPostsCount'] = $this->seriesHandle->unReadPostsCount($series['series_ID']);
        $series['permission'] = $this->seriesHandle->getRole($series);
        $this->seriesHandle->filterViewRecord($series['series_ID']);
        $this->seriesHandle->filterAnyViewRecord($series['series_ID']);
        $this->load->view('View_Blog_v', ['postData' => $postData, 'series' => $series, 'permission' => $series['permission'], 'purchased' => $purchased, 'from' => $from, 'prevPage' => $prevPage, 'isLoggedIn' => $isLoggedIn, 'visitCnt_left' => $visitCnt_left, 'formFields' => $formFields, 'formFieldAnswers' => $formFieldAnswers, 'postsCount' => $postsCount, 'stripeApi_pKey' => $stripeApi_pKey]);
    }

    public function formatPost($postData, $from = 'post') {

        $postData['meta'] = [];

        if ($from == 'post') {
            $postData['meta']['viewUri'] = rawurlencode(BASE_URL . '/view_blog?id=' . $postData['post_ID'] . ($_SESSION['affiliate_id'] !== -1 ? '&affiliate_id=' . $_SESSION['affiliate_id'] : ''));
            $postData['meta']['uriTitle'] = rawurlencode($postData['strPost_title']);
            $metaDes = \Helpers\htmlPurify(substr($postData['strPost_body'], 0, 150));
            $postData['meta']['metaDes'] = str_replace('\'', '"', $metaDes);
            $postData['meta']['image'] = $postData['strPost_featuredimage'];
            switch ((int) $postData['intPost_type']) {
                case 0:
                    $postData['meta']['type'] = 'music';
                    break;
                case 2:
                    $postData['meta']['type'] = 'video';
                    break;
                default:
                    $postData['meta']['type'] = 'website';
                    break;
            }
            if ($postData['strPost_nodeType'] == 'menu') {
                $where = [
                    'intPost_series_ID' => $postData['intPost_series_ID'],
                    'intPost_parent' => $postData['post_ID']
                ];
                $paths = $this->postModel->getPosts($where);
                $postData['paths'] = $paths;
            }
            $where = [
                'postNote_user_ID' => $_SESSION['client_ID'],
                'postNote_post_ID' => $postData['post_ID']
            ];
            $postData['postNotes'] = $this->postNotesModel->getRows_where_post_or_sub($where);
            $favorite = $this->favoriteModel->get(['intFavorite_post_ID' => $postData['post_ID'], 'intFavorite_client_ID' => $_SESSION['client_ID']]);
            $postData['favorite'] = $favorite ? $favorite : false;
            $postData['viewDay'] = $this->postsHandle->calcDay($postData);
        }
        else {
            $postData['meta']['viewUri'] = rawurlencode(BASE_URL . '/view_blog?id=' . $postData['intClientSubscription_post_ID'] . ($_SESSION['affiliate_id'] !== -1 ? '&affiliate_id=' . $_SESSION['affiliate_id'] : ''));
            $postData['meta']['uriTitle'] = rawurlencode($postData['strClientSubscription_title']);
            $metaDes = \Helpers\htmlPurify(substr($postData['strClientSubscription_body'], 0, 150));
            $postData['meta']['metaDes'] = str_replace('\'', '"', $metaDes);
            $postData['meta']['image'] = $postData['strClientSubscription_image'];
            switch ((int) $postData['intClientSubscriptions_type']) {
                case 0:
                    $postData['meta']['type'] = 'music';
                    break;
                case 2:
                    $postData['meta']['type'] = 'video';
                    break;
                default:
                    $postData['meta']['type'] = 'website';
                    break;
            }
            if ($postData['strClientSubscription_nodeType'] == 'menu') {
                if (!is_null($postData['intClientSubscription_post_ID'])) {
                    $post = $this->postModel->get($postData['intClientSubscription_post_ID']);
                    $this->subsHandle->subscribeSeries(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['post_ID']], $postData['intClientSubscription_purchased_ID']);
                }
                $where = [
                    'intClientSubscription_purchased_ID' => $postData['intClientSubscription_purchased_ID'],
                    'intClientSubscriptions_parent' => $postData['clientsubscription_ID']
                ];
                $paths = $this->subsHandle->getSubscriptions($where);
                $postData['paths'] = $paths;
            }
            $where = [
                'postNote_user_ID' => $_SESSION['client_ID'],
                'postNote_subscription_ID' => $postData['clientsubscription_ID']
            ];
            if ($postData['intClientSubscription_post_ID']) {
                $where['postNote_post_ID'] = $postData['intClientSubscription_post_ID'];
            }
            $postData['postNotes'] = $this->postNotesModel->getRows_where_post_or_sub($where);
            $favorite = $this->favoriteModel->get(['intFavorite_subscription_ID' => $postData['clientsubscription_ID'], 'intFavorite_client_ID' => $_SESSION['client_ID']]);
            $postData['favorite'] = $favorite ? $favorite : false;
        }
        $postData['meta']['image'] = \Helpers\isAbsUrl($postData['meta']['image']) ? $postData['meta']['image'] : (BASE_URL . ($postData['meta']['image'][0] === '/' ? '' : '/') . $postData['meta']['image']);
        list($width, $height) = @getimagesize($postData['meta']['image']);
        if ($width && $height) {
            $width = $width > 200 ? $width : 200;
            $height = $height > 200 ? $height : 200;
            $postData['meta']['image_width'] = $width;
            $postData['meta']['image_height'] = $height;
        }
        return $postData;
    }
    public function updatePostType($id = false) {
        $id = $id ? $id : $_GET['id'];
        $post = $this->model->getPostData($id);
        $body = $post['strPost_body'];
        $type = $this->getRightType($body, $post['intPost_type']);
        if ($type != $post['intPost_type']){
            $this->model->updatePost($id, ['intPost_type' => $type]);
        }
        return $type;
    }
    public function ajax_nextPost() {
        $id = $_POST['id'];

        $nextPost = $this->postsHandle->getNextPost($id);

        if ($nextPost){
            $nextPost = $this->formatPost($nextPost);
            echo json_encode(['status' => true, 'data' => $nextPost]);
        }
        else{
            echo json_encode(['status' => false, 'data' => 'no item']);
        }
        die();
    }
    public function ajax_prevPost() {

        $id = $_POST['id'];

        $prevPost = $this->postsHandle->getPrevPost($id);

        if ($prevPost){
            $prevPost = $this->formatPost($prevPost);
            echo json_encode(['status' => true, 'data' => $prevPost]);
        }
        else{
            echo json_encode(['status' => false, 'data' => 'no item']);
        }
        die();
    }
    public function ajax_setPath() {
        $pathId = $_POST['pathId'];
        $seriesId = $_POST['seriesId'];

        $childPost = $this->postModel->get(['intPost_series_ID' => $seriesId, 'intPost_parent' => $pathId], '*', ['intPost_order', 'post_ID']);
        if ($childPost) {
            $childPost = $this->postsHandle->filterRedirect($childPost);
            if ($childPost) {
                $childPost = $this->formatPost($childPost);
                echo json_encode(['status' => true, 'data' => $childPost]);
            }
            else {
                echo json_encode(['status' => false, 'data' => 'no item']);
            }
        }
        else {
            echo json_encode(['status' => false, 'data' => 'no item']);
        }
        die();
    }
    public function ajax_nextSub(){
        $id = $_POST['id'];

        $nextSub = $this->subsHandle->getNextSub($id);

        if ($nextSub){
            $nextSub = $this->subsHandle->formatSubscription($nextSub);
            $nextSub = $this->formatPost($nextSub, 'subscription');
            echo json_encode(['status' => true, 'data' => $nextSub]);
        }
        else{
            echo json_encode(['status' => false, 'data' => 'no item']);
        }
        die();
    }
    public function ajax_prevSub(){

        $id = $_POST['id'];

        $prevSub = $this->subsHandle->getPrevSub($id);

        if ($prevSub){
            $prevSub = $this->subsHandle->formatSubscription($prevSub);
            $prevSub = $this->formatPost($prevSub, 'subscription');
            echo json_encode(['status' => true, 'data' => $prevSub]);
        }
        else{
            echo json_encode(['status' => false, 'data' => 'no item']);
        }
        die();
    }
    public function ajax_setSubPath() {
        $pathId = $_POST['pathId'];
        $purchasedId = $_POST['purchasedId'];

        $childSub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $purchasedId, 'intClientSubscriptions_parent' => $pathId], '*', ['intClientSubscriptions_order', 'clientsubscription_ID']);
        if ($childSub) {
            $childSub = $this->subsHandle->formatSubscription($childSub);
            $childSub = $this->formatPost($childSub, 'subscription');
            echo json_encode(['status' => true, 'data' => $childSub]);
        }
        else {
            echo json_encode(['status' => false, 'data' => 'no item']);
        }
        die();
    }
    public function embedPage() {

        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $from = isset($_POST['from']) ? $_POST['from'] : (isset($_GET['from']) ? $_GET['from'] : 'post');
        $externalStyle = isset($_POST['externalStyle']) ? $_POST['externalStyle'] : (isset($_GET['externalStyle']) ? $_GET['externalStyle'] : false);
        $inlineStyle = isset($_POST['inlineStyle']) ? $_POST['inlineStyle'] : (isset($_GET['inlineStyle']) ? $_GET['inlineStyle'] : false);

        $externalStyle = $externalStyle === 'false' ? false : $externalStyle;
        $inlineStyle = $inlineStyle === 'false' ? false : $inlineStyle;

        $introduced_affiliate = isset($_POST['affiliate_id']) ? $_POST['affiliate_id'] : (isset($_GET['affiliate_id']) ? $_GET['affiliate_id'] : false);

        if ($introduced_affiliate) {
            $_SESSION['introduced_affiliate'] = $introduced_affiliate;
        }

        if (!$id) { die('no id'); }

        if ($from === 'subscription') {
            $postData = $this->subsHandle->get($id);
            $purchased = $this->purchasedModel->get($postData['intClientSubscription_purchased_ID']);
            $series = $this->seriesModel->get($purchased['intPurchased_series_ID']);
        }
        else {
            $postData = $this->postModel->get($id);
            $series = $this->seriesModel->get($postData['intPost_series_ID']);
            $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $series['series_ID']]);
        }

        if (!$postData){ die('no id'); }

        $postData = $this->formatPost($postData, $from);

        $isLoggedIn = $this->isLoggedIn;

        $owner = $this->userHandle->get($series['intSeries_client_ID']);
        $series['stripe_account_id'] = $owner['stripe_account_id'];
        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $series['stripeApi_pKey'] = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';

        $formFields = $this->formFieldsModel->getRows(['intFormField_series_ID' => $series['series_ID']]);
        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);
        $totalMoneySaved = $this->postModel->sum('intPost_savedMoney', ['intPost_series_ID' => $id]);

        $welcomePost = $this->postModel->get(77846);
//        $welcomePost = $this->postModel->get(12062);

        $clientSeriesView = false;
        $seekPost = false;

        if ($isLoggedIn) {
            $where = ['intCSView_client_ID' => $_SESSION['client_ID'], 'intCSView_series_ID' => $id];
            $clientSeriesView = $this->CSViewModel->get($where);
            if (!$clientSeriesView) {
                $this->CSViewModel->insert($where);
                $clientSeriesView = $this->CSViewModel->get($where);
            }
            $seekPost = $this->postsHandle->seekPost($clientSeriesView);
            $seekPost = $this->postsHandle->format($seekPost);
        }

        $this->load->view('ViewEmbed_v', ['postData' => $postData, 'series' => $series, 'purchased' => $purchased, 'from' => $from, 'isLoggedIn' => $isLoggedIn, 'inlineStyle' => $inlineStyle, 'externalStyle' => $externalStyle, 'formFields' => $formFields, 'formFieldAnswers' => $formFieldAnswers, 'totalMoneySaved' => $totalMoneySaved, 'welcomePost' => $welcomePost, 'seekPost' => $seekPost, 'clientSeriesView' => $clientSeriesView]);
    }
    public function getRightType($body, $type){
//        is video
        $isVideo = preg_match('@^(https://www.youtube.com/embed/)@', $body, $matches);
        if ($isVideo){
            return 2;
        }
//        is audio
        $isMyAudio = preg_match('@^([\w\/\.\-\:]+)((\.mp3)|(play))$@', $body, $matches);
        if ($isMyAudio){
            return 0;
        }
        return $type;
    }
    public function test(){
        $posts = $this->model->allPosts();
        foreach ($posts as $post){
            $this->updatePostType($post['post_ID']);
        }
    }
    public function ajax_getPost() {
        $id = $_POST['id'];
        $post = $this->postModel->get($id);
        $post = $this->formatPost($post, 'post');
        echo json_encode(['status' => true, 'data' => $post]);
        die();
    }
    public function ajax_allPosts() {
        if (isset($_POST['id'])) {
            $postId = $_POST['id'];
        } else {
            echo json_encode(['status' => false, 'data' => []]);
        }
        $historyPosts = $this->postsHandle->getHistoryItems($postId);
        $nextPosts = $this->postsHandle->getSerialItems($postId);
        $nextPosts[0]['current'] = true;
        $allPosts = array_merge($historyPosts, $nextPosts);
        if ($_POST['from'] === 'subscription') {
            foreach ($allPosts as &$iPost) {
                $sub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $_POST['purchased_ID'], 'intClientSubscription_post_ID' => $iPost['post_ID']]);
                if ($sub) {
                    $subsCplt = $this->subsCpltModel->get(['intSubscription_ID' => $sub['clientsubscription_ID']], '*', 'created_datetime DESC');

                    if ($subsCplt) {
                        $iPost['boolCompleted'] = (int) $subsCplt['boolCompleted'];
                    }
                    else {
                        $iPost['boolCompleted'] = 0;
                    }
                }
            }
        }
        $unReadPosts = $this->seriesHandle->unReadPosts($nextPosts[0]['intPost_series_ID']);
        echo json_encode(['status' => true, 'data' => $allPosts, 'unReadPosts' => $unReadPosts]);
        die();
    }
}

$viewHandle = new View_Blog();

if (isset($_POST['action']) || isset($_GET['action'])){
    $action = isset($_POST['action']) ? $_POST['action'] : $_GET['action'];
    switch ($action){
        case 'all_posts':
            $viewHandle->ajax_allPosts();
            break;
        case 'get_post':
            $viewHandle->ajax_getPost();
            break;
        case 'next_post':
            $viewHandle->ajax_nextPost();
            break;
        case 'prev_post':
            $viewHandle->ajax_prevPost();
            break;
        case 'set_path':
            $viewHandle->ajax_setPath();
            break;
        case 'next_sub':
            $viewHandle->ajax_nextSub();
            break;
        case 'prev_sub':
            $viewHandle->ajax_prevSub();
            break;
        case 'set_subPath':
            $viewHandle->ajax_setSubPath();
            break;
        case 'update_post_type':
            $viewHandle->updatePostType();
            break;
        case 'embed_page':
            $viewHandle->embedPage();
            break;
        default:
            $viewHandle->index();
            break;
    }
}
else{
    $viewHandle->index();
//    $viewHandle->test();
}
