<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Denver Legal Help - Innovative Law that works for you.</title>
        <meta name="description" content="Denver Legal Help works in the interest of individuals who need help by combining legal experience with technology.">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <!-- Place favicon.ico in the root directory -->

		<!-- CSS here -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
		<link rel="stylesheet" href="css/slinky.min.css">
		<link rel="stylesheet" href="css/meanmenu.css">
        <link rel="stylesheet" href="css/icofont.css">
        <link rel="stylesheet" href="css/hamburgers.min.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="css/default.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

		<!-- header start -->
		<header>
			<div class="header-area header-transparent pt-40 pb-40">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-xl-4 col-lg-3 col-md-3 col-6">
							<div class="logo ">
								<a href="index.html" style="font-size:18px; color:#989898;"> <img style="width: 60px;" src="img/slider/logo.png">Denver Legal Help</a>
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-5 d-none d-md-block">
							<div class="header-info text-right">
								<span>Contact: <span>(303)</span> 862-1061</span>
							</div>
						</div>
						<div class="col-xl-2 col-lg-3 col-md-4 d-none d-lg-block">
							<div class="header-info text-right">
							<a href="mailto:lawyer@denverlegalhelp.com">
								<span>lawyer<span>@</span>denverlegalhelp.com</span></a>
							</div>
						</div>
						<div class="col-xl-2 col-lg-2 col-md-4 col-6">
							<div class="click-menu f-right">
								<span>Menu </span>
								<button class="click-menu-active"><i class="fas fa-bars"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="transparent-mainmenu text-center">
				<div class="transparent-menu-close-icon">
					<button class="close-icon">
						<i class="icofont icofont-close-line"></i>
					</button>
				</div>
				<div id="menu-full" class="text-left">

						<ul>
							<li>
								<a href="index.html">HOME</a>
								<ul>
									<li>
										<a href="index.html">Home style 1</a>
									</li>
									<li>
										<a href="index-2.html">Home style 2</a>
									</li>
									<li>
										<a href="index-3.html">Home style 3</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="about.html">ABOUT US</a>
							</li>
							<li>
								<a href="#">PAGES</a>
								<ul>
									<li>
										<a href="blog.html">Blog</a>
									</li>
									<li>
										<a href="blog-details.html">Blog Details</a>
									</li>
									<li>
										<a href="project-details.html">Project Details</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="project.html">PROJECT</a>
							</li>
							<li>
								<a href="contact.html">CONTACT</a>
							</li>
						</ul>

				</div>
			</div>
		</header>
		<!-- header end -->
		
		
		
		<!-- testimonial-area start -->
		<div class="testimonial-area pl-120 pr-120">
			<div class="testimonial-inner pt-85 pb-90" style="background-image:url(img/testimonial/testimonial-bg.png)">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="area-title mb-60">
								<h1 class="outer">SEARCH RESULTS</h1>
								<h2>OPEN CASES </h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="testimonial-active owl-carousel">
							<div class="testimonial-wrapper">
								<div class="col-xl-12 col-lg-12">
									
									
														
<?php

	include_once 'config.php';
	try {
		// Make a MySQL Connection
		$pdo = new PDO ( "mysql:host=$database_host;dbname=$database_name", "$database_username", "$database_password" );
	} catch ( PDOException $e ) {
		echo "Failed to get DB handle: " . $e->getMessage () . "\n";
		exit ();
	}
	

	

	$countercheck = 0;

	$strUser_casenumber = $_GET ['casenumber'];

	$strUser_lastname = $_GET ['last_name'];
?>
<h4><i class="icon-user"></i>Step 2 of 3: Select a Reminder Type for <?php echo $strUser_lastname; ?></h4>
<h3>Option 1: Select a Specific Case to Receive Reminders for that Case</h3>

<table>
<tr>
<td><button type='submit' onclick='selectall()'>Select All</button></td><td><button type='submit' onclick='clearall()'>Clear All</button></td>
</tr>
</table>
<script>
function selectall(){

$("input[type='checkbox']").each(function(){
$(this).attr('checked', true); 
});

}
</script>
<script>
function clearall(){

$("input[type='checkbox']").each(function(){
$(this).attr('checked', false); 
});

}
</script>
<?php
	$query = "SELECT strDefendant_courtlocation, GROUP_CONCAT(defendant_ID) as defendantIDs
	          FROM tblDefendant
	          WHERE strDefendant_casenumber=:casenumber
	                AND strDefendant_lastname=:lastname
	          GROUP BY strDefendant_courtlocation ASC";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array(':casenumber' => $strUser_casenumber, ':lastname' => $strUser_lastname));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);


	
	foreach($rows as $row):
	?>
		<br><h4><?php echo html_entity_decode(strtoupper($row['strDefendant_courtlocation'])); ?> COUNTY</h4>
		
		
		<ul>
		<?php
		$countercheck=$countercheck+1;	
		/*
		$individualquery=	"
		
		
		SELECT * FROM 

(SELECT defendant_ID, strDefendant_casenumber, dtHearing_dateofhearing, hearing_ID, strHearing_hearingtype  
							FROM tblDefendant, tblHearings 
							WHERE tblDefendant.defendant_ID=tblHearings.intHearing_defendant_ID
							AND strDefendant_courtlocation ='". $row['strDefendant_courtlocation'] ."' 
							AND strDefendant_Firstname=:firstname 
							AND strDefendant_lastname=:lastname
							ORDER BY dtHearing_dateofhearing DESC
) as my_table_tmp
group by strDefendant_casenumber
order by hearing_ID DESC";
*/

$individualquery="

SELECT defendant_ID, strDefendant_casenumber, MAX(dtHearing_dateofhearing) AS dateofhearing, hearing_ID 
							FROM tblDefendant, tblHearings 
							WHERE tblDefendant.defendant_ID=tblHearings.intHearing_defendant_ID
							AND defendant_ID IN (".$row['defendantIDs'].")
							GROUP BY strDefendant_casenumber
							

";
		$stmt = $pdo->prepare($individualquery); 
		$stmt->execute();

   		while($result5 = $stmt->fetch(PDO::FETCH_ASSOC)):
			if (strcmp($court, $result5['strDefendant_courtlocation']) != 0): ?>
				<br><h4><?php echo "asdas" . html_entity_decode( strtoupper( $result5['strDefendant_courtlocation'] ) ) ?> COUNTY</h4>
		<?php
				$court = $result5['strDefendant_courtlocation'];
			endif;
			

			$caseid = $result5 ['defendant_ID'];

			$dateofhearing = html_entity_decode ( $result5 ['dateofhearing'] );

			$hearing_date = strtotime ( $dateofhearing );

			$formattedDateStr = date ( "F j, Y", $hearing_date );

			$formattedTimeStr = date ( "h:i a", $hearing_date );

			

			$todays_date = date ( "Y-m-d" );

			

			$today = strtotime ( $todays_date );

			$mytimestring = "Last";

			if ($hearing_date >= $today) {

				$mytimestring = "Next";

			}

			

			//$hearingtype = html_entity_decode ( $result5 ['strHearing_hearingtype'] );

			?>
			
			<li>
			
			<input type='checkbox' name='caseid[]' id='caseid' value="<?php echo $caseid; ?>">
				<a href="finalizereminder.php?naturalform=1&caseid=<?php echo $caseid; ?>">
					<b><?php echo html_entity_decode( $result5['strDefendant_casenumber'] ) ?></b>
					
					<br>Date of <?php echo $mytimestring; ?> Hearing: <b><?php echo $formattedDateStr; ?> at <?php echo $formattedTimeStr; ?></b>
				</a>
				<br><a href="view_details.php?caseid=<?php echo $caseid; ?>"><b>View Case Details</b></a>
			</li>
	<?php
	 	endwhile; ?>
		</ul>
		
		<script>
		function passvalue(){
	

var checkboxes = document.getElementsByName('caseid[]');
var vals = "";
for (var i=0, n=checkboxes.length;i<n;i++) {
  if (checkboxes[i].checked) 
  {
  vals += ","+checkboxes[i].value;
  

  }
}
if (vals) vals = vals.substring(1);


		window.location.href="finalizereminder.php?naturalform=1&caseid="+vals+"";
		}
		</script>
		<br>
	<?php
	endforeach;
	echo "<button type='submit'  onclick='passvalue();'>Next Step</button>";
	if ($countercheck >= 1) {
			 $outputstring=$outputstring."<h3>OR<h3>";
			 echo "<hr><h2 align='center'>OR</h2><hr>
	<h3>OPTION 2: Get Notified When We Locate Any Case for ". $strUser_firstname ." ". $strUser_lastname ."</h3>";
		} else{
			echo "<hr><h4 align='center'>No Current Cases Found for the Defendant</h4><hr>";
		echo "<h3>Get Notified When We Locate a Case for ". $strUser_firstname ." ". $strUser_lastname ."</h3>";	
		}
	?>

	<p>
	
	
	
	
	
	
	
	
	
	
	
		
	</p>
	
	<p>NOTE: If you do not see the defendant's' case, please check the spelling of the defendant's name.</p>
	<p>Also note that cases do not appear in our system until one week before the defendant's first court appearance. 
	 So if the defendant has not appeared in court and has a court date that is more than a week away,
	  the defendant's case will not be in our system.</p>
  
							
									
									
									
									
									
									
									
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- cta-area start -->
		<div class="cta-area pt-120 pb-120" style="background-image:url(img/bg/cta-bg.jpg)">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-xl-3 col-lg-4 col-md-5">
						<div class="cta-title">
							<h2 class="white">FEEL FREE
								<br /> TO CONTACT US</h2>
						</div>
					</div>
					<div class="col-xl-6 col-lg-5 col-md-7">
						<div class="cta-content pl-60">
							<p style="text-align:center;">We are available by appointment on:<br>Monday-Saturday 9am-6pm</p><p style="text-align:center;"><br>Call (303) 862-1061</p>
						</div>
					</div>
					<div class="col-xl-3 col-lg-3 col-md-12">
						<div class="cta-button text-left text-md-left text-lg-right">
							<a href="#" class="btn">Price List</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- cta-area end -->
		
		<!-- footer end -->



		<!-- JS here -->
        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/slinky.min.js"></script>
		<script src="js/ajax-form.js"></script>
		<script src="js/jquery.meanmenu.min.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
