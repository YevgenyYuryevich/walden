<?php

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class User_Profile extends \Core\Controller_core {
    private $isLoggedIn;
    private $seriesOwnersModel;
    private $seriesHandle;
    private $userHandle;
    private $userModel;
    private $postsModel;
    private $purchasedModel;
    private $categoryModel;

    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->load->model('api_m/Series_Owners_m');
        $this->seriesOwnersModel = new \Models\api\Series_Owners_m();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();

        $this->userModel = $this->seriesHandle->userModel;

        $this->postsModel = $this->seriesHandle->postsModel;
        $this->userHandle = $this->seriesHandle->userHandle;
        $this->purchasedModel = $this->seriesHandle->purchasedModel;
        $this->categoryModel = $this->seriesHandle->categoryModel;
    }
    public function index() {
        $id = isset($_GET['id']) ? $_GET['id'] : ($this->isLoggedIn ? $_SESSION['client_ID'] : false);
        if ($id === false) { echo die('no id'); }
        $user = $this->userModel->get($id, 'id, username, email, f_name, l_name, phone, image, about_me, social_facebook, social_twitter, social_linkedin, social_pinterest');
        $user['image'] = $user['image'] ?: '/assets/images/global-icons/user-avatar.svg';
        $rows = $this->seriesHandle->getAvailableRows();
        $userSeries = [];
        foreach ($rows as $row) {
            $role = $this->getRole($row, $id);
            if ($role) {
                $row['role'] = $role;
                $this->format($row);
                array_push($userSeries, $row);
            }
        }
        $this->load->view('User_Profile_v', ['user_series' => $userSeries, 'user' => $user]);
    }
    private function getRole($row, $id) {
        if ($row['intSeries_client_ID'] == $id) {
            return 1;
        }
        $ownerRow = $this->seriesOwnersModel->get(['intSeriesOwner_user_ID' => $id, 'intSeriesOwner_series_ID' => $row['series_ID']]);
        return $ownerRow ? (int) $ownerRow['intSeriesOwner_role'] : 0;
    }
    private function format(& $row) {
        $row = \Helpers\utf8Encode($row);
        $row['posts_count'] = $this->postsModel->countPosts(['intPost_series_ID' => $row['series_ID']]);

        $owner = $this->userHandle->get($row['intSeries_client_ID']);
        $row['stripe_account_id'] = $owner['stripe_account_id'];

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $row['series_ID']]);
        $row['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;

        $row['category'] = $this->categoryModel->get($row['intSeries_category']);
        return $row;
    }
}
$handle = new User_Profile();
$handle->index();