<?php

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/CreateSeries_m.php';
require_once 'yevgeny/models/api_m/Series_m.php';
require_once 'yevgeny/vendor/autoload.php';
require_once 'yevgeny/models/api_m/ClientMeta_m.php';

use Core\Controller_core;
use \Crew\Unsplash\Photo;
use Crew\Unsplash\Search;
use Models\CreateSeries_m;
use Models\api\Series_m;
use Twingly\Client;
use Models\api\ClientMeta_m;
use function Helpers\{htmlMailHeader, parseStringToArray};

class CreateSeries extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new CreateSeries_m();
        $this->seriesModel = new Series_m();
        $this->clientMetaModel = new ClientMeta_m();
        $this->load->model('api_m/Options_m');
        $this->optionModel = new \Models\api\Options_m();
        $client = new Google_Client();
        $DEVELOPER_KEY = 'AIzaSyDYQGHEJkL-Kghnc_lO04bOML8_3pfG-s8';
        $client->setDeveloperKey($DEVELOPER_KEY);
        $this->youtubeApi = new Google_Service_YouTube($client);

        Crew\Unsplash\HttpClient::init([
            'applicationId'	=> '98ed5ecd389a5b19e3ae817fdcec9b184388937388ba185cd76fadb9d27eca87',
            'utmSource' => 'yevgeny photo web'
        ]);

        $this->unsplash = new Photo();
        $this->unsplashSearch = new Search();

        $twinglyClient = new Client('DAF536AB-B547-48F7-BCBC-CB2273052DF8');
        $this->twingly = $twinglyClient->query();

        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();
    }
    public function index(){
        $level = isset($_POST['level']) ? $_POST['level'] : (isset($_GET['level']) ? $_GET['level'] : 0);
        $categories = $this->model->getCategories();
        $banToCreatePublic = $this->clientMetaModel->get(['meta_key' => 'ban_create_public_series']);
        $banToCreatePublic = $banToCreatePublic ? (int) $banToCreatePublic['meta_value'] : 0;
        $usersCanEditSeries = $this->optionModel->get(['strOption_name' => 'users_can_edit_series']);
        $usersCanEditSeries = $usersCanEditSeries ? (int) $usersCanEditSeries['strOption_value'] : false;
        $me = $this->userHandle->getMe();
        $stripeAccountId = $me['stripe_account_id'];
        $isStripeAvailable = true && $stripeAccountId;

        $stripeClientRow = $this->optionModel->get(['strOption_name' => 'stripe_client_id']);
        $stripeClientId = $stripeClientRow ? $stripeClientRow['strOption_value'] : '';
        if ($usersCanEditSeries) {

            $this->load->view('CreateSeries_v', ['level' => $level, 'categories' => $categories, 'banToCreatePublic' => $banToCreatePublic, 'isStripeAvailable' => $isStripeAvailable, 'stripeClientId' => $stripeClientId]);
        }
        else {
            $this->load->template('403.php');
        }
    }
    public function test(){
        $items = $this->searchFood2Fork('taste');
        $id = $items['items'][0]->recipe_id;
        $data = $this->getFood2Fork($id);
        echo($this->makePostBodyByRecipe($data));
        die();
    }

    public function searchYoutube($keyword, $pageToken = false){
        if (!!$pageToken){
            $searchResponse = $this->youtubeApi->search->listSearch('snippet', array(
                'type' => 'video',
                'q' => $keyword,
                'maxResults' => 10,
                'pageToken' => $pageToken
            ));
        }
        else{
            $searchResponse = $this->youtubeApi->search->listSearch('snippet', array(
                'type' => 'video',
                'q' => $keyword,
                'maxResults' => 10
            ));
        }
        $items = [];
        foreach ($searchResponse->items as $item){
            array_push($items, $item);
        }
        return ['items' => $items, 'nextPageToken' => $searchResponse->nextPageToken];
    }
    public function searchIdeaBox($keyword, $pageToken = false){
        $searchRlts = $this->model->searchIdeaBox($keyword, $pageToken);
        $nextPageToken = count($searchRlts) + $pageToken;
        return ['items' => $searchRlts, 'nextPageToken' => $nextPageToken];
    }
    public function searchPosts($keyword, $pageToken = false){
        $searchRlts = $this->model->searchPosts($keyword, $pageToken);
        $nextPageToken = count($searchRlts) + $pageToken;
        foreach ($searchRlts as &$searchRlt){
            foreach ($searchRlt as &$value){
                $value = utf8_encode($value);
            }
        }
        return ['items' => $searchRlts, 'nextPageToken' => $nextPageToken];
    }
    public function searchRssbPosts($keyword, $pageToken = false){
        $searchRlts = $this->model->searchRssbPosts($keyword, $pageToken);
        $nextPageToken = count($searchRlts) + $pageToken;
        foreach ($searchRlts as &$searchRlt){
            foreach ($searchRlt as &$value){
                $value = utf8_encode($value);
            }
        }
        return ['items' => $searchRlts, 'nextPageToken' => $nextPageToken];
    }
    public function searchSpreaker($keyword, $pageToken = false){
        $curl = curl_init();
        if ($pageToken){
            $url = $pageToken;
        }
        else{
            $keyword = urlencode($keyword);
            $url = 'https://api.spreaker.com/v2/search?type=episodes&q='. $keyword .'&limit=10';
        }
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        if (isset($result->response->error)){
            return ['items' => [], 'nextPageToken' => false];
        }
        return ['items' => $result->response->items, 'nextPageToken' => $result->response->next_url];
    }
    public function searchTwingly($keyword, $pageToken = false){
        if (!$pageToken){
            $pageToken = 0;
        }
        $this->twingly->search_query = $keyword . ' page:'. $pageToken .' page-size:10';
        $result = $this->twingly->execute();
        $imgs = $this->getPhotosFromUsp($keyword, 10, $pageToken);
        $img_ct = count($imgs);
        $posts = $result->posts;
        foreach ($posts as $key => &$post){
            $imgUrl = $img_ct ? $imgs[$key % $img_ct]['urls']['small'] : $this->defaultPhoto;
            $post->imgOrg = $imgUrl;
        }
        return ['items' => $result->posts, 'nextPageToken' => ++$pageToken];
    }
    public function searchFood2Fork($keyword, $pageToken = false){
        $curl = curl_init();
        $pageToken = $pageToken ? $pageToken : 0;
        $keyword = urlencode($keyword);
        $url = 'http://food2fork.com/api/search?key=4b57211bd951556bb6f35998d1586fff&q='. $keyword .'&page=' . floor($pageToken / 3);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        $recipes = $result->recipes;
        $offset = (($pageToken) % 3) * 10;
        $page = array_slice($recipes, $offset, 10);
        $nextPageToken = count($page) == 10 ? ++$pageToken : $pageToken;

        return ['items' => $page, 'nextPageToken' => $nextPageToken];
    }
    public function searchPostsByKeyword(){
        $keyword = $_POST['keyword'];
        $pageTokenY = $_POST['pageTokenY'] !== 'false' ?  $_POST['pageTokenY'] : false;
        $pageTokenS = $_POST['pageTokenS'] !== 'false' ?  $_POST['pageTokenS'] : false;
        $pageTokenT = $_POST['pageTokenT'] !== 'false' ?  $_POST['pageTokenT'] : false;
        $pageTokenF = $_POST['pageTokenF'] !== 'false' ?  $_POST['pageTokenF'] : false;
        $pageTokenI = $_POST['pageTokenI'] !== 'false' ?  $_POST['pageTokenI'] : false;
        $pageTokenP = $_POST['pageTokenP'] !== 'false' ?  $_POST['pageTokenP'] : false;
        $pageTokenRssbP = $_POST['pageTokenRssbP'] !== 'false' ?  $_POST['pageTokenRssbP'] : false;

        $youtubeRes = $this->searchYoutube($keyword, $pageTokenY);
        $spreakerRes = $this->searchSpreaker($keyword, $pageTokenS);
//        $twinglyRes = $this->searchTwingly($keyword, $pageTokenT);
        $twinglyRes = ['items' => [], 'nextPageToken' => false];
        $foodRes = $this->searchFood2Fork($keyword, $pageTokenF);
        $ideaboxRes = $this->searchIdeaBox($keyword, $pageTokenI);
        $postsRes = $this->searchPosts($keyword, $pageTokenP);
        $rssbPostsRes = $this->searchRssbPosts($keyword, $pageTokenRssbP);

        echo json_encode(['status' => true, 'data' => ['youtube' => $youtubeRes, 'spreaker' => $spreakerRes, 'blogs' => $twinglyRes, 'recipes' => $foodRes, 'ideabox' => $ideaboxRes, 'posts' => $postsRes, 'rssbPosts' => $rssbPostsRes]]);
        die();
    }
    public function getFood2Fork($id){
        $curl = curl_init();
        $url = 'http://food2fork.com/api/get?key=4b57211bd951556bb6f35998d1586fff&rId=' . $id;
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        return $result->recipe;
    }
    public function formatYoutubeById($id){
        return 'https://www.youtube.com/embed/' . $id;
    }
    public function formatPodcastsById($id){
        return 'https://api.spreaker.com/v2/episodes/'. $id .'/play';
    }

    public function getPhotoFromUnsplash($query, $for){
        $filter = ['query' => $query, 'w' => 300];
        $unsplashPhoto = $this->unsplash::random($filter);
        if ($unsplashPhoto->errors){
            return ['savedUrl' => $this->defaultPhoto, 'originUrl' => $this->defaultPhoto];
        }
        $uPhotoUrl = $unsplashPhoto->urls['small'];
        preg_match('/fm=([^\&]*)/', $uPhotoUrl, $matches);
        $extension = $matches[1];
        preg_match('/^([^\w]*(?P<fName>\w+))/', $query, $matches);
        $savedUrl = $this->putContents($uPhotoUrl, $for, $matches['fName'], $extension);
        return ['savedUrl' => $savedUrl, 'originUrl' => $uPhotoUrl];
    }

    public function getPhotosFromUsp($query, $size = 10, $pageToken = 1){
        $result = $this->unsplashSearch::photos($query, $pageToken, $size);
        return $result->getResults();
    }

    public function putContents($orgUrl, $prefix, $name, $extends){
        $name = preg_replace('/[\?\/\#]\s/', '-', $name);
        $uniqueFilename = uniqid(str_replace(' ', '_', $prefix)).'_' . urlencode($name) . '.' . $extends;
        $savedUrl = 'assets/images/' . urlencode($uniqueFilename);
        file_put_contents($savedUrl, fopen($orgUrl, 'r'));
        return $savedUrl;
    }

    public function createSeriesAndPosts(){
        $data = $_POST;
        $file = $_FILES['cover_img'];

        if ($file['tmp_name'] !== ''){
            $unique_filename = uniqid('series').'_' . $file['name'];
            $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
            move_uploaded_file($file['tmp_name'], $destination);
            $savedUrl = 'assets/images/' . $unique_filename;
        }
        else{
            $urls = $this->getPhotoFromUnsplash($data['searchTerm'], 'series');
            $savedUrl = $urls['savedUrl'];
        }
        $sets = [
            'strSeries_title' => $data['title'],
            'intSeries_category' => $data['category'],
            'intSeries_frequency_ID' => 1,
            'strSeries_image' => $savedUrl,
            'boolSeries_isPublic' => $data['isPublic'],
            'available_days' => $data['availableDays'],
            'boolSeries_level' => $data['boolSeries_level']
        ];
        $insertedId = $this->seriesModel->insert($sets);
//        $this->model->joinSeries($insertedId);
        $metaPosts = parseStringToArray($data['metaPosts']);
        $youtubePosts = json_decode($data['youtube']);
        $podcastsPosts = json_decode($data['podcasts']);
        $blogsPosts = json_decode($data['blogs']);
        $recipesPosts = json_decode($data['recipes']);
        $ideaboxPosts = json_decode($data['ideabox']);
        $postPosts = json_decode($data['posts']);
        $rssbPostPosts = json_decode($data['rssbPosts']);

        foreach ($metaPosts as $postPost){
            $this->model->insertPost($insertedId, $postPost['title'], $postPost['body'], $postPost['summary'], $postPost['type'], $postPost['image'], $postPost['keyword']);
        }

        foreach ($youtubePosts as $youtubePost){
            $this->model->insertPost($insertedId, $youtubePost->title, $this->formatYoutubeById($youtubePost->body), $youtubePost->summary, $youtubePost->type, $youtubePost->image, $youtubePost->keyword);
        }
        foreach ($podcastsPosts as $post){
            $this->model->insertPost($insertedId, $post->title, $post->body, $post->summary, $post->type, $post->image, $post->keyword);
        }
        foreach ($blogsPosts as $post){
            if (\Helpers\isImage($post->image, $fname, $exs)){
                $savedUrl = $post->image;
            }
            else{
                preg_match('/fm=(?P<extension>[^\&]*)/', $post->image, $matches);
                $savedUrl = $this->putContents($post->image, 'blogs', '', $matches['extension']);
            }
            $this->model->insertPost($insertedId, $post->title, $post->body, $post->summary, $post->type, $savedUrl, $post->keyword);
        }
        foreach ($recipesPosts as $post){
            $recipe = $this->getFood2Fork($post->id);
            $body = $this->makePostBodyByRecipe($recipe);
            $this->model->insertPost($insertedId, $post->title, $body, $post->summary, $post->type, $post->image, $post->keyword);
        }
        foreach ($ideaboxPosts as $ideaboxPost){
            $postBody = $this->makePostBodyFromIdeabox($ideaboxPost);
            $this->model->insertPost($insertedId, $ideaboxPost->title, $postBody, $ideaboxPost->summary, $ideaboxPost->type, $ideaboxPost->image, $ideaboxPost->keyword);
        }
        foreach ($postPosts as $postPost){
            $this->model->insertPost($insertedId, $postPost->title, $postPost->body, $postPost->summary, $postPost->type, $postPost->image, $postPost->keyword);
        }
        foreach ($rssbPostPosts as $rssbPostPost){
            $this->model->insertPost($insertedId, $rssbPostPost->title, $rssbPostPost->body, $rssbPostPost->summary, $rssbPostPost->type, $rssbPostPost->image, $rssbPostPost->keyword);
        }
        $this->filterFirstCreate();
        echo json_encode(['status' => true, 'data' => $insertedId]);
        die();
    }

    public function makePostBodyByRecipe($recipe){
        $body = '<section class="recipe-body">';
        $body .= '<div><img src="'. $recipe->image_url .'"></div>';
        $body .= '<h4 style="margin-bottom: 0px;">Ingredients:</h4>';
        $body .= '<ul>';
        foreach ($recipe->ingredients as $ingredient){
            $body .= '<li>'. $ingredient .'</li>';
        }
        $body .= '</ul>';
        $body .= '<h5><lable>Publisher:</lable> <span class="publisher">'. $recipe->publisher .'</span></h5>';
        $body .= '<div><a href="'. $recipe->source_url .'" target="_blank">View Detail</a></div>';
        $body .= '</section>';
        return $body;
    }

    public function makePostBodyFromIdeabox($postData){
        $refLink = $postData->reference_link;
        $refText = $postData->reference_text;

        $isYoutube = preg_match('@^(https://www.youtube.com/[^/=]+[/=])(?<youtubeId>\w+)@', $refLink, $matches);
        if ($isYoutube){
            $youtubeId = $matches['youtubeId'];
            $refHtml = sprintf('<video class="afterglow" data-autoresize="fit" id="video_%s_%s" width="960" height="540" data-youtube-id="%s"></video>', uniqid(), $youtubeId, $youtubeId);
        }
        else{
            $refHtml = $refLink ? sprintf('<a href="%s">%s</a>', $refLink, $refText) : '';
        }
        $postBody = sprintf('<p><img src="%s"></p><div>%s</div><div>%s</div>', $postData->image, $postData->body, $refHtml);
        return $postBody;
    }

    public function createCategory(){
        $name = $_POST['name'];
        $urls = $this->getPhotoFromUnsplash($name, 'category');
        $newId = $this->model->insertCategory($name, $name, $urls['savedUrl'], $urls['originUrl']);
        echo json_encode(['status' => true, 'data' => $newId]);
        die();
    }

    public function filterFirstCreate(){
        $flgFirstCreate = $this->clientMetaModel->get(['meta_key' => 'first_series_create']);
        if (!$flgFirstCreate){
            $this->clientMetaModel->insert(['meta_key' => 'first_series_create']);
            $headers = htmlMailHeader();
            $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/congrats_creating_first_greyshirt.html');
            mail($_SESSION['guardian']['email'], 'Congratulations on creating your first greyshirt!', $msg, $headers);
        }
    }

}

$createSeriesHandle = new CreateSeries();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'search_posts_by_keyword':
            $createSeriesHandle->searchPostsByKeyword();
            break;
        case 'create_series_and_posts':
            $createSeriesHandle->createSeriesAndPosts();
            break;
        case 'create_category':
            $createSeriesHandle->createCategory();
            break;
        default:
            $createSeriesHandle->index();
            break;
    }
}
else{
    $createSeriesHandle->index();
//    $createSeriesHandle->test();
}