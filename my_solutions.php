<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 14.10.2018
 * Time: 12:40
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

use function Helpers\{
    getApiTypeFromUrl, htmlPurify, isAbsUrl, parseYoutubeUrl
};

class My_Solutions extends \Core\Controller_core
{
    private $model;
    private $categoryModel;
    private $subsModel;
    private $purchasedModel;
    private $trashModel;
    private $postsModel;
    private $seriesHandle;
    private $seriesModel;
    private $clientMetaModel;
    private $formFieldAnswerModel;
    private $formFieldsModel;
    private $postNotesModel;
    private $productsHandle;
    private $goalsHandle;
    private $eventsHandle;
    private $subsHandle;
    private $optionsModel;

    private $currentUser;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Userpage_m');
        $this->model = new \Models\Userpage_m();

        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Subscriptions_m');
        $this->subsModel = new \Models\api\Subscriptions_m();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Trash_m');
        $this->trashModel = new \Models\api\Trash_m();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();
        $this->seriesModel = $this->seriesHandle->model;

        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();

        $this->load->model('api_m/ClientMeta_m');
        $this->clientMetaModel = new \Models\api\ClientMeta_m();

        $this->load->model('api_m/FormFieldAnswer_m');
        $this->formFieldAnswerModel = new \Models\api\FormFieldAnswer_m();

        $this->load->model('api_m/FormField_m');
        $this->formFieldsModel = new \Models\api\FormField_m();

        $this->load->model('api_m/PostNotes_m');
        $this->postNotesModel = new \Models\api\PostNotes_m();

        $this->load->controller('Products_c');
        $this->productsHandle = new \Controllers\Products_c();

        $this->load->controller('Goals_c');
        $this->goalsHandle = new \Controllers\Goals_c();

        $this->load->controller('Events_c');
        $this->eventsHandle = new \Controllers\Events_c();

        $this->load->controller('Subscriptions_c');
        $this->subsHandle = new \Controllers\Subscriptions_c();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->currentUser = $_SESSION['client_ID'];
    }
    public function index() {

        $scrollTo = isset($_POST['scrollTo']) ? $_POST['scrollTo'] : (isset($_GET['scrollTo']) ? $_GET['scrollTo'] : false);
        switch ($scrollTo){
            case 'subscription':
                $focusId = isset($_POST['purchasedId']) ? $_POST['purchasedId'] : (isset($_GET['purchasedId']) ? $_GET['purchasedId'] : false);
                break;
            default:
                $focusId = false;
                break;
        }

        $firstVisited = $this->clientMetaModel->get(['meta_key' => 'first_userpage_visited']);
        $isFirstVisit = false;

        if (!$firstVisited) {
            $isFirstVisit = true;
            $this->clientMetaModel->insert(['meta_key' => 'first_userpage_visited', 'meta_value' => date('Y-m-d')]);
        }

        $purchasedSeries = $this->model->getAvailablePurchasedSeries();
        $availableSeries = [];

        foreach ($purchasedSeries as & $purchasedSery){
            $purchasedSery['formFields'] = $this->formFieldsModel->getRows(['intFormField_series_ID' => $purchasedSery['series_ID']]);

            $cnt = $this->subsModel->countSubscriptions(['intClientSubscription_purchased_ID' => $purchasedSery['purchased_ID'], 'intClientSubscriptions_finished' => 0]);
            if ($cnt < 100){
                $this->subsHandle->subscribeSeries(['intPost_series_ID' => $purchasedSery['series_ID'], 'intPost_parent' => 0], $purchasedSery['purchased_ID'], 100);
            }
            $subscription = $this->subsHandle->seekSubscription($purchasedSery);

            if ($subscription){
                $subscription = $this->stepNextToCanDisplay($purchasedSery, $subscription);
                $purchasedSery['intPurchased_seekParent'] = $subscription['intClientSubscriptions_parent'];
                $purchasedSery['intPurchased_seekOrder'] = $subscription['intClientSubscriptions_order'];
                $purchasedSery['subscription'] = $this->formatSubscription($subscription);
                array_push($availableSeries, $purchasedSery);
            }
            else{
                $sets = $this->subsHandle->setSeek($purchasedSery, ['intPurchased_seekParent' => 0, 'intPurchased_seekOrder' => 1]);
                $purchasedSery = array_merge($purchasedSery, $sets);
                $this->subsHandle->orderSubscriptions(['intClientSubscription_purchased_ID' => $purchasedSery['purchased_ID'], 'intClientSubscriptions_parent' => 0]);
                $subscription = $this->subsHandle->seekSubscription($purchasedSery);
                if ($subscription) {
                    $purchasedSery['subscription'] = $this->formatSubscription($subscription);
                    $purchasedSery = array_merge($purchasedSery, ['intPurchased_seekParent' => 0, 'intPurchased_seekOrder' => 1]);
                    array_push($availableSeries, $purchasedSery);
                }
            }
        }
        $events = $this->eventsHandle->getEvents();
        $today = date('Y-m-d');

        $goalsData = $this->goalsHandle->getGoalsDataByDate($today);

        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);

        $categories = [];
        $flg = [];
        foreach ($availableSeries as & $availableSery) {
            $availableSery['permission'] = $this->seriesHandle->getRole($availableSery);
            $availableSery['unReadPostsCount'] = $this->seriesHandle->unReadPostsCount($availableSery['series_ID']);
            $this->seriesHandle->filterViewRecord($availableSery['series_ID']);
            $this->seriesHandle->filterAnyViewRecord($availableSery['series_ID']);
            if (isset($flg[$availableSery['intSeries_category']])) {
                continue ;
            }
            $flg[$availableSery['intSeries_category']] = true;
            $categories[] = $this->categoryModel->get($availableSery['intSeries_category']);
        }

        $allCategories = [];
        if ( !count($availableSeries) ) {
            $allCategories = $this->categoryModel->getRows();
            foreach ($allCategories as & $category) {
                $sSeries_cnt = $this->seriesModel->count(['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 0]);
                $eSeries_cnt = $this->seriesModel->count(['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 1]);
                $category['boolSimplify'] = $sSeries_cnt ? true : false;
                $category['boolEnrich'] = $eSeries_cnt ? true : false;
                $category['series_count'] = $this->seriesModel->count(['intSeries_category' => $category['category_ID']]);
            }
        }

        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';

        $this->load->view('My_Solutions_v', ['series' => $availableSeries, 'events' => $events, 'goals' => $goalsData, 'scrollTo' => $scrollTo, 'focusId' => $focusId, 'isFirstVisit' => $isFirstVisit, 'formFieldAnswers' => $formFieldAnswers, 'categories' => $categories, 'allCategories' => $allCategories, 'stripeApi_pKey' => $stripeApi_pKey]);
    }

    private function stepNextToCanDisplay($sery, $subscription){
        $startDateCanDisplay = $this->getStartDateCanDisplayByFrequency($sery['intSeries_frequency_ID']);
        if (is_null($sery['intSeries_frequency_ID'])){
            return $subscription;
        }
        if (is_null($subscription['dtClientSubscription_datetoshow']) || $subscription['dtClientSubscription_datetoshow'] >= $startDateCanDisplay){
            return $subscription;
        }
        $nextSubscription = $this->stepNext($sery, $subscription, $sery['intSeries_frequency_ID'], 1);
        $sery['intPurchased_seekOrder'] = $nextSubscription['intClientSubscriptions_order'];
        $sery['intPurchased_seekParent'] = $nextSubscription['intClientSubscriptions_parent'];
        return $this->stepNextToCanDisplay($sery, $nextSubscription);
    }

    private function stepNext($purchased, $subscription, $frequency = 0, $finished = 0){
        if ($subscription){
            $sets['intClientSubscriptions_finished'] = $finished;
            $this->subsModel->update($subscription['clientsubscription_ID'], $sets);
            $prevDate = $subscription['dtClientSubscription_datetoshow'];
        }
        else{
            $prevDate = false;
        }
        $sets = [];
        switch ((int) $frequency){
            case 0:
                break;
            case 1:
                $sets['dtClientSubscription_datetoshow'] = $finished ? date('Y-m-d') : date('Y-m-d', strtotime($prevDate . '+1 day'));
                break;
            case 2:
                $sets['dtClientSubscription_datetoshow'] = $finished ? date('Y-m-d') : date('Y-m-d', strtotime($prevDate . '+7 day'));
                break;
            default:
                break;
        }

        $sets['intClientSubscriptions_finished'] = 0;

        $nextSubscription = $this->subsHandle->stepNext($purchased);
        if ($nextSubscription) {
            $this->subsModel->update($nextSubscription['clientsubscription_ID'], $sets);
            $nextSubscription = array_merge($nextSubscription, $sets);
            return $nextSubscription;
        }
        else {
            if ($purchased['intPurchased_seekParent'] == '0') {
                $purchased = $this->subsHandle->setSeek($purchased, ['intPurchased_seekOrder' => 1]);
                $seekSubscription = $this->subsHandle->seekSubscription($purchased);
                $this->subsModel->update($seekSubscription['clientsubscription_ID'], $sets);
                return array_merge($seekSubscription, $sets);
            }
            else {
                $purchased = $this->subsHandle->seekUpstairs($purchased);
                $purchased = $this->subsHandle->seekUpstairs($purchased);
                $seekSubscription = $this->subsHandle->seekSubscription($purchased);
                return $this->stepNext($purchased, $seekSubscription, $frequency);
            }
        }
    }

    public function stepPrev($purchased, $subscription, $frequency = 0){
        if ($subscription){
            $this->subsModel->update($subscription['clientsubscription_ID'], ['intClientSubscriptions_finished' => 1]);
        }

        $prevSubscription = $this->subsHandle->stepPrev($purchased);
        if ($prevSubscription) {
            $sets = [];
            switch ((int) $frequency){
                case 0:
                    break;
                case 1:
                    $sets['dtClientSubscription_datetoshow'] = date('Y-m-d');
                    break;
                case 2:
                    $sets['dtClientSubscription_datetoshow'] = date('Y-m-d');
                    break;
                default:
                    break;
            }
            $sets['intClientSubscriptions_finished'] = 0;

            $this->subsModel->update($prevSubscription['clientsubscription_ID'], $sets);
            $prevSubscription = array_merge($prevSubscription, $sets);
            return $prevSubscription;
        }
        else {
            if ($purchased['intPurchased_seekParent'] == '0') {
                $purchased = $this->subsHandle->setSeek($purchased, ['intPurchased_seekOrder' => 999999]);
            }
            else {
                $purchased = $this->subsHandle->seekUpstairs($purchased);
                $purchased = $this->subsHandle->seekUpstairs($purchased);
            }
            $seekSubscription = $this->subsHandle->seekSubscription($purchased);
            $sets = ['dtClientSubscription_datetoshow' => date('Y-m-d'), 'intClientSubscriptions_finished' => 0];
            $this->subsModel->update($seekSubscription['clientsubscription_ID'], $sets);
            return array_merge($seekSubscription, $sets);
        }
    }

    public function getStartDateCanDisplayByFrequency($frequency){
        $todayDate = date('Y-m-d');
        switch ((int) $frequency){
            case 0:
                return 0;
            case 1:
                return $todayDate;
            case 2:
                return date('Y-m-d', strtotime($todayDate . '-6 day'));
            default:
                return 0;
                break;
        }
    }

    private function formatSubscription($subscription){
        $subscription = $this->subsHandle->formatSubscription($subscription);
        $body = $subscription['strClientSubscription_body'];
        $subscription['origin_body'] = $body;
        switch ((int) $subscription['intClientSubscriptions_type']){
            case 0:
                $body = isAbsUrl($body) ? $body : BASE_URL . '/' . $body;
                break;
            case 2:
                $apiType = getApiTypeFromUrl($body);
                if ($apiType == 'youtube'){
                    $parse = parseYoutubeUrl($body);
//                    $body = $parse['id'];
                }
                $subscription['body_apiType'] = $apiType;
                break;
            case 8:
                $imgSection = substr($body, 0, strpos($body, "</section>"));
                $body = htmlPurify($imgSection);
                break;
            case 10:
                $body = $body;
                break;
            default:
                $subBody = trim(substr($body, 0, 480));
                $fltBody = stripslashes($subBody . '...');
                $body = htmlPurify($fltBody);
                break;
        }
        $subscription['strClientSubscription_body'] = $body;

        $subsCompletes = $this->model->getSubsCompletesBySubsId($subscription['clientsubscription_ID']);
        $subsCompletes = array_reverse($subsCompletes);
        $subscription['isSubsCompleted'] = $subsCompletes ? (int) $subsCompletes[0]['boolCompleted'] : 0;

        $favorite = $this->model->getFavoriteBySubsId($subscription['clientsubscription_ID']);
        $subscription['favorite'] = $favorite ? $favorite : false;

        if ($subscription['strClientSubscription_nodeType'] == 'menu') {
            if (!is_null($subscription['intClientSubscription_post_ID'])) {
                $post = $this->postsModel->get($subscription['intClientSubscription_post_ID']);
                $this->subsHandle->subscribeSeries(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['post_ID']], $subscription['intClientSubscription_purchased_ID']);
            }
            $where = [
                'intClientSubscription_purchased_ID' => $subscription['intClientSubscription_purchased_ID'],
                'intClientSubscriptions_parent' => $subscription['clientsubscription_ID']
            ];
            $paths = $this->subsHandle->getSubscriptions($where);
            $subscription['paths'] = $paths;
        }

        $where = [
            'postNote_user_ID' => $_SESSION['client_ID'],
            'postNote_subscription_ID' => $subscription['clientsubscription_ID']
        ];
        if ($subscription['intClientSubscription_post_ID']) {
            $where['postNote_post_ID'] = $subscription['intClientSubscription_post_ID'];
        }
        $subscription['postNotes'] = $this->postNotesModel->getRows_where_post_or_sub($where);

        $subscription['meta'] = [];
        $subscription['meta']['viewUri'] = rawurlencode(BASE_URL . '/view_blog?id=' . $subscription['intClientSubscription_post_ID'] . ($_SESSION['affiliate_id'] !== -1 ? '&affiliate_id=' . $_SESSION['affiliate_id'] : ''));
        $subscription['meta']['uriTitle'] = rawurlencode($subscription['strClientSubscription_title']);
        $metaDes = \Helpers\htmlPurify(substr($subscription['strClientSubscription_body'], 0, 150));
        $subscription['meta']['metaDes'] = str_replace('\'', '"', $metaDes);

        return $subscription;
    }
    public function ajax_stepNext(){
        $id = $_POST['id'];
        $purchasedId = $_POST['purchasedId'];
        $frequency = $_POST['frequency'];
        $subscription = $this->subsModel->get($id);
        $purchased = $this->purchasedModel->get($purchasedId);
        $nextSubscription = $this->stepNext($purchased, $subscription, $frequency, 1);
        $nextSubscription = $this->formatSubscription($nextSubscription);
        echo json_encode(['status' => true, 'data' => $nextSubscription]);
        die();
    }
    public function ajax_stepPrev(){
        $id = $_POST['id'];
        $purchasedId = $_POST['purchasedId'];
        $frequency = $_POST['frequency'];
        $subscription = $this->subsModel->get($id);
        $purchased = $this->purchasedModel->get($purchasedId);
        $prevSubscription = $this->stepPrev($purchased, $subscription, $frequency);
        $prevSubscription = $this->formatSubscription($prevSubscription);
        echo json_encode(['status' => true, 'data' => $prevSubscription]);
        die();
    }
    public function ajax_setSeek(){
        $purchasedId = $_POST['purchasedId'];
        $seekParent = $_POST['parentId'];
        $seekOrder = isset($_POST['order']) ? $_POST['order'] : 1;

        $subscription = $this->subsModel->get($seekParent);

        if (!is_null($subscription['intClientSubscription_post_ID'])){
            $purchased = $this->purchasedModel->get($purchasedId);
            $unReads = $this->subsHandle->subscribeSeries(['intPost_series_ID' => $purchased['intPurchased_series_ID'], 'intPost_parent' => $subscription['intClientSubscription_post_ID']], $purchasedId);
        }
        if ($this->subsModel->countSubscriptions(['intClientSubscriptions_parent' => $seekParent])) {
            $purchased = $this->subsHandle->setSeek($purchasedId, ['intPurchased_seekParent' => $seekParent, 'intPurchased_seekOrder' => $seekOrder]);
            $subscription = $this->subsHandle->seekSubscription($purchased);
            $subscription = $this->formatSubscription($subscription);
            echo json_encode(['status' => true, 'data' => $subscription]);
        }
        else {
            echo json_encode(['status' => false, 'data' => $subscription]);
        }
        die();
    }
    public function ajax_seekUpstairs(){
        $purchasedId = $_POST['purchasedId'];
        $purchased = $this->purchasedModel->get($purchasedId);
        if ($purchased['intPurchased_seekParent'] == '0') { echo json_encode(['status' => false]); die(); }
        else{
            $purchased = $this->subsHandle->seekUpstairs($purchased);
            $purchased = $this->subsHandle->seekUpstairs($purchased);
        }

        $seekSubscription = $this->subsHandle->seekSubscription($purchased);
        $sets = ['dtClientSubscription_datetoshow' => date('Y-m-d'), 'intClientSubscriptions_finished' => 0];
        $this->subsModel->update($seekSubscription['clientsubscription_ID'], $sets);
        $seekSubscription = array_merge($seekSubscription, $sets);
        $fSub = $this->formatSubscription($seekSubscription);
        echo json_encode(['status' => true, 'data' => $fSub]);
        die();
    }
    public function ajax_makeFavorite(){
        $id = $_POST['id'];
        $subscription = $this->subsModel->get($id);

        $sets = ['intFavorite_subscription_ID' => $id];

        if ($subscription['intClientSubscription_post_ID']){
            $post = $this->subsModel->getPost($subscription['intClientSubscription_post_ID']);
            $sets['intFavorite_post_ID'] = $subscription['intClientSubscription_post_ID'];
            $sets['intFavorite_series_ID'] = $post['intPost_series_ID'];
        }
        $favId = $this->model->insertFavorite($sets);
        $favorite = $this->model->getFavoriteBySubsId($id);
        echo json_encode(['status' => true, 'data' => $favorite]);
        die();
    }
    public function ajax_makeUnFavorite(){
        $id = $_POST['id'];
        $this->model->deleteFavorite($id);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_trashPost(){
        $id = $_POST['id'];
        $subs = $this->subsModel->get($id);
        $purchased = $this->purchasedModel->get($subs['intClientSubscription_purchased_ID']);
        $series = $this->seriesModel->get($purchased['intPurchased_series_ID']);
        $nextSubs = $this->stepNext($purchased, $subs, $series['intSeries_frequency_ID']);

        if ($subs['intClientSubscription_post_ID']){
            $post = $this->subsModel->getPost($subs['intClientSubscription_post_ID']);
            $sets = [
                'intTrashPost_post_ID' => $subs['intClientSubscription_post_ID'],
                'intTrashPost_series_ID' => $post['intPost_series_ID']
            ];
            $this->model->insertTrashPost($sets);
        }
        $this->subsModel->delete($id);

        if ($id != $nextSubs['clientsubscription_ID']){
            $nextSubs = $this->formatSubscription($nextSubs);
        }
        else{
            $nextSubs = false;
        }
        echo json_encode(['status' => true, 'data' => $nextSubs]);
        die();
    }
    public function ajax_removeSeries(){
        $id = $_POST['id'];
        $this->purchasedModel->delete(['intPurchased_client_ID' => $this->currentUser, 'intPurchased_series_ID' => $id]);
        $this->trashModel->insert(['intTrash_series_ID' => $id]);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_completeSubscription(){
        $id = $_POST['id'];
        $this->model->insertSubsComplete(['intSubscription_ID' => $id, 'boolCompleted' => 1]);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_unCompleteSubscription(){
        $id = $_POST['id'];
        $this->model->insertSubsComplete(['intSubscription_ID' => $id, 'boolCompleted' => 0]);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getTodayExperiences() {
        $purchasedSeries = $this->model->getAvailablePurchasedSeries();
        $availableSeries = [];

        foreach ($purchasedSeries as & $purchasedSery){
            $purchasedSery['formFields'] = $this->formFieldsModel->getRows(['intFormField_series_ID' => $purchasedSery['series_ID']]);

            $cnt = $this->subsModel->countSubscriptions(['intClientSubscription_purchased_ID' => $purchasedSery['purchased_ID'], 'intClientSubscriptions_finished' => 0]);
            if ($cnt < 100){
                $this->subsHandle->subscribeSeries(['intPost_series_ID' => $purchasedSery['series_ID'], 'intPost_parent' => 0], $purchasedSery['purchased_ID'], 100);
            }
            $subscription = $this->subsHandle->seekSubscription($purchasedSery);

            if ($subscription){
                $subscription = $this->stepNextToCanDisplay($purchasedSery, $subscription);
                $purchasedSery['intPurchased_seekParent'] = $subscription['intClientSubscriptions_parent'];
                $purchasedSery['intPurchased_seekOrder'] = $subscription['intClientSubscriptions_order'];
                $purchasedSery['subscription'] = $this->formatSubscription($subscription);
                array_push($availableSeries, $purchasedSery);
            }
            else{
                $sets = $this->subsHandle->setSeek($purchasedSery, ['intPurchased_seekParent' => 0, 'intPurchased_seekOrder' => 1]);
                $purchasedSery = array_merge($purchasedSery, $sets);
                $this->subsHandle->orderSubscriptions(['intClientSubscription_purchased_ID' => $purchasedSery['purchased_ID'], 'intClientSubscriptions_parent' => 0]);
                $subscription = $this->subsHandle->seekSubscription($purchasedSery);
                if ($subscription) {
                    $purchasedSery['subscription'] = $this->formatSubscription($subscription);
                    $purchasedSery = array_merge($purchasedSery, ['intPurchased_seekParent' => 0, 'intPurchased_seekOrder' => 1]);
                    array_push($availableSeries, $purchasedSery);
                }
            }
        }

        $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);

        $categories = [];
        $flg = [];
        foreach ($availableSeries as $availableSery) {
            if (isset($flg[$availableSery['intSeries_category']])) {
                continue ;
            }
            $flg[$availableSery['intSeries_category']] = true;
            $categories[] = $this->categoryModel->get($availableSery['intSeries_category']);
        }
        $data = ['series' => $availableSeries, 'formFieldAnswers' => $formFieldAnswers, 'categories' => $categories];
        echo json_encode(['status' => true, 'data' => $data]);
        die();
    }
}

$handle = new My_Solutions();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'step_next':
            $handle->ajax_stepNext();
            break;
        case 'step_prev':
            $handle->ajax_stepPrev();
            break;
        case 'set_seek':
            $handle->ajax_setSeek();
            break;
        case 'seek_upstairs':
            $handle->ajax_seekUpstairs();
            break;
        case 'make_favorite':
            $handle->ajax_makeFavorite();
            break;
        case 'make_unFavorite':
            $handle->ajax_makeUnFavorite();
            break;
        case 'trash_post':
            $handle->ajax_trashPost();
            break;
        case 'remove_series':
            $handle->ajax_removeSeries();
            break;
        case 'complete_subscription':
            $handle->ajax_completeSubscription();
            break;
        case 'unComplete_subscription':
            $handle->ajax_unCompleteSubscription();
            break;
        case 'get_today_experiences':
            $handle->ajax_getTodayExperiences();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
//    $handle->test();
}