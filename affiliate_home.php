<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 14.09.2018
 * Time: 13:06
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Affiliate_Home extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
    }
    public function index() {
        $this->load->view('Affiliate_Home_v');
    }
}
$handle = new Affiliate_Home();
$handle->index();