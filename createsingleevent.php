<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 04:19
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/CreateEvent_m.php';

use Core\Controller_core;
use Models\{CreateEvent_m};

class CreateSingleEvent extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new CreateEvent_m();
    }

    public function index(){
        $this->load->view('CreateSingleEvent_v');
    }
}

$handle = new CreateSingleEvent();

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case '':
            break;
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}