<?php

namespace Models;

require_once _ROOTPATH_ . '/config.php';
require_once _ROOTPATH_ . '/dbconnect.php';

use function Helpers\utf8Encode;
use PDO;

class EditSeries_m
{
    public function __construct()
    {
        global $myconnection;
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function allSeries(){
        $query = 'SELECT * FROM `tblseries`';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute();
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
    }
    public function allPurchased(){
        $query = 'SELECT * FROM `tblpurchased`';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute();
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function getSeriesById($id){
        $query = 'SELECT * FROM `tblseries` WHERE series_ID = :id AND (intSeries_client_ID = :client_id OR boolSeries_isPublic = 1)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':id' => $id, ':client_id' => $this->currentUser]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getRandomSeries(){
        $query = 'SELECT * FROM `tblseries` WHERE (intSeries_client_ID = :client_id OR boolSeries_isPublic = 1) AND series_ID IN (SELECT intPurchased_series_ID FROM tblpurchased WHERE intPurchased_client_ID = :currentUser)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':client_id' => $this->currentUser, ':currentUser' => $this->currentUser]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getPostsBySeriesId($seriesId){
        $query = 'SELECT post_ID, strPost_title, strPost_featuredimage, intPost_order FROM tblpost WHERE intPost_series_ID = :seriesId ORDER BY intPost_order, post_ID';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':seriesId' => $seriesId]);
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function getPurchaseBySeriesId($seriesId){
        $query = 'SELECT * FROM tblpurchased WHERE intPurchased_client_ID = :cid AND intPurchased_series_ID = :sid AND boolPurchased_active = 1';
        $stmt = $this->db->prepare( $query );
        $stmt->execute([':cid' => $this->currentUser, ':sid' => $seriesId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row ? $row : false;
    }
    public function getClientIdBySeriesID($seriesId){
        $query = 'SELECT `intSeries_client_ID` FROM `tblseries` WHERE `series_ID` = :seriesId';
        $stmt = $this->db->prepare( $query );
        $stmt->execute([':seriesId' => $seriesId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['intSeries_client_ID'];
    }
    public function setSeriesAvailableDays($seriesId, $availableDays){
        $query = 'UPDATE tblseries SET available_days = :availableDays WHERE series_ID = :seriesId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':availableDays' => $availableDays, ':seriesId' => $seriesId]);
        return $rlt;
    }
    public function setPurchaseAvailableDays($purchaseId, $availableDays){
        $query = 'UPDATE tblpurchased SET available_days = :availableDays WHERE purchased_ID = :purchaseId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':availableDays' => $availableDays, ':purchaseId' => $purchaseId]);
        return $rlt;
    }
    public function setPostOrder($postId, $newOrder){
        $query = 'UPDATE tblpost SET intPost_order = :newOrder WHERE post_ID = :postId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':newOrder' => $newOrder, ':postId' => $postId] );
        return $rlt ? true : false;
    }
    public function joinSeries($seriesId){
        $query = "INSERT INTO tblpurchased (intPurchased_client_ID, intPurchased_series_ID, dtPurchased_startdate, boolPurchased_active, intPurchased_clientorder) VALUES(:cid, :series_id, NOW(), 1, 1)";
        $stmt = $this->db->prepare($query);
        $rlt = $stmt->execute(array(':cid' => $this->currentUser, ':series_id' => $seriesId));
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getTrashPostById($postId){
        $query = 'SELECT * FROM `tbltrashpost` WHERE intTrashPost_post_ID = :postId AND intTrashPost_client_ID = :currentUser';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':postId' => $postId, ':currentUser' => $this->currentUser] );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function copyPostToSubscription($postId, $purchasedId, $isOrderSame = false){
        if ($isOrderSame){
            $post = $this->getPostById($postId);
            $nextOrder = $post['intPost_order'];
        }
        else{
            $nextOrder = $this->getMaxSubscriptionOrderByPurchasedId($purchasedId) + 1;
        }
        $query = 'INSERT INTO `tblclientsubscription` (`intClientSubscription_purchased_ID`, `intClientSubscription_post_ID`, `strClientSubscription_body`, `strClientSubscription_notes`, `clientsubscription_ID`, `intClientSubscriptions_finished`, `strClientSubscription_title`, `dtClientSubscription_datetoshow`, intClientSubscriptions_order) VALUES (:purchased_ID, :post_ID, NULL, NULL, NULL, 0, NULL, NULL, :next_order)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':purchased_ID' => $purchasedId, ':post_ID' => $postId, ':next_order' => $nextOrder] );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function insertSubscription($data){
        $purchasedId = $data['purchasedId'];
        $postId = isset($data['postId']) ? $data['postId'] : null;
        $title = isset($data['title']) ? utf8Encode($data['title']) : null;
        $body = isset($data['body']) ? utf8Encode($data['body']) : null;
        $image = isset($data['image']) ? $data['image'] : null;
        $type = isset($data['type']) ? $data['type'] : null;
        $finished = isset($data['finished']) ? $data['finished'] : 0;
        $order = isset($data['order']) ? $data['order'] : false;

        if (!$order){
            $order = 1 + $this->getMaxSubscriptionOrderByPurchasedId($purchasedId);
        }

        $query = 'INSERT INTO `tblclientsubscription` (`intClientSubscription_purchased_ID`, `intClientSubscription_post_ID`, strClientSubscription_title, strClientSubscription_body, strClientSubscription_image, intClientSubscriptions_type, `strClientSubscription_notes`, `intClientSubscriptions_finished`, `dtClientSubscription_datetoshow`, intClientSubscriptions_order) VALUES (:purchased_ID, :post_ID, :title, :body, :image, :ctype, NULL, :finished, NULL, :plus_order)';
        $stmt = $this->db->prepare( $query );

        $rlt = $stmt->execute(array(':purchased_ID' => $purchasedId, ':post_ID' => $postId, ':image' => $image, ':title' => $title, ':body' => $body, ':ctype' => $type, ':finished' => $finished, ':plus_order' => $order));
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getUnsubsPosts($purchasedId, $seriesId = false){
        if (!$purchasedId){
            $purchased = $this->getPurchaseBySeriesId($seriesId);
            $purchasedId = $purchased['intClientSubscription_purchased_ID'];
        }
        if (!$seriesId){
            $purchased = $this->getPurchased($purchasedId);
            $seriesId = $purchased['intPurchased_series_ID'];
        }
        $query = 'SELECT * FROM tblpost WHERE intPost_series_ID = :seriesId AND post_ID NOT IN (SELECT intClientSubscription_post_ID FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchasedId) AND post_ID NOT IN (SELECT intTrashPost_post_ID FROM tbltrashpost WHERE intTrashPost_series_ID = :seriesId1 AND intTrashPost_client_ID = :currentUser)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':seriesId' => $seriesId, ':seriesId1' => $seriesId, ':purchasedId' => $purchasedId, ':currentUser' => $this->currentUser]);
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
    }
    public function getPurchased($id){
        $query = 'SELECT * FROM tblpurchased WHERE purchased_ID = :purchasedId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':purchasedId' => $id] );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getMaxSubscriptionOrderByPurchasedId($purchasedId){
        $query = 'SELECT MAX(intClientSubscriptions_order) AS max_order FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':purchased_id' => $purchasedId) );
        $row = $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
        return $row ? $row['max_order'] : 0;
    }
    public function getSubscriptionByPurchasedIdPostId($purchasedId, $postId){
        $query = 'SELECT * FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchasedId AND intClientSubscription_post_ID = :postId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':purchasedId' => $purchasedId, ':postId' => $postId]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getSubscriptionsByPurchasedId($purchasedId){
        $query = 'SELECT clientsubscription_ID, intClientSubscription_purchased_ID, intClientSubscription_post_ID, intClientSubscriptions_finished, strClientSubscription_title, strClientSubscription_image, intClientSubscriptions_type, intClientSubscriptions_order FROM `tblclientsubscription` WHERE intClientSubscription_purchased_ID = :pid AND (intClientSubscription_post_ID IS NULL OR intClientSubscription_post_ID NOT IN (SELECT intTrashPost_post_ID FROM tbltrashpost WHERE intTrashPost_client_ID = :currentUser)) ORDER BY intClientSubscriptions_order, clientsubscription_ID';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':pid' => $purchasedId, ':currentUser' => $this->currentUser]);
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
    }
    public function getPostById($id){
        $query = 'SELECT * FROM tblpost WHERE post_ID = :postId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':postId' => $id] );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function setSubscriptionOrder($id, $order){
        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_order = :order WHERE clientsubscription_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':order' => $order, ':id' => $id] );
        return $rlt ? true : false;
    }
    public function deletePost($id){
        $query = 'DELETE FROM tblpost WHERE post_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':id' => $id]);
        return $rlt ? true : false;
    }
    public function deleteSubscriptionsByPostId($postId){
        $query = 'DELETE FROM `tblclientsubscription` WHERE intClientSubscription_post_ID = :postId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':postId' => $postId]);
        return $rlt ? true : false;
    }
    public function deleteSubscription($id){
        $query = 'DELETE FROM `tblclientsubscription` WHERE clientsubscription_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':id' => $id]);
        return $rlt ? true : false;
    }
    public function trashPost($purchased_id, $post_id){
        $query = 'DELETE FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id AND intClientSubscription_post_ID = :post_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':purchased_id' => $purchased_id, ':post_id' => $post_id) );

        $query = 'INSERT INTO tblposttrash (posttrash_ID, intPosttrash_purchased_ID, intPosttrash_post_ID, dtPosttrash_datetrashed) VALUES (NULL, :purchased_id, :post_id, CURRENT_TIMESTAMP)';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':purchased_id' => $purchased_id, ':post_id' => $post_id) );
    }
    public function getSubscription($id){
        $query = 'SELECT * FROM `tblclientsubscription` WHERE clientsubscription_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':id' => $id]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getPostMaxOrderBySeriesId($seriesId){
        $query = 'SELECT MAX(intPost_order) AS maxOrder FROM `tblpost` WHERE intPost_series_ID = :seriesId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':seriesId' => $seriesId]);
        $row = $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
        return $row ? $row['maxOrder'] : 0;
    }
    public function insertPost($seriesId, $title, $body, $summary, $type, $image, $keyword = '', $order = false){
        if ($order){
            $nextOrder = $order;
        }
        else{
            $maxOrder = $this->getPostMaxOrderBySeriesId($seriesId);
            $nextOrder = $maxOrder + 1;
        }

        $query = 'INSERT INTO tblpost (intPost_series_ID, strPost_title, strPost_body, strPost_summary, intPost_type, strPost_featuredimage, intPost_order, intPost_miniseries_ID, strPost_keywords) VALUES (:sid, :title, :body, :summary, :pType, :image, :nextOrder, 3, :kwd)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':sid' => $seriesId,
            ':title' => utf8Encode($title),
            ':body' => utf8Encode($body),
            ':summary' => utf8Encode($summary),
            ':pType' => $type,
            ':image' => $image,
            ':kwd' => $keyword,
            ':nextOrder' => $nextOrder
        ]);
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function setSeriesImage($seriesId, $imgUrl){
        $query = 'UPDATE tblseries SET strSeries_image = :img WHERE series_ID = :seriesId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':img' => $imgUrl, ':seriesId' => $seriesId]);
        return $rlt;
    }
}