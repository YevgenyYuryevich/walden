<?php

namespace Models;

require_once _ROOTPATH_ . '/config.php';
require_once _ROOTPATH_ . '/dbconnect.php';

use PDO;

class ExploreInterests_m
{
    public function __construct()
    {
        $rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
        global $myconnection;
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//        $this->currentUser = $_SESSION['client_ID'];
    }
    public function getCategoryById($id){
        $query = 'SELECT * FROM tblcategories WHERE category_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':id' => $id]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getRandomCategory(){
        $query = 'SELECT * FROM tblcategories';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute();
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getSeriesByCategoryId($categoryId){
        $query = 'SELECT * FROM tblseries WHERE intSeries_category = :ctgId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':ctgId' => $categoryId] );
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
    }
}