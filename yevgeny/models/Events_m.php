<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.03.2018
 * Time: 17:47
 */

namespace Models;

require_once __DIR__ . '/Database_m.php';

use PDO;

class Events_m
{
    private $TNAME;
    public function __construct()
    {
        $this->db = new Database_m();
        $this->TNAME = $this->db::EVENT_TNAME;
        $this->currentUser = $_SESSION['client_ID'];
    }

    public function createCancelledEvent($event_id, $date){
        $query = "INSERT INTO `event_instance_exception` (`event_id`, `start_date`, `is_rescheduled`, `is_cancelled`, `created_by`) VALUES (:event_id, :start_date, 0, 1, :created_by)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':start_date' => $date, ':created_by' => $this->currentUser) );
        return $this->db->lastInsertId();
    }

    public function createRescheduledEvent($event_id, $data){
        $query = "INSERT INTO `event_instance_exception` (`event_id`, `event_title`, `event_description`, `event_location`, `start_date`, `start_time`, `created_by`) VALUES (:event_id, :event_title, :event_description, :event_location, :start_date, :start_time, :created_by)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':event_title' => $data['event_title'], ':event_description' => $data['event_description'], ':event_location' => $data['event_location'], ':start_date' => $data['start_date'], ':start_time' => $data['start_time'], ':created_by' => $this->currentUser) );
        return $this->db->lastInsertId();
    }

    public function getRescheduledEventById($id){
        $query = 'SELECT * FROM event_instance_exception WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function createRecurringPattern($event_id, $recurring_pattern_data, $origin_event_data){

        switch ($recurring_pattern_data['recurring_type']){
            case 'd':
                $query = "INSERT INTO `recurring_pattern` (`id`, `event_id`, `recurring_type`, `separation_count`, `days_of_week`) VALUES (NULL, :event_id, :recurring_type, :separation_count, :days_of_week)";
                $stmt = $this->db->prepare(  $query );
                $stmt->execute( array(':event_id' => $event_id, ':recurring_type' => $recurring_pattern_data['recurring_type'], ':separation_count' => $recurring_pattern_data['separation_count'], ':days_of_week' => $recurring_pattern_data['days_of_week']) );
                $new_recurring_id = $this->db->lastInsertId();
                break;
            case 'w':
                $query = "INSERT INTO `recurring_pattern` (`id`, `event_id`, `recurring_type`, `separation_count`) VALUES (NULL, :event_id, :recurring_type, :separation_count)";
                $stmt = $this->db->prepare(  $query );
                $stmt->execute( array(':event_id' => $event_id, ':recurring_type' => $recurring_pattern_data['recurring_type'], ':separation_count' => $recurring_pattern_data['separation_count']) );
                $new_recurring_id = $this->db->lastInsertId();
                break;
        }


        if (isset($recurring_pattern_data['additional_event_dates'])){
            foreach ($recurring_pattern_data['additional_event_dates'] as $date){
                $rescheduled_data = array(
                    'event_title' => $origin_event_data['event_title'],
                    'event_description' => $origin_event_data['event_description'],
                    'event_location' => $origin_event_data['event_location'],
                    'start_date' => $date,
                    'start_time' => $origin_event_data['start_time']);
                $new_rescheduled_id = $this->createRescheduledEvent($event_id, $rescheduled_data);
            }
        }

        if (isset($recurring_pattern_data['omit_dates'])){
            foreach ($recurring_pattern_data['omit_dates'] as $date){
                $new_cancelled_id = $this->createCancelledEvent($event_id, $date);
            }
        }

        return $new_recurring_id;
    }

    public function updateRecurringPattern($event_id, $recurring_pattern_data){

    }

    public function createEvent($data){
        global $current_user;
        $query = "INSERT INTO `events` (`id`, `event_title`, `event_description`, `event_location`, `start_date`, `start_time`, `is_recurring`, `created_by`, `created_datetime`) VALUES (NULL, :event_title, :event_description, :event_location, :start_date, :start_time, :is_recurring, :client, CURRENT_TIMESTAMP)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':event_title' => $data['event_title'], ':event_description' => $data['event_description'], ':event_location' => $data['event_location'], ':start_date' => $data['start_date'], ':start_time' => $data['start_time'], ':is_recurring' => $data['is_recurring'] === 'true' ? 1 : 0, ':client' => $current_user));
        $new_id = $this->db->lastInsertId();
        return $new_id;
    }
    public function getEvents(){
//        $date_now = date('Y-m-d');
//        $time_now = date("H:i:s");
        $date_now = '1990-01-01';
        $time_now = date("H:i:s");

        $query = "SELECT * FROM `events` WHERE created_by = :client_id AND (start_date >= :date_now OR is_recurring = 1)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':date_now' => $date_now, ':client_id' => $this->currentUser) );
        $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $events;
    }
    public function getRecurringPattern($event_id){
        $query = 'SELECT * FROM `recurring_pattern` WHERE event_id = :event_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        $recurring_pattern = $stmt->fetch(PDO::FETCH_ASSOC);
        return $recurring_pattern;
    }
    public function getRescheduledEvents($event_id){
        $query = 'SELECT * FROM event_instance_exception WHERE event_id = :event_id AND is_cancelled = 0';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getCancelledEvents($event_id){
        $query = 'SELECT * FROM event_instance_exception WHERE event_id = :event_id AND is_cancelled = 1';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getCancelledEventById($id){
        $query = 'SELECT * FROM event_instance_exception WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function getEventById($id){
        $query = "SELECT * FROM `events` WHERE id = :id";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function deleteRescheduledEvent($id){
        $query = 'DELETE FROM `event_instance_exception` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
    public function deleteEvent($id){
        $query = 'DELETE FROM `events` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
    public function getEventImagesByEventId($event_id){
        $query = 'SELECT * FROM event_images WHERE event_id = :event_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function insert($sets){
        $defaults = [
            'created_by' => $this->currentUser
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function insertInstanceException($sets){
        $defaults = [
            'created_by' => $this->currentUser
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->db::EVENT_INS_ECP_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function insertRecurringPattern($sets){
        $this->db->from($this->db::EVENT_RC_PT_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function insertEventImage($sets){
        $this->db->from($this->db::EVENT_IMAGE_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function deleteEventImage($id){
        $query = 'DELETE FROM event_images WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
    public function updateEvent($event_id, $data){
        if ($data['start_date'] == null){
            $query = 'UPDATE `events` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_time`=:start_time,`created_by`=:client_id WHERE id = :event_id';
            $stmt = $this->db->prepare( $query );
            return $stmt->execute( array(
                ':event_id' => $event_id,
                ':event_title' => $data['event_title'],
                ':event_description' => $data['event_description'],
                ':event_location' => $data['event_location'],
                ':start_time' => $data['start_time'],
                ':client_id' => $this->currentUser
            ) );
        }
        else{
            $query = 'UPDATE `events` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_date`=:start_date,`start_time`=:start_time,`created_by`=:client_id WHERE id = :event_id';
            $stmt = $this->db->prepare( $query );
            return $stmt->execute( array(
                ':event_id' => $event_id,
                ':event_title' => $data['event_title'],
                ':event_description' => $data['event_description'],
                ':event_location' => $data['event_location'],
                ':start_date' => $data['start_date'],
                ':start_time' => $data['start_time'],
                ':client_id' => $this->currentUser
            ) );
        }
    }
    public function updateReschduledInstance($id, $data){
        $query = 'UPDATE `event_instance_exception` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_date`=:start_date,`start_time`=:start_time,`created_by`=:client_id WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(
            ':id' => $id,
            ':event_title' => $data['event_title'],
            ':event_description' => $data['event_description'],
            ':event_location' => $data['event_location'],
            ':start_date' => $data['start_date'],
            ':start_time' => $data['start_time'],
            ':client_id' => $this->currentUser
        ) );
    }
    public function updateRescheduledInstancesByEventId($event_id, $data){
        $query = 'UPDATE `event_instance_exception` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_time`=:start_time,`created_by`=:client_id WHERE event_id = :event_id AND is_cancelled = 0';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(
            ':event_id' => $event_id,
            ':event_title' => $data['event_title'],
            ':event_description' => $data['event_description'],
            ':event_location' => $data['event_location'],
            ':start_time' => $data['start_time'],
            ':client_id' => $this->currentUser
        ) );
    }
    public function insertEventFavorite($event_id, $is_rescheduled, $start_date, $instance_id){
        if ($is_rescheduled == '1'){
            $query = 'INSERT INTO `event_favorites`(`event_id`, `client_id`, `is_rescheduled`, `instance_id`) VALUES (:event_id, :client_id, :is_rescheduled, :instance_id)';
            $stmt = $this->db->prepare( $query );
            $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->currentUser, ':is_rescheduled' => $is_rescheduled, ':instance_id' => $instance_id) );
            return $this->db->lastInsertId();
        }
        else{
            $query = 'INSERT INTO `event_favorites`(`event_id`, `client_id`, `is_rescheduled`, `start_date`) VALUES (:event_id, :client_id, :is_rescheduled, :start_date)';
            $stmt = $this->db->prepare( $query );
            $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->currentUser, ':is_rescheduled' => $is_rescheduled, ':start_date' => $start_date) );
            return $this->db->lastInsertId();
        }
    }
    public function getFavoritesByEventId($event_id){
        $query = 'SELECT * FROM `event_favorites` WHERE event_id = :event_id AND client_id = :client_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->currentUser) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getFavoriteById($id){
        $query = 'SELECT * FROM `event_favorites` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function deleteEventFavorite($id){
        $query = 'DELETE FROM `event_favorites` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
}