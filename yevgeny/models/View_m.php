<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.02.2018
 * Time: 07:23
 */

namespace Models;

use function Helpers\rootPath;

require_once rootPath() . '/yevgeny/models/Database_m.php';

use core\Database_core;

class View_m
{
    public function __construct()
    {
        $this->db = new Database_core();
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function getPostData($id){

        $this->db->select(['post_ID', 'intPost_type', 'dtPost_due', 'strPost_title', 'strPost_body', 'strPost_summary', 'strPost_keywords', 'intPost_days', 'strPost_featuredimage', 'intPost_order', 'intPost_series_ID']);
        $this->db->select(['strSeries_description', 'strSeries_title', 'strSeries_writernotes', 'series_ID']);
        $this->db->select('strCategory_name');
        $this->db->from($this->db::POST_TNAME);
        $this->db->join('tblseries', 'tblpost.intPost_series_ID = tblseries.series_ID');
        $this->db->join('tblcategories', 'tblseries.intSeries_category=tblcategories.category_ID');
        $this->db->where('post_ID', $id);
        return $this->db->get(true);
    }
    public function allPosts(){
        $this->db->from($this->db::POST_TNAME);
        return $this->db->get();
    }
    public function updatePost($where, $sets){
        if (!is_array($where)){
            $where = ['post_ID' => $where];
        }
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        $this->db->set($sets);
        return $this->db->update();
    }
    public function getRandPostData(){

        $this->db->select(['post_ID', 'intPost_type', 'dtPost_due', 'strPost_title', 'strPost_body', 'strPost_summary', 'strPost_keywords', 'intPost_days', 'strPost_featuredimage']);
        $this->db->select(['strSeriesFrequency_name', 'strSeries_description', 'strSeries_title', 'strSeries_writernotes', 'series_ID']);
        $this->db->select('strCategory_name');
        $this->db->from($this->db::POST_TNAME);
        $this->db->join('tblseries', 'tblpost.intPost_series_ID = tblseries.series_ID');
        $this->db->join('tblseriesfrequency', 'tblseries.intSeries_frequency_ID = tblseriesfrequency.seriesfrequency_ID');
        $this->db->join('tblcategories', 'tblseries.intSeries_category=tblcategories.category_ID');
        return $this->db->get(true);
    }
    public function getPurchasedBySeriesId($seriesId){
        $this->db->select('*');
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->where('intPurchased_series_ID', $seriesId);
        return $this->db->get(true);
    }
    public function getSubscriptionByPurchasedIdPostId($purchasedId, $postId){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->where('intClientSubscription_post_ID', $postId);
        $row = $this->db->get(true);
        if (!$row){ return $row; }
        $subsCompletes = $this->getSubsCompletesBySubsId($row['clientsubscription_ID']);
        $subsCompletes = array_reverse($subsCompletes);
        $row['isSubsCompleted'] = $subsCompletes ? $subsCompletes[0]['boolCompleted'] : 0;
        return $row;
    }

    public function getSubsCompletesBySubsId($subscriptionId){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->where('intSubscription_ID', $subscriptionId);
        $this->db->orderBy('created_datetime');
        return $this->db->get();
    }

    public function insertSubsComplete($subscriptionId, $completed = 0, $dateTime = false){
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->set('intSubscription_ID', $subscriptionId);
        $this->db->set('boolCompleted', $completed);
        if ($dateTime){ $this->db->set('created_datetime', $dateTime); }
        return $this->db->insert();
    }

    public function joinSeries($id){
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->set('intPurchased_client_ID', $this->currentUser);
        $this->db->set('intPurchased_series_ID', $id);
        $this->db->set('boolPurchased_active', 1);
        $this->db->set('intPurchased_clientorder', 1);
        return $this->db->insert();
    }

    public function copyPostToSubscription($postId, $purchasedId){
        $maxOrder = $this->getSubscriptionMaxOrderByPurchasedId($purchasedId);

        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->set('intClientSubscription_post_ID', $postId);
        $this->db->set('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->set('intClientSubscriptions_order', $maxOrder + 1);
        return $this->db->insert();
    }
    public function getSubscriptionMaxOrderByPurchasedId($purchasedId){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->orderBy('intClientSubscriptions_order', 'DESC');
        $subscription = $this->db->get(true);
        return $subscription ? $subscription['intClientSubscriptions_order'] : 0;
    }
    public function setSubscription($id, $sets){
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->set($sets);
        $this->db->where('clientsubscription_ID', $id);
        return $this->db->update();
    }

    public function getSubscription($id){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where('clientsubscription_ID', $id);
        return $this->db->get(true);
    }
    public function getRandomSubscription(){
        return true;
    }
}