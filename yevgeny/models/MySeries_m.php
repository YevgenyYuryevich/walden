<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 26.02.2018
 * Time: 18:28
 */
namespace Models;

use function Helpers\aOrb;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

class MySeries_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = $_SESSION['client_ID'];
        $this->defaultImg = 'assets/images/beautifulideas.jpg';
    }
    public function getUnJoinedSeries(){
        $this->db->from($this->db::SERIES_TNAME);
        $this->db->notInGroupStart('series_ID');
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->select('intPurchased_series_ID');
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        $this->db->groupEnd();
        $this->db->where('intSeries_client_ID', $this->currentUser);
        return $this->db->get();
    }
    public function getUnSubsPosts($seriesId, $purchasedId){
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('intPost_series_ID', $seriesId);
        $this->db->notInGroupStart('post_ID');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->select('intClientSubscription_post_ID');
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->groupEnd();
        return $this->db->get();
    }
    public function getPurchasedSeries(){
        $this->db->select('*');
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->join($this->db::SERIES_TNAME, $this->db::PURCHASED_TNAME . '.intPurchased_series_ID = ' . $this->db::SERIES_TNAME . '.series_ID');
        $this->db->where('intSeries_client_ID', $this->currentUser);
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        $this->db->where('boolPurchased_active', 1);
        return $this->db->get();
    }
    public function getSubsByPurchasedId($purchasedId){
        $subs = $this->db->select('*')
            ->from($this->db::SUBSCRIPTION_TNAME)
            ->where('intClientSubscription_purchased_ID', $purchasedId)
            ->get();
        foreach ($subs as & $sub){
            $sub = $this->formatSubscription($sub);
            unset($sub['strClientSubscription_body']);
        }
        return $subs;
    }
    public function formatSubscription($subscription){
        if (is_null($subscription['intClientSubscription_post_ID'])){
            return $subscription;
        }
        $post = $this->getPost($subscription['intClientSubscription_post_ID']);
        $subscription['strClientSubscription_body'] = aOrb($subscription['strClientSubscription_body'], $post['strPost_body']);
        $subscription['strClientSubscription_title'] = aOrb($subscription['strClientSubscription_title'], $post['strPost_title']);
        $subscription['strClientSubscription_image'] = aOrb($subscription['strClientSubscription_image'], $post['strPost_featuredimage']);
        $subscription['intClientSubscriptions_type'] = aOrb($subscription['intClientSubscriptions_type'], $post['intPost_type']);
        return $subscription;
    }
    public function getPost($id){
        $this->db->select('*');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('post_ID', $id);
        return $this->db->get(true);
    }
    public function test(){
        $this->db->select('*');
        $this->db->where('post_ID !=', 10);
        $this->db->where(['post_ID <' => 12]);
        $this->db->where('intPost_series_ID = 1');
        return $this->db->get();
    }
}