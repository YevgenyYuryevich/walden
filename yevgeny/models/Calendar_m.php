<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.02.2018
 * Time: 15:35
 */

namespace Models;
use function Helpers\aOrb;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';


class Calendar_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = $_SESSION['client_ID'];
        $this->defaultImg = 'assets/images/beautifulideas.jpg';
    }
    public function getUnSubsPosts($seriesId, $purchasedId){
        $this->db->select('post_ID');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('intPost_series_ID', $seriesId);
        $this->db->notInGroupStart('post_ID');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->select('intClientSubscription_post_ID');
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->groupEnd();
        return $this->db->get();
    }
    public function getPurchasedSeries(){
        $this->db->select('*');
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->join($this->db::SERIES_TNAME, $this->db::PURCHASED_TNAME . '.intPurchased_series_ID = ' . $this->db::SERIES_TNAME . '.series_ID');
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        $this->db->where('boolPurchased_active', 1);
        return $this->db->get();
    }
    public function getSubsByPurchasedId($purchasedId){
        $subs = $this->db->select('*')
            ->from($this->db::SUBSCRIPTION_TNAME)
            ->where('intClientSubscription_purchased_ID', $purchasedId)
            ->where('intClientSubscriptions_finished', 0)
            ->orderBy('intClientSubscriptions_order')
            ->limit(50)
            ->get();
        foreach ($subs as & $sub){
            $sub = $this->formatSubscription($sub);
            unset($sub['strClientSubscription_body']);
        }
        return $subs;
    }
    public function getSubscriptionsFromSeek($purchasedId, $seek = 1){
        $subs = $this->db->from($this->db::SUBSCRIPTION_TNAME)
            ->select('intClientSubscription_purchased_ID')
            ->select('intClientSubscription_post_ID')
            ->select('clientsubscription_ID')
            ->select('intClientSubscriptions_finished')
            ->select('strClientSubscription_title')
            ->select('dtClientSubscription_datetoshow')
            ->select('strClientSubscription_image')
            ->select('intClientSubscriptions_type')
            ->select('intClientSubscriptions_order')
            ->where('intClientSubscription_purchased_ID', $purchasedId)
            ->where('intClientSubscriptions_order >=', $seek)
            ->orderBy('intClientSubscriptions_order')
            ->limit(50)
            ->get();
        return $subs;
    }
    public function formatSubscription($subscription){
        if (is_null($subscription['intClientSubscription_post_ID'])){
            return $subscription;
        }
        $post = $this->getPost($subscription['intClientSubscription_post_ID']);
        $subscription['strClientSubscription_body'] = aOrb($subscription['strClientSubscription_body'], $post['strPost_body']);
        $subscription['strClientSubscription_title'] = aOrb($subscription['strClientSubscription_title'], $post['strPost_title']);
        $subscription['strClientSubscription_image'] = aOrb($subscription['strClientSubscription_image'], $post['strPost_featuredimage']);
        $subscription['intClientSubscriptions_type'] = aOrb($subscription['intClientSubscriptions_type'], $post['intPost_type']);
        return $subscription;
    }
    public function getPost($id){
        $this->db->select('*');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('post_ID', $id);
        return $this->db->get(true);
    }
    public function test(){
        $stmt = $this->db->query("SELECT post_ID FROM tblpost WHERE intPost_series_ID = '20' AND post_ID NOT IN (SELECT intClientSubscription_post_ID FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = '17')");
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}