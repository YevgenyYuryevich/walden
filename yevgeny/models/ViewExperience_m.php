<?php


namespace Models;

require_once _ROOTPATH_ . '/config.php';
require_once _ROOTPATH_ . '/dbconnect.php';

use PDO;


class ViewExperience_m
{
    public function __construct()
    {
        global $myconnection;
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function getSeriesById($id){
        $query = 'SELECT * FROM `tblseries` WHERE series_ID = :id AND (intSeries_client_ID = :client_id OR boolSeries_isPublic = 1)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':id' => $id, ':client_id' => $this->currentUser]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getRandomSeries(){
        $query = 'SELECT * FROM `tblseries` WHERE (intSeries_client_ID = :client_id OR boolSeries_isPublic = 1) AND boolSeries_approved = 1';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':client_id' => $this->currentUser]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getPostsBySeriesId($seriesId){
        $query = 'SELECT post_ID, strPost_title, strPost_featuredimage FROM tblpost WHERE intPost_series_ID = :seriesId AND intPost_miniseries_order = 0 ORDER BY intPost_order, post_ID';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':seriesId' => $seriesId]);

        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function joinSeries($seriesId){
        $query = "INSERT INTO tblpurchased (intPurchased_client_ID, intPurchased_series_ID, dtPurchased_startdate, boolPurchased_active, intPurchased_clientorder) VALUES(:cid, :series_id, NOW(), 1, 1)";
        $stmt = $this->db->prepare($query);
        $rlt = $stmt->execute(array(':cid' => $this->currentUser, ':series_id' => $seriesId));
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function unJoinSeries($seriesId){
        $query = 'DELETE FROM tblpurchased WHERE intPurchased_client_ID = :cid AND intPurchased_series_ID = :sid';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':cid' => $this->currentUser, ':sid' => $seriesId]);
        return $rlt;
    }
    public function getPurchasedIdBySeriesId($seriesId){
        $query = 'SELECT * FROM tblpurchased WHERE intPurchased_client_ID = :cid AND intPurchased_series_ID = :sid';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':cid' => $this->currentUser, ':sid' => $seriesId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row ? $row['purchased_ID'] : false;
    }
}