<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.02.2018
 * Time: 10:59
 */

namespace Models;

use function Helpers\aOrb;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

class Favorites_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = $_SESSION['client_ID'];
        $this->defaultImg = 'assets/images/beautifulideas.jpg';
    }
    public function getPurchasedSeries(){
        $this->db->select('*');
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->join($this->db::SERIES_TNAME, $this->db::PURCHASED_TNAME . '.intPurchased_series_ID = ' . $this->db::SERIES_TNAME . '.series_ID');
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        $this->db->where('boolPurchased_active', 1);
        return $this->db->get();
    }
    public function getFavoriteSubsByPurchasedId($purchasedId){
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->join($this->db::FAVORITES_TNAME, $this->db::SUBSCRIPTION_TNAME . '.clientsubscription_ID = ' . $this->db::FAVORITES_TNAME . '.intFavorite_subscription_ID');
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $favoriteSubs = $this->db->get();
        foreach ($favoriteSubs as & $favoriteSub){
            $favoriteSub = $this->formatSubscription($favoriteSub);
        }
        return $favoriteSubs;
    }
    public function formatSubscription($subscription){
        if (is_null($subscription['intClientSubscription_post_ID'])){
            return $subscription;
        }
        $post = $this->getPost($subscription['intClientSubscription_post_ID']);
        $subscription['strClientSubscription_body'] = aOrb($subscription['strClientSubscription_body'], $post['strPost_body']);
        $subscription['strClientSubscription_title'] = aOrb($subscription['strClientSubscription_title'], $post['strPost_title']);
        $subscription['strClientSubscription_image'] = aOrb($subscription['strClientSubscription_image'], $post['strPost_featuredimage']);
        $subscription['intClientSubscriptions_type'] = aOrb($subscription['intClientSubscriptions_type'], $post['intPost_type']);
        return $subscription;
    }
    public function getPost($id){
        $this->db->select('*');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('post_ID', $id);
        return $this->db->get(true);
    }
    public function insertFavorite($sets){
        $defaults = ['intFavorite_client_ID' => $this->currentUser];
        $sets = array_merge($defaults, $sets);
        $this->db->set($sets);
        $this->db->from($this->db::FAVORITES_TNAME);
        return $this->db->insert();
    }
    public function deleteFavorite($id){
        $this->db->from($this->db::FAVORITES_TNAME);
        $this->db->where('favorite_ID', $id);
        return $this->db->delete();
    }
}