<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 22:47
 */

namespace Models\api;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

use function Helpers\utf8Encode;
use Models\Database_m;

class Posts_m
{
    private $TNAME;
    public function __construct()
    {
        $this->db = new Database_m();
        $this->TNAME = $this->db::POST_TNAME;
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function insert($sets){
        $seriesId = $sets['intPost_series_ID'];
        $parentId = isset($sets['intPost_parent']) ? $sets['intPost_parent'] : 0;
        $maxOrder = $this->getMaxOrder(['intPost_series_ID' => $seriesId, 'intPost_parent' => $parentId]);
        $defaults = [
            'intPost_order' => $maxOrder + 1,
            'intPost_miniseries_ID' => 0,
            'intPost_parent' => $parentId,
        ];
        $sets = array_merge($defaults, $sets);
//        $sets = utf8Encode($sets);
        $this->db->set($sets);
        $this->db->from($this->db::POST_TNAME);
        return $this->db->insert();
    }
    public function get($where, $select = '*', $orderBy = ['intPost_order', 'post_ID']){
        if (!is_array($where)){
            $where = ['post_ID' => $where];
        }
        $this->db->select($select);
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        $this->db->orderBy($orderBy);
        return $this->db->get(true);
    }
    public function delete($where){
        if (!is_array($where)){
            $where = ['post_ID' => $where];
        }
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function update($sets, $where){
        if (!is_array($where)){
            $where = ['post_ID' => $where];
        }
        return $this->db->from($this->TNAME)
            ->where($where)
            ->set($sets)
            ->update();
    }
    public function getMaxOrder($where){
        $this->db->select('MAX(intPost_order) as max_order');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        $rlt = $this->db->get(true);
        return $rlt ? (int) $rlt['max_order'] : 0;
    }
    public function getPosts($where = [], $select = '*', $orderby = ['intPost_order', 'post_ID'], $limit = false){
        if (!is_array($where)){
            $where = ['post_ID' => $where];
        }
        $this->db->select($select);
        $this->db->from(($this->db::POST_TNAME));
        $this->db->where($where);
        $this->db->orderBy($orderby);
        if ($limit){
            if (is_array($limit)) {
                $this->db->limit($limit['size'], $limit['offset']);
            }
            else {
                $this->db->limit($limit);
            }
        }
        return $this->db->get();
    }
    public function countPosts($where){
        $this->db->select('count(post_ID) AS cnt');
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $rlt = $this->db->get(true);
        return $rlt ? (int) $rlt['cnt'] : 0;
    }
    public function deleteFavorites($where){
        $this->db->from($this->db::FAVORITES_TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function sum($select, $where = []) {
        $select = 'SUM(' . $select . ') AS sumV';
        $this->db->select($select);
        $this->db->where($where);
        $this->db->from($this->TNAME);
        $row = $this->db->get(true);
        return $row ? (int) $row['sumV'] : 0;
    }
    public function search($keyword, $size, $pageToken = 0, $where = []){
        $this->db->select('post_ID');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->like('strPost_keywords', $keyword);
        $this->db->orLike('strPost_title', $keyword);
        $this->db->groupEnd();
        $this->db->limit($size, $pageToken);
        $rows = $this->db->get();
        foreach ($rows as & $row){
            $this->db->select('*');
            $this->db->from($this->db::POST_TNAME);
            $this->db->where('post_ID', $row['post_ID']);
            $row = $this->db->get(true);
        }
        return $rows;
    }
    public function isSearched($keyword, $where = []) {
        $this->db->select('post_ID');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->like('strPost_keywords', $keyword);
        $this->db->orLike('strPost_title', $keyword);
        $this->db->groupEnd();
        $rows = $this->db->get();
        return $rows ? true : false;
    }
}