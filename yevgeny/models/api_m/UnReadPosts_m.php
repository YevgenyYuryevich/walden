<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.01.2019
 * Time: 16:49
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class UnReadPosts_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::UNREAD_POSTS_TNAME;
        $this->PRIMARY_KEY = 'id';
    }
}