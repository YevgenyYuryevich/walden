<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.11.2018
 * Time: 13:45
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class Series_Invitations_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::SERIES_INVITATIONS_TNAME;
        $this->PRIMARY_KEY = 'series_invitation_ID';
    }
}