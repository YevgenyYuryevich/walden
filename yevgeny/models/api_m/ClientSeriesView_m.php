<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.06.2018
 * Time: 06:05
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class ClientSeriesView_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::CSVIEW_TNAME;
        $this->PRIMARY_KEY = 'clientSeriesView_ID';
    }
}