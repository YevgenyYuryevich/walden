<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.06.2018
 * Time: 08:41
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class FormField_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::FORMFIELD_TNAME;
        $this->PRIMARY_KEY = 'formField_ID';
    }
}