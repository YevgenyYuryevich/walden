<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 22:56
 */

namespace Models\api;

require_once __DIR__ . '/../../core/Model_core.php';
require_once __DIR__ . '/../../helpers/funcs.php';

use Core\Model_Core;
use function Helpers\utf8Encode;
use function Helpers\aOrb;

class Subscriptions_m extends Model_Core
{
    private $currentUser;
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::SUBSCRIPTION_TNAME;
        $this->PRIMARY_KEY = 'clientsubscription_ID';
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function get($where = [], $select = '*', $orderby = ['intClientSubscriptions_order', 'clientsubscription_ID'], $limit = false){
        return parent::get($where, $select, $orderby, $limit);
    }
    public function insert($sets){

        $defaults = [];

        $purchasedId = $sets['intClientSubscription_purchased_ID'];
        $parentId = isset($sets['intClientSubscriptions_parent']) ? $sets['intClientSubscriptions_parent'] : 0;
        if (!isset($sets['intClientSubscriptions_order'])) {
            $maxOrder = $this->getMaxOrder(['intClientSubscription_purchased_ID' => $purchasedId, 'intClientSubscriptions_parent' => $parentId]);
            $defaults['intClientSubscriptions_order'] = $maxOrder + 1;
        }
        if (!isset($sets['intClientSubscription_post_ID'])) {
            $defaults['strClientSubscription_nodeType'] = 'post';
        }
        $sets = array_merge($defaults, $sets);
        $sets = utf8Encode($sets);
        return parent::insert($sets);
    }
    public function getMaxOrder($where){
        $this->db->select('MAX(intClientSubscriptions_order) as max_order');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where($where);
        $rlt = $this->db->get(true);
        $order = $rlt ? $rlt['max_order'] : 0;
        return $order ? $order : 0;
    }
    public function getSubscriptions($where = [], $select = '*', $orderby = ['intClientSubscriptions_order', 'clientsubscription_ID'], $limit = false){
        if (!is_array($where)) {
            $where = ['clientsubscription_ID' => $where];
        }
        $this->db->select($select);
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where($where);
        $this->db->orderBy($orderby);
        if ($limit){
            $this->db->limit($limit);
        }
        return $this->db->get();
    }
    public function getPurchasedBySeriesId($seriesId){
        $this->db->select('*');
        $this->db->from($this->db::PURCHASED_TNAME);
        $this->db->where('intPurchased_series_ID', $seriesId);
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        return $this->db->get(true);
    }
    public function getSubscriptionsByPurchasedIdPostId($purchasedId, $postId){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->where('intClientSubscription_post_ID', $postId);
        return $this->db->get();
    }
    public function insertTrashPost($sets){
        $defaults = [
            'intTrashPost_client_ID' => $this->currentUser,
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->set($sets);
        $this->db->from($this->db::TRASHPOST_TNAME);
        return $this->db->insert();
    }
    public function formatSubscription($subscription){
        if (!isset($subscription['intClientSubscription_post_ID']) || is_null($subscription['intClientSubscription_post_ID'])){
            return $subscription;
        }
        $post = $this->getPost($subscription['intClientSubscription_post_ID']);

        $keyPair = [
            'strClientSubscription_title' => 'strPost_title',
            'strClientSubscription_body' => 'strPost_body',
            'strClientSubscription_image' => 'strPost_featuredimage',
            'intClientSubscriptions_type' => 'intPost_type',
            'strClientSubscription_nodeType' => 'strPost_nodeType',
        ];

        foreach ($subscription as $key => $value){
            if ( isset($keyPair[$key]) ) {
                $subscription[$key] = aOrb($value, $post[$keyPair[$key]]);
            }
        }

        return $subscription;
    }
    public function getPost($id){
        $this->db->select('*');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('post_ID', $id);
        return $this->db->get(true);
    }
    public function countSubscriptions($where){
        $this->db->select('count(clientsubscription_ID) AS cnt');
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $rlt = $this->db->get(true);
        return $rlt ? (int) $rlt['cnt'] : 0;
    }
    public function deleteSubsCompletes($where){
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function deleteFavorites($where){
        $this->db->from($this->db::FAVORITES_TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function emptyTable(){
        $this->db->query('TRUNCATE ' . $this->TNAME);
    }
}