<?php


namespace Models\api;


use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class StripePlans_m extends Model_Core
{
    public function __construct($database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::STRIPE_PLANS;
        $this->PRIMARY_KEY = 'plan_ID';
    }
}