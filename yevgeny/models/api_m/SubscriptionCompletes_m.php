<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.05.2018
 * Time: 07:01
 */

namespace models\api_m;


use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class SubscriptionCompletes_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::SUBSCPLT_TNAME;
        $this->PRIMARY_KEY = 'subscriptionComplete_ID';
    }
}