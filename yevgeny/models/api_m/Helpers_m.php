<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

namespace Models\api;
use Models\Database_m;

require_once __DIR__ . '/../Database_m.php';

class Helpers_m
{
    private $TNAME;
    public function __construct()
    {
        $this->db = new Database_m();
        $this->TNAME = $this->db::HELPERS_TNAME;
    }
    public function randWhere($where){
        if (!is_array($where)){
            $where = ['strHelper_key' => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->orderBy('RAND()');
        return $this->db->get(true);
    }
}