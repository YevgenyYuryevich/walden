<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

namespace Models\api;
use core\Database_core;
use Models\Database_m;
use function Helpers\filterUnsetArr;

require_once __DIR__ . '/../../core/Database_core.php';

class Goals_m
{
    private $TNAME;

    public function __construct()
    {
        $this->db = new Database_core();
        $this->TNAME = $this->db::GOALS_TNAME;
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function getGoals($where){
        $defaults = [
            'intGoal_client_ID' => $this->currentUser
        ];
        $where = array_merge($defaults, $where);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->orderBy('intGoal_order');
        return $this->db->get();
    }
    public function getRows($where = [], $selects = '*', $orderBy = [], $limit = false, $offset = 0){
        $defaults = [
            'intGoal_client_ID' => $this->currentUser
        ];
        $where = array_merge($defaults, $where);
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        foreach ($where as $key => $value) {
            if (is_array($value)) {
                $this->db->groupStart();
                foreach ($value as $v) {
                    $this->db->orWhere($key, $v);
                }
                $this->db->groupEnd();
            }
            else {
                $this->db->where($key, $value);
            }
        }
        $this->db->orderBy($orderBy);
        if ($limit){
            $this->db->limit($limit, $offset);
        }
        return $this->db->get();
    }
    public function countWhere($where){
        $this->db->select('count(*) AS cnt');
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $rlt = $this->db->get(true);
        return $rlt ? $rlt['cnt'] : 0;
    }
    public function insert($sets){
        $date = date('Y-m-d');
        $defaults = [
            'intGoal_client_ID' => $this->currentUser,
            'dtGoal_date' => $date
        ];
        $sets = array_merge($defaults, $sets);
        $maxOrder = $this->maxOrderByDate($sets['dtGoal_date']);
        $sets['intGoal_order'] = $maxOrder + 1;

        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function get($where = []){
        if (!is_array($where)){
            $where = ['goal_ID' => $where];
        }
        $defaults = [
            'intGoal_client_ID' => $this->currentUser
        ];
        $where = array_merge($defaults, $where);

        if ($where['intGoal_client_ID'] == -1){
            unset($where['intGoal_client_ID']);
        }

        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get(true);
    }
    public function delete($where){
        if (!is_array($where)){
            $where = ['goal_ID' => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function update($sets, $where = []){
        $defWhere = [
            'intGoal_client_ID' => $this->currentUser
        ];
        if (!is_array($where)){
            $where = ['goal_ID' => $where];
        }
        $where = array_merge($defWhere, $where);
        filterUnsetArr($where);
        $this->db->set($sets);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->update();
    }
    public function randWhere($where){
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->orderBy('RAND()');
        $this->db->limit(1);
        return $this->db->get(true);
    }
    public function maxOrderByDate($date = false){
        $date = $date ? $date : date('Y-m-d');
        $this->db->select('MAX(intGoal_order) AS maxOrder');
        $this->db->from($this->TNAME);
        $this->db->where('intGoal_client_ID', $this->currentUser);
//        $this->db->where('dtGoal_date', $date);
        $row = $this->db->get(true);
        return $row ? $row['maxOrder'] : 0;
    }
    public function countMainGoals($where = []){
        $defWhere = [
            'dtGoal_date' => date('Y-m-d'),
            'intGoal_client_ID' => $this->currentUser,
        ];
        $where = array_merge($defWhere, $where);
        filterUnsetArr($where);

        $this->db->select('count(*) AS cnt');
        $this->db->from($this->TNAME);
        $this->db->notIn('intGoal_type', [4, 5]);
        $this->db->where($where);
        $rlt = $this->db->get(true);
        return $rlt ? (int) $rlt['cnt'] : 0;
    }
}