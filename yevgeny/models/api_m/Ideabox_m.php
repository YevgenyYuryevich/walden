<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.02.2018
 * Time: 01:17
 */

namespace Models\api;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

use Models\Database_m;

class Ideabox_m
{
    private $TNAME;
    public function __construct()
    {
        $this->db = new Database_m();
        $this->TNAME = $this->db::IDEABOX_TNAME;
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function delete($id){
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('ideabox_ID', $id);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        return $this->db->delete();
    }
    public function insert($sets){
        $p = isset($sets['intIdeaBox_subcategory']) ? $sets['intIdeaBox_subcategory'] : 0;
        $maxOrder = $this->getMaxOrderByParentId($p);
        $defaults = [
            'intIdeaBox_contractor_ID' => $this->currentUser,
            'intIdeaBox_fromclient' => $this->currentUser,
            'intIdeaBox_series_ID' => 0,
            'intIdeaBox_subcategory' => 0,
            'intIdeaBox_visible' => 1,
            'intIdeaBox_order' => $maxOrder + 1,
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function update($sets, $where){
        if (!is_array($where)){
            $where = ['ideabox_ID' => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        $this->db->where($where);
        return $this->db->update();
    }
    public function get($where = []){
        if (!is_array($where)){
            $where = ['ideabox_ID' => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get(true);
    }
    public function getMaxOrderByParentId($parentId){
        $this->db->select('MAX(intIdeaBox_order) as max_order');
        $this->db->from($this->TNAME);
        $this->db->where('intIdeaBox_subcategory', $parentId);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        $rlt = $this->db->get(true);
        return $rlt ? $rlt['max_order'] : 0;
    }
}