<?php


namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class CheckoutSession_Series_m extends Model_Core
{
    public function __construct($database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::CHECKOUT_SESSION_SERIES;
        $this->PRIMARY_KEY = 'session_id';
    }
}
