<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 26.02.2018
 * Time: 19:03
 */

namespace Models\api;

require_once __DIR__ . '/../../core/Model_core.php';

use Core\Model_Core;

class Purchased_m extends Model_Core
{
    private $currentUser;
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::PURCHASED_TNAME;
        $this->PRIMARY_KEY = 'purchased_ID';
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function insert($sets){
        $default = [
            'boolPurchased_active' => 1,
            'intPurchased_clientorder' => 1,
            'intPurchased_client_ID' => $this->currentUser
        ];
        $sets = array_merge($default, $sets);
        if ($sets['intPurchased_client_ID'] == -1){
            return 0;
        }
        return parent::insert($sets);
    }
    public function get($where = [], $selects = '*', $orderBy = [], $limit = false){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $defWhere = ['intPurchased_client_ID' => $this->currentUser];
        $where = array_merge($defWhere, $where);
        return parent::get($where, $selects, $orderBy, $limit);
    }
}