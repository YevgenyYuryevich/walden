<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

namespace Models\api;

require_once __DIR__ . '/../Database_m.php';

use Models\Database_m;

class TrashPosts_m
{
    private $TNAME;
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
        $this->TNAME = $this->db::TRASHPOST_TNAME;
    }
    public function getWhere($where, $select = '*'){
        $defWhere = [
            'intTrashPost_client_ID' => $this->currentUser
        ];
        $where = array_merge($defWhere, $where);
        $this->db->select($select);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get();
    }
}