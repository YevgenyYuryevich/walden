<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.03.2018
 * Time: 21:54
 */

namespace Models\api;

require_once __DIR__ . '/../Database_m.php';

use Models\Database_m;

class Trash_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function insert($sets){
        $defaults = [
            'intTrash_client_ID' => $this->currentUser
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->set($sets);
        $this->db->from($this->db::TRASHSERIES_TNAME);
        return $this->db->insert();
    }
    public function delete($where){

    }
}