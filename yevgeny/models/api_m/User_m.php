<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.03.2018
 * Time: 22:41
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';


class User_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct('guardian');
        $this->TNAME = $this->db::USERS_TNAME;
        $this->PRIMARY_KEY = 'id';
    }
    public function getUsers($where = []){
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get();
    }
    public function search($keyword, $size = 100, $pageToken = 0, $where = []){
        $this->db->select('*');
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->like('id', $keyword);
        $this->db->orLike('username', $keyword);
        $this->db->orLike('email', $keyword);
        $this->db->orLike('f_name', $keyword);
        $this->db->orLike('phone', $keyword);
        $this->db->groupEnd();
        $this->db->limit($size, $pageToken);
        $rows = $this->db->get();
        return $rows;
    }
}