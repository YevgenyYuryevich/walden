<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.03.2018
 * Time: 05:42
 */

namespace Models\api;

require_once __DIR__ . '/../Database_m.php';
use Models\Database_m;

class ClientMeta_m
{
    private $TNAME;
    public function __construct()
    {
        $this->db = new Database_m();
        $this->TNAME = $this->db::CLIENT_META_TNAME;
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function get($where){
        if (!is_array($where)){
            $where = ['clientMeta_id' => $where];
        }
        $defWhere = [
            'client_id' => $this->currentUser
        ];
        $where = array_merge($defWhere, $where);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get(true);
    }
    public function insert($sets){
        $defaults = [
            'client_id' => $this->currentUser,
            'meta_value' => 1
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
}