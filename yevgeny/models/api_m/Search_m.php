<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.02.2018
 * Time: 21:09
 */

namespace Models\api;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

use function Helpers\utf8Encode;
use Models\Database_m;

class Search_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function test(){
    }
    public function searchSeries($keyword, $size = 50, $pageToken = 0, $where = []){
        $this->db->select('*');
        $this->db->from($this->db::SERIES_TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->like('strSeries_title', $keyword);
        $this->db->orLike('strSeries_description', $keyword);
        $this->db->groupEnd();
        $this->db->limit($size, $pageToken);
        $searchedRows = $this->db->get();
        $needMore = $size - count($searchedRows);
        if ($needMore == 0){
            return $searchedRows;
        }
        $this->db->select('*');
        $this->db->from($this->db::SERIES_TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->notLike('strSeries_title', $keyword);
        $this->db->notLike('strSeries_description', $keyword);
        $this->db->groupEnd();
        $rows = $this->db->get();
        foreach ($rows as $row){
            $searchedPosts = $this->searchPosts($keyword, 1, 0, ['intPost_series_ID' => $row['series_ID']]);
            if ($searchedPosts){
                array_push($searchedRows, $row);
                $needMore--;
            }
            if ($needMore == 0) break;
        }
        return $searchedRows;
    }
    public function searchIdeaBox($keyword, $size = 10, $pageToken = 0){
        $this->db->select('*');
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        $this->db->groupStart()
            ->like('strIdeaBox_title', $keyword)
            ->orLike('strIdeaBox_idea', $keyword)
            ->groupEnd();
        $this->db->limit($size, $pageToken);
        return $this->db->get();
    }
    public function searchPosts($keyword, $size, $pageToken = 0, $where = []){
        $this->db->select('post_ID');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->like('strPost_keywords', $keyword);
        $this->db->orLike('strPost_title', $keyword);
        $this->db->groupEnd();
        $this->db->limit($size, $pageToken);
        $rows = $this->db->get();
        foreach ($rows as & $row){
            $this->db->select('*');
            $this->db->from($this->db::POST_TNAME);
            $this->db->where('post_ID', $row['post_ID']);
            $row = $this->db->get(true);
        }
        return $rows;
    }
    public function searchRssbPosts($keyword, $size, $pageToken = 0){
        $this->db->select('*');
        $this->db->from($this->db::RSSPOST_TNAME);
        $this->db->like('strRSSBlogPosts_title', $keyword);
        $this->db->orLike('strRSSBlogPosts_keywords', $keyword);
        $this->db->limit($size, $pageToken);
        return $this->db->get();
    }
}