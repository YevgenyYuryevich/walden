<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 27.11.2018
 * Time: 16:19
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class Series_Owners_m extends Model_Core
{
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::SERIES_OWNERS_TNAME;
        $this->PRIMARY_KEY = 'seriesOwner_ID';
    }
}