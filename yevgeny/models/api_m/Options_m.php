<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 11.05.2018
 * Time: 08:01
 */

namespace Models\api;


use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class Options_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::OPTION_TNAME;
        $this->PRIMARY_KEY = 'option_ID';
    }
    public function getOption($key) {
        $row = $this->get(['strOption_name' => $key]);
        return $row ? $row['strOption_value'] : 'undefined';
    }
}