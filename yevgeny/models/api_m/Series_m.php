<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 20:29
 */

namespace Models\api;

require_once __DIR__ . '/../../core/Model_core.php';

use Core\Model_Core;

class Series_m extends Model_Core
{
    public $currentUser;
    public function __construct()
    {
        parent::__construct();
        $this->TNAME = $this->db::SERIES_TNAME;
        $this->PRIMARY_KEY = 'series_ID';
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }

    public function getPurchasedSeries()
    {
        $this->db->select('*');
        $this->db->from($this->db::SERIES_TNAME);
        $this->db->join($this->db::PURCHASED_TNAME, $this->db::SERIES_TNAME . '.series_ID = ' . $this->db::PURCHASED_TNAME . '.intPurchased_series_ID');
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        return $this->db->get();
    }
    public function insert($sets){
        $defaults = [
            'intSeries_client_ID' => $this->currentUser,
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function get($where = [], $selects = '*', $orderBy = [], $limit = false){
        if (!is_array($where)){
            $where = ['series_ID' => $where];
        }
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get(true);
    }

    public function getSeries($where = [], $select = '*'){
        if (!is_array($where)){
            $where = ['series_ID' => $where];
        }
        $this->db->select($select);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->get();
    }
    public function delete($where){
        if (!is_array($where)){
            $where = ['series_ID' => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function search($keyword, $size, $offset, $where = []) {
        $this->db->select('*');
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->orLike('strSeries_title', $keyword);
        $this->db->like('strSeries_description', $keyword);
        $this->db->limit($size, $offset);
        $rows = $this->db->get();
        return $rows;
    }
    public function isSearched($keyword, $where = []) {
        $this->db->select('series_ID');
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $this->db->groupStart();
        $this->db->like('strSeries_title', $keyword);
        $this->db->orLike('strSeries_description', $keyword);
        $this->db->groupEnd();
        $rows = $this->db->get();
        return $rows ? true : false;
    }
}