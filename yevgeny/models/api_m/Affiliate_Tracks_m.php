<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.09.2018
 * Time: 03:40
 */

namespace Models\api;

use Core\Model_Core;

require_once __DIR__ . '/../../core/Model_core.php';

class Affiliate_Tracks_m extends Model_Core
{
    public function __construct(string $database = 'greyshirt')
    {
        parent::__construct($database);
        $this->TNAME = $this->db::AFFILIATE_TRACKS_TNAME;
        $this->PRIMARY_KEY = 'affiliate_track_ID';
    }
}