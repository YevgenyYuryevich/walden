<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.03.2018
 * Time: 16:17
 */

namespace Models;
require_once __DIR__ . '/Database_m.php';

class Userpage_m
{
    const likeDays = ['Mon' => '1______', 'Tue' => '_1_____', 'Wed' => '__1____', 'Thu' => '___1___', 'Fri' => '____1__', 'Sat' => '_____1_', 'Sun' => '______1'];

    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function getAvailablePurchasedSeries(){
        $today = date('D');
        $this->db->select('*');
        $this->db->select($this->db::PURCHASED_TNAME . '.available_days AS available_days');
        $this->db->from($this->db::SERIES_TNAME);
        $this->db->join($this->db::PURCHASED_TNAME, $this->db::SERIES_TNAME . '.series_ID = ' . $this->db::PURCHASED_TNAME . '.intPurchased_series_ID');
        $this->db->where('intPurchased_client_ID', $this->currentUser);
        $this->db->where('boolPurchased_active', 1);
        $this->db->like($this->db::PURCHASED_TNAME . '.available_days', $this::likeDays[$today]);
        return $this->db->get();
    }
    public function getFirstSubscriptionByPurchasedId($purchasedId){
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->orderBy('intClientSubscriptions_order');
        return $this->db->get(true);
    }
    public function getSubsCompletesBySubsId($subscriptionId){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->where('intSubscription_ID', $subscriptionId);
        $this->db->orderBy('created_datetime');
        return $this->db->get();
    }
    public function getLastSubscriptonByPurchasedId($purchasedId){
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->orderBy('intClientSubscriptions_order', 'DESC');
        return $this->db->get(true);
    }
    public function getFavoriteBySubsId($subsId){
        $this->db->from($this->db::FAVORITES_TNAME);
        $this->db->where('intFavorite_subscription_ID', $subsId);
        $this->db->where('intFavorite_client_ID', $this->currentUser);
        return $this->db->get(true);
    }
    public function insertFavorite($sets){
        $defaults = [
            'intFavorite_client_ID' => $this->currentUser
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->db::FAVORITES_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function deleteFavorite($id){
        $this->db->from($this->db::FAVORITES_TNAME);
        $this->db->where('favorite_ID', $id);
        return $this->db->delete();
    }
    public function insertTrashPost($sets){
        $defaults = [
            'intTrashPost_client_ID' => $this->currentUser
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->db::TRASHPOST_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function getUnSubsPosts($seriesId, $purchasedId){
        $this->db->select('post_ID');
        $this->db->from($this->db::POST_TNAME);
        $this->db->where('intPost_series_ID', $seriesId);
        $this->db->notInGroupStart('post_ID');
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->select('intClientSubscription_post_ID');
        $this->db->where('intClientSubscription_purchased_ID', $purchasedId);
        $this->db->groupEnd();
        $this->db->notInGroupStart('post_ID');
        $this->db->select('intTrashPost_post_ID');
        $this->db->from($this->db::TRASHPOST_TNAME);
        $this->db->where('intTrashPost_client_ID', $this->currentUser);
        $this->db->where('intTrashPost_series_ID', $seriesId);
        $this->db->groupEnd();
        return $this->db->get();
    }
    public function insertSubsComplete($sets){
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
}