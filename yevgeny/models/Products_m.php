<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.03.2018
 * Time: 17:19
 */

namespace Models;

require_once __DIR__ . '/Database_m.php';

use PDO;

class Products_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->current_user = $_SESSION['client_ID'];
    }
    public function insertProduct($product_title, $product_description, $product_image, $product_large_image, $product_asin, $product_amazon_url){
        $query = 'INSERT INTO `tblProduct` (`strProduct_name`, `strProduct_description`, `strProduct_image`, `strProduct_large_img`, `strProduct_ASIN`, `strProduct_amazonURL`, `dtProduct_dateadded`) VALUES (:product_title, :product_description, :product_image, :product_large_img, :product_asin, :product_amazon_url, CURRENT_TIMESTAMP)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(
            ':product_title' => $product_title,
            ':product_description' => $product_description,
            ':product_image' => $product_image,
            ':product_large_img' => $product_large_image,
            ':product_asin' => $product_asin,
            ':product_amazon_url' => $product_amazon_url
        ) );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getProductByAsin($product_asin){
        $query = 'SELECT * FROM tblProduct WHERE strProduct_ASIN = :product_asin';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_asin' => $product_asin) );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getProductUser($product_id, $user_id = false){
        $user_id = $user_id ? $user_id : $this->current_user;
        $query = 'SELECT * FROM tblProductUsers WHERE intProductUsers_product_ID = :product_id AND intProductUsers_client_ID = :user_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_id' => $product_id, ':user_id' => $user_id) );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function insertProductUser($product_id, $user_id = false){
        $user_id = $user_id ? $user_id : $this->current_user;
        $query = 'INSERT INTO tblProductUsers (`intProductUsers_client_ID`, `intProductUsers_product_ID`) VALUES (:user_id, :product_id)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':user_id' => $user_id, ':product_id' => $product_id) );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getLatestProductPrice($product_id){
        $query = 'SELECT * FROM tblProductPrice WHERE intProductPrice_product_ID = :product_id ORDER BY dtProductPrice_date DESC, productPrice_ID DESC LIMIT 1';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':product_id' => $product_id] );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function insertProductPrice($product_id, $new_price, $new_quantity, $used_price, $used_quantity){

        $now = date('Y-m-d');
        $query = 'INSERT INTO `tblProductPrice`(`dtProductPrice_date`, `intProductPrice_newprice`, `intProductPrice_newquantity`, `intProductPrice_usedprice`, `intProductPrice_usedquantity`, `intProductPrice_product_ID`) VALUES (:now_date, :new_price, :new_quantity, :used_price, :used_quantity, :product_id)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(
            ':now_date' => $now,
            ':new_price' => $new_price,
            ':new_quantity' => $new_quantity,
            ':used_price' => $used_price,
            ':used_quantity' => $used_quantity,
            ':product_id' => $product_id
        ) );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getProductRowById($product_id){
        $query = 'SELECT * FROM tblProduct WHERE product_ID = :product_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_id' => $product_id) );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getProductPricesByProductId($product_id){
        $query = 'SELECT * FROM tblProductPrice WHERE intProductPrice_product_ID = :product_id ORDER BY dtProductPrice_date, productPrice_ID';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_id' => $product_id) );
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function getProductIdsByClientId($client_id = false){
        $client_id = $client_id ? $client_id : $this->current_user;
        $query = 'SELECT tblProduct.product_ID AS product_ID, tblProduct.strProduct_ASIN AS strProduct_ASIN FROM tblProduct INNER JOIN tblProductUsers ON tblProductUsers.intProductUsers_product_ID = tblProduct.product_ID WHERE tblProductUsers.intProductUsers_client_ID = :client_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':client_id' => $client_id) );
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function deleteProductUser($product_id, $user_id = false){
        $user_id = $user_id ? $user_id : $this->current_user;
        $query = 'DELETE From tblProductUsers WHERE intProductUsers_client_ID = :client_id AND intProductUsers_product_ID = :product_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':client_id' => $user_id, ':product_id' => $product_id] );
        return $rlt;
    }
}