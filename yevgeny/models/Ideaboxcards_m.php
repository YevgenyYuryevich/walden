<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.02.2018
 * Time: 06:41
 */

namespace Models;
use function Helpers\aOrb;

require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

class Ideaboxcards_m
{
    public function __construct()
    {
        $this->db = new Database_m();
        $this->currentUser = $_SESSION['client_ID'];
        $this->defaultImg = 'assets/images/beautifulideas.jpg';
    }
    public function getIdeaboxes(){
        $this->db->select('*');
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        $ideaboxes = $this->db->get();
        foreach ($ideaboxes as & $ideabox){
            $ideabox = $this->formatIdeabox($ideabox);
        }
        return $ideaboxes;
    }
    public function getIdeabox($id){
        $this->db->select('*');
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        $this->db->where('ideabox_ID', $id);
        $ideabox = $this->db->get(true);
        $ideabox = $this->formatIdeabox($ideabox);
        return $ideabox;
    }
    public function getIdeaboxesByParentId($parentId){
        $this->db->select('*');
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        $this->db->where('intIdeaBox_subcategory', $parentId);
        return $this->db->get();
    }
    public function getMaxOrderByParentId($parentId){
        $this->db->select('MAX(intIdeaBox_order) as max_order');
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('intIdeaBox_subcategory', $parentId);
        $this->db->where('intIdeaBox_contractor_ID', $this->currentUser);
        $rlt = $this->db->get(true);
        return $rlt ? $rlt['max_order'] : 0;
    }
    public function insertIdeabox($sets){
        $parentId = aOrb($sets['intIdeaBox_subcategory'], 0);
        $max_order = $this->getMaxOrderByParentId($parentId);
        $default = [
            'intIdeaBox_contractor_ID' => $this->currentUser,
            'intIdeaBox_fromclient' => $this->currentUser,
            'strIdeaBox_idea' => '',
            'intIdeaBox_private' => 1,
            'intIdeaBox_series_ID' => 0,
            'strIdeaBox_title' => '',
            'intIdeaBox_visible' => 0,
            'intIdeaBox_subcategory' => 0,
            'intIdeaBox_order' => $max_order + 1,
            'strIdeaBox_image' => $this->defaultImg
        ];
        $sets = array_merge($default, $sets);
        $this->db->set($sets);
        $this->db->from($this->db::IDEABOX_TNAME);
        return $this->db->insert();
    }
    public function updateIdeabox($id, $sets){
        $this->db->from($this->db::IDEABOX_TNAME);
        foreach ($sets as $key => $val){
            $this->db->set($key, $val);
        }
        $this->db->where('ideabox_ID', $id);
        return $this->db->update();
    }
    public function deleteIdeabox($id){
        $this->db->from($this->db::IDEABOX_TNAME);
        $this->db->where('ideabox_ID', $id);
        return $this->db->delete();
    }
    public function formatIdeabox($ideabox){
        if($ideabox['dtIdeaBox_deadline'] === null){
            $ideabox['dtIdeaBox_deadline'] = false;
        }
        else{
            $ideabox['dtIdeaBox_deadline'] = date("m/d/Y", strtotime($ideabox['dtIdeaBox_deadline']));
        }
        $ideabox['strIdeaBox_reference_link'] = $ideabox['strIdeaBox_reference_link'] == null ? '' : $ideabox['strIdeaBox_reference_link'];

        $ideabox['strIdeaBox_reference_text'] = $ideabox['strIdeaBox_reference_text'] == null ? '' : $ideabox['strIdeaBox_reference_text'];
        $ideabox['next_deadline'] = -1;
        $ideabox['strIdeaBox_image'] = aOrb($ideabox['strIdeaBox_image'], $this->defaultImg);
        return $ideabox;
    }
}