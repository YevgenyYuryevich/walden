<?php

namespace Models;

require_once _ROOTPATH_ . '/config.php';
require_once _ROOTPATH_ . '/dbconnect.php';
require_once _ROOTPATH_ . '/yevgeny/models/Database_m.php';

use PDO;

class Explore_m
{
    public function __construct()
    {
        $rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
        global $myconnection;
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->currentUser = $_SESSION['client_ID'];
        $this->dbh = new Database_m();
    }
    public function getVisitedPageHistory($page){
        $this->dbh->from($this->dbh::CltVstPgHistory_TNAME);
        $this->dbh->select('*');
        $this->dbh->where('client_id', $this->currentUser);
        $this->dbh->where('page_name', $page);
        return $this->dbh->get();
    }
    public function insertVisitedPageHistory($page){
        $this->dbh->set('client_id', $this->currentUser);
        $this->dbh->set('page_name', $page);
        $this->dbh->set('visited_date', date('Y-m-d'));
        $this->dbh->from($this->dbh::CltVstPgHistory_TNAME);
        return $this->dbh->insert();
    }
    public function getCategories(){
        $query = 'SELECT * FROM tblcategories';
        $stmt = $this->db->query( $query );
        $categories = $stmt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
        return $this->utf8Encrypt($categories);
    }
    public function getSeriesByCategoryId($categoryId){
        $query = 'SELECT * FROM tblseries WHERE intSeries_category = :ctgId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':ctgId' => $categoryId] );
        $series = $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
        return $this->utf8Encrypt($series);
    }
    public function getPostsBySeriesId($seriesId){
        $query = 'SELECT * FROM tblpost WHERE intPost_series_ID = :seriesId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':seriesId' => $seriesId]);
        $posts = $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
        return $this->utf8Encrypt($posts);
    }
    public function utf8Encrypt($arr){
        if (!is_array($arr)){
            return utf8_encode($arr);
        }
        foreach ($arr as &$value){
            $value = $this->utf8Encrypt($value);
        }
        return $arr;
    }
    public function test(){
        $query = 'TRUNCATE ' . $this->dbh::CltVstPgHistory_TNAME;
        return $this->dbh->query($query);
    }
}