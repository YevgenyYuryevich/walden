<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.02.2018
 * Time: 10:11
 */

namespace Models;

use core\Database_core;
use Core\Model_Core;

require_once __DIR__ . '/../core/Model_core.php';

use const _ROOTPATH_;
use PDO;

class Invitation_m
{
    private $adminDB;
    private $db;
    public $currentUser;

    public function __construct()
    {
        $this->adminDB = new Database_core('guardian');
        $this->db = new Database_core();
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function getUserInfo(){
        $this->adminDB->select('*');
        $this->adminDB->from($this->adminDB::USERS_TNAME);
        $this->adminDB->where('id', $this->currentUser);
        $row = $this->adminDB->get(true);
        return $row;
    }
    public function get($where) {
        $this->db->from($this->db::CLIENTINVITE_TNAME);
        $this->db->select('*');
        $this->db->where($where);
        return $this->db->get(true);
    }
    public function insertInvitation($sets){
        $this->db->set('intClient_ID', $this->currentUser);
        foreach ($sets as $key => $val){
            $this->db->set($key, $val);
        }
        $this->db->from($this->db::CLIENTINVITE_TNAME);
        return $this->db->insert();
    }
    public function insert($sets){
        $defaults = [
            'intClient_ID' => $this->currentUser,
        ];
        $sets = array_merge($defaults, $sets);
        $this->db->from($this->db::CLIENTINVITE_TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function updateUser($sets){
        $this->adminDB->from($this->adminDB::USERS_TNAME);
        foreach ($sets as $key => $val){
            $this->adminDB->set($key, $val);
        }
        $this->adminDB->where('id', $this->currentUser);
        return $this->adminDB->update();
    }
    public function test(){
        $this->db->query('DELETE FROM tbltrash WHERE intTrash_client_ID = ' . $this->currentUser);
    }
}