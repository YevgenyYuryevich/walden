<?php

namespace Models;

require_once _ROOTPATH_ . '/config.php';
require_once _ROOTPATH_ . '/dbconnect.php';

use function Helpers\utf8Encode;
use PDO;

class CreateSeries_m
{
    const STEPS_FREQUENCY = array(0, 1, 7);

    public function __construct()
    {
        $rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
        global $myconnection;
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->current_user = $_SESSION['client_ID'];
    }
    public function getCategories(){
        $query = 'SELECT * FROM tblcategories';
        $stmt = $this->db->query($query);
        $rlts = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rlts;
    }
    public function searchIdeaBox($keyword, $pageToken = false){
        if (!!$pageToken){
            $query = 'SELECT * FROM tblideabox WHERE intIdeaBox_contractor_ID = :cid AND (strIdeaBox_title LIKE "%'. $keyword .'%" OR strIdeaBox_idea LIKE "%'. $keyword .'%") LIMIT '. $pageToken .',10';
        }
        else{
            $query = 'SELECT * FROM tblideabox WHERE intIdeaBox_contractor_ID = :cid AND (strIdeaBox_title LIKE "%'. $keyword .'%" OR strIdeaBox_idea LIKE "%'. $keyword .'%") LIMIT 0,10';
        }
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':cid' => $this->current_user]);
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function searchPosts($keyword, $pageToken = false){
        if (!!$pageToken){
            $query = 'SELECT * FROM tblpost WHERE strPost_title LIKE "%'. $keyword .'%" OR strPost_keywords LIKE "%'. $keyword .'%" OR strPost_body LIKE "%' . $keyword . '%" LIMIT '. $pageToken .',10';
            $stmt = $this->db->prepare( $query );
            $rlt = $stmt->execute();
        }
        else{
            $query = 'SELECT * FROM tblpost WHERE strPost_title LIKE "%'. $keyword .'%" OR strPost_keywords LIKE "%'. $keyword .'%" OR strPost_body LIKE "%' . $keyword . '%" LIMIT 0,10';
            $stmt = $this->db->prepare( $query );
            $rlt = $stmt->execute();
        }
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function searchRssbPosts($keyword, $pageToken = false){
        if (!$pageToken){
            $pageToken = 0;
        }
        $query = 'SELECT * FROM tblRSSBlogPosts WHERE strRSSBlogPosts_title LIKE "%'. $keyword .'%" OR strRSSBlogPosts_keywords LIKE "%'. $keyword .'%" OR strRSSBlogPosts_content LIKE "%' . $keyword . '%" OR strRSSBlogPosts_description LIKE "%'. $keyword .'%" LIMIT '. $pageToken .',10';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute();
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function insertSeries($title, $description, $category, $isPublic, $image, $frequency, $availableDays = '1111111'){
        if ($image){
            $query = 'INSERT INTO tblseries (intSeries_client_ID, strSeries_title, strSeries_description, intSeries_category, boolSeries_isPublic, strSeries_image, intSeries_frequency_ID, available_days) VALUES (:cid, :title, :description, :category, :isPublic, :image, :fid, :available_days)';
            $stmt = $this->db->prepare( $query );
            $rlt = $stmt->execute([
                ':cid' => $this->current_user,
                ':title' => $title,
                ':description' => $description,
                ':category' => $category,
                ':isPublic' => $isPublic,
                ':image' => $image,
                ':fid' => $frequency,
                ':available_days' => $availableDays
            ]);
        }
        else{
            $query = 'INSERT INTO tblseries (intSeries_client_ID, strSeries_title, strSeries_description, intSeries_category, boolSeries_isPublic, intSeries_frequency_ID, available_days) VALUES (:cid, :title, :description, :category, :isPublic, :fid, :available_days)';
            $stmt = $this->db->prepare( $query );
            $rlt = $stmt->execute([
                ':cid' => $this->current_user,
                ':title' => $title,
                ':description' => $description,
                ':category' => $category,
                ':isPublic' => $isPublic,
                ':fid' => $frequency,
                '::available_days' => $availableDays
            ]);
        }
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getPostMaxOrderBySeriesId($seriesId){
        $query = 'SELECT MAX(intPost_order) AS maxOrder FROM `tblpost` WHERE intPost_series_ID = :seriesId';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([':seriesId' => $seriesId]);
        $row = $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
        return $row ? $row['maxOrder'] : 0;
    }
    public function insertPost($seriesId, $title, $body, $summary, $type, $image, $keyword = ''){

        $maxOrder = $this->getPostMaxOrderBySeriesId($seriesId);
        $nextOrder = $maxOrder + 1;

        $query = 'INSERT INTO tblpost (intPost_series_ID, strPost_title, strPost_body, strPost_summary, intPost_type, strPost_featuredimage, intPost_order, intPost_miniseries_ID, strPost_keywords) VALUES (:sid, :title, :body, :summary, :pType, :image, :nextOrder, 3, :kwd)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':sid' => $seriesId,
            ':title' => utf8Encode($title),
            ':body' => utf8Encode($body),
            ':summary' => utf8Encode($summary),
            ':pType' => $type,
            ':image' => $image,
            ':nextOrder' => $nextOrder,
            ':kwd' => $keyword
        ]);
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function insertCategory($title, $description, $imgUrl, $orgImageUrl){
        $query = 'INSERT INTO tblcategories (strCategory_name, strCategory_description, strCategory_image, strCategory_orgImage) VALUES (:title, :description, :img, :oImg)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':title' => $title,
            ':description' => $description,
            ':img' => $imgUrl,
            ':oImg' => $orgImageUrl
        ]);
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function joinSeries($seriesId){
        $query = "INSERT INTO tblpurchased (intPurchased_client_ID, intPurchased_series_ID, dtPurchased_startdate, boolPurchased_active, intPurchased_clientorder) VALUES(:cid, :series_id, NOW(), 1, 1)";
        $stmt = $this->db->prepare($query);
        $rlt = $stmt->execute(array(':cid' => $this->current_user, ':series_id' => $seriesId));
        return $rlt ? $this->db->lastInsertId() : false;
    }
}