<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.02.2018
 * Time: 07:57
 */

namespace Models;

use function Helpers\aOrb;
use function Helpers\rootPath;
use PDO;
use const _ROOTPATH_;

require_once _ROOTPATH_ . '/config.php';
require_once _ROOTPATH_ . '/dbconnect.php';
require_once _ROOTPATH_ . '/guardian/options/options.php';

class Database_m
{
    const POST_TNAME = 'tblpost';
    const PURCHASED_TNAME = 'tblpurchased';
    const SUBSCRIPTION_TNAME = 'tblclientsubscription';
    const SUBSCPLT_TNAME = 'tblSubscriptionCompletes';
    const CLIENTINVITE_TNAME = 'tblClientInvitation';
    const CltVstPgHistory_TNAME = 'tblClientVisitedPageHistory';
    const USERS_TNAME = 'guard_users';
    const IDEABOX_TNAME = 'tblideabox';
    const SERIES_TNAME = 'tblseries';
    const FAVORITES_TNAME = 'tblfavorites';
    const RSSPOST_TNAME = 'tblRSSBlogPosts';
    const TRASHPOST_TNAME = 'tbltrashpost';
    const TRASHSERIES_TNAME = 'tbltrash';
    const GOALS_TNAME = 'tblGoals';
    const HELPERS_TNAME = 'tblHelpers';
    const EVENT_TNAME = 'events';
    const EVENT_IMAGE_TNAME = 'event_images';
    const EVENT_INS_ECP_TNAME = 'event_instance_exception';
    const EVENT_RC_PT_TNAME = 'recurring_pattern';
    const CLIENT_META_TNAME = 'tblClientMeta';

    private $queryStore;
    private $groupHandle;
    private $parent;

    public function __construct($dbConnect = 'greyshirt', $parentHandle = null)
    {
        switch ($dbConnect){
            case 'guardian':
                global $guardian;
                $this->db = new PDO('mysql:host='. $guardian['db_host'] .';dbname=' . $guardian['db_name'],$guardian['db_user'],$guardian['db_pass']);;
                break;
            case 'greyshirt':
                global $myconnection;
                $this->db = $myconnection;
                break;
            default:
                $this->db = $dbConnect;
                break;
        }
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->queryStore = [
            'stmt' => null,
            'query' => '',
            'queryName' => '',
            'from' => self::POST_TNAME,
            'select' => [],
            'set' => [],
            'where' => [],
            'orderBy' => [],
            'innerJoin' => [],
            'limit' => '',
            'groupType' => 'and_group',
            'groupMeta' => ''
        ];
        $this->parent = $parentHandle;
        $this->groupHandle = null;
    }
    public function groupStart(){
        if ($this->groupHandle){
            return $this->groupHandle->groupStart();
        }
        $this->groupHandle = new Database_m($this->db, $this);
        $this->queryStore['groupType'] = 'and_group';
        return $this->groupHandle;
    }
    public function notInGroupStart($colName){
        if ($this->groupHandle){
            return $this->groupHandle->notInGroupStart($colName);
        }
        $this->groupHandle = new Database_m($this->db, $this);

        $this->queryStore['groupType'] = 'not_in_group';
        $this->queryStore['groupMeta'] = $colName;
        return $this->groupHandle;
    }
    public function groupEnd(){
        if ($this->groupHandle){
            return $this->groupHandle->groupEnd();
        }
        return $this->parent->addGroupQuery();
    }
    public function from($tName){
        if ($this->groupHandle){
            return $this->groupHandle->from($tName);
        }
        $this->queryStore['from'] = $tName;
        return $this;
    }
    public function select($columns){
        if ($this->groupHandle){
            return $this->groupHandle->select($columns);
        }
        $this->queryStore['queryName'] = 'select';
        $this->queryStore['select'][] = is_array($columns) ? implode(', ', $columns) : $columns;
        return $this;
    }
    public function set($colName, $value = null){
        if ($this->groupHandle){
            return $this->groupHandle->set($colName, $value);
        }
        if (is_array($colName)){
            foreach ($colName as &$v){
                $v = $this->db->quote($v);
            }
            $this->queryStore['set'] = array_merge($this->queryStore['set'], $colName);
        }
        else{
            $this->queryStore['set'][$colName] = $this->db->quote($value);
        }
        return $this;
    }
    public function join($tName, $cond){
        if ($this->groupHandle){
            return $this->groupHandle->join($tName, $cond);
        }
        $this->queryStore['innerJoin'][] = 'INNER JOIN ' . $tName . ' ON ' . $cond;
        return $this;
    }
    public function where($key, $value = null){
        if ($this->groupHandle){
            return $this->groupHandle->where($key, $value);
        }
        if (is_array($key)){
            foreach ($key as $k => $value){
                $prevWhere = $this->queryStore['where'];
                $where = $prevWhere ? 'AND ' : '';
                $flgOperation = $this->parseOperation($k);
                if ($flgOperation){
                    $where .= $k . $this->db->quote($value);
                }
                else{
                    $where .= $k . ' = ' . $this->db->quote($value);
                }
                $this->queryStore['where'][] = $where;
            }
        }
        else{
            $prevWhere = $this->queryStore['where'];
            $where = $prevWhere ? 'AND ' : '';
            if (is_null($value)){
                $where .= $key;
            }
            else{
                $flgOperation = $this->parseOperation($key);
                if ($flgOperation){
                    $where .= $key . $this->db->quote($value);
                }
                else{
                    $where .= $key . ' = ' . $this->db->quote($value);
                }
            }
            $this->queryStore['where'][] = $where;
        }
        return $this;
    }
    public function orWhere($key, $value = null){
        if ($this->groupHandle){
            return $this->groupHandle->orWhere($key, $value);
        }
        if (is_array($key)){
            foreach ($key as $k => $value){
                $prevWhere = $this->queryStore['where'];
                $where = $prevWhere ? 'OR ' : '';
                $flgOperation = $this->parseOperation($k);
                if ($flgOperation){
                    $where .= $k . $this->db->quote($value);
                }
                else{
                    $where .= $k . ' = ' . $this->db->quote($value);
                }
                $this->queryStore['where'][] = $where;
            }
        }
        else{
            $prevWhere = $this->queryStore['where'];
            $where = $prevWhere ? 'OR ' : '';
            if (is_null($value)){
                $where .= $key;
            }
            else{
                $flgOperation = $this->parseOperation($key);
                if ($flgOperation){
                    $where .= $key . $value;
                }
                else{
                    $where .= $key . ' = ' . $value;
                }
            }
            $this->queryStore['where'][] = $where;
        }
        return $this;
    }
    public function like($key, $value){
        if ($this->groupHandle){
            return $this->groupHandle->like($key, $value);
        }
        $prevWhere = $this->queryStore['where'];
        $where = $prevWhere ? 'AND ' : '';
        $where .= is_null($value) ? $key : $key . ' LIKE ' . $this->db->quote('%'. $value .'%');
        $this->queryStore['where'][] = $where;
        return $this;
    }
    public function notLike($key, $value){
        if ($this->groupHandle){
            return $this->groupHandle->notLike($key, $value);
        }
        $prevWhere = $this->queryStore['where'];
        $where = $prevWhere ? 'AND NOT ' : 'NOT ';
        $where .= is_null($value) ? $key : $key . ' LIKE ' . $this->db->quote('%'. $value .'%');
        $this->queryStore['where'][] = $where;
        return $this;
    }
    public function orLike($key, $value){
        if ($this->groupHandle){
            return $this->groupHandle->orLike($key, $value);
        }
        $prevWhere = $this->queryStore['where'];
        $where = $prevWhere ? 'OR ' : '';
        $where .= is_null($value) ? $key : $key . ' LIKE ' . $this->db->quote('%'. $value .'%');
        $this->queryStore['where'][] = $where;
        return $this;
    }
    public function notIn($key, $values = []){
        if ($this->groupHandle){
            return $this->groupHandle->notIn($key, $values);
        }
        if (!$values){
            return $this;
        }
        $where = $key . ' NOT IN (' . implode(', ', $values) . ')';
        $this->where($where);
    }
    public function in($key, $values = []){
        if ($this->groupHandle){
            return $this->groupHandle->in($key, $values);
        }
        if (!$values){
            return $this;
        }
        $where = $key . ' IN (' . implode(', ', $values) . ')';
        $this->where($where);
    }
    public function orderBy($colName, $dir = false){
        if ($this->groupHandle){
            return $this->groupHandle->orderBy($colName, $dir);
        }
        if (is_array($colName)){
            foreach ($colName as $c){
                $this->queryStore['orderBy'][] = $c;
            }
        }
        else {
            $this->queryStore['orderBy'][] = $colName . ($dir ? ' ' . $dir : '');
        }
        return $this;
    }
    public function limit($size, $offset = 0){
        $this->queryStore['limit'] = $offset . ', ' . $size;
        return $this;
    }
    public function get($isSingle = false){
        $this->queryStore['queryName'] = 'select';
        $query = $this->getQuery();
        $stmt = $this->db->query( $query );
        if ($isSingle){
            return $stmt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
        }
        else{
            return $stmt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
        }
    }
    public function insert(){
        $this->queryStore['queryName'] = 'insert';
        $query = $this->getQuery();
        $rlt = $this->db->query( $query );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function update(){
        $this->queryStore['queryName'] = 'update';
        $query = $this->getQuery();
        $rlt = $this->db->query( $query );
        return $rlt ? true : false;
    }
    public function delete(){
        $this->queryStore['queryName'] = 'delete';
        $query = $this->getQuery();
        $rlt = $this->db->query( $query );
        return $rlt ? true : false;
    }
    public function getQuery(){
        $queryStore = & $this->queryStore;
        $query = & $queryStore['query'];
        switch ($this->queryStore['queryName']){
            case 'select':
                $query = 'SELECT ' . aOrb(implode(', ', $queryStore['select']), '*');
                $query .= ' FROM ' . $queryStore['from'];
                break;
            case 'insert':
                $query = 'INSERT INTO ' . $queryStore['from'];
                $columns = array_keys($queryStore['set']);
                $values = array_values($queryStore['set']);
                $query .= ' (' . implode(', ', $columns) . ') ';
                $query .= 'VALUES(' . implode(', ', $values) . ')';
                $this->resetQueryStore();
                return $query;
                break;
            case 'update':
                $query = 'UPDATE ' . $queryStore['from'] . ' SET ';
                $sets = [];
                foreach ($queryStore['set'] as $colName => $value){
                    $sets[] = $colName . ' = ' . $value;
                }
                $query .= implode(', ', $sets);
                break;
            case 'delete':
                $query = 'DELETE FROM ' . $queryStore['from'];
                break;
            default:
                break;
        }

        if ($queryStore['innerJoin']){
            $query .= ' ' . implode(' ', $queryStore['innerJoin']);
        }
        if ($queryStore['where']){
            if ($this->queryStore['queryName']){ $query .= ' WHERE '; }
            $query .= implode(' ', $queryStore['where']);
        }

        if ($queryStore['orderBy']){
            $query .= ' ORDER BY ' . implode(', ', $queryStore['orderBy']);
        }

        if ($queryStore['limit']){
            $query .= ' LIMIT ' . $queryStore['limit'];
        }

        $this->resetQueryStore();
        return $query;
    }
    public function addGroupQuery(){
        $query = $this->groupHandle->getQuery();
        $this->groupHandle = null;
        switch ($this->queryStore['groupType']){
            case 'and_group':
                $query = '(' . $query . ')';
                $this->where($query);
                break;
            case 'not_in_group':
                $query = $this->queryStore['groupMeta'] . ' NOT IN (' . $query . ')';
                $this->where($query);
                break;
            default:
                $query = '(' . $query . ')';
                $this->where($query);
                break;
        }
        return $this;
    }
    private function resetQueryStore(){
        $this->queryStore = [
            'stmt' => null,
            'query' => '',
            'queryName' => '',
            'from' => self::POST_TNAME,
            'select' => [],
            'set' => [],
            'where' => [],
            'orderBy' => [],
            'innerJoin' => [],
            'limit' => '',
            'groupType' => 'and_group'
        ];
    }
    private function parseOperation($string){
        $rlt = preg_match('@^(\s*(?P<key>\w+)\s*)(\s*(?P<operation>[=!><]+)\s*)$@', $string, $matches);
        return $rlt ? ['key' => $matches['key'], 'operation' => $matches['operation']] : false;
    }

//    raw method start
    public function query($query){
        return $this->db->query( $query );
    }
    public function prepare($query){
        return $this->db->prepare($query);
    }
    public function lastInsertId(){
        return $this->db->lastInsertId();
    }
//   raw method end
}