<footer class="home-footer">
    <div class="flex-row d-block d-sm-flex space-between vertical-center flex-wrap">
        <div class="flex-col fix-col">
            <section class="intro-section">
                <div class="logo-text">
                    Walden.ly
                </div>
                <div class="make-time">

                    2019 Waldenly, Inc. All rights reserved.
                </div>
                <div class="social-links">
                    <div class="flex-row jc-sm-left jc-center margin-between">
                        <!---
                        <div class="flex-col fix-col">
                            <a href="javascript:;">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M363.273,0H148.728C66.719,0,0,66.719,0,148.728v214.544C0,445.281,66.719,512,148.728,512h214.544
                                                C445.281,512,512,445.281,512,363.273V148.728C512,66.719,445.281,0,363.273,0z M472,363.272C472,423.225,423.225,472,363.273,472
                                                H148.728C88.775,472,40,423.225,40,363.273V148.728C40,88.775,88.775,40,148.728,40h214.544C423.225,40,472,88.775,472,148.728
                                                V363.272z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M256,118c-76.094,0-138,61.906-138,138s61.906,138,138,138s138-61.906,138-138S332.094,118,256,118z M256,354
                                                c-54.037,0-98-43.963-98-98s43.963-98,98-98s98,43.963,98,98S310.037,354,256,354z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <circle cx="396" cy="116" r="20"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
--->
                        <div class="flex-col fix-col">
                            <a href="https://www.facebook.com/challengetosolution/">
                                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m75 512h167v-182h-60v-60h60v-75c0-41.355469 33.644531-75 75-75h75v60h-60c-16.542969 0-30 13.457031-30 30v60h87.292969l-10 60h-77.292969v182h135c41.355469 0 75-33.644531 75-75v-362c0-41.355469-33.644531-75-75-75h-362c-41.355469 0-75 33.644531-75 75v362c0 41.355469 33.644531 75 75 75zm-45-437c0-24.8125 20.1875-45 45-45h362c24.8125 0 45 20.1875 45 45v362c0 24.8125-20.1875 45-45 45h-105v-122h72.707031l20-120h-92.707031v-30h90v-120h-105c-57.898438 0-105 47.101562-105 105v45h-60v120h60v122h-137c-24.8125 0-45-20.1875-45-45zm0 0"/></svg>
                            </a>
                        </div>


                        <!---
                        <div class="flex-col fix-col">
                            <a href="javascript:;">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M437.219,245.162c0-3.088-0.056-6.148-0.195-9.18c13.214-9.848,24.675-22.171,33.744-36.275
                                                    c-12.129,5.453-25.148,9.097-38.835,10.626c13.965-8.596,24.675-22.338,29.738-38.834c-13.075,7.928-27.54,13.603-42.924,16.552
                                                    c-12.323-14.021-29.904-22.95-49.35-23.284c-37.332-0.612-67.598,30.934-67.598,70.463c0,5.619,0.584,11.072,1.752,16.329
                                                    c-56.22-3.616-106.042-32.881-139.369-77c-5.814,10.571-9.152,22.922-9.152,36.164c0,25.037,11.934,47.291,30.071,60.421
                                                    c-11.099-0.5-21.503-3.866-30.627-9.375c0,0.306,0,0.612,0,0.918c0,34.996,23.312,64.316,54.245,71.159
                                                    c-5.675,1.613-11.656,2.448-17.804,2.421c-4.367-0.028-8.596-0.501-12.713-1.392c8.596,28.681,33.577,49.628,63.147,50.323
                                                    c-23.145,19.194-52.298,30.655-83.955,30.572c-5.453,0-10.849-0.361-16.135-1.029c29.933,20.53,65.456,32.491,103.65,32.491
                                                    C369.23,447.261,437.219,339.048,437.219,245.162z"/>
                                                <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                    C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                    S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
                                            </g>
                                        </g>
                                    </svg>
                            </a>
                        </div>


                        --->
                    </div>
                </div>
            </section>
        </div>
        <div class="flex-col fix-col">
            <section class="video-series-section">
                <div class="video-series-text">
                    Grow your business by sharing<br>
                    the best content with your clients.
                </div>
                <form class="search-wrapper search-for-inspiration d-xs-none">
                    <input class="search-input" placeholder="What interests you?"/>
                    <button class="search-icon-wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                <g>
                                    <g id="Zoom_2_">
                                        <g>
                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#1fcd98"/>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                    </button>
                </form>
            </section>
        </div>
        <div class="flex-col fix-col d-xs-none">
            <ul class="footer-menu">

                <li><a href="mailto:nbaca@walden.ly">Contact</a></li>
                <li><a href="https://www.walden.ly/view_blog?id=76772">Privacy</a></li>
                <li><a href="https://www.walden.ly/view_blog?id=76773">Terms</a></li>

            </ul>
        </div>
    </div>
</footer>
<div class="site-loading" hidden>
    <div class="progress-container">
        <div class="progress-item sample" hidden>
            <div class="progress-txt">waiting...</div>
            <div class="progress-bar">
                <div class="loaded-bar"></div>
            </div>
        </div>
    </div>
    <div class="loading-lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>
<div class="site-back-drop"></div>
<script>
    const CURRENT_PAGE = window.location.pathname.substr(1);
    const BASE_URL = window.location.protocol + "//" + window.location.host;
    const AFFILIATE_ID = <?php echo isset($_SESSION['affiliate_id']) ? json_encode($_SESSION['affiliate_id']) : -1;?>;
    const ACTION_URL = BASE_URL + window.location.pathname;
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
    var stripeApi_pKey = false;
    var myAccount = <?php echo json_encode($_SESSION['guardian']);?>;
</script>
<script src="https://js.stripe.com/v3/"></script>
<script src="<?php echo BASE_URL;?>/assets/components/StripeCardModal/StripeCardModal.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/StripeCheckout/StripeCheckout.js?version=<?php echo time();?>"></script>
