<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120610962-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120610962-2');
</script>
<link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/StripeCardModal/StripeCardModal.css?version=<?php echo time();?>" />

<header class="home-header">
    <nav class="header-nav">
        <div class="flex-row space-between vertical-center">
            <div class="flex-col fg-0 fg-xs-1">
                <div class="flex-row vertical-center flex-wrap">
                    <div class="flex-col fg-0 logo-col">
                        <div class="logo-wrapper">
                            <a href="<?php echo BASE_URL;?>">
                                <img class="logo-image" src="assets/images/global-icons/site-logo.png" />
                            </a>
                        </div>
                    </div>
                    <div class="flex-col fg-0 d-none d-md-block">
                        <div class="browse drop-down-browse">
                            <div>
                                Browse
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"/>
                                    </g>
                                </g>
                            </svg>
                            </div>
                            <div class="browse-drop-down-wrapper">
                                <div class="browse-drop-down-inner">
                                    <div class="make-wrapper">
                                        <div class="browse-header-wrapper">
                                            <div class="flex-row margin-between vertical-center">
                                                <div class="flex-col">
                                                    <h3>Click Category to browse</h3>
                                                </div>
                                                <div class="flex-col">
                                                    <a href="javascript:;" class="sort-by-alphabet flex-centering">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
                                                            <g>
                                                                <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <ul>
                                            <li class="sample" hidden><a href="javascript:;"><i class="glyphicon glyphicon-menu-right"></i><span>Daily Recipe</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <aside class="fill-background"></aside>
                        </div>
                    </div>
                    <div class="flex-col fg-0 fg-xs-1">
                        <form type="post" class="search-for-inspiration">
                            <div class="flex-row vertical-center">
                                <div class="flex-col">
                                    <div class="search-input-wrapper d-xs-flex" style="color:grey;">
                                        <input class="fg-xs-1" placeholder="What interests you?" data-toggle="popover" data-content="Search for content that interests you.">
                                        <button class="search-submit-button">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#1fcd98"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="flex-col fix-col nav-right-col">
                <div class="flex-row vertical-center" style="color:grey;">
                    <div class="flex-col hide-on-login">
                        <a href="javascript:;" class="top__link d-none d-md-block">Price</a>
                    </div>
                    <div class="flex-col hide-on-login">
                        <a href="javascript:;" class="top__link d-none d-md-block">For creators</a>
                    </div>
                    <div class="flex-col hide-on-login">
                        <a href="javascript:;" class="top__link d-none d-md-block">Resources</a>
                    </div>
                    <div class="flex-col show-on-login">
                        <a href="create_series" class="create-experience d-none d-md-block" data-toggle="popover" data-content="Create a collection of content to give a great experience to others and help them grow.">
                            Create <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                        </g>
                                    </g>
                                </svg>
                        </a>
                    </div>
                    <div class="flex-col hide-on-login">
                        <a href="#login-modal" data-action="register" class="log-btn register-btn d-none d-md-block" data-toggle="modal">Sign up</a>
                    </div>
                    <div class="flex-col hide-on-login">
                        <a href="#login-modal" data-action="login" class="log-btn login-btn d-none d-md-block" data-toggle="modal">Log in</a>
                    </div>
                    <div class="flex-col fix-col show-on-login">
                        <a href="my_solutions" class="my-solutions-top d-none d-md-block" data-toggle="popover" data-content="View today's experiences for every series that you have joined.">
                            <span>Today's Experiences</span> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="90px" height="90px">
                                <path style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;isolation:auto;mix-blend-mode:normal" d="M 23 3 C 19 3 15 5 15 10 C 15 10.973231 15.122689 11.762482 15.345703 12.408203 C 15.110698 12.892023 14.913907 13.352214 14.742188 13.798828 C 14.398495 13.260462 14.015661 12.710362 13.556641 12.140625 C 13.365164 11.902964 13.141121 11.662106 12.931641 11.421875 C 12.970634 11.219379 13 11.01828 13 10.832031 C 13 6.6660312 9.6660312 5 6.3320312 5 L 3 5 L 3 7.5 C 3 12.5 6.3320312 15 8.8320312 15 C 9.3360312 15 9.7755938 14.913969 10.183594 14.792969 C 9.4995937 13.790969 8.3406562 12.306141 6.7226562 10.994141 C 6.2936562 10.646141 6.2262188 10.016891 6.5742188 9.5878906 C 6.885044 9.2047182 7.4136431 9.1284428 7.8300781 9.3613281 C 9.4521413 10.610677 10.915267 12.050113 12 13.396484 C 12.657154 14.212145 13.178849 14.991178 13.521484 15.640625 C 13.86412 16.290072 14 16.829379 14 17 L 16 17 C 16 16.376908 16.460893 14.617266 17.455078 12.691406 C 18.37503 11.301449 19.749407 9.2146745 20.945312 8.0136719 C 21.335313 7.6226719 21.968375 7.6197656 22.359375 8.0097656 C 22.751375 8.3997656 22.753281 9.0328281 22.363281 9.4238281 C 20.709281 11.084828 19.549844 12.9165 18.839844 14.9375 C 19.206844 14.9795 19.593 15 20 15 C 23 15 27 12 27 6 L 27 3 L 23 3 z M 9 19 A 1.0001 1.0001 0 1 0 9 21 L 9.1425781 21 L 9.7539062 25.283203 C 9.8949063 26.268203 10.739375 27 11.734375 27 L 18.265625 27 C 19.261625 27 20.104141 26.268203 20.244141 25.283203 L 20.857422 21 L 21 21 A 1.0001 1.0001 0 1 0 21 19 L 9 19 z" font-weight="400" font-family="sans-serif" white-space="normal" overflow="visible"/>
                            </svg>
                        </a>
                    </div>
                    <div class="flex-col show-on-login">
                        <div class="my-account d-none d-md-block">
                            <div class="dropdown-icon drop-down-menu flex-centering">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 17 17" style="enable-background:new 0 0 17 17;" xml:space="preserve">
                                    <g>
                                        <path style="fill:#030104;" d="M13.5,5c0-1.381-0.56-2.631-1.464-3.535C11.131,0.56,9.881,0,8.5,0S5.869,0.56,4.964,1.465
                                            C4.06,2.369,3.5,3.619,3.5,5s0.56,2.631,1.464,3.535C5.869,9.44,7.119,10,8.5,10s2.631-0.56,3.536-1.465
                                            C12.94,7.631,13.5,6.381,13.5,5z"/>
                                        <path style="fill:#030104;" d="M2.5,15c0,1,2.25,2,6,2c3.518,0,6-1,6-2c0-2-2.354-4-6-4C4.75,11,2.5,13,2.5,15z"/>
                                    </g>
                                </svg>
                            </div>
                            <div class="account-drop-down-wrapper">
                                <div class="account-drop-down-inner">
                                    <ul>
                                        <li>
                                            <a href="my_solutions"><i class="glyphicon glyphicon-menu-right"></i><span>View Experiences</span></a>
                                        </li>
                                        <li>
                                            <a href="my_series"><i class="glyphicon glyphicon-menu-right"></i><span>Edit My Collections</span></a>
                                        </li>
                                        <li>
                                            <a href="my_favorites"><i class="glyphicon glyphicon-menu-right"></i><span>Favorites </span></a>
                                        </li>
                                        <li>
                                            <a href="my_notes"><i class="glyphicon glyphicon-menu-right"></i><span>Notes</span></a>
                                        </li>
                                        <li>
                                            <a href="my_profile"><i class="glyphicon glyphicon-menu-right"></i><span>Profile</span></a>
                                        </li>
                                        <li>
                                            <a href="logout"><i class="glyphicon glyphicon-menu-right"></i><span>Log out</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col">
                        <a class="hamburger-nav-toggle d-md-none flex-centering" href="javascript:;">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 17 17" style="enable-background:new 0 0 17 17;" xml:space="preserve">
                                    <g>
                                        <path style="fill:#030104;" d="M13.5,5c0-1.381-0.56-2.631-1.464-3.535C11.131,0.56,9.881,0,8.5,0S5.869,0.56,4.964,1.465
                                            C4.06,2.369,3.5,3.619,3.5,5s0.56,2.631,1.464,3.535C5.869,9.44,7.119,10,8.5,10s2.631-0.56,3.536-1.465
                                            C12.94,7.631,13.5,6.381,13.5,5z"/>
                                        <path style="fill:#030104;" d="M2.5,15c0,1,2.25,2,6,2c3.518,0,6-1,6-2c0-2-2.354-4-6-4C4.75,11,2.5,13,2.5,15z"/>
                                    </g>
                                </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="header-background-wrapper">
        <div class="left-section side-section skew-right">
        </div>
        <div class="center-section side-section skew-left skew-right">
        </div>
        <div class="right-section side-section skew-left">
        </div>
    </div>
</header>
<div class="site-search">
    <div class="tab-content">
        <div class="series-search-tab tab-pane fade in active" id="site-search-series-tab">
            <div class="series-search-results">
                <div class="filter-wrapper size-full">
                    <a href="javascript:;" class="resize-filter d-xs-none">
                        <i class="glyphicon glyphicon-resize-full"></i>
                        <i class="glyphicon glyphicon-resize-small"></i>
                    </a>
                    <aside class="drag-handler">
                        <img class="close-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filiter-drag-handler.png" alt="no" />
                        <img class="open-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filter-drag-handler-open.png" alt="no" />
                    </aside>
                    <div class="flex-row vertical-center space-between flex-wrap">
                        <div class="flex-col fix-col d-xs-none">
                            <div class="search-title-mobile">
                                Your search results
                            </div>
                        </div>
                        <div class="flex-col fix-col d-xs-none">
                            <div class="filters">
                                <div class="filter-name">Filters</div>
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col fix-col">
                                        <div class="filter-item">
                                            <input id="free-content" type="checkbox"/><label for="free-content">free content</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="filter-item">
                                            <input id="premium-content" type="checkbox"/><label for="premium-content">premium content</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="filter-item">
                                            <input id="affiliates-content" type="checkbox"/><label for="affiliates-content">affiliates</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="source-select-wrapper">
                                <div class="filter-name d-xs-none">From</div>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuFromSource" data-toggle="dropdown" data-hover="dropdown">
                                        <span>show series</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                        </g>
                                    </g>
                                </svg>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuFromSource">
                                        <li hidden><a href="#" data-value="show-series">show series</a></li>
                                        <li><div><a href="#site-search-find-new-tab" data-toggle="tab" data-value="find-new">find new content</a></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col d-xs-none">
                            <div class="sort-by">
                                <div class="filter-name">Sort by</div>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                        <span>popular</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                        </g>
                                    </g>
                                </svg>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
                                        <li hidden><a href="#" data-value="popular">popular</a></li>
                                        <li><a href="#" data-value="new">new</a></li>
                                        <li><a href="#" data-value="a-z">A-z</a></li>
                                        <li><a href="#" data-value="z-a">Z-a</a></li>
                                        <li><a href="#" data-value="video">Video</a></li>
                                        <li><a href="#" data-value="audio">Audio</a></li>
                                        <li><a href="#" data-value="text">Text</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="series-search-content d-no-content-none">
                    <div class="content__header">
                        <div class="flex-row align-items-center margin-between">
                            <div class="flex-col fix-col">
                                <div class="search-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#404040"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="search-txt">Search results for "<span>Yoga</span>"</div>
                            </div>
                        </div>
                    </div>
                    <div class="user-result">
                        <h2 class="user-result-title">User Result</h2>
                        <div class="list__item sample" hidden>
                            <div class="item-image">
                            </div>
                            <div class="user-name">Nathaniel Baca</div>
                            <a href="javascript:;" target="_blank" class="view-profile">View Profile</a>
                            <div class="share-with-list flex-row vertical-center margin-between">
                                <div class="flex-col fix-col">
                                    <a target="_blank" href="javascript:;" class="share-item item-facebook">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                <g>
                                    <circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
                                    <path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                        c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                </g>
                                </svg>
                                    </a>
                                </div>
                                <div class="flex-col fix-col">
                                    <a target="_blank" href="javascript:;" class="share-item item-twitter">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
                                                c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
                                                c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
                                                c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
                                                c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
                                                c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
                                                c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
                                                C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                                        </g>
                                    </g>
                                </svg>
                                    </a>
                                </div>
                                <div class="flex-col fix-col">
                                    <a target="_blank" href="javascript:;" class="share-item item-linkedin">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
                                                c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
                                                c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
                                                C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
                                                c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
                                                 M27.865,83.739H41.27V43.409H27.865V83.739z"/>
                                        </g>
                                    </g>
                                </svg>
                                    </a>
                                </div>
                                <div class="flex-col fix-col">
                                    <a href="javascript:;" class="share-item item-email">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="512pt" version="1.1" viewBox="0 0 512 512" width="512pt">
                                            <g id="surface1">
                                                <path d="M 512 256 C 512 397.386719 397.386719 512 256 512 C 114.613281 512 0 397.386719 0 256 C 0 114.613281 114.613281 0 256 0 C 397.386719 0 512 114.613281 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294118%,32.941176%,87.843137%);fill-opacity:1;" />
                                                <path d="M 512 256 C 512 245.425781 511.347656 235.003906 510.101562 224.765625 L 379.667969 94.332031 L 100.5 409 L 196.542969 505.042969 C 215.625 509.582031 235.527344 512 256 512 C 397.386719 512 512 397.386719 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(22.745098%,25.490196%,80%);fill-opacity:1;" />
                                                <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 100.5 409 L 411.5 409 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(98.039216%,36.470588%,5.882353%);fill-opacity:1;" />
                                                <path d="M 411.5 188 L 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 409 L 411.5 409 Z M 411.5 188 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.705882%,29.803922%,5.882353%);fill-opacity:1;" />
                                                <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 197.046875 293.835938 C 228.695312 328.527344 283.304688 328.527344 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;" />
                                                <path d="M 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 319.847656 C 277.40625 319.910156 299.070312 311.246094 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(92.156863%,92.156863%,94.117647%);fill-opacity:1;" />
                                                <path d="M 100.5 409 L 197.046875 303.164062 C 228.695312 268.472656 283.304688 268.472656 314.953125 303.164062 L 411.5 409 Z M 100.5 409 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,58.431373%,0%);fill-opacity:1;" />
                                                <path d="M 314.953125 303.164062 C 299.070312 285.753906 277.40625 277.085938 255.761719 277.152344 L 255.761719 409 L 411.5 409 Z M 314.953125 303.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(91.764706%,50.980392%,2.352941%);fill-opacity:1;" />
                                                <path d="M 132.332031 94.332031 L 132.332031 222.894531 L 197.046875 293.835938 C 212.871094 311.179688 234.4375 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 132.332031 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(90.196078%,90.196078%,90.196078%);fill-opacity:1;" />
                                                <path d="M 255.761719 94.332031 L 255.761719 319.847656 C 255.839844 319.847656 255.921875 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 255.761719 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.313725%,84.313725%,84.313725%);fill-opacity:1;" />
                                                <path d="M 159 124.332031 L 231 124.332031 L 231 211 L 159 211 Z M 159 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                <path d="M 247 124.332031 L 355.667969 124.332031 L 355.667969 139 L 247 139 Z M 247 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                <path d="M 255.761719 124.332031 L 355.667969 124.332031 L 355.667969 139 L 255.761719 139 Z M 255.761719 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                <path d="M 247 160.332031 L 355.667969 160.332031 L 355.667969 175 L 247 175 Z M 247 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                <path d="M 255.761719 160.332031 L 355.667969 160.332031 L 355.667969 175 L 255.761719 175 Z M 255.761719 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                <path d="M 247 196.332031 L 355.667969 196.332031 L 355.667969 211 L 247 211 Z M 247 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                <path d="M 255.761719 196.332031 L 355.667969 196.332031 L 355.667969 211 L 255.761719 211 Z M 255.761719 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                <path d="M 159 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 159 246.40625 Z M 159 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                <path d="M 255.761719 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 255.761719 246.40625 Z M 255.761719 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="flex-row margin-between">
                            <div class="flex-col fix-col overflow-visible d-xs-none">
                                <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                </a>
                            </div>
                            <div class="flex-col fb-0 min-width-0">
                                <div class="user--list">
                                </div>
                            </div>
                            <div class="flex-col fix-col overflow-visible d-xs-none">
                                <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                </a>
                            </div>
                        </div>
                    </div>
                    <h2 class="series-result-title">Series Result</h2>
                    <div class="content__list">
                        <div class="list__item sample" hidden>
                            <div class="flex-row flex-wrap d-xs-block">
                                <div class="flex-col fg-0 series-col">
                                    <article class="series-article">
                                        <div class="item-title"></div>
                                        <div class="item-image-wrapper">
                                            <div class="item-image"></div>
                                            <div class="hover-content">
                                                <div class="footer-buttons">
                                                    <div class="flex-row">
                                                        <div class="flex-col">
                                                            <a href="javascript:;" class="preview-btn" target="_blank">
                                                                <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                                            </a>
                                                        </div>
                                                        <div class="flex-col last-col">
                                                            <a href="javascript:;" class="join-btn">
                                                                <svg class="join-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                                <svg class="purchased-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                                                join
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="child-count-wrapper">
                                            <div class="child-count"><span class="child-filtered-count">2</span> Relevant Results out of <span class="child-total-count">25</span> Posts</div>
                                        </div>
                                        <div class="d-block d-sm-none show--relevant-content">
                                            Show Relevant Content
                                        </div>
                                        <div class="item-category-wrapper">
                                            <a href="javascript:;" class="item-category" target="_blank">category name</a>
                                        </div>
                                    </article>
                                </div>
                                <div class="flex-col posts-col">
                                    <div class="relevant-result">
                                        <div class="relevant__header">
                                            <div class="flex-row align-items-center margin-between">
                                                <div class="flex-col">
                                                    <div class="relevant--title">
                                                        <svg class="d-none d-xs-inline-block" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 26.676 26.676" style="enable-background:new 0 0 26.676 26.676;" xml:space="preserve">
                                                            <g>
                                                                <path d="M26.105,21.891c-0.229,0-0.439-0.131-0.529-0.346l0,0c-0.066-0.156-1.716-3.857-7.885-4.59
                                                                    c-1.285-0.156-2.824-0.236-4.693-0.25v4.613c0,0.213-0.115,0.406-0.304,0.508c-0.188,0.098-0.413,0.084-0.588-0.033L0.254,13.815
                                                                    C0.094,13.708,0,13.528,0,13.339c0-0.191,0.094-0.365,0.254-0.477l11.857-7.979c0.175-0.121,0.398-0.129,0.588-0.029
                                                                    c0.19,0.102,0.303,0.295,0.303,0.502v4.293c2.578,0.336,13.674,2.33,13.674,11.674c0,0.271-0.191,0.508-0.459,0.562
                                                                    C26.18,21.891,26.141,21.891,26.105,21.891z"/>
                                                            </g>
                                                            </svg>
                                                        Relevant Content in <span>Beginning Yoga</span>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col d-xs-none">
                                                    <a href="javascript:;" class="preview--wrap" target="_blank">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"></path>
                                                                <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"></path>
                                                                <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"></path>
                                                                <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                        <span>Preview</span>
                                                    </a>
                                                </div>
                                                <div class="flex-col fix-col d-xs-none">
                                                    <div class="share--wrap">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 525.152 525.152" style="enable-background:new 0 0 525.152 525.152;" xml:space="preserve">
                                                            <g>
                                                                <path d="M420.735,371.217c-20.021,0-37.942,7.855-51.596,20.24L181.112,282.094c1.357-6.061,2.407-12.166,2.407-18.468
                                                                    c0-6.302-1.072-12.385-2.407-18.468l185.904-108.335c14.179,13.129,32.931,21.334,53.719,21.334
                                                                    c43.828,0,79.145-35.251,79.145-79.079C499.88,35.338,464.541,0,420.735,0c-43.741,0-79.079,35.338-79.079,79.057
                                                                    c0,6.389,1.072,12.385,2.407,18.468L158.158,205.947c-14.201-13.194-32.931-21.378-53.741-21.378
                                                                    c-43.828,0-79.145,35.317-79.145,79.057s35.317,79.079,79.145,79.079c20.787,0,39.54-8.206,53.719-21.334l187.698,109.604
                                                                    c-1.291,5.58-2.101,11.4-2.101,17.199c0,42.45,34.594,76.979,76.979,76.979c42.428,0,77.044-34.507,77.044-76.979
                                                                    S463.163,371.217,420.735,371.217z"></path>
                                                            </g>
                                                        </svg>
                                                        <span>share</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="relevant__body">
                                            <div class="search-item sample" hidden>
                                                <div class="item-img-wrapper" onclick="">
                                                    <img class="item-img" />
                                                    <aside class="blog-duration">
                                                        waiting...
                                                    </aside>
                                                    <div class="hover-content">
                                                        <h4 class="show-on-login">Add to series</h4>
                                                        <div class="flex-row vertical-center margin-between show-on-login">
                                                            <div class="flex-col">
                                                                <select name="series">
                                                                </select>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <a href="javascript:;" class="add-btn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <a href="javascript:;" class="preview-btn" target="_blank" tabindex="0">
                                                            <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"></path>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                                        </a>
                                                    </div>
                                                </div>
                                                <a class="item-title" target="_blank" href="javascript:;">
                                                    Daily recipes
                                                </a>
                                            </div>
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fix-col overflow-visible d-xs-none">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                    </a>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="search-items-list">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col overflow-visible d-xs-none">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="circle-nav-wrapper">
                                <div class="circle-nav-toggle">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                        <g>
                                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                                S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"/>
                                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                                s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="circle-nav-panel"></div>
                                <ul class="circle-nav-menu">
                                    <li class="circle-nav-item circle-nav-item-1">
                                        <a class="facebook-link" href="https://www.facebook.com/sharer/sharer.php?u=test" target="_blank">
                                            <svg version="1.1" id="facebook-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="30px" height="30px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;"
                                                 xml:space="preserve">
                                                <g><path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803
                                                        c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654
                                                        c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246
                                                        c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"/>
                                                </g>
                                            </svg><span>Facebook</span>
                                        </a>
                                    </li>
                                    <li class="circle-nav-item circle-nav-item-2">
                                        <a class="twitter-link" href="https://twitter.com/intent/tweet?url=text" target="_blank">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512.002px" height="512.002px" viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;"xml:space="preserve">
                                    <g>
                                        <path d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115
                                            c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039
                                            c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809
                                            c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321
                                            c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888
                                            c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471
                                            c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59
                                            C480.111,136.775,497.92,118.275,512.002,97.211z"/>
                                    </g>
                                </svg><span>Twitter</span>
                                        </a>
                                    </li>
                                    <li class="circle-nav-item circle-nav-item-3">
                                        <a class="linkedin-link" href="https://www.linkedin.com/shareArticle?mini=true&url=test" target="_blank">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="430.117px" height="430.117px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
                                                 xml:space="preserve">
                                    <g>
                                        <path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
                                        c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
                                        v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
                                        C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
                                        c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
                                         M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
                                    </g></svg><span>Linkedlin</span>
                                        </a>
                                    </li>
                                    <li class="circle-nav-item circle-nav-item-4">
                                        <a class="google-link" href="https://plus.google.com/share?url=test" target="_blank">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="96.828px" height="96.827px" viewBox="0 0 96.828 96.827" style="enable-background:new 0 0 96.828 96.827;"
                                                 xml:space="preserve">
                                        <g>
                                            <path d="M62.617,0H39.525c-10.29,0-17.413,2.256-23.824,7.552c-5.042,4.35-8.051,10.672-8.051,16.912
                                                c0,9.614,7.33,19.831,20.913,19.831c1.306,0,2.752-0.134,4.028-0.253l-0.188,0.457c-0.546,1.308-1.063,2.542-1.063,4.468
                                                c0,3.75,1.809,6.063,3.558,8.298l0.22,0.283l-0.391,0.027c-5.609,0.384-16.049,1.1-23.675,5.787
                                                c-9.007,5.355-9.707,13.145-9.707,15.404c0,8.988,8.376,18.06,27.09,18.06c21.76,0,33.146-12.005,33.146-23.863
                                                c0.002-8.771-5.141-13.101-10.6-17.698l-4.605-3.582c-1.423-1.179-3.195-2.646-3.195-5.364c0-2.672,1.772-4.436,3.336-5.992
                                                l0.163-0.165c4.973-3.917,10.609-8.358,10.609-17.964c0-9.658-6.035-14.649-8.937-17.048h7.663c0.094,0,0.188-0.026,0.266-0.077
                                                l6.601-4.15c0.188-0.119,0.276-0.348,0.214-0.562C63.037,0.147,62.839,0,62.617,0z M34.614,91.535
                                                c-13.264,0-22.176-6.195-22.176-15.416c0-6.021,3.645-10.396,10.824-12.997c5.749-1.935,13.17-2.031,13.244-2.031
                                                c1.257,0,1.889,0,2.893,0.126c9.281,6.605,13.743,10.073,13.743,16.678C53.141,86.309,46.041,91.535,34.614,91.535z
                                                 M34.489,40.756c-11.132,0-15.752-14.633-15.752-22.468c0-3.984,0.906-7.042,2.77-9.351c2.023-2.531,5.487-4.166,8.825-4.166
                                                c10.221,0,15.873,13.738,15.873,23.233c0,1.498,0,6.055-3.148,9.22C40.94,39.337,37.497,40.756,34.489,40.756z"/>
                                            <path d="M94.982,45.223H82.814V33.098c0-0.276-0.225-0.5-0.5-0.5H77.08c-0.276,0-0.5,0.224-0.5,0.5v12.125H64.473
                                                c-0.276,0-0.5,0.224-0.5,0.5v5.304c0,0.275,0.224,0.5,0.5,0.5H76.58V63.73c0,0.275,0.224,0.5,0.5,0.5h5.234
                                                c0.275,0,0.5-0.225,0.5-0.5V51.525h12.168c0.276,0,0.5-0.223,0.5-0.5v-5.302C95.482,45.446,95.259,45.223,94.982,45.223z"/>
                                        </g></svg><span>Google</span>
                                        </a>
                                    </li>
                                    <li class="circle-nav-item circle-nav-item-5">
                                        <a class="mail-link" href="mailto:nbaca@mtnlegal.com?&subject=welcome">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" style="enable-background:new 0 0 79.536 79.536;"
                                                 xml:space="preserve">
                                    <g>
                                        <path style="fill:#010002;" d="M39.773,1.31L0,31.004v47.222h79.536V31.004L39.773,1.31z M28.77,22.499
                                        c1.167-2.133,2.775-3.739,4.815-4.805c2.035-1.075,4.357-1.616,6.983-1.616c2.214,0,4.191,0.435,5.921,1.292
                                        c1.729,0.87,3.045,2.094,3.967,3.687c0.9,1.595,1.367,3.334,1.367,5.217c0,2.247-0.694,4.279-2.082,6.097
                                        c-1.74,2.292-3.961,3.436-6.68,3.436c-0.732,0-1.279-0.122-1.654-0.38c-0.365-0.262-0.621-0.632-0.743-1.129
                                        c-1.022,1.012-2.231,1.52-3.589,1.52c-1.465,0-2.679-0.507-3.643-1.509c-0.966-1.012-1.447-2.361-1.447-4.031
                                        c0-2.084,0.578-3.966,1.743-5.672c1.416-2.084,3.218-3.13,5.424-3.13c1.571,0,2.731,0.601,3.475,1.805l0.331-1.468h3.5
                                        l-1.998,9.479c-0.125,0.606-0.187,0.986-0.187,1.163c0,0.228,0.052,0.38,0.149,0.497c0.099,0.111,0.223,0.165,0.357,0.165
                                        c0.436,0,0.979-0.248,1.646-0.769c0.901-0.663,1.627-1.574,2.181-2.695c0.554-1.129,0.839-2.299,0.839-3.508
                                        c0-2.165-0.782-3.977-2.352-5.445c-1.573-1.45-3.77-2.185-6.578-2.185c-2.393,0-4.417,0.487-6.077,1.468
                                        c-1.66,0.966-2.913,2.343-3.765,4.114c-0.839,1.76-1.258,3.607-1.258,5.52c0,1.856,0.479,3.552,1.411,5.074
                                        c0.945,1.533,2.26,2.641,3.956,3.345c1.696,0.697,3.643,1.046,5.828,1.046c2.097,0,3.909-0.293,5.432-0.881
                                        c1.522-0.587,2.739-1.457,3.666-2.641h2.807c-0.88,1.792-2.227,3.192-4.049,4.215c-2.092,1.163-4.64,1.74-7.644,1.74
                                        c-2.918,0-5.426-0.487-7.542-1.468c-2.121-0.986-3.689-2.434-4.73-4.35c-1.028-1.918-1.535-4.008-1.535-6.268
                                        C27.017,26.952,27.595,24.64,28.77,22.499z M2.804,31.941l29.344,19.68L2.804,74.333V31.941z M5.033,75.844l34.74-26.885
                                        l34.729,26.885H5.033z M76.729,74.333L47.391,51.621l29.339-19.68V74.333z M41.205,24.661c0.466,0.531,0.699,1.295,0.699,2.292
                                        c0,0.891-0.174,1.856-0.513,2.879c-0.334,1.036-0.743,1.826-1.209,2.361c-0.318,0.375-0.658,0.652-0.992,0.826
                                        c-0.439,0.249-0.906,0.37-1.41,0.37c-0.674,0.006-1.23-0.264-1.691-0.794c-0.45-0.531-0.673-1.346-0.673-2.465
                                        c0-0.839,0.158-1.805,0.487-2.889c0.329-1.088,0.81-1.916,1.453-2.509c0.647-0.588,1.346-0.881,2.1-0.881
                                        C40.162,23.856,40.749,24.125,41.205,24.661z"/>
                                    </g></svg><span>Opened-Email-Envelope</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="no-content-wrapper">
                    <div class="flex-row vertical-center d-xs-block">
                        <div class="flex-col img-col min-width-0">
                            <div class="no-content-img-wrapper">
                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-share-experience.svg">
                            </div>
                        </div>
                        <div class="flex-col txt-col min-width-0">
                            <div class="no-content-txt-wrapper">
                                <div class="there-no-txt">There are no results for your search...</div>
                                <h3 class="let-go-txt">Let's go somewhere new!</h3>
                                <div class="plus-wrapper">
                                    <a href="create_series" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    </a>
                                </div>
                                <div class="you-first-txt">
                                    You are the first person<br> to search for this topic. Let's create<br> a series from your interests and build something great!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="find-new-tab tab-pane fade" id="site-search-find-new-tab">
            <div class="find-new-content">
                <header class="search-header filter-wrapper size-full">
                    <a href="javascript:;" class="resize-filter d-xs-none">
                        <i class="glyphicon glyphicon-resize-full"></i>
                        <i class="glyphicon glyphicon-resize-small"></i>
                    </a>
                    <aside class="drag-handler">
                        <img class="close-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filiter-drag-handler.png" alt="no" />
                        <img class="open-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filter-drag-handler-open.png" alt="no" />
                    </aside>
                    <div class="flex-row vertical-center space-between flex-wrap">
                        <div class="flex-col fix-col">
                            <div class="search-title-mobile">
                                Your search results
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="source-select-wrapper">
                                <div class="filter-name d-xs-none">From</div>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuFromSource" data-toggle="dropdown" data-hover="dropdown">
                                        <span>find new content</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                        </g>
                                    </g>
                                </svg>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuFromSource">
                                        <li><div><a href="#site-search-series-tab" data-value="show-series" data-toggle="tab" data-value="show-series">show series</a></div></li>
                                        <li hidden><div><a href="#site-search-find-new-tab" data-toggle="tab" data-value="find-new">find new content</a></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col d-xs-none">
                            <div class="sort-by">
                                <h4 class="filter-name">Filter by</h4>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                        <span>All</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li hidden><a href="#" data-value="-1">All</a></li>
                                        <li><a href="#" data-value="audio">Audio</a></li>
                                        <li><a href="#" data-value="video">Video</a></li>
                                        <li><a href="#" data-value="text">Text</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="search-content d-no-content-none">
                    <div class="search-container">
                        <div class="search-row sample" hidden>
                            <header class="search-row-header">
                                <div class="flex-row vertical-center space-between">
                                    <div class="flex-col fix-col">
                                        <h2 class="from-name">from YouTube</h2>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <a href="javascript:;" class="items-found-wrapper">
                                            <span class="found-value">53</span> items found
                                        </a>
                                    </div>
                                </div>
                            </header>
                            <div class="search-row-body">
                                <div class="search-item sample" hidden>
                                    <div class="item-img-wrapper" onclick="">
                                        <img class="item-img" />
                                        <aside class="blog-duration">
                                            waiting...
                                        </aside>
                                        <div class="hover-content">
                                            <h4 class="show-on-login">Add to series</h4>
                                            <div class="flex-row vertical-center margin-between show-on-login">
                                                <div class="flex-col">
                                                    <select name="series">
                                                    </select>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <a href="javascript:;" class="add-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <a href="javascript:;" class="preview-btn" tabindex="0">
                                                <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"></path>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-title">
                                        Daily recipes
                                    </div>
                                </div>
                                <div class="flex-row margin-between">
                                    <div class="flex-col fix-col overflow-visible d-xs-none">
                                        <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                        </a>
                                    </div>
                                    <div class="flex-col fb-0">
                                        <div class="search-items-list">
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col overflow-visible d-xs-none">
                                        <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="search-footer d-no-content-none">
                    <div class="still-not-find-txt">Still didn't find what were looking for?</div>
                    <div class="btn-wrapper">
                        <button class="load-more-btn btn">Load more</button>
                    </div>
                </footer>
                <div class="no-content-wrapper">
                    <div class="flex-row vertical-center d-xs-block">
                        <div class="flex-col img-col min-width-0">
                            <div class="no-content-img-wrapper">
                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-share-experience.svg">
                            </div>
                        </div>
                        <div class="flex-col txt-col min-width-0">
                            <div class="no-content-txt-wrapper">
                                <div class="there-no-txt">There are no results for your search...</div>
                                <h3 class="let-go-txt">Let's go somewhere new!</h3>
                                <div class="plus-wrapper">
                                    <a href="create_series" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    </a>
                                </div>
                                <div class="you-first-txt">
                                    You are the first person<br> to search for this topic. Let's create<br> a series from your interests and build something great!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="posts-search-tab tab-pane fade" id="site-search-posts-tab">
            <div class="posts-search-content">
                <header class="search-header filter-wrapper size-full">
                    <a href="javascript:;" class="resize-filter d-xs-none">
                        <i class="glyphicon glyphicon-resize-full"></i>
                        <i class="glyphicon glyphicon-resize-small"></i>
                    </a>
                    <aside class="drag-handler">
                        <img class="close-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filiter-drag-handler.png" alt="no" />
                        <img class="open-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filter-drag-handler-open.png" alt="no" />
                    </aside>
                    <div class="flex-row vertical-center space-between flex-wrap">
                        <div class="flex-col fix-col d-none d-xs-block">
                            <div class="search-title-mobile">
                                Your search results
                            </div>
                        </div>
                        <div class="flex-col fix-col d-xs-none">
                            <div>
                                <div class="simplify-browse do-browse">
                                    Simplify
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                </g>
                            </g>
                        </svg>
                                    <div class="browse-drop-down-wrapper">
                                        <div class="browse-drop-down-inner">
                                            <ul>
                                                <li><a href="browse?id=simplify"><i class="glyphicon glyphicon-menu-right"></i><span>All categories</span></a></li>
                                                <li class="sample" hidden><a href="javascript:;"><i class="glyphicon glyphicon-menu-right"></i><span></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="enrich-browse do-browse">
                                    Enrich
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#f26d7e"></path>
                                </g>
                            </g>
                        </svg>
                                    <div class="browse-drop-down-wrapper">
                                        <div class="browse-drop-down-inner">
                                            <ul>
                                                <li><a href="browse?id=enrich"><i class="glyphicon glyphicon-menu-right"></i><span>All categories</span></a></li>
                                                <li class="sample" hidden><a href="javascript:;"><i class="glyphicon glyphicon-menu-right"></i><span></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="source-select-wrapper">
                                <div class="filter-name d-xs-none">From</div>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuFromSource" data-toggle="dropdown" data-hover="dropdown">
                                        <span>show posts</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                        </g>
                                    </g>
                                </svg>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuFromSource">
                                        <li><div><a href="#site-search-series-tab" data-value="show-series" data-toggle="tab" data-value="show-series">show series</a></div></li>
                                        <li hidden><div><a href="#site-search-posts-tab" data-toggle="tab" data-value="show-posts">show posts</a></div></li>
                                        <li><div><a href="#site-search-find-new-tab" data-toggle="tab" data-value="find-new">find new content</a></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col d-xs-none">
                            <div class="sort-by">
                                <h4 class="filter-name">Filter by</h4>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                        <span>All</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li hidden><a href="#" data-value="-1">All</a></li>
                                        <li><a href="#" data-value="audio">Audio</a></li>
                                        <li><a href="#" data-value="video">Video</a></li>
                                        <li><a href="#" data-value="text">Text</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="search-content d-no-content-none">
                    <div class="search-container">
                        <div class="search-row sample" hidden>
                            <header class="search-row-header">
                                <div class="flex-row vertical-center space-between">
                                    <div class="flex-col fix-col">
                                        <h2 class="from-name">from YouTube</h2>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <a href="javascript:;" class="items-found-wrapper">
                                            <span class="found-value">53</span> items found
                                        </a>
                                    </div>
                                </div>
                            </header>
                            <div class="search-row-body">
                                <div class="search-item sample" hidden>
                                    <div class="item-img-wrapper" onclick="">
                                        <img class="item-img" />
                                        <aside class="blog-duration">
                                            waiting...
                                        </aside>
                                        <div class="hover-content">
                                            <h4 class="show-on-login">Add to series</h4>
                                            <div class="flex-row vertical-center margin-between show-on-login">
                                                <div class="flex-col">
                                                    <select name="series">
                                                    </select>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <a href="javascript:;" class="add-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <a href="javascript:;" class="preview-btn" target="_blank" tabindex="0">
                                                <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"></path>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-title">
                                        Daily recipes
                                    </div>
                                </div>
                                <div class="flex-row margin-between">
                                    <div class="flex-col fix-col overflow-visible d-xs-none">
                                        <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                        </a>
                                    </div>
                                    <div class="flex-col fb-0">
                                        <div class="search-items-list">
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col overflow-visible d-xs-none">
                                        <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="search-footer d-no-content-none">
                    <div class="still-not-find-txt">Still didn't find what were looking for?</div>
                    <div class="btn-wrapper">
                        <button class="load-more-btn btn">Load more</button>
                    </div>
                </footer>
                <div class="no-content-wrapper">
                    <div class="flex-row vertical-center d-xs-block">
                        <div class="flex-col img-col min-width-0">
                            <div class="no-content-img-wrapper">
                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-share-experience.svg">
                            </div>
                        </div>
                        <div class="flex-col txt-col min-width-0">
                            <div class="no-content-txt-wrapper">
                                <div class="there-no-txt">There are no results for your search...</div>
                                <h3 class="let-go-txt">Let's go somewhere new!</h3>
                                <div class="plus-wrapper">
                                    <a href="create_series" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                                <div class="you-first-txt">
                                    You are the first person<br> to search for this topic. Let's create<br> a series from your interests and build something great!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="site-search-footer">
        <button class="back-to-main btn">back to main</button>
    </footer>
</div>
<section id="side-menu-for-mobile">
    <header class="side-menu-header">
        <div class="flex-row vertical-center">
            <div class="flex-col fix-col">
                <a class="logo" href="<?php echo BASE_URL;?>">
                    <img class="logo-image" src="assets/images/global-icons/site-logo.png" />
                </a>
            </div>
            <div class="flex-col">
                <a href="#login-modal" data-toggle="modal" data-action="register" class="log-btn register-btn">Sign up</a>
            </div>
        </div>
        <form type="post" class="search-for-inspiration">
            <div class="search-input-wrapper">
                <div class="flex-row vertical-center">
                    <div class="flex-col">
                        <input class="fg-xs-1" placeholder="What interests you?">
                    </div>
                    <div class="flex-col fix-col">
                        <button class="search-submit-button">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#1fcd98"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </header>
    <div class="side-menu-content">
        <div class="tab-content">
            <section role="tabpanel" class="tab-pane active" id="menu-section">
                <ul>
                    <li>
                        <div>
                            <a class="browse" data-toggle="tab" role="tab" href="#browse-section">
                                Browse
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"/>
                                                    </g>
                                                </g>
                                            </svg>
                            </a>
                        </div>
                    </li>
                    <li>
                        <a href="create_series" class="create-experience">
                            Create experience <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                        </g>
                                    </g>
                                </svg>
                        </a>
                    </li>
                    <li class="hide-on-login">
                        <a href="#login-modal" data-toggle="modal" data-action="login">
                            Login <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                       viewBox="0 0 299.997 299.997" style="enable-background:new 0 0 299.997 299.997;" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M149.996,0C67.157,0,0.001,67.158,0.001,149.997c0,82.837,67.156,150,149.995,150s150-67.163,150-150
                                            C299.996,67.156,232.835,0,149.996,0z M150.453,220.763v-0.002h-0.916H85.465c0-46.856,41.152-46.845,50.284-59.097l1.045-5.587
                                            c-12.83-6.502-21.887-22.178-21.887-40.512c0-24.154,15.712-43.738,35.089-43.738c19.377,0,35.089,19.584,35.089,43.738
                                            c0,18.178-8.896,33.756-21.555,40.361l1.19,6.349c10.019,11.658,49.802,12.418,49.802,58.488H150.453z"/>
                                    </g>
                                </g>
                                </svg>
                        </a>
                    </li>
                    <li class="show-on-login">
                        <div class="my-account">
                                <span>My Account <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                      viewBox="0 0 299.997 299.997" style="enable-background:new 0 0 299.997 299.997;" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M149.996,0C67.157,0,0.001,67.158,0.001,149.997c0,82.837,67.156,150,149.995,150s150-67.163,150-150
                                            C299.996,67.156,232.835,0,149.996,0z M150.453,220.763v-0.002h-0.916H85.465c0-46.856,41.152-46.845,50.284-59.097l1.045-5.587
                                            c-12.83-6.502-21.887-22.178-21.887-40.512c0-24.154,15.712-43.738,35.089-43.738c19.377,0,35.089,19.584,35.089,43.738
                                            c0,18.178-8.896,33.756-21.555,40.361l1.19,6.349c10.019,11.658,49.802,12.418,49.802,58.488H150.453z"/>
                                    </g>
                                </g>
                                </svg></span>
                            <ul>
                                <li>
                                    <a href="my_solutions"><i class="glyphicon glyphicon-menu-right"></i><span>Today's experiences</span></a>
                                </li>
                                <li>
                                    <a href="my_series"><i class="glyphicon glyphicon-menu-right"></i><span>My contributions</span></a>
                                </li>
                                <li>
                                    <a href="my_favorites"><i class="glyphicon glyphicon-menu-right"></i><span>My favorites</span></a>
                                </li>
                                <li>
                                    <a href="my_notes"><i class="glyphicon glyphicon-menu-right"></i><span>My notes</span></a>
                                </li>
                                <li>
                                    <a href="my_profile"><i class="glyphicon glyphicon-menu-right"></i><span>My profile</span></a>
                                </li>
                                <li>
                                    <a href="logout"><i class="glyphicon glyphicon-menu-right"></i><span>Log out</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <div class="download-wrapper">
                    <a href="javascript:;" class="download-app">Download the app</a>
                </div>
            </section>
            <section role="tabpanel" class="tab-pane" id="browse-section">
                <div class="browse">
                    <a data-toggle="tab" role="tab" href="#menu-section">Browse
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <div class="browse-drop-down-inner">
                        <div class="browse-header-wrapper">
                            <div class="flex-row margin-between vertical-center">
                                <div class="flex-col">
                                    <h3>Click Category to browse</h3>
                                </div>
                                <div class="flex-col">
                                    <a href="javascript:;" class="sort-by-alphabet flex-centering">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
                                            <g>
                                                <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <ul>
                            <li class="sample" hidden><a href="javascript:;"><i class="glyphicon glyphicon-menu-right"></i><span>Daily Recipe</span></a></li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <a href="#login-modal" class="modal-close" data-dismiss="modal"></a>
            <div class="modal-body">
                <div class="auth-wrapper">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="login-tab">
                            <form action="" class="auth-form login-form">
                                <h4 class="form-name">
                                    Log in
                                </h4>
                                <div class="alert alert-danger" role="alert" hidden>
                                    username or password is wrong
                                </div>
                                <div class="form-group left-circle left-line">
                                    <label for="login-username" class="control-label">E-mail</label>
                                    <input type="text" id="login-username" class="form-control" placeholder="type your email or username" required/>
                                </div>
                                <div class="form-group left-circle mb-0">
                                    <label for="login-password" class="control-label">Password</label>
                                    <input type="password" id="login-password" class="form-control" placeholder="type password" required/>
                                </div>
                                <aside class="forgot-password">
                                    <a href="#forgot-password" data-toggle="collapse">Forgot password?</a>
                                    <div class="collapse" id="forgot-password">
                                        <div class="form-group left-circle">
                                            <label for="login-reset-password">Username or email</label>
                                            <input type="text" placeholder="type your username or email" class="form-control" id="login-reset-password">
                                        </div>
                                        <div class="button-wrapper">
                                            <button type="button" class="auth-btn small reset-password">Send me</button>
                                        </div>
                                    </div>
                                </aside>
                                <div class="button-wrapper">
                                    <button class="auth-btn">Log In</button>
                                </div>
                                <aside class="auth-helper">
                                    <div class="auth-helper-text">
                                        If you don't have account yet, please
                                    </div>
                                    <div class="helper-btn-wrapper">
                                        <button class="helper-btn" data-toggle="tab" data-target="#signup-tab">
                                            Sign Up
                                        </button>
                                    </div>
                                </aside>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="signup-tab">
                            <form action="" class="auth-form signup-form">
                                <h4 class="form-name">
                                    Sign Up
                                </h4>
                                <div class="alert alert-danger" role="alert" hidden>
                                    Something is wrong
                                </div>
                                <div class="form-group left-circle left-line">
                                    <label for="signup-full-name" class="control-label">Full name</label>
                                    <input type="text" id="signup-full-name" name="f_name" class="form-control" placeholder="type your full name" />
                                </div>
                                <div class="form-group left-circle left-line">
                                    <label for="signup-email" class="control-label">E-mail</label>
                                    <input type="email" id="signup-email" name="email" class="form-control" placeholder="type your email" required/>
                                </div>
                                <div class="form-group left-circle">
                                    <label for="signup-password" class="control-label">Password</label>
                                    <input type="password" id="signup-password" name="password" class="form-control" placeholder="type your password" required/>
                                </div>
                                <div class="button-wrapper">
                                    <button class="auth-btn">Sign Up</button>
                                </div>
                                <aside class="auth-helper">
                                    <div class="auth-helper-text">
                                        If you have account already, please
                                    </div>
                                    <div class="helper-btn-wrapper">
                                        <button class="helper-btn" data-toggle="tab" data-target="#login-tab">
                                            Log in
                                        </button>
                                    </div>
                                </aside>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require ASSETS_PATH . '/components/StripeCardModal/StripeCardModal.html';
?>
