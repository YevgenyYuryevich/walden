<footer class="site-footer">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="brand-footer"><a href="#"><img class="logo-image" src="assets/images/logo2.png"><span> Walden.ly</span><span> [</span><span>Make time for what matters.</span><span>]</span></a></div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <ul class="footer-menu">
                <li class=""><a href="view.php?id=76771&prevpage=viewexperience">About</a></li>
                <li class=""><a href="view.php?id=76770&prevpage=viewexperience">Contact</a></li>
                <li class=""><a href="view.php?id=76772&prevpage=viewexperience">Privacy</a></li>
                <li class=""><a href="view.php?id=76773&prevpage=viewexperience">Terms</a></li>
                <li class=""><a href="view.php?id=76774&prevpage=viewexperience">Jobs</a></li>
            </ul>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <div class="footer-copyright">Copyright 2017. All rights reserved.</div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    const CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
    const AFFILIATE_ID = <?php echo isset($_SESSION['affiliate_id']) ? json_encode($_SESSION['affiliate_id']) : -1;?>;
    const CURRENT_PAGE = window.location.pathname.substr(1);
    const BASE_URL = window.location.protocol + "//" + window.location.host;
</script>