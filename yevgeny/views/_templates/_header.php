<header class="site-header">
    <div class="site-header-inner-wrap">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo BASE_URL;?>"><img class="logo-image" src="assets/images/global-icons/site-logo.png"><span> | </span><span>Make time for what matters.</span></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="<?php echo CURRENT_PAGE == 'userpage' ? 'active' : '';?>"><a href="userpage">My Solutions</a></li>
                        <li class="<?php echo CURRENT_PAGE == 'simplify' ? 'active' : '';?>"><a href="simplify">Simplify</a></li>
                        <li class="<?php echo CURRENT_PAGE == 'enrich' ? 'active' : '';?>"><a href="enrich">Enrich</a></li>
                        <li class="<?php echo CURRENT_PAGE == 'createyourexperience' ? 'active' : '';?>"><a href="createyourexperience">Create</a></li>
                        <li class="<?php echo CURRENT_PAGE == 'ideaboxcards' ? 'active' : '';?>"><a href="ideaboxcards">IdeaBox</a></li>
                    </ul>
                    <form class="navbar-form navbar-right search-form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <a data-toggle="collapse" class="hamburger-nav-toggle" href="#collapse-hamburger">
                            <i class="fa fa-bars humburger_button" aria-hidden="true"></i>
                        </a>
                    </form>
                </div>
            </div>
        </nav>
        <div class="collapse" id="collapse-hamburger">
            <nav class="hamburger-nav">
                <ul>
                    <li><a href="userpage"><img src="assets/images/global-icons/Userpage.png"> Userpage</a></li>
                    <li><a href="simplify"><img src="assets/images/global-icons/Explore.png"> Simplify</a></li>
                    <li><a href="enrich"><img src="assets/images/global-icons/enrich.png"> Enrich</a></li>
                    <li><a href="createyourexperience"><img src="assets/images/global-icons/Create.png"> Create</a></li>
                    <li><a href="ideaboxcards"><img src="assets/images/global-icons/ideabox.png"> IdeaBox</a></li>
                    <li><a href="mygoals"><img src="assets/images/global-icons/My-Growth.png"> My Growth</a></li>
                    <li><a href="myseries"><img src="assets/images/global-icons/My-Series.png"> My Series</a></li>
                    <li><a href="favorites"><img src="assets/images/global-icons/My-Favorites.png"> My Favorites</a></li>
                    <li><a href="myprofile"><img src="assets/images/global-icons/My-Profile.png"> My Profile</a></li>
                    <li><a href="invitation"><img src="assets/images/global-icons/Invite.png"> Invite Friend</a></li>
                    <li><a href="logout"><img src="assets/images/global-icons/Logout.png"> Logout</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="collapse" id="search-results">
        <div class="search-results-wrapper">
            <aside class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Results
                </div>
            </aside>
            <header class="search-header">
                <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <div class="results-from">
                            <span>Results from: </span>
                            <div class="dropdown">
                                <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select types To View <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:;"><input id="from-web" name="from" type="checkbox" value="youtube" checked/><label for="from-web">WEB</label></a></li>
                                    <li><a href="javascript:;"><input id="from-spreaker" name="from" type="checkbox" value="spreaker" checked/><label for="from-spreaker">Podcasts</label></a></li>
                                    <li><a href="javascript:;"><input id="from-twingly" name="from" type="checkbox" value="twingly" checked/><label for="from-twingly">Blogs</label></a></li>
                                    <li><a href="javascript:;"><input id="from-foodfork" name="from" type="checkbox" value="recipes"/><label for="from-foodfork">Recipes</label></a></li>
                                    <li><a href="javascript:;"><input id="from-ideabox" name="from" type="checkbox" value="ideabox" checked/><label for="from-ideabox">IDEABOX</label></a></li>
                                    <li><a href="javascript:;"><input id="from-posts" name="from" type="checkbox" value="posts" checked/><label for="from-posts">POSTS</label></a></li>
                                    <li><a href="javascript:;"><input id="from-rssbposts" name="from" type="checkbox" value="rssbposts"/><label for="from-rssbposts">RSSBlogPosts</label></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="search-term-wrapper">
                            <form class="form-inline" method="post">
                                <label for="search-term2">Search: </label>
                                <input class="form-control" id="search-term2" size="30"/>
                                <button class="btn btn-default" type="submit">Submit</button>
                                <button class="btn btn-default clear-search-term" type="reset">Clear</button>
                            </form>
                        </div>
                    </div>
                </div>
            </header>
            <div class="search-results-content">
                <div class="item-sample-wrapper" hidden>
                    <a>
                        <div class="item-inner">
                            <aside>
                                <div class="bottom-line"></div>
                            </aside>
                            <div class="img-wrapper">
                                <img class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <div class="item-title"></div>
                            </div>
                            <div class="add-to-series-wrapper">
                                <h6>Add to series</h6>
                                <div class="flex-row">
                                    <div class="flex-col select-col">
                                        <select name="series">
                                        </select>
                                    </div>
                                    <div class="flex-col btn-col">
                                        <button class="add-post btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="buttons-wrapper">
                                <button href="#" class="view-post btn btn-circle">View</button>
                            </div>
                            <div class="remove-wrapper">
                                <span href="#" class="remove-post">Remove</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="tt-grid-wrapper youtube-grid after-clearfix">
                    <h3 class="from-where">Results from Youtube</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <div class="tt-grid-wrapper podcasts-grid after-clearfix">
                    <h3 class="from-where">Results from Podcasts</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <div class="tt-grid-wrapper blogs-grid after-clearfix">
                    <h3 class="from-where">Results from Blogs</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <div class="tt-grid-wrapper recipes-grid after-clearfix">
                    <h3 class="from-where">Results from Recipes</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <div class="tt-grid-wrapper ideabox-grid after-clearfix">
                    <h3 class="from-where">Results from Ideabox</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <div class="tt-grid-wrapper posts-grid after-clearfix">
                    <h3 class="from-where">Results from Posts</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <div class="tt-grid-wrapper rssb-posts-grid after-clearfix">
                    <h3 class="from-where">Results from RSSBlogPost</h3>
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
                <footer>
                    <button class="btn btn-circle show-more" type="button"><span class="glyphicon glyphicon-play"></span> Show more</button>
                    <button class="btn btn-circle i-am-done" type="button">I am done!</button>
                </footer>
            </div>
        </div>
    </div>
</header>