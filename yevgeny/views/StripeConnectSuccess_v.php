<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.08.2018
 * Time: 13:00
 */
?>
<?php

$rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
$default_image = 'assets/images/beautifulideas.jpg';

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Connect Stripe</title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <style type="text/css" rel="stylesheet">
        .alert{
            max-width: 1000px;
            margin: 50px auto;
        }
    </style>
</head>

<body>

<div class="jumbotron text-center">
    <h1 class="display-3">Thank You!</h1>
    <p class="lead">Now you can charge and set budget for your series</p>
    <hr>
    <p>
        Having trouble? <a href="#">Contact us</a>
    </p>
    <p class="lead">
        <a class="btn btn-primary btn-md previous-btn" href="https://bootstrapcreative.com/" role="button" onclick="goToPreviousPage()">Continue to Previous Page</a>
    </p>
</div>

<script>
    var startPage = localStorage.getItem('walden_start_page_stripe_connect');
    function goToPreviousPage(){
        location.href = startPage;
    };
    setTimeout(function () {
        if (startPage) {
            location.href = startPage;
        }
    }, 5000);
</script>

</body>
</html>