<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.02.2018
 * Time: 06:43
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="ideaBox" />
    <meta name="Description" content="Organize your thoughts in ideaboxes so you can easily find them when you need them" />
    <title>The ideaBox - Organize and save your thoughts</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/ideaboxcards-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/ideaboxcards-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-ideabox">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Ideabox
                </div>
            </div>
            <header class="content-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <form class="form-inline search-form" method="post" hidden>
                            <label for="search-term">Search</label>&nbsp;&nbsp;
                            <input name="searchTerm" class="form-control" size="25" id="search-term" />
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <form class="form-inline sort-from" method="post">
                            <div class="back-origin-wrapper">
                                <label for="back-org">Back</label>&nbsp;
                                <a href="javascript:;" class="back-to-top-level"><img src="assets/images/global-icons/back.png"></a>
                            </div>
                            <div class="new-idea-wrapper">
                                New <a class="create-ideabox" href="javascript:;"><svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a>
                            </div>
                        </form>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <h2 class="page-title text-center">
                    This is a place to save your ideas and other things that interest you. If you want to share your idea publicly, you can add it to any series that you create.
                </h2>
                <div class="item-sample-wrapper" hidden>
                    <div>
                        <div class="item-inner">
                            <aside>
                                <img class="delete-ideabox" src="assets/images/global-icons/close.png" />
                                <div class="bottom-line"></div>
                            </aside>
                            <div class="img-wrapper">
                                <img class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <textarea name="ideaTitle" rows="3"></textarea>
                                <div class="item-title"></div>
                                <aside class="actions-container">
                                    <span class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></span>
                                    <span class="save-entry"><i class="glyphicon glyphicon-check"></i></span>
                                    <span class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></span>
                                </aside>
                            </div>
                            <div class="idea-wrapper">
                                <textarea name="ideaDescription" rows="5"></textarea>
                                <div class="item-idea"></div>
                                <aside class="actions-container">
                                    <span class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></span>
                                    <span class="save-entry"><i class="glyphicon glyphicon-check"></i></span>
                                    <span class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></span>
                                </aside>
                            </div>
                            <div class="reference-wrapper">
                                <span class="reference-link"></span>
                            </div>
                            <div class="deadline-divider"></div>
                            <div class="next-deadline-wrapper">
                                Next Deadline: <span class="next-deadline"></span>
                            </div>
                            <div class="add-deadline-wrapper">
                                <div class="flex-row">
                                    <div class="flex-col fix-col">
                                        Set Deadline:
                                    </div>
                                    <div class="flex-col">
                                        <div class="input-wrapper"><input class="deadline"><i class="glyphicon glyphicon-calendar"></i><span class="deadline-show"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="add-to-series-wrapper">
                                <h6>Add to series</h6>
                                <div class="flex-row">
                                    <div class="flex-col select-col">
                                        <select name="series">
                                        </select>
                                    </div>
                                    <div class="flex-col btn-col">
                                        <button class="add-post btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                    </div>
                                </div>
                            </div>
                            <footer class="btns-container">
                                <div class="flex-row space-between margin-between">
                                    <div class="flex-col fix-col">
                                        <div class="btn-wrapper view-btn">
                                            <div><img src="assets/images/global-icons/read.png"/></div>
                                            View
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="btn-wrapper explore-btn">
                                            <div><img src="assets/images/global-icons/ideabox.png"/></div>
                                            explore
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="btn-wrapper todo-btn">
                                            <div><img src="assets/images/global-icons/todo.png"/></div>
                                            to do
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="btn-wrapper edit-btn">
                                            <div><img src="/assets/images/global-icons/edit-calendar-interface-symbol.svg"/></div>
                                            edit
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                <div class="tt-grid-wrapper after-clearfix">
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                    </ul>
                </div>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/ideaboxcards-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initialIdeaboxes = <?php echo json_encode($ideaboxes);?>;
    var initialParentId = <?php echo $parentId;?>;
    var initialSeries = <?php echo json_encode($series);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/ideaboxcards-page.js?version=<?php echo time();?>"></script>

</body>
</html>
