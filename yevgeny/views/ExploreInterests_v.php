<?php

$rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',  $_SERVER['SERVER_NAME'] );

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="<?php echo $category['strCategory_name'];?>">
    <meta name="Description" content="Explore <?php echo $category['strCategory_name'];?> and find the daily experiences that are right for you.">
    <title><?php echo $category['strCategory_name'];?></title>

    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/css/yevgeny/exploreInterests-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/css/yevgeny/exploreInterests-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-exploreInterests">
    <?php require_once $rootPath . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Explore this Paths
                </div>
                <div class="decoration top-right view-all-path"><a href="<?php echo $prevPage;?>"><span>&lt;</span><span>&lt;</span>View all Path</a></div>
            </div>
            <header class="content-header">
                <div class="page-top-title">
                    Select Solutions below that will benefit your life.<br>
                    Content from these solutions will be available to you on a daily basis on the "My Solutions" page.
                </div>
                <div class="scroll-fix-rplc" hidden>
                    <div class="flex-row vertical-center space-between">
                        <div class="flex-col fix-col">
                            <a class="back-to-prev" href="javascript:;"><i class="glyphicon glyphicon-menu-left"></i></a>
                        </div>
                        <div class="flex-col">
                            <div class="rplc-content-wrp text-center">
                                <a href="createseries">Not finding what you want? Create the perfect experience for you. <svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <h2><?php echo $category['strCategory_name'];?></h2>
                <div class="item-sample-wrapper" hidden>
                    <div>
                        <div class="item-inner">
                            <div class="img-wrapper">
                                <img src="" alt="" class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <div class="item-title"></div>
                            </div>
                            <div class="buttons-wrapper">
                                <a href="#" class="view-post btn btn-circle" target="_blank">View</a>
                            </div>
                            <div class="buttons-wrapper">
                                <div class="wrap">
                                    <button class="select-post">Join</button>
                                    <img src="../assets/images/check_arrow_2.svg" alt="">
                                    <svg width="42px" height="42px">
                                        <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                    </svg>
                                </div>
                            </div>
                            <div class="object_line"></div>
                        </div>
                    </div>
                </div>
                <div class="tt-grid-wrapper after-clearfix">
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay sample" hidden>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                        <li class="tt-empty"></li>
                    </ul>
                </div>
            </main>
        </div>
    </div>
    <?php require_once $rootPath . '/yevgeny/views/_templates/_footer.php';?>
</div>

<script>
    const ACTION_URL = BASE_URL + window.location.pathname;

    var initCategory = <?php echo json_encode($category);?>;
    var initSeries = <?php echo json_encode($series);?>;
    var initPrevPage = <?php echo json_encode($prevPage);?>;

</script>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo $base_url;?>/assets/js/yevgeny/exploreInterests-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo $base_url;?>/assets/js/yevgeny/exploreInterests-page.js?version=<?php echo time();?>"></script>

</body>
</html>