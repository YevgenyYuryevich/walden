<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 03:42
 */
?>

<!DOCTYPE html>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="Create an Event" />
    <meta name="Description" content="Create a one-time or recurring event" />
    <title>Create an Event</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/createEvent-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/createEvent-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-createEvent">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Create
                </div>
            </div>
            <main class="main-content">
                <h2 class="text-center">What type of event would you like to create?</h2>
                <ul>
                    <li>
                        <a href="createsingleevent">
                            <div class="icon-wrapper">
                                <img src="assets/images/global-icons/new-event.png">
                            </div>
                            <div class="icon-label">
                                A New Single Event
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="createrecurringevent">
                            <div class="icon-wrapper">
                                <img src="assets/images/global-icons/create-recurring-event.png">
                            </div>
                            <div class="icon-label">
                                A New Recurring Event
                            </div>
                        </a>
                    </li>
                </ul>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    const CURRENT_USER = <?php echo $_SESSION['client_ID'];?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/createEvent-global.js?version=<?php echo time();?>"></script>

</body>
</html>