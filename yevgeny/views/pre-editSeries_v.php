<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Edit the solution to make it perfect for you - thegreyshirt</title>
    <meta name="description" content="Edit your daily solution to include the content that you want in the order that you want it." />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=1.2" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/editSeries-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/editSeries-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-editseries">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <ul class="nav nav-tabs" hidden>
                <li class="<?php echo $panel == 'main' ? 'active': '';?>"><a data-toggle="tab" href="#main-tab">series</a></li>
                <li class="<?php echo $panel == 'addNew' ? 'active': '';?>"><a data-toggle="tab" href="#add-items-tab">posts</a></li>
            </ul>
            <div class="tab-content">
                <div id="main-tab" class="tab-pane fade <?php echo $panel == 'main' ? 'in active': '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Customize
                        </div>
                    </div>
                    <header class="content-header">
                        <div class="open-add-items-wrapper pull-right">Add New Item <a class="open-add-items" href="javascript:;"><svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a></div>
                        <div class="clearfix"></div>
                    </header>
                    <main class="main-content">
                        <header>
                            <h2 class="series-title"><?php echo $series['strSeries_title'];?></h2>
                            <div class="cover-image-wrapper">
                                <div class="img-wrapper">
                                    <img src="<?php  echo $series['strSeries_image'];?>" />
                                </div>
                                <div>{Drag Image here to upload}</div>
                                <input class="form-control" type="file" name="cover_img"/>
                            </div>
                            <?php if ($isSeriesMine): ?>
                                <div class="charge-helper">
                                    <a href="myprofile">
                                        To charge for your series, you must setup stripe public API in your settings.<br/>
                                        Click here to do this
                                    </a>
                                </div>
                                <div class="<?php echo (int)$series['boolSeries_charge'] === 1 ? 'charged' : '';?> flex-row vertical-center margin-between horizon-center series-charge-wrapper">
                                    <div class="flex-col fix-col">
                                        <span class="charge-label">Charge: </span>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <label class="c-switch">
                                            <input type="checkbox" name="boolSeries_charge" <?php echo (int)$series['boolSeries_charge'] === 1 ? 'checked' : '';?> value="1">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <span class="price-label">Price: </span>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <input class="form-control charge-price" name="intSeries_price" type="number" <?php echo (int)$series['boolSeries_charge'] === 1 ? '' : 'disabled';?> value="<?php echo (int)$series['boolSeries_charge'] === 1 ? $series['intSeries_price'] : '';?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="available-days-wrapper">
                                <div class="display-on">Display on: </div>
                                <div class="available-days-container">
                                    <ul class="available-days-list">
                                        <li class="<?php echo $availableDays[0] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'mon'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            M
                                        </li>
                                        <li class="<?php echo $availableDays[1] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'tus'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            T
                                        </li>
                                        <li class="<?php echo $availableDays[2] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'wed'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            W
                                        </li>
                                        <li class="<?php echo $availableDays[3] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'thi'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            Th
                                        </li>
                                        <li class="<?php echo $availableDays[4] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'fri'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            F
                                        </li>
                                        <li class="<?php echo $availableDays[5] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'sat'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            Sa
                                        </li>
                                        <li class="<?php echo $availableDays[6] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'sun'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            S
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php if ($isSeriesMine): ?>
                                <div class="series-description-wrapper">
                                    <div class="form-group">
                                        <label form="series-description" for="series-description">Series Description</label>
                                        <textarea id="series-description" class="series-description form-control" name="strSeries_description"><?php echo $series['strSeries_description'];?></textarea>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </header>
                        <aside class="add-to-links-section">
                            <h2>Add to Series:</h2>
                            <div class="flex-row vertical-center horizon-center flex-wrap">
                                <div class="flex-col">
                                    <a href="javascript:;" data-node-type="post" data-post-type="7">
                                        Add Post<br />
                                        from Scratch
                                    </a>
                                </div>
                                <div class="flex-col">
                                    <a href="javascript:;" class="add-from-search">
                                        Add Post(s)<br />
                                        from Search
                                    </a>
                                </div>
                                <?php if ($isSeriesMine): ?>
                                    <div class="flex-col">
                                        <a href="javascript:;" data-node-type="menu">
                                            Create<br />
                                            Menu/Dcision-Point
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" data-node-type="post" data-post-type="9">
                                            Create<br />
                                            "Switch To" Point
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" data-tab="bulkupload">
                                            Add Post(s)<br />
                                            from Bulk Upload
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <div class="flex-col">
                                    <a href="javascript:;" data-node-type="post" data-post-type="10">
                                        Create<br />
                                        Form
                                    </a>
                                </div>
                            </div>
                        </aside>
                        <h2 class="tree-helper-title">Edit Series Structure:  <span>(Drag an Item to Change the Order)</span></h2>
                        <div class="posts-container jstree jstree-default jstree-default-large <?php echo (int)$series['boolSeries_charge'] ? 'charged' : '';?>">
                            <li class="sample post-row jstree-node jstree-closed" hidden>
                                <div class="post-row-inner">
                                    <div class="flex-row vertical-center space-between margin-between">
                                        <div class="flex-col fix-col"><i class="jstree-icon jstree-ocl" role="presentation"></i></div>
                                        <div class="flex-col fix-col">
                                            <div class="drag-icon-wrapper">
                                                <img class="drag-icon type-root" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/series/start-(3).png">
                                                <img class="drag-icon type-post" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/post/blog-(4).png">
                                                <img class="drag-icon type-menu" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/menu/path-selection-(4).png">
                                                <img class="drag-icon type-path" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/option/next-(4).png">
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="post-title"></div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="actions">
                                                <div class="flex-row margin-between vertical-center">
                                                    <?php  if ($isSeriesMine): ?>
                                                        <div class="flex-col publish-col">
                                                            <a class="publish-post">
                                                                <img class="active-icon" src="assets/images/global-icons/check-active.png">
                                                                <img class="inactive-icon" src="assets/images/global-icons/check-inactive.png">
                                                            </a>
                                                        </div>
                                                        <div class="flex-col charge-col">
                                                            <div class="flex-row vertical-center margin-between horizon-center post-charge-wrapper">
                                                                <div class="flex-col fix-col">
                                                                    <span class="charge-label">Free? </span>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <label class="c-switch">
                                                                        <input type="checkbox" name="boolPost_free" value="1">
                                                                        <div class="slider round"></div>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php  endif; ?>
                                                    <div class="flex-col view-col">
                                                        <a class="view-post" href="javascript:;">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/open-book.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col edit-col">
                                                        <a class="edit-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/edit-series.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col save-col">
                                                        <a class="save-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/diskette-save-interface-symbol.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col comeback-col">
                                                        <a class="comeback-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/comeback-arrow.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col">
                                                        <a class="clone-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/clone.png">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col del-col">
                                                        <a class="delete-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/close-cross-circular-interface-button.png">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <div class="modal-edit-entry-wrapper" hidden>
                                <?php require_once ASSETS_PATH . '/components/EditPostModal/EditPostModal.html';?>
                            </div>
                            <ul class="posts-list">
                            </ul>
                        </div>
                    </main>
                </div>
                <div id="add-items-tab" class="tab-pane fade <?php echo $panel == 'addNew' ? 'in active': '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Results
                        </div>
                    </div>
                    <header class="content-header">
                        <div class="flex-row space-between flex-wrap">
                            <div class="flex-col fix-col">
                                <div class="results-from">
                                    <span>Results from: </span>
                                    <div class="dropdown">
                                        <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select types To View <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:;"><input id="from-url" name="from" type="checkbox" value="url"/><label for="from-url">My Unique URLs</label></a></li>
                                            <li><a href="javascript:;"><input id="from-web" name="from" type="checkbox" value="youtube" checked/><label for="from-web">WEB</label></a></li>
                                            <li><a href="javascript:;"><input id="from-spreaker" name="from" type="checkbox" value="spreaker" checked/><label for="from-spreaker">Podcasts</label></a></li>
                                            <li><a href="javascript:;"><input id="from-twingly" name="from" type="checkbox" value="twingly" checked/><label for="from-twingly">Blogs</label></a></li>
                                            <li><a href="javascript:;"><input id="from-foodfork" name="from" type="checkbox" value="recipes"/><label for="from-foodfork">Recipes</label></a></li>
                                            <li><a href="javascript:;"><input id="from-ideabox" name="from" type="checkbox" value="ideabox"/><label for="from-ideabox">IDEABOX</label></a></li>
                                            <li><a href="javascript:;"><input id="from-posts" name="from" type="checkbox" value="posts"/><label for="from-posts">POSTS</label></a></li>
                                            <li><a href="javascript:;"><input id="from-rssbposts" name="from" type="checkbox" value="rssbposts"/><label for="from-rssbposts">RSSBlogPosts</label></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="search-term-wrapper">
                                    <form class="form-inline" method="post">
                                        <label for="search-term2">Search: </label>
                                        <input class="form-control" id="search-term2" size="30"/>
                                        <button class="btn btn-default" type="submit">Submit</button>
                                        <button class="btn btn-default clear-search-term" type="reset">Clear</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </header>
                    <main class="main-content">
                        <aside class="actions-container">
                            <a href="javascript:;" class="add-new-posts action">
                                Create your own idea
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                        <g>
                                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                        S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"></path>
                                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                        s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"></path></g>
                                    </svg>
                            </a>
                        </aside>
                        <h2 class="text-center">Here are some ideas for your experience. Please select the ones you like</h2>
                        <h3 class="text-center">(You can also drag a URL here to add it)</h3>
                        <div class="meta-posts-block-container">
                            <div class="item-sample" hidden>
                                <div class="item-inner">
                                    <aside>
                                        <img class="delete-item" src="assets/images/global-icons/close.png" />
                                    </aside>
                                    <div class="img-wrapper">
                                        <img class="item-img" />
                                        <div class="input-wrapper">
                                            <aside class="helper-text">Click or Drag</aside>
                                            <input name="item_image" type="file" title="Click or Drag">
                                        </div>
                                    </div>
                                    <div class="title-wrapper">
                                        <textarea name="item_title" rows="2" placeholder="Post Title"></textarea>
                                        <div class="item-title"></div>
                                        <aside class="actions-container">
                                            <div class="edit-wrapper">
                                                <span class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></span>
                                            </div>
                                            <div class="save-wrapper">
                                                <div class="flex-row margin-between space-between">
                                                    <div class="flex-col">
                                                        <button class="btn btn-primary btn-block btn-xs save-entry">save</button>
                                                    </div>
                                                    <div class="flex-col">
                                                        <button class="btn btn-block btn-default btn-xs back-to-origin">cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                    <div class="description-wrapper">
                                        <textarea name="item_body" rows="3" placeholder="Describe Here..."></textarea>
                                        <div class="item-description"></div>
                                        <aside class="actions-container">
                                            <div class="edit-wrapper">
                                                <span class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></span>
                                            </div>
                                            <div class="save-wrapper">
                                                <div class="flex-row margin-between space-between">
                                                    <div class="flex-col">
                                                        <button class="btn btn-primary btn-block btn-xs save-entry">save</button>
                                                    </div>
                                                    <div class="flex-col">
                                                        <button class="btn btn-block btn-default btn-xs back-to-origin">cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                    <div class="type-wrapper">
                                        <div class="flex-row margin-between vertical-center">
                                            <div class="flex-col fix-col">Post Type: </div>
                                            <div class="flex-col">
                                                <select name="type">
                                                    <option value="2">Video</option>
                                                    <option value="0">Audio</option>
                                                    <option value="8">Image</option>
                                                    <option value="7">Text</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="buttons-wrapper">
                                        <div class="wrap">
                                            <button class="select-post">Select</button>
                                            <img src="../assets/images/check_arrow_2.svg" alt="">
                                            <svg width="42px" height="42px">
                                                <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="bottom-line"></div>
                                </div>
                            </div>
                            <div class="tt-grid-wrapper posts-block sample" hidden>
                                <h3 class="from-where">My Unique URLs</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                </ul>
                            </div>
                        </div>
                        <div class="posts-block-container">
                            <div class="item-sample-wrapper" hidden>
                                <div>
                                    <div class="item-inner">
                                        <aside>
                                            <div class="bottom-line"></div>
                                        </aside>
                                        <div class="img-wrapper">
                                            <img src="" alt="" class="item-img">
                                        </div>
                                        <div class="title-wrapper">
                                            <div class="item-title"></div>
                                        </div>
                                        <div class="buttons-wrapper">
                                            <div class="wrap">
                                                <button class="select-post">Select</button>
                                                <img src="../assets/images/check_arrow_2.svg" alt="">
                                                <svg width="42px" height="42px">
                                                    <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tt-grid-wrapper posts-block sample" hidden>
                                <h3 class="from-where">Results from</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                        </div>
                        <footer>
                            <button class="btn btn-circle back-to-main" type="button"><span class="glyphicon glyphicon-arrow-left"></span> Go To Main</button>
                            <button class="btn btn-circle show-more" type="button"><span class="glyphicon glyphicon-play"></span> Show more</button>
                        </footer>
                    </main>
                </div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<form class="modal-edit-stripe" hidden>
    <header class="site-modal-header">To begin accepting payments, you will need to setup your stripe credentials:</header>
    <div class="site-modal-body">
        <div class="flex-row vertical-center margin-between">
            <div class="flex-col fix-col">
                <label class="control-label">Stripe Api Public Key:</label>
            </div>
            <div class="flex-col">
                <input value="<?php echo $stripePKey;?>" name="stripeApi_pKey" class="form-control" required/>
            </div>
        </div>
        <div class="flex-row vertical-center margin-between">
            <div class="flex-col fix-col">
                <label class="control-label">Stripe Api Charge Url:</label>
            </div>
            <div class="flex-col">
                <input value="<?php echo $stripeCUrl;?>" name="stripeApi_cUrl" class="form-control" required/>
            </div>
        </div>
    </div>
    <footer class="site-modal-footer">
        <div class="flex-row vertical-center margin-between">
            <div class="flex-col fix-col ml-md-auto">
                <button type="submit" class="btn create-btn">Save</button>
            </div>
            <div class="flex-col fix-col">
                <button type="button" class="btn cancel-close">Cancel</button>
            </div>
        </div>
    </footer>
</form>
<div class="loading" style="display: none;">Loading&#8230;</div>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=1.2"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/editSeries-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var isSeriesMine = <?php echo json_encode($isSeriesMine);?>;
    var initialSeries = <?php echo json_encode($series);?>;
    var initialPurchase = <?php echo json_encode($purchase);?>;
    const DEFAULT_IMAGE = '<?php echo $default_image;?>';
    const PANEL = <?php echo json_encode($panel);?>;
    var isStripeAvailable = <?php echo json_encode($isStripeAvailable);?>;
    var initialFormFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/editSeries-page.js?version=<?php echo time();?>"></script>

</body>
</html>