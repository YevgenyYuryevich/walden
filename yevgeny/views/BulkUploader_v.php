<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - thegreyshirt</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/bulkUploader-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/bulkUploader-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-switchSubscription">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Create a unique Post
                </div>
            </div>
            <main class="main-content">
                <h2 class="text-center">Please Select files that you'd like to upload</h2>
                <div class="upload-section">
                    <div class="actions-container">
                        <div class="flex-row vertical-center margin-between space-between">
                            <div class="flex-col fix-col">
                                <div href="javascript:;" class="add-new-files action">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                    <g>
                                        <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"></path>
                                        <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"></path>
                                    </g>
                                </svg>
                                    Add new files
                                    <input type="file" name="files[]" multiple/>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <button class="btn btn-lg btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Upload All</span>
                                </button>
                                <button class="btn btn-lg btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancel All</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <ul class="files-list">
                        <li class="file-row sample" hidden>
                            <div class="flex-row vertical-center space-between margin-between">
                                <div class="flex-col fix-col">
                                    <div class="file-image-wrapper">
                                        <img class="file-image" src="assets/images/global-icons/cloud-uploading.png">
                                        <input type="file" class="image-input"/>
                                    </div>
                                </div>
                                <div class="flex-col">
                                    <input class="file-title" placeholder="My Favorite.mp4"/>
                                </div>
                                <div class="flex-col fix-col" hidden>
                                    <div class="file-thumb-wrapper">
                                        <div class="file-thumb">
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="file-type">Video</div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="actions">
                                        <button class="btn btn-lg btn-primary preview">
                                            <i class="glyphicon glyphicon-music"></i>
                                            <span>Preview</span>
                                        </button>
                                        <button class="btn btn-lg btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Upload</span>
                                        </button>
                                        <button class="btn btn-lg btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                        <button class="btn btn-lg btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Delete</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <footer class="content-footer">
                        <button class="btn btn-circle return-btn" type="button">Return to Edit Series</button>
                    </footer>
                </div>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="view-widget maximize-view">
    <div class="widget-inner">
        <header class="widget-header view-only-min">
            <div class="flex-row space-between margin-between vertical-center">
                <div class="flex-col">
                    <div class="post-title"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_max"></div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_close"></div>
                    </div>
                </div>
            </div>
        </header>
        <aside class="widget-top-control view-only-max">
            <div class="mv_top_button mv_top_close">
                <div class="mv_close_icon"></div>
            </div>
            <div class="mv_top_button mv_top_minimize">
                <div class="mv_minimize_icon"></div>
            </div>
        </aside>
        <div class="widget-body">
            <div class="video-widget sample" hidden>
                <video data-autoresize="fit" width="960" height="540" data-autoplay="true">
                    <source />
                </video>
            </div>
            <div class="audio-widget sample" hidden>
                <div class="audiogallery">
                    <div class="items">
                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                    </div>
                </div>
            </div>
            <div class="image-widget sample" hidden></div>
            <div class="text-widget sample" hidden></div>
            <div class="form-widget sample" hidden></div>
        </div>
        <footer class="widget-footer view-only-max">
            <div class="post-title"></div>
        </footer>
    </div>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/bulkUploader-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    const seriesId = <?php echo json_encode($seriesId);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/bulkUploader-page.js?version=<?php echo time();?>"></script>

</body>
</html>