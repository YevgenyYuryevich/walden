<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.12.2018
 * Time: 05:46
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Create a content channel on Walden.ly</title>
    <meta name="description" content="Create a content channel that highlights your area of expertise and helps others.">

    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION; ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/css/yevgeny/home-common.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/css/yevgeny/create_series-global.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/PostsStructureExplainer/PostsStructureExplainer.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/PostsStructure/PostsStructure.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormLoopEditor/FormLoopEditor.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormsList/FormsList.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormField/FormField.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/PostCreator/PostCreator.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/css/yevgeny/create_series-page.css?version=<?php echo time(); ?>">
</head>

<body>
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : ''; ?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php'; ?>
    <div class="site-content">
        <header class="content-header">
            <div class="content-header-content">
                <div class="flex-row align-items-end space-between">
                    <div class="flex-col fix-col">
                        <div class="title-wrapper">
                            <h2 class="page-title">Create a Content Channel</h2>

                            <div class="step-wrapper">
                                step <span class="step-value">1</span>
                            </div>
                            <!---
                            <div class="step-name">Set a alue for your membership service</div>
--->
                        </div>
                    </div>
                    <div class="flex-col d-xs-none min-width-0">
                        <div class="step-progress-wrapper step-progress-1">
                            <div class="progress-wrapper">
                                <div class="progress-item step-1 active right-dotted">
                                    <div class="step-wrapper"><span class="step-value">1.</span> Set Payment Options</div>
                                </div>
                                <div class="progress-item step-2 right-dotted">
                                    <div class="step-wrapper"><span class="step-value">2.</span> Describe your Channel</div>
                                </div>
                                <div class="progress-item step-3 right-dotted">
                                    <div class="step-wrapper"><span class="step-value">3.</span> Add Content</div>
                                </div>
                                <div class="progress-item step-4">
                                    <div class="step-wrapper"><span class="step-value">4.</span> Tell People</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <aside class="posts-count-box flex-centering align-items-end">
                            <span class="posts-count">0</span>
                        </aside>
                    </div>
                </div>
            </div>
        </header>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="step-0-membership-charge">
                <div class="step-0-membership-charge-pane step-pane">
                    <div class="pane-title">Would you like to charge a Payment?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/membership-charge.png"
                                         alt=""/>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">Would you like to charge<br> a monthly payment fee for
                                        membership?
                                    </div>
                                    <div class="yes-no-wrap">
                                        <button class="btn btn-yes">Yes</button>
                                        <button class="btn btn-no">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-0-charge-type">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-0-charge-type-pane step-pane">
                    <div class="pane-title">Would you like to charge <br>a monthly fee or one-time fee?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/membership-charge.png"
                                         alt=""/>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">Would you like to charge <br>a monthly fee or one-time fee?
                                    </div>
                                    <div class="btn__wrap">
                                        <button class="btn btn-monthly">
                                            <div class="charge-type__value">Monthly</div>
                                            <div class="great__text">Great for On-Demand</div>
                                        </button>
                                        <button class="btn btn-onetime">
                                            <div class="charge-type__value">One-Time</div>
                                            <div class="great__text">Great for Bootcamps</div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-0-onetime-value">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-0-onetime-value-pane step-pane">
                    <div class="pane-title">How much would you like to make?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/membership-value.png"
                                         alt=""/>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">How much would you<br> like to charge?</div>
                                    <div class="value-wrap">
                                        <div class="flex-row align-items-center margin-between">
                                            <div class="flex-col fix-col">
                                                <div class="end-value">$0</div>
                                            </div>
                                            <div class="flex-col">
                                                <input type="range" name="intSeries_price" min="0" max="250"
                                                       value="45"/>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="end-value">$45</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="next-wrap">
                                        <button class="btn btn-yes">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-0-membership-value">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-0-membership-value-pane step-pane">
                    <div class="pane-title">How much would you like to make?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/membership-value.png"
                                         alt=""/>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">How much would you<br> like to charge per month?</div>
                                    <div class="value-wrap">
                                        <div class="flex-row align-items-center margin-between">
                                            <div class="flex-col fix-col">
                                                <div class="end-value">$0</div>
                                            </div>
                                            <div class="flex-col">
                                                <input type="range" name="charge_per_month" min="0" max="250"
                                                       value="45"/>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="end-value">$45</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="next-wrap">
                                        <button class="btn btn-yes">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-0-affiliate-charge">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-0-affiliate-charge-pane step-pane">
                    <div class="pane-title">How much would you like to make?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/affiliate-charge.png"
                                         alt=""/>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">Would you like to use affiliates<br> to market for you
                                        and expand your reach?
                                    </div>
                                    <div class="yes-no-wrap">
                                        <button class="btn btn-yes">Yes</button>
                                        <button class="btn btn-no">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-0-affiliate-value">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="#step-0-affiliate-charge" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-0-affiliate-value-pane step-pane">
                    <div class="pane-title">How much would you like to make?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/affiliate-charge.png"
                                         alt=""/>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">How much would you<br> like to pay affiliates per
                                        conversion?
                                    </div>
                                    <div class="value-wrap">
                                        <div class="flex-row margin-between align-items-center">
                                            <div class="flex-col fix-col">
                                                <div class="end-value-wrap">
                                                    <div class="end-value you-value">$24</div>
                                                    <div>You</div>
                                                </div>
                                            </div>
                                            <div class="flex-col">
                                                <input type="range" name="charge_per_conversion" min="0" max="250"
                                                       value="45"/>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="end-value-wrap">
                                                    <div class="end-value affiliate-value">$6</div>
                                                    <div>Affiliate</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="next-wrap">
                                        <button class="btn btn-yes">Next >></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-0-stripe-connect">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-0-stripe-connect-pane step-pane">
                    <div class="pane-title">How much would you like to make?</div>
                    <div class="pane-sub-title">(You can always change this later)</div>
                    <div class="pane-content">
                        <div class="flex-row align-items-center d-block d-md-flex">
                            <div class="flex-col fix-col">
                                <div class="pane-image">
                                    <img src="assets/images/global-icons/powered_by_stripe.svg" alt="" />
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="pane-main">
                                    <div class="pane-description">We use Stripe Connect to process payments. Please
                                        login to stripe to complete the setup process so payments will go directly to
                                        you from stripe.
                                    </div>
                                    <div class="connect-account-wrapper">
                                        <a target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect');?>" class="stripe-connect-btn">
                                            <img class="img-normal" src="assets/images/global-icons/stripe-connect-light-on-light@2x.png" alt="">
                                            <img class="img-focus" src="assets/images/global-icons/stripe-connect-light-on-dark@2x.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="#step-3-create-series" class="slider-nav flex-centering arrow-right" data-toggle="tab">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-select-from">
                <div class="step-1-select-from-pane step-pane">
                    <div class="pane-title">Add amazing content for your clients.</div>
                    <div class="pane-sub-title">What great content would you like to add first?<br>(You can always do this later)</div>
                    <div class="flex-row d-xs-block">
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-scratch" data-toggle="tab" class="from-item item-from-scratch">
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromScratch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Content from an Idea</div>
                                <div class="item-sub-title">Craft your idea from scratch</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-search" data-toggle="tab" class="from-item item-from-search">
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromSearch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Content from the web</div>
                                <div class="item-sub-title">Select existing content from youtube, blogs, and podcasts
                                </div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0">
                            <a href="#step-1-add-from-upload" data-toggle="tab" class="from-item item-from-upload">
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/BulkUpload.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Upload Content from existing files and your favorite URLs</div>
                                <div class="item-sub-title">Upload docs, pdfs, videos, and audio files.</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-from-scratch">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-from-scratch-pane step-pane">
                    <div class="pane-sub-title">Let's create a collection of great resources about something you love
                    </div>
                    <div class="identify-board">
                        <div class="flex-row vertical-center d-xs-block">
                            <div class="flex-col fix-col">
                                <div class="board-img-wrapper">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromScratch.svg"
                                         alt=""/>
                                    <div class="board-title">Create content from scratch</div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="board-txt">
                                    Creating content from scratch is just like writing a blog
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="" class="control-label">Name your post:</label>
                            <input type="text" class="form-control" name="strPost_title"
                                   placeholder="type the name here"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Subtitle:</label>
                            <input type="text" class="form-control" name="strPost_subtitle"
                                   placeholder="type the subtitle"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Body:</label>
                            <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                        </div>
                        <div class="flex-row flex-wrap margin-between d-xs-block">
                            <div class="flex-col fb-0 mr-2 mr-xs-0">
                                <div class="form-group">
                                    <label for="" class="control-label">Post Type:</label>
                                    <div class="dropdown" data-value="7">
                                        <button class="btn btn-default dropdown-toggle" type="button"
                                                id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                            <span>Text</span>
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1"
                                                 x="0px" y="0px" viewBox="0 0 512 512"
                                                 style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z"
                                                                      fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:;" data-value="0">Audio</a></li>
                                            <li><a href="javascript:;" data-value="2">Video</a></li>
                                            <li hidden><a href="javascript:;" data-value="7">Text</a></li>
                                            <li><a href="javascript:;" data-value="8">Image</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fb-0">
                                
                                <!---
                                <div class="form-group">
                                    <label for="" class="control-label">
                                        Money Saved, $ <span>(optional)</span>:
                                    </label>
                                    <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                </div>
                                --->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Keywords (optional):</label>
                            <input type="text" class="form-control" name="strPost_keywords"
                                   placeholder="What keywords will help people find your content?"/>
                            <div class="terms-container flex-row margin-between flex-wrap">
                                <div class="flex-col fix-col term-item sample" hidden>#design</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload Cover Image for this Post (optional)</label>
                            <div class="cover-image-wrapper">
                                <div class="flex-row margin-between horizon-center vertical-center">
                                    <div class="flex-col fix-col">
                                        <div class="img-wrapper">
                                            <img src="assets/images/global-icons/cloud-uploading.png">
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="upload-helper-txt">Drag Image here to upload</div>
                                    </div>
                                </div>
                                <input class="form-control" type="file" name="strPost_featuredimage"/>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="save-btn">Create</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-from-search">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-from-search-pane step-pane">
                    <div class="pane-sub-title">Let's create a collection of great resources about something you love
                    </div>
                    <div class="identify-board">
                        <div class="flex-row vertical-center d-xs-block">
                            <div class="flex-col fix-col">
                                <div class="board-img-wrapper">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromSearch.svg"
                                         alt=""/>
                                    <div class="board-title">post from search</div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <form class="search-input-wrapper flex-row margin-between vertical-center">
                                    <input class="flex-col search-input" required
                                           placeholder="Type search term here...">
                                    <button class="search-submit-button fix-col">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1"
                                             x="0px" y="0px" viewBox="0 0 76.652 76.652"
                                             style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z"
                                                                  fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <section class="search-section-wrapper" hidden>
                        <div class="your-search-title">Your Search Results</div>
                        <div class="search-container">
                            <div class="search-row sample" hidden>
                                <header class="search-row-header">
                                    <div class="flex-row vertical-center space-between">
                                        <div class="flex-col fix-col">
                                            <h2 class="from-name">Video</h2>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a href="javascript:;" class="items-found-wrapper">
                                                <span class="found-value">0</span> items found
                                            </a>
                                        </div>
                                    </div>
                                </header>
                                <div class="search-row-body">
                                    <div class="search-item sample" hidden>
                                        <div class="item-img-wrapper">
                                            <img class="item-img"/>
                                            <aside class="blog-duration">
                                                waiting...
                                            </aside>
                                            <div class="hover-content flex-centering">
                                                <svg version="1.1" id="Layer_1"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="49px" height="38px"
                                                     viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38"
                                                     xml:space="preserve">
<title>Rectangle 4</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id="Page-1" sketch:type="MSPage">
                                                        <g id="Check_1" transform="translate(115.000000, 102.000000)"
                                                           sketch:type="MSLayerGroup">
                                                            <path id="Rectangle-4" sketch:type="MSShapeGroup"
                                                                  fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
			c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
			c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                                        </g>
                                                    </g>
</svg>
                                            </div>
                                        </div>
                                        <a href="javascript:;" class="item-title">
                                            Daily recipes
                                        </a>
                                    </div>
                                    <div class="flex-row margin-between">
                                        <div class="flex-col fix-col overflow-visible">
                                            <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                            </a>
                                        </div>
                                        <div class="flex-col fb-0">
                                            <div class="search-items-list">
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col overflow-visible">
                                            <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="add-selected-btn">Add selected</button>
                        </div>
                        <div class="still-not-find-txt">Still didn't find what were looking for?</div>
                        <div class="btn-wrapper">
                            <button class="load-more-btn btn">Load more</button>
                        </div>
                    </section>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none" hidden>
                    <a href="#step-2-arrange" data-toggle="tab" class="slider-nav flex-centering arrow-right">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-from-upload">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-from-upload-pane step-pane">
                    <div class="pane-sub-title">Let's create a collection of great resources about something you love
                    </div>
                    <div class="identify-board">
                        <div class="flex-row vertical-center d-xs-block">
                            <div class="flex-col fix-col">
                                <div class="board-img-wrapper">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/BulkUpload.svg"
                                         alt=""/>
                                    <div class="board-title">Upload Content</div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="upload-input-wrapper">
                                    <div class="file-input-wrapper">
                                        <div class="drag-urls-txt">
                                            Drag URLs or files here to upload or click here to select your files
                                        </div>
                                        <div class="cloud-wrapper">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 184.979 184.979"
                                                 style="enable-background:new 0 0 184.979 184.979;"
                                                 xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path style="fill:#fff;" d="M121.539,122.24c0.585,0.585,1.349,0.871,2.112,0.871c0.764,0,1.528-0.292,2.112-0.871
                                                            c1.164-1.164,1.164-3.055,0-4.219L94.605,86.869c0,0,0,0-0.006-0.006l-2.106-2.106l-2.106,2.106c0,0,0,0-0.006,0.006
                                                            l-31.159,31.153c-1.164,1.164-1.164,3.055,0,4.219c1.164,1.164,3.055,1.164,4.219,0l26.069-26.063v52.556
                                                            c0,1.647,1.337,2.983,2.983,2.983c1.647,0,2.983-1.337,2.983-2.983V96.177L121.539,122.24z"/>
                                                        <path style="fill:#fff;" d="M151.057,67.673C143.712,47.01,124.32,33.25,102.236,33.25c-20.812,0-39.531,12.518-47.628,31.493
                                                            c-3.813-1.766-7.96-2.685-12.172-2.685c-16.051,0-29.118,13.044-29.148,29.088C4.941,97.531,0,107.448,0,117.998
                                                            c0,18.593,15.168,33.719,34.31,33.725c0.215,0,0.43,0.006,0.621,0.006c0.203,0,0.382,0,0.525-0.012h31.445
                                                            c1.647,0,2.983-1.337,2.983-2.983s-1.337-2.983-2.983-2.983H35.3c-0.292,0.018-0.597,0.006-0.889,0l-0.609-0.006
                                                            c-15.347,0-27.835-12.447-27.835-27.752c0-9.147,4.511-17.722,12.065-22.925l1.372-1.366l-0.149-2.494
                                                            c0-12.781,10.4-23.181,23.181-23.181c4.284,0,8.491,1.205,12.166,3.479l3.156,1.957l1.229-3.509
                                                            c6.432-18.384,23.808-30.729,43.254-30.729c20.073,0,37.639,12.847,43.707,31.976l0.555,1.748l1.814,0.292
                                                            c17.781,2.876,30.694,18.032,30.694,36.04c0,20.114-16.403,36.475-36.559,36.475h-24.375c-1.647,0-2.983,1.337-2.983,2.983
                                                            s1.337,2.983,2.983,2.983h24.375c23.45,0,42.526-19.04,42.526-42.442C184.973,88.933,170.814,71.719,151.057,67.673z"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <input type="file" class="file-input"/>
                                    </div>
                                    <div class="url-input-wrapper">
                                        <aside class="enter-icon-wrapper flex-centering">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 26 26" style="enable-background:new 0 0 26 26;"
                                                 xml:space="preserve">
                                                    <g>
                                                        <path d="M25,2H9C8.449,2,8,2.449,8,3c0,0,0,7,0,9s-2,2-2,2H1c-0.551,0-1,0.449-1,1v8c0,0.551,0.449,1,1,1h24
                                                            c0.551,0,1-0.449,1-1V3C26,2.449,25.551,2,25,2z M22,14c0,1.436-1.336,4-4,4h-3.586l1.793,1.793c0.391,0.391,0.391,1.023,0,1.414
                                                            C16.012,21.402,15.756,21.5,15.5,21.5s-0.512-0.098-0.707-0.293l-3.5-3.5c-0.391-0.391-0.391-1.023,0-1.414l3.5-3.5
                                                            c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L14.414,16H18c1.398,0,2-1.518,2-2v-2c0-0.553,0.447-1,1-1s1,0.447,1,1V14z"></path>
                                                    </g>
                                                </svg>
                                        </aside>
                                        <input type="text" class="form-control url-input" placeholder="Paste URL"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pane-body">
                        <div class="upload-items-title-wrapper">
                            <div class="flex-row space-between vertical-center">
                                <div class="flex-col">
                                    <h2 class="uploaded-items-txt">Uploaded Items</h2>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="uploaded-cnt bottom-underline">
                                        <span class="cnt-value">0</span> items uploaded
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="drag-title-wrapper">
                            <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/move.png" alt=""> <span
                                    class="drag-title-txt">Drag on items to change the order</span>
                        </div>
                        <div class="items-container">
                            <div class="upload-term-item sample" hidden>
                                <div class="flex-row margin-between vertical-center flex-wrap">
                                    <div class="flex-col fix-col">
                                        <a class="item-action delete-action" href="javascript:;">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="612px" height="612px" viewBox="0 0 612 612"
                                                 style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                        </a>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="item-img-wrapper">
                                            <img class="item-img" />
                                            <input type="file" class="image-input"/>
                                        </div>
                                    </div>
                                    <div class="flex-col">
                                        <div class="form-group">
                                            <input type="text" class="form-control item-title-input"/>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col data-generate-col">
                                        <button class="site-green-btn keep-btn">Keep format</button>
                                    </div>
                                    <div class="flex-col fix-col data-generate-col">
                                        <button class="site-green-btn convert-btn">Convert</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pane-footer">
                        <div class="button-wrapper">
                            <button class="save-btn" disabled>Upload all</button>
                        </div>
                        <div class="changed-txt">if you changed your mind to upload</div>
                        <div class="btn-wrapper">
                            <button class="cancel-all-btn btn">Cancel all</button>
                        </div>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none" hidden>
                    <a href="javascript:;" class="slider-nav flex-centering arrow-right">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-more">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-more-pane step-pane">
                    <div class="pane-title">Looks great! Would you like to add more?</div>
                    <div class="pane-sub-title">(You can always do this later)</div>
                    <div class="flex-row d-xs-block">
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-scratch" data-toggle="tab" class="from-item item-from-scratch">
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromScratch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Create content from scratch</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-search" data-toggle="tab" class="from-item item-from-search">
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromSearch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">post from search</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0">
                            <a href="#step-1-add-from-upload" data-toggle="tab" class="from-item item-from-upload">
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/BulkUpload.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Upload Content</div>
                            </a>
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <button class="go-next-btn" data-toggle="tab" data-target="#step-2-arrange">Go to next step
                        </button>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="#step-2-arrange" data-toggle="tab" class="slider-nav flex-centering arrow-right">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-2-arrange">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                        <span>Add More Content</span>
                    </a>
                </div>
                <div class="step-2-arrange-pane step-pane">
                    <div class="pane-title">Great work! Would you like to arrange your content?</div>
                    <div class="pane-sub-title">(This will allow subscribers to view your content in a specific order. You can always do this later)</div>
                    <?php require_once ASSETS_PATH . '/components/PostsStructureExplainer/PostsStructureExplainer.html'; ?>
                    <?php require_once ASSETS_PATH . '/components/PostsStructure/PostsStructure.php'; ?>
                    <div class="pane-footer">
                        <div class="all-set-txt">All set!</div>
                        <div class="button-wrapper">
                            <button class="save-btn">Looks good!</button>
                        </div>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="#step-4-select-set" data-toggle="tab" class="slider-nav flex-centering arrow-right">
                        <span>Step 4</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-3-create-series">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                        <span>Change Payment</span>
                    </a>
                </div>
                <form class="step-3-create-series-pane step-pane">
                    <div class="identify-board">
                        <div class="ready-finish-title">Great now let's create the content channel<br>
                            and start giving great content to your clients.
                        </div>
                        <div class="board-img-wrapper">
                            <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostCreated.svg" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Name your Content Channel:</label>
                        <input placeholder="type your name here" type="text" name="strSeries_title" class="form-control"
                               data-toggle="popover" data-content="type experience title" required/>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Describe it:</label>
                        <div class="multi-line-background">
                            <textarea class="about-input" rows="4" data-toggle="popover"
                                      data-content="Please describe your content channel" required
                                      placeholder="Please describe your content channel"
                                      name="strSeries_description"></textarea>
                            <div class="background-placeholder"></div>
                        </div>
                    </div>
                    <div class="form-group" data-toggle="popover" data-content="Select the level that your channel fits into best">
                        <label class="control-label">Would you like to create a bootcamp, competition, or on-demond session?</label>
                        <select name="strSeries_level" class="form-control">
                            <option value="Bootcamp">Bootcamp</option>
                            <option value="On Demand">On Demand</option>
                            <option value="Competition">Competition</option>
                        </select>
                    </div>
                    <div class="form-group" data-toggle="popover" data-content="Select the category that your channel fits into best">
                        <label class="control-label">Choose Category:</label>
                        <select name="intSeries_category" class="form-control">
                            <?php foreach ($categories as $category): ?>
                                <option value="<?php echo $category['category_ID']; ?>"><?php echo $category['strCategory_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="flex-row margin-between flex-wrap">
                        <div class="flex-col">
                            <div class="form-group">
                                <label class="control-label">Make it available to others?</label>
                                <label class="c-switch" data-toggle="popover"
                                       data-content="Please select whether your content should be Public or Private">
                                    <input type="checkbox" name="boolSeries_isPublic" value="1" checked>
                                    <div class="slider round"></div>
                                </label>
                                <span class="public-value-txt">Public</span>
                            </div>
                        </div>
                        <div class="flex-col">
                            <div class="form-group">
                                <label class="control-label">Days when your channel will appear on Subscriber's feeds:</label>
                                <div class="flex-row margin-between flex-wrap" data-toggle="popover"
                                     data-content="Days when you would like your channel to appear in your subscriber's feeds.">
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-1" type="checkbox" checked/><label for="day-1">Mon</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-2" type="checkbox" checked/><label for="day-2">Tue</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-3" type="checkbox" checked/><label for="day-3">Wed</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-4" type="checkbox" checked/><label for="day-4">Thu</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-5" type="checkbox" checked/><label for="day-5">Fri</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-6" type="checkbox"/><label for="day-6">Sat</label>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="day-item">
                                            <input id="day-0" type="checkbox" checked/><label for="day-0">Sun</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Upload Cover Image (A default image will be selected if you do not upload one.)</label>
                        <div class="cover-image-wrapper" data-toggle="popover"
                             data-content="Drag and Drop an Image here to upload it.">
                            <div class="flex-row margin-between horizon-center vertical-center">
                                <div class="flex-col fix-col">
                                    <div class="img-wrapper">
                                        <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/cloud-uploading.png">
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="upload-helper-txt">Drag Image here or click to upload</div>
                                </div>
                            </div>
                            <input class="form-control" type="file" name="strSeries_image">
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <button class="save-btn">Looks good</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="step-4-select-set">
                <div class="step-4-select-set-pane step-pane">
                    <div class="pane-title">Your new Content Channel is ready to go!</div>
                    <div class="pane-sub-title">Here are some great next steps</div>
                    <div class="flex-row d-xs-block">
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-invite" data-toggle="tab" class="set-item item-invite">
                                <div class="item-title">Invite Co-Creators</div>
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/step-enrich.svg"
                                         alt="">
                                </div>
                                <div class="item-sub-title">Invite colleagues, friends, or family to co-create with you.
                                </div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-share" data-toggle="tab" class="set-item item-share">
                                <div class="item-title">Share on social media</div>
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/blogger_home_hero.png"
                                         alt="">
                                </div>
                                <div class="item-sub-title">Share your Content Channel with people who will benefit from it.</div>
                            </a>
                        </div>
                        
                        <!---
                        <div class="flex-col fb-0 min-width-0">
                            <a href="#step-0-membership-charge" data-toggle="tab" class="set-item item-charge">
                                <div class="item-title">Charge for your work</div>
                                <div class="item-img-wrapper">
                                    <img class="item-img"
                                         src="<?php echo BASE_URL; ?>/assets/images/global-icons/gold-multi-storey.svg"
                                         alt="">
                                </div>
                                <div class="item-sub-title">Charge people to join your experience and pay affiliates to
                                    market it for you.
                                </div>
                            </a>
                        </div>
                        --->
                        
                    </div>
                    <div class="button-wrapper">
                        <a href="javascript:;" class="go-next-btn">Finished! Let's see it!</a>
                    </div>
                    <div class="actually-link-wrapper">
                        <a href="javascript:;" class="actually-link">Actually, let me edit it</a>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-right">
                        <span>View&nbsp;&nbsp;</span>
                        <div class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-charge">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-charge-pane step-pane">
                    <div class="pane-title">Charge users to view your Content Channel</div>
                    <div class="pane-sub-title">Connect through Stripe to get paid when users join your Content Channel</div>
                    <div class="charge-series-wrapper">
                        <div class="flex-row margin-between vertical-center">
                            <div class="flex-col">
                                <div class="charge-content">
                                    <div class="connect-account-wrapper">
                                        <a target="_blank"
                                           href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId ?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect'); ?>"
                                           class="stripe-connect"><span>Connect with Stripe</span></a> to charge for
                                        your series
                                    </div>
                                    <div class="group-row-wrapper charge-users-wrapper">
                                        <div class="flex-row margin-between space-between">
                                            <div class="flex-col fix-col">
                                                <div class="would-charge-wrapper group-col toggle-col">
                                                    <div class="would-charge-txt col-name">Would you like to charge
                                                        users?
                                                    </div>
                                                    <div class="flex-row vertical-center margin-between">
                                                        <div class="flex-col fix-col">
                                                            <label class="c-switch">
                                                                <input type="checkbox" name="boolSeries_charge"
                                                                       value="1">
                                                                <div class="slider round"></div>
                                                            </label>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="get-payment-txt">you will get payment</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col center-col">
                                                <div class="charge-price-wrapper group-col">
                                                    <label class="price-label col-name">Price, $:</label>
                                                    <input type="text" name="intSeries_price" class="col-value"
                                                           placeholder="0"/>
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="you-will-get-wrapper group-col gets-col">
                                                    <label class="you-will-get-label col-name">You will get:</label>
                                                    <div class="you-will-get-value col-value"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pay-affiliates-wrapper group-row-wrapper">
                                        <div class="flex-row margin-between space-between">
                                            <div class="flex-col fix-col">
                                                <div class="would-pay-affiliate-wrapper group-col toggle-col">
                                                    <div class="would-pay-txt col-name">Would you like to Pay Affiliates
                                                        for Referrals?
                                                    </div>
                                                    <div class="flex-row vertical-center margin-between">
                                                        <div class="flex-col fix-col">
                                                            <label class="c-switch">
                                                                <input type="checkbox" name="boolSeries_affiliated"
                                                                       value="1">
                                                                <div class="slider round"></div>
                                                            </label>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="get-payment-txt">Affiliate will get fee</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col center-col overflow-visible">
                                                <div class="affiliate-percent-wrapper group-col">
                                                    <label class="percent-label col-name">Percent, %:</label>
                                                    <input class="percent-value" name="intSeries_affiliate_percent"
                                                           type="range" max="100" value="30"/>
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="affiliate-will-get-wrapper group-col gets-col">
                                                    <label class="affiliate-will-get-label col-name">Affiliate
                                                        gets:</label>
                                                    <div class="affiliate-will-get-value col-value"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="step-description">
                                    To charge for your series, you must setup strpe public API in your settings
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <a href="javascript:;" class="go-next-btn preview-series-link" target="_blank">Finished! Let's
                            see it!</a>
                    </div>
                    <div class="actually-link-wrapper">
                        <a href="javascript:;" class="actually-link edit-series-link" target="_blank">Actually, let me
                            edit it</a>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-right preview-series-link"
                       target="_blank">
                        <span>View&nbsp;&nbsp;</span>
                        <div class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-invite">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-invite-pane step-pane">
                    <div class="pane-title">Doing things with friends is always better</div>
                    <div class="pane-sub-title">Invite colleaguesto add to contribute to your Content Channel</div>
                    <div class="invite-friend-wrapper">
                        <div class="flex-row space-between vertical-center">
                            <div class="flex-col fix-col">
                                <div class="invite-content">
                                    <h4 class="invite-txt">Invite a colleague to co-create with you</h4>
                                    <div class="to-add-txt">Create and curate content with colleagues.</div>
                                    <form class="invite-friend-form" method="post">
                                        <div class="input-wrapper">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col">
                                                    <input type="text" name="emails"
                                                           placeholder="Type emails of colleagues (separate with a comma)" required>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <button class="btn" type="submit">
                                                        <svg version="1.1" id="Layer_1"
                                                             xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 512 512"
                                                             style="enable-background:new 0 0 512 512;"
                                                             xml:space="preserve">
                                                            <polygon style="fill:#5EBAE7;"
                                                                     points="490.452,21.547 16.92,235.764 179.068,330.053 179.068,330.053 "/>
                                                            <polygon style="fill:#36A9E1;"
                                                                     points="490.452,21.547 276.235,495.079 179.068,330.053 179.068,330.053 "/>
                                                            <rect x="257.137" y="223.122"
                                                                  transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 277.6362 609.0793)"
                                                                  style="fill:#FFFFFF;" width="15.652" height="47.834"/>
                                                            <path style="fill:#1D1D1B;" d="M0,234.918l174.682,102.4L277.082,512L512,0L0,234.918z M275.389,478.161L190.21,332.858
                                                                l52.099-52.099l-11.068-11.068l-52.099,52.099L33.839,236.612L459.726,41.205L293.249,207.682l11.068,11.068L470.795,52.274
                                                                L275.389,478.161z"/>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <aside class="plane-wrapper">
                                        <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/plane.png"
                                             alt="">
                                    </aside>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="invite-image-wrapper">
                                    <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/invite_illustration.png"
                                         alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="co-owners-wrapper">
                        <div class="form-group">
                            <label for="" class="control-label">Co-Creators</label>
                            <label class="c-switch">
                                <input type="checkbox" class="owner-switch" value="1">
                                <div class="slider round"></div>
                                <div class="flex-centering value-text-wrapper">
                                    <div class="value-txt accepted-txt">accepted</div>
                                    <div class="value-txt invited-txt">invited</div>
                                </div>
                            </label>
                        </div>
                        <div class="owners-container flex-row flex-wrap">
                            <article class="owner-item sample flex-col fix-col" hidden>
                                <header class="item-header">
                                    <div class="flex-row vertical-center margin-between">
                                        <div class="flex-col">
                                            <div class="role-txt">
                                                Creator
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a class="item-action edit-action flex-centering" href="javascript:;">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329"
                                                     style="enable-background:new 0 0 540.329 540.329;"
                                                     xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon
                                                                                                    points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon
                                                                                                    points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                            </a>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a class="item-action delete-action" href="javascript:;">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="612px" height="612px" viewBox="0 0 612 612"
                                                     style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                            </a>
                                        </div>
                                    </div>
                                </header>
                                <div class="item-img-wrapper">
                                </div>
                                <div class="user-name">Someone's Name</div>
                            </article>
                            <div class="flex-col fix-col add-friend-card">
                                <div class="flex-centering add-friend-inner">
                                    <div class="form-group">
                                        <label class="control-label">Invite another Colleague</label>
                                        <div class="input-wrapper">
                                            <aside class="enter-icon-wrapper flex-centering">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 26 26" style="enable-background:new 0 0 26 26;"
                                                     xml:space="preserve">
                                                    <g>
                                                        <path d="M25,2H9C8.449,2,8,2.449,8,3c0,0,0,7,0,9s-2,2-2,2H1c-0.551,0-1,0.449-1,1v8c0,0.551,0.449,1,1,1h24
                                                            c0.551,0,1-0.449,1-1V3C26,2.449,25.551,2,25,2z M22,14c0,1.436-1.336,4-4,4h-3.586l1.793,1.793c0.391,0.391,0.391,1.023,0,1.414
                                                            C16.012,21.402,15.756,21.5,15.5,21.5s-0.512-0.098-0.707-0.293l-3.5-3.5c-0.391-0.391-0.391-1.023,0-1.414l3.5-3.5
                                                            c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L14.414,16H18c1.398,0,2-1.518,2-2v-2c0-0.553,0.447-1,1-1s1,0.447,1,1V14z"
                                                        />
                                                    </g>
                                                </svg>
                                            </aside>
                                            <input type="text" class="form-control add-input"
                                                   placeholder="type email and press">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <a href="javascript:;" class="go-next-btn preview-series-link" target="_blank">Finished! Let's
                            see it!</a>
                    </div>
                    <div class="actually-link-wrapper">
                        <a href="javascript:;" class="actually-link edit-series-link" target="_blank">Actually, let me
                            edit it</a>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-right preview-series-link"
                       target="_blank">
                        <span>View&nbsp;&nbsp;</span>
                        <div class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-share">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-share-pane step-pane">
                    <div class="pane-title">How would you like to share your new channel?</div>
                    <div class="pane-sub-title">(You can always do this later)</div>
                    <div class="share-icons-wrapper">
                        <div class="flex-row space-between vertical-center flex-wrap">
                            <div class="flex-col fix-col">
                                <a target="_blank" href="javascript:;" class="share-item item-facebook">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.196 112.196"
                                         style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                <g>
                                    <circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
                                    <path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                        c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                </g>
                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a target="_blank" href="javascript:;" class="share-item item-twitter">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.197 112.197"
                                         style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
                                                c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
                                                c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
                                                c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
                                                c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
                                                c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
                                                c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
                                                C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                                        </g>
                                    </g>
                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a target="_blank" href="javascript:;" class="share-item item-linkedin">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.196 112.196"
                                         style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
                                                c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
                                                c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
                                                C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
                                                c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
                                                 M27.865,83.739H41.27V43.409H27.865V83.739z"/>
                                        </g>
                                    </g>
                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a target="_blank" href="javascript:;" class="share-item item-google">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.196 112.196"
                                         style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
<g>
    <g>
        <circle id="XMLID_30_" style="fill:#DC4E41;" cx="56.098" cy="56.097" r="56.098"/>
    </g>
    <g>
        <path style="fill:#DC4E41;" d="M19.531,58.608c-0.199,9.652,6.449,18.863,15.594,21.867c8.614,2.894,19.205,0.729,24.937-6.648
			c4.185-5.169,5.136-12.06,4.683-18.498c-7.377-0.066-14.754-0.044-22.12-0.033c-0.012,2.628,0,5.246,0.011,7.874
			c4.417,0.122,8.835,0.066,13.252,0.155c-1.115,3.821-3.655,7.377-7.51,8.757c-7.443,3.28-16.94-1.005-19.282-8.813
			c-2.827-7.477,1.801-16.5,9.442-18.675c4.738-1.667,9.619,0.21,13.673,2.673c2.054-1.922,3.976-3.976,5.864-6.052
			c-4.606-3.854-10.525-6.217-16.61-5.698C29.526,35.659,19.078,46.681,19.531,58.608z"/>
        <path style="fill:#DC4E41;" d="M79.102,48.668c-0.022,2.198-0.045,4.407-0.056,6.604c-2.209,0.022-4.406,0.033-6.604,0.044
			c0,2.198,0,4.384,0,6.582c2.198,0.011,4.407,0.022,6.604,0.045c0.022,2.198,0.022,4.395,0.044,6.604c2.187,0,4.385-0.011,6.582,0
			c0.012-2.209,0.022-4.406,0.045-6.615c2.197-0.011,4.406-0.022,6.604-0.033c0-2.198,0-4.384,0-6.582
			c-2.197-0.011-4.406-0.022-6.604-0.044c-0.012-2.198-0.033-4.407-0.045-6.604C83.475,48.668,81.288,48.668,79.102,48.668z"/>
        <g>
            <path style="fill:#FFFFFF;" d="M19.531,58.608c-0.453-11.927,9.994-22.949,21.933-23.092c6.085-0.519,12.005,1.844,16.61,5.698
				c-1.889,2.077-3.811,4.13-5.864,6.052c-4.054-2.463-8.935-4.34-13.673-2.673c-7.642,2.176-12.27,11.199-9.442,18.675
				c2.342,7.808,11.839,12.093,19.282,8.813c3.854-1.38,6.395-4.936,7.51-8.757c-4.417-0.088-8.835-0.033-13.252-0.155
				c-0.011-2.628-0.022-5.246-0.011-7.874c7.366-0.011,14.743-0.033,22.12,0.033c0.453,6.439-0.497,13.33-4.683,18.498
				c-5.732,7.377-16.322,9.542-24.937,6.648C25.981,77.471,19.332,68.26,19.531,58.608z"/>
            <path style="fill:#FFFFFF;" d="M79.102,48.668c2.187,0,4.373,0,6.57,0c0.012,2.198,0.033,4.407,0.045,6.604
				c2.197,0.022,4.406,0.033,6.604,0.044c0,2.198,0,4.384,0,6.582c-2.197,0.011-4.406,0.022-6.604,0.033
				c-0.022,2.209-0.033,4.406-0.045,6.615c-2.197-0.011-4.396,0-6.582,0c-0.021-2.209-0.021-4.406-0.044-6.604
				c-2.197-0.023-4.406-0.033-6.604-0.045c0-2.198,0-4.384,0-6.582c2.198-0.011,4.396-0.022,6.604-0.044
				C79.057,53.075,79.079,50.866,79.102,48.668z"/>
        </g>
    </g>
</g>
</svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a target="_blank" href="javascript:;" class="share-item item-email">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="512pt" version="1.1"
                                         viewBox="0 0 512 512" width="512pt">
                                        <g id="surface1">
                                            <path d="M 512 256 C 512 397.386719 397.386719 512 256 512 C 114.613281 512 0 397.386719 0 256 C 0 114.613281 114.613281 0 256 0 C 397.386719 0 512 114.613281 512 256 Z M 512 256 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294118%,32.941176%,87.843137%);fill-opacity:1;"/>
                                            <path d="M 512 256 C 512 245.425781 511.347656 235.003906 510.101562 224.765625 L 379.667969 94.332031 L 100.5 409 L 196.542969 505.042969 C 215.625 509.582031 235.527344 512 256 512 C 397.386719 512 512 397.386719 512 256 Z M 512 256 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(22.745098%,25.490196%,80%);fill-opacity:1;"/>
                                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 100.5 409 L 411.5 409 L 411.5 188 Z M 314.953125 82.164062 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(98.039216%,36.470588%,5.882353%);fill-opacity:1;"/>
                                            <path d="M 411.5 188 L 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 409 L 411.5 409 Z M 411.5 188 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(84.705882%,29.803922%,5.882353%);fill-opacity:1;"/>
                                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 197.046875 293.835938 C 228.695312 328.527344 283.304688 328.527344 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;"/>
                                            <path d="M 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 319.847656 C 277.40625 319.910156 299.070312 311.246094 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(92.156863%,92.156863%,94.117647%);fill-opacity:1;"/>
                                            <path d="M 100.5 409 L 197.046875 303.164062 C 228.695312 268.472656 283.304688 268.472656 314.953125 303.164062 L 411.5 409 Z M 100.5 409 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,58.431373%,0%);fill-opacity:1;"/>
                                            <path d="M 314.953125 303.164062 C 299.070312 285.753906 277.40625 277.085938 255.761719 277.152344 L 255.761719 409 L 411.5 409 Z M 314.953125 303.164062 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(91.764706%,50.980392%,2.352941%);fill-opacity:1;"/>
                                            <path d="M 132.332031 94.332031 L 132.332031 222.894531 L 197.046875 293.835938 C 212.871094 311.179688 234.4375 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 132.332031 94.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(90.196078%,90.196078%,90.196078%);fill-opacity:1;"/>
                                            <path d="M 255.761719 94.332031 L 255.761719 319.847656 C 255.839844 319.847656 255.921875 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 255.761719 94.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(84.313725%,84.313725%,84.313725%);fill-opacity:1;"/>
                                            <path d="M 159 124.332031 L 231 124.332031 L 231 211 L 159 211 Z M 159 124.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;"/>
                                            <path d="M 247 124.332031 L 355.667969 124.332031 L 355.667969 139 L 247 139 Z M 247 124.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;"/>
                                            <path d="M 255.761719 124.332031 L 355.667969 124.332031 L 355.667969 139 L 255.761719 139 Z M 255.761719 124.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;"/>
                                            <path d="M 247 160.332031 L 355.667969 160.332031 L 355.667969 175 L 247 175 Z M 247 160.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;"/>
                                            <path d="M 255.761719 160.332031 L 355.667969 160.332031 L 355.667969 175 L 255.761719 175 Z M 255.761719 160.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;"/>
                                            <path d="M 247 196.332031 L 355.667969 196.332031 L 355.667969 211 L 247 211 Z M 247 196.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;"/>
                                            <path d="M 255.761719 196.332031 L 355.667969 196.332031 L 355.667969 211 L 255.761719 211 Z M 255.761719 196.332031 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;"/>
                                            <path d="M 159 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 159 246.40625 Z M 159 231.738281 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;"/>
                                            <path d="M 255.761719 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 255.761719 246.40625 Z M 255.761719 231.738281 "
                                                  style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;"/>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <a href="javascript:;" class="go-next-btn preview-series-link" target="_blank">Finished! Let's
                            see it!</a>
                    </div>
                    <div class="actually-link-wrapper">
                        <a href="javascript:;" class="actually-link edit-series-link" target="_blank">Actually, let me
                            edit it</a>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-right preview-series-link"
                       target="_blank">
                        <span>View&nbsp;&nbsp;</span>
                        <div class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="virtual-links" hidden>
            <a href="#step-0-membership-charge" data-toggle="tab"></a>
            <a href="#step-0-charge-type" data-toggle="tab"></a>
            <a href="#step-0-onetime-value" data-toggle="tab"></a>
            <a href="#step-0-membership-value" data-toggle="tab"></a>
            <a href="#step-0-affiliate-charge" data-toggle="tab"></a>
            <a href="#step-0-affiliate-value" data-toggle="tab"></a>
            <a href="#step-0-stripe-connect" data-toggle="tab"></a>
            <a href="#step-1-select-from" data-toggle="tab"></a>
            <a href="#step-1-add-from-scratch" data-toggle="tab"></a>
            <a href="#step-1-add-from-search" data-toggle="tab"></a>
            <a href="#step-1-add-from-upload" data-toggle="tab"></a>
            <a href="#step-1-add-more" data-toggle="tab"></a>
            <a href="#step-2-arrange" data-toggle="tab"></a>
            <a href="#step-3-create-series" data-toggle="tab"></a>
            <a href="#step-4-select-set" data-toggle="tab"></a>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php'; ?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var allowUnApprovedUserCanCreatePublicSeries = <?php echo json_encode($allowUnApprovedUserCanCreatePublicSeries);?>;
</script>

<script src="<?php echo BASE_URL; ?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION; ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/yevgeny/home-common.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/yevgeny/create_series-global.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/plugins/ace/build/src/ace.js"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/PostsStructureExplainer/PostsStructureExplainer.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/FormLoopEditor/FormLoopEditor.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/FormsList/FormsList.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/AnsweredFormField/AnsweredFormField.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/PostsStructure/PostsStructure.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/components/PostCreator/PostCreator.js?version=<?php echo time(); ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/yevgeny/create_series-page.js?version=<?php echo time(); ?>"></script>

</body>
</html>
