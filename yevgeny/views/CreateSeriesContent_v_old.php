<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.04.2018
 * Time: 06:30
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - thegreyshirt</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/createSeriesContent-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/createSeriesContent-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-switchSubscription">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <ul class="nav nav-tabs" hidden>
                <li class="<?php echo !$nodeType ? 'active' : '';?>"><a data-toggle="tab" href="#options-tab"></a></li>
                <li class="<?php echo $nodeType == 'post' ? 'active' : '';?>"><a data-toggle="tab" href="#create-new-content"></a></li>
                <li class="<?php echo $nodeType == 'menu' ? 'active' : '';?>"><a data-toggle="tab" href="#create-new-menu"></a></li>
            </ul>
            <div class="tab-content">
                <div id="options-tab" class="tab-pane fade <?php echo !$nodeType&&!$tab ? 'in active' : '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Create new content
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Create new content for your series</h2>
                        <ul>
                            <li>
                                <a data-toggle="tab" href="#create-new-content" data-type="7">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-existing.png">
                                    </div>
                                    <div class="icon-label">
                                        create post from scratch
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#add-from-search">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-existing.png">
                                    </div>
                                    <div class="icon-label">
                                        add
                                        post(s) from search
                                    </div>
                                </a>
                            </li>
                            <?php if ($isSeriesMine): ?>
                                <li>
                                    <a data-toggle="tab" href="#create-new-menu">
                                        <div class="icon-wrapper">
                                            <img src="assets/images/global-icons/switch-from-scratch.png">
                                        </div>
                                        <div class="icon-label">
                                            create menu/decision point
                                        </div>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a data-toggle="tab" href="#create-new-content" data-type="9">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-existing.png">
                                    </div>
                                    <div class="icon-label">
                                        create switch to point
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#create-from-bulkupload">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-existing.png">
                                    </div>
                                    <div class="icon-label">
                                        add posts from bulk upload
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#create-new-content" data-type="10">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-existing.png">
                                    </div>
                                    <div class="icon-label">
                                        create form
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </main>
                </div>
                <div id="add-from-search" class="tab-pane fade">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Results
                        </div>
                    </div>
                    <?php require_once ASSETS_PATH . '/components/SeriesSearchPosts/SeriesSearchPosts.html';?>
                </div>
                <div id="create-from-bulkupload" class="tab-pane fade <?php echo $tab == 'bulkupload' ? 'in active' : '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Create a unique Post
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Please Select files that you'd like to upload</h2>
                        <div class="upload-section">
                            <div class="actions-container">
                                <div class="flex-row vertical-center margin-between space-between">
                                    <div class="flex-col fix-col">
                                        <div href="javascript:;" class="add-new-files action">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                    <g>
                                        <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"></path>
                                        <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"></path>
                                    </g>
                                </svg>
                                            Add new files
                                            <input type="file" name="files[]" multiple/>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <button class="btn btn-lg btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Upload All</span>
                                        </button>
                                        <button class="btn btn-lg btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel All</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <ul class="files-list">
                                <li class="file-row sample" hidden>
                                    <div class="flex-row vertical-center space-between margin-between">
                                        <div class="flex-col fix-col">
                                            <div class="file-image-wrapper">
                                                <img class="file-image" src="assets/images/global-icons/cloud-uploading.png">
                                                <input type="file" class="image-input"/>
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <input class="file-title" placeholder="My Favorite.mp4"/>
                                        </div>
                                        <div class="flex-col fix-col" hidden>
                                            <div class="file-thumb-wrapper">
                                                <div class="file-thumb">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="file-type">Video</div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="actions">
                                                <button class="btn btn-lg btn-primary preview">
                                                    <i class="glyphicon glyphicon-music"></i>
                                                    <span>Preview</span>
                                                </button>
                                                <button class="btn btn-lg btn-primary edit">
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                    <span>Edit</span>
                                                </button>
                                                <button class="btn btn-lg btn-primary start">
                                                    <i class="glyphicon glyphicon-upload"></i>
                                                    <span>Upload</span>
                                                </button>
                                                <button class="btn btn-lg btn-warning cancel">
                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                    <span>Cancel</span>
                                                </button>
                                                <button class="btn btn-lg btn-danger delete">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                    <span>Delete</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <footer class="content-footer">
                                <button class="btn btn-circle return-btn" type="button">Return to Edit Series</button>
                            </footer>
                        </div>
                    </main>
                </div>
                <div id="create-new-content" class="tab-pane fade <?php echo $nodeType == 'post' ? 'in active' : '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Create a unique Post
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Your Personal Post</h2>
                        <?php if ($isSeriesMine): ?>
                            <div class="series-tree-wrapper insert-position">
                                <h3>Please select node after which you want to insert</h3>
                                <div class="series-tree"></div>
                            </div>
                        <?php endif; ?>
                        <form role="form" method="post" id="post-form" enctype="multipart/form-data">
                            <ul>
                                <li>
                                    <div class="form-group">
                                        <label>Name your Post:</label>
                                        <input class="form-control" name="title" required/>
                                    </div>
                                </li>
                                <li>
                                    <div class="flex-row margin-between">
                                        <div class="flex-col">
                                            <div class="form-group">
                                                <label>Describe your Post:</label>
                                                <textarea name="description" class="form-control" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div hidden class="flex-col fix-col form-field-col">
                                            <div class="form-fields">
                                                <div class="create-form">
                                                    Create Form
                                                </div>
                                                <h3>Form fields in Series</h3>
                                                <ul class="fields-list">
                                                    <li class="sample" hidden>
                                                        <div class="flex-row vertical-center margin-between">
                                                            <div class="flex-col fix-col">
                                                                <a class="append-item action"><i class="glyphicon glyphicon-circle-arrow-left"></i></a>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <a class="append-answered-item action"><i class="glyphicon glyphicon-arrow-left"></i></a>
                                                            </div>
                                                            <div class="flex-col">
                                                                <div class="field-name">Field name</div>
                                                            </div>
                                                            <div class="flex-col fix-col del-col">
                                                                <a class="delete-post action">×</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="answered-form-field-wrapper sample" hidden>
                                                    <?php require_once ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Select Post Type:</label>
                                        <select class="form-control" name="type">
                                            <option value="2">Video</option>
                                            <option value="0">Audio</option>
                                            <option value="8">Image</option>
                                            <option value="7" <?php echo !$type || $type == 7 ? 'selected' : '';?>>Text</option>
                                            <option value="10" <?php echo (int)$type == 10 ? 'selected' : '';?>>Form Field</option>
                                            <option value="9" <?php echo (int)$type == 9 ? 'selected' : '';?>>Redirect To</option>
                                        </select>
                                    </div>
                                </li>
                                <li class="redirect-item" hidden>
                                    <div class="form-group">
                                        <label>Select Position where to redirect</label>
                                        <div class="series-tree-wrapper">
                                            <div class="series-tree"></div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Money Saved: (optional)</label>
                                        <input class="form-control" name="saved" type="number"/>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Keywords: (optional)</label>
                                        <input class="form-control" name="keywords"/>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Upload Cover Image for this Post: (optional)</label>
                                        <div class="cover-image-wrapper">
                                            <div class="img-wrapper">
                                                <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                            </div>
                                            <div>{Drag Image here to upload}</div>
                                            <input class="form-control" type="file" name="image"/>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <footer>
                                <button class="btn btn-circle" type="submit" name="allSet">All set!</button>
                                <button class="btn btn-circle" type="submit" name="createMore">Create more ideas!</button>
                            </footer>
                        </form>
                    </main>
                </div>
                <div id="create-new-menu" class="tab-pane fade <?php echo $nodeType == 'menu' ? 'in active' : '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Create a unique Menu
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Your Personal Menu</h2>
                        <div class="series-tree-wrapper">
                            <h3>Please select node after which you want to insert menu</h3>
                            <div id="series-tree" class="series-tree"></div>
                        </div>
                        <div class="menu-section">
                            <form role="form" method="post" id="menu-form">
                                <ul>
                                    <li>
                                        <div class="form-group">
                                            <label>Name your Menu:</label>
                                            <input class="form-control" name="strPost_title" required/>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <label>Describe your Menu:</label>
                                            <textarea name="strPost_body" class="form-control" rows="2" required></textarea>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <label>How many choices doese your menu have:</label>
                                            <select class="form-control" name="path_count">
                                                <option value="2" selected>2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </li>
                                </ul>
                            </form>
                            <div class="paths-wrapper">
                                <div class="path-item sample" hidden>
                                    <h2 class="path-title text-center">Option <span class="path-no"></span></h2>
                                    <form role="form" method="post" class="path-form" enctype="multipart/form-data">
                                        <ul>
                                            <li>
                                                <div class="form-group">
                                                    <label>Name of this option:</label>
                                                    <input class="form-control" name="strPost_title" required/>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <label>Description of this option:</label>
                                                    <textarea name="strPost_body" class="form-control" rows="2" required></textarea>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <label>Image for this option: (optional)</label>
                                                    <div class="cover-image-wrapper">
                                                        <div class="img-wrapper">
                                                            <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                                        </div>
                                                        <div>{Drag Image here to upload}</div>
                                                        <input class="form-control" type="file" name="image"/>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                                <div class="path-list">
                                </div>
                            </div>
                            <footer>
                                <button class="btn btn-circle" type="submit" name="allSet">All set! Create menu>></button>
                            </footer>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="modal-create-field sample" hidden>
    <header class="site-modal-header">
        <h3 class="modal-title">Create Form</h3>
    </header>
    <section class="site-modal-body">
        <h3 class="field-number">Field <span class="number">1</span>:</h3>
        <form role="form" method="post">
            <ul>
                <li>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <label>Name:</label>
                        </div>
                        <div class="flex-col">
                            <input name="strFormField_name" class="form-control" required/>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <label>Type:</label>
                        </div>
                        <div class="flex-col">
                            <select name="strFormField_type" class="form-control" required>
                                <option value="text">Text</option>
                                <option value="checkbox">Checkbox</option>
                            </select>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <label>Default Value:</label>
                        </div>
                        <div class="flex-col">
                            <div class="text-type">
                                <input name="strFormField_default" class="form-control"/>
                            </div>
                            <div class="checkbox-type" hidden>
                                <input id="form-field-default" class="form-control" name="strFormField_default" type="checkbox" value="1"/>
                                <label for="default-value">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <label>Helper Text:</label>
                        </div>
                        <div class="flex-col">
                            <input name="strFormField_helper" class="form-control" required/>
                        </div>
                    </div>
                </li>
                <li>
                    <button type="submit" class="add-another">Add Another Field</button>
                </li>
            </ul>
        </form>
    </section>
    <footer class="site-modal-footer mt-md-5">
        <div class="flex-row vertical-center margin-between">
            <div class="flex-col fix-col ml-md-auto">
                <button class="btn create-btn">Create</button>
            </div>
            <div class="flex-col fix-col">
                <button class="btn cancel-close">Cancel</button>
            </div>
        </div>
    </footer>
</div>
<div class="view-widget maximize-view">
    <div class="widget-inner">
        <header class="widget-header view-only-min">
            <div class="flex-row space-between margin-between vertical-center">
                <div class="flex-col">
                    <div class="post-title"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_max"></div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_close"></div>
                    </div>
                </div>
            </div>
        </header>
        <aside class="widget-top-control view-only-max">
            <div class="mv_top_button mv_top_close">
                <div class="mv_close_icon"></div>
            </div>
            <div class="mv_top_button mv_top_minimize">
                <div class="mv_minimize_icon"></div>
            </div>
        </aside>
        <div class="widget-body">
            <div class="video-widget sample" hidden>
                <video class="video-js" controls width="960" height="540"></video>
            </div>
            <div class="audio-widget sample" hidden>
                <div class="audiogallery">
                    <div class="items">
                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                    </div>
                </div>
            </div>
            <div class="image-widget sample" hidden></div>
            <div class="text-widget sample" hidden></div>
            <div class="form-widget sample" hidden></div>
        </div>
        <footer class="widget-footer view-only-max">
            <div class="post-title"></div>
        </footer>
    </div>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/createSeriesContent-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var isSeriesMine = <?php echo json_encode($isSeriesMine);?>;
    var initialSeries = <?php echo json_encode($series);?>;
    var initialPurchased = <?php echo json_encode($purchased);?>;
    var initialFormFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var type = <?php echo json_encode($type);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/createSeriesContent-page.js?version=<?php echo time();?>"></script>

</body>
</html>
