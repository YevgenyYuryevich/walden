<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.02.2018
 * Time: 15:35
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>View your next five experiences - walden</title>
    <meta name="description" content="View and edit your next five experiences." />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/calendar-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/calendar-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-calendar">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Next 5 Posts
                </div>
            </div>
            <header class="content-header">
                <div class="multi-select-series-wrapper">
                    <span>View Multiple</span>
                    <div class="dropdown">
                        <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select Series To View <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                        </ul>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <div class="item-sample-wrapper" hidden>
                    <div>
                        <div class="item-inner">
                            <aside>
                                <div class="bottom-line"></div>
                            </aside>
                            <div class="img-wrapper">
                                <img class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <div class="item-title"></div>
                            </div>
                            <footer class="btns-container">
                                <div class="btn-wrapper view-btn">
                                    <img src="assets/images/global-icons/read.png"/>
                                </div>
                                <div class="btn-wrapper switch-btn">
                                    <span href="javascript:;" class="open-switch-lightbox"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></span>
                                </div>
                                <div class="btn-wrapper trash-btn">
                                    <span class="make-trash"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></span>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                <section class="posts-block sample" hidden>
                    <header>
                        <div class="row">
                            <div class="col-md-8 col-sm-12"><h3 class="sery-title"></h3></div>
                            <div class="col-md-4 col-sm-12">
                                <?php if ($usersCanEditSeries): ?>
                                <div class="actions-wrapper">
                                    <a href="javascript:;" class="customize-series action">
                                        Customize
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M397.736,78.378c6.824,0,12.358-5.533,12.358-12.358V27.027C410.094,12.125,397.977,0,383.08,0H121.641
                                                        c-3.277,0-6.42,1.303-8.739,3.62L10.527,105.995c-2.317,2.317-3.62,5.461-3.62,8.738v370.239C6.908,499.875,19.032,512,33.935,512
                                                        h349.144c14.897,0,27.014-12.125,27.014-27.027V296.289c0.001-6.824-5.532-12.358-12.357-12.358
                                                        c-6.824,0-12.358,5.533-12.358,12.358v188.684c0,1.274-1.031,2.311-2.297,2.311H33.936c-1.274,0-2.311-1.037-2.311-2.311v-357.88
                                                        h75.36c14.898,0,27.016-12.12,27.016-27.017V24.716H383.08c1.267,0,2.297,1.037,2.297,2.311V66.02
                                                        C385.377,72.845,390.911,78.378,397.736,78.378z M109.285,100.075c0,1.269-1.032,2.301-2.3,2.301H49.107l60.178-60.18V100.075z"/>
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <path d="M492.865,100.396l-14.541-14.539c-16.304-16.304-42.832-16.302-59.138,0L303.763,201.28H103.559
                                                        c-6.825,0-12.358,5.533-12.358,12.358c0,6.825,5.533,12.358,12.358,12.358h175.488l-74.379,74.379H103.559
                                                        c-6.825,0-12.358,5.533-12.358,12.358s5.533,12.358,12.358,12.358h76.392l-0.199,0.199c-1.508,1.508-2.598,3.379-3.169,5.433
                                                        l-19.088,68.747h-53.936c-6.825,0-12.358,5.533-12.358,12.358s5.533,12.358,12.358,12.358h63.332c0.001,0,2.709-0.306,3.107-0.41
                                                        c0.065-0.017,77.997-21.642,77.997-21.642c2.054-0.57,3.926-1.662,5.433-3.169l239.438-239.435
                                                        C509.168,143.228,509.168,116.7,492.865,100.396z M184.644,394.073l10.087-36.326l26.24,26.24L184.644,394.073z M244.69,372.752
                                                        l-38.721-38.721l197.648-197.648l38.722,38.721L244.69,372.752z M475.387,142.054l-15.571,15.571l-38.722-38.722l15.571-15.571
                                                        c6.669-6.668,17.517-6.667,24.181,0l14.541,14.541C482.054,124.54,482.054,135.388,475.387,142.054z"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                    <a href="javascript:;" class="add-new-posts action">
                                        Add new ideas
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                        <g>
                                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                        S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"></path>
                                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                        s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"></path></g>
                                    </svg>
                                    </a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </header>
                    <div class="tt-grid-wrapper after-clearfix">
                        <span class="pagging prev-pagging">
                            <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
                        </span>
                        <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                        </ul>
                        <span class="pagging next-pagging">
                            <i class="glyphicon glyphicon-play"></i>
                        </span>
                    </div>
                </section>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="switch-lightbox-wrapper sample" hidden>
    <div class="switch-header">
        Select New Topic
    </div>
    <section class="new-posts posts-block">
        <div class="item-sample-wrapper" hidden>
            <a>
                <div class="item-inner">
                    <aside>
                        <div class="bottom-line"></div>
                    </aside>
                    <div class="img-wrapper">
                        <img class="item-img" />
                    </div>
                    <div class="title-wrapper">
                        <div class="item-title"></div>
                    </div>
                    <footer class="btns-container">
                        <button href="#" class="select-post btn btn-circle">Select</button>
                    </footer>
                </div>
            </a>
        </div>
        <h2>New Ideas</h2>
        <div class="tt-grid-wrapper after-clearfix">
            <span class="pagging prev-pagging">
                <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
            </span>
            <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
            </ul>
            <span class="pagging next-pagging">
                <i class="glyphicon glyphicon-play"></i>
            </span>
        </div>
    </section>
    <section class="favorite-posts posts-block">
        <div class="item-sample-wrapper" hidden>
            <a>
                <div class="item-inner">
                    <aside>
                        <div class="bottom-line"></div>
                    </aside>
                    <div class="img-wrapper">
                        <img class="item-img" />
                    </div>
                    <div class="title-wrapper">
                        <div class="item-title"></div>
                    </div>
                    <footer class="btns-container">
                        <button href="#" class="select-post btn btn-circle">Select</button>
                    </footer>
                </div>
            </a>
        </div>
        <h2>Favorites</h2>
        <div class="tt-grid-wrapper after-clearfix">
            <span class="pagging prev-pagging">
                <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
            </span>
            <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
            </ul>
            <span class="pagging next-pagging">
                <i class="glyphicon glyphicon-play"></i>
            </span>
        </div>
    </section>
    <section class="ideabox-posts posts-block">
        <div class="item-sample-wrapper" hidden>
            <a>
                <div class="item-inner">
                    <aside>
                        <div class="bottom-line"></div>
                    </aside>
                    <div class="img-wrapper">
                        <img class="item-img" />
                    </div>
                    <div class="title-wrapper">
                        <div class="item-title"></div>
                    </div>
                    <footer class="btns-container">
                        <div class="btn-wrapper explore-btn">
                            <div><img src="assets/images/global-icons/ideabox.png"></div>
                            explore
                        </div>
                        <div class="btn-wrapper select-btn">
                            <button href="#" class="select-post btn btn-circle">Select</button>
                        </div>
                    </footer>
                </div>
            </a>
        </div>
        <h2>Ideabox <img class="back-to-top-level" src="<?php echo BASE_URL;?>/assets/images/global-icons/back.png"></h2>
        <div class="tt-grid-wrapper after-clearfix">
            <span class="pagging prev-pagging">
                <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
            </span>
            <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
                <li class="tt-empty"></li>
            </ul>
            <span class="pagging next-pagging">
                <i class="glyphicon glyphicon-play"></i>
            </span>
        </div>
    </section>
</div>

<div class="site-lightbox sample">
    <div class="lightbox-content-wrapper">
        <aside class="close-lightbox">X</aside>
        <div class="lightbox-content"></div>
    </div>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/calendar-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initial_id = <?php echo json_encode($id);?>;
    var initial_purchasedSeries = <?php echo json_encode($purchasedSeries);?>;
    var initial_ideaboxcards = <?php echo json_encode($ideaboxcards);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/calendar-page.js?version=<?php echo time();?>"></script>

</body>
</html>