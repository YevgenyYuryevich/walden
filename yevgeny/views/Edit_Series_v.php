<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 23.11.2018
 * Time: 11:41
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Edit your experience</title>
    <meta name="description" content="Make changes to your experience, invite friends to join you, and add content to make your experience even better.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/edit_series-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostsStructureExplainer/PostsStructureExplainer.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostsStructure/PostsStructure.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostCreator/PostCreator.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormField/FormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SkypeLoader/SkypeLoader.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesConversation/SeriesConversation.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/edit_series-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="content-header">
            <div class="flex-row space-between">
                <div class="flex-col fix-col">
                    <div class="header-left">
                        <h1 class="page-name d-none d-normal-block">Edit experience</h1>
                        <div class="d-none d-structure-block">
                            <div class="flex-row vertical-center margin-between">
                                <div class="flex-col fix-col">
                                    <h1 class="pane-name">Edit series structure</h1>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="posts-count common-radius-aside"><span class="count-value">25</span> posts</div>
                                </div>
                            </div>
                        </div>
                        <h1 class="page-name d-none d-embed-block">Embed series on your website</h1>
                        <div class="is-editing d-embed-none">Add content, invite your friends, and make a more meaningful experience</div>
                        <div class="d-none d-embed-block">
                            <div class="flex-row vertical-center margin-between">
                                <div class="flex-col fix-col">
                                    <div class="some-txt">Just select embed type:</div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="select-embed-type-wrapper">
                                        <div class="dropdown site-dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                                <span>Guided</span>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                        </g>
                                    </g>
                                </svg>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
                                                <li hidden><a href="#" data-value="guided">Guided</a></li>
                                                <li><a href="#" data-value="list">List</a></li>
                                                <li><a href="#" data-value="grid">Grid</a></li>
                                                <li><a href="#" data-value="gallery">Preview Gallery</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="my_series" class="back-to-my_series bottom-underline"><<< Back to all of the experiences that I can edit</a>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="header-right">
                        <div class="flex-row margin-between">
                            <div class="flex-col fix-col d-none d-normal-block d-embed-block">
                                <a href="#series-structure-tab" data-toggle="tab" class="action-item">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                </g>
                                        <g>
                                            <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                                            <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                                            <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                                        </g>
                            </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col d-none d-structure-block d-embed-block">
                                <a href="#series-editing-tab" data-toggle="tab" class="action-item">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                    l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col d-none d-normal-block d-structure-block">
                                <a href="#series-embed-tab" data-toggle="tab" class="action-item">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="522.468px" height="522.469px" viewBox="0 0 522.468 522.469" style="enable-background:new 0 0 522.468 522.469;"
                                         xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M325.762,70.513l-17.706-4.854c-2.279-0.76-4.524-0.521-6.707,0.715c-2.19,1.237-3.669,3.094-4.429,5.568L190.426,440.53
                                                                c-0.76,2.475-0.522,4.809,0.715,6.995c1.237,2.19,3.09,3.665,5.568,4.425l17.701,4.856c2.284,0.766,4.521,0.526,6.71-0.712
                                                                c2.19-1.243,3.666-3.094,4.425-5.564L332.042,81.936c0.759-2.474,0.523-4.808-0.716-6.999
                                                                C330.088,72.747,328.237,71.272,325.762,70.513z"/>
                                                            <path d="M166.167,142.465c0-2.474-0.953-4.665-2.856-6.567l-14.277-14.276c-1.903-1.903-4.093-2.857-6.567-2.857
                                                                s-4.665,0.955-6.567,2.857L2.856,254.666C0.95,256.569,0,258.759,0,261.233c0,2.474,0.953,4.664,2.856,6.566l133.043,133.044
                                                                c1.902,1.906,4.089,2.854,6.567,2.854s4.665-0.951,6.567-2.854l14.277-14.268c1.903-1.902,2.856-4.093,2.856-6.57
                                                                c0-2.471-0.953-4.661-2.856-6.563L51.107,261.233l112.204-112.201C165.217,147.13,166.167,144.939,166.167,142.465z"/>
                                                            <path d="M519.614,254.663L386.567,121.619c-1.902-1.902-4.093-2.857-6.563-2.857c-2.478,0-4.661,0.955-6.57,2.857l-14.271,14.275
                                                                c-1.902,1.903-2.851,4.09-2.851,6.567s0.948,4.665,2.851,6.567l112.206,112.204L359.163,373.442
                                                                c-1.902,1.902-2.851,4.093-2.851,6.563c0,2.478,0.948,4.668,2.851,6.57l14.271,14.268c1.909,1.906,4.093,2.854,6.57,2.854
                                                                c2.471,0,4.661-0.951,6.563-2.854L519.614,267.8c1.903-1.902,2.854-4.096,2.854-6.57
                                                                C522.468,258.755,521.517,256.565,519.614,254.663z"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="add-new-item-wrapper">
                                    <a href="add_posts?id=<?php echo $series['series_ID'];?>" class="add-new-item-btn" target="_blank">Add New Item</a>
                                </div>
                                <a href="javascript:;" class="delete-series">delete experience</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main class="main-content">
            <div class="tab-content">
                <div class="tab-pane fade <?php echo $tab === 'series' ? 'in active' : '';?>" id="series-editing-tab">
                    <form method="post" action="" class="edit-series-form">
                        <div class="flex-row">
                            <div class="flex-col">
                                <div class="form-left">
                                    <div class="form-group">
                                        <label class="control-label">Name:</label>
                                        <input value="<?php echo $series['strSeries_title'];?>" placeholder="type your name here" type="text" name="strSeries_title" class="form-control" required/>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Describe:</label>
                                        <div class="multi-line-background">
                                            <textarea class="about-input" rows="4" required placeholder="type description for your experience" name="strSeries_description"><?php echo $series['strSeries_description'];?></textarea>
                                            <div class="background-placeholder"></div>
                                        </div>
                                    </div>
                                    <div class="flex-row space-between flex-wrap">
                                        <div class="flex-col fix-col">
                                            <div class="form-group">
                                                <label class="control-label margin-bottom">Category:</label>
                                                <select name="intSeries_category" class="form-control">
                                                    <?php foreach ($categories as $category): ?>
                                                        <option <?php echo $category['category_ID'] == $series['intSeries_category'] ? 'selected' : '';?> value="<?php echo $category['category_ID'];?>"><?php echo $category['strCategory_name'];?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="form-group">
                                                <label class="control-label margin-bottom-2">Display on:</label>
                                                <div class="flex-row margin-between">
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-1" type="checkbox"/><label for="day-1">Mon</label>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-2" type="checkbox"/><label for="day-2">Tue</label>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-3" type="checkbox"/><label for="day-3">Wed</label>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-4" type="checkbox"/><label for="day-4">Thu</label>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-5" type="checkbox"/><label for="day-5">Fri</label>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-6" type="checkbox"/><label for="day-6">Sat</label>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="day-item">
                                                            <input id="day-0" type="checkbox"/><label for="day-0">Sun</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="form-right">
                                    <div class="cover-image-wrapper">
                                        <div class="img-wrapper">
                                            <img class="user-image" src="<?php echo $series['strSeries_image'];?>" />
                                        </div>
                                        <div class="cloud-img-wrapper">
                                            <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                        </div>
                                        <div>Drag Image here to upload</div>
                                        <input class="form-control" type="file" name="strSeries_image"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="co-owners-wrapper">
                            <div class="form-group">
                                <label for="" class="control-label">Co-Owners</label>
                                <label class="c-switch">
                                    <input type="checkbox" class="owner-switch" value="1">
                                    <div class="slider round"></div>
                                    <div class="flex-centering value-text-wrapper">
                                        <div class="value-txt accepted-txt">accepted</div>
                                        <div class="value-txt invited-txt">invited</div>
                                    </div>
                                </label>
                            </div>
                            <div class="owners-container flex-row flex-wrap">
                                <article class="owner-item sample flex-col fix-col" hidden>
                                    <header class="item-header">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col">
                                                <div class="role-txt">
                                                    Creator
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <a class="item-action edit-action flex-centering" href="javascript:;">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                                </a>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <a class="item-action delete-action" href="javascript:;">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                </a>
                                            </div>
                                        </div>
                                    </header>
                                    <div class="item-img-wrapper">
                                    </div>
                                    <div class="user-name">Someone's Name</div>
                                </article>
                                <div class="flex-col fix-col add-friend-card">
                                    <div class="flex-centering add-friend-inner">
                                        <div class="form-group">
                                            <label class="control-label">Invite a friend</label>
                                            <div class="input-wrapper">
                                                <aside class="enter-icon-wrapper flex-centering">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 26 26" style="enable-background:new 0 0 26 26;" xml:space="preserve">
                                                    <g>
                                                        <path d="M25,2H9C8.449,2,8,2.449,8,3c0,0,0,7,0,9s-2,2-2,2H1c-0.551,0-1,0.449-1,1v8c0,0.551,0.449,1,1,1h24
                                                            c0.551,0,1-0.449,1-1V3C26,2.449,25.551,2,25,2z M22,14c0,1.436-1.336,4-4,4h-3.586l1.793,1.793c0.391,0.391,0.391,1.023,0,1.414
                                                            C16.012,21.402,15.756,21.5,15.5,21.5s-0.512-0.098-0.707-0.293l-3.5-3.5c-0.391-0.391-0.391-1.023,0-1.414l3.5-3.5
                                                            c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L14.414,16H18c1.398,0,2-1.518,2-2v-2c0-0.553,0.447-1,1-1s1,0.447,1,1V14z"
                                                        />
                                                    </g>
                                                </svg>
                                                </aside>
                                                <input type="text" class="form-control add-input" placeholder="type email and press">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="charge-series-wrapper">
                            <div class="flex-row margin-between vertical-center">
                                <div class="flex-col">
                                    <div class="charge-content">
                                        <div class="connect-account-wrapper">
                                            <a target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect');?>" class="stripe-connect"><span>Connect with Stripe</span></a> to charge for your series
                                        </div>
                                        <div class="group-row-wrapper charge-users-wrapper">
                                            <div class="flex-row margin-between space-between">
                                                <div class="flex-col fix-col">
                                                    <div class="would-charge-wrapper group-col toggle-col">
                                                        <div class="would-charge-txt col-name">Would you like to charge a monthly payment fee for membership?</div>
                                                        <div class="flex-row vertical-center margin-between">
                                                            <div class="flex-col fix-col">
                                                                <label class="c-switch">
                                                                    <input type="checkbox" name="boolSeries_charge" value="1">
                                                                    <div class="slider round"></div>
                                                                </label>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="get-payment-txt">you will get payment</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col center-col">
                                                    <div class="value-wrap">
                                                        <div class="flex-row align-items-center margin-between">
                                                            <div class="flex-col fix-col">
                                                                <div class="end-value">$0</div>
                                                            </div>
                                                            <div class="flex-col">
                                                                <input type="range" name="intSeries_price" min="0" max="45"
                                                                       value="45"/>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="end-value">$45</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pay-affiliates-wrapper group-row-wrapper">
                                            <div class="flex-row margin-between space-between">
                                                <div class="flex-col fix-col">
                                                    <div class="would-pay-affiliate-wrapper group-col toggle-col">
                                                        <div class="would-pay-txt col-name">Would you like to use affiliates to market for you and expand your reach?</div>
                                                        <div class="flex-row vertical-center margin-between">
                                                            <div class="flex-col fix-col">
                                                                <label class="c-switch">
                                                                    <input type="checkbox" name="boolSeries_affiliated" value="1">
                                                                    <div class="slider round"></div>
                                                                </label>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="get-payment-txt">Affiliate will get fee</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col center-col overflow-visible">
                                                    <div class="value-wrap">
                                                        <div class="flex-row margin-between align-items-center">
                                                            <div class="flex-col fix-col">
                                                                <div class="end-value-wrap">
                                                                    <div class="end-value you-value">$24</div>
                                                                    <div>You</div>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col">
                                                                <input type="range" name="intSeries_affiliate_price" min="0" max="250"
                                                                       value="45"/>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="end-value-wrap">
                                                                    <div class="end-value affiliate-value">$6</div>
                                                                    <div>Affiliate</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="step-description">
                                        To charge for your series, you must setup strpe public API by clicking connect button above.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <p>All set!</p>
                            <button class="save-btn">Save changes</button>
                        </div>
                    </form>
                    <div class="invite-friend-wrapper">
                        <div class="flex-row space-between vertical-center">
                            <div class="flex-col fix-col">
                                <div class="invite-content">
                                    <h4 class="invite-txt">Invite a friend</h4>
                                    <div class="to-add-txt">to add content to the series together</div>
                                    <form class="invite-friend-form" method="post">
                                        <div class="input-wrapper">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col">
                                                    <input type="text" name="emails" placeholder="Type friend's emails with comma" required>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <button class="btn" type="submit">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <polygon style="fill:#5EBAE7;" points="490.452,21.547 16.92,235.764 179.068,330.053 179.068,330.053 "/>
                                                            <polygon style="fill:#36A9E1;" points="490.452,21.547 276.235,495.079 179.068,330.053 179.068,330.053 "/>
                                                            <rect x="257.137" y="223.122" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 277.6362 609.0793)" style="fill:#FFFFFF;" width="15.652" height="47.834"/>
                                                            <path style="fill:#1D1D1B;" d="M0,234.918l174.682,102.4L277.082,512L512,0L0,234.918z M275.389,478.161L190.21,332.858
                                                                l52.099-52.099l-11.068-11.068l-52.099,52.099L33.839,236.612L459.726,41.205L293.249,207.682l11.068,11.068L470.795,52.274
                                                                L275.389,478.161z"/>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <aside class="plane-wrapper">
                                        <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/plane.png"
                                             alt="">
                                    </aside>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="invite-image-wrapper">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/invite_illustration.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade list-structure-mode <?php echo $tab === 'structure' ? 'in active' : '';?>" id="series-structure-tab">
                    <?php require_once ASSETS_PATH . '/components/PostsStructureExplainer/PostsStructureExplainer.html';?>
                    <?php require_once ASSETS_PATH . '/components/PostsStructure/PostsStructure.php';?>
                </div>
                <div class="tab-pane fade <?php echo $tab === 'embed' ? 'in active' : '';?>" id="series-embed-tab">
                    <div class="embed-preview-header">
                        <div class="flex-row vertical-center margin-between">
                            <div class="flex-col fix-col">
                                <div class="style-txt">Preview embed style</div>
                            </div>
                            <div class="flex-col fix-col get-code-col">
                                <button class="get-code-btn">Get code</button>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col">
                                        <a href="javascript:;" class="embed-style-item" data-type="grid">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M9.172,9.179V0.146H0v9.033H9.172z"/>
                                                                <path d="M19.132,9.179V0.146H9.959v9.033H19.132z"/>
                                                                <path d="M19.132,18.986V9.955H9.959v9.032H19.132z"/>
                                                                <path d="M9.172,18.986V9.955H0v9.032H9.172z"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" class="embed-style-item" data-type="list">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 294.842 294.842" style="enable-background:new 0 0 294.842 294.842;" xml:space="preserve">
                                                        <g>
                                                            <path d="M292.128,214.846c-2.342-2.344-6.143-2.344-8.484,0l-59.512,59.511V6c0-3.313-2.687-6-6-6s-6,2.687-6,6v268.356
                                                                l-59.513-59.512c-2.342-2.342-6.142-2.343-8.485,0.001c-2.343,2.343-2.343,6.142,0.001,8.485l69.755,69.754
                                                                c1.171,1.171,2.707,1.757,4.242,1.757s3.071-0.586,4.242-1.758l69.754-69.754C294.472,220.987,294.472,217.188,292.128,214.846z"></path>
                                                            <path d="M6.956,12h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,12,6.956,12z"></path>
                                                            <path d="M6.956,82.228h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,82.228,6.956,82.228z"></path>
                                                            <path d="M6.956,152.456h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,152.456,6.956,152.456z"></path>
                                                            <path d="M124.438,210.685H6.956c-3.313,0-6,2.687-6,6s2.687,6,6,6h117.482c3.313,0,6-2.687,6-6S127.752,210.685,124.438,210.685z"></path>
                                                            <path d="M124.438,280.912H6.956c-3.313,0-6,2.687-6,6s2.687,6,6,6h117.482c3.313,0,6-2.687,6-6S127.752,280.912,124.438,280.912z"></path>
                                                        </g>
                                                    </svg>
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" class="embed-style-item active-item" data-type="guided">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 452.986 452.986" style="enable-background:new 0 0 452.986 452.986;" xml:space="preserve">
<g>
    <g>
        <path d="M0,1.898v449.19h452.986V1.898H0z M423.025,31.86c0,26.791,0,362.475,0,389.288
			c-26.834,0-366.25,0-393.084,0c0-26.812,0-362.497,0-389.288C56.774,31.86,396.191,31.86,423.025,31.86z"/>
        <path d="M226.493,120.3v29.66h155.223V120.3H226.493z M182.338,253.565h199.4v-29.638h-199.4V253.565z
			 M67.171,357.191h314.567v-29.638H67.171V357.191z"/>
        <path d="M195.841,128.195c0-37.965-30.803-68.768-68.703-68.768c-38.008,0-68.725,30.76-68.746,68.768
			c0,34.103,24.914,62.189,57.421,67.603l0.324,16.006l-9.858,3.365l0.022,78.518c0,0,9.448,9.642,21.808,8.822
			c12.36-0.777,19.78-8.822,19.78-8.822l0.022-78.561l-9.146-2.2l-0.302-17.127C170.992,190.341,195.841,162.277,195.841,128.195z
			 M127.095,181.195c-29.207,0-52.956-23.728-52.956-52.978s23.728-52.913,52.999-52.956c29.207,0,52.935,23.706,52.956,52.956
			C180.073,157.467,156.388,181.151,127.095,181.195z"/>
    </g>
</g>
</svg>
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" class="embed-style-item" data-type="gallery">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 15.564 15.564" style="enable-background:new 0 0 15.564 15.564;" xml:space="preserve">
<g>
    <path d="M15.383,4.677c0.103-0.007,0.181-0.091,0.181-0.192v-1.12c0-0.106-0.086-0.193-0.193-0.193h-1.309
		c-0.107,0-0.193,0.087-0.193,0.193c0,0.418-0.361,0.765-0.824,0.813V3.988c0-0.102-0.079-0.187-0.181-0.193
		c-0.486-0.031-0.866-0.388-0.866-0.811c0-0.423,0.38-0.78,0.866-0.812c0.102-0.007,0.181-0.091,0.181-0.193v-1.12
		c0-0.107-0.087-0.193-0.193-0.193h-1.31c-0.106,0-0.193,0.086-0.193,0.193c0,0.451-0.42,0.818-0.936,0.818S9.477,1.31,9.477,0.859
		c0-0.107-0.087-0.193-0.193-0.193H7.576c-0.095,0-0.175,0.068-0.191,0.162C7.319,1.228,6.923,1.53,6.463,1.53
		c-0.459,0-0.856-0.302-0.922-0.702C5.525,0.735,5.445,0.666,5.35,0.666H3.61c-0.095,0-0.175,0.068-0.19,0.162
		C3.354,1.228,2.958,1.53,2.498,1.53c-0.459,0-0.856-0.302-0.922-0.702C1.56,0.735,1.48,0.666,1.386,0.666H0.194
		c-0.106,0-0.193,0.086-0.193,0.193v1.12c0,0.053,0.021,0.104,0.061,0.141s0.094,0.056,0.144,0.052c0.004,0,0.067-0.006,0.071-0.007
		c0.516,0,0.936,0.367,0.936,0.819c0,0.449-0.417,0.815-0.955,0.815c-0.001,0-0.003,0-0.005,0c-0.004,0-0.042-0.004-0.046-0.005
		c-0.05-0.002-0.105,0.017-0.144,0.053S0.001,3.934,0.001,3.988v1.53c0,0.102,0.079,0.187,0.18,0.193
		c0.486,0.031,0.866,0.388,0.866,0.812c0,0.423-0.38,0.778-0.866,0.811c-0.102,0.007-0.18,0.092-0.18,0.193v1.543
		c0,0.102,0.079,0.186,0.18,0.192c0.486,0.032,0.866,0.389,0.866,0.812s-0.38,0.779-0.866,0.812C0.079,10.892,0,10.976,0,11.077
		v1.12c0,0.107,0.086,0.193,0.193,0.193h1.646c0.002,0,0.005,0,0.007,0c0.106,0,0.193-0.086,0.193-0.193
		c0-0.022-0.004-0.045-0.012-0.065c0.017-0.438,0.43-0.791,0.936-0.791c0.4,0,0.721,0.354,0.738,0.559
		c0.089,0.43,0.02,0.73,0.02,0.679c0,0.423-0.535,0.78-1.021,0.812c-0.102,0.007-0.181,0.091-0.181,0.192v1.12
		c0,0.106,0.086,0.193,0.193,0.193h1.646c0.002,0,0.006,0,0.008,0c0.107,0,0.193-0.087,0.193-0.193c0-0.022-0.004-0.045-0.011-0.065
		c0.017-0.438,0.43-0.791,0.936-0.791c0.516,0,0.936,0.367,0.937,0.806c-0.001,0.009-0.005,0.036-0.005,0.046
		c-0.001,0.052,0.018,0.103,0.055,0.14c0.036,0.038,0.086,0.059,0.139,0.059h1.708c0.099,0,0.181-0.073,0.192-0.17
		c0.051-0.417,0.448-0.732,0.925-0.732s0.874,0.315,0.926,0.732c0.012,0.097,0.094,0.17,0.191,0.17h1.73
		c0.098,0,0.18-0.073,0.191-0.17c0.051-0.417,0.449-0.732,0.926-0.732s0.875,0.315,0.926,0.732c0.012,0.098,0.094,0.17,0.191,0.17
		h0.854c0.107,0,0.193-0.087,0.193-0.193v-1.12c0-0.053-0.021-0.104-0.061-0.141c-0.038-0.036-0.091-0.053-0.145-0.052
		c-0.004,0-0.066,0.007-0.07,0.007c-0.516,0-0.936-0.367-0.936-0.818c0-0.449,0.417-0.815,0.955-0.815c0.002,0,0.004,0,0.006,0
		c0.004,0,0.041,0.004,0.045,0.004c0.054,0.005,0.106-0.016,0.145-0.052c0.039-0.037,0.061-0.087,0.061-0.141v-1.53
		c0-0.102-0.078-0.187-0.181-0.193c-0.485-0.031-0.866-0.388-0.866-0.812c0-0.423,0.381-0.779,0.866-0.811
		c0.103-0.007,0.181-0.091,0.181-0.192V6.495c0-0.102-0.078-0.186-0.181-0.192c-0.485-0.032-0.866-0.389-0.866-0.812
		S14.897,4.71,15.383,4.677z M4.303,12.39h1.496c0.098,0,0.18-0.073,0.192-0.17c0.05-0.418,0.448-0.732,0.924-0.732
		c0.477,0,0.875,0.314,0.925,0.732c0.012,0.097,0.094,0.17,0.192,0.17h1.731c0.098,0,0.18-0.073,0.191-0.17
		c0.051-0.418,0.448-0.732,0.925-0.732s0.875,0.314,0.926,0.732c0.012,0.097,0.095,0.17,0.192,0.17h0.854
		c0.106,0,0.193-0.086,0.193-0.193v-1.12c0-0.054-0.022-0.104-0.062-0.141c-0.039-0.037-0.093-0.054-0.145-0.052
		c-0.004,0-0.066,0.006-0.069,0.007c-0.517,0-0.936-0.367-0.936-0.818c0-0.45,0.416-0.815,0.956-0.815
		c0.005-0.001,0.043,0.003,0.049,0.004c0.052,0.003,0.105-0.015,0.145-0.052c0.039-0.036,0.062-0.087,0.062-0.141V7.538
		c0-0.102-0.079-0.186-0.181-0.192c-0.486-0.032-0.866-0.389-0.866-0.812s0.38-0.779,0.866-0.811
		c0.102-0.007,0.181-0.092,0.181-0.193V4.787h0.784v8.492H4.303V12.39z M11.308,2.282v8.493H1.782V2.282H11.308z"/>
</g>
</svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="embed-preview-wrapper">
                    </section>
                    <div class="embed-code-header">
                        Code to embed Series with <span>Guided</span> Style
                    </div>
                    <section class="code-editor-wrapper">
                        <?php require_once ASSETS_PATH . '/components/EmbedEditor/EmbedEditor.html';?>
                    </section>
                </div>
            </div>
            <?php require_once ASSETS_PATH . '/components/PostEditor/PostEditor.php';?>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var tab = <?php echo json_encode($tab);?>;
    var series = <?php echo json_encode($series);?>;
    var seriesOwners = <?php echo json_encode($seriesOwners);?>;
    var invitations = <?php echo json_encode($invitations);?>;
    var isSeriesMine = <?php echo json_encode($isSeriesMine);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/edit_series-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/plugins/ace/build/src/ace.js"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostsStructureExplainer/PostsStructureExplainer.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostsStructure/PostsStructure.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostCreator/PostCreator.js?version=<?php echo time();?>"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.1/firebase-database.js"></script>
<script src="<?php echo BASE_URL;?>/assets/config/js/firebase-config.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SkypeLoader/SkypeLoader.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesConversation/SeriesConversation.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormField/FormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/edit_series-page.js?version=<?php echo time();?>"></script>

</body>
</html>
