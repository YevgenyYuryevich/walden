<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.04.2019
 * Time: 15:55
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $series['strSeries_title'];?> - Join this experience to grow and share.</title>
    <meta name="description" content="<?php echo $series['strSeries_description'];?>. Contribute to this experience to help others or join it to help yourself.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/shared-posts-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/shared-posts-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-shared-posts <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="content-header">
            <div class="series-title-wrapper">
                <div class="flex-row align-items-end margin-between">
                    <div class="flex-col fix-col">
                        <div class="series-title"><?php echo $series['strSeries_title'];?></div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="posts-count common-radius-aside"><span class="count-value"><?php echo count($posts);?></span> posts</div>
                    </div>
                </div>
            </div>
        </header>
        <main class="main-content">
            <div class="post-list flex-row flex-wrap">
                <article class="flex-col fix-col item sample" hidden>
                    <header class="item-header">
                        <div class="flex-row vertical-center margin-between">
                            <div class="flex-col fix-col">
                                <div class="media-icon-wrapper">
                                    <i class="fa fa-youtube-play video-icon"></i>
                                    <i class="fa fa-volume-up audio-icon"></i>
                                    <i class="fa fa-file-text text-icon"></i>
                                </div>
                            </div>
                            <div class="flex-col min-width-0">
                                <div class="shared-times">shared 5 times</div>
                            </div>
                            <div class="flex-col fix-col">
                                <a class="item-action delete-action" href="javascript:;" tabindex="0">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                    286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"></polygon>
                                                <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                    C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                    S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </header>
                    <div class="item-type-body">
                        <div class="blog-type type-0 sample" hidden>
                        </div>
                        <div class="blog-type type-2 sample" hidden>
                            <img alt="">
                        </div>
                        <div class="blog-type type-default sample" hidden>
                            Lorem lpsum is simply dummy text of the printing and type
                        </div>
                        <aside class="blog-duration">
                            waiting...
                        </aside>
                    </div>
                    <div class="item-title-wrapper">
                        <a href="javascript:;" target="_blank" class="item-title">Why gifted student needs to be taught formal writing</a>
                    </div>
                </article>
            </div>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var series = <?php echo json_encode($series);?>;
    var posts = <?php echo json_encode($posts);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/shared-posts-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/shared-posts-page.js?version=<?php echo time();?>"></script>

</body>
</html>