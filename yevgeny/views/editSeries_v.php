<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Edit the solution to make it perfect for you - thegreyshirt</title>
    <meta name="description" content="Edit your daily solution to include the content that you want in the order that you want it." />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/editSeries-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/editSeries-page.css?version=<?php echo time();?>">
</head>

<body>
<div class="site-wrapper page page-editseries">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <ul class="nav nav-tabs" hidden>
                <li class="<?php echo $panel == 'main' ? 'active': '';?>"><a data-toggle="tab" href="#main-tab">series</a></li>
                <li class="<?php echo $panel == 'addNew' ? 'active': '';?>"><a data-toggle="tab" href="#add-items-tab">posts</a></li>
            </ul>
            <div class="tab-content">
                <div id="main-tab" class="tab-pane fade <?php echo $panel == 'main' ? 'in active': '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Customize
                        </div>
                    </div>
                    <header class="content-header">
                        <div class="open-add-items-wrapper pull-right">Add New Item <a class="open-add-items" href="javascript:;"><svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a></div>
                        <div class="clearfix"></div>
                    </header>
                    <main class="main-content">
                        <header>
                            <h2 class="series-title"><?php echo $series['strSeries_title'];?></h2>
                            <div class="cover-image-wrapper">
                                <div class="img-wrapper">
                                    <img src="<?php  echo $series['strSeries_image'];?>" />
                                </div>
                                <div>{Drag Image here to upload}</div>
                                <input class="form-control" type="file" name="cover_img"/>
                            </div>
                            <?php if ($isSeriesMine): ?>
                                <div class="charge-helper">
                                        <span>
                                <?php if ($isStripeAvailable): ?>
                                    To change your connected stripe account <br/>
                                    Click here
                                <?php else: ?>
                                    To charge for your series, you must setup stripe public API in your settings.<br/>
                                    Click here
                                <?php endif; ?>
                                        </span>
                                    <a target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect');?>" class="stripe-connect"><span>Connect with Stripe</span></a>
                                </div>
                                <div class="<?php echo (int)$series['boolSeries_charge'] === 1 ? 'charged' : '';?> flex-row vertical-center margin-between horizon-center series-charge-wrapper">
                                    <div class="flex-col fix-col">
                                        <span class="charge-label">Charge: </span>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <label class="c-switch">
                                            <input type="checkbox" name="boolSeries_charge" <?php echo (int)$series['boolSeries_charge'] === 1 ? 'checked' : '';?> value="1">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <span class="price-label">Price:</span>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <input class="form-control charge-price" name="intSeries_price" type="number" <?php echo (int)$series['boolSeries_charge'] === 1 ? '' : 'disabled';?> value="<?php echo (int)$series['boolSeries_charge'] === 1 ? $series['intSeries_price'] : '';?>"/>
                                    </div>
                                </div>
                                <div class="affiliate-settings">
                                    <div class="flex-row vertical-center margin-between horizon-center">
                                        <div class="flex-col">
                                            <div class="field-label">Pay Affiliates for Referrals:</div>
                                        </div>
                                        <div class="flex-col">
                                            <label class="c-switch">
                                                <input type="checkbox" name="boolSeries_affiliated" <?php echo (int)$series['boolSeries_affiliated'] === 1 ? 'checked' : '';?> value="1">
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <div class="flex-col">
                                            <div class="field-label percent-label">Percent:</div>
                                        </div>
                                        <div class="flex-col">
                                            <input class="field-value percent-value" name="intSeries_affiliate_percent" type="range" max="100" value="30"/>
                                        </div>
                                        <div class="flex-col">
                                            <span class="field-helper" style="font-style: italic;" data-toggle="popover" data-trigger="hover" contenteditable="false" data-content="affiliater will get this percent of total budget of this series" data-original-title="" title="">i</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="money-expects">
                                    <div class="flex-row vertical-center horizon-center margin-between">
                                        <div class="flex-col">
                                            <div class="field-label charge-field">You get:</div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="field-value charge-field"></div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="field-label affiliate-field">The Affiliate Gets:</div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="field-value affiliate-field"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="available-days-wrapper">
                                <div class="display-on">Display on: </div>
                                <div class="available-days-container">
                                    <ul class="available-days-list">
                                        <li class="<?php echo $availableDays[0] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'mon'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            M
                                        </li>
                                        <li class="<?php echo $availableDays[1] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'tus'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            T
                                        </li>
                                        <li class="<?php echo $availableDays[2] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'wed'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            W
                                        </li>
                                        <li class="<?php echo $availableDays[3] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'thi'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            Th
                                        </li>
                                        <li class="<?php echo $availableDays[4] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'fri'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            F
                                        </li>
                                        <li class="<?php echo $availableDays[5] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'sat'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            Sa
                                        </li>
                                        <li class="<?php echo $availableDays[6] == '1' ? 'active-day' : '';?>">
                                            <div class="icon-wrapper" data-day = 'sun'>
                                                <img class="active-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-active.png" />
                                                <img class="inactive-icon" src="<?php echo BASE_URL?>/assets/images/global-icons/check-inactive.png" />
                                            </div>
                                            S
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php if ($isSeriesMine): ?>
                                <div class="series-description-wrapper">
                                    <div class="form-group">
                                        <label form="series-description" for="series-description">Series Description</label>
                                        <textarea id="series-description" class="series-description form-control" name="strSeries_description"><?php echo $series['strSeries_description'];?></textarea>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </header>
                        <aside class="add-to-links-section">
                            <h2>Add to Series:</h2>
                            <div class="flex-row vertical-center horizon-center flex-wrap">
                                <div class="flex-col">
                                    <a href="javascript:;" data-node-type="post" data-post-type="7">
                                        Add Post<br />
                                        from Scratch
                                    </a>
                                </div>
                                <div class="flex-col">
                                    <a href="javascript:;" class="add-from-search">
                                        Add Post(s)<br />
                                        from Search
                                    </a>
                                </div>
                                <?php if ($isSeriesMine): ?>
                                    <div class="flex-col">
                                        <a href="javascript:;" data-node-type="menu">
                                            Create<br />
                                            Menu/Dcision-Point
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" data-node-type="post" data-post-type="9">
                                            Create<br />
                                            "Switch To" Point
                                        </a>
                                    </div>
                                    <div class="flex-col">
                                        <a href="javascript:;" data-tab="bulkupload">
                                            Add Post(s)<br />
                                            from Bulk Upload
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <div class="flex-col">
                                    <a href="javascript:;" data-node-type="post" data-post-type="10">
                                        Create<br />
                                        Form
                                    </a>
                                </div>
                            </div>
                        </aside>
                        <h2 class="tree-helper-title">Edit Series Structure:  <span>(Drag an Item to Change the Order)</span></h2>
                        <div class="posts-container jstree jstree-default jstree-default-large <?php echo (int)$series['boolSeries_charge'] ? 'charged' : '';?>">
                            <li class="sample post-row jstree-node jstree-closed" hidden>
                                <div class="post-row-inner">
                                    <div class="flex-row vertical-center space-between margin-between">
                                        <div class="flex-col fix-col"><i class="jstree-icon jstree-ocl" role="presentation"></i></div>
                                        <div class="flex-col fix-col">
                                            <div class="drag-icon-wrapper">
                                                <img class="drag-icon type-root" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/series/start-(3).png">
                                                <img class="drag-icon type-post" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/post/blog-(4).png">
                                                <img class="drag-icon type-menu" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/menu/path-selection-(4).png">
                                                <img class="drag-icon type-path" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/option/next-(4).png">
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="post-title"></div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="actions">
                                                <div class="flex-row margin-between vertical-center">
                                                    <?php  if ($isSeriesMine): ?>
                                                        <div class="flex-col publish-col">
                                                            <a class="publish-post">
                                                                <img class="active-icon" src="assets/images/global-icons/check-active.png">
                                                                <img class="inactive-icon" src="assets/images/global-icons/check-inactive.png">
                                                            </a>
                                                        </div>
                                                        <div class="flex-col charge-col">
                                                            <div class="flex-row vertical-center margin-between horizon-center post-charge-wrapper">
                                                                <div class="flex-col fix-col">
                                                                    <span class="charge-label">Free? </span>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <label class="c-switch">
                                                                        <input type="checkbox" name="boolPost_free" value="1">
                                                                        <div class="slider round"></div>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php  endif; ?>
                                                    <div class="flex-col view-col">
                                                        <a class="view-post" href="javascript:;">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/open-book.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col edit-col">
                                                        <a class="edit-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/edit-series.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col save-col">
                                                        <a class="save-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/diskette-save-interface-symbol.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col comeback-col">
                                                        <a class="comeback-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/comeback-arrow.svg">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col">
                                                        <a class="clone-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/clone.png">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col del-col">
                                                        <a class="delete-post">
                                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/close-cross-circular-interface-button.png">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <div class="modal-edit-entry-wrapper" hidden>
                                <?php require_once ASSETS_PATH . '/components/EditPostModal/EditPostModal.html';?>
                            </div>
                            <ul class="posts-list">
                            </ul>
                        </div>
                        <footer class="main-content-footer">
                            <div class="flex-row space-between vertical-center">
                                <div class="flex-col">
                                    <h3 class="main-footer-title">Embed Series on your Website</h3>
                                </div>
                                <div class="flex-col fix-col">
                                    <a class="go-to-embed-preview" href="preview_series_embed?id=<?php echo $series['series_ID'];?>" target="_blank">Preview Embed</a>
                                </div>
                            </div>
                            <ul class="nav nav-tabs" hidden>
                                <li class="active"><a data-toggle="tab" href="#select-layout">select layout</a></li>
                                <li class=""><a data-toggle="tab" href="#information-display">info to display</a></li>
                                <li class=""><a data-toggle="tab" href="#code-editor">codepen</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="select-layout" class="tab-pane fade in active">
                                    <h3 class="text-center">Select Layout for your website</h3>
                                    <div class="flex-row">
                                        <div class="flex-col">
                                            <div class="layout-item">
                                                <div class="item-icon-wrapper" data-layout="grid">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 341.333 341.333" style="enable-background:new 0 0 341.333 341.333;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <rect x="128" y="128" width="85.333" height="85.333"/>
                                                            <rect x="0" y="0" width="85.333" height="85.333"/>
                                                            <rect x="128" y="256" width="85.333" height="85.333"/>
                                                            <rect x="0" y="128" width="85.333" height="85.333"/>
                                                            <rect x="0" y="256" width="85.333" height="85.333"/>
                                                            <rect x="256" y="0" width="85.333" height="85.333"/>
                                                            <rect x="128" y="0" width="85.333" height="85.333"/>
                                                            <rect x="256" y="128" width="85.333" height="85.333"/>
                                                            <rect x="256" y="256" width="85.333" height="85.333"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                                </div>
                                                <h3>Grid</h3>
                                                <p>Great for displaying<br>
                                                    all of your work.
                                                </p>
                                                <a href="#">View Example</a>
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="layout-item">
                                                <div class="item-icon-wrapper" data-layout="list">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 294.842 294.842" style="enable-background:new 0 0 294.842 294.842;" xml:space="preserve">
                                                        <g>
                                                            <path d="M292.128,214.846c-2.342-2.344-6.143-2.344-8.484,0l-59.512,59.511V6c0-3.313-2.687-6-6-6s-6,2.687-6,6v268.356
                                                                l-59.513-59.512c-2.342-2.342-6.142-2.343-8.485,0.001c-2.343,2.343-2.343,6.142,0.001,8.485l69.755,69.754
                                                                c1.171,1.171,2.707,1.757,4.242,1.757s3.071-0.586,4.242-1.758l69.754-69.754C294.472,220.987,294.472,217.188,292.128,214.846z"/>
                                                            <path d="M6.956,12h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,12,6.956,12z"/>
                                                            <path d="M6.956,82.228h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,82.228,6.956,82.228z"/>
                                                            <path d="M6.956,152.456h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,152.456,6.956,152.456z"/>
                                                            <path d="M124.438,210.685H6.956c-3.313,0-6,2.687-6,6s2.687,6,6,6h117.482c3.313,0,6-2.687,6-6S127.752,210.685,124.438,210.685z"
                                                            />
                                                            <path d="M124.438,280.912H6.956c-3.313,0-6,2.687-6,6s2.687,6,6,6h117.482c3.313,0,6-2.687,6-6S127.752,280.912,124.438,280.912z"
                                                            />
                                                        </g>
                                                    </svg>
                                                </div>
                                                <h3>List</h3>
                                                <p>A great widget<br>
                                                    to show off your work.
                                                </p>
                                                <a href="viewexperience_embed?id=<?php echo $series['series_ID'];?>&viewVersion=popup" target="_blank">View Example</a>
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="layout-item">
                                                <div class="item-icon-wrapper" data-layout="guided">
                                                    <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/turbotax%20style%20icon.png" />
                                                </div>
                                                <h3>Guided</h3>
                                                <p>A great way to<br>
                                                    let user's view your work.
                                                </p>
                                                <a href="viewexperience_embed?id=<?php echo $series['series_ID'];?>" target="_blank">View Example</a>
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="layout-item last-item">
                                                <div class="item-icon-wrapper" data-layout="gallery">
                                                    <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/gallery.png" />
                                                </div>
                                                <h3>Preview Gallery</h3>
                                                <p>A gallery to display<br>
                                                    preview your work.
                                                </p>
                                                <a href="#">View Example</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="information-display" class="tab-pane fade">
                                    <div class="flex-row horizon-center">
                                        <div class="flex-col">
                                            <div class="tab-pane-content">
                                                <h3>What information would you like to display?</h3>
                                                <div class="flex-row space-between vertical-center">
                                                    <div class="flex-col">
                                                        <ul>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Series Title</div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Series Description</div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Series Image</div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="flex-col">
                                                        <ul>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Post Titles</div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Post Preview (first 250 characters of your post)</div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Full text of Post</div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="field-item active-item">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="icon-wrapper" data-field = 'series-title'>
                                                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="field-name">Post Image</div>
                                                                    </div>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div style="padding-left: 40px;">
                                                    <a class="go-to-codepen">Continue</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="code-editor" class="tab-pane fade">
                                    <div class="tab-pan-content">
                                        <h3>
                                            Here is embed code. You can change style here to fit the look of your website.
                                        </h3>
                                        <?php require_once ASSETS_PATH . '/components/EmbedEditor/EmbedEditor.html';?>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </main>
                </div>
                <div id="add-items-tab" class="tab-pane fade <?php echo $panel == 'addNew' ? 'in active': '';?>">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Results
                        </div>
                    </div>
                    <?php require_once ASSETS_PATH . '/components/SeriesSearchPosts/SeriesSearchPosts.html';?>
                </div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/plugins/ace/build/src/ace.js"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/editSeries-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var isSeriesMine = <?php echo json_encode($isSeriesMine);?>;
    var initialSeries = <?php echo json_encode($series);?>;
    var initialPurchase = <?php echo json_encode($purchase);?>;
    const DEFAULT_IMAGE = '<?php echo $default_image;?>';
    const PANEL = <?php echo json_encode($panel);?>;
    var isStripeAvailable = <?php echo json_encode($isStripeAvailable);?>;
    var initialFormFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/editSeries-page.js?version=<?php echo time();?>"></script>

</body>
</html>