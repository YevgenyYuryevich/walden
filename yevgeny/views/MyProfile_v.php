<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 11.07.2018
 * Time: 11:34
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - thegreyshirt</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/myProfile-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-switchSubscription">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Your Profile
                </div>
            </div>
            <main class="main-content">
                <div class="container bootstrap snippets">
                    <div class="row">
                        <div class="col-xs-12">
                            <form class="form-horizontal">
                                <div class="panel panel-default">
                                    <div class="panel-body text-center">
                                        <div class="cover-image-wrapper">
                                            <img src="https://bootdey.com/img/Content/avatar/avatar6.png" class="img-circle profile-avatar" alt="User avatar">
                                            <input name="image" type="file"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">User info</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label required">Username</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="username" class="form-control" required>
                                                <div class="alert alert-danger">
                                                    Already exist
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label required">First name</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="f_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label required">Last name</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="l_name" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Contact info</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mobile number</label>
                                            <div class="col-sm-10">
                                                <input type="tel" name="phone" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label required">E-mail address</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" class="form-control" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Security</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Current password</label>
                                            <div class="col-sm-10">
                                                <input name="hash" type="password" class="form-control">
                                                <span href="javascript:;" class="helper-text">
                                                    Please enter current password to manage your secret info.
                                                </span>
                                                <div class="alert alert-danger">
                                                    Incorrect Password
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">New password</label>
                                            <div class="col-sm-10">
                                                <input name="new_hash" type="password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="button" class="btn btn-default cancel">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var user = <?php echo json_encode($user);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/myProfile-page.js?version=<?php echo time();?>"></script>

</body>
</html>