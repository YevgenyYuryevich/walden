<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.11.2018
 * Time: 08:31
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - walden</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/create_post-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/create_post-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="content-header">
            <h2 class="post-create-type">Create a unique post</h2>
            <div class="series-name"><?php echo $series['strSeries_title'];?></div>
        </header>
        <main class="main-content">
            <div class="tab-content">
                <div class="tab-pane fade <?php echo $create_type == 'from_scratch' || $create_type == 'menu_decision' || $create_type == 'switch_to' || $create_type == 'form' || $create_type == 'form_loop' ? 'in active' : ''?>" id="tab-normal-create">
                    <div class="flex-row margin-between">
                        <div class="flex-col">
                            <div class="tab-content">
                                <div class="tab-pane fade <?php echo $create_type == 'from_scratch' ? 'in active' : '';?>">
                                    <div class="create-from-scratch">
                                        <form class="create-post-form" action="" method="post">
                                            <div class="form-group">
                                                <label for="" class="control-label">Name your post:</label>
                                                <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Describe it:</label>
                                                <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                            </div>
                                            <div class="flex-row flex-wrap margin-between">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Post Type:</label>
                                                        <div class="dropdown" data-value="7">
                                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                                                <span>Text</span>
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="javascript:;" data-value="0">Audio</a></li>
                                                                <li><a href="javascript:;" data-value="2">Video</a></li>
                                                                <li hidden><a href="javascript:;" data-value="7">Text</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">
                                                            Money Saved, $ <span>(optional)</span>:
                                                        </label>
                                                        <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Keywords (optional):</label>
                                                <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                                <div class="terms-container flex-row margin-between">
                                                    <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                                <div class="cover-image-wrapper">
                                                    <div class="flex-row margin-between horizon-center vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <div class="img-wrapper">
                                                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="upload-helper-txt">Drag Image here to upload</div>
                                                        </div>
                                                    </div>
                                                    <input class="form-control" type="file" name="strPost_featuredimage" />
                                                </div>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="save-btn">Create</button>
                                                <a href="javascript:;" class="preview-post bottom-underline">Preview post</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?php echo $create_type == 'menu_decision' ? 'in active' : '';?>">
                                    <div class="create-menu-decision">
                                        <form action="" class="create-menu-decision-form" method="post">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Name your Menu:</label>
                                                        <input class="form-control" type="text" name="strPost_title" placeholder="type the name here"/>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="form-group options-cnt-slider">
                                                        <label for="" class="control-label">Options quantity:</label>
                                                        <input type="range" class="options-cnt-input form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Describe it:</label>
                                                <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                            </div>
                                            <div class="options-container">
                                                <div class="option-item sample" hidden>
                                                    <h2 class="option-number">Option <span class="number-value">1</span></h2>
                                                    <div class="flex-row margin-between">
                                                        <div class="flex-col mr-2">
                                                            <div class="form-group">
                                                                <label for="" class="control-label">Name of this option:</label>
                                                                <input class="form-control title-input" type="text" placeholder="type the name here"/>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="control-label">Description of this option:</label>
                                                                <textarea class="form-control body-input" type="text" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="cover-image-wrapper">
                                                                <div class="img-wrapper">
                                                                    <img class="option-image" />
                                                                </div>
                                                                <div class="cloud-img-wrapper">
                                                                    <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                                                </div>
                                                                <div>Drag Image here to upload</div>
                                                                <input class="form-control" type="file"/>
                                                                <a href="javascript:;" class="delete-action">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"></polygon>
                                            <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"></path>
                                        </g>
                                    </g>
                                </svg>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="save-btn">Create Menu</button>
                                                <a href="javascript:;" class="preview-post bottom-underline">Preview post</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?php echo $create_type == 'switch_to' ? 'in active' : '';?>">
                                    <div class="create-switch-to">
                                        <form class="create-switch-to-form" action="" method="post">
                                            <div class="form-group">
                                                <label for="" class="control-label">Name your post:</label>
                                                <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Describe it:</label>
                                                <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                            </div>
                                            <div class="flex-row flex-wrap margin-between redirect-setting-wrapper align-items-end">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Post Type:</label>
                                                        <div class="dropdown" data-value="9">
                                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                                                <span>redirect to</span>
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li hidden><a href="javascript:;" data-value="9">Redirect to</a></li>
                                                                <li><a href="javascript:;" data-value="7">Text</a></li>
                                                                <li><a href="javascript:;" data-value="2">Video</a></li>
                                                                <li><a href="javascript:;" data-value="0">Audio</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col mr-2">
                                                    <a href="#redirect-to-modal" class="switch-to-post-link" data-toggle="modal">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 512.422 512.422" style="enable-background:new 0 0 512.422 512.422;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path d="M41.053,223.464c2.667,1.067,5.76,1.067,8.427-0.213l83.307-37.867c5.333-2.56,7.573-8.96,5.013-14.293
                                                                            c-2.453-5.12-8.533-7.467-13.76-5.12l-58.347,26.56c27.84-83.307,105.387-138.987,194.667-138.987
                                                                            c93.547,0,175.36,62.507,198.933,152c1.493,5.653,7.36,9.067,13.013,7.573c5.653-1.493,9.067-7.36,7.573-13.013
                                                                            c-26.027-98.773-116.267-167.893-219.52-167.893c-98.453,0-184.107,61.44-215.04,153.387l-24.533-61.333
                                                                            c-1.813-5.547-7.893-8.64-13.44-6.827c-5.547,1.813-8.64,7.893-6.827,13.44c0.107,0.427,0.32,0.853,0.533,1.28l34.027,85.333
                                                                            C36.146,220.158,38.279,222.398,41.053,223.464z"/>
                                                                        <path d="M511.773,380.904c-0.107-0.213-0.213-0.427-0.213-0.64l-34.027-85.333c-1.067-2.667-3.2-4.907-5.973-5.973
                                                                            c-2.667-1.067-5.76-0.96-8.427,0.213l-83.307,37.867c-5.44,2.24-8,8.533-5.76,13.973c2.24,5.44,8.533,8,13.973,5.76
                                                                            c0.213-0.107,0.427-0.213,0.64-0.32l58.347-26.56c-28.053,83.307-105.707,138.987-194.88,138.987
                                                                            c-93.547,0-175.36-62.507-198.933-152c-1.493-5.653-7.36-9.067-13.013-7.573c-5.653,1.493-9.067,7.36-7.573,13.013
                                                                            c25.92,98.88,116.267,167.893,219.52,167.893c98.453,0,184-61.44,215.04-153.387l24.533,61.333
                                                                            c2.027,5.547,8.107,8.427,13.653,6.4C510.919,392.531,513.799,386.451,511.773,380.904z"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col fix-col mr-2">
                                                    <div class="or-txt">or</div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control redirect-url-input" placeholder="insert URL"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Keywords (optional):</label>
                                                        <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                                        <div class="terms-container flex-row margin-between">
                                                            <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">
                                                            Money Saved, $ <span>(optional)</span>:
                                                        </label>
                                                        <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                                <div class="cover-image-wrapper">
                                                    <div class="flex-row margin-between horizon-center vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <div class="img-wrapper">
                                                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="upload-helper-txt">Drag Image here to upload</div>
                                                        </div>
                                                    </div>
                                                    <input class="form-control" type="file" name="strPost_featuredimage" />
                                                </div>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="save-btn">Create</button>
                                                <a href="javascript:;" class="preview-post bottom-underline">Preview post</a>
                                            </div>
                                        </form>
                                        <div class="modal fade redirect-to-modal" tabindex="-1" role="dialog" id="redirect-to-modal">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <a class="modal-close" data-dismiss="modal"></a>
                                                    <div class="modal-body">
                                                        <div class="redirect-to-content-inner">
                                                            <h3 class="select-page-title">Select page where to redirect</h3>
                                                            <div class="posts-tree-wrapper">
                                                                <?php require ASSETS_PATH . '/components/SeriesPostsTree/SeriesPostsTree.html';?>
                                                            </div>
                                                            <div class="btn-wrapper">
                                                                <button class="choose-btn btn">Choose</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?php echo $create_type == 'form' ? 'in active' : '';?>">
                                    <div class="create-with-form">
                                        <form action="" method="post">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Name your post:</label>
                                                        <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                                    </div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Post Type:</label>
                                                        <div class="dropdown" data-value="10">
                                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                                                <span>Form</span>
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li hidden><a href="javascript:;" data-value="10">Form</a></li>
                                                                <li><a href="javascript:;" data-value="7">Text</a></li>
                                                                <li><a href="javascript:;" data-value="2">Video</a></li>
                                                                <li><a href="javascript:;" data-value="0">Audio</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Describe it:</label>
                                                <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                            </div>
                                            <div class="form-create-wrapper">
                                                <div class="flex-row margin-between vertical-center type-select-row">
                                                    <div class="flex-col fix-col mr-2">
                                                        <div class="choose-question-type-txt">Choose question type</div>
                                                    </div>
                                                    <div class="flex-col fix-col mr-2">
                                                        <a href="javascript:;" class="type-choose-item active-item" data-type="checkbox">
                                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 469.333 469.333;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path d="M426.667,0h-384C19.146,0,0,19.135,0,42.667v384c0,23.531,19.146,42.667,42.667,42.667h384
                                                                            c23.521,0,42.667-19.135,42.667-42.667v-384C469.333,19.135,450.188,0,426.667,0z M448,426.667
                                                                            c0,11.76-9.563,21.333-21.333,21.333h-384c-11.771,0-21.333-9.573-21.333-21.333v-384c0-11.76,9.563-21.333,21.333-21.333h384
                                                                            c11.771,0,21.333,9.573,21.333,21.333V426.667z"/>
                                                                        <path d="M365.583,131.344L192.187,314.927l-88.25-98.062c-3.979-4.385-10.708-4.729-15.083-0.792s-4.729,10.688-0.792,15.063
                                                                            l96,106.667c2,2.219,4.833,3.49,7.813,3.531c0.042,0,0.083,0,0.125,0c2.938,0,5.729-1.208,7.75-3.344l181.333-192
                                                                            c4.042-4.281,3.854-11.031-0.417-15.073C376.396,126.865,369.604,127.062,365.583,131.344z"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                    <div class="flex-col fix-col mr-2">
                                                        <div class="or-txt">or</div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <a href="javascript:;" class="type-choose-item" data-type="text">
                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <path style="fill:#303C42;" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224C83.146,0,64,19.135,64,42.667
                                                                    v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                <path style="fill:#E6E6E6;" d="M341.333,36.417l70.25,70.25h-48.917c-11.771,0-21.333-9.573-21.333-21.333V36.417z"/>
                                                                <path style="fill:#FFFFFF;" d="M405.333,490.667H106.667c-11.771,0-21.333-9.573-21.333-21.333V42.667
                                                                    c0-11.76,9.563-21.333,21.333-21.333H320v64C320,108.865,339.146,128,362.667,128h64v341.333
                                                                    C426.667,481.094,417.104,490.667,405.333,490.667z"/>
                                                                <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-30.0002" y1="641.8779" x2="-25.7514" y2="637.6291" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                    <stop  offset="0" style="stop-color:#000000;stop-opacity:0.1"/>
                                                                    <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                                                                </linearGradient>
                                                                <path style="fill:url(#SVGID_1_);" d="M362.667,128c-10.005,0-19.098-3.605-26.383-9.397l-0.259-0.027l90.642,90.642V128H362.667z"
                                                                />
                                                                <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-46.4381" y1="639.0643" x2="-23.653" y2="628.4363" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                    <stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
                                                                    <stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
                                                                </linearGradient>
                                                                <path style="fill:url(#SVGID_2_);" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224
                                                                    C83.146,0,64,19.135,64,42.667v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                <g>
                                                                    <path style="fill:#303C42;" d="M352,277.333h-85.333c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h21.333V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667H320c5.896,0,10.667-4.771,10.667-10.667c0-5.896-4.771-10.667-10.667-10.667v-85.333h21.333
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667V288C362.667,282.104,357.896,277.333,352,277.333z"/>
                                                                    <path style="fill:#303C42;" d="M234.667,384V234.667h42.667c0,5.896,4.771,10.667,10.667,10.667
                                                                        c5.896,0,10.667-4.771,10.667-10.667V224c0-5.896-4.771-10.667-10.667-10.667H160c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h42.667V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667h21.333c5.896,0,10.667-4.771,10.667-10.667C245.333,388.771,240.563,384,234.667,384z"/>
                                                                </g>
                                                                </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="flex-row margin-between flex-wrap">
                                                    <div class="flex-col fb-0 mr-2">
                                                        <div class="multi-line-background">
                                                            <textarea class="form-question-input" rows="3" required placeholder="My question is..."></textarea>
                                                            <div class="background-placeholder"></div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fb-0">
                                                        <div class="multi-line-background">
                                                            <textarea class="form-helper-input" rows="3" required placeholder="This hint is help of..."></textarea>
                                                            <div class="background-placeholder"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-wrapper">
                                                    <button class="add-form-btn btn" type="button">Create Form</button>
                                                </div>
                                            </div>
                                            <div class="form-fields-wrapper">
                                                <div class="form-fields-title-wrapper">
                                                    <div class="flex-row">
                                                        <div class="flex-col">
                                                            <div class="form-fields-title">Form Fields</div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <a href="javascript:;" class="preview-form-link bottom-underline">preview form</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-fiels-container">
                                                    <div class="form-field-item sample" hidden>
                                                        <div class="flex-row margin-between vertical-center">
                                                            <div class="flex-col fix-col">
                                                                <div class="append-action append-question flex-centering"><div>Q</div></div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="append-action append-answer flex-centering"><div>A</div></div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <a class="item-action delete-action" href="javascript:;">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                </a>
                                                            </div>
                                                            <div class="flex-col">
                                                                <div type="text" class="form-control item-name-input"></div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="field-item-type-wrapper">
                                                                    <svg class="text-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <path style="fill:#303C42;" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224C83.146,0,64,19.135,64,42.667
                                                                    v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                        <path style="fill:#E6E6E6;" d="M341.333,36.417l70.25,70.25h-48.917c-11.771,0-21.333-9.573-21.333-21.333V36.417z"/>
                                                                        <path style="fill:#FFFFFF;" d="M405.333,490.667H106.667c-11.771,0-21.333-9.573-21.333-21.333V42.667
                                                                    c0-11.76,9.563-21.333,21.333-21.333H320v64C320,108.865,339.146,128,362.667,128h64v341.333
                                                                    C426.667,481.094,417.104,490.667,405.333,490.667z"/>
                                                                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-30.0002" y1="641.8779" x2="-25.7514" y2="637.6291" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                            <stop  offset="0" style="stop-color:#000000;stop-opacity:0.1"/>
                                                                            <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                                                                        </linearGradient>
                                                                        <path style="fill:url(#SVGID_1_);" d="M362.667,128c-10.005,0-19.098-3.605-26.383-9.397l-0.259-0.027l90.642,90.642V128H362.667z"
                                                                        />
                                                                        <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-46.4381" y1="639.0643" x2="-23.653" y2="628.4363" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                            <stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
                                                                            <stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
                                                                        </linearGradient>
                                                                        <path style="fill:url(#SVGID_2_);" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224
                                                                    C83.146,0,64,19.135,64,42.667v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                        <g>
                                                                            <path style="fill:#303C42;" d="M352,277.333h-85.333c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h21.333V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667H320c5.896,0,10.667-4.771,10.667-10.667c0-5.896-4.771-10.667-10.667-10.667v-85.333h21.333
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667V288C362.667,282.104,357.896,277.333,352,277.333z"/>
                                                                            <path style="fill:#303C42;" d="M234.667,384V234.667h42.667c0,5.896,4.771,10.667,10.667,10.667
                                                                        c5.896,0,10.667-4.771,10.667-10.667V224c0-5.896-4.771-10.667-10.667-10.667H160c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h42.667V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667h21.333c5.896,0,10.667-4.771,10.667-10.667C245.333,388.771,240.563,384,234.667,384z"/>
                                                                        </g>
                                                                </svg>
                                                                    <svg class="checkbox-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 469.333 469.333;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path d="M426.667,0h-384C19.146,0,0,19.135,0,42.667v384c0,23.531,19.146,42.667,42.667,42.667h384
                                                                            c23.521,0,42.667-19.135,42.667-42.667v-384C469.333,19.135,450.188,0,426.667,0z M448,426.667
                                                                            c0,11.76-9.563,21.333-21.333,21.333h-384c-11.771,0-21.333-9.573-21.333-21.333v-384c0-11.76,9.563-21.333,21.333-21.333h384
                                                                            c11.771,0,21.333,9.573,21.333,21.333V426.667z"/>
                                                                        <path d="M365.583,131.344L192.187,314.927l-88.25-98.062c-3.979-4.385-10.708-4.729-15.083-0.792s-4.729,10.688-0.792,15.063
                                                                            l96,106.667c2,2.219,4.833,3.49,7.813,3.531c0.042,0,0.083,0,0.125,0c2.938,0,5.729-1.208,7.75-3.344l181.333-192
                                                                            c4.042-4.281,3.854-11.031-0.417-15.073C376.396,126.865,369.604,127.062,365.583,131.344z"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                            </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flex-row flex-wrap margin-between">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Keywords (optional):</label>
                                                        <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                                        <div class="terms-container flex-row margin-between">
                                                            <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">
                                                            Money Saved, $ <span>(optional)</span>:
                                                        </label>
                                                        <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                                <div class="cover-image-wrapper">
                                                    <div class="flex-row margin-between horizon-center vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <div class="img-wrapper">
                                                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="upload-helper-txt">Drag Image here to upload</div>
                                                        </div>
                                                    </div>
                                                    <input class="form-control" type="file" name="strPost_featuredimage" />
                                                </div>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="save-btn">Create</button>
                                                <a href="javascript:;" class="preview-post bottom-underline">Preview post</a>
                                            </div>
                                            <div class="answered-form-field-wrapper sample" hidden>
                                                <?php require_once ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?php echo $create_type == 'form_loop' ? 'in active' : '';?>">
                                    <div class="create-with-form-loop">
                                        <form action="" method="post">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Name your post:</label>
                                                        <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                                                    </div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Post Type:</label>
                                                        <div class="dropdown" data-value="10">
                                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                                                <span>Form</span>
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li hidden><a href="javascript:;" data-value="10">Form</a></li>
                                                                <li><a href="javascript:;" data-value="7">Text</a></li>
                                                                <li><a href="javascript:;" data-value="2">Video</a></li>
                                                                <li><a href="javascript:;" data-value="0">Audio</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Describe it:</label>
                                                <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                                            </div>
                                            <div class="form-loop-create-wrapper">
                                                <?php require_once ASSETS_PATH . '/components/FormLoopEditor/FormLoopEditor.html';?>
                                            </div>
                                            <div class="form-fields-wrapper">
                                                <div class="form-fields-title-wrapper">
                                                    <div class="flex-row">
                                                        <div class="flex-col">
                                                            <div class="form-fields-title">Form Fields</div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <a href="javascript:;" class="preview-form-link bottom-underline">preview form</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-fiels-container">
                                                    <div class="form-field-item sample" hidden>
                                                        <div class="flex-row margin-between vertical-center">
                                                            <div class="flex-col fix-col">
                                                                <div class="append-action append-question flex-centering"><div>Q</div></div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="append-action append-answer flex-centering"><div>A</div></div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <a class="item-action delete-action" href="javascript:;">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                </a>
                                                            </div>
                                                            <div class="flex-col">
                                                                <div type="text" class="form-control item-name-input"></div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="field-item-type-wrapper">
                                                                    <svg class="text-icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <path style="fill:#303C42;" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224C83.146,0,64,19.135,64,42.667
                                                                    v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                        <path style="fill:#E6E6E6;" d="M341.333,36.417l70.25,70.25h-48.917c-11.771,0-21.333-9.573-21.333-21.333V36.417z"/>
                                                                        <path style="fill:#FFFFFF;" d="M405.333,490.667H106.667c-11.771,0-21.333-9.573-21.333-21.333V42.667
                                                                    c0-11.76,9.563-21.333,21.333-21.333H320v64C320,108.865,339.146,128,362.667,128h64v341.333
                                                                    C426.667,481.094,417.104,490.667,405.333,490.667z"/>
                                                                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-30.0002" y1="641.8779" x2="-25.7514" y2="637.6291" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                            <stop  offset="0" style="stop-color:#000000;stop-opacity:0.1"/>
                                                                            <stop  offset="1" style="stop-color:#000000;stop-opacity:0"/>
                                                                        </linearGradient>
                                                                        <path style="fill:url(#SVGID_1_);" d="M362.667,128c-10.005,0-19.098-3.605-26.383-9.397l-0.259-0.027l90.642,90.642V128H362.667z"
                                                                        />
                                                                        <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-46.4381" y1="639.0643" x2="-23.653" y2="628.4363" gradientTransform="matrix(21.3333 0 0 -21.3333 996.3334 13791.667)">
                                                                            <stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:0.2"/>
                                                                            <stop  offset="1" style="stop-color:#FFFFFF;stop-opacity:0"/>
                                                                        </linearGradient>
                                                                        <path style="fill:url(#SVGID_2_);" d="M444.875,109.792L338.208,3.125c-2-2-4.708-3.125-7.542-3.125h-224
                                                                    C83.146,0,64,19.135,64,42.667v426.667C64,492.865,83.146,512,106.667,512h298.667C428.854,512,448,492.865,448,469.333v-352
                                                                    C448,114.5,446.875,111.792,444.875,109.792z"/>
                                                                        <g>
                                                                            <path style="fill:#303C42;" d="M352,277.333h-85.333c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h21.333V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667H320c5.896,0,10.667-4.771,10.667-10.667c0-5.896-4.771-10.667-10.667-10.667v-85.333h21.333
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667V288C362.667,282.104,357.896,277.333,352,277.333z"/>
                                                                            <path style="fill:#303C42;" d="M234.667,384V234.667h42.667c0,5.896,4.771,10.667,10.667,10.667
                                                                        c5.896,0,10.667-4.771,10.667-10.667V224c0-5.896-4.771-10.667-10.667-10.667H160c-5.896,0-10.667,4.771-10.667,10.667v10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667c5.896,0,10.667-4.771,10.667-10.667h42.667V384c-5.896,0-10.667,4.771-10.667,10.667
                                                                        c0,5.896,4.771,10.667,10.667,10.667h21.333c5.896,0,10.667-4.771,10.667-10.667C245.333,388.771,240.563,384,234.667,384z"/>
                                                                        </g>
                                                                </svg>
                                                                    <svg class="checkbox-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 469.333 469.333;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path d="M426.667,0h-384C19.146,0,0,19.135,0,42.667v384c0,23.531,19.146,42.667,42.667,42.667h384
                                                                            c23.521,0,42.667-19.135,42.667-42.667v-384C469.333,19.135,450.188,0,426.667,0z M448,426.667
                                                                            c0,11.76-9.563,21.333-21.333,21.333h-384c-11.771,0-21.333-9.573-21.333-21.333v-384c0-11.76,9.563-21.333,21.333-21.333h384
                                                                            c11.771,0,21.333,9.573,21.333,21.333V426.667z"/>
                                                                        <path d="M365.583,131.344L192.187,314.927l-88.25-98.062c-3.979-4.385-10.708-4.729-15.083-0.792s-4.729,10.688-0.792,15.063
                                                                            l96,106.667c2,2.219,4.833,3.49,7.813,3.531c0.042,0,0.083,0,0.125,0c2.938,0,5.729-1.208,7.75-3.344l181.333-192
                                                                            c4.042-4.281,3.854-11.031-0.417-15.073C376.396,126.865,369.604,127.062,365.583,131.344z"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                            </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flex-row flex-wrap margin-between">
                                                <div class="flex-col fb-0 mr-2">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Keywords (optional):</label>
                                                        <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                                                        <div class="terms-container flex-row margin-between">
                                                            <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="form-group">
                                                        <label for="" class="control-label">
                                                            Money Saved, $ <span>(optional)</span>:
                                                        </label>
                                                        <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Upload Cover Image for this Post (optional)</label>
                                                <div class="cover-image-wrapper">
                                                    <div class="flex-row margin-between horizon-center vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <div class="img-wrapper">
                                                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="upload-helper-txt">Drag Image here to upload</div>
                                                        </div>
                                                    </div>
                                                    <input class="form-control" type="file" name="strPost_featuredimage" />
                                                </div>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="save-btn">Create</button>
                                                <a href="javascript:;" class="preview-post bottom-underline">Preview post</a>
                                            </div>
                                            <div class="answered-form-field-wrapper sample" hidden>
                                                <?php require_once ASSETS_PATH . '/components/AnsweredFormField/AnsweredFormField.html';?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="right-portion">
                                <div class="select-node-txt">Select node after which you want to insert post</div>
                                <div class="posts-tree-wrapper">
                                    <?php require ASSETS_PATH . '/components/SeriesPostsTree/SeriesPostsTree.html';?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?php echo $create_type == 'from_search' ? 'in active' : ''?>" id="tab-search-create">
                    <div class="create-from-search">
                        <header class="pane-header">
                            <div class="flex-row space-between vertical-center">
                                <div class="flex-col fix-col">
                                    <div class="form-group">
                                        <label for="" class="control-label">Search for items:</label>
                                        <input type="text" class="form-control search-input" placeholder="start typing terms for your experience" />
                                    </div>
                                    <div class="terms-container flex-row margin-between">
                                        <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="or-txt">or</div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="cover-image-wrapper">
                                        <div class="flex-row margin-between horizon-center vertical-center">
                                            <div class="flex-col fix-col">
                                                <div class="img-wrapper">
                                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="upload-helper-txt">Drag Image here to upload</div>
                                            </div>
                                        </div>
                                        <input class="form-control" type="file" name="strSeries_image">
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div class="pane-body">
                            <div class="search-container">
                                <div class="search-row sample" hidden>
                                    <header class="search-row-header">
                                        <div class="flex-row vertical-center space-between">
                                            <div class="flex-col fix-col">
                                                <h2 class="from-name">from YouTube</h2>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <a href="javascript:;" class="items-found-wrapper">
                                                    <span class="found-value">0</span> items found
                                                </a>
                                            </div>
                                        </div>
                                    </header>
                                    <div class="search-row-body">
                                        <div class="search-item sample" hidden>
                                            <div class="item-header">
                                                <div class="flex-row space-between">
                                                    <div class="flex-col"></div>
                                                    <div class="flex-col fix-col">
                                                        <aside class="added-icon-wrapper">
                                                            <svg class="added-icon-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        </aside>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-img-wrapper">
                                                <img class="item-img" />
                                                <aside class="blog-duration">
                                                    waiting...
                                                </aside>
                                                <div class="hover-content">
                                                    <h4>Add to series</h4>
                                                    <div class="footer-buttons">
                                                        <div class="flex-row">
                                                            <div class="flex-col">
                                                                <a href="javascript:;" class="preview-btn" target="_blank">
                                                                    <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                                                </a>
                                                            </div>
                                                            <div class="flex-col last-col">
                                                                <a href="javascript:;" class="join-btn">
                                                                    <svg class="join-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                            <g>
                                                                                <g>
                                                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                    <svg class="added-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                        <g>
                                                                            <g>
                                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                                            </g>
                                                                        </g>
                                                                    </svg>
                                                                    add
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-title">
                                                Daily recipes
                                            </div>
                                        </div>
                                        <div class="flex-row margin-between">
                                            <div class="flex-col fix-col overflow-visible">
                                                <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                </a>
                                            </div>
                                            <div class="flex-col fb-0">
                                                <div class="search-items-list">
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col overflow-visible">
                                                <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="selected-items-wrapper grid-structure-mode">
                                <h2 class="selected-items-title">Selected Items</h2>
                                <header class="structure-header">
                                    <div class="flex-row space-between vertical-center">
                                        <div class="flex-col fix-col">
                                            <div class="drag-title-wrapper">
                                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/move.png" alt=""> <span class="drag-title-txt">Drag on items to change the order</span>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="structure-type-select-wrapper">
                                                <a href="#structure-grid-tab" data-toggle="tab" class="show-on-list-structure">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"></path>
                                                    <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"></path>
                                                    <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"></path>
                                                    <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                                </a>
                                                <a href="#structure-list-tab" data-toggle="tab" class="show-on-grid-structure">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                                    c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                                <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                                    h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                                <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                                    h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                            </g>
                                                        <g>
                                                            <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                                                            <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                                                            <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                                                        </g>
                                        </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </header>
                                <div class="structure-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="structure-list-tab">
                                            <div class="posts-container">
                                                <li class="sample post-row" hidden>
                                                    <div class="post-row-inner">
                                                        <div class="flex-row space-between margin-between">
                                                            <div class="flex-col fix-col">
                                                                <a class="item-action delete-action" href="javascript:;">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                </a>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="item-img-wrapper">
                                                                    <img class="item-img">
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="item-type-wrapper">
                                                                    <div class="item-type-icon type-post-icon show-on-post-node"></div>
                                                                    <div class="item-type-icon type-post-icon show-on-menu-node"></div>
                                                                    <div class="item-type-icon type-post-icon show-on-path-node"></div>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="media-icon-wrapper">
                                                                    <i class="fa fa-youtube-play video-icon"></i>
                                                                    <i class="fa fa-volume-up audio-icon"></i>
                                                                    <i class="fa fa-file-text text-icon"></i>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col">
                                                                <div class="post-title"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <ul class="posts-list">
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade in active" id="structure-grid-tab">
                                            <article class="post-item sample" hidden>
                                                <header class="item-header">
                                                    <div class="flex-row vertical-center margin-between">
                                                        <div class="flex-col fix-col">
                                                            <div class="media-icon-wrapper">
                                                                <i class="fa fa-youtube-play video-icon"></i>
                                                                <i class="fa fa-volume-up audio-icon"></i>
                                                                <i class="fa fa-file-text text-icon"></i>
                                                            </div>
                                                        </div>
                                                        <div class="flex-col">
                                                            <div class="day-wrapper">
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <a class="item-action delete-action" href="javascript:;">
                                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </header>
                                                <div class="item-type-body">
                                                    <div class="blog-type type-0 sample" hidden>
                                                    </div>
                                                    <div class="blog-type type-2 sample" hidden>
                                                        <img alt="">
                                                    </div>
                                                    <div class="blog-type type-default sample" hidden>
                                                        Lorem lpsum is simply dummy text of the printing and type
                                                    </div>
                                                    <aside class="blog-duration">
                                                        waiting...
                                                    </aside>
                                                </div>
                                                <div class="item-title">Why gifted student needs to be taught formal writing</div>
                                            </article>
                                            <div class="items-container">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="structure-footer">
                                    <div class="button-wrapper">
                                        <button class="save-btn">Add selected</button>
                                    </div>
                                    <div class="still-not-find-txt">Still didn't find what were looking for?</div>
                                    <div class="btn-wrapper">
                                        <button class="load-more-btn btn">Load more</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?php echo $create_type == 'bulk_upload' ? 'in active' : ''?>">
                    <div class="create-from-bulk-upload">
                        <header class="pane-header">
                            <div class="flex-row vertical-center space-between margin-between">
                                <div class="flex-col fix-col">
                                    <div class="add-new-wrapper mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        <span class="add-new-txt bottom-underline">Add New Item</span>
                                        <input type="file" class="add-new-input" />
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="or-txt mr-2">or</div>
                                </div>
                                <div class="flex-col">
                                    <div class="cover-image-wrapper">
                                        <div class="flex-row margin-between horizon-center vertical-center">
                                            <div class="flex-col fix-col">
                                                <div class="img-wrapper">
                                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="upload-helper-txt">Drag Image here to upload</div>
                                            </div>
                                        </div>
                                        <input class="form-control" type="file" name="strSeries_image">
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div class="pane-body">
                            <div class="upload-items-title-wrapper">
                                <div class="flex-row space-between vertical-center">
                                    <div class="flex-col">
                                        <h2 class="uploaded-items-txt">Uploaded Items</h2>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="uploaded-cnt bottom-underline">
                                            <span class="cnt-value">0</span> items uploaded
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="drag-title-wrapper">
                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/move.png" alt=""> <span class="drag-title-txt">Drag on items to change the order</span>
                            </div>
                            <div class="items-container">
                                <div class="upload-term-item sample" hidden>
                                    <div class="flex-row margin-between vertical-center">
                                        <div class="flex-col fix-col edit-col">
                                            <a class="item-action edit-action flex-centering" href="javascript:;">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                            </a>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a class="item-action delete-action" href="javascript:;">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                            </a>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="item-img-wrapper">
                                                <img class="item-img">
                                                <input type="file" class="image-input" />
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="form-group">
                                                <input type="text" class="form-control item-title-input" />
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="file-type">
                                                image/jpeg
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pane-footer">
                            <div class="button-wrapper">
                                <button class="save-btn">Upload all</button>
                            </div>
                            <div class="changed-txt">if you changed your mind to upload</div>
                            <div class="btn-wrapper">
                                <button class="cancel-all-btn btn">Cancel all</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var createType = <?php echo json_encode($create_type);?>;
    var series = <?php echo json_encode($series);?>;
    var isSeriesMine = <?php echo json_encode(true);?>;
    var formFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers  = <?php echo json_encode($formFieldAnswers );?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/create_post-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/create_post-page.js?version=<?php echo time();?>"></script>

</body>
</html>