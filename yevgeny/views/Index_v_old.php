<?php
/**
 * Client: Nathaniel Baca
 * Developer: Other Developer
 * Created by Something.
 * Date: SomeDate
 * Time: SomeTime
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Walden.ly.  Make time for what matters.</title>
    <meta name="description" content="Walden.ly is about simplifying life, so you can focus on the things that matter with focus, presence, and peace of mind.">

    <!--Css contection!-->
    <link rel="stylesheet" href="assets/css/yevgeny/index-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/yevgeny/index-page.css?version=<?php echo time();?>" />

</head>

<body>
<div class="content_container">
    <div class="slider">
        <img id="itemed" class="slider_item" src="assets/images/slide_1.jpg">
        <div class="s_content">
            <div class="titled">Walden.ly</div>
            <div class="subtitled">Make time for what matters.</div>
            <div class="h-content">We provide you with daily solutions and experiences to improve your life so you can focus on what matters and stay commited to your values.</div>
            <div class="solutions-wrap">
                <div class="flex-row space-between margin-between vertical-center">
                    <div class="flex-col">
                        <div class="solution-wrap">
                            <h3>SOLUTIONS</h3>
                            <div>to simplify daily life</div>
                        </div>
                    </div>
                    <div class="flex-col">
                        <div class="plus-wrap">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 42 42" style="enable-background:new 0 0 42 42;" xml:space="preserve">
                                <polygon points="42,20 22,20 22,0 20,0 20,20 0,20 0,22 20,22 20,42 22,42 22,22 42,22 "/>
                            </svg>
                        </div>
                    </div>
                    <div class="flex-col">
                        <div class="solution-wrap">
                            <h3>EXPERIENCES</h3>
                            <div>to enrich daily life</div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="login" id="h_sign_up" class="buttoner " style="margin:auto;margin-top:10px;">Login/Register</a>
        </div>
    </div>
    <div style="background:#14272e;">
        <div id="start" class="row head_wrapper backer">
            <div class="col-md-3"></div>
            <div class="col-md-9" style="min-height:955px;">
                <div class="philosophy">
                    <div class="paddinger" >
                        <div class="titler" data-aos="fade-up" data-aos-duration="700" data-aos-once="true"> The Philosophy </div>
                        <div class="texter" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">"Things which matter most must never be at the mercy of things which matter least."</div>
                        <div class="name" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">  Johann Wolfgang Von Goethe  </div>
                        <!---
                        <div class="liner">  </div>
                        <a href="login?register" id="h_join"  data-aos="fade-up" data-aos-duration="700" data-aos-once="true"   data-aos="fade-up" data-aos-duration="700" data-aos-delay="300" data-aos-once="true" class="buttoner ">Join Now</a>
                        --->

                    </div>
                </div>
            </div>
            <div class="col-md-8 marger">
                <div class="philosophy_2 " data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                    <div class="titler"> The Method </div>
                    <div class="texter">
                    	
                    	<ol>
                    		<li style="padding-bottom:24px;"><span style="font-size:26px">Simplify</span>
                    			<ul>
                    				<li>Select areas in your life where you would benefit from simplifying.  We will provide you with daily solutions to overcome and simplify those challenges.</li>
                    				
                    			</ul>
                    		</li>
                    		<li style="padding-bottom:24px;"><span style="font-size:26px;">Enrich</span>
                    			<ul>
                    				<li>Enrich your life with daily experiences in areas that bring value to your life.</li>
                    			
                    			</ul>
                    		</li>
                    		<li style="padding-bottom:24px;"><span style="font-size:26px">Set daily goals</span>
                    			<ul>
                    				<li>Keep a daily list of things that you will do to remind yourself of what matters.</li>
                    				
                    			</ul>
                    		</li>
                    		<li style="padding-bottom:24px;"><span style="font-size:26px">Build your community</span>
                    			<ul>
                    				<li>Create solutions and experiences that are perfect for you, then share them with others.</li>
                    			
                    			</ul>
                    		</li>
                    	</ol>
                    	
                    </div>
              
                </div>
            </div>
        </div>
    </div>
    <div class="subcontent_container">
        <div class="wrapper">
            <div  class=" header " data-aos="fade-up" data-aos-duration="700">
                <div class="line_container"> <img src="assets/images/line.png" alt="line"> </div>
                <div class="text_container" style="margin-top:25px;">
                    <div class="subtitle"> FEATURED GREY SHIRTS </div>
                    <div class="title"> DAILY SOLUTIONS </div>
                </div>
                <div class="line_container" style="margin-top:20px;"> <img src="assets/images/line.png" alt="line"> </div>
            </div>
            <div class="recent_container">

                <!---
                <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                  <div class="col-md-6 " style="padding:0;">
                    <div class="content">
                      <div class="small">ROMANCE</div>
                      <div class="title">To My Love</div>
                      <div style="width:100%; text-align:center;">
                          <img src="assets/images/line_1.png" alt="" class="imager_prew"  style="height:38px; width:38px;">
                      </div>
                      <div class="sub_text">Romantic gestures to say simply, I love you.</div>
                      <div class="sub_button">	VIEW MORE	</div>
                    </div>
                  </div>
                  <div class="col-md-6" style="padding:0;">
                    <div class="imager">
                      <img src="assets/images/work-img-2.jpg" class="imager_prew"  alt="" style="width:100%; height:100%;">

                    </div>
                  </div>
                </div>
                --->

                <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">

                    <div class="col-md-6 " style="padding:0;">
                        <div class="content">
                            <div class="small">MINDFULNESS</div>
                            <div class="title">Commit to the Present</div>
                            <div style="width:100%; text-align:center;">
                                <img src="assets/images/line_1.png" alt="" style="height:38px; width:38px;">
                            </div>
                            <div class="sub_text">Daily meditations that thoughtfully introduce the practice of mindfulness.</div>
                            <div class="sub_button" onclick="location.href='viewexperience.php?id=5';">	VIEW MORE	</div>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0;">
                        <div class="imager">
                            <img src="assets/images/featured_meditation.jpg" class="imager_prew"  alt="" style="width:100%; height:100%;">

                        </div>
                    </div>
                </div>




                <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                    <div class="col-md-6" style="padding:0;">
                        <div class="imager">
                            <img src="assets/images/featured_running.jpg" alt="" class="imager_prew" style="width:100%; height:100%;">

                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0;">
                        <div class="content">
                            <div class="small">MIND & BODY</div>
                            <div class="title">Run Your First Half-Marathon</div>
                            <div style="width:100%; text-align:center;">
                                <img src="assets/images/line_1.png" alt="" style="height:38px; width:38px;">
                            </div>
                            <div class="sub_text">Daily training to help you run a half-marathon.</div>
                            <div class="sub_button" onclick="location.href='viewexperience.php?id=74';">	VIEW MORE	</div>
                        </div>
                    </div>
                </div>


                <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">

                    <div class="col-md-6 " style="padding:0;">
                        <div class="content">
                            <div class="small">CHILDHOOD DEVELOPMENT</div>
                            <div class="title">Enjoy Outdoor Activities with Your Children</div>
                            <div style="width:100%; text-align:center;">
                                <img src="assets/images/line_1.png" alt="" style="height:38px; width:38px;">
                            </div>
                            <div class="sub_text">Learn daily ways to spend time with you child outdoors to broaden your child's mind and appreciation for the world.</div>
                            <div class="sub_button" onclick="location.href='viewexperience.php?id=91';">	VIEW MORE	</div>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0;">
                        <div class="imager">
                            <img src="assets/images/children_1.jpg" class="imager_prew"  alt="" style="width:100%; height:100%;">

                        </div>
                    </div>
                </div>





                <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                    <div class="col-md-6" style="padding:0;">
                        <div class="imager">
                            <img src="assets/images/featured_meal.jpg" alt="" class="imager_prew" style="width:100%; height:100%;">

                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0;">
                        <div class="content">
                            <div class="small">FOOD</div>
                            <div class="title">Plan Your Meals with Ease</div>
                            <div style="width:100%; text-align:center;">
                                <img src="assets/images/line_1.png" alt="" style="height:38px; width:38px;">
                            </div>
                            <div class="sub_text">Receive meal recommendations on a daily basis and easily switch meals in advance to have a perfectly schedule meal plan.</div>
                            <div class="sub_button" onclick="location.href='exploreinterests?id=15';">	VIEW MORE	</div>
                        </div>
                    </div>
                </div>




   <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">

                    <div class="col-md-6 " style="padding:0;">
                        <div class="content">
                            <div class="small">FICTION</div>
                            <div class="title">Read the Classics</div>
                            <div style="width:100%; text-align:center;">
                                <img src="assets/images/line_1.png" alt="" style="height:38px; width:38px;">
                            </div>
                            <div class="sub_text">Daily short stories from authors including Anton Chekhov, Virginia Woolf, James Joyce, Joseph Conrad, Edgar Allan Poe, and more.</div>
                            <div class="sub_button" onclick="location.href='viewexperience.php?id=17';">	VIEW MORE	</div>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0;">
                        <div class="imager">
                            <img src="assets/images/featured_short_stories.jpg" class="imager_prew"  alt="" style="width:100%; height:100%;">

                        </div>
                    </div>
                </div>








                <div class="row recent_works" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                    <div class="col-md-6" style="padding:0;">
                        <div class="imager">
                            <img src="assets/images/featuredwork4.jpg" alt="" class="imager_prew" style="width:100%; height:100%;">

                        </div>
                    </div>
                    <div class="col-md-6" style="padding:0;">
                        <div class="content">
                            <div class="small">FOR YOU</div>
                            <div class="title">Create Your Own Solutions + Experiences</div>
                            <div style="width:100%; text-align:center;">
                                <img src="assets/images/line_1.png" alt="" style="height:38px; width:38px;">
                            </div>
                            <div class="sub_text">Easily create solutions and experiences that meet your needs and receive relevant content on a daily basis.</div>
                            <a href="login" id="join2" class="sub_button" style="padding:15px;">	JOIN TODAY	</a>
                        </div>
                    </div>
                </div>





                <div class="" style="text-align:center; padding-bottom:60px;">
                    <div onclick="location.href='enrich';" class="portfolio aos-init aos-animate" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                        SEE WAYS TO ENRICH YOUR LIFE
                    </div>
                </div>







            </div>
        </div>
    </div>
    <!---
    <div class="signup">
       <div class="parallax-container">
           <div class="parallax"><img src="assets/images/slide_4.jpg"></div>
       </div>
       <div class="singup_overlay">
         <div class="title">SIGN UP</div>
       </div>
       <div class="signup_content">




          <div class="buttoner center" id="sign">SIGN UP</div>
          <div style="position:absolute; bottom:180px; left:0px; width:70%; height:1px; background:white; opacity:0.15; max-width:600px;"></div>
       </div>
    </div>

    --->

    <div class="footer">
        <div id="header_3" class="animated header" style="margin-top:5px;">

            <div class="text_container" style="margin-top:25px;">
                <div class="title" style="margin-top:25px; padding-top:50px;">It begins with you</div>

                <div class="ended_1" style="margin-top:15px;">Find the solution that is right for you.</div>
                <div class="line_container"> <img src="assets/images/line.png" alt="line"  style="margin-top:25px;"> </div>

            </div>
            <div class="row" style="margin-top:50px;">
                <div class="col-md-12">
                    <div style="text-align: center;" class="philosophy_2 aos-init aos-animate" data-aos="fade-up" data-aos-duration="700" data-aos-once="true">
                        <div class="titler"> Your Grey Shirt </div>

                        <a href="login" id="join3" class="buttoner" style="min-width: 220px; margin:25px auto;">Choose to Begin</a>
                        <div class="texter" style="margin:40px;">"Things which matter most must never be at the mercy of things which matter least."<br><br>- Johann Wolfgang Von Goethe</div>

                    </div>
                </div>
            </div>


        </div>
        <div class="footer_end">
            <div class="row footer_container">
                <div class="col-md-4">
                    <div class="forma_header" style="margin-top: 15px;font-size: 26px;">
                        <img src="assets/images/logo2.png" width="40px">
                        <font style="color:grey;">Walden.ly</font> <font style="font-size:18px;color:grey">[</font><font style="font-size:18px;">Make time for what matters.</font><font style="font-size:18px;color:grey">]</font>
                        <br>

                    </div>

                </div>
                <div class="col-md-4" style="text-align: center">
                    <div class="col-md-2 menuer" style="text-align: center"><span class="text"><a href="http://thegreyshirt.com/view.php?id=76771" style="color:white">About</a>  </span></div>
                    <div class="col-md-2 menuer" style="text-align: center"><span class="text"><a href="http://thegreyshirt.com/view.php?id=76770" style="color:white">Contact</a></span></div>
                    <div class="col-md-2 menuer" style="text-align: center"><span class="text"><a href="http://thegreyshirt.com/view.php?id=76772" style="color:white">Privacy</a></span></div>
                    <div class="col-md-2 menuer" style="text-align: center"><span class="text"><a href="http://thegreyshirt.com/view.php?id=76773" style="color:white">Terms</a></span></div>
                    <div class="col-md-2 menuer" style="text-align: center"><span class="text"><a href="http://thegreyshirt.com/view.php?id=76774" style="color:white">Jobs</a></span></div>

                </div>

                <div class="col-md-4" style="text-align: right">
                    <div class="" style="padding-top:25px; font-size: 14px;">
                        Copyright 2017.  All rights reserved.


                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<!--Popup!-->
<div id="p1" class="popup hided">
    <div id="p1_b" class="popup_body hided_b">
        <div class="row top_header">
            <div id="login" class="col-xs-6 col-sm-6 col-md-6 popup_menu actived_menu" style="border-right:none; border-left:none;">
                Log in
            </div>
            <div id="register" class="col-xs-6 col-sm-6 col-md-6 popup_menu" style="border-right:none;">
                Register
            </div>
        </div>
        <div  id="popup_content" class="popup_content">

            <form action="login.php" method="post">
                <input type="email" name="" value="" class="p_input" placeholder="Username">
                <input type="password" name="" value="" class="p_input" placeholder="Password">
                <div class="p_button2" onclick="javascript:this.parentNode.submit();" style="margin-top:35px;" >Login</div>
            </form>

            <div id="p_forgoter" class="p_forgoter">
                Forgot password ?
            </div>


        </div>

    </div>
</div>

<div id="p2" class="popup hided">
    <div id="p2_b" class="popup_body hided_b" style="height:235px; text-align:center;">
        <div class="popup_menu actived_menu" style="border:none;cursor:default;"> Forgot your password ?</div>
        <div id="error_m_2" class="error_msg" style="width:100%; top:60px;"></div>
        <form action="forgot_pwd.php" method="post">
            <input id="fpwd_email" type="email" name="" value="" class="p_input" placeholder="Email" style="width:240px;">
            <div id="fpwd_button" class="p_button"  onclick="javascript:this.parentNode.submit();" style="margin-top:15px;">Send</div>
        </form>
    </div>
</div>

<!--Popup!-->
<div id="p1" class="popup hided">
    <div id="p1_b" class="popup_body popup_new hided_b">
        <div class="popup_left">
            <div class="popup_title">
                Create new
            </div>
        </div>
        <div class="popup_right">
            <div class="row" style="padding:35px;">
                <div class="col-sm-12">
                    <div class="popup_head">Basic info </div>
                </div>
                <div class="col-xs-6 col-sm-6"><input type="email" name="" value="" class="p_input" placeholder="Name"></div>
                <div class="col-xs-6 col-sm-6"><input type="email" name="" value="" class="p_input" placeholder="Email"></div>
                <div class="col-xs-6 col-sm-6"><input type="email" name="" value="" class="p_input" placeholder="Password"></div>
                <div class="col-xs-6 col-sm-6"><input type="email" name="" value="" class="p_input" placeholder="Repear password"></div>
                <div class="col-sm-12">
                    <div class="popup_head" style="margin-top:45px;">Receive notifications for </div>
                </div>
                <div class="col-xs-12 col-sm-6 checkbox_element">
                    <div class="check_box_description">
                        Product updates
                    </div>
                    <div class="checkbox">
                        <i class="fa fa-times checkbox_item check_false" aria-hidden="true"></i>
                        <i class="fa fa-check checkbox_item check_true" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 checkbox_element">
                    <div class="check_box_description">
                        Special offets
                    </div>
                    <div class="checkbox">
                        <i class="fa fa-times checkbox_item check_false" aria-hidden="true"></i>
                        <i class="fa fa-check checkbox_item check_true" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 checkbox_element">
                    <div class="check_box_description">
                        New messages
                    </div>
                    <div class="checkbox checked">
                        <i class="fa fa-times checkbox_item check_false" aria-hidden="true"></i>
                        <i class="fa fa-check checkbox_item check_true" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 checkbox_element">
                    <div class="check_box_description">
                        Job posting
                    </div>
                    <div class="checkbox">
                        <i class="fa fa-times checkbox_item check_false" aria-hidden="true"></i>
                        <i class="fa fa-check checkbox_item check_true" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <div class="button_place">
                        <div class="p_button" style="margin-top:10px; max-width: 180px; display: inline-block;">CREATE NEW</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!--Script contection!-->
<script src="assets/js/yevgeny/index-global.js?version=<?php echo time();?>"></script>
</body>

</html>
