<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 14.10.2018
 * Time: 12:55
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Today's experiences - Make time for what matters.</title>
    <meta name="description" content="Make time for your interests and what matters, on a daily basis. Today's experiences organize your interests so you can quickly enjoy the things that matter.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my_solutions-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostNotes/PostNotes.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PurchasedSubscriptionsTree/PurchasedSubscriptionsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time();?>" />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormField/FormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/ListingSubscription/ListingSubscription.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.css?version=<?php echo time();?>" />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my-solutions-page.css?version=<?php echo time();?>">

</head>

<body class="">
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <div class="filter-wrapper" <?php echo count($series) ? '' : 'hidden';?>>
            <div class="filter-content">
                <div class="flex-row vertical-center space-between margin-between">
                    <div class="flex-col">
                        <div class="my-solutions">
                            <h3 class="filter-name">Today's experiences</h3>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="categories-filter-wrapper">
                            <div class="flex-row vertical-center margin-between d-xs-block">
                                <div class="flex-col d-xs-none">
                                    <span class="category-txt">Category</span>
                                </div>
                                <div class="flex-col">
                                    <div class="dropdown" data-toggle="popover" data-content="Show today's experiences by category">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                            <span>All</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li hidden><a class="filter-category active" href="javascript:;" data-category="-1">All</a></li>
                                            <?php foreach ($categories as $category): ?>
                                                <li>
                                                    <a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col ml-2 d-xs-none">
                        <div class="sort-by">
                            <div class="flex-row vertical-center margin-between">
                                <div class="flex-col">
                                    <h4 class="filter-name">Filter by</h4>
                                </div>
                                <div class="flex-col">
                                    <div class="dropdown" data-toggle="popover" data-content="Sort today's experiences by audio, video, or text">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                            <span>All</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li hidden><a href="javascript:;" data-value="-1">All</a></li>
                                            <li><a href="javascript:;" data-value="0">Audio</a></li>
                                            <li><a href="javascript:;" data-value="2">Video</a></li>
                                            <li><a href="javascript:;" data-value="7">Text</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <header class="getting-started-header" <?php echo count($series) ? 'hidden' : '';?>>
            <div class="content-header-content">
                <div class="flex-row align-items-end space-between">
                    <div class="flex-col fix-col">
                        <div class="title-wrapper">
                            <h2 class="page-title">Getting Started</h2>
                            <div class="step-wrapper">
                                step <span class="step-value">1</span> of 3
                            </div>
                        </div>
                    </div>
                    <div class="flex-col d-xs-none min-width-0">
                        <div class="step-progress-wrapper step-progress-1">
                            <div class="progress-wrapper">
                                <div class="progress-item step-1 active right-dotted">
                                    <div class="step-wrapper">1.Select between Creating content or Subscribing to Content</div>
                                </div>
                                <div class="progress-item step-2 right-dotted">
                                    <div class="step-wrapper">2.Select Categories that interest you</div>
                                </div>
                                <div class="progress-item step-3">
                                    <div class="step-wrapper">3.Select Content Channels</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <aside class="light-box flex-centering align-items-end">
                        </aside>
                    </div>
                </div>
            </div>
        </header>
        <div class="experience-goal-switcher-wrapper" <?php echo count($series) ? '' : 'hidden';?>>
            <div class="d-none d-xs-block">
                <div class="experience-goal-switcher">
                    <div class="flex-row vertical-center">
                        <div class="flex-col fb-0">
                            <div class="experience-switch switch-item active-item">Experiences</div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="goal-switch switch-item">Goals</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade <?php echo count($series) ? 'in active' : '';?>" id="page-main-tab">
                <main class="main-content experiences-active">
                    <div class="flex-row d-xs-block">
                        <div class="flex-col main-col">
                            <section class="articles-container">
                                <article class="sample blog-article" hidden>
                                    <header class="article-header">
                                        <div class="flex-row vertical-center margin-between d-xs-block">
                                            <div class="flex-col fix-col actions-col float-xs-right">
                                                <div class="actions-wrapper">
                                                    <div class="flex-row vertical-center margin-between">
                                                        <div class="flex-col fix-col" hidden>
                                                            <a href="javascript:;" class="action-item complete-action" data-toggle="popover" data-content="Mark as complete when you finish to record your progress." data-placement="bottom">
                                                                <svg version="1.1" id="Layer_1"
                                                                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                                                                     viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title></title>
                                                                    <desc>Created with Sketch.</desc>
                                                                    <g id="Page-1" sketch:type="MSPage">
                                                                        <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                                            <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                                                        </g>
                                                                    </g>
                                                        </svg>
                                                            </a>
                                                        </div>
                                                        <div class="flex-col fix-col" hidden>
                                                            <a href="javascript:;" class="action-item favorite-action" data-toggle="popover" data-content="Add to favorites" data-placement="bottom">
                                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                                            <polygon points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                                                81.485,416.226 106.667,269.41 0,165.436 147.409,144.017"/>
                                                        </svg>
                                                            </a>
                                                        </div>
                                                        <div class="flex-col fix-col" hidden>
                                                            <a href="javascript:;" class="action-item share-post" data-toggle="popover" data-content="Share this experience on your favorite social network." data-placement="bottom">
                                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     viewBox="0 0 525.152 525.152" style="enable-background:new 0 0 525.152 525.152;" xml:space="preserve">
                                                             <g>
                                                                 <path d="M420.735,371.217c-20.021,0-37.942,7.855-51.596,20.24L181.112,282.094c1.357-6.061,2.407-12.166,2.407-18.468
                                                                    c0-6.302-1.072-12.385-2.407-18.468l185.904-108.335c14.179,13.129,32.931,21.334,53.719,21.334
                                                                    c43.828,0,79.145-35.251,79.145-79.079C499.88,35.338,464.541,0,420.735,0c-43.741,0-79.079,35.338-79.079,79.057
                                                                    c0,6.389,1.072,12.385,2.407,18.468L158.158,205.947c-14.201-13.194-32.931-21.378-53.741-21.378
                                                                    c-43.828,0-79.145,35.317-79.145,79.057s35.317,79.079,79.145,79.079c20.787,0,39.54-8.206,53.719-21.334l187.698,109.604
                                                                    c-1.291,5.58-2.101,11.4-2.101,17.199c0,42.45,34.594,76.979,76.979,76.979c42.428,0,77.044-34.507,77.044-76.979
                                                                    S463.163,371.217,420.735,371.217z"/>
                                                             </g>
                                                        </svg>
                                                            </a>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <button class="site-green-btn action-btn favorite-btn">
                                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                                                                <polygon points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                                                                    81.485,416.226 106.667,269.41 0,165.436 147.409,144.017"></polygon>
                                                                            </svg>
                                                                <span class="favorite-text">Favorite</span>
                                                                <span class="un-favorite-text">Favorite</span>
                                                            </button>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <button class="site-green-btn share-btn">Share</button>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="dropdown" data-toggle="popover" data-content="More options" data-placement="top">
                                                                <a href="javascript:;" class="action-item more-action dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
                                                                <g>
                                                                    <path d="M30,16c4.411,0,8-3.589,8-8s-3.589-8-8-8s-8,3.589-8,8S25.589,16,30,16z"/>
                                                                    <path d="M30,44c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,44,30,44z"/>
                                                                    <path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z"/>
                                                                </g>
                                                                </svg>
                                                                </a>
                                                                <ul class="dropdown-menu more-drop-down">
                                                                    <li hidden>
                                                                        <a href="javascript:;" class="action-item embed-action" target="_blank">
                                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                 width="522.468px" height="522.469px" viewBox="0 0 522.468 522.469" style="enable-background:new 0 0 522.468 522.469;"
                                                                                 xml:space="preserve">
<g>
    <g>
        <path d="M325.762,70.513l-17.706-4.854c-2.279-0.76-4.524-0.521-6.707,0.715c-2.19,1.237-3.669,3.094-4.429,5.568L190.426,440.53
			c-0.76,2.475-0.522,4.809,0.715,6.995c1.237,2.19,3.09,3.665,5.568,4.425l17.701,4.856c2.284,0.766,4.521,0.526,6.71-0.712
			c2.19-1.243,3.666-3.094,4.425-5.564L332.042,81.936c0.759-2.474,0.523-4.808-0.716-6.999
			C330.088,72.747,328.237,71.272,325.762,70.513z"/>
        <path d="M166.167,142.465c0-2.474-0.953-4.665-2.856-6.567l-14.277-14.276c-1.903-1.903-4.093-2.857-6.567-2.857
			s-4.665,0.955-6.567,2.857L2.856,254.666C0.95,256.569,0,258.759,0,261.233c0,2.474,0.953,4.664,2.856,6.566l133.043,133.044
			c1.902,1.906,4.089,2.854,6.567,2.854s4.665-0.951,6.567-2.854l14.277-14.268c1.903-1.902,2.856-4.093,2.856-6.57
			c0-2.471-0.953-4.661-2.856-6.563L51.107,261.233l112.204-112.201C165.217,147.13,166.167,144.939,166.167,142.465z"/>
        <path d="M519.614,254.663L386.567,121.619c-1.902-1.902-4.093-2.857-6.563-2.857c-2.478,0-4.661,0.955-6.57,2.857l-14.271,14.275
			c-1.902,1.903-2.851,4.09-2.851,6.567s0.948,4.665,2.851,6.567l112.206,112.204L359.163,373.442
			c-1.902,1.902-2.851,4.093-2.851,6.563c0,2.478,0.948,4.668,2.851,6.57l14.271,14.268c1.909,1.906,4.093,2.854,6.57,2.854
			c2.471,0,4.661-0.951,6.563-2.854L519.614,267.8c1.903-1.902,2.854-4.096,2.854-6.57
			C522.468,258.755,521.517,256.565,519.614,254.663z"/>
    </g>
</g>
</svg>
                                                                        </a>
                                                                    </li>
                                                                    <li hidden>
                                                                        <a href="javascript:;" class="action-item add-action" target="_blank">
                                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                 width="73.17px" height="73.17px" viewBox="0 0 73.17 73.17" style="enable-background:new 0 0 73.17 73.17;" xml:space="preserve">
                                                                                <g>
                                                                                    <g id="Plus">
                                                                                        <g>
                                                                                            <path d="M68.928,32.343H40.827V4.243C40.827,1.903,38.928,0,36.585,0s-4.242,1.903-4.242,4.242v28.101H4.242
                                                                                                C1.9,32.343,0,34.246,0,36.585c0,2.347,1.9,4.242,4.242,4.242h28.101v28.1c0,2.349,1.899,4.242,4.242,4.242
                                                                                                s4.242-1.896,4.242-4.242v-28.1h28.101c2.344,0,4.242-1.896,4.242-4.242C73.17,34.246,71.271,32.343,68.928,32.343z"/>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </svg>
                                                                        </a>
                                                                    </li>
                                                                    <li hidden>
                                                                        <a href="javascript:;" target="_blank" class="action-item edit-action">
                                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                 width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;"
                                                                                 xml:space="preserve">
<g>
    <g>
        <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"/>
        <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"/>
        <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
			l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"/>
    </g>
</g>
</svg>
                                                                        </a>
                                                                    </li>
                                                                    <li hidden>
                                                                        <a href="javascript:;" class="action-item trash-action">
                                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                 width="774.266px" height="774.266px" viewBox="0 0 774.266 774.266" style="enable-background:new 0 0 774.266 774.266;"
                                                                                 xml:space="preserve">
<g>
    <g>
        <path d="M640.35,91.169H536.971V23.991C536.971,10.469,526.064,0,512.543,0c-1.312,0-2.187,0.438-2.614,0.875
			C509.491,0.438,508.616,0,508.179,0H265.212h-1.74h-1.75c-13.521,0-23.99,10.469-23.99,23.991v67.179H133.916
			c-29.667,0-52.783,23.116-52.783,52.783v38.387v47.981h45.803v491.6c0,29.668,22.679,52.346,52.346,52.346h415.703
			c29.667,0,52.782-22.678,52.782-52.346v-491.6h45.366v-47.981v-38.387C693.133,114.286,670.008,91.169,640.35,91.169z
			 M285.713,47.981h202.84v43.188h-202.84V47.981z M599.349,721.922c0,3.061-1.312,4.363-4.364,4.363H179.282
			c-3.052,0-4.364-1.303-4.364-4.363V230.32h424.431V721.922z M644.715,182.339H129.551v-38.387c0-3.053,1.312-4.802,4.364-4.802
			H640.35c3.053,0,4.365,1.749,4.365,4.802V182.339z"/>
        <rect x="475.031" y="286.593" width="48.418" height="396.942"/>
        <rect x="363.361" y="286.593" width="48.418" height="396.942"/>
        <rect x="251.69" y="286.593" width="48.418" height="396.942"/>
    </g>
</g>
</svg>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <button class="site-green-btn action-btn finish-btn">
                                                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px" viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                                                <title></title>
                                                                                                        <desc>Created with Sketch.</desc>
                                                                                                        <g id="Page-1" sketch:type="MSPage">
                                                                                                            <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                                                                                <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                                            c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                                            c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"></path>
                                                                                                            </g>
                                                                                                        </g>
                                                                            </svg>
                                                                            <span class="complete-text">Finished</span>
                                                                            <span class="un-complete-text">Finish</span>
                                                                        </button>
                                                                    </li>
                                                                    <li>
                                                                        <button class="site-green-btn action-btn edit-btn">Edit</button>
                                                                    </li>
                                                                    <li>
                                                                        <button class="site-green-btn action-btn trash-btn">Trash</button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flex-col fix-col toggle-col d-xs-inline-block">
                                                <div class="listing-toggle flex-centering d-xs-inline-flex" data-toggle="popover" data-content="View all of the content in this experience." data-placement="bottom">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"/>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"/>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"/>
                                </g>
                                                        <g>
                                                            <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"/>
                                                            <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"/>
                                                            <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"/>
                                                        </g>
                            </svg>
                                                    <div class="unread-posts-count"></div>
                                                </div>
                                            </div>
                                            <div class="flex-col title-col d-xs-inline">
                                                <div class="header-text-wrapper d-xs-inline">
                                                    <aside class="media-icon-wrapper d-xs-none">
                                                        <i class="fa fa-youtube-play video-icon"></i>
                                                        <i class="fa fa-volume-up audio-icon"></i>
                                                        <i class="fa fa-file-text text-icon"></i>
                                                    </aside>
                                                    <a href="javascript:;" class="article-title" target="_blank" data-toggle="popover" data-content="View this experience." data-placement="bottom">Why gifted students need to be taught formal writing</a><span>&nbsp;-&nbsp;</span>
                                                    <span class="series-name">daily recipes</span><span> | day </span><span class="view-day">1</span>
                                                </div>
                                            </div>
                                        </div>
                                    </header>
                                    <div class="article-body">
                                        <div class="type-0 subscription-type sample" hidden>
                                            <div class="audiogallery">
                                                <div class="items">
                                                    <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="type-2 subscription-type sample" hidden>
                                            <div class="video-wrapper">
                                                <video class="video-js" controls width="960" height="540"></video>
                                            </div>
                                        </div>
                                        <div class="type-menu subscription-type sample" hidden>
                                            <?php require_once ASSETS_PATH . '/components/MenuOptionsBlog/MenuOptionsBlog.html';?>
                                        </div>
                                        <div class="type-8 subscription-type sample" hidden></div>
                                        <div class="type-10 subscription-type sample" hidden>
                                            <div class="item-description"></div>
                                        </div>
                                        <div class="type-other subscription-type sample" hidden>
                                            <div class="item-description"></div>
                                        </div>
                                        <aside class="blog-duration">
                                            waiting...
                                        </aside>
                                        <div class="slider-nav-wrapper left-wrapper">
                                            <div class="slider-nav flex-centering arrow-left">
                                                <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="slider-nav-wrapper right-wrapper">
                                            <div class="slider-nav flex-centering arrow-right">
                                                <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <aside class="note-wrapper" data-toggle="popover" data-content="Add private notes" data-placement="top">
                                        <?php require_once ASSETS_PATH . '/components/PostNotes/PostNotes.html';?>
                                    </aside>
                                    <aside class="item-meta">
                                        <nav class="circle-nav-wrapper">
                                            <div class="circle-nav-toggle">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                        <g>
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"/>
                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"/>
                        </g>
                    </svg>
                                            </div>
                                            <div class="circle-nav-panel"></div>
                                            <ul class="circle-nav-menu">
                                                <li class="circle-nav-item circle-nav-item-1">
                                                    <a class="facebook-link" href="https://www.facebook.com/sharer/sharer.php?u=test" target="_blank">
                                                        <svg version="1.1" id="facebook-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="30px" height="30px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;"
                                                             xml:space="preserve">
                                    <g><path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803
                                            c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654
                                            c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246
                                            c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"/>
                                    </g>
                                </svg><span>Facebook</span>
                                                    </a>
                                                </li>
                                                <li class="circle-nav-item circle-nav-item-2">
                                                    <a class="twitter-link" href="https://twitter.com/intent/tweet?url=text" target="_blank">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512.002px" height="512.002px" viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;"xml:space="preserve">
                                    <g>
                                        <path d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115
                                            c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039
                                            c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809
                                            c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321
                                            c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888
                                            c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471
                                            c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59
                                            C480.111,136.775,497.92,118.275,512.002,97.211z"/>
                                    </g>
                                </svg><span>Twitter</span>
                                                    </a>
                                                </li>
                                                <li class="circle-nav-item circle-nav-item-3">
                                                    <a class="linkedin-link" href="https://www.linkedin.com/shareArticle?mini=true&url=test" target="_blank">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="430.117px" height="430.117px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
                                                             xml:space="preserve">
                                    <g>
                                        <path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
                                        c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
                                        v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
                                        C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
                                        c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
                                         M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
                                    </g></svg><span>Linkedlin</span>
                                                    </a>
                                                </li>
                                                <li class="circle-nav-item circle-nav-item-4">
                                                    <a class="google-link" href="https://plus.google.com/share?url=test" target="_blank">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="96.828px" height="96.827px" viewBox="0 0 96.828 96.827" style="enable-background:new 0 0 96.828 96.827;"
                                                             xml:space="preserve">
                                        <g>
                                            <path d="M62.617,0H39.525c-10.29,0-17.413,2.256-23.824,7.552c-5.042,4.35-8.051,10.672-8.051,16.912
                                                c0,9.614,7.33,19.831,20.913,19.831c1.306,0,2.752-0.134,4.028-0.253l-0.188,0.457c-0.546,1.308-1.063,2.542-1.063,4.468
                                                c0,3.75,1.809,6.063,3.558,8.298l0.22,0.283l-0.391,0.027c-5.609,0.384-16.049,1.1-23.675,5.787
                                                c-9.007,5.355-9.707,13.145-9.707,15.404c0,8.988,8.376,18.06,27.09,18.06c21.76,0,33.146-12.005,33.146-23.863
                                                c0.002-8.771-5.141-13.101-10.6-17.698l-4.605-3.582c-1.423-1.179-3.195-2.646-3.195-5.364c0-2.672,1.772-4.436,3.336-5.992
                                                l0.163-0.165c4.973-3.917,10.609-8.358,10.609-17.964c0-9.658-6.035-14.649-8.937-17.048h7.663c0.094,0,0.188-0.026,0.266-0.077
                                                l6.601-4.15c0.188-0.119,0.276-0.348,0.214-0.562C63.037,0.147,62.839,0,62.617,0z M34.614,91.535
                                                c-13.264,0-22.176-6.195-22.176-15.416c0-6.021,3.645-10.396,10.824-12.997c5.749-1.935,13.17-2.031,13.244-2.031
                                                c1.257,0,1.889,0,2.893,0.126c9.281,6.605,13.743,10.073,13.743,16.678C53.141,86.309,46.041,91.535,34.614,91.535z
                                                 M34.489,40.756c-11.132,0-15.752-14.633-15.752-22.468c0-3.984,0.906-7.042,2.77-9.351c2.023-2.531,5.487-4.166,8.825-4.166
                                                c10.221,0,15.873,13.738,15.873,23.233c0,1.498,0,6.055-3.148,9.22C40.94,39.337,37.497,40.756,34.489,40.756z"/>
                                            <path d="M94.982,45.223H82.814V33.098c0-0.276-0.225-0.5-0.5-0.5H77.08c-0.276,0-0.5,0.224-0.5,0.5v12.125H64.473
                                                c-0.276,0-0.5,0.224-0.5,0.5v5.304c0,0.275,0.224,0.5,0.5,0.5H76.58V63.73c0,0.275,0.224,0.5,0.5,0.5h5.234
                                                c0.275,0,0.5-0.225,0.5-0.5V51.525h12.168c0.276,0,0.5-0.223,0.5-0.5v-5.302C95.482,45.446,95.259,45.223,94.982,45.223z"/>
                                        </g></svg><span>Google</span>
                                                    </a>
                                                </li>
                                                <li class="circle-nav-item circle-nav-item-5">
                                                    <a class="mail-link" href="mailto:nbaca@mtnlegal.com?&subject=welcome">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" style="enable-background:new 0 0 79.536 79.536;"
                                                             xml:space="preserve">
                                    <g>
                                        <path style="fill:#010002;" d="M39.773,1.31L0,31.004v47.222h79.536V31.004L39.773,1.31z M28.77,22.499
                                        c1.167-2.133,2.775-3.739,4.815-4.805c2.035-1.075,4.357-1.616,6.983-1.616c2.214,0,4.191,0.435,5.921,1.292
                                        c1.729,0.87,3.045,2.094,3.967,3.687c0.9,1.595,1.367,3.334,1.367,5.217c0,2.247-0.694,4.279-2.082,6.097
                                        c-1.74,2.292-3.961,3.436-6.68,3.436c-0.732,0-1.279-0.122-1.654-0.38c-0.365-0.262-0.621-0.632-0.743-1.129
                                        c-1.022,1.012-2.231,1.52-3.589,1.52c-1.465,0-2.679-0.507-3.643-1.509c-0.966-1.012-1.447-2.361-1.447-4.031
                                        c0-2.084,0.578-3.966,1.743-5.672c1.416-2.084,3.218-3.13,5.424-3.13c1.571,0,2.731,0.601,3.475,1.805l0.331-1.468h3.5
                                        l-1.998,9.479c-0.125,0.606-0.187,0.986-0.187,1.163c0,0.228,0.052,0.38,0.149,0.497c0.099,0.111,0.223,0.165,0.357,0.165
                                        c0.436,0,0.979-0.248,1.646-0.769c0.901-0.663,1.627-1.574,2.181-2.695c0.554-1.129,0.839-2.299,0.839-3.508
                                        c0-2.165-0.782-3.977-2.352-5.445c-1.573-1.45-3.77-2.185-6.578-2.185c-2.393,0-4.417,0.487-6.077,1.468
                                        c-1.66,0.966-2.913,2.343-3.765,4.114c-0.839,1.76-1.258,3.607-1.258,5.52c0,1.856,0.479,3.552,1.411,5.074
                                        c0.945,1.533,2.26,2.641,3.956,3.345c1.696,0.697,3.643,1.046,5.828,1.046c2.097,0,3.909-0.293,5.432-0.881
                                        c1.522-0.587,2.739-1.457,3.666-2.641h2.807c-0.88,1.792-2.227,3.192-4.049,4.215c-2.092,1.163-4.64,1.74-7.644,1.74
                                        c-2.918,0-5.426-0.487-7.542-1.468c-2.121-0.986-3.689-2.434-4.73-4.35c-1.028-1.918-1.535-4.008-1.535-6.268
                                        C27.017,26.952,27.595,24.64,28.77,22.499z M2.804,31.941l29.344,19.68L2.804,74.333V31.941z M5.033,75.844l34.74-26.885
                                        l34.729,26.885H5.033z M76.729,74.333L47.391,51.621l29.339-19.68V74.333z M41.205,24.661c0.466,0.531,0.699,1.295,0.699,2.292
                                        c0,0.891-0.174,1.856-0.513,2.879c-0.334,1.036-0.743,1.826-1.209,2.361c-0.318,0.375-0.658,0.652-0.992,0.826
                                        c-0.439,0.249-0.906,0.37-1.41,0.37c-0.674,0.006-1.23-0.264-1.691-0.794c-0.45-0.531-0.673-1.346-0.673-2.465
                                        c0-0.839,0.158-1.805,0.487-2.889c0.329-1.088,0.81-1.916,1.453-2.509c0.647-0.588,1.346-0.881,2.1-0.881
                                        C40.162,23.856,40.749,24.125,41.205,24.661z"/>
                                    </g></svg><span>Opened-Email-Envelope</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </aside>
                                </article>
                            </section>
                        </div>
                        <div class="flex-col goals-col">
                            <section class="goals-section">
                                <div class="my-goals">
                                    <div class="flex-row margin-between space-between  align-items-end">
                                        <div class="flex-col fix-col">
                                            <div class="i-will-do">
                                                <h3 class="filter-name">Today, I can...</h3>
                                                <div>
                                                    <a href="javascript:;" class="goal-date active" data-when="<?php echo date('Y-m-d');?>">today</a>
                                                    <a href="javascript:;" class="goal-date" data-when="<?php echo date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));?>">tomorrow</a>
                                                    <a href="javascript:;" class="goal-date" data-when="calendar">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             width="36.447px" height="36.447px" viewBox="0 0 36.447 36.447" style="enable-background:new 0 0 36.447 36.447;"
                                                             xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M30.224,3.948h-1.098V2.75c0-1.517-1.197-2.75-2.67-2.75c-1.474,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75
                                                                c0-1.517-1.197-2.75-2.67-2.75c-1.473,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75c0-1.517-1.197-2.75-2.67-2.75
                                                                c-1.473,0-2.67,1.233-2.67,2.75v1.197H6.224c-2.343,0-4.25,1.907-4.25,4.25v24c0,2.343,1.907,4.25,4.25,4.25h24
                                                                c2.344,0,4.25-1.907,4.25-4.25v-24C34.474,5.855,32.567,3.948,30.224,3.948z M25.286,2.75c0-0.689,0.525-1.25,1.17-1.25
                                                                c0.646,0,1.17,0.561,1.17,1.25v4.896c0,0.689-0.524,1.25-1.17,1.25c-0.645,0-1.17-0.561-1.17-1.25V2.75z M17.206,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z M9.125,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z
                                                                 M31.974,32.198c0,0.965-0.785,1.75-1.75,1.75h-24c-0.965,0-1.75-0.785-1.75-1.75v-22h27.5V32.198z"/>
                                                            <rect x="6.724" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="12.857" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="18.995" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="25.128" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="6.724" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="6.724" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="25.54" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="25.54" width="4.596" height="4.086"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                                        <input type="text" class="active-date" data-date-format="yyyy-mm-dd"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div data-toggle="popover" data-content="Add a clear goal that you can accomplish today" data-placement="bottom">
                                                <a href="#add-goal-modal" class="add-goal" data-toggle="modal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                </g>
                                            </g>
                                        </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="goal-input-wrapper multi-line-background" >
                                    <textarea class="goal-input" data-type="normal" rows="4" required placeholder="Set clear goals to accomplish today."></textarea>
                                    <div class="background-placeholder"></div>
                                </div>
                                <section class="goals-container">
                                    <div class="goal-item sample" hidden>
                                        <div class="item-date"></div>
                                        <div class="goal-text">Lorem lpsum is simply dummy text of printing and typesetting industry. Lorem Ipsum has been instry standard dummy</div>
                                        <footer class="goal-actions">
                                            <div class="flex-row space-between vertical-center">
                                                <div class="flex-col fix-col">
                                                    <div class="main-actions">
                                                        <div class="flex-row margin-between vertical-center">
                                                            <div class="flex-col">
                                                                <a href="javascript:;" class="action-item complete-action">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <path d="M437.019,74.98C388.667,26.629,324.38,0,256,0C187.619,0,123.331,26.629,74.98,74.98C26.628,123.332,0,187.62,0,256
                                                                            s26.628,132.667,74.98,181.019C123.332,485.371,187.619,512,256,512c68.38,0,132.667-26.629,181.019-74.981
                                                                            C485.371,388.667,512,324.38,512,256S485.371,123.333,437.019,74.98z M256,482C131.383,482,30,380.617,30,256S131.383,30,256,30
                                                                            s226,101.383,226,226S380.617,482,256,482z"/>
                                                                    </g>
                                                                </g>
                                                                        <g>
                                                                            <g>
                                                                                <path d="M378.305,173.859c-5.857-5.856-15.355-5.856-21.212,0.001L224.634,306.319l-69.727-69.727
                                                                            c-5.857-5.857-15.355-5.857-21.213,0c-5.858,5.857-5.858,15.355,0,21.213l80.333,80.333c2.929,2.929,6.768,4.393,10.606,4.393
                                                                            c3.838,0,7.678-1.465,10.606-4.393l143.066-143.066C384.163,189.215,384.163,179.717,378.305,173.859z"/>
                                                                            </g>
                                                                        </g>
                                                            </svg>
                                                                </a>
                                                            </div>
                                                            <div class="flex-col">
                                                                <a href="javascript:;" class="action-item delete-action">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                </a>
                                                            </div>
                                                            <div class="flex-col">
                                                                <a href="javascript:;" class="action-item item-goal-date" data-when="calendar">
                                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                         width="36.447px" height="36.447px" viewBox="0 0 36.447 36.447" style="enable-background:new 0 0 36.447 36.447;"
                                                                         xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M30.224,3.948h-1.098V2.75c0-1.517-1.197-2.75-2.67-2.75c-1.474,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75
                                                                c0-1.517-1.197-2.75-2.67-2.75c-1.473,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75c0-1.517-1.197-2.75-2.67-2.75
                                                                c-1.473,0-2.67,1.233-2.67,2.75v1.197H6.224c-2.343,0-4.25,1.907-4.25,4.25v24c0,2.343,1.907,4.25,4.25,4.25h24
                                                                c2.344,0,4.25-1.907,4.25-4.25v-24C34.474,5.855,32.567,3.948,30.224,3.948z M25.286,2.75c0-0.689,0.525-1.25,1.17-1.25
                                                                c0.646,0,1.17,0.561,1.17,1.25v4.896c0,0.689-0.524,1.25-1.17,1.25c-0.645,0-1.17-0.561-1.17-1.25V2.75z M17.206,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z M9.125,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z
                                                                 M31.974,32.198c0,0.965-0.785,1.75-1.75,1.75h-24c-0.965,0-1.75-0.785-1.75-1.75v-22h27.5V32.198z"/>
                                                            <rect x="6.724" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="12.857" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="18.995" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="25.128" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="6.724" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="6.724" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="25.54" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="25.54" width="4.596" height="4.086"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                                                    <input type="text" class="active-date" data-date-format="yyyy-mm-dd"/>
                                                                </a>
                                                            </div>
                                                            <div class="flex-col">
                                                                <a href="javascript:;" class="action-item item-for-tomorrow" data-when="tomorrow">
                                                                    tomorrow
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="drag-icon-wrapper">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 180 180" style="enable-background:new 0 0 180 180;" xml:space="preserve">
                                                    <path d="M38.937,128.936L0,89.999l38.935-38.936l0.001,29.982h42.11V38.937l-29.982,0L90.001,0l38.935,38.936l-29.982,0v42.109
                                                        h42.109l-0.001-29.982L180,90.001l-38.936,38.935l-0.001-29.982h-42.11v42.109l29.982,0L89.999,180l-38.936-38.936l29.982,0V98.954
                                                        H38.937L38.937,128.936z"/>
                                                </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                    <div class="goal--list">
                                    </div>
                                </section>
                                <section class="morning-section">
                                    <h2 class="section-title">Morning pages</h2>
                                    <div class="goal-input-wrapper multi-line-background">
                                        <textarea class="goal-input" rows="4" required></textarea>
                                        <div class="background-placeholder">
                                            The Morning page is a stream of consciousness writing opportunity to awaken your creative mind.
                                        </div>
                                    </div>
                                    <div class="flex-row space-between vertical-center">
                                        <div class="flex-col fix-col">
                                            <div class="need-help" data-toggle="popover" data-content="Trouble getting started?  Click here for a writing prompt to help you get started." data-placement="bottom">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 486 486" style="enable-background:new 0 0 486 486;" xml:space="preserve">
                                    <g>
                                        <path id="XMLID_49_" d="M298.4,424.7v14.2c0,11.3-8.3,20.7-19.1,22.3l-3.5,12.9c-1.9,7-8.2,11.9-15.5,11.9h-34.7
                                            c-7.3,0-13.6-4.9-15.5-11.9l-3.4-12.9c-10.9-1.7-19.2-11-19.2-22.4v-14.2c0-7.6,6.1-13.7,13.7-13.7h83.5
                                            C292.3,411,298.4,417.1,298.4,424.7z M362.7,233.3c0,32.3-12.8,61.6-33.6,83.1c-15.8,16.4-26,37.3-29.4,59.6
                                            c-1.5,9.6-9.8,16.7-19.6,16.7h-74.3c-9.7,0-18.1-7-19.5-16.6c-3.5-22.3-13.8-43.5-29.6-59.8c-20.4-21.2-33.1-50-33.4-81.7
                                            c-0.7-66.6,52.3-120.5,118.9-121C308.7,113.1,362.7,166.9,362.7,233.3z M256.5,160.8c0-7.4-6-13.5-13.5-13.5
                                            c-47.6,0-86.4,38.7-86.4,86.4c0,7.4,6,13.5,13.5,13.5c7.4,0,13.5-6,13.5-13.5c0-32.8,26.7-59.4,59.4-59.4
                                            C250.5,174.3,256.5,168.3,256.5,160.8z M243,74.3c7.4,0,13.5-6,13.5-13.5V13.5c0-7.4-6-13.5-13.5-13.5s-13.5,6-13.5,13.5v47.3
                                            C229.5,68.3,235.6,74.3,243,74.3z M84.1,233.2c0-7.4-6-13.5-13.5-13.5H23.3c-7.4,0-13.5,6-13.5,13.5c0,7.4,6,13.5,13.5,13.5h47.3
                                            C78.1,246.7,84.1,240.7,84.1,233.2z M462.7,219.7h-47.3c-7.4,0-13.5,6-13.5,13.5c0,7.4,6,13.5,13.5,13.5h47.3
                                            c7.4,0,13.5-6,13.5-13.5C476.2,225.8,470.2,219.7,462.7,219.7z M111.6,345.6l-33.5,33.5c-5.3,5.3-5.3,13.8,0,19.1
                                            c2.6,2.6,6.1,3.9,9.5,3.9s6.9-1.3,9.5-3.9l33.5-33.5c5.3-5.3,5.3-13.8,0-19.1C125.4,340.3,116.8,340.3,111.6,345.6z M364.9,124.8
                                            c3.4,0,6.9-1.3,9.5-3.9l33.5-33.5c5.3-5.3,5.3-13.8,0-19.1c-5.3-5.3-13.8-5.3-19.1,0l-33.5,33.5c-5.3,5.3-5.3,13.8,0,19.1
                                            C358,123.5,361.4,124.8,364.9,124.8z M111.6,120.8c2.6,2.6,6.1,3.9,9.5,3.9s6.9-1.3,9.5-3.9c5.3-5.3,5.3-13.8,0-19.1L97.1,68.2
                                            c-5.3-5.3-13.8-5.3-19.1,0c-5.3,5.3-5.3,13.8,0,19.1L111.6,120.8z M374.4,345.6c-5.3-5.3-13.8-5.3-19.1,0c-5.3,5.3-5.3,13.8,0,19.1
                                            l33.5,33.5c2.6,2.6,6.1,3.9,9.5,3.9s6.9-1.3,9.5-3.9c5.3-5.3,5.3-13.8,0-19.1L374.4,345.6z"/>
                                    </g>
                                </svg>
                                                <span>Need help?</span>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a href="javascript:;" class="add-goal">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                </g>
                                            </g>
                                        </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mor-even-container">
                                        <div class="mor-even-item sample" hidden>
                                            <div class="goal-text">Lorem lpsum is simply dummy text of printing and typesetting industry. Lorem Ipsum has been instry standard dummy</div>
                                            <footer class="goal-actions">
                                                <div class="flex-row space-between vertical-center">
                                                    <div class="flex-col fix-col">
                                                        <div class="main-actions">
                                                            <div class="flex-row margin-between">
                                                                <div class="flex-col">
                                                                    <a href="javascript:;" class="action-item complete-action">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <path d="M437.019,74.98C388.667,26.629,324.38,0,256,0C187.619,0,123.331,26.629,74.98,74.98C26.628,123.332,0,187.62,0,256
                                                                            s26.628,132.667,74.98,181.019C123.332,485.371,187.619,512,256,512c68.38,0,132.667-26.629,181.019-74.981
                                                                            C485.371,388.667,512,324.38,512,256S485.371,123.333,437.019,74.98z M256,482C131.383,482,30,380.617,30,256S131.383,30,256,30
                                                                            s226,101.383,226,226S380.617,482,256,482z"/>
                                                                    </g>
                                                                </g>
                                                                            <g>
                                                                                <g>
                                                                                    <path d="M378.305,173.859c-5.857-5.856-15.355-5.856-21.212,0.001L224.634,306.319l-69.727-69.727
                                                                            c-5.857-5.857-15.355-5.857-21.213,0c-5.858,5.857-5.858,15.355,0,21.213l80.333,80.333c2.929,2.929,6.768,4.393,10.606,4.393
                                                                            c3.838,0,7.678-1.465,10.606-4.393l143.066-143.066C384.163,189.215,384.163,179.717,378.305,173.859z"/>
                                                                                </g>
                                                                            </g>
                                                            </svg>
                                                                    </a>
                                                                </div>
                                                                <div class="flex-col">
                                                                    <a href="javascript:;" class="action-item delete-action">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <a href="javascript:;" class="more-action">
                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <circle cx="69.545" cy="306" r="69.545"/>
                                                        <circle cx="306" cy="306" r="69.545"/>
                                                        <circle cx="542.455" cy="306" r="69.545"/>
                                                    </g>
                                                </g>
                                                </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </section>
                                <section class="evening-section">
                                    <h2 class="section-title">Evening reflection</h2>
                                    <div class="goal-input-wrapper multi-line-background" >
                                        <textarea class="goal-input" rows="4" required></textarea>
                                        <div class="background-placeholder">
                                            The evening reflection is a free writing opportunity to bring closure and completion to your day.
                                        </div>
                                    </div>
                                    <div class="flex-row space-between vertical-center">
                                        <div class="flex-col fix-col">
                                            <div class="need-help" data-toggle="popover" data-content="Trouble getting started?  Click here for a writing prompt to help you get started." data-placement="bottom">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 486 486" style="enable-background:new 0 0 486 486;" xml:space="preserve">
                                    <g>
                                        <path id="XMLID_49_" d="M298.4,424.7v14.2c0,11.3-8.3,20.7-19.1,22.3l-3.5,12.9c-1.9,7-8.2,11.9-15.5,11.9h-34.7
                                            c-7.3,0-13.6-4.9-15.5-11.9l-3.4-12.9c-10.9-1.7-19.2-11-19.2-22.4v-14.2c0-7.6,6.1-13.7,13.7-13.7h83.5
                                            C292.3,411,298.4,417.1,298.4,424.7z M362.7,233.3c0,32.3-12.8,61.6-33.6,83.1c-15.8,16.4-26,37.3-29.4,59.6
                                            c-1.5,9.6-9.8,16.7-19.6,16.7h-74.3c-9.7,0-18.1-7-19.5-16.6c-3.5-22.3-13.8-43.5-29.6-59.8c-20.4-21.2-33.1-50-33.4-81.7
                                            c-0.7-66.6,52.3-120.5,118.9-121C308.7,113.1,362.7,166.9,362.7,233.3z M256.5,160.8c0-7.4-6-13.5-13.5-13.5
                                            c-47.6,0-86.4,38.7-86.4,86.4c0,7.4,6,13.5,13.5,13.5c7.4,0,13.5-6,13.5-13.5c0-32.8,26.7-59.4,59.4-59.4
                                            C250.5,174.3,256.5,168.3,256.5,160.8z M243,74.3c7.4,0,13.5-6,13.5-13.5V13.5c0-7.4-6-13.5-13.5-13.5s-13.5,6-13.5,13.5v47.3
                                            C229.5,68.3,235.6,74.3,243,74.3z M84.1,233.2c0-7.4-6-13.5-13.5-13.5H23.3c-7.4,0-13.5,6-13.5,13.5c0,7.4,6,13.5,13.5,13.5h47.3
                                            C78.1,246.7,84.1,240.7,84.1,233.2z M462.7,219.7h-47.3c-7.4,0-13.5,6-13.5,13.5c0,7.4,6,13.5,13.5,13.5h47.3
                                            c7.4,0,13.5-6,13.5-13.5C476.2,225.8,470.2,219.7,462.7,219.7z M111.6,345.6l-33.5,33.5c-5.3,5.3-5.3,13.8,0,19.1
                                            c2.6,2.6,6.1,3.9,9.5,3.9s6.9-1.3,9.5-3.9l33.5-33.5c5.3-5.3,5.3-13.8,0-19.1C125.4,340.3,116.8,340.3,111.6,345.6z M364.9,124.8
                                            c3.4,0,6.9-1.3,9.5-3.9l33.5-33.5c5.3-5.3,5.3-13.8,0-19.1c-5.3-5.3-13.8-5.3-19.1,0l-33.5,33.5c-5.3,5.3-5.3,13.8,0,19.1
                                            C358,123.5,361.4,124.8,364.9,124.8z M111.6,120.8c2.6,2.6,6.1,3.9,9.5,3.9s6.9-1.3,9.5-3.9c5.3-5.3,5.3-13.8,0-19.1L97.1,68.2
                                            c-5.3-5.3-13.8-5.3-19.1,0c-5.3,5.3-5.3,13.8,0,19.1L111.6,120.8z M374.4,345.6c-5.3-5.3-13.8-5.3-19.1,0c-5.3,5.3-5.3,13.8,0,19.1
                                            l33.5,33.5c2.6,2.6,6.1,3.9,9.5,3.9s6.9-1.3,9.5-3.9c5.3-5.3,5.3-13.8,0-19.1L374.4,345.6z"/>
                                    </g>
                                </svg>
                                                <span>Need help?</span>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a href="javascript:;" class="add-goal">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                </g>
                                            </g>
                                        </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mor-even-container">
                                        <div class="mor-even-item sample" hidden>
                                            <div class="goal-text">Lorem lpsum is simply dummy text of printing and typesetting industry. Lorem Ipsum has been instry standard dummy</div>
                                            <footer class="goal-actions">
                                                <div class="flex-row space-between vertical-center">
                                                    <div class="flex-col fix-col">
                                                        <div class="main-actions">
                                                            <div class="flex-row margin-between">
                                                                <div class="flex-col">
                                                                    <a href="javascript:;" class="action-item complete-action">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <path d="M437.019,74.98C388.667,26.629,324.38,0,256,0C187.619,0,123.331,26.629,74.98,74.98C26.628,123.332,0,187.62,0,256
                                                                            s26.628,132.667,74.98,181.019C123.332,485.371,187.619,512,256,512c68.38,0,132.667-26.629,181.019-74.981
                                                                            C485.371,388.667,512,324.38,512,256S485.371,123.333,437.019,74.98z M256,482C131.383,482,30,380.617,30,256S131.383,30,256,30
                                                                            s226,101.383,226,226S380.617,482,256,482z"/>
                                                                    </g>
                                                                </g>
                                                                            <g>
                                                                                <g>
                                                                                    <path d="M378.305,173.859c-5.857-5.856-15.355-5.856-21.212,0.001L224.634,306.319l-69.727-69.727
                                                                            c-5.857-5.857-15.355-5.857-21.213,0c-5.858,5.857-5.858,15.355,0,21.213l80.333,80.333c2.929,2.929,6.768,4.393,10.606,4.393
                                                                            c3.838,0,7.678-1.465,10.606-4.393l143.066-143.066C384.163,189.215,384.163,179.717,378.305,173.859z"/>
                                                                                </g>
                                                                            </g>
                                                            </svg>
                                                                    </a>
                                                                </div>
                                                                <div class="flex-col">
                                                                    <a href="javascript:;" class="action-item delete-action">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <a href="javascript:;" class="more-action">
                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <circle cx="69.545" cy="306" r="69.545"/>
                                                        <circle cx="306" cy="306" r="69.545"/>
                                                        <circle cx="542.455" cy="306" r="69.545"/>
                                                    </g>
                                                </g>
                                                </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </section>
                            </section>
                        </div>
                    </div>
                </main>
            </div>
            <div class="tab-pane fade <?php echo !count($series) ? 'in active' : '';?>" id="what-do-first">
                <div class="what-do-first-pane">
                    <h3 class="pane-title">
                        Thanks for joining!<br>
                        What would you like to do first?
                    </h3>
                    <div class="btn-wrapper">
                        <div class="d-flex justify-content-center d-xs-block">
                            <a href="create_series" class="btn-create d-block">Create content to help your clients</a>
                            <a href="#select-categories-tab" class="btn-get d-block" data-toggle="tab">Get Content that benefits you</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="select-categories-tab">
                <div class="select-categories-pane">
                    <h3 class="pane-title">
                        Great!<br>
                        What categories interests you?
                    </h3>
                    <div class="at-least-text">(Select at least 3 to continue)</div>
                    <div class="categories-list flex-row flex-wrap d-xs-block">
                        <div class="category-item sample flex-col fix-col min-width-0" hidden>
                            <div class="item-title d-none d-xs-block"></div>
                            <div class="item-image-wrapper">
                                <div class="item-image"></div>
                                <div class="hover-content flex-centering">
                                    <svg version="1.1" id="Layer_1"
                                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                                         viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
<title></title>
                                        <desc>Created with Sketch.</desc>
                                        <g id="Page-1" sketch:type="MSPage">
                                            <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
			c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
			c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                            </g>
                                        </g>
</svg>
                                </div>
                            </div>
                            <div class="child-count-wrapper">
                                <div class="child-count"><span>2</span> experiences</div>
                            </div>
                            <div class="item-title d-xs-none">Daily Yoga</div>
                        </div>
                    </div>
                    <div class="footer-btn-wrapper">
                        <button class="site-green-btn show-me-btn" data-toggle="tab" data-target="#select-series-tab" disabled>
                            <span class="active-txt">Show me great experiences for my interests!</span>
                            <span class="inactive-txt">Select at least 3 interests to continue</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="select-series-tab">
                <div class="select-series-pane">
                    <h3 class="pane-title">
                        Almost there!<br>
                        Select at least 3 content channels to wrap this up
                    </h3>
                    <form class="search-input-wrapper d-xs-flex" style="color: grey;">
                        <input class="fg-xs-1" placeholder="Search for content" data-toggle="popover" data-content="Search for content that interests you.">
                        <button class="search-submit-button">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#1fcd98"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                        </button>
                    </form>
                    <div class="series-list flex-row flex-wrap d-xs-block">
                        <article class="series-article sample flex-col fix-col" hidden onclick="">
                            <div class="item-title d-none d-xs-block"></div>
                            <div class="item-image-wrapper">
                                <div class="item-image"></div>
                                <div class="check-mark-wrapper flex-centering">
                                    <svg version="1.1" id="Layer_1"
                                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                                         viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                        <title></title>
                                                                                <desc>Created with Sketch.</desc>
                                                                                <g id="Page-1" sketch:type="MSPage">
                                                                                    <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                                                        <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                                                                    </g>
                                                                                </g>
                                        </svg>
                                </div>
                                <div class="hover-content">
                                    <div class="footer-buttons">
                                        <div class="flex-row">
                                            <div class="flex-col">
                                                <a href="javascript:;" class="preview-btn" target="_blank">
                                                    <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                                </a>
                                            </div>
                                            <div class="flex-col last-col">
                                                <a href="javascript:;" class="join-btn">
                                                    <svg class="join-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                <svg class="purchased-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                                    join
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="child-count-wrapper">
                                <div class="child-count"><span>2</span> posts</div>
                            </div>
                            <div class="item-title d-xs-none"></div>
                            <div class="item-category-wrapper">
                                <a href="javascript:;" class="item-category" target="_blank">category name</a>
                            </div>
                        </article>
                    </div>
                    <footer class="pane-footer">
                        <div class="load-more-status flex-centering">
                            <span class="load-more">load more</span> <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                        </div>
                        <button class="site-green-btn let-go-btn" data-toggle="tab" data-target="#page-main-tab" disabled>
                            <span class="active-txt">I am set! Let's go!</span>
                            <span class="inactive-txt">Select at least 3 experience to continue.</span>
                        </button>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
    <?php require ASSETS_PATH . '/components/PostEditor/PostEditor.php';?>
</div>
<div class="modal fade" id="add-goal-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <a href="#add-goal-modal" class="modal-close" data-dismiss="modal"></a>
            <div class="modal-body">
                <div class="add-goal-wrapper">
                    <div class="content-header">
                        <div class="flex-row margin-between">
                            <div class="flex-col fix-col">
                                <div class="i-will-do">
                                    <h3 class="filter-name">Today, I can..</h3>
                                    <div>
                                        <a href="javascript:;" class="goal-date active" data-when="<?php echo date('Y-m-d');?>">today</a>
                                        <a href="javascript:;" class="goal-date" data-when="<?php echo date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));?>">tomorrow</a>
                                        <a href="javascript:;" class="goal-date" data-when="calendar">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="36.447px" height="36.447px" viewBox="0 0 36.447 36.447" style="enable-background:new 0 0 36.447 36.447;"
                                                 xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M30.224,3.948h-1.098V2.75c0-1.517-1.197-2.75-2.67-2.75c-1.474,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75
                                                                c0-1.517-1.197-2.75-2.67-2.75c-1.473,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75c0-1.517-1.197-2.75-2.67-2.75
                                                                c-1.473,0-2.67,1.233-2.67,2.75v1.197H6.224c-2.343,0-4.25,1.907-4.25,4.25v24c0,2.343,1.907,4.25,4.25,4.25h24
                                                                c2.344,0,4.25-1.907,4.25-4.25v-24C34.474,5.855,32.567,3.948,30.224,3.948z M25.286,2.75c0-0.689,0.525-1.25,1.17-1.25
                                                                c0.646,0,1.17,0.561,1.17,1.25v4.896c0,0.689-0.524,1.25-1.17,1.25c-0.645,0-1.17-0.561-1.17-1.25V2.75z M17.206,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z M9.125,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z
                                                                 M31.974,32.198c0,0.965-0.785,1.75-1.75,1.75h-24c-0.965,0-1.75-0.785-1.75-1.75v-22h27.5V32.198z"/>
                                                            <rect x="6.724" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="12.857" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="18.995" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="25.128" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="6.724" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="6.724" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="25.54" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="25.54" width="4.596" height="4.086"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            <input type="text" class="active-date" data-date-format="yyyy-mm-dd"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <a href="#add-goal-modal" class="add-goal" data-toggle="modal">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                </g>
                                            </g>
                                        </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="multi-line-background">
                        <textarea class="goal-input" required placeholder="Start typing your next goal"></textarea>
                        <div class="background-placeholder"></div>
                    </div>
                    <div class="button-wrapper">
                        <button class="add-btn">Add inspiration</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-panel-component-wrap" hidden>
    <?php require_once ASSETS_PATH . '/components/ListingSubscription/ListingSubscription.php';?>
    <?php require_once ASSETS_PATH . '/components/ListingPost/ListingPost.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var series = <?php echo json_encode($series);?>;
    var goals = <?php echo json_encode($goals);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var allCategories = <?php echo json_encode($allCategories);?>;
    var categories = <?php echo json_encode($categories);?>;
    var stripeApi_pKey = <?php echo json_encode($stripeApi_pKey);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my_solutions-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostNotes/PostNotes.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PurchasedSubscriptionsTree/PurchasedSubscriptionsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/plugins/touch/touch.js?version=<?php echo time();?>"></script>

<script src="<?php echo BASE_URL;?>/assets/components/FormField/FormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/ListingSubscription/ListingSubscription.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.js?version=<?php echo time();?>"></script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my-solutions-page.js?version=<?php echo time();?>"></script>

</body>
</html>
