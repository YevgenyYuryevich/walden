<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.02.2018
 * Time: 07:23
 */

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="strPost_title">
    <meta name="Description" content='$metaDes'>
    <meta property="og:title" content="strPost_title"/>
    <meta property="og:image" content="strPost_featuredimage"/>
    <meta property="og:description" content='$metaDes'/>

    <title>Title</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/view-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/StripeCardModal/StripeCardModal.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/view-page.css?version=<?php echo time();?>" />
</head>
<body>
<div class="site-wrapper page page-view">
    <div class="decoration-container">
        <div class="nav-portion left-portion flex-box vertical-center horizon-center">
            <a class="cst-arrow left-arrow prev-post">previous</a>
        </div>
        <div class="nav-portion right-portion flex-box vertical-center horizon-center">
            <a class="cst-arrow right-arrow next-post">next</a>
        </div>
    </div>
    <div class="site-content post-type-someone">
        <header class="sticky-header">
            <div class="flex-row vertical-center">
                <div class="flex-col">
                    <a class="back-to-prev" href="<?php echo $prevPage;?>">
                        <i class="glyphicon glyphicon-menu-left"></i>
                    </a>
                    <div class="sticky-series-title"></div>
                </div>
                <div class="flex-col">
                    <div class="sticky-post-title"></div>
                </div>
                <div class="flex-col fix-col embed-col">
                    <?php require_once ASSETS_PATH . '/components/PostCopyEmbed/PostCopyEmbed.html';?>
                </div>
                <div class="flex-col fix-col circle-col">
                    <nav class="circle-nav-wrapper">
                        <div class="circle-nav-toggle">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="50px" height="50px">
                                <g>
                                    <path d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M30,58C14.561,58,2,45.439,2,30   S14.561,2,30,2s28,12.561,28,28S45.439,58,30,58z" fill="#006DF0"/>
                                    <path d="M39,20c3.309,0,6-2.691,6-6s-2.691-6-6-6c-3.131,0-5.705,2.411-5.973,5.474L18.961,23.788C18.086,23.289,17.077,23,16,23   c-3.309,0-6,2.691-6,6s2.691,6,6,6c1.077,0,2.086-0.289,2.961-0.788l14.065,10.314C33.295,47.589,35.869,50,39,50   c3.309,0,6-2.691,6-6s-2.691-6-6-6c-2.69,0-4.972,1.78-5.731,4.223l-12.716-9.325C21.452,31.848,22,30.488,22,29   s-0.548-2.848-1.448-3.898l12.716-9.325C34.028,18.22,36.31,20,39,20z M39,10c2.206,0,4,1.794,4,4s-1.794,4-4,4s-4-1.794-4-4   S36.794,10,39,10z M12,29c0-2.206,1.794-4,4-4s4,1.794,4,4s-1.794,4-4,4S12,31.206,12,29z M39,40c2.206,0,4,1.794,4,4s-1.794,4-4,4   s-4-1.794-4-4S36.794,40,39,40z" fill="#006DF0"/>
                                </g></svg>
                        </div>
                        <div class="circle-nav-panel"></div>
                        <ul class="circle-nav-menu">
                            <li class="circle-nav-item circle-nav-item-1">
                                <a href="https://www.facebook.com/sharer" target="_blank">
                                    <svg version="1.1" id="facebook-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="30px" height="30px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;"
                                         xml:space="preserve">
                                    <g><path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803
                                            c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654
                                            c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246
                                            c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"/>
                                    </g>
                                </svg><span>Facebook</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-2">
                                <a href="https://twitter.com/intent" target="_blank">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512.002px" height="512.002px" viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;"xml:space="preserve">
                                    <g>
                                        <path d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115
                                            c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039
                                            c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809
                                            c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321
                                            c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888
                                            c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471
                                            c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59
                                            C480.111,136.775,497.92,118.275,512.002,97.211z"/>
                                    </g>
                                </svg><span>Twitter</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-3">
                                <a href="https://www.linkedin.com/shareArticle" target="_blank">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="430.117px" height="430.117px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
                                         xml:space="preserve">
                                    <g>
                                        <path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
                                        c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
                                        v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
                                        C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
                                        c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
                                         M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
                                    </g></svg><span>Linkedlin</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-4">
                                <a href="https://plus.google.com/share" target="_blank">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="96.828px" height="96.827px" viewBox="0 0 96.828 96.827" style="enable-background:new 0 0 96.828 96.827;"
                                         xml:space="preserve">
                                        <g>
                                            <path d="M62.617,0H39.525c-10.29,0-17.413,2.256-23.824,7.552c-5.042,4.35-8.051,10.672-8.051,16.912
                                                c0,9.614,7.33,19.831,20.913,19.831c1.306,0,2.752-0.134,4.028-0.253l-0.188,0.457c-0.546,1.308-1.063,2.542-1.063,4.468
                                                c0,3.75,1.809,6.063,3.558,8.298l0.22,0.283l-0.391,0.027c-5.609,0.384-16.049,1.1-23.675,5.787
                                                c-9.007,5.355-9.707,13.145-9.707,15.404c0,8.988,8.376,18.06,27.09,18.06c21.76,0,33.146-12.005,33.146-23.863
                                                c0.002-8.771-5.141-13.101-10.6-17.698l-4.605-3.582c-1.423-1.179-3.195-2.646-3.195-5.364c0-2.672,1.772-4.436,3.336-5.992
                                                l0.163-0.165c4.973-3.917,10.609-8.358,10.609-17.964c0-9.658-6.035-14.649-8.937-17.048h7.663c0.094,0,0.188-0.026,0.266-0.077
                                                l6.601-4.15c0.188-0.119,0.276-0.348,0.214-0.562C63.037,0.147,62.839,0,62.617,0z M34.614,91.535
                                                c-13.264,0-22.176-6.195-22.176-15.416c0-6.021,3.645-10.396,10.824-12.997c5.749-1.935,13.17-2.031,13.244-2.031
                                                c1.257,0,1.889,0,2.893,0.126c9.281,6.605,13.743,10.073,13.743,16.678C53.141,86.309,46.041,91.535,34.614,91.535z
                                                 M34.489,40.756c-11.132,0-15.752-14.633-15.752-22.468c0-3.984,0.906-7.042,2.77-9.351c2.023-2.531,5.487-4.166,8.825-4.166
                                                c10.221,0,15.873,13.738,15.873,23.233c0,1.498,0,6.055-3.148,9.22C40.94,39.337,37.497,40.756,34.489,40.756z"/>
                                            <path d="M94.982,45.223H82.814V33.098c0-0.276-0.225-0.5-0.5-0.5H77.08c-0.276,0-0.5,0.224-0.5,0.5v12.125H64.473
                                                c-0.276,0-0.5,0.224-0.5,0.5v5.304c0,0.275,0.224,0.5,0.5,0.5H76.58V63.73c0,0.275,0.224,0.5,0.5,0.5h5.234
                                                c0.275,0,0.5-0.225,0.5-0.5V51.525h12.168c0.276,0,0.5-0.223,0.5-0.5v-5.302C95.482,45.446,95.259,45.223,94.982,45.223z"/>
                                        </g></svg><span>Google</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-5">
                                <a href="mailto:">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" style="enable-background:new 0 0 79.536 79.536;"
                                         xml:space="preserve">
                                    <g>
                                        <path style="fill:#010002;" d="M39.773,1.31L0,31.004v47.222h79.536V31.004L39.773,1.31z M28.77,22.499
                                        c1.167-2.133,2.775-3.739,4.815-4.805c2.035-1.075,4.357-1.616,6.983-1.616c2.214,0,4.191,0.435,5.921,1.292
                                        c1.729,0.87,3.045,2.094,3.967,3.687c0.9,1.595,1.367,3.334,1.367,5.217c0,2.247-0.694,4.279-2.082,6.097
                                        c-1.74,2.292-3.961,3.436-6.68,3.436c-0.732,0-1.279-0.122-1.654-0.38c-0.365-0.262-0.621-0.632-0.743-1.129
                                        c-1.022,1.012-2.231,1.52-3.589,1.52c-1.465,0-2.679-0.507-3.643-1.509c-0.966-1.012-1.447-2.361-1.447-4.031
                                        c0-2.084,0.578-3.966,1.743-5.672c1.416-2.084,3.218-3.13,5.424-3.13c1.571,0,2.731,0.601,3.475,1.805l0.331-1.468h3.5
                                        l-1.998,9.479c-0.125,0.606-0.187,0.986-0.187,1.163c0,0.228,0.052,0.38,0.149,0.497c0.099,0.111,0.223,0.165,0.357,0.165
                                        c0.436,0,0.979-0.248,1.646-0.769c0.901-0.663,1.627-1.574,2.181-2.695c0.554-1.129,0.839-2.299,0.839-3.508
                                        c0-2.165-0.782-3.977-2.352-5.445c-1.573-1.45-3.77-2.185-6.578-2.185c-2.393,0-4.417,0.487-6.077,1.468
                                        c-1.66,0.966-2.913,2.343-3.765,4.114c-0.839,1.76-1.258,3.607-1.258,5.52c0,1.856,0.479,3.552,1.411,5.074
                                        c0.945,1.533,2.26,2.641,3.956,3.345c1.696,0.697,3.643,1.046,5.828,1.046c2.097,0,3.909-0.293,5.432-0.881
                                        c1.522-0.587,2.739-1.457,3.666-2.641h2.807c-0.88,1.792-2.227,3.192-4.049,4.215c-2.092,1.163-4.64,1.74-7.644,1.74
                                        c-2.918,0-5.426-0.487-7.542-1.468c-2.121-0.986-3.689-2.434-4.73-4.35c-1.028-1.918-1.535-4.008-1.535-6.268
                                        C27.017,26.952,27.595,24.64,28.77,22.499z M2.804,31.941l29.344,19.68L2.804,74.333V31.941z M5.033,75.844l34.74-26.885
                                        l34.729,26.885H5.033z M76.729,74.333L47.391,51.621l29.339-19.68V74.333z M41.205,24.661c0.466,0.531,0.699,1.295,0.699,2.292
                                        c0,0.891-0.174,1.856-0.513,2.879c-0.334,1.036-0.743,1.826-1.209,2.361c-0.318,0.375-0.658,0.652-0.992,0.826
                                        c-0.439,0.249-0.906,0.37-1.41,0.37c-0.674,0.006-1.23-0.264-1.691-0.794c-0.45-0.531-0.673-1.346-0.673-2.465
                                        c0-0.839,0.158-1.805,0.487-2.889c0.329-1.088,0.81-1.916,1.453-2.509c0.647-0.588,1.346-0.881,2.1-0.881
                                        C40.162,23.856,40.749,24.125,41.205,24.661z"/>
                                    </g></svg><span>Opened-Email-Envelope</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <?php if ($isLoggedIn): ?>
                    <div class="flex-col fix-col">
                        <?php if ($from === 'subscription'): ?>
                            <div class="complete-wrapper">
                                <button class="toogle-complete btn btn-block"><i class="glyphicon glyphicon-ok"></i></button>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php else: ?>
                <div class="flex-col fix-col">
                    <div class="login-wrapper">
                        <a href="login" class="btn">Login</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </header>
        <article class="blog-wrapper">
            <header class="blog-header">
                <h2 class="series-title"></h2>
                <h3 class="post-title"></h3>
            </header>
            <aside class="read-time-wrapper" style="">Read time: <div class="eta"></div></aside>
            <div class="blog-body">
                <div class="type-pay blog-type sample" hidden>
                    <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
                </div>
                <div class="type-0 blog-type sample" hidden>
                    <div class="audiogallery">
                        <div class="items">
                            <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                        </div>
                    </div>
                </div>
                <div class="type-2 blog-type sample" hidden>
                    <div class="video-wrapper">
                        <video class="video-js" controls width="960" height="540"></video>
                    </div>
                </div>
                <div class="type-8 blog-type sample" hidden>
                </div>
                <div class="type-menu blog-type sample" hidden>
                    <header>
                        <h3 class="menu-title"></h3>
                        <p class="menu-description"></p>
                    </header>
                    <div class="paths-container">
                        <div class="flex-col path-col sample" hidden>
                            <div class="img-wrapper">
                                <img />
                            </div>
                            <h3 class="path-title"></h3>
                            <p class="path-description"></p>
                        </div>
                        <div class="flex-row paths-list flex-wrap">
                        </div>
                    </div>
                </div>
                <div class="type-10 blog-type sample" hidden></div>
                <div class="type-other blog-type sample" hidden></div>
            </div>
        </article>
        <footer class="sticky-footer">
            <div class="flex-row <?php echo !$isLoggedIn ? 'not-loggedin' : '';?>">
                <div class="speaker-wrapper">
                    <div class="play-speaker-wrapper">
                        <a class="play-speaker" href="javascript:;"><i class="glyphicon glyphicon-play-circle"></i></a>
                        <a class="pause-speaker" href="javascript:;"><i class="glyphicon glyphicon-pause"></i></a>
                    </div>
                    <div class="choose-lang-wrapper">
                        <label>Choose Language</label>
                        <select name="speaker_lang"></select>
                    </div>
                    <div class="rate-wrapper">
                        <label>Rate</label>
                        <div class="range-field">
                            <input type="range" name="speaker_rate" min="1" max="100" value="10" />
                        </div>
                    </div>
                    <div class="pitch-wrapper">
                        <label>Pitch</label>
                        <div class="range-field">
                            <input type="range" name="speaker_pitch" min="0" max="2" value="1" />
                        </div>
                    </div>
                </div>
                <?php if (!$isLoggedIn): ?>
                    <div class="visit-cnt-left-wrapper">
                        <div class="visit-cnt-left-content">
                            <?php if ($visitCnt_left > 0): ?>
                                You have <span class="left-<?php echo $visitCnt_left;?>"><?php echo $visitCnt_left;?></span> view<?php echo $visitCnt_left > 1 ? 's': '';?> left
                            <?php else: ?>
                                You have no views to left. <a href="login">Login</a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="font-control-wrapper">
                    <div class="font-control-container">
                        <button class="font-control reduce-fontsize">
                            <i class="glyphicon glyphicon-font"></i>-
                        </button>
                        <button class="font-control default-fontsize">
                            <i class="glyphicon glyphicon-font"></i>
                        </button>
                        <button class="font-control increase-fontsize">
                            <i class="glyphicon glyphicon-font"></i>+
                        </button>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <?php require_once ASSETS_PATH . '/components/StripeCardModal/StripeCardModal.html';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    const BASE_URL = window.location.protocol + "//" + window.location.host;
</script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/view-global.js?version=<?php echo time();?>"></script>
<script type="text/javascript">
    const ACTION_URL = BASE_URL + window.location.pathname;
    var from = <?php echo json_encode($from);?>;
    var series = <?php echo json_encode($series);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var initialPostData = <?php echo json_encode($postData);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var formFields = <?php echo json_encode($formFields);?>;
    const CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
    const AFFILIATE_ID = <?php echo isset($_SESSION['affiliate_id']) ? json_encode($_SESSION['affiliate_id']) : -1;?>;
</script>
<script src="https://js.stripe.com/v3/"></script>
<script src="<?php echo BASE_URL;?>/assets/components/StripeCardModal/StripeCardModal.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/view-page.js?version=<?php echo time();?>"></script>
</body>
</html>