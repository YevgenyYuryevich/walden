<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.08.2018
 * Time: 12:10
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $series['strSeries_title'];?></title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/seriesEmbedGrid-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/SeriesEmbedGrid-page.css?version=<?php echo time();?>">
    <?php
    if ( $externalStyle && $externalStyle !== 'false' ) {
        echo '<link rel="stylesheet" href="'. $externalStyle .'" />';
    }
    ?>
    <style type="text/css">
        <?php echo $inlineStyle && $inlineStyle !== 'false' ? $inlineStyle : '';?>
    </style>
</head>

<body>
<div class="site-wrapper <?php echo $isLoggedIn ? 'logged-in' : '';?>">
    <main class="main-content">
        <div class="content-inner">
            <header class="content-header">
                <div class="flex-row vertical-center space-between">
                    <div class="flex-col">
                        <h3 class="welcome">Welcome back, <span class="display-name"><?php echo $isLoggedIn ? $_SESSION['f_name'] : '';?></span>!</h3>
                        <div class="login-btn">
                            <a class="btn big-login" href="javascript:;">Log in</a>
                        </div>
                    </div>
                </div>
            </header>
            <section class="posts-grid">
                <div class="item-sample-wrapper" hidden>
                    <div>
                        <div class="item-inner">
                            <aside>
                                <div class="bottom-line"></div>
                                <svg class="star-item" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                    <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                        81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                                    </svg>
                            </aside>
                            <div class="img-wrapper">
                                <img src="" alt="" class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <div class="item-title"></div>
                            </div>
                            <div class="buttons-wrapper">
                                <a href="javascript:;" class="view-post btn btn-circle">View</a>
                            </div>
                            <div class="object_line"></div>
                        </div>
                    </div>
                </div>
                <div class="tt-grid-wrapper after-clearfix">
                    <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                        <li class="tt-empty"></li>
                    </ul>
                </div>
            </section>
            <footer class="content-footer">
                <div class="nav-wrapper">
                    <div class="flex-row space-between vertical-center">
                        <div class="flex-col fix-col">
                            <a class="step-back">
                                <span class="content-turbotax__back-arr">‹</span> Back
                            </a>
                        </div>
                        <div class="flex-col fix-col">
                            <a class="step-next">Continue</a>
                        </div>
                    </div>
                </div>
                <div class="powered-by">Powered by Walden.ly</div>
            </footer>
        </div>
    </main>
</div>
<div class="modal fade login" id="loginModal">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login with</h4>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="content">
                        <div class="social">
                            <a class="circle github" href="/auth/github">
                                <i class="fa fa-github fa-fw"></i>
                            </a>
                            <a id="google_login" class="circle google" href="/auth/google_oauth2">
                                <i class="fa fa-google-plus fa-fw"></i>
                            </a>
                            <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                                <i class="fa fa-facebook fa-fw"></i>
                            </a>
                        </div>
                        <div class="division">
                            <div class="line l"></div>
                            <span>or</span>
                            <div class="line r"></div>
                        </div>
                        <div class="error"></div>
                        <div class="form loginBox">
                            <form method="post">
                                <input class="form-control" type="text" placeholder="Email" name="email" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="btn btn-default btn-login" type="submit" value="Login">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content registerBox" style="display:none;">
                        <div class="form">
                            <form method="post">
                                <input class="form-control" type="email" placeholder="Email" name="email" required>
                                <input class="form-control" type="text" placeholder="First Name" name="first_name" required>
                                <input class="form-control" type="text" placeholder="Last Name" name="last_name" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation" required>
                                <input class="btn btn-default btn-register" type="submit" value="Create account" name="commit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="forgot login-footer">
                            <span>Looking to
                                 <a href="javascript:;" class="show-register">create an account</a>
                            ?</span>
                </div>
                <div class="forgot register-footer" style="display:none">
                    <span>Already have an account?</span>
                    <a class="show-login" href="javascript:;">Login</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once ASSETS_PATH . '/components/ViewPostPopupWidget/ViewPostPopupWidget.php';?>

<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
    const CURRENT_PAGE = window.location.pathname.substr(1);
    const BASE_URL = window.location.protocol + "//" + window.location.host;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/seriesEmbedGrid-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var series = <?php echo json_encode($series);?>;
    var initialPosts = <?php echo json_encode($posts);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var clientName = <?php echo $isLoggedIn ? json_encode($_SESSION['f_name']) : json_encode(false);?>;
    var clientSeriesView = <?php echo json_encode($clientSeriesView);?>;
    var seekPost = <?php echo json_encode($seekPost);?>;
    var formFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var welcomePost = <?php echo json_encode($welcomePost);?>;
    var embedId = <?php echo json_encode($embedId);?>;
    var postViewVersion = <?php echo json_encode($postViewVersion);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/services/EmbedPageGlobal.js?version=<?php echo time();?>"></script>
<?php
if ( $externalScript && $externalScript !== 'false' ) {
    echo '<script src="' . $externalScript . '"></script>';
}
?>
<script type="application/javascript">
    <?php echo $inlineScript && $inlineScript !== 'false' ? $inlineScript : '';?>
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/SeriesEmbedGrid-page.js?version=<?php echo time();?>"></script>
</body>
</html>
