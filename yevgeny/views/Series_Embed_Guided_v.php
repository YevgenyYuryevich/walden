<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.12.2018
 * Time: 11:07
 */

?>

<!DOCTYPE html>
<html class="walden-site">

<head>
    <!--    <meta name="viewport" content="width=screen-width, initial-scale=1, maximum-scale=1, user-scalable=no">-->
    <title>Edit the solution to make it perfect for you - thegreyshirt</title>
    <meta name="description"
          content="Edit your daily solution to include the content that you want in the order that you want it."/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION; ?>"/>
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/css/yevgeny/home-common.css?version=<?php echo time(); ?>"/>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PayBlog/PayBlog.css?version=<?php echo time();?>" />
    <link rel="stylesheet"
          href="<?php echo BASE_URL; ?>/assets/css/yevgeny/series_embed_guided-page.css?version=<?php echo time(); ?>"/>
    <?php
    if ($externalStyle && $externalStyle !== 'false') {
        echo '<link rel="stylesheet" href="' . $externalStyle . '" />';
    }
    ?>
    <style type="text/css">
        <?php echo $inlineStyle && $inlineStyle !== 'false' ? $inlineStyle : '';?>
    </style>
</head>
<body class="listing-opened">
<div class="site-wrapper <?php echo $isLoggedIn ? 'logged-in' : ''; ?>">
    <div class="view-panel">
        <div class="listing-toggle flex-centering">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                </g>
                <g>
                    <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                    <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                    <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                </g>
                            </svg>
        </div>
        <aside class="money-saved">
            <span class="money-amount">$<span><?php echo $totalMoneySaved ? $totalMoneySaved : 0;?></span></span>
            <span class="w-name"> saved</span>
        </aside>
        <div class="blog-content-wrapper">
            <div class="post-image-wrapper">
                <img class="post-img" />
            </div>
            <div class="post-title"></div>
            <div class="post-body">
                <div class="type-0 blog-type sample" hidden>
                    <div class="audiogallery">
                        <div class="items">
                            <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                        </div>
                    </div>
                </div>
                <div class="type-2 blog-type sample" hidden>
                    <div class="video-wrapper">
                        <video controls class="video-js" data-autoresize="fit" width="960" height="540"></video>
                    </div>
                </div>
                <div class="type-8 blog-type sample" hidden>
                </div>
                <div class="type-menu blog-type sample" hidden>
                    <header>
                        <h3 class="menu-title"></h3>
                        <p class="menu-description"></p>
                    </header>
                    <div class="paths-container">
                        <div class="flex-col path-col sample" hidden>
                            <div class="img-wrapper">
                                <img />
                            </div>
                            <h3 class="path-title"></h3>
                            <p class="path-description"></p>
                        </div>
                        <div class="flex-row paths-list flex-wrap">
                        </div>
                    </div>
                </div>
                <div class="type-10 blog-type sample" hidden></div>
                <div class="type-other blog-type sample" hidden></div>
            </div>
        </div>
        <div class="pay-blog-wrapper" hidden>
            <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
        </div>
        <footer class="view-panel-footer">
            <div class="nav-wrapper">
                <div class="flex-row space-between vertical-center">
                    <div class="flex-col fix-col">
                        <a class="step-back bottom-underline" href="javascript:;">
                            <<< back
                        </a>
                    </div>
                    <div class="flex-col fix-col">
                        <a class="step-next">Continue</a>
                    </div>
                </div>
            </div>
            <div class="powered-by">Powered by Walden.ly</div>
        </footer>
    </div>
    <div class="listing-panel">
        <section class="listing-posts" id="listing-posts">
            <header class="listing-posts-header">
                <div class="title-wrapper">
                        <span class="series-title">
                            <?php echo $series['strSeries_title'];?>
                        </span>
                    <div class="posts-count common-radius-aside"><span
                                class="count-value"><?php echo $postsCount; ?></span> posts
                    </div>
                </div>
                <a href="javascript:;" class="section-close"></a>
            </header>
            <div class="listing-posts-content">
                <ul class="posts-list">
                    <li class="item sample" hidden>
                        <div class="flex-row margin-between">
                            <div class="flex-col fix-col">
                                <img class="item-img" alt=""/>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="item-view-states-wrapper">
                                    <svg version="1.1" class="complete-state view-state" id="Layer_1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px"
                                         height="38px" viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38"
                                         xml:space="preserve">
                                                        <title>Rectangle 4</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id="Page-1" sketch:type="MSPage">
                                            <g id="Check_1" transform="translate(115.000000, 102.000000)"
                                               sketch:type="MSLayerGroup">
                                                <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"></path>
                                            </g>
                                        </g>
                                                        </svg>
                                    <div class="current-state view-state"></div>
                                    <div class="normal-state view-state"></div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="title-wrapper">
                                    <aside class="media-icon-wrapper">
                                        <i class="fa fa-youtube-play video-icon"></i>
                                        <i class="fa fa-volume-up audio-icon"></i>
                                        <i class="fa fa-file-text text-icon"></i>
                                    </aside>
                                    <a href="javascript:;" class="item-title">Great breakfasts</a> | <span
                                            class="item-duration">waiting...</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    const BASE_URL = <?php echo json_encode(BASE_URL);?>;
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
</script>
<script src="<?php echo BASE_URL; ?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION; ?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/yevgeny/series_embed_guided-global.js?version=<?php echo time(); ?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var from = 'post';
    var formFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var welcomePost = <?php echo json_encode($welcomePost);?>;
    var clientName = <?php echo $isLoggedIn ? json_encode($_SESSION['f_name']) : json_encode(false);?>;
    var clientSeriesView = <?php echo json_encode($clientSeriesView);?>;
    var posts = <?php echo json_encode($posts);?>;
    var seekPost = <?php echo json_encode($seekPost);?>;
    var series = <?php echo json_encode($series);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var embedId = <?php echo json_encode($embedId);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PayBlog/PayBlog.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/yevgeny/series_embed_guided-page.js?version=<?php echo time(); ?>"></script>
<?php
if ($externalScript && $externalScript !== 'false') {
    echo '<script src="' . $externalScript . '"></script>';
}
?>
<script type="application/javascript">
    <?php echo $inlineScript && $inlineScript !== 'false' ? $inlineScript : '';?>
</script>
</body>
</html>