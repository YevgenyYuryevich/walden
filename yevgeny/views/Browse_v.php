<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.10.2018
 * Time: 18:34
 */
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Find solutions to challenges in your life.</title>
    <meta name="description" content="Offer help to people who have have asked for help in this area and get help and support from others in this area when you need it.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <!--    <link rel="stylesheet" href="--><?php //echo BASE_URL;?><!--/assets/css/yevgeny/browse-global.css?version=--><?php //echo time();?><!--">-->
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/browse-page.css?version=<?php echo time();?>">

</head>

<body>

<div class="site-wrapper page page-browse <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <div class="filter-wrapper size-full d-xs-none">
            <a href="javascript:;" class="resize-filter">
                <i class="glyphicon glyphicon-resize-full"></i>
                <i class="glyphicon glyphicon-resize-small"></i>
            </a>
            <aside class="drag-handler">
                <img class="close-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filiter-drag-handler.png" alt="no" />
                <img class="open-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/filter-drag-handler-open.png" alt="no" />
            </aside>
            <div class="flex-row vertical-center space-between">
                <div class="flex-col fix-col">
                    <div>
                        <div class="bootcamp-browse do-browse">
                            Bootcamp
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                </g>
                            </g>
                        </svg>
                            <div class="browse-drop-down-wrapper">
                                <div class="browse-drop-down-inner">
                                    <ul>
                                        <li><a href="browse?id=bootcamp"><i class="glyphicon glyphicon-menu-right"></i><span>All categories</span></a></li>
                                        <?php
                                        foreach ($categories['simplify'] as $category) {
                                            ?>
                                            <li><a href="browse?id=<?php echo $category['category_ID']?>"><i class="glyphicon glyphicon-menu-right"></i><span><?php echo $category['strCategory_name'];?></span></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="on-demand-browse do-browse">
                            On-Demand
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#f26d7e"></path>
                                </g>
                            </g>
                        </svg>
                            <div class="browse-drop-down-wrapper">
                                <div class="browse-drop-down-inner">
                                    <ul>
                                        <li><a href="browse?id=on-demand"><i class="glyphicon glyphicon-menu-right"></i><span>All categories</span></a></li>
                                        <?php
                                        foreach ($categories['enrich'] as $category) {
                                            ?>
                                            <li><a href="browse?id=<?php echo $category['category_ID'];?>"><i class="glyphicon glyphicon-menu-right"></i><span><?php echo $category['strCategory_name'];?></span></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="filters">
                        <div class="filter-name">Filters</div>
                        <div class="flex-row vertical-center margin-between">
                            <div class="flex-col fix-col">
                                <div class="filter-item">
                                    <input id="browse-free-content" type="checkbox"/><label for="browse-free-content">free content</label>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="filter-item">
                                    <input id="browse-premium-content" type="checkbox"/><label for="browse-premium-content">premium content</label>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="filter-item">
                                    <input id="browse-affiliates-content" type="checkbox"/><label for="browse-affiliates-content">affiliates</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="sort-by">
                        <div class="filter-name">Sort by</div>
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                <span>popular</span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                        </g>
                                    </g>
                                </svg>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
                                <li hidden><a href="#" data-value="popular">popular</a></li>
                                <li><a href="#" data-value="new">new</a></li>
                                <li><a href="#" data-value="a-z">A-z</a></li>
                                <li><a href="#" data-value="z-a">Z-a</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <main class="main-content <?php echo !$inputCategory ? 'all-categories' : ('category-' . $inputCategory['category_ID'])?>">
            <div class="flex-row flex-wrap">
                <div class="flex-col fg-0 category-col">
                    <article class="category-article">
                        <header class="category-title">Daily recipes</header>
                        <div class="category-description">The content from these category will be provided to you on daily basis.</div>
                    </article>
                </div>
                <div class="flex-col d-none d-xs-block min-width-0">
                    <div class="category-filter-wrapper">
                        <div class="flex-row vertical-center align-items-xs-start">
                            <div class="flex-col fix-col">
                                <a href="browse" class="category-item <?php echo !$inputCategory ? 'active' : '';?>" data-category="-1">All</a>
                            </div>
                            <div class="flex-col fix-col">
                                <a href="browse?id=simplify" class="category-item <?php echo $inputCategory['category_ID'] == 'simplify' ? 'active' : '';?>" data-category="simplify">Simplify</a>
                            </div>
                            <div class="flex-col fix-col">
                                <a href="browse?id=enrich" class="category-item <?php echo $inputCategory['category_ID'] == 'enrich' ? 'active' : '';?>" data-category="enrich">Enrich</a>
                            </div>
                            <?php foreach ($categories['simplify'] as $category): ?>
                                <div class="flex-col fix-col">
                                    <a href="browse?id=<?php echo $category['category_ID'];?>" class="category-item <?php echo $inputCategory['category_ID'] == $category['category_ID'] ? 'active' : '';?>" data-category="<?php echo $category['category_ID'];?>"><?php echo $category['strCategory_name'];?></a>
                                </div>
                            <?php endforeach; ?>
                            <?php foreach ($categories['enrich'] as $category): ?>
                                <div class="flex-col fix-col">
                                    <a href="browse?id=<?php echo $category['category_ID'];?>" class="category-item <?php echo $inputCategory['category_ID'] == $category['category_ID'] ? 'active' : '';?>" data-category="<?php echo $category['category_ID'];?>"><?php echo $category['strCategory_name'];?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-col fg-0 sample series-col" hidden>
                    <article class="series-article" onclick="">
                        <div class="item-title d-none d-xs-block"></div>
                        <div class="item-image-wrapper">
                            <div class="item-image"></div>
                            <div class="hover-content">
                                <div class="footer-buttons">
                                    <div class="flex-row">
                                        <div class="flex-col">
                                            <a href="javascript:;" class="preview-btn" target="_blank">
                                                <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                            </a>
                                        </div>
                                        <div class="flex-col last-col">
                                            <a href="javascript:;" class="join-btn">
                                                <svg class="join-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                <svg class="purchased-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                                join
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="child-count-wrapper">
                            <div class="child-count"><span>2</span> posts</div>
                        </div>
                        <div class="item-title d-xs-none"></div>
                        <div class="item-category-wrapper">
                            <a href="javascript:;" class="item-category" target="_blank">category name</a>
                        </div>
                    </article>
                </div>
                <div class="flex-col fg-0 share-col">
                    <article class="share-experiences">
                        <div class="flex-row d-block d-sm-flex vertical-center space-around">
                            <div class="flex-col fix-col">
                                <div class="image-item">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-share-experience.svg">
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <div class="text-item">
                                    <h3>Share your passion</h3>
                                    <div>Collect content about something you love<br>and develop your collection with friends, family, co-workers,<br>and leaders from around the world.<br><br></div>
                                    <aside>
                                        <a href="https://www.walden.ly/create_series">Get started</a><br><br>
                                    </aside>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <a href="https://www.walden.ly/create_series" class="plus-item">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                </g>
                            </g>
                        </svg>
                                </a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script>
    var series = <?php echo json_encode($series);?>;
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
    var categories = <?php echo json_encode($categories);?>;
    var category = <?php echo json_encode($inputCategory);?>;
    var stripeApi_pKey = <?php echo json_encode($stripeApi_pKey);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/browse-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/browse-page.js?version=<?php echo time();?>"></script>

</body>
</html>
