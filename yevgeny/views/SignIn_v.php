<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.07.2018
 * Time: 16:11
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Please login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/signIn-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/signIn-page.css?version=<?php echo time();?>" />
</head>
<body>

<div class="page-wrapper">
    <div class="container-login100" style="background: lightgrey; /* For browsers that do not support gradients */
    background: linear-gradient(to bottom left, #fff, #8ecbff);">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 tab-content">
            <form method="post" role="tabpanel" class="login100-form for-signin validate-form tab-pane fade <?php echo $type =='signin' ? 'in active' : ''?>" id="signin-form">
                <span class="login100-form-title p-b-20" style="color:#151515;">
						Welcome
					</span>
                <span style="display:block; font-size:22px; text-align:center; color:#747474">Please login</span>
                <div class="alert alert-danger" role="alert" style="opacity: 0">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Username or Password is wrong
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is required">
                    <span class="label-input100">Username</span>
                    <input class="input100" type="text" name="username" placeholder="Type your username">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Type your password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="text-right p-t-8 p-b-31">
                    <a data-toggle="tab" href="#forgot-password-form">
                        Forgot password?
                    </a>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                </div>

                

                

                <div class="flex-col-c p-t-105">
						

                    <a data-toggle="tab" href="#<?php echo $canRegister ? 'signup-form' : 'send-email-form';?>" class="txt2">
                        Sign Up
                    </a>
                </div>
            </form>
            <form method="post" role="tabpanel" class="login100-form for-signup validate-form tab-pane fade <?php echo $type =='register' && $canRegister ? 'in active' : ''?>" id="signup-form" autocomplete="off">
                <span class="login100-form-title p-b-20">
						Sign Up
					</span>
                <div class="alert alert-danger" role="alert" style="opacity: 0">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Sorry! Something is wrong.
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is required" data-exist = "Username is already exist." data-require="Username is required">
                            <span class="label-input100">Username</span>
                            <input class="input100" type="text" name="username" placeholder="Type your username" value=''>
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-23" data-validate = "Email is required" data-exist = "Email is already exist." data-require="Email is required">
                            <span class="label-input100">Email</span>
                            <input class="input100" type="email" name="email" placeholder="Type your email"  value='<?php echo $invitationRow ? $invitationRow['strInvitedEmail'] : '';?>'>
                            <span class="focus-input100" data-symbol="&#xf15a;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="First Name is required">
                            <span class="label-input100">First Name</span>
                            <input class="input100" type="text" name="f_name" placeholder="Type your first name">
                            <span class="focus-input100" data-symbol="&#xf20e;"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Last Name is required">
                            <span class="label-input100">Last Name</span>
                            <input class="input100" type="text" name="l_name" placeholder="Type your last name">
                            <span class="focus-input100" data-symbol="&#xf20e;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Password is required">
                            <span class="label-input100">Password</span>
                            <input class="input100" type="password" name="password" placeholder="Type your password">
                            <span class="focus-input100" data-symbol="&#xf190;"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Password does not match">
                            <span class="label-input100">Confirm Password</span>
                            <input class="input100" type="password" name="re_password" placeholder="conform your password">
                            <span class="focus-input100" data-symbol="&#xf191;"></span>
                        </div>
                    </div>
                </div>

                <div class="container-login100-form-btn p-t-20">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Sign Up
                        </button>
                    </div>
                </div>


                <div class="flex-col-c p-t-23">
						<span class="txt1 p-b-17">
							If you have account already
						</span>

                    <a data-toggle="tab" href="#signin-form" class="txt2">
                        Please Login
                    </a>
                </div>
                <input name="token" value="<?php echo $token;?>" hidden />
            </form>
            <form method="post" role="tabpanel" class="login100-form for-forgot-password validate-form tab-pane fade" id="forgot-password-form">
                <span class="login100-form-title p-b-20">
						Forget Password
					</span>
                <div class="alert alert-danger" role="alert" style="opacity: 0">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Something is wrong
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Username or Email is reauired">
                    <span class="label-input100">Username or Email</span>
                    <input class="input100" type="text" name="username" placeholder="Type your username or email">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Send
                        </button>
                    </div>
                </div>
                <div class="p-t-40 text-center">
                    <a data-toggle="tab" href="#signin-form" class="txt2">
                        Sign In
                    </a> /
                    <a data-toggle="tab" href="#<?php echo $canRegister ? 'signup-form' : 'send-email-form';?>" class="txt2">
                        Sign Up
                    </a>
                </div>
            </form>
            <form method="post" role="tabpanel" class="login100-form for-forgot-password validate-form tab-pane fade <?php echo $type =='rest_pw' ? 'in active' : ''?>" id="rest-password-form">
                <span class="login100-form-title p-b-20">
						Rest Password
					</span>
                <div class="alert alert-danger" role="alert" style="opacity: 0">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Something is wrong
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "password is reauired">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Type your password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "password does not match">
                    <span class="label-input100">Repeat Password</span>
                    <input class="input100" type="password" name="re_password" placeholder="Repeat Password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Rest Password
                        </button>
                    </div>
                </div>
                <input name="temper" value="<?php echo $temper?>" hidden />
            </form>
            <form method="post" role="tabpanel" class="login100-form for-forgot-password validate-form tab-pane fade <?php echo $type =='register' && !$canRegister ? 'in active' : ''?>" id="send-email-form">
                <span class="login100-form-title p-b-20">
						Send Email
					</span>
                <div class="alert alert-danger" role="alert" style="opacity: 0">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Something is wrong
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Email is reauired">
                    <span class="label-input100">Email</span>
                    <input class="input100" type="text" name="email" placeholder="Type your email">
                    <span class="focus-input100" data-symbol="&#xf15a;"></span>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Send
                        </button>
                    </div>
                </div>
                <div class="flex-col-c p-t-73">
						<span class="txt1 p-b-17">
							If you have account already
						</span>
                    <a data-toggle="tab" href="#signin-form" class="txt2">
                        Sign In
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    const CURRENT_PAGE = window.location.pathname.substr(1);
    const BASE_URL = window.location.protocol + "//" + window.location.host;
    var prevPage = <?php echo json_encode($prevPage);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/signIn-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/signIn-page.js?version=<?php echo time();?>"></script>

</body>
</html>