<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 30.05.2018
 * Time: 08:29
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="Create your experience" />
    <meta name="Description" content="Explore your ideas and create experiences that are perfect for you" />
    <title>Home</title>

    <link rel="stylesheet" href="assets/css/yevgeny/createYourExperience-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/yevgeny/createYourExperience-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-createYourExperience">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Create
                </div>
            </div>
            <main class="main-content">
                <h2 class="text-center">What would you like to create?</h2>
                <ul>
                    <?php if ($usersCanEditSeries): ?>
                    <li>
                        <a href="createseries.php">
                            <div class="icon-wrapper">
                                <img src="assets/images/global-icons/new-series.svg">
                            </div>
                            <div class="icon-label">
                                A New Series
                            </div>
                        </a>
                    </li>
                    <?php endif; ?>
                    <!---
                    <li>
                        <a href="createevent">
                            <div class="icon-wrapper">
                                <img src="assets/images/global-icons/new-event.png">
                            </div>
                            <div class="icon-label">
                                A New Event
                            </div>
                        </a>
                    </li>
--->
                    <li>
                        <a href="editideabox">
                            <div class="icon-wrapper">
                                <img src="assets/images/global-icons/new-idea.png">
                            </div>
                            <div class="icon-label">
                                A New Idea
                            </div>
                        </a>
                    </li>
                </ul>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="assets/js/yevgeny/createYourExperience-global.js?version=<?php echo time();?>"></script>

</body>
</html>
