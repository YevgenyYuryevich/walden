<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 04:54
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="Add a new idea, note, or thought to your ideaBox" />
    <meta name="Description" content="Keep your ideas, notes, and thoughts safe in an ideaBox so that they will be available to you later" />
    <title>Add a new idea, note, or thought to your ideaBox</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/editIdeabox-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/editIdeabox-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-create-ideabox">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Create Event
                </div>
            </div>
            <main class="main-content">
                <h2 class="text-center">Your Personal Idea</h2>
                <form role="form" method="post" id="ideabox-form" enctype="multipart/form-data">
                    <ul>
                        <li>
                            <div class="form-group">
                                <label>Name your Ideabox:</label>
                                <input class="form-control" name="title" value="<?php echo $ideabox ? $ideabox['strIdeaBox_title'] : '';?>" required/>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Describe your Idea:</label>
                                <textarea name="description" class="summernote form-control" rows="3" required><?php echo $ideabox ? $ideabox['strIdeaBox_idea'] : '';?></textarea>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Do you want this private?</label>
                                <div class="cd-switch">
                                    <div class="switch">
                                        <input type="radio" name="isPrivate" id="isPublic_yes" value="1" <?php echo !$ideabox || $ideabox['intIdeaBox_private'] == '1' ? 'checked' : '';?>>
                                        <label for="isPublic_yes">Yes</label>
                                        <input type="radio" name="isPrivate" value="0" id="isPublic_no" <?php echo $ideabox && $ideabox['intIdeaBox_private'] == '0' ? 'checked' : '';?>>
                                        <label for="isPublic_no">No</label>
                                        <span class="switchFilter"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="reference-item">
                            <aside>Type Reference Information to remind (optional)</aside>
                            <div class="flex-row vertical-center margin-between space-between flex-wrap">
                                <div class="flex-col fix-col">
                                    <div class="form-group">
                                        <label>Text</label>
                                        <input value="<?php echo $ideabox ? $ideabox['strIdeaBox_reference_text'] : '';?>" name="refText" class="form-control" />
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="form-group">
                                        <label>Link</label>
                                        <input value="<?php echo $ideabox ? $ideabox['strIdeaBox_reference_link'] : '';?>" name="refLink" class="form-control" size="40"/>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Upload Cover Image for this Idea: (optional)</label>
                                <div class="cover-image-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo $ideabox ? $ideabox['strIdeaBox_image'] : '/assets/images/global-icons/cloud-uploading.png';?>" />
                                    </div>
                                    <div>{Drag Image here to upload}</div>
                                    <input class="form-control" type="file" name="cover_img"/>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <footer>
                        <button class="btn btn-circle" type="submit">
                            <?php echo $ideabox ? 'All set! Now Let\'s update' : 'All set! Now let\'s create! >>'; ?>
                        </button>
                    </footer>
                </form>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php  echo BASE_URL;?>/assets/js/yevgeny/editIdeabox-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var parentId = <?php echo $parentId;?>;
    var inital_ideabox = <?php echo json_encode($ideabox);?>;
</script>
<script src="<?php  echo BASE_URL;?>/assets/js/yevgeny/editIdeabox-page.js?version=<?php echo time();?>"></script>

</body>
</html>