<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.03.2018
 * Time: 17:54
 */

$default_event_image = 'assets/images/slide_1.jpg';
$is_first_visit = 0;

?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="Title" content="The daily space for a thoughtful life - thegreyshirt" />
    <meta name="Description" content="Your daily goals and experiences designed to help you live more" />
    <title>Your daily space for a thoughtful life - thegreyshirt</title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL;?>/assets/css/yevgeny/user-global.css?version=<?php echo time();?>">
    <link rel='stylesheet' type="text/css" href="<?php echo BASE_URL;?>/assets/css/yevgeny/user-page.css?version=<?php echo time();?>"/>

</head>

<body>

<div class="site-wrapper page page-userpage">
    <?php require_once 'yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    <div class="inspiration" style="display: block">Today's Inspirations <i class="fa fa-caret-down drop_down" aria-hidden="true"></i></div>
                    <div class="date-range" style="display: none">
                        <i class="fa fa-arrow-left back-to-today" aria-hidden="true"></i>&nbsp;&nbsp;<span class="start-date"></span> - <span class="end-date"></span>
                    </div>
                </div>
            </div>
            <div class="flex-row">
                <div class="flex-col left-col">
                    <main class="main-content">
                        <?php if (!$series): ?>
                            <div class="welcome-item item">
                                <div class="item-data">
                                    <header class="item-header">
                                        Welcom! Let's find daily experiences that are right for you.
                                    </header>
                                    <div class="item-body">
                                        <div class="flex-row">
                                            <div class="flex-col">
                                                <a href="view" class="step-item">
                                                    <div class="item-inner">
                                                        <header>Step 1:</header>
                                                        <div>Learn how it work!</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="flex-col">
                                                <a href="explore" class="step-item">
                                                    <div class="item-inner">
                                                        <header>Step 2:</header>
                                                        <div>Select experiences that are right for you</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="flex-col">
                                                <a href="javascript:;" class="step-item">
                                                    <div class="item-inner">
                                                        <header>Step 3:</header>
                                                        <div>Enjoy!</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="subscription-item item sample" hidden>
                            <div class="item-data">
                                <header class="item-header">
                                    <div class="flex-row">
                                        <div class="flex-col title-col">
                                            <div class="item-title"></div>
                                        </div>
                                        <div class="flex-col complete-col">
                                            <button class="toogle-complete btn btn-block"><i class="glyphicon glyphicon-ok"></i></button>
                                        </div>
                                    </div>
                                </header>
                                <div class="item-body">
                                    <div class="item-decoration-container">
                                        <div class="nav-portion left-portion flex-box vertical-center horizon-center">
                                            <a class="cst-arrow left-arrow prev-post">previous</a>
                                        </div>
                                        <div class="nav-portion right-portion flex-box vertical-center horizon-center">
                                            <a class="cst-arrow right-arrow next-post">next</a>
                                        </div>
                                    </div>
                                    <div class="type-0 subscription-type sample" hidden>
                                        <div class="audiogallery">
                                            <div class="items">
                                                <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="type-2 subscription-type sample" hidden>
                                        <div class="video-wrapper">
                                            <video class="video-js" controls width="960" height="540"></video>
                                        </div>
                                    </div>
                                    <div class="type-menu subscription-type sample" hidden>
                                        <header>
                                            <h3 class="menu-title"></h3>
                                            <p class="menu-description"></p>
                                        </header>
                                        <div class="paths-container">
                                            <div class="flex-col path-col sample" hidden>
                                                <div class="img-wrapper">
                                                    <img />
                                                </div>
                                                <h3 class="path-title"></h3>
                                                <p class="path-description"></p>
                                            </div>
                                            <div class="flex-row paths-list flex-wrap">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="type-8 subscription-type sample" hidden></div>
                                    <div class="type-10 subscription-type sample" hidden>
                                        <div class="item-description"></div>
                                    </div>
                                    <div class="type-other subscription-type sample" hidden>
                                        <div class="item-description"></div>
                                    </div>
                                </div>
                                <footer class="item-footer">
                                    <div class="flex-row">
                                        <div class="flex-col"><a class="view-post" href="javascript:;"><img width="28px" src="assets/images/read.png"></a></div>
                                        <div class="flex-col"><a href="javascript:;" class="make-favorite ">
                                                <i style="color: grey;" class="un-favorite fa fa-star-o fa-2x" aria-hidden="true"></i>
                                                <span class="favorite-wrapper"><i style="color: yellow; " class="fa fa-star fa-2x" aria-hidden="true"></i><i style="color: #ffd800; position: absolute;" class="un-favorite fa fa-star-o fa-2x" aria-hidden="true"></i></span></a>
                                        </div>
                                        <div class="flex-col"><a href="javascript:;" class="open-switch-page"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></a></div>
                                        <div class="flex-col"><a class="open-calendar" target="_blank" href="javascript:;">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="32.75px" height="32.75px" viewBox="0 0 32.75 32.75" style="enable-background:new 0 0 32.75 32.75;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M29.375,1.25h-1.124c0.028-0.093,0.058-0.186,0.058-0.289C28.309,0.43,27.878,0,27.348,0
                                                                c-0.531,0-0.961,0.431-0.961,0.961c0,0.103,0.028,0.196,0.059,0.289h-3.68c0.029-0.093,0.058-0.186,0.058-0.289
                                                                C22.823,0.43,22.393,0,21.861,0C21.331,0,20.9,0.431,20.9,0.961c0,0.103,0.029,0.196,0.059,0.289h-3.68
                                                                c0.029-0.093,0.058-0.186,0.058-0.289C17.337,0.43,16.906,0,16.376,0c-0.53,0-0.961,0.431-0.961,0.961
                                                                c0,0.103,0.029,0.196,0.058,0.289h-3.68c0.029-0.093,0.058-0.186,0.058-0.289C11.851,0.43,11.42,0,10.89,0
                                                                c-0.531,0-0.961,0.431-0.961,0.961c0,0.103,0.028,0.196,0.058,0.289h-3.68c0.03-0.093,0.058-0.186,0.058-0.289
                                                                C6.365,0.43,5.935,0,5.404,0C4.873,0,4.443,0.431,4.443,0.961c0,0.103,0.029,0.196,0.058,0.289H3.375
                                                                c-1.517,0-2.75,1.233-2.75,2.75v26c0,1.518,1.233,2.75,2.75,2.75H26.27l5.855-5.855V4C32.125,2.483,30.893,1.25,29.375,1.25z
                                                                 M3.375,31.25c-0.689,0-1.25-0.561-1.25-1.25V9h28.5v17.273l-0.311,0.311h-2.356c-1.101,0-2,0.9-2,2v2.355l-0.31,0.311H3.375z"/>
                                                            <path d="M19.52,12.536l-1.136,1.232l-0.692,0.749l-7.615,8.252l-1.058,3.662l-0.7,2.422l2.356-0.893l3.565-1.352l7.615-8.25
                                                                l0.691-0.75l1.135-1.234c1.062-1.148,0.989-2.941-0.16-4.002C22.371,11.314,20.58,11.387,19.52,12.536z M10.294,27.609
                                                                l-0.897-0.827l1.011-3.496l0.269,0.039l2.919,2.692l0.086,0.311L10.294,27.609z M22.204,17.293l-3.481-3.213l0.822-0.892
                                                                l3.482,3.212L22.204,17.293z"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a></div>
                                        <div class="flex-col"><a class="trash-post"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></a></div>
                                        <div class="flex-col">
                                            <a href="javascript:;" class="share-post">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
                                                    <g>
                                                        <path d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M30,58C14.561,58,2,45.439,2,30   S14.561,2,30,2s28,12.561,28,28S45.439,58,30,58z" fill="#006DF0"></path>
                                                        <path d="M39,20c3.309,0,6-2.691,6-6s-2.691-6-6-6c-3.131,0-5.705,2.411-5.973,5.474L18.961,23.788C18.086,23.289,17.077,23,16,23   c-3.309,0-6,2.691-6,6s2.691,6,6,6c1.077,0,2.086-0.289,2.961-0.788l14.065,10.314C33.295,47.589,35.869,50,39,50   c3.309,0,6-2.691,6-6s-2.691-6-6-6c-2.69,0-4.972,1.78-5.731,4.223l-12.716-9.325C21.452,31.848,22,30.488,22,29   s-0.548-2.848-1.448-3.898l12.716-9.325C34.028,18.22,36.31,20,39,20z M39,10c2.206,0,4,1.794,4,4s-1.794,4-4,4s-4-1.794-4-4   S36.794,10,39,10z M12,29c0-2.206,1.794-4,4-4s4,1.794,4,4s-1.794,4-4,4S12,31.206,12,29z M39,40c2.206,0,4,1.794,4,4s-1.794,4-4,4   s-4-1.794-4-4S36.794,40,39,40z" fill="#006DF0"></path>
                                                    </g></svg>
                                            </a>
                                        </div>
                                    </div>
                                </footer>
                                <aside class="item-meta">
                                    <nav class="circle-nav-wrapper">
                                        <div class="circle-nav-toggle">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                        <g>
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"/>
                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"/>
                        </g>
                    </svg>
                                        </div>
                                        <div class="circle-nav-panel"></div>
                                        <ul class="circle-nav-menu">
                                            <li class="circle-nav-item circle-nav-item-1">
                                                <a class="facebook-link" href="https://www.facebook.com/sharer/sharer.php?u=test" target="_blank">
                                                    <svg version="1.1" id="facebook-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="30px" height="30px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;"
                                                         xml:space="preserve">
                                    <g><path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803
                                            c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654
                                            c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246
                                            c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"/>
                                    </g>
                                </svg><span>Facebook</span>
                                                </a>
                                            </li>
                                            <li class="circle-nav-item circle-nav-item-2">
                                                <a class="twitter-link" href="https://twitter.com/intent/tweet?url=text" target="_blank">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512.002px" height="512.002px" viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;"xml:space="preserve">
                                    <g>
                                        <path d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115
                                            c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039
                                            c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809
                                            c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321
                                            c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888
                                            c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471
                                            c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59
                                            C480.111,136.775,497.92,118.275,512.002,97.211z"/>
                                    </g>
                                </svg><span>Twitter</span>
                                                </a>
                                            </li>
                                            <li class="circle-nav-item circle-nav-item-3">
                                                <a class="linkedin-link" href="https://www.linkedin.com/shareArticle?mini=true&url=test" target="_blank">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="430.117px" height="430.117px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
                                                         xml:space="preserve">
                                    <g>
                                        <path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
                                        c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
                                        v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
                                        C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
                                        c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
                                         M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
                                    </g></svg><span>Linkedlin</span>
                                                </a>
                                            </li>
                                            <li class="circle-nav-item circle-nav-item-4">
                                                <a class="google-link" href="https://plus.google.com/share?url=test" target="_blank">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="96.828px" height="96.827px" viewBox="0 0 96.828 96.827" style="enable-background:new 0 0 96.828 96.827;"
                                                         xml:space="preserve">
                                        <g>
                                            <path d="M62.617,0H39.525c-10.29,0-17.413,2.256-23.824,7.552c-5.042,4.35-8.051,10.672-8.051,16.912
                                                c0,9.614,7.33,19.831,20.913,19.831c1.306,0,2.752-0.134,4.028-0.253l-0.188,0.457c-0.546,1.308-1.063,2.542-1.063,4.468
                                                c0,3.75,1.809,6.063,3.558,8.298l0.22,0.283l-0.391,0.027c-5.609,0.384-16.049,1.1-23.675,5.787
                                                c-9.007,5.355-9.707,13.145-9.707,15.404c0,8.988,8.376,18.06,27.09,18.06c21.76,0,33.146-12.005,33.146-23.863
                                                c0.002-8.771-5.141-13.101-10.6-17.698l-4.605-3.582c-1.423-1.179-3.195-2.646-3.195-5.364c0-2.672,1.772-4.436,3.336-5.992
                                                l0.163-0.165c4.973-3.917,10.609-8.358,10.609-17.964c0-9.658-6.035-14.649-8.937-17.048h7.663c0.094,0,0.188-0.026,0.266-0.077
                                                l6.601-4.15c0.188-0.119,0.276-0.348,0.214-0.562C63.037,0.147,62.839,0,62.617,0z M34.614,91.535
                                                c-13.264,0-22.176-6.195-22.176-15.416c0-6.021,3.645-10.396,10.824-12.997c5.749-1.935,13.17-2.031,13.244-2.031
                                                c1.257,0,1.889,0,2.893,0.126c9.281,6.605,13.743,10.073,13.743,16.678C53.141,86.309,46.041,91.535,34.614,91.535z
                                                 M34.489,40.756c-11.132,0-15.752-14.633-15.752-22.468c0-3.984,0.906-7.042,2.77-9.351c2.023-2.531,5.487-4.166,8.825-4.166
                                                c10.221,0,15.873,13.738,15.873,23.233c0,1.498,0,6.055-3.148,9.22C40.94,39.337,37.497,40.756,34.489,40.756z"/>
                                            <path d="M94.982,45.223H82.814V33.098c0-0.276-0.225-0.5-0.5-0.5H77.08c-0.276,0-0.5,0.224-0.5,0.5v12.125H64.473
                                                c-0.276,0-0.5,0.224-0.5,0.5v5.304c0,0.275,0.224,0.5,0.5,0.5H76.58V63.73c0,0.275,0.224,0.5,0.5,0.5h5.234
                                                c0.275,0,0.5-0.225,0.5-0.5V51.525h12.168c0.276,0,0.5-0.223,0.5-0.5v-5.302C95.482,45.446,95.259,45.223,94.982,45.223z"/>
                                        </g></svg><span>Google</span>
                                                </a>
                                            </li>
                                            <li class="circle-nav-item circle-nav-item-5">
                                                <a class="mail-link" href="mailto:nbaca@mtnlegal.com?&subject=welcome">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" style="enable-background:new 0 0 79.536 79.536;"
                                                         xml:space="preserve">
                                    <g>
                                        <path style="fill:#010002;" d="M39.773,1.31L0,31.004v47.222h79.536V31.004L39.773,1.31z M28.77,22.499
                                        c1.167-2.133,2.775-3.739,4.815-4.805c2.035-1.075,4.357-1.616,6.983-1.616c2.214,0,4.191,0.435,5.921,1.292
                                        c1.729,0.87,3.045,2.094,3.967,3.687c0.9,1.595,1.367,3.334,1.367,5.217c0,2.247-0.694,4.279-2.082,6.097
                                        c-1.74,2.292-3.961,3.436-6.68,3.436c-0.732,0-1.279-0.122-1.654-0.38c-0.365-0.262-0.621-0.632-0.743-1.129
                                        c-1.022,1.012-2.231,1.52-3.589,1.52c-1.465,0-2.679-0.507-3.643-1.509c-0.966-1.012-1.447-2.361-1.447-4.031
                                        c0-2.084,0.578-3.966,1.743-5.672c1.416-2.084,3.218-3.13,5.424-3.13c1.571,0,2.731,0.601,3.475,1.805l0.331-1.468h3.5
                                        l-1.998,9.479c-0.125,0.606-0.187,0.986-0.187,1.163c0,0.228,0.052,0.38,0.149,0.497c0.099,0.111,0.223,0.165,0.357,0.165
                                        c0.436,0,0.979-0.248,1.646-0.769c0.901-0.663,1.627-1.574,2.181-2.695c0.554-1.129,0.839-2.299,0.839-3.508
                                        c0-2.165-0.782-3.977-2.352-5.445c-1.573-1.45-3.77-2.185-6.578-2.185c-2.393,0-4.417,0.487-6.077,1.468
                                        c-1.66,0.966-2.913,2.343-3.765,4.114c-0.839,1.76-1.258,3.607-1.258,5.52c0,1.856,0.479,3.552,1.411,5.074
                                        c0.945,1.533,2.26,2.641,3.956,3.345c1.696,0.697,3.643,1.046,5.828,1.046c2.097,0,3.909-0.293,5.432-0.881
                                        c1.522-0.587,2.739-1.457,3.666-2.641h2.807c-0.88,1.792-2.227,3.192-4.049,4.215c-2.092,1.163-4.64,1.74-7.644,1.74
                                        c-2.918,0-5.426-0.487-7.542-1.468c-2.121-0.986-3.689-2.434-4.73-4.35c-1.028-1.918-1.535-4.008-1.535-6.268
                                        C27.017,26.952,27.595,24.64,28.77,22.499z M2.804,31.941l29.344,19.68L2.804,74.333V31.941z M5.033,75.844l34.74-26.885
                                        l34.729,26.885H5.033z M76.729,74.333L47.391,51.621l29.339-19.68V74.333z M41.205,24.661c0.466,0.531,0.699,1.295,0.699,2.292
                                        c0,0.891-0.174,1.856-0.513,2.879c-0.334,1.036-0.743,1.826-1.209,2.361c-0.318,0.375-0.658,0.652-0.992,0.826
                                        c-0.439,0.249-0.906,0.37-1.41,0.37c-0.674,0.006-1.23-0.264-1.691-0.794c-0.45-0.531-0.673-1.346-0.673-2.465
                                        c0-0.839,0.158-1.805,0.487-2.889c0.329-1.088,0.81-1.916,1.453-2.509c0.647-0.588,1.346-0.881,2.1-0.881
                                        C40.162,23.856,40.749,24.125,41.205,24.661z"/>
                                    </g></svg><span>Opened-Email-Envelope</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </aside>
                            </div>
                        </div>
                        <div class="event-item item sample" hidden>
                            <img class="item-img" src="<?php echo $default_event_image;?>">
                            <div class="item-data">
                                <header class="item-header">
                                    <span class="event-title">event title</span> &nbsp;&nbsp;&nbsp; <span class="start-datetime"></span>
                                </header>
                                <div class="item-body">
                                    <div class="event-description">event description</div>
                                    <div class="event-location"></div>
                                </div>
                                <footer class="item-footer">
                                    <div class="flex-row">
                                        <div class="flex-col"><a href="javascript:;"><img width="28px" src="assets/images/read.png"></a></div>
                                        <div class="flex-col"><a href="javascript:;" class="make-favorite">
                                                <i style="color: grey;" class="un-favorite fa fa-star-o fa-2x" aria-hidden="true"></i>
                                                <span class="favorite-wrapper"><i style="color: yellow; " class="fa fa-star fa-2x" aria-hidden="true"></i><i style="color: #ffd800; position: absolute;" class="un-favorite fa fa-star-o fa-2x" aria-hidden="true"></i></span>
                                            </a></div>
                                        <div class="flex-col"><a href="javascript:;" class="open-switch-lightbox"><i style="color: grey; " class="fa fa-exchange fa-2x" aria-hidden="true"></i></a></div>
                                        <div class="flex-col"><a class="trash-post"><i style="color: grey; " class="fa fa-trash-o fa-2x" aria-hidden="true"></i></a></div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="amazon-item item sample" hidden>
                            <div class="item-data">
                                <header class="item-header"></header>
                                <div class="product-basic-description-wrapper">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-12">
                                            <div class="image-wrapper">
                                                <img src="" />
                                            </div>
                                        </div>
                                        <div class="col-md-10 col-sm-12">
                                            <div class="description-wrapper">
                                                <label>Description</label>
                                                <div class="product-description"></div>
                                                <div class="view-more-wrapper">
                                                    <a class="view-more" href="javascript:;">view more ...</a>
                                                    <a class="view-less" href="javascript:;">view less</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prices-wrapper">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="new-price-wrapper price-item active">
                                                <div class="item-name">new price</div>
                                                <div class="item-value"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="used-price-wrapper price-item">
                                                <div class="item-name">used price</div>
                                                <div class="item-value"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="new-quantity-wrapper price-item">
                                                <div class="item-name">new quantity</div>
                                                <div class="item-value"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="used-quantity-wrapper price-item">
                                                <div class="item-name">used quantity</div>
                                                <div class="item-value"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="value-history">Price History</div>
                                <div class="chart-root"></div>
                                <footer class="item-footer amazon-link-wrapper">
                                    <a href="#" target="_blank">LINK TO PRODUCT</a>
                                </footer>
                            </div>
                        </div>
                        <div class="goals-init item" hidden>
                            <div class="item-data">
                                <header class="item-header">Today, I will</header>
                                <div class="item-body">
                                    <form method="post" class="step-form step-1">
                                        <label for="grate-init">Step 1:</label>
                                        <div class="flex-row">
                                            <div class="flex-col">
                                                <textarea id="grate-init" name="grate_for" rows="1" placeholder="be grateful for..." required></textarea>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <button class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <form method="post" class="step-form step-2">
                                        <label for="grate-init">Step 2:</label>
                                        <div class="flex-row">
                                            <div class="flex-col">
                                                <textarea id="goals-init" name="goals_of" rows="1" placeholder="have the goal of..." required></textarea>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <button class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <form method="post" class="step-form step-3">
                                        <label for="grate-init">Step 3:</label>
                                        <div class="flex-row">
                                            <div class="flex-col">
                                                <textarea id="trap-init" name="trap_not" rows="1" placeholder="not fall into the trap of..." required></textarea>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <button class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="goals-main item">
                            <div class="item-data">
                                <section class="goals-block grate-goals-wrapper" hidden>
                                    <header class="item-header">
                                        <span class="display-date">Today</span>,
                                        <span class="display-title">I will be grateful for...</span>
                                    </header>
                                    <div class="item-body">
                                        <li class="sample goal-item" hidden>
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col fix-col">
                                                    <a href="javascript:;" class="delete-goal">×</a>
                                                </div>
                                                <div class="flex-col fix-col goal-number-col">
                                                    Goal <span class="goal-number"></span>:
                                                </div>
                                                <div class="flex-col">
                                                    <div class="goal-text-wrapper">
                                                        <div class="for-view">
                                                            <span class="goal-text"></span>
                                                            <a href="javascript:;" class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></a>
                                                            <a href="javascript:;" class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></a>
                                                        </div>
                                                        <div class="for-edit">
                                                            <form class="update-form" method="post">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col">
                                                                        <textarea name="goal_text" rows="1" required></textarea>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <button type="submit" class="save-entry"><i class="glyphicon glyphicon-check"></i></button>
                                                                        <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <div class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="for-tomorrow-wrapper">
                                                        <span>tomorrow</span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 59.99 59.99" style="enable-background:new 0 0 59.99 59.99;" xml:space="preserve" width="45px" height="45px">
                                                            <g>
                                                                <path d="M1.005,31c0.552,0,1-0.447,1-1c0-15.439,12.561-28,28-28c6.327,0,12.378,2.115,17.302,6h-5.302c-0.552,0-1,0.447-1,1   s0.448,1,1,1h7.915c0.066,0.006,0.128,0.007,0.193,0h0.891V1c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.81   C43.648,2.408,36.986,0,30.005,0c-16.542,0-30,13.458-30,30C0.005,30.553,0.453,31,1.005,31z" fill="#818181"/>
                                                                <path d="M30.005,8c-12.131,0-22,9.869-22,22s9.869,22,22,22s22-9.869,22-22S42.136,8,30.005,8z M30.005,50   c-11.028,0-20-8.972-20-20s8.972-20,20-20s20,8.972,20,20S41.033,50,30.005,50z" fill="#818181"/>
                                                                <path d="M30.005,15c0.552,0,1-0.447,1-1v-2c0-0.553-0.448-1-1-1s-1,0.447-1,1v2C29.005,14.553,29.453,15,30.005,15z" fill="#818181"/>
                                                                <path d="M30.005,45c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C31.005,45.447,30.557,45,30.005,45z" fill="#818181"/>
                                                                <path d="M46.005,31h2c0.552,0,1-0.447,1-1s-0.448-1-1-1h-2c-0.552,0-1,0.447-1,1S45.453,31,46.005,31z" fill="#818181"/>
                                                                <path d="M14.005,29h-2c-0.552,0-1,0.447-1,1s0.448,1,1,1h2c0.552,0,1-0.447,1-1S14.557,29,14.005,29z" fill="#818181"/>
                                                                <path d="M31.005,25.142V20c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.142c-1.72,0.447-3,1.999-3,3.858c0,0.74,0.215,1.424,0.567,2.019   l-8.274,8.274c-0.391,0.391-0.391,1.023,0,1.414C18.493,40.902,18.749,41,19.005,41s0.512-0.098,0.707-0.293l8.274-8.274   C28.581,32.785,29.265,33,30.005,33c2.206,0,4-1.794,4-4C34.005,27.141,32.725,25.589,31.005,25.142z M30.005,31   c-1.103,0-2-0.897-2-2s0.897-2,2-2s2,0.897,2,2S31.108,31,30.005,31z" fill="#818181"/>
                                                                <path d="M11.894,52.66c-0.33,0.439-0.24,1.069,0.21,1.399c0.17,0.13,0.38,0.19,0.59,0.19c0.31,0,0.61-0.13,0.81-0.4   c0.32-0.439,0.23-1.069-0.21-1.399C12.854,52.12,12.225,52.21,11.894,52.66z" fill="#818181"/>
                                                                <path d="M7.235,46.27c-0.32-0.439-0.95-0.55-1.4-0.229c-0.45,0.33-0.55,0.95-0.23,1.399c0.2,0.271,0.5,0.42,0.82,0.42   c0.2,0,0.4-0.06,0.58-0.189C7.454,47.35,7.555,46.72,7.235,46.27z" fill="#818181"/>
                                                                <path d="M7.624,48.45c-0.41,0.369-0.45,1-0.08,1.409c0.19,0.23,0.47,0.341,0.75,0.341c0.23,0,0.47-0.08,0.66-0.25   c0.41-0.37,0.45-1,0.08-1.41C8.675,48.12,8.045,48.09,7.624,48.45z" fill="#818181"/>
                                                                <path d="M11.065,50.6c-0.4-0.37-1.04-0.34-1.41,0.061c-0.38,0.409-0.35,1.04,0.06,1.42c0.19,0.17,0.43,0.26,0.67,0.26   c0.27,0,0.54-0.11,0.74-0.32c0.37-0.41,0.35-1.04-0.06-1.41C11.065,50.6,11.065,50.6,11.065,50.6z" fill="#818181"/>
                                                                <path d="M5.675,43.84c-0.01,0-0.01,0-0.01,0c-0.27-0.48-0.88-0.65-1.36-0.37c-0.48,0.271-0.65,0.88-0.37,1.36   c0.18,0.319,0.52,0.5,0.87,0.5c0.16,0,0.34-0.04,0.49-0.13C5.775,44.93,5.945,44.319,5.675,43.84z" fill="#818181"/>
                                                                <path d="M58.945,32.12c0.02,0,0.03,0,0.04,0c0.54,0,0.98-0.431,1-0.96c0.02-0.561-0.41-1.021-0.96-1.04   c-0.55-0.021-1.02,0.41-1.04,0.96S58.394,32.1,58.945,32.12z" fill="#818181"/>
                                                                <path d="M1.265,33.99c0.55-0.061,0.95-0.551,0.89-1.101s-0.55-0.95-1.1-0.89c-0.55,0.05-0.95,0.55-0.89,1.09   c0.05,0.52,0.49,0.9,0.99,0.9C1.195,33.99,1.225,33.99,1.265,33.99z" fill="#818181"/>
                                                                <path d="M1.825,36.93c0.54-0.11,0.89-0.64,0.77-1.18c-0.11-0.54-0.64-0.891-1.18-0.771c-0.54,0.11-0.89,0.641-0.77,1.181   c0.09,0.47,0.51,0.79,0.97,0.79C1.684,36.95,1.755,36.95,1.825,36.93z" fill="#818181"/>
                                                                <path d="M4.365,41.25c-0.22-0.5-0.81-0.73-1.32-0.51c-0.5,0.22-0.73,0.81-0.51,1.319c0.17,0.37,0.53,0.601,0.92,0.601   c0.13,0,0.27-0.03,0.4-0.091C4.354,42.35,4.584,41.76,4.365,41.25z" fill="#818181"/>
                                                                <path d="M3.334,38.55c-0.16-0.53-0.73-0.82-1.25-0.65c-0.53,0.17-0.82,0.73-0.65,1.261c0.14,0.42,0.53,0.689,0.95,0.689   c0.1,0,0.21-0.01,0.31-0.04C3.215,39.64,3.505,39.069,3.334,38.55z" fill="#818181"/>
                                                                <path d="M29.465,57.99c-0.55-0.011-1.01,0.43-1.02,0.979c-0.01,0.561,0.43,1.01,0.98,1.021c0.01,0,0.02,0,0.02,0   c0.55,0,0.99-0.431,1-0.98C30.454,58.46,30.015,58,29.465,57.99z" fill="#818181"/>
                                                                <path d="M52.135,47.14c-0.34,0.43-0.26,1.061,0.17,1.4c0.19,0.14,0.4,0.21,0.62,0.21c0.29,0,0.59-0.13,0.79-0.391   c0.34-0.43,0.26-1.06-0.18-1.399S52.475,46.7,52.135,47.14z" fill="#818181"/>
                                                                <path d="M45.834,53.08c-0.45,0.31-0.57,0.939-0.25,1.39c0.19,0.28,0.5,0.43,0.82,0.43c0.2,0,0.39-0.05,0.57-0.17   c0.45-0.31,0.57-0.939,0.25-1.39C46.914,52.88,46.295,52.77,45.834,53.08z" fill="#818181"/>
                                                                <path d="M48.135,51.319c-0.42,0.36-0.47,0.99-0.11,1.41c0.2,0.23,0.48,0.351,0.76,0.351c0.23,0,0.46-0.08,0.65-0.24   c0.42-0.36,0.47-0.99,0.11-1.41C49.184,51.01,48.555,50.96,48.135,51.319z" fill="#818181"/>
                                                                <path d="M55.164,44.439c-0.47-0.29-1.09-0.14-1.38,0.32c-0.29,0.47-0.15,1.09,0.32,1.38c0.17,0.101,0.35,0.15,0.53,0.15   c0.33,0,0.66-0.16,0.85-0.471C55.775,45.35,55.635,44.729,55.164,44.439z" fill="#818181"/>
                                                                <path d="M50.245,49.33c-0.39,0.399-0.37,1.029,0.03,1.41c0.19,0.189,0.44,0.279,0.69,0.279c0.26,0,0.53-0.1,0.72-0.31   c0.38-0.4,0.37-1.03-0.03-1.41S50.624,48.93,50.245,49.33z" fill="#818181"/>
                                                                <path d="M58.854,33.109c-0.54-0.079-1.05,0.311-1.13,0.851c-0.08,0.55,0.3,1.05,0.85,1.13c0.05,0.01,0.1,0.01,0.14,0.01   c0.49,0,0.92-0.359,0.99-0.859C59.785,33.7,59.405,33.189,58.854,33.109z" fill="#818181"/>
                                                                <path d="M58.385,36.069c-0.54-0.13-1.08,0.2-1.22,0.73c-0.13,0.54,0.2,1.08,0.73,1.22c0.08,0.021,0.16,0.03,0.24,0.03   c0.45,0,0.86-0.31,0.97-0.76C59.245,36.75,58.914,36.21,58.385,36.069z" fill="#818181"/>
                                                                <path d="M15.704,54.06c-0.48-0.279-1.09-0.13-1.37,0.351c-0.29,0.47-0.13,1.09,0.34,1.37c0.16,0.1,0.34,0.14,0.51,0.14   c0.34,0,0.67-0.17,0.86-0.49C16.334,54.96,16.175,54.35,15.704,54.06z" fill="#818181"/>
                                                                <path d="M57.604,38.97c-0.52-0.189-1.1,0.08-1.28,0.6c-0.19,0.521,0.07,1.091,0.59,1.28c0.11,0.04,0.23,0.061,0.34,0.061   c0.41,0,0.8-0.25,0.94-0.66C58.385,39.729,58.124,39.16,57.604,38.97z" fill="#818181"/>
                                                                <path d="M56.525,41.77c-0.49-0.239-1.1-0.04-1.34,0.46c-0.24,0.5-0.03,1.101,0.46,1.34c0.14,0.07,0.29,0.101,0.44,0.101   c0.37,0,0.73-0.21,0.9-0.561C57.225,42.609,57.025,42.01,56.525,41.77z" fill="#818181"/>
                                                                <path d="M26.575,57.79c-0.55-0.07-1.04,0.319-1.11,0.87c-0.07,0.55,0.32,1.05,0.87,1.109v0.011c0.04,0,0.08,0,0.12,0   c0.5,0,0.93-0.37,0.99-0.881C27.515,58.35,27.124,57.859,26.575,57.79z" fill="#818181"/>
                                                                <path d="M20.945,56.5c-0.52-0.181-1.09,0.09-1.27,0.62c-0.18,0.52,0.1,1.09,0.62,1.27c0.11,0.03,0.22,0.05,0.33,0.05   c0.41,0,0.8-0.26,0.94-0.67C21.745,57.24,21.465,56.67,20.945,56.5z" fill="#818181"/>
                                                                <path d="M23.725,57.29c-0.54-0.12-1.07,0.21-1.2,0.75c-0.12,0.54,0.22,1.08,0.75,1.2c0.08,0.02,0.15,0.02,0.23,0.02   c0.45,0,0.87-0.31,0.97-0.77C24.604,57.95,24.265,57.41,23.725,57.29z" fill="#818181"/>
                                                                <path d="M18.265,55.42c-0.51-0.24-1.1-0.021-1.33,0.479s-0.02,1.101,0.48,1.33c0.14,0.061,0.28,0.09,0.42,0.09   c0.38,0,0.74-0.21,0.91-0.579C18.975,56.24,18.765,55.649,18.265,55.42z" fill="#818181"/>
                                                                <path d="M35.225,57.51c-0.54,0.1-0.9,0.63-0.8,1.17c0.09,0.48,0.52,0.811,0.99,0.811c0.06,0,0.12,0,0.18-0.011   c0.54-0.109,0.9-0.63,0.8-1.17C36.295,57.76,35.765,57.41,35.225,57.51z" fill="#818181"/>
                                                                <path d="M38.035,56.83c-0.53,0.149-0.83,0.71-0.67,1.239c0.13,0.431,0.53,0.711,0.96,0.711c0.09,0,0.19-0.011,0.29-0.04   c0.53-0.16,0.83-0.721,0.67-1.25C39.124,56.97,38.565,56.67,38.035,56.83z" fill="#818181"/>
                                                                <path d="M40.765,55.85c-0.51,0.21-0.75,0.8-0.54,1.311c0.16,0.38,0.53,0.609,0.92,0.609c0.13,0,0.26-0.02,0.39-0.069   c0.51-0.221,0.75-0.801,0.54-1.311C41.854,55.88,41.275,55.64,40.765,55.85z" fill="#818181"/>
                                                                <path d="M32.354,57.899c-0.55,0.051-0.96,0.53-0.91,1.08c0.04,0.521,0.48,0.92,1,0.92c0.02,0,0.05,0,0.08,0v-0.01   c0.55-0.04,0.96-0.52,0.91-1.07C33.394,58.27,32.905,57.859,32.354,57.899z" fill="#818181"/>
                                                                <path d="M43.374,54.6c-0.49,0.26-0.67,0.87-0.4,1.351c0.18,0.34,0.52,0.529,0.88,0.529c0.16,0,0.32-0.04,0.47-0.13   c0.49-0.26,0.67-0.87,0.4-1.35C44.465,54.51,43.854,54.33,43.374,54.6z" fill="#818181"/>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="goal-complete-wrapper">
                                                        <span>complete</span>
                                                        <button class="toogle-complete btn"><i class="glyphicon glyphicon-ok"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <ul class="item-list">
                                        </ul>
                                    </div>
                                    <footer>
                                        <form method="post" class="add-goal-form">
                                            <div class="flex-row">
                                                <div class="flex-col">
                                                    <textarea name="goal_text" rows="1" placeholder="add another..." required></textarea>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <button type="submit" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="goals-process">
                                            <ul class="progress-indicator">
                                            </ul>
                                        </div>
                                    </footer>
                                </section>
                                <section class="goals-block normal-goals-wrapper">
                                    <header class="item-header">
                                        <span class="display-date">Today</span>,
                                        <span class="display-title">I will...</span>
                                    </header>
                                    <div class="item-body">
                                        <li class="sample goal-item" hidden>
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col fix-col">
                                                    <a href="javascript:;" class="delete-goal">×</a>
                                                </div>
                                                <div class="flex-col fix-col goal-number-col">
                                                    Goal <span class="goal-number"></span>:
                                                </div>
                                                <div class="flex-col">
                                                    <div class="goal-text-wrapper">
                                                        <div class="for-view">
                                                            <span class="goal-text"></span>
                                                            <a href="javascript:;" class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></a>
                                                            <a href="javascript:;" class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></a>
                                                        </div>
                                                        <div class="for-edit">
                                                            <form class="update-form" method="post">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col">
                                                                        <textarea name="goal_text" rows="1" required></textarea>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <button type="submit" class="save-entry"><i class="glyphicon glyphicon-check"></i></button>
                                                                        <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <div class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="for-tomorrow-wrapper">
                                                        <span>tomorrow</span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 59.99 59.99" style="enable-background:new 0 0 59.99 59.99;" xml:space="preserve" width="45px" height="45px">
                                                            <g>
                                                                <path d="M1.005,31c0.552,0,1-0.447,1-1c0-15.439,12.561-28,28-28c6.327,0,12.378,2.115,17.302,6h-5.302c-0.552,0-1,0.447-1,1   s0.448,1,1,1h7.915c0.066,0.006,0.128,0.007,0.193,0h0.891V1c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.81   C43.648,2.408,36.986,0,30.005,0c-16.542,0-30,13.458-30,30C0.005,30.553,0.453,31,1.005,31z" fill="#818181"/>
                                                                <path d="M30.005,8c-12.131,0-22,9.869-22,22s9.869,22,22,22s22-9.869,22-22S42.136,8,30.005,8z M30.005,50   c-11.028,0-20-8.972-20-20s8.972-20,20-20s20,8.972,20,20S41.033,50,30.005,50z" fill="#818181"/>
                                                                <path d="M30.005,15c0.552,0,1-0.447,1-1v-2c0-0.553-0.448-1-1-1s-1,0.447-1,1v2C29.005,14.553,29.453,15,30.005,15z" fill="#818181"/>
                                                                <path d="M30.005,45c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C31.005,45.447,30.557,45,30.005,45z" fill="#818181"/>
                                                                <path d="M46.005,31h2c0.552,0,1-0.447,1-1s-0.448-1-1-1h-2c-0.552,0-1,0.447-1,1S45.453,31,46.005,31z" fill="#818181"/>
                                                                <path d="M14.005,29h-2c-0.552,0-1,0.447-1,1s0.448,1,1,1h2c0.552,0,1-0.447,1-1S14.557,29,14.005,29z" fill="#818181"/>
                                                                <path d="M31.005,25.142V20c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.142c-1.72,0.447-3,1.999-3,3.858c0,0.74,0.215,1.424,0.567,2.019   l-8.274,8.274c-0.391,0.391-0.391,1.023,0,1.414C18.493,40.902,18.749,41,19.005,41s0.512-0.098,0.707-0.293l8.274-8.274   C28.581,32.785,29.265,33,30.005,33c2.206,0,4-1.794,4-4C34.005,27.141,32.725,25.589,31.005,25.142z M30.005,31   c-1.103,0-2-0.897-2-2s0.897-2,2-2s2,0.897,2,2S31.108,31,30.005,31z" fill="#818181"/>
                                                                <path d="M11.894,52.66c-0.33,0.439-0.24,1.069,0.21,1.399c0.17,0.13,0.38,0.19,0.59,0.19c0.31,0,0.61-0.13,0.81-0.4   c0.32-0.439,0.23-1.069-0.21-1.399C12.854,52.12,12.225,52.21,11.894,52.66z" fill="#818181"/>
                                                                <path d="M7.235,46.27c-0.32-0.439-0.95-0.55-1.4-0.229c-0.45,0.33-0.55,0.95-0.23,1.399c0.2,0.271,0.5,0.42,0.82,0.42   c0.2,0,0.4-0.06,0.58-0.189C7.454,47.35,7.555,46.72,7.235,46.27z" fill="#818181"/>
                                                                <path d="M7.624,48.45c-0.41,0.369-0.45,1-0.08,1.409c0.19,0.23,0.47,0.341,0.75,0.341c0.23,0,0.47-0.08,0.66-0.25   c0.41-0.37,0.45-1,0.08-1.41C8.675,48.12,8.045,48.09,7.624,48.45z" fill="#818181"/>
                                                                <path d="M11.065,50.6c-0.4-0.37-1.04-0.34-1.41,0.061c-0.38,0.409-0.35,1.04,0.06,1.42c0.19,0.17,0.43,0.26,0.67,0.26   c0.27,0,0.54-0.11,0.74-0.32c0.37-0.41,0.35-1.04-0.06-1.41C11.065,50.6,11.065,50.6,11.065,50.6z" fill="#818181"/>
                                                                <path d="M5.675,43.84c-0.01,0-0.01,0-0.01,0c-0.27-0.48-0.88-0.65-1.36-0.37c-0.48,0.271-0.65,0.88-0.37,1.36   c0.18,0.319,0.52,0.5,0.87,0.5c0.16,0,0.34-0.04,0.49-0.13C5.775,44.93,5.945,44.319,5.675,43.84z" fill="#818181"/>
                                                                <path d="M58.945,32.12c0.02,0,0.03,0,0.04,0c0.54,0,0.98-0.431,1-0.96c0.02-0.561-0.41-1.021-0.96-1.04   c-0.55-0.021-1.02,0.41-1.04,0.96S58.394,32.1,58.945,32.12z" fill="#818181"/>
                                                                <path d="M1.265,33.99c0.55-0.061,0.95-0.551,0.89-1.101s-0.55-0.95-1.1-0.89c-0.55,0.05-0.95,0.55-0.89,1.09   c0.05,0.52,0.49,0.9,0.99,0.9C1.195,33.99,1.225,33.99,1.265,33.99z" fill="#818181"/>
                                                                <path d="M1.825,36.93c0.54-0.11,0.89-0.64,0.77-1.18c-0.11-0.54-0.64-0.891-1.18-0.771c-0.54,0.11-0.89,0.641-0.77,1.181   c0.09,0.47,0.51,0.79,0.97,0.79C1.684,36.95,1.755,36.95,1.825,36.93z" fill="#818181"/>
                                                                <path d="M4.365,41.25c-0.22-0.5-0.81-0.73-1.32-0.51c-0.5,0.22-0.73,0.81-0.51,1.319c0.17,0.37,0.53,0.601,0.92,0.601   c0.13,0,0.27-0.03,0.4-0.091C4.354,42.35,4.584,41.76,4.365,41.25z" fill="#818181"/>
                                                                <path d="M3.334,38.55c-0.16-0.53-0.73-0.82-1.25-0.65c-0.53,0.17-0.82,0.73-0.65,1.261c0.14,0.42,0.53,0.689,0.95,0.689   c0.1,0,0.21-0.01,0.31-0.04C3.215,39.64,3.505,39.069,3.334,38.55z" fill="#818181"/>
                                                                <path d="M29.465,57.99c-0.55-0.011-1.01,0.43-1.02,0.979c-0.01,0.561,0.43,1.01,0.98,1.021c0.01,0,0.02,0,0.02,0   c0.55,0,0.99-0.431,1-0.98C30.454,58.46,30.015,58,29.465,57.99z" fill="#818181"/>
                                                                <path d="M52.135,47.14c-0.34,0.43-0.26,1.061,0.17,1.4c0.19,0.14,0.4,0.21,0.62,0.21c0.29,0,0.59-0.13,0.79-0.391   c0.34-0.43,0.26-1.06-0.18-1.399S52.475,46.7,52.135,47.14z" fill="#818181"/>
                                                                <path d="M45.834,53.08c-0.45,0.31-0.57,0.939-0.25,1.39c0.19,0.28,0.5,0.43,0.82,0.43c0.2,0,0.39-0.05,0.57-0.17   c0.45-0.31,0.57-0.939,0.25-1.39C46.914,52.88,46.295,52.77,45.834,53.08z" fill="#818181"/>
                                                                <path d="M48.135,51.319c-0.42,0.36-0.47,0.99-0.11,1.41c0.2,0.23,0.48,0.351,0.76,0.351c0.23,0,0.46-0.08,0.65-0.24   c0.42-0.36,0.47-0.99,0.11-1.41C49.184,51.01,48.555,50.96,48.135,51.319z" fill="#818181"/>
                                                                <path d="M55.164,44.439c-0.47-0.29-1.09-0.14-1.38,0.32c-0.29,0.47-0.15,1.09,0.32,1.38c0.17,0.101,0.35,0.15,0.53,0.15   c0.33,0,0.66-0.16,0.85-0.471C55.775,45.35,55.635,44.729,55.164,44.439z" fill="#818181"/>
                                                                <path d="M50.245,49.33c-0.39,0.399-0.37,1.029,0.03,1.41c0.19,0.189,0.44,0.279,0.69,0.279c0.26,0,0.53-0.1,0.72-0.31   c0.38-0.4,0.37-1.03-0.03-1.41S50.624,48.93,50.245,49.33z" fill="#818181"/>
                                                                <path d="M58.854,33.109c-0.54-0.079-1.05,0.311-1.13,0.851c-0.08,0.55,0.3,1.05,0.85,1.13c0.05,0.01,0.1,0.01,0.14,0.01   c0.49,0,0.92-0.359,0.99-0.859C59.785,33.7,59.405,33.189,58.854,33.109z" fill="#818181"/>
                                                                <path d="M58.385,36.069c-0.54-0.13-1.08,0.2-1.22,0.73c-0.13,0.54,0.2,1.08,0.73,1.22c0.08,0.021,0.16,0.03,0.24,0.03   c0.45,0,0.86-0.31,0.97-0.76C59.245,36.75,58.914,36.21,58.385,36.069z" fill="#818181"/>
                                                                <path d="M15.704,54.06c-0.48-0.279-1.09-0.13-1.37,0.351c-0.29,0.47-0.13,1.09,0.34,1.37c0.16,0.1,0.34,0.14,0.51,0.14   c0.34,0,0.67-0.17,0.86-0.49C16.334,54.96,16.175,54.35,15.704,54.06z" fill="#818181"/>
                                                                <path d="M57.604,38.97c-0.52-0.189-1.1,0.08-1.28,0.6c-0.19,0.521,0.07,1.091,0.59,1.28c0.11,0.04,0.23,0.061,0.34,0.061   c0.41,0,0.8-0.25,0.94-0.66C58.385,39.729,58.124,39.16,57.604,38.97z" fill="#818181"/>
                                                                <path d="M56.525,41.77c-0.49-0.239-1.1-0.04-1.34,0.46c-0.24,0.5-0.03,1.101,0.46,1.34c0.14,0.07,0.29,0.101,0.44,0.101   c0.37,0,0.73-0.21,0.9-0.561C57.225,42.609,57.025,42.01,56.525,41.77z" fill="#818181"/>
                                                                <path d="M26.575,57.79c-0.55-0.07-1.04,0.319-1.11,0.87c-0.07,0.55,0.32,1.05,0.87,1.109v0.011c0.04,0,0.08,0,0.12,0   c0.5,0,0.93-0.37,0.99-0.881C27.515,58.35,27.124,57.859,26.575,57.79z" fill="#818181"/>
                                                                <path d="M20.945,56.5c-0.52-0.181-1.09,0.09-1.27,0.62c-0.18,0.52,0.1,1.09,0.62,1.27c0.11,0.03,0.22,0.05,0.33,0.05   c0.41,0,0.8-0.26,0.94-0.67C21.745,57.24,21.465,56.67,20.945,56.5z" fill="#818181"/>
                                                                <path d="M23.725,57.29c-0.54-0.12-1.07,0.21-1.2,0.75c-0.12,0.54,0.22,1.08,0.75,1.2c0.08,0.02,0.15,0.02,0.23,0.02   c0.45,0,0.87-0.31,0.97-0.77C24.604,57.95,24.265,57.41,23.725,57.29z" fill="#818181"/>
                                                                <path d="M18.265,55.42c-0.51-0.24-1.1-0.021-1.33,0.479s-0.02,1.101,0.48,1.33c0.14,0.061,0.28,0.09,0.42,0.09   c0.38,0,0.74-0.21,0.91-0.579C18.975,56.24,18.765,55.649,18.265,55.42z" fill="#818181"/>
                                                                <path d="M35.225,57.51c-0.54,0.1-0.9,0.63-0.8,1.17c0.09,0.48,0.52,0.811,0.99,0.811c0.06,0,0.12,0,0.18-0.011   c0.54-0.109,0.9-0.63,0.8-1.17C36.295,57.76,35.765,57.41,35.225,57.51z" fill="#818181"/>
                                                                <path d="M38.035,56.83c-0.53,0.149-0.83,0.71-0.67,1.239c0.13,0.431,0.53,0.711,0.96,0.711c0.09,0,0.19-0.011,0.29-0.04   c0.53-0.16,0.83-0.721,0.67-1.25C39.124,56.97,38.565,56.67,38.035,56.83z" fill="#818181"/>
                                                                <path d="M40.765,55.85c-0.51,0.21-0.75,0.8-0.54,1.311c0.16,0.38,0.53,0.609,0.92,0.609c0.13,0,0.26-0.02,0.39-0.069   c0.51-0.221,0.75-0.801,0.54-1.311C41.854,55.88,41.275,55.64,40.765,55.85z" fill="#818181"/>
                                                                <path d="M32.354,57.899c-0.55,0.051-0.96,0.53-0.91,1.08c0.04,0.521,0.48,0.92,1,0.92c0.02,0,0.05,0,0.08,0v-0.01   c0.55-0.04,0.96-0.52,0.91-1.07C33.394,58.27,32.905,57.859,32.354,57.899z" fill="#818181"/>
                                                                <path d="M43.374,54.6c-0.49,0.26-0.67,0.87-0.4,1.351c0.18,0.34,0.52,0.529,0.88,0.529c0.16,0,0.32-0.04,0.47-0.13   c0.49-0.26,0.67-0.87,0.4-1.35C44.465,54.51,43.854,54.33,43.374,54.6z" fill="#818181"/>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="goal-complete-wrapper">
                                                        <span>complete</span>
                                                        <button class="toogle-complete btn"><i class="glyphicon glyphicon-ok"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <ul class="item-list">
                                        </ul>
                                    </div>
                                    <footer>
                                        <form method="post" class="add-goal-form">
                                            <div class="flex-row">
                                                <div class="flex-col">
                                                    <textarea name="goal_text" rows="1" placeholder="add another goal..." required></textarea>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <button type="submit" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="goals-process">
                                            <ul class="progress-indicator">
                                            </ul>
                                        </div>
                                    </footer>
                                </section>
                                <section class="goals-block trap-goals-wrapper" hidden>
                                    <header class="item-header">
                                        <span class="display-date">Today</span>,
                                        <span class="display-title">I will avoid the trap of...</span>
                                    </header>
                                    <div class="item-body">
                                        <li class="sample goal-item" hidden>
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col fix-col">
                                                    <a href="javascript:;" class="delete-goal">×</a>
                                                </div>
                                                <div class="flex-col fix-col goal-number-col">
                                                    Goal <span class="goal-number"></span>:
                                                </div>
                                                <div class="flex-col">
                                                    <div class="goal-text-wrapper">
                                                        <div class="for-view">
                                                            <span class="goal-text"></span>
                                                            <a href="javascript:;" class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></a>
                                                            <a href="javascript:;" class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></a>
                                                        </div>
                                                        <div class="for-edit">
                                                            <form class="update-form" method="post">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col">
                                                                        <textarea name="goal_text" rows="1" required></textarea>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <button type="submit" class="save-entry"><i class="glyphicon glyphicon-check"></i></button>
                                                                        <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <div class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="for-tomorrow-wrapper">
                                                        <span>tomorrow</span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 59.99 59.99" style="enable-background:new 0 0 59.99 59.99;" xml:space="preserve" width="45px" height="45px">
                                                            <g>
                                                                <path d="M1.005,31c0.552,0,1-0.447,1-1c0-15.439,12.561-28,28-28c6.327,0,12.378,2.115,17.302,6h-5.302c-0.552,0-1,0.447-1,1   s0.448,1,1,1h7.915c0.066,0.006,0.128,0.007,0.193,0h0.891V1c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.81   C43.648,2.408,36.986,0,30.005,0c-16.542,0-30,13.458-30,30C0.005,30.553,0.453,31,1.005,31z" fill="#818181"/>
                                                                <path d="M30.005,8c-12.131,0-22,9.869-22,22s9.869,22,22,22s22-9.869,22-22S42.136,8,30.005,8z M30.005,50   c-11.028,0-20-8.972-20-20s8.972-20,20-20s20,8.972,20,20S41.033,50,30.005,50z" fill="#818181"/>
                                                                <path d="M30.005,15c0.552,0,1-0.447,1-1v-2c0-0.553-0.448-1-1-1s-1,0.447-1,1v2C29.005,14.553,29.453,15,30.005,15z" fill="#818181"/>
                                                                <path d="M30.005,45c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C31.005,45.447,30.557,45,30.005,45z" fill="#818181"/>
                                                                <path d="M46.005,31h2c0.552,0,1-0.447,1-1s-0.448-1-1-1h-2c-0.552,0-1,0.447-1,1S45.453,31,46.005,31z" fill="#818181"/>
                                                                <path d="M14.005,29h-2c-0.552,0-1,0.447-1,1s0.448,1,1,1h2c0.552,0,1-0.447,1-1S14.557,29,14.005,29z" fill="#818181"/>
                                                                <path d="M31.005,25.142V20c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.142c-1.72,0.447-3,1.999-3,3.858c0,0.74,0.215,1.424,0.567,2.019   l-8.274,8.274c-0.391,0.391-0.391,1.023,0,1.414C18.493,40.902,18.749,41,19.005,41s0.512-0.098,0.707-0.293l8.274-8.274   C28.581,32.785,29.265,33,30.005,33c2.206,0,4-1.794,4-4C34.005,27.141,32.725,25.589,31.005,25.142z M30.005,31   c-1.103,0-2-0.897-2-2s0.897-2,2-2s2,0.897,2,2S31.108,31,30.005,31z" fill="#818181"/>
                                                                <path d="M11.894,52.66c-0.33,0.439-0.24,1.069,0.21,1.399c0.17,0.13,0.38,0.19,0.59,0.19c0.31,0,0.61-0.13,0.81-0.4   c0.32-0.439,0.23-1.069-0.21-1.399C12.854,52.12,12.225,52.21,11.894,52.66z" fill="#818181"/>
                                                                <path d="M7.235,46.27c-0.32-0.439-0.95-0.55-1.4-0.229c-0.45,0.33-0.55,0.95-0.23,1.399c0.2,0.271,0.5,0.42,0.82,0.42   c0.2,0,0.4-0.06,0.58-0.189C7.454,47.35,7.555,46.72,7.235,46.27z" fill="#818181"/>
                                                                <path d="M7.624,48.45c-0.41,0.369-0.45,1-0.08,1.409c0.19,0.23,0.47,0.341,0.75,0.341c0.23,0,0.47-0.08,0.66-0.25   c0.41-0.37,0.45-1,0.08-1.41C8.675,48.12,8.045,48.09,7.624,48.45z" fill="#818181"/>
                                                                <path d="M11.065,50.6c-0.4-0.37-1.04-0.34-1.41,0.061c-0.38,0.409-0.35,1.04,0.06,1.42c0.19,0.17,0.43,0.26,0.67,0.26   c0.27,0,0.54-0.11,0.74-0.32c0.37-0.41,0.35-1.04-0.06-1.41C11.065,50.6,11.065,50.6,11.065,50.6z" fill="#818181"/>
                                                                <path d="M5.675,43.84c-0.01,0-0.01,0-0.01,0c-0.27-0.48-0.88-0.65-1.36-0.37c-0.48,0.271-0.65,0.88-0.37,1.36   c0.18,0.319,0.52,0.5,0.87,0.5c0.16,0,0.34-0.04,0.49-0.13C5.775,44.93,5.945,44.319,5.675,43.84z" fill="#818181"/>
                                                                <path d="M58.945,32.12c0.02,0,0.03,0,0.04,0c0.54,0,0.98-0.431,1-0.96c0.02-0.561-0.41-1.021-0.96-1.04   c-0.55-0.021-1.02,0.41-1.04,0.96S58.394,32.1,58.945,32.12z" fill="#818181"/>
                                                                <path d="M1.265,33.99c0.55-0.061,0.95-0.551,0.89-1.101s-0.55-0.95-1.1-0.89c-0.55,0.05-0.95,0.55-0.89,1.09   c0.05,0.52,0.49,0.9,0.99,0.9C1.195,33.99,1.225,33.99,1.265,33.99z" fill="#818181"/>
                                                                <path d="M1.825,36.93c0.54-0.11,0.89-0.64,0.77-1.18c-0.11-0.54-0.64-0.891-1.18-0.771c-0.54,0.11-0.89,0.641-0.77,1.181   c0.09,0.47,0.51,0.79,0.97,0.79C1.684,36.95,1.755,36.95,1.825,36.93z" fill="#818181"/>
                                                                <path d="M4.365,41.25c-0.22-0.5-0.81-0.73-1.32-0.51c-0.5,0.22-0.73,0.81-0.51,1.319c0.17,0.37,0.53,0.601,0.92,0.601   c0.13,0,0.27-0.03,0.4-0.091C4.354,42.35,4.584,41.76,4.365,41.25z" fill="#818181"/>
                                                                <path d="M3.334,38.55c-0.16-0.53-0.73-0.82-1.25-0.65c-0.53,0.17-0.82,0.73-0.65,1.261c0.14,0.42,0.53,0.689,0.95,0.689   c0.1,0,0.21-0.01,0.31-0.04C3.215,39.64,3.505,39.069,3.334,38.55z" fill="#818181"/>
                                                                <path d="M29.465,57.99c-0.55-0.011-1.01,0.43-1.02,0.979c-0.01,0.561,0.43,1.01,0.98,1.021c0.01,0,0.02,0,0.02,0   c0.55,0,0.99-0.431,1-0.98C30.454,58.46,30.015,58,29.465,57.99z" fill="#818181"/>
                                                                <path d="M52.135,47.14c-0.34,0.43-0.26,1.061,0.17,1.4c0.19,0.14,0.4,0.21,0.62,0.21c0.29,0,0.59-0.13,0.79-0.391   c0.34-0.43,0.26-1.06-0.18-1.399S52.475,46.7,52.135,47.14z" fill="#818181"/>
                                                                <path d="M45.834,53.08c-0.45,0.31-0.57,0.939-0.25,1.39c0.19,0.28,0.5,0.43,0.82,0.43c0.2,0,0.39-0.05,0.57-0.17   c0.45-0.31,0.57-0.939,0.25-1.39C46.914,52.88,46.295,52.77,45.834,53.08z" fill="#818181"/>
                                                                <path d="M48.135,51.319c-0.42,0.36-0.47,0.99-0.11,1.41c0.2,0.23,0.48,0.351,0.76,0.351c0.23,0,0.46-0.08,0.65-0.24   c0.42-0.36,0.47-0.99,0.11-1.41C49.184,51.01,48.555,50.96,48.135,51.319z" fill="#818181"/>
                                                                <path d="M55.164,44.439c-0.47-0.29-1.09-0.14-1.38,0.32c-0.29,0.47-0.15,1.09,0.32,1.38c0.17,0.101,0.35,0.15,0.53,0.15   c0.33,0,0.66-0.16,0.85-0.471C55.775,45.35,55.635,44.729,55.164,44.439z" fill="#818181"/>
                                                                <path d="M50.245,49.33c-0.39,0.399-0.37,1.029,0.03,1.41c0.19,0.189,0.44,0.279,0.69,0.279c0.26,0,0.53-0.1,0.72-0.31   c0.38-0.4,0.37-1.03-0.03-1.41S50.624,48.93,50.245,49.33z" fill="#818181"/>
                                                                <path d="M58.854,33.109c-0.54-0.079-1.05,0.311-1.13,0.851c-0.08,0.55,0.3,1.05,0.85,1.13c0.05,0.01,0.1,0.01,0.14,0.01   c0.49,0,0.92-0.359,0.99-0.859C59.785,33.7,59.405,33.189,58.854,33.109z" fill="#818181"/>
                                                                <path d="M58.385,36.069c-0.54-0.13-1.08,0.2-1.22,0.73c-0.13,0.54,0.2,1.08,0.73,1.22c0.08,0.021,0.16,0.03,0.24,0.03   c0.45,0,0.86-0.31,0.97-0.76C59.245,36.75,58.914,36.21,58.385,36.069z" fill="#818181"/>
                                                                <path d="M15.704,54.06c-0.48-0.279-1.09-0.13-1.37,0.351c-0.29,0.47-0.13,1.09,0.34,1.37c0.16,0.1,0.34,0.14,0.51,0.14   c0.34,0,0.67-0.17,0.86-0.49C16.334,54.96,16.175,54.35,15.704,54.06z" fill="#818181"/>
                                                                <path d="M57.604,38.97c-0.52-0.189-1.1,0.08-1.28,0.6c-0.19,0.521,0.07,1.091,0.59,1.28c0.11,0.04,0.23,0.061,0.34,0.061   c0.41,0,0.8-0.25,0.94-0.66C58.385,39.729,58.124,39.16,57.604,38.97z" fill="#818181"/>
                                                                <path d="M56.525,41.77c-0.49-0.239-1.1-0.04-1.34,0.46c-0.24,0.5-0.03,1.101,0.46,1.34c0.14,0.07,0.29,0.101,0.44,0.101   c0.37,0,0.73-0.21,0.9-0.561C57.225,42.609,57.025,42.01,56.525,41.77z" fill="#818181"/>
                                                                <path d="M26.575,57.79c-0.55-0.07-1.04,0.319-1.11,0.87c-0.07,0.55,0.32,1.05,0.87,1.109v0.011c0.04,0,0.08,0,0.12,0   c0.5,0,0.93-0.37,0.99-0.881C27.515,58.35,27.124,57.859,26.575,57.79z" fill="#818181"/>
                                                                <path d="M20.945,56.5c-0.52-0.181-1.09,0.09-1.27,0.62c-0.18,0.52,0.1,1.09,0.62,1.27c0.11,0.03,0.22,0.05,0.33,0.05   c0.41,0,0.8-0.26,0.94-0.67C21.745,57.24,21.465,56.67,20.945,56.5z" fill="#818181"/>
                                                                <path d="M23.725,57.29c-0.54-0.12-1.07,0.21-1.2,0.75c-0.12,0.54,0.22,1.08,0.75,1.2c0.08,0.02,0.15,0.02,0.23,0.02   c0.45,0,0.87-0.31,0.97-0.77C24.604,57.95,24.265,57.41,23.725,57.29z" fill="#818181"/>
                                                                <path d="M18.265,55.42c-0.51-0.24-1.1-0.021-1.33,0.479s-0.02,1.101,0.48,1.33c0.14,0.061,0.28,0.09,0.42,0.09   c0.38,0,0.74-0.21,0.91-0.579C18.975,56.24,18.765,55.649,18.265,55.42z" fill="#818181"/>
                                                                <path d="M35.225,57.51c-0.54,0.1-0.9,0.63-0.8,1.17c0.09,0.48,0.52,0.811,0.99,0.811c0.06,0,0.12,0,0.18-0.011   c0.54-0.109,0.9-0.63,0.8-1.17C36.295,57.76,35.765,57.41,35.225,57.51z" fill="#818181"/>
                                                                <path d="M38.035,56.83c-0.53,0.149-0.83,0.71-0.67,1.239c0.13,0.431,0.53,0.711,0.96,0.711c0.09,0,0.19-0.011,0.29-0.04   c0.53-0.16,0.83-0.721,0.67-1.25C39.124,56.97,38.565,56.67,38.035,56.83z" fill="#818181"/>
                                                                <path d="M40.765,55.85c-0.51,0.21-0.75,0.8-0.54,1.311c0.16,0.38,0.53,0.609,0.92,0.609c0.13,0,0.26-0.02,0.39-0.069   c0.51-0.221,0.75-0.801,0.54-1.311C41.854,55.88,41.275,55.64,40.765,55.85z" fill="#818181"/>
                                                                <path d="M32.354,57.899c-0.55,0.051-0.96,0.53-0.91,1.08c0.04,0.521,0.48,0.92,1,0.92c0.02,0,0.05,0,0.08,0v-0.01   c0.55-0.04,0.96-0.52,0.91-1.07C33.394,58.27,32.905,57.859,32.354,57.899z" fill="#818181"/>
                                                                <path d="M43.374,54.6c-0.49,0.26-0.67,0.87-0.4,1.351c0.18,0.34,0.52,0.529,0.88,0.529c0.16,0,0.32-0.04,0.47-0.13   c0.49-0.26,0.67-0.87,0.4-1.35C44.465,54.51,43.854,54.33,43.374,54.6z" fill="#818181"/>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="goal-complete-wrapper">
                                                        <span>complete</span>
                                                        <button class="toogle-complete btn"><i class="glyphicon glyphicon-ok"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <ul class="item-list">
                                        </ul>
                                    </div>
                                    <footer>
                                        <form method="post" class="add-goal-form">
                                            <div class="flex-row">
                                                <div class="flex-col">
                                                    <textarea name="goal_text" rows="1" placeholder="add another trap to avoid..." required></textarea>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <button type="submit" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="goals-process">
                                            <ul class="progress-indicator">
                                            </ul>
                                        </div>
                                    </footer>
                                </section>
                                <footer class="item-footer">
                                    <div class="flex-wrapper">
                                        <div class="flex-row space-around">
                                            <div class="flex-col fix-col"><a href="javascript:;" class="step-before"><i style="color: grey; " class="fa fa-angle-left fa-2x" aria-hidden="true"></i></a></div>
                                            <div class="flex-col fix-col">
                                                <a class="open-goals" target="_blank" href="mygoals.php">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 423.01 423.01" style="enable-background:new 0 0 423.01 423.01;" xml:space="preserve">
                                                        <path d="M412.501,423.007L253.248,147.171V0H137.229v85h101.019v62.171l-64.533,111.775l-34.242-59.304L10.508,423.01h244.93v-0.003
                                                            H412.501z M152.229,70V15h86.019v55H152.229z M104.975,408.007l90.211-156.251l19.982,40.028l30.58-28.227l30.58,28.227
                                                            l19.981-40.027l90.211,156.25H104.975z M204.177,236.183l41.571-72.003l41.572,72.004l-15.491,31.031l-26.08-24.073l-26.08,24.073
                                                            L204.177,236.183z M139.472,229.641l25.582,44.305L87.653,408.01H36.489L139.472,229.641z"/>
                                                        </svg>
                                                </a>
                                            </div>
                                            <div class="flex-col fix-col"><a href="javascript:;" class="step-next"><i style="color: grey; " class="fa fa-angle-right fa-2x" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="goals-morevn item">
                            <div class="flex-row vertial-top margin-between">
                                <div class="flex-col">
                                    <div class="goal-page morning-pages item-data">
                                        <aside class="page-meta">
                                            <button style="display: none;" class="toogle-complete btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                        </aside>
                                        <header>
                                            Morning<br>
                                            Pages
                                        </header>
                                        <div class="page-text-wrapper">
                                            <div class="hint">
                                                An opportunity to clear your mind and free yourself for the day ahead.
                                            </div>
                                            <div class="page-text"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-col">
                                    <div class="goal-page evening-pages item-data">
                                        <aside class="page-meta">
                                            <button style="display: none;" class="toogle-complete btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                        </aside>
                                        <header>
                                            Evening<br>
                                            Reflections
                                        </header>
                                        <div class="page-text-wrapper">
                                            <div class="hint">
                                                An opportunity to reflect and bring closure to your day.
                                            </div>
                                            <div class="page-text"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="flex-col right-col" hidden>
                    <div class="right-panel">
                        <section class="events-section">
                            <div class="calendar">
                                <div class="current_day">
                                    <div class="number"><?php echo date('d'); ?></div>
                                    <div class="month"><?php echo date('F'); ?></div>
                                </div>
                                <div id="calendar_for_today"></div>
                            </div>
                            <section class="personal-events">
                                <header>PERSONAL EVENTS</header>
                                <ul class="event-list">
                                    <li class="sample" hidden>
                                        <div class="flex-row vertical-center">
                                            <div class="flex-col fix-col fav-col">
                                                <a href="javascript:;" class="make-favorite">
                                                    <i style="color: grey;" class="un-favorite fa fa-star-o fa-1x" aria-hidden="true"></i>
                                                    <span class="favorite-wrapper"><i style="color: yellow; " class="fa fa-star fa-1x" aria-hidden="true"></i><i style="color: #ffd800; position: absolute;" class="fa fa-star-o fa-1x" aria-hidden="true"></i></span>
                                                </a>
                                            </div>
                                            <div class="flex-col title-col">
                                                <a href="javascript:;" class="event-instance"><span class="occurring-datetime"></span> <span class="event-title"></span></a>
                                            </div>
                                            <div class="flex-col fix-col del-col">
                                                <button type="button" class="close cancel-event-instance" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <footer>
                                    <a href="javascript:;" class="view-more-event">View More</a>&nbsp&nbsp|&nbsp&nbsp<a class="add-event" href="createevent">Add Event</a>
                                </footer>
                            </section>
                        </section>
                        <section class="amazon-products-watchlist-section">
                            <h1 class="watch-list-title">watch &nbsp;list</h1>
                            <div class="new-product-wrapper">
                                <div class="input-wrapper">
                                    <input class="form-control" name="new_product" placeholder="Drag Amazon Url Here"/>
                                </div>
                                <div class="btn-wrapper">
                                    <button class="check-product btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                </div>
                            </div>
                            <div class="watch-list-wrapper">
                                <ul class="product-list">
                                    <li class="list-item sample" hidden>
                                        <div class="amchar-icon-wrapper"><a href="javascript:;"><img src="assets/images/global-icons/bar-chart.png"></a></div>
                                        <div class="product-info-wrapper">
                                            <div class="basic-info-wrapper">
                                                <div class="product-title">product title</div>
                                                <div class="right-side">
                                                    <div class="product-price"><a href="" target="_blank">$19.99</a></div>
                                                    <div class="close-product-wrapper">
                                                        <button type="button" class="close close-product" aria-label="Close">
                                                            <span>×</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="recommendation-wrapper">
                                                Recommendation: <span class="recommendation">Buy Now</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="view-all-wrapper">
                                <a href="javascript:;" class="view-all">View All</a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once 'yevgeny/views/_templates/_footer.php';?>
</div>

<div class="goals-main item first-goals-popup sample" hidden>
    <div class="item-data">
        <section class="goals-block grate-goals-wrapper" hidden>
            <header class="item-header">
                <span class="display-date">Today</span>,
                <span class="display-title">I will be grateful for...</span>
            </header>
            <div class="item-body">
                <li class="sample goal-item" hidden>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <a href="javascript:;" class="delete-goal">×</a>
                        </div>
                        <div class="flex-col fix-col goal-number-col">
                            Goal <span class="goal-number"></span>:
                        </div>
                        <div class="flex-col">
                            <div class="goal-text-wrapper">
                                <div class="for-view">
                                    <span class="goal-text"></span>
                                    <a href="javascript:;" class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></a>
                                    <a href="javascript:;" class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></a>
                                </div>
                                <div class="for-edit">
                                    <form class="update-form" method="post">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col">
                                                <textarea name="goal_text" rows="1" required></textarea>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <button type="submit" class="save-entry"><i class="glyphicon glyphicon-check"></i></button>
                                                <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="for-tomorrow-wrapper">
                                <span>tomorrow</span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 59.99 59.99" style="enable-background:new 0 0 59.99 59.99;" xml:space="preserve" width="45px" height="45px">
                                                            <g>
                                                                <path d="M1.005,31c0.552,0,1-0.447,1-1c0-15.439,12.561-28,28-28c6.327,0,12.378,2.115,17.302,6h-5.302c-0.552,0-1,0.447-1,1   s0.448,1,1,1h7.915c0.066,0.006,0.128,0.007,0.193,0h0.891V1c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.81   C43.648,2.408,36.986,0,30.005,0c-16.542,0-30,13.458-30,30C0.005,30.553,0.453,31,1.005,31z" fill="#818181"/>
                                                                <path d="M30.005,8c-12.131,0-22,9.869-22,22s9.869,22,22,22s22-9.869,22-22S42.136,8,30.005,8z M30.005,50   c-11.028,0-20-8.972-20-20s8.972-20,20-20s20,8.972,20,20S41.033,50,30.005,50z" fill="#818181"/>
                                                                <path d="M30.005,15c0.552,0,1-0.447,1-1v-2c0-0.553-0.448-1-1-1s-1,0.447-1,1v2C29.005,14.553,29.453,15,30.005,15z" fill="#818181"/>
                                                                <path d="M30.005,45c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C31.005,45.447,30.557,45,30.005,45z" fill="#818181"/>
                                                                <path d="M46.005,31h2c0.552,0,1-0.447,1-1s-0.448-1-1-1h-2c-0.552,0-1,0.447-1,1S45.453,31,46.005,31z" fill="#818181"/>
                                                                <path d="M14.005,29h-2c-0.552,0-1,0.447-1,1s0.448,1,1,1h2c0.552,0,1-0.447,1-1S14.557,29,14.005,29z" fill="#818181"/>
                                                                <path d="M31.005,25.142V20c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.142c-1.72,0.447-3,1.999-3,3.858c0,0.74,0.215,1.424,0.567,2.019   l-8.274,8.274c-0.391,0.391-0.391,1.023,0,1.414C18.493,40.902,18.749,41,19.005,41s0.512-0.098,0.707-0.293l8.274-8.274   C28.581,32.785,29.265,33,30.005,33c2.206,0,4-1.794,4-4C34.005,27.141,32.725,25.589,31.005,25.142z M30.005,31   c-1.103,0-2-0.897-2-2s0.897-2,2-2s2,0.897,2,2S31.108,31,30.005,31z" fill="#818181"/>
                                                                <path d="M11.894,52.66c-0.33,0.439-0.24,1.069,0.21,1.399c0.17,0.13,0.38,0.19,0.59,0.19c0.31,0,0.61-0.13,0.81-0.4   c0.32-0.439,0.23-1.069-0.21-1.399C12.854,52.12,12.225,52.21,11.894,52.66z" fill="#818181"/>
                                                                <path d="M7.235,46.27c-0.32-0.439-0.95-0.55-1.4-0.229c-0.45,0.33-0.55,0.95-0.23,1.399c0.2,0.271,0.5,0.42,0.82,0.42   c0.2,0,0.4-0.06,0.58-0.189C7.454,47.35,7.555,46.72,7.235,46.27z" fill="#818181"/>
                                                                <path d="M7.624,48.45c-0.41,0.369-0.45,1-0.08,1.409c0.19,0.23,0.47,0.341,0.75,0.341c0.23,0,0.47-0.08,0.66-0.25   c0.41-0.37,0.45-1,0.08-1.41C8.675,48.12,8.045,48.09,7.624,48.45z" fill="#818181"/>
                                                                <path d="M11.065,50.6c-0.4-0.37-1.04-0.34-1.41,0.061c-0.38,0.409-0.35,1.04,0.06,1.42c0.19,0.17,0.43,0.26,0.67,0.26   c0.27,0,0.54-0.11,0.74-0.32c0.37-0.41,0.35-1.04-0.06-1.41C11.065,50.6,11.065,50.6,11.065,50.6z" fill="#818181"/>
                                                                <path d="M5.675,43.84c-0.01,0-0.01,0-0.01,0c-0.27-0.48-0.88-0.65-1.36-0.37c-0.48,0.271-0.65,0.88-0.37,1.36   c0.18,0.319,0.52,0.5,0.87,0.5c0.16,0,0.34-0.04,0.49-0.13C5.775,44.93,5.945,44.319,5.675,43.84z" fill="#818181"/>
                                                                <path d="M58.945,32.12c0.02,0,0.03,0,0.04,0c0.54,0,0.98-0.431,1-0.96c0.02-0.561-0.41-1.021-0.96-1.04   c-0.55-0.021-1.02,0.41-1.04,0.96S58.394,32.1,58.945,32.12z" fill="#818181"/>
                                                                <path d="M1.265,33.99c0.55-0.061,0.95-0.551,0.89-1.101s-0.55-0.95-1.1-0.89c-0.55,0.05-0.95,0.55-0.89,1.09   c0.05,0.52,0.49,0.9,0.99,0.9C1.195,33.99,1.225,33.99,1.265,33.99z" fill="#818181"/>
                                                                <path d="M1.825,36.93c0.54-0.11,0.89-0.64,0.77-1.18c-0.11-0.54-0.64-0.891-1.18-0.771c-0.54,0.11-0.89,0.641-0.77,1.181   c0.09,0.47,0.51,0.79,0.97,0.79C1.684,36.95,1.755,36.95,1.825,36.93z" fill="#818181"/>
                                                                <path d="M4.365,41.25c-0.22-0.5-0.81-0.73-1.32-0.51c-0.5,0.22-0.73,0.81-0.51,1.319c0.17,0.37,0.53,0.601,0.92,0.601   c0.13,0,0.27-0.03,0.4-0.091C4.354,42.35,4.584,41.76,4.365,41.25z" fill="#818181"/>
                                                                <path d="M3.334,38.55c-0.16-0.53-0.73-0.82-1.25-0.65c-0.53,0.17-0.82,0.73-0.65,1.261c0.14,0.42,0.53,0.689,0.95,0.689   c0.1,0,0.21-0.01,0.31-0.04C3.215,39.64,3.505,39.069,3.334,38.55z" fill="#818181"/>
                                                                <path d="M29.465,57.99c-0.55-0.011-1.01,0.43-1.02,0.979c-0.01,0.561,0.43,1.01,0.98,1.021c0.01,0,0.02,0,0.02,0   c0.55,0,0.99-0.431,1-0.98C30.454,58.46,30.015,58,29.465,57.99z" fill="#818181"/>
                                                                <path d="M52.135,47.14c-0.34,0.43-0.26,1.061,0.17,1.4c0.19,0.14,0.4,0.21,0.62,0.21c0.29,0,0.59-0.13,0.79-0.391   c0.34-0.43,0.26-1.06-0.18-1.399S52.475,46.7,52.135,47.14z" fill="#818181"/>
                                                                <path d="M45.834,53.08c-0.45,0.31-0.57,0.939-0.25,1.39c0.19,0.28,0.5,0.43,0.82,0.43c0.2,0,0.39-0.05,0.57-0.17   c0.45-0.31,0.57-0.939,0.25-1.39C46.914,52.88,46.295,52.77,45.834,53.08z" fill="#818181"/>
                                                                <path d="M48.135,51.319c-0.42,0.36-0.47,0.99-0.11,1.41c0.2,0.23,0.48,0.351,0.76,0.351c0.23,0,0.46-0.08,0.65-0.24   c0.42-0.36,0.47-0.99,0.11-1.41C49.184,51.01,48.555,50.96,48.135,51.319z" fill="#818181"/>
                                                                <path d="M55.164,44.439c-0.47-0.29-1.09-0.14-1.38,0.32c-0.29,0.47-0.15,1.09,0.32,1.38c0.17,0.101,0.35,0.15,0.53,0.15   c0.33,0,0.66-0.16,0.85-0.471C55.775,45.35,55.635,44.729,55.164,44.439z" fill="#818181"/>
                                                                <path d="M50.245,49.33c-0.39,0.399-0.37,1.029,0.03,1.41c0.19,0.189,0.44,0.279,0.69,0.279c0.26,0,0.53-0.1,0.72-0.31   c0.38-0.4,0.37-1.03-0.03-1.41S50.624,48.93,50.245,49.33z" fill="#818181"/>
                                                                <path d="M58.854,33.109c-0.54-0.079-1.05,0.311-1.13,0.851c-0.08,0.55,0.3,1.05,0.85,1.13c0.05,0.01,0.1,0.01,0.14,0.01   c0.49,0,0.92-0.359,0.99-0.859C59.785,33.7,59.405,33.189,58.854,33.109z" fill="#818181"/>
                                                                <path d="M58.385,36.069c-0.54-0.13-1.08,0.2-1.22,0.73c-0.13,0.54,0.2,1.08,0.73,1.22c0.08,0.021,0.16,0.03,0.24,0.03   c0.45,0,0.86-0.31,0.97-0.76C59.245,36.75,58.914,36.21,58.385,36.069z" fill="#818181"/>
                                                                <path d="M15.704,54.06c-0.48-0.279-1.09-0.13-1.37,0.351c-0.29,0.47-0.13,1.09,0.34,1.37c0.16,0.1,0.34,0.14,0.51,0.14   c0.34,0,0.67-0.17,0.86-0.49C16.334,54.96,16.175,54.35,15.704,54.06z" fill="#818181"/>
                                                                <path d="M57.604,38.97c-0.52-0.189-1.1,0.08-1.28,0.6c-0.19,0.521,0.07,1.091,0.59,1.28c0.11,0.04,0.23,0.061,0.34,0.061   c0.41,0,0.8-0.25,0.94-0.66C58.385,39.729,58.124,39.16,57.604,38.97z" fill="#818181"/>
                                                                <path d="M56.525,41.77c-0.49-0.239-1.1-0.04-1.34,0.46c-0.24,0.5-0.03,1.101,0.46,1.34c0.14,0.07,0.29,0.101,0.44,0.101   c0.37,0,0.73-0.21,0.9-0.561C57.225,42.609,57.025,42.01,56.525,41.77z" fill="#818181"/>
                                                                <path d="M26.575,57.79c-0.55-0.07-1.04,0.319-1.11,0.87c-0.07,0.55,0.32,1.05,0.87,1.109v0.011c0.04,0,0.08,0,0.12,0   c0.5,0,0.93-0.37,0.99-0.881C27.515,58.35,27.124,57.859,26.575,57.79z" fill="#818181"/>
                                                                <path d="M20.945,56.5c-0.52-0.181-1.09,0.09-1.27,0.62c-0.18,0.52,0.1,1.09,0.62,1.27c0.11,0.03,0.22,0.05,0.33,0.05   c0.41,0,0.8-0.26,0.94-0.67C21.745,57.24,21.465,56.67,20.945,56.5z" fill="#818181"/>
                                                                <path d="M23.725,57.29c-0.54-0.12-1.07,0.21-1.2,0.75c-0.12,0.54,0.22,1.08,0.75,1.2c0.08,0.02,0.15,0.02,0.23,0.02   c0.45,0,0.87-0.31,0.97-0.77C24.604,57.95,24.265,57.41,23.725,57.29z" fill="#818181"/>
                                                                <path d="M18.265,55.42c-0.51-0.24-1.1-0.021-1.33,0.479s-0.02,1.101,0.48,1.33c0.14,0.061,0.28,0.09,0.42,0.09   c0.38,0,0.74-0.21,0.91-0.579C18.975,56.24,18.765,55.649,18.265,55.42z" fill="#818181"/>
                                                                <path d="M35.225,57.51c-0.54,0.1-0.9,0.63-0.8,1.17c0.09,0.48,0.52,0.811,0.99,0.811c0.06,0,0.12,0,0.18-0.011   c0.54-0.109,0.9-0.63,0.8-1.17C36.295,57.76,35.765,57.41,35.225,57.51z" fill="#818181"/>
                                                                <path d="M38.035,56.83c-0.53,0.149-0.83,0.71-0.67,1.239c0.13,0.431,0.53,0.711,0.96,0.711c0.09,0,0.19-0.011,0.29-0.04   c0.53-0.16,0.83-0.721,0.67-1.25C39.124,56.97,38.565,56.67,38.035,56.83z" fill="#818181"/>
                                                                <path d="M40.765,55.85c-0.51,0.21-0.75,0.8-0.54,1.311c0.16,0.38,0.53,0.609,0.92,0.609c0.13,0,0.26-0.02,0.39-0.069   c0.51-0.221,0.75-0.801,0.54-1.311C41.854,55.88,41.275,55.64,40.765,55.85z" fill="#818181"/>
                                                                <path d="M32.354,57.899c-0.55,0.051-0.96,0.53-0.91,1.08c0.04,0.521,0.48,0.92,1,0.92c0.02,0,0.05,0,0.08,0v-0.01   c0.55-0.04,0.96-0.52,0.91-1.07C33.394,58.27,32.905,57.859,32.354,57.899z" fill="#818181"/>
                                                                <path d="M43.374,54.6c-0.49,0.26-0.67,0.87-0.4,1.351c0.18,0.34,0.52,0.529,0.88,0.529c0.16,0,0.32-0.04,0.47-0.13   c0.49-0.26,0.67-0.87,0.4-1.35C44.465,54.51,43.854,54.33,43.374,54.6z" fill="#818181"/>
                                                            </g>
                                                        </svg>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="goal-complete-wrapper">
                                <span>complete</span>
                                <button class="toogle-complete btn"><i class="glyphicon glyphicon-ok"></i></button>
                            </div>
                        </div>
                    </div>
                </li>
                <ul class="item-list">
                </ul>
            </div>
            <footer>
                <form method="post" class="add-goal-form">
                    <div class="flex-row">
                        <div class="flex-col">
                            <textarea name="goal_text" rows="1" placeholder="add another..." required></textarea>
                        </div>
                        <div class="flex-col fix-col">
                            <button type="submit" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                        </div>
                    </div>
                </form>
                <div class="goals-process">
                    <ul class="progress-indicator">
                    </ul>
                </div>
            </footer>
        </section>
        <section class="goals-block normal-goals-wrapper">
            <header class="item-header">
                <span class="display-date">Today</span>,
                <span class="display-title">I will...</span>
            </header>
            <div class="item-body">
                <li class="sample goal-item" hidden>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <a href="javascript:;" class="delete-goal">×</a>
                        </div>
                        <div class="flex-col fix-col goal-number-col">
                            Goal <span class="goal-number"></span>:
                        </div>
                        <div class="flex-col">
                            <div class="goal-text-wrapper">
                                <div class="for-view">
                                    <span class="goal-text"></span>
                                    <a href="javascript:;" class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></a>
                                    <a href="javascript:;" class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></a>
                                </div>
                                <div class="for-edit">
                                    <form class="update-form" method="post">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col">
                                                <textarea name="goal_text" rows="1" required></textarea>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <button type="submit" class="save-entry"><i class="glyphicon glyphicon-check"></i></button>
                                                <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="for-tomorrow-wrapper">
                                <span>tomorrow</span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 59.99 59.99" style="enable-background:new 0 0 59.99 59.99;" xml:space="preserve" width="45px" height="45px">
                                                            <g>
                                                                <path d="M1.005,31c0.552,0,1-0.447,1-1c0-15.439,12.561-28,28-28c6.327,0,12.378,2.115,17.302,6h-5.302c-0.552,0-1,0.447-1,1   s0.448,1,1,1h7.915c0.066,0.006,0.128,0.007,0.193,0h0.891V1c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.81   C43.648,2.408,36.986,0,30.005,0c-16.542,0-30,13.458-30,30C0.005,30.553,0.453,31,1.005,31z" fill="#818181"/>
                                                                <path d="M30.005,8c-12.131,0-22,9.869-22,22s9.869,22,22,22s22-9.869,22-22S42.136,8,30.005,8z M30.005,50   c-11.028,0-20-8.972-20-20s8.972-20,20-20s20,8.972,20,20S41.033,50,30.005,50z" fill="#818181"/>
                                                                <path d="M30.005,15c0.552,0,1-0.447,1-1v-2c0-0.553-0.448-1-1-1s-1,0.447-1,1v2C29.005,14.553,29.453,15,30.005,15z" fill="#818181"/>
                                                                <path d="M30.005,45c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C31.005,45.447,30.557,45,30.005,45z" fill="#818181"/>
                                                                <path d="M46.005,31h2c0.552,0,1-0.447,1-1s-0.448-1-1-1h-2c-0.552,0-1,0.447-1,1S45.453,31,46.005,31z" fill="#818181"/>
                                                                <path d="M14.005,29h-2c-0.552,0-1,0.447-1,1s0.448,1,1,1h2c0.552,0,1-0.447,1-1S14.557,29,14.005,29z" fill="#818181"/>
                                                                <path d="M31.005,25.142V20c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.142c-1.72,0.447-3,1.999-3,3.858c0,0.74,0.215,1.424,0.567,2.019   l-8.274,8.274c-0.391,0.391-0.391,1.023,0,1.414C18.493,40.902,18.749,41,19.005,41s0.512-0.098,0.707-0.293l8.274-8.274   C28.581,32.785,29.265,33,30.005,33c2.206,0,4-1.794,4-4C34.005,27.141,32.725,25.589,31.005,25.142z M30.005,31   c-1.103,0-2-0.897-2-2s0.897-2,2-2s2,0.897,2,2S31.108,31,30.005,31z" fill="#818181"/>
                                                                <path d="M11.894,52.66c-0.33,0.439-0.24,1.069,0.21,1.399c0.17,0.13,0.38,0.19,0.59,0.19c0.31,0,0.61-0.13,0.81-0.4   c0.32-0.439,0.23-1.069-0.21-1.399C12.854,52.12,12.225,52.21,11.894,52.66z" fill="#818181"/>
                                                                <path d="M7.235,46.27c-0.32-0.439-0.95-0.55-1.4-0.229c-0.45,0.33-0.55,0.95-0.23,1.399c0.2,0.271,0.5,0.42,0.82,0.42   c0.2,0,0.4-0.06,0.58-0.189C7.454,47.35,7.555,46.72,7.235,46.27z" fill="#818181"/>
                                                                <path d="M7.624,48.45c-0.41,0.369-0.45,1-0.08,1.409c0.19,0.23,0.47,0.341,0.75,0.341c0.23,0,0.47-0.08,0.66-0.25   c0.41-0.37,0.45-1,0.08-1.41C8.675,48.12,8.045,48.09,7.624,48.45z" fill="#818181"/>
                                                                <path d="M11.065,50.6c-0.4-0.37-1.04-0.34-1.41,0.061c-0.38,0.409-0.35,1.04,0.06,1.42c0.19,0.17,0.43,0.26,0.67,0.26   c0.27,0,0.54-0.11,0.74-0.32c0.37-0.41,0.35-1.04-0.06-1.41C11.065,50.6,11.065,50.6,11.065,50.6z" fill="#818181"/>
                                                                <path d="M5.675,43.84c-0.01,0-0.01,0-0.01,0c-0.27-0.48-0.88-0.65-1.36-0.37c-0.48,0.271-0.65,0.88-0.37,1.36   c0.18,0.319,0.52,0.5,0.87,0.5c0.16,0,0.34-0.04,0.49-0.13C5.775,44.93,5.945,44.319,5.675,43.84z" fill="#818181"/>
                                                                <path d="M58.945,32.12c0.02,0,0.03,0,0.04,0c0.54,0,0.98-0.431,1-0.96c0.02-0.561-0.41-1.021-0.96-1.04   c-0.55-0.021-1.02,0.41-1.04,0.96S58.394,32.1,58.945,32.12z" fill="#818181"/>
                                                                <path d="M1.265,33.99c0.55-0.061,0.95-0.551,0.89-1.101s-0.55-0.95-1.1-0.89c-0.55,0.05-0.95,0.55-0.89,1.09   c0.05,0.52,0.49,0.9,0.99,0.9C1.195,33.99,1.225,33.99,1.265,33.99z" fill="#818181"/>
                                                                <path d="M1.825,36.93c0.54-0.11,0.89-0.64,0.77-1.18c-0.11-0.54-0.64-0.891-1.18-0.771c-0.54,0.11-0.89,0.641-0.77,1.181   c0.09,0.47,0.51,0.79,0.97,0.79C1.684,36.95,1.755,36.95,1.825,36.93z" fill="#818181"/>
                                                                <path d="M4.365,41.25c-0.22-0.5-0.81-0.73-1.32-0.51c-0.5,0.22-0.73,0.81-0.51,1.319c0.17,0.37,0.53,0.601,0.92,0.601   c0.13,0,0.27-0.03,0.4-0.091C4.354,42.35,4.584,41.76,4.365,41.25z" fill="#818181"/>
                                                                <path d="M3.334,38.55c-0.16-0.53-0.73-0.82-1.25-0.65c-0.53,0.17-0.82,0.73-0.65,1.261c0.14,0.42,0.53,0.689,0.95,0.689   c0.1,0,0.21-0.01,0.31-0.04C3.215,39.64,3.505,39.069,3.334,38.55z" fill="#818181"/>
                                                                <path d="M29.465,57.99c-0.55-0.011-1.01,0.43-1.02,0.979c-0.01,0.561,0.43,1.01,0.98,1.021c0.01,0,0.02,0,0.02,0   c0.55,0,0.99-0.431,1-0.98C30.454,58.46,30.015,58,29.465,57.99z" fill="#818181"/>
                                                                <path d="M52.135,47.14c-0.34,0.43-0.26,1.061,0.17,1.4c0.19,0.14,0.4,0.21,0.62,0.21c0.29,0,0.59-0.13,0.79-0.391   c0.34-0.43,0.26-1.06-0.18-1.399S52.475,46.7,52.135,47.14z" fill="#818181"/>
                                                                <path d="M45.834,53.08c-0.45,0.31-0.57,0.939-0.25,1.39c0.19,0.28,0.5,0.43,0.82,0.43c0.2,0,0.39-0.05,0.57-0.17   c0.45-0.31,0.57-0.939,0.25-1.39C46.914,52.88,46.295,52.77,45.834,53.08z" fill="#818181"/>
                                                                <path d="M48.135,51.319c-0.42,0.36-0.47,0.99-0.11,1.41c0.2,0.23,0.48,0.351,0.76,0.351c0.23,0,0.46-0.08,0.65-0.24   c0.42-0.36,0.47-0.99,0.11-1.41C49.184,51.01,48.555,50.96,48.135,51.319z" fill="#818181"/>
                                                                <path d="M55.164,44.439c-0.47-0.29-1.09-0.14-1.38,0.32c-0.29,0.47-0.15,1.09,0.32,1.38c0.17,0.101,0.35,0.15,0.53,0.15   c0.33,0,0.66-0.16,0.85-0.471C55.775,45.35,55.635,44.729,55.164,44.439z" fill="#818181"/>
                                                                <path d="M50.245,49.33c-0.39,0.399-0.37,1.029,0.03,1.41c0.19,0.189,0.44,0.279,0.69,0.279c0.26,0,0.53-0.1,0.72-0.31   c0.38-0.4,0.37-1.03-0.03-1.41S50.624,48.93,50.245,49.33z" fill="#818181"/>
                                                                <path d="M58.854,33.109c-0.54-0.079-1.05,0.311-1.13,0.851c-0.08,0.55,0.3,1.05,0.85,1.13c0.05,0.01,0.1,0.01,0.14,0.01   c0.49,0,0.92-0.359,0.99-0.859C59.785,33.7,59.405,33.189,58.854,33.109z" fill="#818181"/>
                                                                <path d="M58.385,36.069c-0.54-0.13-1.08,0.2-1.22,0.73c-0.13,0.54,0.2,1.08,0.73,1.22c0.08,0.021,0.16,0.03,0.24,0.03   c0.45,0,0.86-0.31,0.97-0.76C59.245,36.75,58.914,36.21,58.385,36.069z" fill="#818181"/>
                                                                <path d="M15.704,54.06c-0.48-0.279-1.09-0.13-1.37,0.351c-0.29,0.47-0.13,1.09,0.34,1.37c0.16,0.1,0.34,0.14,0.51,0.14   c0.34,0,0.67-0.17,0.86-0.49C16.334,54.96,16.175,54.35,15.704,54.06z" fill="#818181"/>
                                                                <path d="M57.604,38.97c-0.52-0.189-1.1,0.08-1.28,0.6c-0.19,0.521,0.07,1.091,0.59,1.28c0.11,0.04,0.23,0.061,0.34,0.061   c0.41,0,0.8-0.25,0.94-0.66C58.385,39.729,58.124,39.16,57.604,38.97z" fill="#818181"/>
                                                                <path d="M56.525,41.77c-0.49-0.239-1.1-0.04-1.34,0.46c-0.24,0.5-0.03,1.101,0.46,1.34c0.14,0.07,0.29,0.101,0.44,0.101   c0.37,0,0.73-0.21,0.9-0.561C57.225,42.609,57.025,42.01,56.525,41.77z" fill="#818181"/>
                                                                <path d="M26.575,57.79c-0.55-0.07-1.04,0.319-1.11,0.87c-0.07,0.55,0.32,1.05,0.87,1.109v0.011c0.04,0,0.08,0,0.12,0   c0.5,0,0.93-0.37,0.99-0.881C27.515,58.35,27.124,57.859,26.575,57.79z" fill="#818181"/>
                                                                <path d="M20.945,56.5c-0.52-0.181-1.09,0.09-1.27,0.62c-0.18,0.52,0.1,1.09,0.62,1.27c0.11,0.03,0.22,0.05,0.33,0.05   c0.41,0,0.8-0.26,0.94-0.67C21.745,57.24,21.465,56.67,20.945,56.5z" fill="#818181"/>
                                                                <path d="M23.725,57.29c-0.54-0.12-1.07,0.21-1.2,0.75c-0.12,0.54,0.22,1.08,0.75,1.2c0.08,0.02,0.15,0.02,0.23,0.02   c0.45,0,0.87-0.31,0.97-0.77C24.604,57.95,24.265,57.41,23.725,57.29z" fill="#818181"/>
                                                                <path d="M18.265,55.42c-0.51-0.24-1.1-0.021-1.33,0.479s-0.02,1.101,0.48,1.33c0.14,0.061,0.28,0.09,0.42,0.09   c0.38,0,0.74-0.21,0.91-0.579C18.975,56.24,18.765,55.649,18.265,55.42z" fill="#818181"/>
                                                                <path d="M35.225,57.51c-0.54,0.1-0.9,0.63-0.8,1.17c0.09,0.48,0.52,0.811,0.99,0.811c0.06,0,0.12,0,0.18-0.011   c0.54-0.109,0.9-0.63,0.8-1.17C36.295,57.76,35.765,57.41,35.225,57.51z" fill="#818181"/>
                                                                <path d="M38.035,56.83c-0.53,0.149-0.83,0.71-0.67,1.239c0.13,0.431,0.53,0.711,0.96,0.711c0.09,0,0.19-0.011,0.29-0.04   c0.53-0.16,0.83-0.721,0.67-1.25C39.124,56.97,38.565,56.67,38.035,56.83z" fill="#818181"/>
                                                                <path d="M40.765,55.85c-0.51,0.21-0.75,0.8-0.54,1.311c0.16,0.38,0.53,0.609,0.92,0.609c0.13,0,0.26-0.02,0.39-0.069   c0.51-0.221,0.75-0.801,0.54-1.311C41.854,55.88,41.275,55.64,40.765,55.85z" fill="#818181"/>
                                                                <path d="M32.354,57.899c-0.55,0.051-0.96,0.53-0.91,1.08c0.04,0.521,0.48,0.92,1,0.92c0.02,0,0.05,0,0.08,0v-0.01   c0.55-0.04,0.96-0.52,0.91-1.07C33.394,58.27,32.905,57.859,32.354,57.899z" fill="#818181"/>
                                                                <path d="M43.374,54.6c-0.49,0.26-0.67,0.87-0.4,1.351c0.18,0.34,0.52,0.529,0.88,0.529c0.16,0,0.32-0.04,0.47-0.13   c0.49-0.26,0.67-0.87,0.4-1.35C44.465,54.51,43.854,54.33,43.374,54.6z" fill="#818181"/>
                                                            </g>
                                                        </svg>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="goal-complete-wrapper">
                                <span>complete</span>
                                <button class="toogle-complete btn"><i class="glyphicon glyphicon-ok"></i></button>
                            </div>
                        </div>
                    </div>
                </li>
                <ul class="item-list">
                </ul>
            </div>
            <footer>
                <form method="post" class="add-goal-form">
                    <div class="flex-row">
                        <div class="flex-col">
                            <textarea name="goal_text" rows="1" placeholder="add another goal..." required></textarea>
                        </div>
                        <div class="flex-col fix-col">
                            <button type="submit" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                        </div>
                    </div>
                </form>
                <div class="goals-process">
                    <ul class="progress-indicator">
                    </ul>
                </div>
            </footer>
        </section>
        <section class="goals-block trap-goals-wrapper" hidden>
            <header class="item-header">
                <span class="display-date">Today</span>,
                <span class="display-title">I will avoid the trap of...</span>
            </header>
            <div class="item-body">
                <li class="sample goal-item" hidden>
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fix-col">
                            <a href="javascript:;" class="delete-goal">×</a>
                        </div>
                        <div class="flex-col fix-col goal-number-col">
                            Goal <span class="goal-number"></span>:
                        </div>
                        <div class="flex-col">
                            <div class="goal-text-wrapper">
                                <div class="for-view">
                                    <span class="goal-text"></span>
                                    <a href="javascript:;" class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></a>
                                    <a href="javascript:;" class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></a>
                                </div>
                                <div class="for-edit">
                                    <form class="update-form" method="post">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col">
                                                <textarea name="goal_text" rows="1" required></textarea>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <button type="submit" class="save-entry"><i class="glyphicon glyphicon-check"></i></button>
                                                <a href="javascript:;" class="back-to-origin"><i class="glyphicon glyphicon-repeat"></i></a>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="drag-icon-wrapper"><img class="drag-icon" src="assets/images/global-icons/move.png"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="for-tomorrow-wrapper">
                                <span>tomorrow</span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 59.99 59.99" style="enable-background:new 0 0 59.99 59.99;" xml:space="preserve" width="45px" height="45px">
                                                            <g>
                                                                <path d="M1.005,31c0.552,0,1-0.447,1-1c0-15.439,12.561-28,28-28c6.327,0,12.378,2.115,17.302,6h-5.302c-0.552,0-1,0.447-1,1   s0.448,1,1,1h7.915c0.066,0.006,0.128,0.007,0.193,0h0.891V1c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.81   C43.648,2.408,36.986,0,30.005,0c-16.542,0-30,13.458-30,30C0.005,30.553,0.453,31,1.005,31z" fill="#818181"/>
                                                                <path d="M30.005,8c-12.131,0-22,9.869-22,22s9.869,22,22,22s22-9.869,22-22S42.136,8,30.005,8z M30.005,50   c-11.028,0-20-8.972-20-20s8.972-20,20-20s20,8.972,20,20S41.033,50,30.005,50z" fill="#818181"/>
                                                                <path d="M30.005,15c0.552,0,1-0.447,1-1v-2c0-0.553-0.448-1-1-1s-1,0.447-1,1v2C29.005,14.553,29.453,15,30.005,15z" fill="#818181"/>
                                                                <path d="M30.005,45c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C31.005,45.447,30.557,45,30.005,45z" fill="#818181"/>
                                                                <path d="M46.005,31h2c0.552,0,1-0.447,1-1s-0.448-1-1-1h-2c-0.552,0-1,0.447-1,1S45.453,31,46.005,31z" fill="#818181"/>
                                                                <path d="M14.005,29h-2c-0.552,0-1,0.447-1,1s0.448,1,1,1h2c0.552,0,1-0.447,1-1S14.557,29,14.005,29z" fill="#818181"/>
                                                                <path d="M31.005,25.142V20c0-0.553-0.448-1-1-1s-1,0.447-1,1v5.142c-1.72,0.447-3,1.999-3,3.858c0,0.74,0.215,1.424,0.567,2.019   l-8.274,8.274c-0.391,0.391-0.391,1.023,0,1.414C18.493,40.902,18.749,41,19.005,41s0.512-0.098,0.707-0.293l8.274-8.274   C28.581,32.785,29.265,33,30.005,33c2.206,0,4-1.794,4-4C34.005,27.141,32.725,25.589,31.005,25.142z M30.005,31   c-1.103,0-2-0.897-2-2s0.897-2,2-2s2,0.897,2,2S31.108,31,30.005,31z" fill="#818181"/>
                                                                <path d="M11.894,52.66c-0.33,0.439-0.24,1.069,0.21,1.399c0.17,0.13,0.38,0.19,0.59,0.19c0.31,0,0.61-0.13,0.81-0.4   c0.32-0.439,0.23-1.069-0.21-1.399C12.854,52.12,12.225,52.21,11.894,52.66z" fill="#818181"/>
                                                                <path d="M7.235,46.27c-0.32-0.439-0.95-0.55-1.4-0.229c-0.45,0.33-0.55,0.95-0.23,1.399c0.2,0.271,0.5,0.42,0.82,0.42   c0.2,0,0.4-0.06,0.58-0.189C7.454,47.35,7.555,46.72,7.235,46.27z" fill="#818181"/>
                                                                <path d="M7.624,48.45c-0.41,0.369-0.45,1-0.08,1.409c0.19,0.23,0.47,0.341,0.75,0.341c0.23,0,0.47-0.08,0.66-0.25   c0.41-0.37,0.45-1,0.08-1.41C8.675,48.12,8.045,48.09,7.624,48.45z" fill="#818181"/>
                                                                <path d="M11.065,50.6c-0.4-0.37-1.04-0.34-1.41,0.061c-0.38,0.409-0.35,1.04,0.06,1.42c0.19,0.17,0.43,0.26,0.67,0.26   c0.27,0,0.54-0.11,0.74-0.32c0.37-0.41,0.35-1.04-0.06-1.41C11.065,50.6,11.065,50.6,11.065,50.6z" fill="#818181"/>
                                                                <path d="M5.675,43.84c-0.01,0-0.01,0-0.01,0c-0.27-0.48-0.88-0.65-1.36-0.37c-0.48,0.271-0.65,0.88-0.37,1.36   c0.18,0.319,0.52,0.5,0.87,0.5c0.16,0,0.34-0.04,0.49-0.13C5.775,44.93,5.945,44.319,5.675,43.84z" fill="#818181"/>
                                                                <path d="M58.945,32.12c0.02,0,0.03,0,0.04,0c0.54,0,0.98-0.431,1-0.96c0.02-0.561-0.41-1.021-0.96-1.04   c-0.55-0.021-1.02,0.41-1.04,0.96S58.394,32.1,58.945,32.12z" fill="#818181"/>
                                                                <path d="M1.265,33.99c0.55-0.061,0.95-0.551,0.89-1.101s-0.55-0.95-1.1-0.89c-0.55,0.05-0.95,0.55-0.89,1.09   c0.05,0.52,0.49,0.9,0.99,0.9C1.195,33.99,1.225,33.99,1.265,33.99z" fill="#818181"/>
                                                                <path d="M1.825,36.93c0.54-0.11,0.89-0.64,0.77-1.18c-0.11-0.54-0.64-0.891-1.18-0.771c-0.54,0.11-0.89,0.641-0.77,1.181   c0.09,0.47,0.51,0.79,0.97,0.79C1.684,36.95,1.755,36.95,1.825,36.93z" fill="#818181"/>
                                                                <path d="M4.365,41.25c-0.22-0.5-0.81-0.73-1.32-0.51c-0.5,0.22-0.73,0.81-0.51,1.319c0.17,0.37,0.53,0.601,0.92,0.601   c0.13,0,0.27-0.03,0.4-0.091C4.354,42.35,4.584,41.76,4.365,41.25z" fill="#818181"/>
                                                                <path d="M3.334,38.55c-0.16-0.53-0.73-0.82-1.25-0.65c-0.53,0.17-0.82,0.73-0.65,1.261c0.14,0.42,0.53,0.689,0.95,0.689   c0.1,0,0.21-0.01,0.31-0.04C3.215,39.64,3.505,39.069,3.334,38.55z" fill="#818181"/>
                                                                <path d="M29.465,57.99c-0.55-0.011-1.01,0.43-1.02,0.979c-0.01,0.561,0.43,1.01,0.98,1.021c0.01,0,0.02,0,0.02,0   c0.55,0,0.99-0.431,1-0.98C30.454,58.46,30.015,58,29.465,57.99z" fill="#818181"/>
                                                                <path d="M52.135,47.14c-0.34,0.43-0.26,1.061,0.17,1.4c0.19,0.14,0.4,0.21,0.62,0.21c0.29,0,0.59-0.13,0.79-0.391   c0.34-0.43,0.26-1.06-0.18-1.399S52.475,46.7,52.135,47.14z" fill="#818181"/>
                                                                <path d="M45.834,53.08c-0.45,0.31-0.57,0.939-0.25,1.39c0.19,0.28,0.5,0.43,0.82,0.43c0.2,0,0.39-0.05,0.57-0.17   c0.45-0.31,0.57-0.939,0.25-1.39C46.914,52.88,46.295,52.77,45.834,53.08z" fill="#818181"/>
                                                                <path d="M48.135,51.319c-0.42,0.36-0.47,0.99-0.11,1.41c0.2,0.23,0.48,0.351,0.76,0.351c0.23,0,0.46-0.08,0.65-0.24   c0.42-0.36,0.47-0.99,0.11-1.41C49.184,51.01,48.555,50.96,48.135,51.319z" fill="#818181"/>
                                                                <path d="M55.164,44.439c-0.47-0.29-1.09-0.14-1.38,0.32c-0.29,0.47-0.15,1.09,0.32,1.38c0.17,0.101,0.35,0.15,0.53,0.15   c0.33,0,0.66-0.16,0.85-0.471C55.775,45.35,55.635,44.729,55.164,44.439z" fill="#818181"/>
                                                                <path d="M50.245,49.33c-0.39,0.399-0.37,1.029,0.03,1.41c0.19,0.189,0.44,0.279,0.69,0.279c0.26,0,0.53-0.1,0.72-0.31   c0.38-0.4,0.37-1.03-0.03-1.41S50.624,48.93,50.245,49.33z" fill="#818181"/>
                                                                <path d="M58.854,33.109c-0.54-0.079-1.05,0.311-1.13,0.851c-0.08,0.55,0.3,1.05,0.85,1.13c0.05,0.01,0.1,0.01,0.14,0.01   c0.49,0,0.92-0.359,0.99-0.859C59.785,33.7,59.405,33.189,58.854,33.109z" fill="#818181"/>
                                                                <path d="M58.385,36.069c-0.54-0.13-1.08,0.2-1.22,0.73c-0.13,0.54,0.2,1.08,0.73,1.22c0.08,0.021,0.16,0.03,0.24,0.03   c0.45,0,0.86-0.31,0.97-0.76C59.245,36.75,58.914,36.21,58.385,36.069z" fill="#818181"/>
                                                                <path d="M15.704,54.06c-0.48-0.279-1.09-0.13-1.37,0.351c-0.29,0.47-0.13,1.09,0.34,1.37c0.16,0.1,0.34,0.14,0.51,0.14   c0.34,0,0.67-0.17,0.86-0.49C16.334,54.96,16.175,54.35,15.704,54.06z" fill="#818181"/>
                                                                <path d="M57.604,38.97c-0.52-0.189-1.1,0.08-1.28,0.6c-0.19,0.521,0.07,1.091,0.59,1.28c0.11,0.04,0.23,0.061,0.34,0.061   c0.41,0,0.8-0.25,0.94-0.66C58.385,39.729,58.124,39.16,57.604,38.97z" fill="#818181"/>
                                                                <path d="M56.525,41.77c-0.49-0.239-1.1-0.04-1.34,0.46c-0.24,0.5-0.03,1.101,0.46,1.34c0.14,0.07,0.29,0.101,0.44,0.101   c0.37,0,0.73-0.21,0.9-0.561C57.225,42.609,57.025,42.01,56.525,41.77z" fill="#818181"/>
                                                                <path d="M26.575,57.79c-0.55-0.07-1.04,0.319-1.11,0.87c-0.07,0.55,0.32,1.05,0.87,1.109v0.011c0.04,0,0.08,0,0.12,0   c0.5,0,0.93-0.37,0.99-0.881C27.515,58.35,27.124,57.859,26.575,57.79z" fill="#818181"/>
                                                                <path d="M20.945,56.5c-0.52-0.181-1.09,0.09-1.27,0.62c-0.18,0.52,0.1,1.09,0.62,1.27c0.11,0.03,0.22,0.05,0.33,0.05   c0.41,0,0.8-0.26,0.94-0.67C21.745,57.24,21.465,56.67,20.945,56.5z" fill="#818181"/>
                                                                <path d="M23.725,57.29c-0.54-0.12-1.07,0.21-1.2,0.75c-0.12,0.54,0.22,1.08,0.75,1.2c0.08,0.02,0.15,0.02,0.23,0.02   c0.45,0,0.87-0.31,0.97-0.77C24.604,57.95,24.265,57.41,23.725,57.29z" fill="#818181"/>
                                                                <path d="M18.265,55.42c-0.51-0.24-1.1-0.021-1.33,0.479s-0.02,1.101,0.48,1.33c0.14,0.061,0.28,0.09,0.42,0.09   c0.38,0,0.74-0.21,0.91-0.579C18.975,56.24,18.765,55.649,18.265,55.42z" fill="#818181"/>
                                                                <path d="M35.225,57.51c-0.54,0.1-0.9,0.63-0.8,1.17c0.09,0.48,0.52,0.811,0.99,0.811c0.06,0,0.12,0,0.18-0.011   c0.54-0.109,0.9-0.63,0.8-1.17C36.295,57.76,35.765,57.41,35.225,57.51z" fill="#818181"/>
                                                                <path d="M38.035,56.83c-0.53,0.149-0.83,0.71-0.67,1.239c0.13,0.431,0.53,0.711,0.96,0.711c0.09,0,0.19-0.011,0.29-0.04   c0.53-0.16,0.83-0.721,0.67-1.25C39.124,56.97,38.565,56.67,38.035,56.83z" fill="#818181"/>
                                                                <path d="M40.765,55.85c-0.51,0.21-0.75,0.8-0.54,1.311c0.16,0.38,0.53,0.609,0.92,0.609c0.13,0,0.26-0.02,0.39-0.069   c0.51-0.221,0.75-0.801,0.54-1.311C41.854,55.88,41.275,55.64,40.765,55.85z" fill="#818181"/>
                                                                <path d="M32.354,57.899c-0.55,0.051-0.96,0.53-0.91,1.08c0.04,0.521,0.48,0.92,1,0.92c0.02,0,0.05,0,0.08,0v-0.01   c0.55-0.04,0.96-0.52,0.91-1.07C33.394,58.27,32.905,57.859,32.354,57.899z" fill="#818181"/>
                                                                <path d="M43.374,54.6c-0.49,0.26-0.67,0.87-0.4,1.351c0.18,0.34,0.52,0.529,0.88,0.529c0.16,0,0.32-0.04,0.47-0.13   c0.49-0.26,0.67-0.87,0.4-1.35C44.465,54.51,43.854,54.33,43.374,54.6z" fill="#818181"/>
                                                            </g>
                                                        </svg>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="goal-complete-wrapper">
                                <span>complete</span>
                                <button class="toogle-complete btn"><i class="glyphicon glyphicon-ok"></i></button>
                            </div>
                        </div>
                    </div>
                </li>
                <ul class="item-list">
                </ul>
            </div>
            <footer>
                <form method="post" class="add-goal-form">
                    <div class="flex-row">
                        <div class="flex-col">
                            <textarea name="goal_text" rows="1" placeholder="add another trap to avoid..." required></textarea>
                        </div>
                        <div class="flex-col fix-col">
                            <button type="submit" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                        </div>
                    </div>
                </form>
                <div class="goals-process">
                    <ul class="progress-indicator">
                    </ul>
                </div>
            </footer>
        </section>
        <footer class="item-footer" hidden>
            <div class="flex-wrapper">
                <div class="flex-row space-around">
                    <div class="flex-col fix-col"><a href="javascript:;" class="step-before"><i style="color: grey; " class="fa fa-angle-left fa-2x" aria-hidden="true"></i></a></div>
                    <div class="flex-col fix-col"><a class="open-goals" target="_blank" href="mygoals.php"><i style="color: grey; " class="fa fa-calendar fa-2x" aria-hidden="true"></i></a></div>
                    <div class="flex-col fix-col"><a href="javascript:;" class="step-next"><i style="color: grey; " class="fa fa-angle-right fa-2x" aria-hidden="true"></i></a></div>
                </div>
            </div>
        </footer>
    </div>
    <aside>
        Your goals are road maps that guide you and show you what is possible for your life.
        <label class="les-brown">- Les Brown</label>
    </aside>
    <footer class="modal-footer">
        <button class="btn btn-default">My Goals are set!</button>
    </footer>
</div>

<div class="goal-page-popup-content sample" hidden>
    <header class="popup-content-header">
        <span class="display-title">My Morning Page</span> for <span class="display-date">Monday, February 26, 2018</span>
    </header>
    <aside class="need-helper-wrapper">
        <div class="helper-tip-wrapper">
            <div class="flex-row margin-between vertical-center">
                <div class="flex-col">
                    <div class="helper-tip-text">
                        Morning pages are an opportunity to remove the clutter from your thoughts so that you can approach the day with creativity and clarity. Don't worry about what you write, just write from your stream of conscious.
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="helper-tip-btn">
                        Need help?<br>
                        Here's a writing prompt.
                    </div>
                </div>
            </div>
        </div>
        <div class="helper-content"></div>
    </aside>
    <div class="popup-content-body">
        <form method="post" class="morevn-form">
            <textarea name="morevn_text" rows="10"></textarea>
            <footer>
                <button href="#" class="btn btn-circle">Complete</button>
                <aside class="footer-meta">
                    <button type="button" href="#" class="btn cancel-btn">Cancel</button>
                </aside>
            </footer>
        </form>
    </div>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/user-global.js?version=<?php echo time();?>" type="text/javascript"></script>

<script type="text/javascript">
    const ACTION_URL = BASE_URL + window.location.pathname;
    const default_event_image = "<?php echo $default_event_image;?>";
    var initialSeries = <?php echo json_encode($series);?>;
    var initialEvents = <?php echo json_encode($events);?>;
    var initialGoals = <?php echo json_encode($goals);?>;
    var serverToday = <?php echo json_encode(date('Y-m-d'));?>;
    var initialScrollTo = <?php echo json_encode($scrollTo)?>;
    var initialFocusId = <?php echo json_encode($focusId)?>;
    var isFirstVisit = <?php echo json_encode($isFirstVisit)?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/user-page.js?version=<?php echo time();?>"  type="text/javascript"></script>

</body>
</html>