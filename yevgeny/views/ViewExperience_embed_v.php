<!DOCTYPE html>
<html class="walden-site">

<head>
    <!--    <meta name="viewport" content="width=screen-width, initial-scale=1, maximum-scale=1, user-scalable=no">-->
    <title>Edit the solution to make it perfect for you - thegreyshirt</title>
    <meta name="description" content="Edit your daily solution to include the content that you want in the order that you want it." />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/viewExperienceEmbed-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/viewExperinece-embed-page.css?version=<?php echo time();?>">
    <?php
    if ( $externalStyle && $externalStyle !== 'false' ) {
        echo '<link rel="stylesheet" href="'. $externalStyle .'" />';
    }
    ?>
    <style type="text/css">
        <?php echo $inlineStyle && $inlineStyle !== 'false' ? $inlineStyle : '';?>
    </style>
</head>
<body>
<div class="site-wrapper <?php echo 'view-on-' . $viewVersion;?> <?php echo $isLoggedIn ? 'logged-in' : '';?>">
    <div class="flex-row">
        <div class="flex-col left-panel">
            <header class="left-header">
                <div class="flex-row vertical-center space-between">
                    <div class="flex-col">
                        <h3 class="welcome">Welcome back, <span class="display-name"><?php echo $isLoggedIn ? $_SESSION['f_name'] : '';?></span>!</h3>
                        <div class="login-btn">
                            <a class="btn big-login" href="javascript:;">Log in</a>
                        </div>
                    </div>
                    <?php if ($viewVersion === 'right-panel'): ?>
                        <div class="flex-col fix-col">
                            <div class="collapse-wrapper">
                                <i class="glyphicon glyphicon-chevron-left"></i>
                                <i class="glyphicon glyphicon-chevron-right"></i>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </header>
            <div class="posts-container jstree jstree-default jstree-default-large">
                <li class="sample post-row jstree-node jstree-closed" hidden>
                    <div class="post-row-inner">
                        <div class="flex-row vertical-center space-between margin-between">
                            <div class="flex-col fix-col"><i class="jstree-icon jstree-ocl" role="presentation"></i></div>
                            <div class="flex-col fix-col">
                                <div class="item_thumb_wrap">
                                    <img class="item_thumb type-root" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/series/start-(3).png">
                                    <img class="item_thumb type-menu" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/menu/path-selection-(4).png">
                                    <img class="item_thumb type-path" src="<?php echo BASE_URL;?>/assets/images/global-icons/tree/option/next-(4).png">
                                    <div class="item_thumb type-post">
                                        <div class="video-post">
                                            <div class="thumb_cover_back"></div>
                                            <div class="thumb_play"></div>
                                        </div>
                                        <div class="audio-post">
                                            <div class="thumb_cover_back"></div>
                                            <div class="thumb_play"></div>
                                        </div>
                                        <div class="image-post"></div>
                                        <div class="text-post"></div>
                                        <div class="form-post"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="post-title"></div>
                                <div class="post-meta-info">
                                    <span class="post-type-info"></span><span class="post-dyn-info"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="sample post-load-more jstree-node jstree-closed" hidden>
                    <div class="post-row-inner">
                        <div class="flex-row vertical-center space-between margin-between">
                            <div class="flex-col fix-col"><i class="jstree-icon jstree-ocl" role="presentation"></i></div>
                            <div class="flex-col">
                                <div class="post-title">Load More ...</div>
                            </div>
                        </div>
                    </div>
                </li>
                <ul class="posts-list">
                </ul>
            </div>
        </div>
        <?php if ($viewVersion === 'right-panel'): ?>
            <div class="flex-col right-panel">
                <div class="right-panel-inner">
                    <aside class="money-saved">
                        <div class="money-amount">$<span><?php echo $totalMoneySaved ? $totalMoneySaved : 0;?></span></div>
                        <div class="w-name">Money saved</div>
                    </aside>
                    <div class="blog-content-wrapper">
                        <div class="post-image-wrapper">
                            <img class="post-img" />
                        </div>
                        <div class="post-title"></div>
                        <div class="post-body">
                            <div class="type-0 blog-type sample" hidden>
                                <div class="audiogallery">
                                    <div class="items">
                                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="type-2 blog-type sample" hidden>
                                <div class="video-wrapper">
                                    <video controls class="video-js" data-autoresize="fit" width="960" height="540"></video>
                                </div>
                            </div>
                            <div class="type-8 blog-type sample" hidden>
                            </div>
                            <div class="type-menu blog-type sample" hidden>
                                <header>
                                    <h3 class="menu-title"></h3>
                                    <p class="menu-description"></p>
                                </header>
                                <div class="paths-container">
                                    <div class="flex-col path-col sample" hidden>
                                        <div class="img-wrapper">
                                            <img />
                                        </div>
                                        <h3 class="path-title"></h3>
                                        <p class="path-description"></p>
                                    </div>
                                    <div class="flex-row paths-list flex-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="type-10 blog-type sample" hidden></div>
                            <div class="type-other blog-type sample" hidden></div>
                        </div>
                    </div>
                    <div class="pay-blog-wrapper" hidden>
                        <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
                    </div>
                    <footer class="right-panel-footer">
                        <div class="nav-wrapper">
                            <div class="flex-row space-between vertical-center">
                                <div class="flex-col fix-col">
                                    <a class="step-back">
                                        <span class="content-turbotax__back-arr">‹</span> Back
                                    </a>
                                </div>
                                <div class="flex-col fix-col">
                                    <a class="step-next">Continue</a>
                                </div>
                            </div>
                        </div>
                        <div class="powered-by">Powered by Walden.ly</div>
                    </footer>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="view-widget maximize-view">
    <div class="widget-inner">
        <header class="widget-header view-only-min">
            <div class="flex-row space-between margin-between vertical-center">
                <div class="flex-col">
                    <div class="post-title"></div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_max"></div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="mv_min_control">
                        <div class="mv_min_control_close"></div>
                    </div>
                </div>
            </div>
        </header>
        <aside class="widget-top-control view-only-max">
            <div class="mv_top_button mv_top_close">
                <div class="mv_close_icon"></div>
            </div>
            <div class="mv_top_button mv_top_minimize">
                <div class="mv_minimize_icon"></div>
            </div>
        </aside>
        <div class="widget-body">
            <div class="video-widget sample" hidden>
                <video controls class="video-js" data-autoresize="fit" width="960" height="540"></video>
            </div>
            <div class="audio-widget sample" hidden>
                <div class="audiogallery">
                    <div class="items">
                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                    </div>
                </div>
            </div>
            <div class="image-widget sample" hidden></div>
            <div class="text-widget sample" hidden></div>
            <div class="form-widget sample" hidden></div>
        </div>
        <footer class="widget-footer view-only-max">
            <div class="post-title"></div>
        </footer>
    </div>
</div>

<div class="modal fade login" id="loginModal">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login with</h4>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="content">
                        <div class="social">
                            <a class="circle github" href="/auth/github">
                                <i class="fa fa-github fa-fw"></i>
                            </a>
                            <a id="google_login" class="circle google" href="/auth/google_oauth2">
                                <i class="fa fa-google-plus fa-fw"></i>
                            </a>
                            <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                                <i class="fa fa-facebook fa-fw"></i>
                            </a>
                        </div>
                        <div class="division">
                            <div class="line l"></div>
                            <span>or</span>
                            <div class="line r"></div>
                        </div>
                        <div class="error"></div>
                        <div class="form loginBox">
                            <form method="post">
                                <input class="form-control" type="text" placeholder="Email" name="email" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="btn btn-default btn-login" type="submit" value="Login">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content registerBox" style="display:none;">
                        <div class="form">
                            <form method="post">
                                <input class="form-control" type="email" placeholder="Email" name="email" required>
                                <input class="form-control" type="text" placeholder="First Name" name="first_name" required>
                                <input class="form-control" type="text" placeholder="Last Name" name="last_name" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation" required>
                                <input class="btn btn-default btn-register" type="submit" value="Create account" name="commit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="forgot login-footer">
                            <span>Looking to
                                 <a href="javascript:;" class="show-register">create an account</a>
                            ?</span>
                </div>
                <div class="forgot register-footer" style="display:none">
                    <span>Already have an account?</span>
                    <a class="show-login" href="javascript:;">Login</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    const BASE_URL = <?php echo json_encode(BASE_URL);?>;
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/viewExperienceEmbed-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initialSeries = <?php echo json_encode($series);?>;
    var from = 'post';
    var formFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var viewVersion = <?php echo json_encode($viewVersion);?>;
    var welcomePost = <?php echo json_encode($welcomePost);?>;
    var clientName = <?php echo $isLoggedIn ? json_encode($_SESSION['f_name']) : json_encode(false);?>;
    var clientSeriesView = <?php echo json_encode($clientSeriesView);?>;
    var seekPost = <?php echo json_encode($seekPost);?>;
    var series = <?php echo json_encode($series);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var embedId = <?php echo json_encode($embedId);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/viewExperience-embed-page.js?version=<?php echo time();?>"></script>

<?php
if ( $externalScript && $externalScript !== 'false' ) {
    echo '<script src="'. $externalScript .'"></script>';
}
?>
<script type="application/javascript">
    <?php echo $inlineScript && $inlineScript !== 'false' ? $inlineScript : '';?>
</script>

</body>
</html>