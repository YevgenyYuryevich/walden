<?php

$rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',  $_SERVER['SERVER_NAME'] );

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="Daily Experiences for You">
    <meta name="Description" content="Find the daily experiences that are right for you">
    <title>Daily Experiences for You</title>

    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/css/yevgeny/explore-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/css/yevgeny/explore-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-explore">
    <?php require_once $rootPath . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Explore All Paths
                </div>
            </div>
            <header class="content-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <form class="form-inline search-form" method="post">
                            <label for="search-term">Search</label>&nbsp;&nbsp;
                            <input name="searchTerm" class="form-control" size="25" id="search-term" />
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <form class="form-inline sort-from" method="post">
                            <label for="sort-by">Search</label>&nbsp;&nbsp;
                            <select name="sortBy" class="form-control" id="sort-by">
                                <option value="a-z">A&nbsp;&nbsp;-&nbsp;&nbsp;Z</option>
                                <option value="z-a">Z&nbsp;&nbsp;-&nbsp;&nbsp;A</option>
                            </select>
                            <div class="new-series">
                                New <a href="createseries"><svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="scroll-fix-rplc" hidden>
                    <div class="flex-row vertical-center space-between">
                        <div class="flex-col fix-col">
                            <a class="back-to-prev" href="javascript:;"><i class="glyphicon glyphicon-menu-left"></i></a>
                        </div>
                        <div class="flex-col">
                            <div class="rplc-content-wrp text-center">
                                <a href="createseries">Not finding what you want? Create the perfect experience for you. <svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <div class="item-sample-wrapper" hidden>
                    <a>
                        <div class="column">
                            <div class="column_object">
                                <div class="image-wrapper">
                                    <img src="" alt="" class="object_image" />
                                </div>
                                <div class="title-wrapper">
                                    <div class="object_title"></div>
                                    <div class="view-more-wrapper">
                                        <span class="view-more" href="javascript:;">more ...</span>
                                        <span class="view-less" href="javascript:;">less</span>
                                    </div>
                                </div>
                                <div class="buttons-wrapper">
                                    <button href="#" class="view-post btn btn-circle">View</button>
                                </div>
                                <div class="object_line"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <section class="posts-block sample" hidden>
                    <div class="row">
                        <div class="col-md-10 col-sm-12"><h3 class="category-title"></h3></div>
                        <div class="col-md-2 col-sm-12"><div class="view-category-wrapper"><a class="view-category" href="javascript:;">View all</a></div></div>
                    </div>
                    <div class="tt-grid-wrapper after-clearfix">
                        <span class="pagging prev-pagging">
                            <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
                        </span>
                        <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                        </ul>
                        <span class="pagging next-pagging">
                            <i class="glyphicon glyphicon-play"></i>
                        </span>
                    </div>
                </section>
            </main>
        </div>
    </div>
    <?php require_once $rootPath . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo $base_url;?>/assets/js/yevgeny/explore-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initCategories = <?php echo json_encode($categories);?>;
    var isFirstVisit = <?php echo json_encode($isFirstVisit);?>;
</script>
<script src="<?php echo $base_url;?>/assets/js/yevgeny/explore-page.js?version=<?php echo time();?>"></script>

</body>
</html>