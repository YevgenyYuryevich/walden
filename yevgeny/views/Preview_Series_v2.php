<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.11.2019
 * Time: 11:01
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="<?php echo $series['strSeries_title'];?>" />
    <meta name="Description" content='<?php echo $series['strSeries_description'];?>. Contribute to this experience to help others or join it to help yourself.' />
    <meta property="og:image" content="<?php echo $shareData['image'];?>" />
    <meta property="og:title" content="<?php echo $series['strSeries_title'];?>" />
    <?php if (isset($shareData['image_width'])): ?>
        <meta property="og:image:width" content="<?php echo $shareData['image_width']?>" />
        <meta property="og:image:height" content="<?php echo $shareData['image_height']?>" />
    <?php endif; ?>
    <meta property="og:description" content="<?php echo $series['strSeries_description'];?>. Contribute to this experience to help others or join it to help yourself." />

    <title><?php echo $series['strSeries_title'];?> - Join this experience to grow and share.</title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/preview_series-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormField/FormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/preview_series-page-v2.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-preview-series <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="page__header">
            <section class="header__series">
                <div class="section__inner">
                    <div class="series__card">
                        <div class="card__inner">
                            <div class="inner__content">
                                <h3 class="series__title title-font"><?php echo $series['strSeries_title'];?></h3>
                                <aside class="series__category">
                                    <a href="/browse?id=<?php echo $category['category_ID'];?>"><<< <?php echo $category['strCategory_name']?></a>
                                </aside>
                                <div class="series__price-join">
                                    <div class="d-flex align-items-center">
                                        <div class="series__price mr-auto">
                                            <div class="price__value"><?php echo $series['boolSeries_charge'] === "1" ? '$' . $series['intSeries_price'] : 'free' ?></div>
                                            <?php if ($series['boolSeries_charge'] === "1" && $series['strSeries_charge_type'] === "monthly"): ?>
                                                <div class="per-month">per month</div>
                                            <?php endif; ?>
                                        </div>
                                        <button class="site-green-btn btn__join">
                                            <span>Join</span>
                                            <svg style="display: none" version="1.1" id="Layer_1"
                                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="35px" height="28px"
                                                 viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                            <title>Rectangle 4</title>
                                                <desc>Created with Sketch.</desc>
                                                <g id="Page-1" sketch:type="MSPage">
                                                    <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                        <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="series__image flex-centering">
                        <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                            <mask id="series__image__mask" maskContentUnits="objectBoundingBox">
                                <rect fill="white" x="0" y="0" width="100%" height="100%" />
                                <circle cx=".5" cy=".5" r=".45"></circle>
                            </mask>
                            <defs>
                                <linearGradient id="series__image__grad" x1="0%" y1="100%" x2="100%" y2="0%">
                                    <stop offset="0%" style="stop-color:rgb(255,255,255);stop-opacity:.3" />
                                    <stop offset="100%" style="stop-color:rgb(255,255,255);stop-opacity:1" />
                                </linearGradient>
                            </defs>
                            <circle cx="50" cy="50" r="50" fill="url(#series__image__grad)"
                                    mask="url(#series__image__mask)"/>
                        </svg>
                        <img src="<?php echo $series['strSeries_image']?>" alt="" />
                    </div>
                </div>
            </section>
            <section class="header__author">
                <div class="section__inner">
                    <div class="d-flex align-items-center justify-content-xs-center flex-wrap-xs-wrap">
                        <div class="layout__image">
                            <a href="/user_profile?id=<?php echo $creator['id']?>" class="author__image flex-centering">
                                <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                    <mask id="author__image__mask" maskContentUnits="objectBoundingBox">
                                        <rect fill="white" x="0" y="0" width="100%" height="100%" />
                                        <circle cx=".5" cy=".5" r=".45"></circle>
                                    </mask>
                                    <defs>
                                        <linearGradient id="author__image__grad" x1="0%" y1="100%" x2="100%" y2="0%">
                                            <stop offset="0%" style="stop-color:rgb(255,255,255);stop-opacity:.3" />
                                            <stop offset="100%" style="stop-color:rgb(255,255,255);stop-opacity:1" />
                                        </linearGradient>
                                    </defs>
                                    <circle cx="50" cy="50" r="50" fill="url(#author__image__grad)"
                                            mask="url(#author__image__mask)"/>
                                </svg>
                                <img src="<?php echo $creator['image']?>" alt="" />
                            </a>
                        </div>
                        <div class="author__text">
                            <div class="author__name"><a href="/user_profile?id=<?php echo $creator['id']?>"><?php echo $creator['f_name'];?></a></div>
                            <div class="author__description"><?php echo $creator['about_me'] ? $creator['about_me'] : 'No Description yet';?></div>
                        </div>
                    </div>
                </div>
            </section>
        </header>
        <div class="page__content">
            <section class="explore__this">
                <div class="section__inner">
                    <h2 class="section__header">Explore this <?php echo $series['strSeries_level'];?></h2>
                    <div class="series__description">
                        <?php echo $series['strSeries_description'];?>
                    </div>
                </div>
            </section>
            <section class="series__score">
                <div class="section__inner">
                    <div class="d-flex align-items-center justify-content-center flex-wrap">
                        <div class="score__item score__share flex-centering">
                            <div class="item__inner">
                                <div class="item__name">
                                    Shares
                                </div>
                                <div class="item__value join__count__value">
                                    <?php echo $joinCount;?>
                                </div>
                                <div class="item__icon">
                                    <svg version="1.1" id="share__present" x="0px" y="0px"
                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M480,143.686H378.752c7.264-4.96,13.504-9.888,17.856-14.304c25.792-25.952,25.792-68.192,0-94.144
                                                    c-25.056-25.216-68.768-25.248-93.856,0c-13.856,13.92-50.688,70.592-45.6,108.448h-2.304
                                                    c5.056-37.856-31.744-94.528-45.6-108.448c-25.088-25.248-68.8-25.216-93.856,0C89.6,61.19,89.6,103.43,115.36,129.382
                                                    c4.384,4.416,10.624,9.344,17.888,14.304H32c-17.632,0-32,14.368-32,32v80c0,8.832,7.168,16,16,16h16v192
                                                    c0,17.632,14.368,32,32,32h384c17.632,0,32-14.368,32-32v-192h16c8.832,0,16-7.168,16-16v-80
                                                    C512,158.054,497.632,143.686,480,143.686z M138.08,57.798c6.496-6.528,15.104-10.112,24.256-10.112
                                                    c9.12,0,17.728,3.584,24.224,10.112c21.568,21.696,43.008,77.12,35.552,84.832c0,0-1.344,1.056-5.92,1.056
                                                    c-22.112,0-64.32-22.976-78.112-36.864C124.672,93.318,124.672,71.302,138.08,57.798z M240,463.686H64v-192h176V463.686z
                                                     M240,239.686H32v-64h184.192H240V239.686z M325.44,57.798c12.992-13.024,35.52-12.992,48.48,0
                                                    c13.408,13.504,13.408,35.52,0,49.024c-13.792,13.888-56,36.864-78.112,36.864c-4.576,0-5.92-1.024-5.952-1.056
                                                    C282.432,134.918,303.872,79.494,325.44,57.798z M448,463.686H272v-192h176V463.686z M480,239.686H272v-64h23.808H480V239.686z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                <mask id="score__share__mask" maskContentUnits="objectBoundingBox">
                                    <rect fill="white" x="0" y="0" width="100%" height="100%" />
                                    <circle cx=".5" cy=".5" r=".43"></circle>
                                </mask>
                                <defs>
                                    <linearGradient id="score__share__grad" x1="0%" y1="100%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color:rgb(255,255,255);stop-opacity:.1" />
                                        <stop offset="100%" style="stop-color:rgb(255,255,255);stop-opacity:.3" />
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50" fill="url(#score__share__grad)"
                                        mask="url(#score__share__mask)"/>
                            </svg>
                        </div>
                        <div class="score__item score__star flex-centering">
                            <div class="item__inner">
                                <div class="item__name">
                                    Favorites
                                </div>
                                <div class="item__value">
                                    <?php echo $series['favorite_count'];?>
                                </div>
                                <div class="item__icon">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                        <polygon style="fill:#fff;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                            81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                                    </svg>
                                </div>
                            </div>
                            <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                <mask id="score__star__mask" maskContentUnits="objectBoundingBox">
                                    <rect fill="white" x="0" y="0" width="100%" height="100%" />
                                    <circle cx=".5" cy=".5" r=".44"></circle>
                                </mask>
                                <defs>
                                    <linearGradient id="score__star__grad" x1="0%" y1="100%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color:rgb(255,255,255);stop-opacity:.4" />
                                        <stop offset="100%" style="stop-color:rgb(255,255,255);stop-opacity:.4" />
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50" fill="url(#score__star__grad)"
                                        mask="url(#score__star__mask)"/>
                            </svg>
                        </div>
                        <div class="score__item score__day flex-centering">
                            <div class="item__inner">
                                <div class="item__name">
                                    Days of Success
                                </div>
                                <div class="item__value">
                                    <?php echo $postsCount;?>
                                </div>
                                <div class="item__icon">
                                    <img src="/assets/images/global-icons/2-people.png" alt="" />
                                </div>
                            </div>
                            <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                <mask id="score__day__mask" maskContentUnits="objectBoundingBox">
                                    <rect fill="white" x="0" y="0" width="100%" height="100%" />
                                    <circle cx=".5" cy=".5" r=".43"></circle>
                                </mask>
                                <defs>
                                    <linearGradient id="score__day__grad" x1="0%" y1="100%" x2="100%" y2="0%">
                                        <stop offset="0%" style="stop-color:rgb(255,255,255);stop-opacity:.1" />
                                        <stop offset="100%" style="stop-color:rgb(255,255,255);stop-opacity:.3" />
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50" fill="url(#score__day__grad)"
                                        mask="url(#score__day__mask)"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </section>
            <main class="content__main">
                <div class="item sample" hidden>
                    <header class="item-header current-day-show d-xs-block">
                        <div class="flex-row vertical-center margin-between horizon-center flex-wrap-xs-wrap">
                            <div class="flex-col fix-col">
                                <div class="day-wrapper">
                                    day <span class="day-value">2</span>
                                </div>
                            </div>
                            <div class="flex-col fix-col">
                                <aside class="media-icon-wrapper">
                                    <i class="fa fa-youtube-play video-icon"></i>
                                    <i class="fa fa-volume-up audio-icon"></i>
                                    <i class="fa fa-file-text text-icon"></i>
                                </aside>
                            </div>
                            <div class="flex-col">
                                <a href="javascript:;" class="item-title" target="_blank">Why gifted students need to be taught formal writing</a>
                            </div>
                            <?php if ($permission): ?>
                                <div class="flex-col">
                                    <button class="site-green-btn edit-btn">Edit</button>
                                </div>
                            <?php endif; ?>
                            <div class="flex-col">
                                <button class="site-green-btn share-btn">Share</button>
                            </div>
                        </div>
                    </header>
                    <div class="item-body">
                        <div class="type-pay blog-type sample" hidden>
                            <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
                        </div>
                        <div class="type-0 blog-type sample" hidden>
                            <div class="audiogallery">
                                <div class="items">
                                    <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="type-2 blog-type sample" hidden>
                            <div class="video-wrapper">
                                <video controls class="video-js"></video>
                            </div>
                        </div>
                        <div class="type-8 blog-type sample" hidden>
                        </div>
                        <div class="type-menu blog-type sample" hidden>
                            <?php require_once ASSETS_PATH . '/components/MenuOptionsBlog/MenuOptionsBlog.html';?>
                        </div>
                        <div class="type-other blog-type sample" hidden></div>
                        <aside class="blog-duration current-day-show d-xs-block">
                            waiting...
                        </aside>
                    </div>
                    <div class="preview-pan other-day-show d-xs-none">
                        <header class="item-header pan-header">
                            <div class="flex-row vertical-center margin-between">
                                <div class="flex-col fix-col">
                                    <div class="day-wrapper">
                                        day <span class="day-value">2</span>
                                    </div>
                                </div>
                                <div class="flex-col fix-col mr-auto">
                                    <aside class="media-icon-wrapper">
                                        <i class="fa fa-youtube-play video-icon"></i>
                                        <i class="fa fa-volume-up audio-icon"></i>
                                        <i class="fa fa-file-text text-icon"></i>
                                    </aside>
                                </div>
                                <div class="flex-col fix-col">
                                    <button class="site-green-btn share-btn">Share</button>
                                </div>
                            </div>
                        </header>
                        <div class="img-wrapper">
                            <img class="item-img" alt="" />
                            <aside class="blog-duration">
                                waiting...
                            </aside>
                        </div>
                        <div class="item-title-wrapper">
                            <a href="javascript:;" class="item-title" target="_blank">
                                Everyday Nut-Free Desserts Recipes
                            </a>
                        </div>
                    </div>
                </div>
                <h2 class="sneak__preview">Sneak Preview</h2>
                <div class="posts-container">
                </div>
                <div class="slider-nav flex-centering arrow-left d-xs-none">
                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-slider-arrow-left.png" />
                </div>
                <div class="slider-nav flex-centering arrow-right d-xs-none">
                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-slider-arrow-right.png" />
                </div>
            </main>
            <section class="series__share-join">
                <div class="d-flex align-items-center justify-content-center">
                    <button class="site-green-btn share-btn">Share</button>
                    <div class="wrap">
                        <button class="select-post">Join</button>
                        <img src="../assets/images/check_arrow_2.svg" alt="">
                        <svg width="42px" height="42px">
                            <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                        </svg>
                    </div>
                </div>
            </section>
            <div class="quick__overview">
                <div class="section__inner" id="post__list">
                    <h2 class="section__title">Quick Overview</h2>
                    <div class="posts-container flex-row flex-wrap">
                        <article class="flex-col fix-col item sample" hidden>
                            <header class="item-header">
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col fix-col">
                                        <div class="media-icon-wrapper">
                                            <i class="fa fa-youtube-play video-icon"></i>
                                            <i class="fa fa-volume-up audio-icon"></i>
                                            <i class="fa fa-file-text text-icon"></i>
                                        </div>
                                    </div>
                                    <div class="flex-col">
                                        <div class="day-wrapper">
                                            <span>day</span> <span class="day-value">11</span>
                                        </div>
                                    </div>
                                    <?php if ($permission): ?>
                                        <div class="flex-col fix-col">
                                            <a class="item-action edit-action flex-centering" href="javascript:;" tabindex="0">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                    <div class="flex-col fix-col">
                                        <button class="site-green-btn share-btn">Share</button>
                                    </div>
                                </div>
                            </header>
                            <div class="item-type-body">
                                <div class="blog-type type-0 sample" hidden>
                                </div>
                                <div class="blog-type type-2 sample" hidden>
                                    <img alt="">
                                </div>
                                <div class="blog-type type-default sample" hidden>
                                    Lorem lpsum is simply dummy text of the printing and type
                                </div>
                                <aside class="blog-duration">
                                    waiting...
                                </aside>
                            </div>
                            <div class="item-title-wrapper">
                                <a href="javascript:;" target="_blank" class="item-title">Why gifted student needs to be taught formal writing</a>
                            </div>
                        </article>
                    </div>
                    <div class="load-more-status flex-centering">
                        <span class="load-more">load more</span> <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="page__footer">
            <div class="footer-inner">
                <div class="flex-row vertical-center justify-content-center justify-content-xs-center">
                    <div class="flex-col fix-col">
                        <div class="take-first-step-wrapper">
                            <h3 class="take-first-step">
                                Take the first step
                            </h3>
                            <div class="actions-container">
                                <div class="flex-row margin-between justify-content-xs-center">
                                    <div class="flex-col fix-col">
                                        <div class="wrap">
                                            <button class="select-post">Join</button>
                                            <img src="../assets/images/check_arrow_2.svg" alt="">
                                            <svg width="42px" height="42px">
                                                <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col d-xs-none">
                                        <button class="site-green-btn share-btn">Share</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <aside class="line-decoration-wrapper d-xs-none">
                <div class="line-decoration-inner">
                    <div class="flex-row vertical-center">
                        <div class="flex-col fix-col">
                            <div class="you-are-here">you are here</div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="you-are-here-line"></div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="on-your-path">
                                on your path to
                            </div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="on-your-path-line"></div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="self-development">
                                personal growth
                            </div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="self-development-line"></div>
                        </div>
                    </div>
                </div>
            </aside>
        </footer>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
    <?php require ASSETS_PATH . '/components/ListingPost/ListingPost.php';?>
    <?php require ASSETS_PATH . '/components/PostEditor/PostEditor.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
  var series = <?php echo json_encode($series);?>;
  var posts = <?php echo json_encode($posts);?>;
  var stripeApi_pKey = <?php echo json_encode($stripeApi_pKey);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/preview_series-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/preview_series-page-v2.js?version=<?php echo time();?>"></script>

</body>
</html>
