<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.11.2018
 * Time: 19:58
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - walden</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/create_series-global.css?version=<?php echo time();?>" />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/create_series-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <div class="create-series-content">
            <header class="content-header">
                <div class="content-header-content">
                    <div class="flex-row align-items-end space-between">
                        <div class="flex-col fix-col">
                            <div class="title-wrapper">
                                <h2 class="page-title">Create experience</h2>
                                <div class="step-name">Name and describe</div>
                                <div class="step-wrapper">
                                    step <span class="step-value">1</span>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="step-progress-wrapper step-progress-1">
                                <div class="step-wrapper">step <span class="step-value">1</span></div>
                                <div class="progress-wrapper">
                                    <div class="progress-item step-1 active right-dotted"></div>
                                    <div class="progress-item step-2 right-dotted"></div>
                                    <div class="progress-item step-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-step-1">
                        <div class="create-series-step-1">
                            <div class="step-content-wrapper">
                                <div class="flex-row margin-between">
                                    <div class="flex-col">
                                        <div class="step-main-content">
                                            <div class="form-group">
                                                <label class="control-label">Name your experience:</label>
                                                <input placeholder="type your name here" type="text" name="strSeries_title" class="form-control" required/>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Describe it:</label>
                                                <div class="multi-line-background">
                                                    <textarea class="about-input" rows="4" required placeholder="type description for your experience" name="strSeries_description"></textarea>
                                                    <div class="background-placeholder"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Choose category:</label>
                                                <select name="intSeries_category" class="form-control">
                                                    <?php foreach ($categories as $category): ?>
                                                        <option value="<?php echo $category['category_ID'];?>"><?php echo $category['strCategory_name'];?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="flex-row margin-between flex-wrap">
                                                <div class="flex-col">
                                                    <div class="form-group">
                                                        <label class="control-label">Make it available to others?</label>
                                                        <label class="c-switch">
                                                            <input type="checkbox" name="boolSeries_isPublic" value="1">
                                                            <div class="slider round"></div>
                                                        </label>
                                                        <span class="public-value-txt"></span>
                                                    </div>
                                                </div>
                                                <div class="flex-col">
                                                    <div class="form-group">
                                                        <label class="control-label">Display on:</label>
                                                        <div class="flex-row margin-between">
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-1" type="checkbox"/><label for="day-1">Mon</label>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-2" type="checkbox"/><label for="day-2">Tue</label>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-3" type="checkbox"/><label for="day-3">Wed</label>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-4" type="checkbox"/><label for="day-4">Thu</label>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-5" type="checkbox"/><label for="day-5">Fri</label>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-6" type="checkbox"/><label for="day-6">Sat</label>
                                                                </div>
                                                            </div>
                                                            <div class="flex-col fix-col">
                                                                <div class="day-item">
                                                                    <input id="day-0" type="checkbox"/><label for="day-0">Sun</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Upload Cover Image (optional)</label>
                                                <div class="cover-image-wrapper">
                                                    <div class="flex-row margin-between horizon-center vertical-center">
                                                        <div class="flex-col fix-col">
                                                            <div class="img-wrapper">
                                                                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png">
                                                            </div>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <div class="upload-helper-txt">Drag Image here to upload</div>
                                                        </div>
                                                    </div>
                                                    <input class="form-control" type="file" name="strSeries_image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="step-description">
                                            Lorem lpsum is simply dummy text of the printing and typesetting industry.
                                            Lorem lpsum has been the industry's standard dummy
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="step-footer">
                                <div class="let-go-wrapper">
                                    <div class="next-step-des">Search for items and charge users at the next steps>>></div>
                                    <div class="btn-wrapper">
                                        <a href="#tab-step-2" data-toggle="tab" class="let-go-btn">Let's go</a>
                                    </div>
                                </div>
                                <div class="step-progress-wrapper">
                                    <div class="next-step-wrapper">
                                        <div class="next-step-name">Search for items</div>
                                        <div class="next-step-value">step <span>2</span></div>
                                    </div>
                                    <div class="progress-wrapper">
                                        <div class="progress-item step-1 active right-dotted"></div>
                                        <div class="progress-item step-2 right-dotted"></div>
                                        <div class="progress-item step-3"></div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-step-2">
                        <div class="create-series-step-2">
                            <header class="step-header">
                                <div class="flex-row space-between">
                                    <div class="flex-col fix-col">
                                        <div class="form-group">
                                            <label for="" class="control-label">Search for items:</label>
                                            <input type="text" class="form-control search-input" placeholder="start typing terms for your experience" />
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="step-description">
                                            We use these to find your experience
                                        </div>
                                    </div>
                                </div>
                                <div class="terms-container flex-row margin-between">
                                    <div class="flex-col fix-col term-item sample" hidden>#design</div>
                                </div>
                            </header>
                            <div class="step-body">
                                <div class="search-container">
                                    <div class="search-row sample" hidden>
                                        <header class="search-row-header">
                                            <div class="flex-row vertical-center space-between">
                                                <div class="flex-col fix-col">
                                                    <h2 class="from-name">from YouTube</h2>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <a href="javascript:;" class="items-found-wrapper">
                                                        <span class="found-value">0</span> items found
                                                    </a>
                                                </div>
                                            </div>
                                        </header>
                                        <div class="search-row-body">
                                            <div class="search-item sample" hidden>
                                                <div class="item-img-wrapper">
                                                    <img class="item-img" />
                                                    <aside class="blog-duration">
                                                        waiting...
                                                    </aside>
                                                    <aside class="added-icon-wrapper">
                                                        <svg class="added-icon-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </aside>
                                                    <div class="hover-content">
                                                        <h4>Add to series</h4>
                                                        <div class="footer-buttons">
                                                            <div class="flex-row">
                                                                <div class="flex-col">
                                                                    <a href="javascript:;" class="preview-btn" target="_blank">
                                                                        <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                                                    </a>
                                                                </div>
                                                                <div class="flex-col last-col">
                                                                    <a href="javascript:;" class="join-btn">
                                                                        <svg class="join-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                <svg class="added-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                                                        add
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-title">
                                                    Daily recipes
                                                </div>
                                            </div>
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fix-col overflow-visible">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                    </a>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <div class="search-items-list">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col overflow-visible">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <footer class="step-footer">
                                <div class="let-go-wrapper">
                                    <div class="next-step-des">Charge users at the next step >>></div>
                                    <div class="btn-wrapper">
                                        <a href="#tab-step-3" data-toggle="tab" class="let-go-btn">Let's go</a>
                                    </div>
                                </div>
                                <div class="step-progress-wrapper">
                                    <div class="next-step-wrapper">
                                        <div class="next-step-name">Charge users</div>
                                        <div class="next-step-value">step <span>3</span></div>
                                    </div>
                                    <div class="progress-wrapper">
                                        <div class="progress-item step-1 active right-dotted"></div>
                                        <div class="progress-item step-2 active right-dotted"></div>
                                        <div class="progress-item step-3"></div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-step-3">
                        <div class="create-series-step-3">
                            <div class="flex-row margin-between">
                                <div class="flex-col">
                                    <div class="charge-series-wrapper">
                                        <div class="charge-content">
                                            <div class="connect-account-wrapper">
                                                <a target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect');?>" class="stripe-connect"><span>Connect with Stripe</span></a> to charge for your series
                                            </div>
                                            <div class="group-row-wrapper charge-users-wrapper">
                                                <div class="flex-row margin-between space-between">
                                                    <div class="flex-col fix-col">
                                                        <div class="would-charge-wrapper group-col toggle-col">
                                                            <div class="would-charge-txt col-name">Would you like to charge users?</div>
                                                            <div class="flex-row vertical-center margin-between">
                                                                <div class="flex-col fix-col">
                                                                    <label class="c-switch">
                                                                        <input type="checkbox" name="boolSeries_charge" value="1">
                                                                        <div class="slider round"></div>
                                                                    </label>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <div class="get-payment-txt">you will get payment</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col center-col">
                                                        <div class="charge-price-wrapper group-col">
                                                            <label class="price-label col-name">Price, $:</label>
                                                            <input type="text" name="intSeries_price" class="col-value" placeholder="0"/>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="you-will-get-wrapper group-col gets-col">
                                                            <label class="you-will-get-label col-name">You will get:</label>
                                                            <div class="you-will-get-value col-value"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pay-affiliates-wrapper group-row-wrapper">
                                                <div class="flex-row margin-between space-between">
                                                    <div class="flex-col fix-col">
                                                        <div class="would-pay-affiliate-wrapper group-col toggle-col">
                                                            <div class="would-pay-txt col-name">Would you like to Pay Affiliates for Referrals?</div>
                                                            <div class="flex-row vertical-center margin-between">
                                                                <div class="flex-col fix-col">
                                                                    <label class="c-switch">
                                                                        <input type="checkbox" name="boolSeries_affiliated" value="1">
                                                                        <div class="slider round"></div>
                                                                    </label>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <div class="get-payment-txt">Affiliate will get fee</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col center-col overflow-visible">
                                                        <div class="affiliate-percent-wrapper group-col">
                                                            <label class="percent-label col-name">Percent, %:</label>
                                                            <input class="percent-value" name="intSeries_affiliate_percent" type="range" max="100" value="30"/>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="affiliate-will-get-wrapper group-col gets-col">
                                                            <label class="affiliate-will-get-label col-name">Affiliate gets:</label>
                                                            <div class="affiliate-will-get-value col-value"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="step-description">
                                        To charge for your series, you must setup strpe public API in your settings
                                    </div>
                                </div>
                            </div>
                            <div class="button-wrapper">
                                <p>All set! Now let's create!</p>
                                <button class="save-btn">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div class="after-adding-content" hidden>
            <div class="congratulations-wrapper">
                <h1>Congratulations!</h1>
                <div class="series-name-wrapper">
                    <span class="series-name"></span> have just been created
                </div>
            </div>
            <div class="series-item">
                <div class="flex-row">
                    <div class="flex-col fb-0">
                        <div class="series-content-wrapper">
                            <header class="series-content-header">
                                <div class="flex-row">
                                    <div class="flex-col">
                                        <h3 class="series-title"></h3>
                                    </div>
                                    <div class="flex-col fix-col overflow-visible">
                                        <div class="actions-container">
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col">
                                                    <a href="javascript:;" target="_blank" class="action-item edit-action">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                        <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                        <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                            l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col">
                                                    <a href="javascript:;" class="action-item">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                </g>
                                                            <g>
                                                                <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                                                                <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                                                                <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                                                            </g>
                            </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col overflow-visible">
                                                    <?php require_once ASSETS_PATH . '/components/SeriesCopyEmbed/SeriesCopyEmbed.html';?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            <div class="series-content-body">
                                <div class="flex-row margin-between">
                                    <div class="flex-col fb-0">
                                        <div class="posts-list-wrapper">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fix-col overflow-visible">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                    </a>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <article class="post-item sample" hidden>
                                                        <header class="item-header">
                                                            <div class="flex-row vertical-center margin-between">
                                                                <div class="flex-col fix-col">
                                                                    <div class="media-icon-wrapper">
                                                                        <i class="fa fa-youtube-play video-icon"></i>
                                                                        <i class="fa fa-volume-up audio-icon"></i>
                                                                        <i class="fa fa-file-text text-icon"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-col">
                                                                    <div class="day-wrapper">
                                                                        <span>day</span> <span class="day-value">11</span>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <a class="item-action delete-action" href="javascript:;">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                    </a>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <a class="item-action edit-action flex-centering" href="javascript:;">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </header>
                                                        <div class="item-type-body">
                                                            <div class="blog-type type-0 sample" hidden>
                                                            </div>
                                                            <div class="blog-type type-2 sample" hidden>
                                                                <img alt="">
                                                            </div>
                                                            <div class="blog-type type-default sample" hidden>
                                                                Lorem lpsum is simply dummy text of the printing and type
                                                            </div>
                                                            <aside class="blog-duration">
                                                                waiting...
                                                            </aside>
                                                        </div>
                                                        <div class="item-title">Why gifted student needs to be taught formal writing</div>
                                                    </article>
                                                    <div class="posts-list">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col overflow-visible">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <aside class="aside-wrapper">
                                            <div class="add-post-wrapper flex-centering">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                            </div>
                                            <div class="full-list-wrapper">
                                                <a href="javascript:;" class="full-list-action">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"/>
                                                                    <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"/>
                                                                    <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"/>
                                                                    <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"/>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    <span>full list</span>
                                                </a>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col overflow-visible">
                        <div class="add-helper">
                            <span class="helper-txt">
                                From here you can add new posts
                            </span>
                            <img class="helper-arrow-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/from-here-arrow.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="invite-friend-wrapper">
                <div class="flex-row space-between vertical-center">
                    <div class="flex-col fix-col">
                        <div class="invite-content">
                            <h4 class="invite-txt">Invite a friend</h4>
                            <div class="to-add-txt">to add content to the sereis together</div>
                            <form class="invite-friend-form" method="post">
                                <div class="input-wrapper">
                                    <div class="flex-row margin-between">
                                        <div class="flex-col">
                                            <input type="text" name="emails" placeholder="Type friends emails with comma" required>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <button class="btn" type="submit">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <polygon style="fill:#5EBAE7;" points="490.452,21.547 16.92,235.764 179.068,330.053 179.068,330.053 "/>
                                                    <polygon style="fill:#36A9E1;" points="490.452,21.547 276.235,495.079 179.068,330.053 179.068,330.053 "/>
                                                    <rect x="257.137" y="223.122" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 277.6362 609.0793)" style="fill:#FFFFFF;" width="15.652" height="47.834"/>
                                                    <path style="fill:#1D1D1B;" d="M0,234.918l174.682,102.4L277.082,512L512,0L0,234.918z M275.389,478.161L190.21,332.858
                                                                l52.099-52.099l-11.068-11.068l-52.099,52.099L33.839,236.612L459.726,41.205L293.249,207.682l11.068,11.068L470.795,52.274
                                                                L275.389,478.161z"/>
                                                        </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <aside class="plane-wrapper">
                                <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/plane.png"
                                     alt="">
                            </aside>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="invite-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/invite_illustration.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <footer class="step-footer">
                <div class="button-wrapper">
                    <button class="create-new-btn">Create new series</button>
                </div>
                <div class="to-change-txt">To change experience's series,</div>
                <div class="order-embed-txt">order or embed go to</div>
                <div class="btn-wrapper">
                    <a href="my_series" class="my-series-link">My Series</a>
                </div>
            </footer>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/create_series-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/create_series-page.js?version=<?php echo time();?>"></script>

</body>
</html>
