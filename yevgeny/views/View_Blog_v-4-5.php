<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.11.2018
 * Time: 12:23
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="strPost_title">
    <meta name="Description" content='$metaDes'>
    <meta property="og:title" content="strPost_title"/>
    <meta property="og:image" content="strPost_featuredimage"/>
    <meta property="og:description" content='$metaDes'/>

    <title>Title</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/view_blog-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostNotes/PostNotes.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/view-blog-page.css?version=<?php echo time();?>" />

</head>
<body>
<div class="site-wrapper page page-view-blog <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="view-blog-header">
            <div class="d-none d-xs-block series-mobile-title">
                <span class="series-title">Series Name</span> | <span class="day-wrapper">day <span class="day-value">1</span></span>
            </div>
            <div class="flex-row vertical-center margin-between d-xs-block">
                <div class="flex-col mb-xs-1">
                    <div class="flex-row vertical-center margin-between align-items-xs-start">
                        <div class="flex-col fix-col">
                            <div class="listing-toggle flex-centering">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"/>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"/>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"/>
                                </g>
                                    <g>
                                        <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"/>
                                        <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"/>
                                        <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"/>
                                    </g>
                            </svg>
                                <div class="unread-posts-count"></div>
                            </div>
                        </div>
                        <div class="flex-col">
                            <div class="title-wrapper">
                            <span class="blog-title">
                                Why gifted students needs to be taught formal writing
                            </span><span class="d-xs-none"> - <span class="series-title">Series Name</span> | <span class="day-wrapper">day <span class="day-value">1</span></span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="flex-row vertical-center margin-between">
                        <div class="flex-col fg-0 overflow-hidden">
                            <div class="choose-lang-wrapper">
                                <a href="javascript:;" class="play-btn"></a>
                                <label>Choose Language</label>
                                <select name="speaker_lang"></select>
                            </div>
                        </div>
                        <?php if ($from === 'subscription'): ?>
                            <div class="flex-col fix-col">
                                <a href="javascript:;" class="action-item complete-action">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px" viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title>Rectangle 4</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id="Page-1" sketch:type="MSPage">
                                            <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"></path>
                                            </g>
                                        </g>
                                                        </svg>
                                </a>
                            </div>
                        <?php endif; ?>
                        <div class="flex-col fix-col">
                            <div class="dropdown">
                                <button class="action-item more-action dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
                                                                <g>
                                                                    <path d="M30,16c4.411,0,8-3.589,8-8s-3.589-8-8-8s-8,3.589-8,8S25.589,16,30,16z"></path>
                                                                    <path d="M30,44c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,44,30,44z"></path>
                                                                    <path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z"></path>
                                                                </g>
                                                                </svg>
                                </button>
                                <ul class="dropdown-menu more-drop-down">
                                    <?php if ($from === 'subscription'): ?>
                                        <li>
                                            <a href="javascript:;" class="action-item favorite-action">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                                            <polygon points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                                                81.485,416.226 106.667,269.41 0,165.436 147.409,144.017"></polygon>
                                                        </svg>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <li>
                                        <a href="javascript:;" class="action-item share-action">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 525.152 525.152" style="enable-background:new 0 0 525.152 525.152;" xml:space="preserve">
                                                             <g>
                                                                 <path d="M420.735,371.217c-20.021,0-37.942,7.855-51.596,20.24L181.112,282.094c1.357-6.061,2.407-12.166,2.407-18.468
                                                                    c0-6.302-1.072-12.385-2.407-18.468l185.904-108.335c14.179,13.129,32.931,21.334,53.719,21.334
                                                                    c43.828,0,79.145-35.251,79.145-79.079C499.88,35.338,464.541,0,420.735,0c-43.741,0-79.079,35.338-79.079,79.057
                                                                    c0,6.389,1.072,12.385,2.407,18.468L158.158,205.947c-14.201-13.194-32.931-21.378-53.741-21.378
                                                                    c-43.828,0-79.145,35.317-79.145,79.057s35.317,79.079,79.145,79.079c20.787,0,39.54-8.206,53.719-21.334l187.698,109.604
                                                                    c-1.291,5.58-2.101,11.4-2.101,17.199c0,42.45,34.594,76.979,76.979,76.979c42.428,0,77.044-34.507,77.044-76.979
                                                                    S463.163,371.217,420.735,371.217z"></path>
                                                             </g>
                                                        </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <?php require ASSETS_PATH . '/components/PostCopyEmbed/PostCopyEmbed.html';?>
                                    </li>
                                    <?php if ($from === 'subscription'): ?>
                                        <li>
                                            <a href="editseries?id=157" target="_blank" class="action-item edit-action">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                    <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                    <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                        l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="action-item trash-action">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="774.266px" height="774.266px" viewBox="0 0 774.266 774.266" style="enable-background:new 0 0 774.266 774.266;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M640.35,91.169H536.971V23.991C536.971,10.469,526.064,0,512.543,0c-1.312,0-2.187,0.438-2.614,0.875
                                                                C509.491,0.438,508.616,0,508.179,0H265.212h-1.74h-1.75c-13.521,0-23.99,10.469-23.99,23.991v67.179H133.916
                                                                c-29.667,0-52.783,23.116-52.783,52.783v38.387v47.981h45.803v491.6c0,29.668,22.679,52.346,52.346,52.346h415.703
                                                                c29.667,0,52.782-22.678,52.782-52.346v-491.6h45.366v-47.981v-38.387C693.133,114.286,670.008,91.169,640.35,91.169z
                                                                 M285.713,47.981h202.84v43.188h-202.84V47.981z M599.349,721.922c0,3.061-1.312,4.363-4.364,4.363H179.282
                                                                c-3.052,0-4.364-1.303-4.364-4.363V230.32h424.431V721.922z M644.715,182.339H129.551v-38.387c0-3.053,1.312-4.802,4.364-4.802
                                                                H640.35c3.053,0,4.365,1.749,4.365,4.802V182.339z"></path>
                                                            <rect x="475.031" y="286.593" width="48.418" height="396.942"></rect>
                                                            <rect x="363.361" y="286.593" width="48.418" height="396.942"></rect>
                                                            <rect x="251.69" y="286.593" width="48.418" height="396.942"></rect>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main class="main-content">
            <div class="view-blog-body">
                <div class="body-wrapper">
                    <div class="type-pay blog-type sample" hidden>
                        <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
                    </div>
                    <div class="type-0 subscription-type sample" hidden>
                        <div class="audiogallery">
                            <div class="items">
                                <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="type-2 subscription-type sample" hidden>
                        <div class="video-wrapper">
                            <video class="video-js" controls width="960" height="540"></video>
                        </div>
                    </div>
                    <div class="type-menu subscription-type sample" hidden>
                        <?php require_once ASSETS_PATH . '/components/MenuOptionsBlog/MenuOptionsBlog.html';?>
                    </div>
                    <div class="type-8 subscription-type sample" hidden></div>
                    <div class="type-10 subscription-type sample" hidden>
                    </div>
                    <div class="type-other subscription-type sample" hidden>
                    </div>
                    <aside class="blog-duration">
                        waiting...
                    </aside>
                    <nav class="circle-nav-wrapper">
                        <div class="circle-nav-toggle">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                        <g>
                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"/>
                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"/>
                        </g>
                    </svg>
                        </div>
                        <div class="circle-nav-panel"></div>
                        <ul class="circle-nav-menu">
                            <li class="circle-nav-item circle-nav-item-1">
                                <a class="facebook-link" href="https://www.facebook.com/sharer/sharer.php?u=test" target="_blank">
                                    <svg version="1.1" id="facebook-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="30px" height="30px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;"
                                         xml:space="preserve">
                                    <g><path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803
                                            c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654
                                            c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246
                                            c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"/>
                                    </g>
                                </svg><span>Facebook</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-2">
                                <a class="twitter-link" href="https://twitter.com/intent/tweet?url=text" target="_blank">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512.002px" height="512.002px" viewBox="0 0 512.002 512.002" style="enable-background:new 0 0 512.002 512.002;"xml:space="preserve">
                                    <g>
                                        <path d="M512.002,97.211c-18.84,8.354-39.082,14.001-60.33,16.54c21.686-13,38.342-33.585,46.186-58.115
                                            c-20.299,12.039-42.777,20.78-66.705,25.49c-19.16-20.415-46.461-33.17-76.674-33.17c-58.011,0-105.042,47.029-105.042,105.039
                                            c0,8.233,0.929,16.25,2.72,23.939c-87.3-4.382-164.701-46.2-216.509-109.753c-9.042,15.514-14.223,33.558-14.223,52.809
                                            c0,36.444,18.544,68.596,46.73,87.433c-17.219-0.546-33.416-5.271-47.577-13.139c-0.01,0.438-0.01,0.878-0.01,1.321
                                            c0,50.894,36.209,93.348,84.261,103c-8.813,2.399-18.094,3.687-27.674,3.687c-6.769,0-13.349-0.66-19.764-1.888
                                            c13.368,41.73,52.16,72.104,98.126,72.949c-35.95,28.176-81.243,44.967-130.458,44.967c-8.479,0-16.84-0.496-25.058-1.471
                                            c46.486,29.807,101.701,47.197,161.021,47.197c193.211,0,298.868-160.062,298.868-298.872c0-4.554-0.104-9.084-0.305-13.59
                                            C480.111,136.775,497.92,118.275,512.002,97.211z"/>
                                    </g>
                                </svg><span>Twitter</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-3">
                                <a class="linkedin-link" href="https://www.linkedin.com/shareArticle?mini=true&url=test" target="_blank">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="430.117px" height="430.117px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
                                         xml:space="preserve">
                                    <g>
                                        <path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
                                        c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
                                        v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
                                        C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
                                        c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
                                         M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
                                    </g></svg><span>Linkedlin</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-4">
                                <a class="google-link" href="https://plus.google.com/share?url=test" target="_blank">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="96.828px" height="96.827px" viewBox="0 0 96.828 96.827" style="enable-background:new 0 0 96.828 96.827;"
                                         xml:space="preserve">
                                        <g>
                                            <path d="M62.617,0H39.525c-10.29,0-17.413,2.256-23.824,7.552c-5.042,4.35-8.051,10.672-8.051,16.912
                                                c0,9.614,7.33,19.831,20.913,19.831c1.306,0,2.752-0.134,4.028-0.253l-0.188,0.457c-0.546,1.308-1.063,2.542-1.063,4.468
                                                c0,3.75,1.809,6.063,3.558,8.298l0.22,0.283l-0.391,0.027c-5.609,0.384-16.049,1.1-23.675,5.787
                                                c-9.007,5.355-9.707,13.145-9.707,15.404c0,8.988,8.376,18.06,27.09,18.06c21.76,0,33.146-12.005,33.146-23.863
                                                c0.002-8.771-5.141-13.101-10.6-17.698l-4.605-3.582c-1.423-1.179-3.195-2.646-3.195-5.364c0-2.672,1.772-4.436,3.336-5.992
                                                l0.163-0.165c4.973-3.917,10.609-8.358,10.609-17.964c0-9.658-6.035-14.649-8.937-17.048h7.663c0.094,0,0.188-0.026,0.266-0.077
                                                l6.601-4.15c0.188-0.119,0.276-0.348,0.214-0.562C63.037,0.147,62.839,0,62.617,0z M34.614,91.535
                                                c-13.264,0-22.176-6.195-22.176-15.416c0-6.021,3.645-10.396,10.824-12.997c5.749-1.935,13.17-2.031,13.244-2.031
                                                c1.257,0,1.889,0,2.893,0.126c9.281,6.605,13.743,10.073,13.743,16.678C53.141,86.309,46.041,91.535,34.614,91.535z
                                                 M34.489,40.756c-11.132,0-15.752-14.633-15.752-22.468c0-3.984,0.906-7.042,2.77-9.351c2.023-2.531,5.487-4.166,8.825-4.166
                                                c10.221,0,15.873,13.738,15.873,23.233c0,1.498,0,6.055-3.148,9.22C40.94,39.337,37.497,40.756,34.489,40.756z"/>
                                            <path d="M94.982,45.223H82.814V33.098c0-0.276-0.225-0.5-0.5-0.5H77.08c-0.276,0-0.5,0.224-0.5,0.5v12.125H64.473
                                                c-0.276,0-0.5,0.224-0.5,0.5v5.304c0,0.275,0.224,0.5,0.5,0.5H76.58V63.73c0,0.275,0.224,0.5,0.5,0.5h5.234
                                                c0.275,0,0.5-0.225,0.5-0.5V51.525h12.168c0.276,0,0.5-0.223,0.5-0.5v-5.302C95.482,45.446,95.259,45.223,94.982,45.223z"/>
                                        </g></svg><span>Google</span>
                                </a>
                            </li>
                            <li class="circle-nav-item circle-nav-item-5">
                                <a class="mail-link" href="mailto:nbaca@mtnlegal.com?&subject=welcome">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" style="enable-background:new 0 0 79.536 79.536;"
                                         xml:space="preserve">
                                    <g>
                                        <path style="fill:#010002;" d="M39.773,1.31L0,31.004v47.222h79.536V31.004L39.773,1.31z M28.77,22.499
                                        c1.167-2.133,2.775-3.739,4.815-4.805c2.035-1.075,4.357-1.616,6.983-1.616c2.214,0,4.191,0.435,5.921,1.292
                                        c1.729,0.87,3.045,2.094,3.967,3.687c0.9,1.595,1.367,3.334,1.367,5.217c0,2.247-0.694,4.279-2.082,6.097
                                        c-1.74,2.292-3.961,3.436-6.68,3.436c-0.732,0-1.279-0.122-1.654-0.38c-0.365-0.262-0.621-0.632-0.743-1.129
                                        c-1.022,1.012-2.231,1.52-3.589,1.52c-1.465,0-2.679-0.507-3.643-1.509c-0.966-1.012-1.447-2.361-1.447-4.031
                                        c0-2.084,0.578-3.966,1.743-5.672c1.416-2.084,3.218-3.13,5.424-3.13c1.571,0,2.731,0.601,3.475,1.805l0.331-1.468h3.5
                                        l-1.998,9.479c-0.125,0.606-0.187,0.986-0.187,1.163c0,0.228,0.052,0.38,0.149,0.497c0.099,0.111,0.223,0.165,0.357,0.165
                                        c0.436,0,0.979-0.248,1.646-0.769c0.901-0.663,1.627-1.574,2.181-2.695c0.554-1.129,0.839-2.299,0.839-3.508
                                        c0-2.165-0.782-3.977-2.352-5.445c-1.573-1.45-3.77-2.185-6.578-2.185c-2.393,0-4.417,0.487-6.077,1.468
                                        c-1.66,0.966-2.913,2.343-3.765,4.114c-0.839,1.76-1.258,3.607-1.258,5.52c0,1.856,0.479,3.552,1.411,5.074
                                        c0.945,1.533,2.26,2.641,3.956,3.345c1.696,0.697,3.643,1.046,5.828,1.046c2.097,0,3.909-0.293,5.432-0.881
                                        c1.522-0.587,2.739-1.457,3.666-2.641h2.807c-0.88,1.792-2.227,3.192-4.049,4.215c-2.092,1.163-4.64,1.74-7.644,1.74
                                        c-2.918,0-5.426-0.487-7.542-1.468c-2.121-0.986-3.689-2.434-4.73-4.35c-1.028-1.918-1.535-4.008-1.535-6.268
                                        C27.017,26.952,27.595,24.64,28.77,22.499z M2.804,31.941l29.344,19.68L2.804,74.333V31.941z M5.033,75.844l34.74-26.885
                                        l34.729,26.885H5.033z M76.729,74.333L47.391,51.621l29.339-19.68V74.333z M41.205,24.661c0.466,0.531,0.699,1.295,0.699,2.292
                                        c0,0.891-0.174,1.856-0.513,2.879c-0.334,1.036-0.743,1.826-1.209,2.361c-0.318,0.375-0.658,0.652-0.992,0.826
                                        c-0.439,0.249-0.906,0.37-1.41,0.37c-0.674,0.006-1.23-0.264-1.691-0.794c-0.45-0.531-0.673-1.346-0.673-2.465
                                        c0-0.839,0.158-1.805,0.487-2.889c0.329-1.088,0.81-1.916,1.453-2.509c0.647-0.588,1.346-0.881,2.1-0.881
                                        C40.162,23.856,40.749,24.125,41.205,24.661z"/>
                                    </g></svg><span>Opened-Email-Envelope</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="download-pdf-wrap">
                    <button class="site-green-btn download-as-pdf-btn">Download as PDF</button>
                </div>
                <aside class="note-wrapper">
                    <?php require_once ASSETS_PATH . '/components/PostNotes/PostNotes.html';?>
                </aside>
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <div class="slider-nav flex-centering arrow-left">
                        <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-slider-arrow-left.png" />
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <div class="slider-nav flex-centering arrow-right">
                        <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-slider-arrow-right.png" />
                    </div>
                </div>
            </div>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
    <section class="listing-posts" id="listing-posts">
        <header class="listing-posts-header">
            <div class="title-wrapper">
                <span class="series-title">
                    Developing language skills
                </span>
                <div class="posts-count common-radius-aside"><span class="count-value"><?php echo $postsCount;?></span> posts</div>
            </div>
            <?php require_once ASSETS_PATH . '/components/SeriesOwners/SeriesOwners.html';?>
            <a href="javascript:;" class="section-close"></a>
        </header>
        <div class="listing-posts-content">
            <li class="item sample" hidden>
                <div class="flex-row margin-between">
                    <div class="flex-col fix-col">
                        <img class="item-img" alt="" />
                    </div>
                    <div class="flex-col fix-col">
                        <div class="item-view-states-wrapper">
                            <svg version="1.1" class="complete-state view-state" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px" viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title>Rectangle 4</title>
                                <desc>Created with Sketch.</desc>
                                <g id="Page-1" sketch:type="MSPage">
                                    <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                        <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"></path>
                                    </g>
                                </g>
                                                        </svg>
                            <div class="current-state view-state"></div>
                            <div class="normal-state view-state"></div>
                        </div>
                    </div>
                    <div class="flex-col">
                        <div class="title-wrapper">
                            <aside class="media-icon-wrapper">
                                <i class="fa fa-youtube-play video-icon"></i>
                                <i class="fa fa-volume-up audio-icon"></i>
                                <i class="fa fa-file-text text-icon"></i>
                            </aside>
                            <a href="javascript:;" class="item-title">Great breakfasts</a> | <span class="item-duration">waiting...</span>
                        </div>
                    </div>
                </div>
            </li>
            <div class="new-title">New</div>
            <ul class="unread-posts-list">
            </ul>
            <div class="all-title">All</div>
            <?php require_once ASSETS_PATH . '/components/SeriesStructureTree/SeriesStructureTree.php';?>
        </div>
    </section>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    var from = <?php echo json_encode($from);?>;
    var series = <?php echo json_encode($series);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var initialPostData = <?php echo json_encode($postData);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var formFields = <?php echo json_encode($formFields);?>;
    var stripeApi_pKey = <?php echo json_encode($stripeApi_pKey);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostNotes/PostNotes.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/view_blog-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/plugins/touch/touch.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/view-blog-page.js?version=<?php echo time();?>"></script>
</body>
</html>