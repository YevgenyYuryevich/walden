<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.11.2018
 * Time: 11:51
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Browse - walden</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my-profile-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-favorite <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <div class="filter-wrapper">
            <div class="filter-content">
                <h3 class="your-profile-title">My Profile</h3>
                <div class="general-information d-xs-none">General information</div>
            </div>
        </div>
        <main class="main-content">
            <form method="post" class="profile-form" enctype="multipart/form-data">
                <div class="flex-row space-between flex-wrap justify-content-xs-center">
                    <div class="flex-col">
                        <div class="contact-info-wrapper section-wrapper">
                            <h3 class="contact-info-title section-title">Contact info</h3>
                            <div class="form-group">
                                <label for="first-name" class="control-label required">First name</label>
                                <input placeholder="Name" type="text" id="first-name" name="f_name" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label for="last-name" class="control-label required">Last name</label>
                                <input placeholder="Name" type="text" id="last-name" name="l_name" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label for="e-mail" class="control-label required">E-mail</label>
                                <input placeholder="info@gmail.com" type="text" id="e-mail" name="email" class="form-control" required/>
                            </div>
                        </div>
                        <div class="social-link-wrapper section-wrapper">
                            <h3 class="social-link-title section-title">Social Link</h3>
                            <div class="form-group">
                                <label for="social-facebook" class="control-label">Facebook</label>
                                <input placeholder="Social Facebook" type="text" id="social-facebook" name="social_facebook" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="last-name" class="control-label">Twitter</label>
                                <input placeholder="Social Twitter" type="text" id="social-twitter" name="social_twitter" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="social-pinterest" class="control-label">Pinterest</label>
                                <input placeholder="Social Pinterest" type="text" id="social-pinterest" name="social_pinterest" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="social-linkedin" class="control-label">Linkedin</label>
                                <input placeholder="Social Linkdin" type="text" id="social-linkedin" name="social_linkedin" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col">
                        <div class="section-wrapper">
                            <h3 class="section-title">About</h3>
                            <div class="multi-line-background">
                                <textarea class="about-input" rows="4" required placeholder="tell people about yourself" name="about_me"></textarea>
                                <div class="background-placeholder"></div>
                            </div>
                        </div>
                        <div class="section-wrapper">
                            <h3 class="section-title">Security</h3>
                            <div class="form-group">
                                <label for="current-password" class="control-label">Current password:</label>
                                <input placeholder="current password" type="password" id="current-password" name="current_password" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="new-password" class="control-label">New password:</label>
                                <input placeholder="new password" type="password" id="new-password" name="new_password" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="cover-image-wrapper">
                            <div class="img-wrapper">
                                <img class="user-image" src="<?php  echo BASE_URL;?>/assets/images/global-icons/user-avatar.svg" />
                            </div>
                            <div class="cloud-img-wrapper">
                                <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                            </div>
                            <div>Drag Image here to upload</div>
                            <input class="form-control" type="file" name="image"/>
                            <a href="javascript:;" class="delete-action">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"></polygon>
                                            <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"></path>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="submit-wrapper">
                    <button type="submit" class="submit-btn btn">Submit</button>
                </div>
                <div class="cancel-wrapper">
                    <button type="button" class="cancel-btn btn">Cancel</button>
                </div>
            </form>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script>
    var user = <?php echo json_encode($user);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my-profile-page.js?version=<?php echo time();?>"></script>

</body>
</html>
