<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 24.12.2018
 * Time: 18:57
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - walden</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/add_posts-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostsStructureExplainer/PostsStructureExplainer.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostsStructure/PostsStructure.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormField/FormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostCreator/PostCreator.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/add_posts-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="content-header">
            <div class="content-header-content">
                <div class="flex-row align-items-end space-between">
                    <div class="flex-col fix-col">
                        <div class="title-wrapper">
                            <h2 class="page-title">Create experience</h2>
                            <div class="step-name">Add posts</div>
                            <div class="step-wrapper">
                                step <span class="step-value">1</span>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col d-xs-none min-width-0">
                        <div class="step-progress-wrapper step-progress-1">
                            <div class="progress-wrapper">
                                <div class="progress-item step-1 active right-dotted">
                                    <div class="step-wrapper">step <span class="step-value">1</span></div>
                                </div>
                                <div class="progress-item step-2 right-dotted">
                                    <div class="step-wrapper">step <span class="step-value">2</span></div>
                                </div>
                                <div class="progress-item step-3">
                                    <div class="step-wrapper">step <span class="step-value">3</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <aside class="posts-count-box flex-centering align-items-end">
                            <span class="posts-count">0</span>
                        </aside>
                    </div>
                </div>
            </div>
        </header>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="step-1-select-from">
                <div class="step-1-select-from-pane step-pane">
                    <div class="pane-title">Let’s add great content to your experience</div>
                    <div class="pane-sub-title">What great content would you like to add first?</div>
                    <div class="flex-row d-xs-block">
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-scratch" data-toggle="tab" class="from-item item-from-scratch">
                                <div class="item-img-wrapper">
                                    <img class="item-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromScratch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Content from an Idea</div>
                                <div class="item-sub-title">Craft your idea from scratch.</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-search" data-toggle="tab" class="from-item item-from-search">
                                <div class="item-img-wrapper">
                                    <img class="item-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromSearch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Content from the web</div>
                                <div class="item-sub-title">Select existing content from youtube, blogs, and podcasts</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0">
                            <a href="#step-1-add-from-upload" data-toggle="tab" class="from-item item-from-upload">
                                <div class="item-img-wrapper">
                                    <img class="item-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/BulkUpload.svg"
                                         alt="">
                                </div>
                                <div class="item-title">Content from existing files and your favorite URLs</div>
                                <div class="item-sub-title">Upload docs, pdfs, videos, and audio files.</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-from-scratch">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-from-scratch-pane step-pane">
                    <div class="pane-sub-title">Let’s add great content to your experience</div>
                    <div class="identify-board">
                        <div class="flex-row vertical-center d-xs-block">
                            <div class="flex-col fix-col">
                                <div class="board-img-wrapper">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/PostFromScratch.svg" alt="" />
                                    <div class="board-title">post from scratch</div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="board-txt">
                                    Creating content from scratch is just like writing a blog
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="" class="control-label">Name your post:</label>
                            <input type="text" class="form-control" name="strPost_title" placeholder="type the name here"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Subtitle:</label>
                            <input type="text" class="form-control" name="strPost_subtitle" placeholder="type the name here"/>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Describe it:</label>
                            <textarea name="strPost_body" class="form-control" rows="3" required></textarea>
                        </div>
                        <div class="flex-row flex-wrap margin-between d-xs-block">
                            <div class="flex-col fb-0 mr-2 mr-xs-0">
                                <div class="form-group">
                                    <label for="" class="control-label">Post Type:</label>
                                    <div class="dropdown" data-value="7">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                            <span>Text</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:;" data-value="0">Audio</a></li>
                                            <li><a href="javascript:;" data-value="2">Video</a></li>
                                            <li hidden><a href="javascript:;" data-value="7">Text</a></li>
                                            <li><a href="javascript:;" data-value="8">Image</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-col fb-0">
                                <div class="form-group">
                                    <label for="" class="control-label">
                                        Money Saved, $ <span>(optional)</span>:
                                    </label>
                                    <input type="text" class="form-control" name="intPost_savedMoney" placeholder="0"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Keywords (optional):</label>
                            <input type="text" class="form-control" name="strPost_keywords" placeholder="subscribers will find your posts by keywords"/>
                            <div class="terms-container flex-row margin-between flex-wrap">
                                <div class="flex-col fix-col term-item sample" hidden>#design</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload Cover Image for this Post (optional)</label>
                            <div class="cover-image-wrapper">
                                <div class="flex-row margin-between horizon-center vertical-center">
                                    <div class="flex-col fix-col">
                                        <div class="img-wrapper">
                                            <img src="assets/images/global-icons/cloud-uploading.png">
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="upload-helper-txt">Drag Image here to upload</div>
                                    </div>
                                </div>
                                <input class="form-control" type="file" name="strPost_featuredimage" />
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="save-btn">Create</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-from-search">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-from-search-pane step-pane">
                    <div class="pane-sub-title">Let’s add great content to your experience</div>
                    <div class="identify-board">
                        <div class="flex-row vertical-center d-xs-block">
                            <div class="flex-col fix-col">
                                <div class="board-img-wrapper">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/PostFromSearch.svg" alt="" />
                                    <div class="board-title">post from search</div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <form class="search-input-wrapper flex-row margin-between vertical-center">
                                    <input class="flex-col search-input" required placeholder="Type search term here...">
                                    <button class="search-submit-button fix-col">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <section class="search-section-wrapper" hidden>
                        <div class="your-search-title">Your Search Results</div>
                        <div class="search-container">
                            <div class="search-row sample" hidden>
                                <header class="search-row-header">
                                    <div class="flex-row vertical-center space-between">
                                        <div class="flex-col fix-col">
                                            <h2 class="from-name">from YouTube</h2>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <a href="javascript:;" class="items-found-wrapper">
                                                <span class="found-value">0</span> items found
                                            </a>
                                        </div>
                                    </div>
                                </header>
                                <div class="search-row-body">
                                    <div class="search-item sample" hidden>
                                        <div class="item-img-wrapper">
                                            <img class="item-img" />
                                            <aside class="blog-duration">
                                                waiting...
                                            </aside>
                                            <div class="hover-content flex-centering">
                                                <svg version="1.1" id="Layer_1"
                                                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                                                     viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
<title>Rectangle 4</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id="Page-1" sketch:type="MSPage">
                                                        <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                            <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
			c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
			c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                                        </g>
                                                    </g>
</svg>
                                            </div>
                                        </div>
                                        <a href="javascript:;" class="item-title">
                                            Daily recipes
                                        </a>
                                    </div>
                                    <div class="flex-row margin-between">
                                        <div class="flex-col fix-col overflow-visible">
                                            <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                            </a>
                                        </div>
                                        <div class="flex-col fb-0">
                                            <div class="search-items-list">
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col overflow-visible">
                                            <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="add-selected-btn">Add selected</button>
                        </div>
                        <div class="still-not-find-txt">Still didn't find what were looking for?</div>
                        <div class="btn-wrapper">
                            <button class="load-more-btn btn">Load more</button>
                        </div>
                    </section>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none" hidden>
                    <a href="#step-2-arrange" data-toggle="tab" class="slider-nav flex-centering arrow-right">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-from-upload">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-from-upload-pane step-pane">
                    <div class="pane-sub-title">Let’s add great content to your experience</div>
                    <div class="identify-board">
                        <div class="flex-row vertical-center d-xs-block">
                            <div class="flex-col fix-col">
                                <div class="board-img-wrapper">
                                    <img src="<?php echo BASE_URL;?>/assets/images/global-icons/BulkUpload.svg" alt="" />
                                    <div class="board-title">bulk upload</div>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="upload-input-wrapper">
                                    <div class="file-input-wrapper">
                                        <div class="drag-urls-txt">
                                            Drag URLs or files here to upload or click here to select your files
                                        </div>
                                        <div class="cloud-wrapper">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 184.979 184.979" style="enable-background:new 0 0 184.979 184.979;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path style="fill:#fff;" d="M121.539,122.24c0.585,0.585,1.349,0.871,2.112,0.871c0.764,0,1.528-0.292,2.112-0.871
                                                            c1.164-1.164,1.164-3.055,0-4.219L94.605,86.869c0,0,0,0-0.006-0.006l-2.106-2.106l-2.106,2.106c0,0,0,0-0.006,0.006
                                                            l-31.159,31.153c-1.164,1.164-1.164,3.055,0,4.219c1.164,1.164,3.055,1.164,4.219,0l26.069-26.063v52.556
                                                            c0,1.647,1.337,2.983,2.983,2.983c1.647,0,2.983-1.337,2.983-2.983V96.177L121.539,122.24z"/>
                                                        <path style="fill:#fff;" d="M151.057,67.673C143.712,47.01,124.32,33.25,102.236,33.25c-20.812,0-39.531,12.518-47.628,31.493
                                                            c-3.813-1.766-7.96-2.685-12.172-2.685c-16.051,0-29.118,13.044-29.148,29.088C4.941,97.531,0,107.448,0,117.998
                                                            c0,18.593,15.168,33.719,34.31,33.725c0.215,0,0.43,0.006,0.621,0.006c0.203,0,0.382,0,0.525-0.012h31.445
                                                            c1.647,0,2.983-1.337,2.983-2.983s-1.337-2.983-2.983-2.983H35.3c-0.292,0.018-0.597,0.006-0.889,0l-0.609-0.006
                                                            c-15.347,0-27.835-12.447-27.835-27.752c0-9.147,4.511-17.722,12.065-22.925l1.372-1.366l-0.149-2.494
                                                            c0-12.781,10.4-23.181,23.181-23.181c4.284,0,8.491,1.205,12.166,3.479l3.156,1.957l1.229-3.509
                                                            c6.432-18.384,23.808-30.729,43.254-30.729c20.073,0,37.639,12.847,43.707,31.976l0.555,1.748l1.814,0.292
                                                            c17.781,2.876,30.694,18.032,30.694,36.04c0,20.114-16.403,36.475-36.559,36.475h-24.375c-1.647,0-2.983,1.337-2.983,2.983
                                                            s1.337,2.983,2.983,2.983h24.375c23.45,0,42.526-19.04,42.526-42.442C184.973,88.933,170.814,71.719,151.057,67.673z"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <input type="file" class="file-input" />
                                    </div>
                                    <div class="url-input-wrapper">
                                        <aside class="enter-icon-wrapper flex-centering">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 26 26" style="enable-background:new 0 0 26 26;" xml:space="preserve">
                                                    <g>
                                                        <path d="M25,2H9C8.449,2,8,2.449,8,3c0,0,0,7,0,9s-2,2-2,2H1c-0.551,0-1,0.449-1,1v8c0,0.551,0.449,1,1,1h24
                                                            c0.551,0,1-0.449,1-1V3C26,2.449,25.551,2,25,2z M22,14c0,1.436-1.336,4-4,4h-3.586l1.793,1.793c0.391,0.391,0.391,1.023,0,1.414
                                                            C16.012,21.402,15.756,21.5,15.5,21.5s-0.512-0.098-0.707-0.293l-3.5-3.5c-0.391-0.391-0.391-1.023,0-1.414l3.5-3.5
                                                            c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L14.414,16H18c1.398,0,2-1.518,2-2v-2c0-0.553,0.447-1,1-1s1,0.447,1,1V14z"></path>
                                                    </g>
                                                </svg>
                                        </aside>
                                        <input type="text" class="form-control url-input" placeholder="Paste URL" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pane-body">
                        <div class="upload-items-title-wrapper">
                            <div class="flex-row space-between vertical-center">
                                <div class="flex-col">
                                    <h2 class="uploaded-items-txt">Uploaded Items</h2>
                                </div>
                                <div class="flex-col fix-col">
                                    <div class="uploaded-cnt bottom-underline">
                                        <span class="cnt-value">0</span> items uploaded
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="drag-title-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/move.png" alt=""> <span class="drag-title-txt">Drag on items to change the order</span>
                        </div>
                        <div class="items-container">
                            <div class="upload-term-item sample" hidden>
                                <div class="flex-row margin-between vertical-center flex-wrap">
                                    <div class="flex-col fix-col">
                                        <a class="item-action delete-action" href="javascript:;">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                        </a>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="item-img-wrapper">
                                            <img class="item-img">
                                            <input type="file" class="image-input" />
                                        </div>
                                    </div>
                                    <div class="flex-col">
                                        <div class="form-group">
                                            <input type="text" class="form-control item-title-input" />
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col data-generate-col">
                                        <button class="site-green-btn keep-btn">Keep format</button>
                                    </div>
                                    <div class="flex-col fix-col data-generate-col">
                                        <button class="site-green-btn convert-btn">Convert</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pane-footer">
                        <div class="button-wrapper">
                            <button class="save-btn" disabled>Upload all</button>
                        </div>
                        <div class="changed-txt">if you changed your mind to upload</div>
                        <div class="btn-wrapper">
                            <button class="cancel-all-btn btn">Cancel all</button>
                        </div>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none" hidden>
                    <a href="javascript:;" class="slider-nav flex-centering arrow-right">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-1-add-more">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                    </a>
                </div>
                <div class="step-1-add-more-pane step-pane">
                    <div class="pane-title">Looks great! Would you like to add more?</div>
                    <div class="pane-sub-title">(You can always do this later)</div>
                    <div class="flex-row d-xs-block">
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-scratch" data-toggle="tab" class="from-item item-from-scratch">
                                <div class="item-img-wrapper">
                                    <img class="item-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromScratch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">post from scratch</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0 mr-3 mr-xs-0 mb-xs-2">
                            <a href="#step-1-add-from-search" data-toggle="tab" class="from-item item-from-search">
                                <div class="item-img-wrapper">
                                    <img class="item-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/PostFromSearch.svg"
                                         alt="">
                                </div>
                                <div class="item-title">post from search</div>
                            </a>
                        </div>
                        <div class="flex-col fb-0 min-width-0">
                            <a href="#step-1-add-from-upload" data-toggle="tab" class="from-item item-from-upload">
                                <div class="item-img-wrapper">
                                    <img class="item-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/BulkUpload.svg"
                                         alt="">
                                </div>
                                <div class="item-title">bulk upload</div>
                            </a>
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <button class="go-next-btn" data-toggle="tab" data-target="#step-2-arrange">Go to next step</button>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="#step-2-arrange" data-toggle="tab" class="slider-nav flex-centering arrow-right">
                        <span>Step 2</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="step-2-arrange">
                <div class="slider-nav-wrapper left-wrapper d-xs-none">
                    <a href="javascript:;" class="slider-nav flex-centering arrow-left">
                        <div class="nav-action arrow-icon-wrapper arrow-left">
                        </div>
                        <span>Add More Content</span>
                    </a>
                </div>
                <div class="step-2-arrange-pane step-pane grid-structure-mode">
                    <div class="pane-title">Great work! Would you like to arrange your content?</div>
                    <div class="pane-sub-title">(You can always do this later)</div>
                    <?php require_once ASSETS_PATH . '/components/PostsStructureExplainer/PostsStructureExplainer.html';?>
                    <?php require_once ASSETS_PATH . '/components/PostsStructure/PostsStructure.php';?>
                    <div class="structure-footer">
                        <div class="button-wrapper">
                            <a href="preview_series?id=<?php echo $series['series_ID'];?>" class="go-next-btn" target="_blank">Finished. Let’s see it!</a>
                        </div>
                        <div class="actually-link-wrapper">
                            <a href="edit_series?id=<?php echo $series['series_ID'];?>" class="actually-link" target="_blank">Actually, let me edit it.</a>
                        </div>
                    </div>
                </div>
                <div class="slider-nav-wrapper right-wrapper d-xs-none">
                    <a href="preview_series?id=<?php echo $series['series_ID'];?>" class="slider-nav flex-centering arrow-right" target="_blank">
                        <span>View</span>
                        <div href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="virtual-links" hidden>
            <a href="#step-1-select-from" data-toggle="tab"></a>
            <a href="#step-1-add-from-scratch" data-toggle="tab"></a>
            <a href="#step-1-add-from-search" data-toggle="tab"></a>
            <a href="#step-1-add-from-upload" data-toggle="tab"></a>
            <a href="#step-1-add-more" data-toggle="tab"></a>
            <a href="#step-2-arrange" data-toggle="tab"></a>
            <a href="#step-3-share" data-toggle="tab"></a>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var series = <?php echo json_encode($series);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/add_posts-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/plugins/ace/build/src/ace.js"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostsStructureExplainer/PostsStructureExplainer.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostsStructure/PostsStructure.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormField/FormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostCreator/PostCreator.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/add_posts-page.js?version=<?php echo time();?>"></script>

</body>
</html>
