<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 27.11.2018
 * Time: 18:40
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Congratulations! You are now part of this experience.</title>
    <meta name="description" content="Make time for the things that you love and contribute your vision to make the world a better place.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/series_invitation_accepted-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SkypeLoader/SkypeLoader.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesConversation/SeriesConversation.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/series_invitation_accepted-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-series-invitation-accepted <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <main class="main-content">
            <div class="congratulations-wrapper">
                <div class="flex-row space-between">
                    <div class="flex-col">
                        <h1>Welcome on board!</h1>
                        <div class="series-name-wrapper">
                            You can now add, edit and enjoy the following experience
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none">
                        <a class="go-series-page-link" href="edit_series?id=<?php echo $series['series_ID'];?>">series page</a>
                    </div>
                </div>
            </div>
            <div class="d-none d-xs-block">
                <div class="series-header-mobile">
                    <div class="flex-row horizon-center margin-between vertical-center flex-wrap">
                        <div class="flex-col fix-col max-width-100">
                            <div class="series-title">Beginning Yoga</div>
                        </div>
                        <div class="flex-col fix-col">
                            <a href="add_posts?id=<?php echo $series['series_ID'];?>" target="_blank" class="add-post-wrapper flex-centering">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                                </g>
                                            </g>
                                        </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="series-item">
                <div class="flex-row">
                    <div class="flex-col fb-0">
                        <div class="series-content-wrapper">
                            <header class="series-content-header d-xs-none">
                                <div class="flex-row">
                                    <div class="flex-col">
                                        <h3 class="series-title"></h3>
                                    </div>
                                    <div class="flex-col fix-col overflow-visible">
                                        <div class="actions-container">
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col">
                                                    <a href="javascript:;" target="_blank" class="action-item edit-action">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                        <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                        <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                            l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col">
                                                    <a href="javascript:;" class="action-item">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                </g>
                                                            <g>
                                                                <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                                                                <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                                                                <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                                                            </g>
                            </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col overflow-visible">
                                                    <?php require_once ASSETS_PATH . '/components/SeriesCopyEmbed/SeriesCopyEmbed.html';?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            <div class="series-content-body">
                                <div class="flex-row margin-between d-xs-block">
                                    <div class="flex-col fb-0">
                                        <div class="posts-list-wrapper">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col fix-col overflow-visible d-xs-none">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                    </a>
                                                </div>
                                                <div class="flex-col fb-0">
                                                    <article class="post-item sample" hidden>
                                                        <header class="item-header">
                                                            <div class="flex-row vertical-center margin-between">
                                                                <div class="flex-col fix-col">
                                                                    <div class="media-icon-wrapper">
                                                                        <i class="fa fa-youtube-play video-icon"></i>
                                                                        <i class="fa fa-volume-up audio-icon"></i>
                                                                        <i class="fa fa-file-text text-icon"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-col">
                                                                    <div class="day-wrapper">
                                                                        <span>day</span> <span class="day-value">11</span>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <a class="item-action delete-action" href="javascript:;">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                             width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                    </a>
                                                                </div>
                                                                <div class="flex-col fix-col d-xs-none">
                                                                    <a class="item-action edit-action flex-centering" href="javascript:;">
                                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </header>
                                                        <div class="item-type-body">
                                                            <div class="blog-type type-0 sample" hidden>
                                                            </div>
                                                            <div class="blog-type type-2 sample" hidden>
                                                                <img alt="">
                                                            </div>
                                                            <div class="blog-type type-default sample" hidden>
                                                                Lorem lpsum is simply dummy text of the printing and type
                                                            </div>
                                                            <aside class="blog-duration">
                                                                waiting...
                                                            </aside>
                                                        </div>
                                                        <a href="javascript:;" class="item-title" target="_blank">Why gifted student needs to be taught formal writing</a>
                                                    </article>
                                                    <div class="posts-list">
                                                    </div>
                                                </div>
                                                <div class="flex-col fix-col overflow-visible d-xs-none">
                                                    <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <aside class="aside-wrapper">
                                            <a class="add-post-wrapper flex-centering d-xs-none" href="add_posts?id=<?php echo $series['series_ID'];?>" target="_blank">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                            </a>
                                            <div class="full-list-wrapper">
                                                <a href="javascript:;" class="full-list-action">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"/>
                                                                <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"/>
                                                                <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"/>
                                                                <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    <span>full list</span>
                                                </a>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col overflow-visible d-xs-none">
                        <div class="add-helper">
                            <span class="helper-txt">
                                From here you can add new posts
                            </span>
                            <img class="helper-arrow-img" src="<?php echo BASE_URL;?>/assets/images/global-icons/from-here-arrow.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="series-conversation-wrapper">
                <?php require ASSETS_PATH . '/components/SeriesConversation/SeriesConversation.php';?>
            </div>
            <div class="invite-friend-wrapper">
                <div class="flex-row space-between vertical-center d-xs-block">
                    <div class="flex-col fix-col">
                        <div class="invite-content">
                            <h4 class="invite-txt">Invite a friend</h4>
                            <div class="to-add-txt">to add content to the series together</div>
                            <form class="invite-friend-form" method="post">
                                <div class="input-wrapper">
                                    <div class="flex-row margin-between">
                                        <div class="flex-col">
                                            <input type="text" name="emails" placeholder="Type friend's emails with comma" required>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <button class="btn" type="submit">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <polygon style="fill:#5EBAE7;" points="490.452,21.547 16.92,235.764 179.068,330.053 179.068,330.053 "/>
                                                    <polygon style="fill:#36A9E1;" points="490.452,21.547 276.235,495.079 179.068,330.053 179.068,330.053 "/>
                                                    <rect x="257.137" y="223.122" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 277.6362 609.0793)" style="fill:#FFFFFF;" width="15.652" height="47.834"/>
                                                    <path style="fill:#1D1D1B;" d="M0,234.918l174.682,102.4L277.082,512L512,0L0,234.918z M275.389,478.161L190.21,332.858
                                                                l52.099-52.099l-11.068-11.068l-52.099,52.099L33.839,236.612L459.726,41.205L293.249,207.682l11.068,11.068L470.795,52.274
                                                                L275.389,478.161z"/>
                                                        </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <aside class="plane-wrapper">
                                <img src="<?php echo BASE_URL; ?>/assets/images/global-icons/plane.png"
                                     alt="">
                            </aside>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none">
                        <div class="invite-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/invite_illustration.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <footer class="step-footer d-xs-none">
                <div class="button-wrapper">
                    <a class="create-new-btn" href="create_series">Create new series</a>
                </div>
                <div class="to-change-txt">To change experience's series,</div>
                <div class="order-embed-txt">order or embed go to</div>
                <div class="btn-wrapper">
                    <a href="my_series" class="my-series-link">My Series</a>
                </div>
            </footer>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var series = <?php echo json_encode($series);?>;
    var invitedEmail = <?php echo json_encode($invitedEmail);?>;
    var invitationId = <?php echo json_encode($invitationId);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/series_invitation_accepted-global.js?version=<?php echo time();?>"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.1/firebase-database.js"></script>
<script src="<?php echo BASE_URL;?>/assets/config/js/firebase-config.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SkypeLoader/SkypeLoader.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesConversation/SeriesConversation.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/series_invitation_accepted-page.js?version=<?php echo time();?>"></script>

</body>
</html>