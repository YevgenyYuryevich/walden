<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 30.03.2018
 * Time: 23:29
 */
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="Daily Experiences for You">
    <meta name="Description" content="Find or create the daily experiences that enrich your life.">
    <title>Daily Experiences to Enrich your life - thegreyshirt</title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/explore-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/explore-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-explore">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Step 2: Enrich your life
                </div>
            </div>
            <header class="content-header">
                <div class="filter-wrapper">
                    <div class="flex-row vertical-center space-between flex-wrap">
                        <div class="flex-col fix-col">
                            <form class="form-inline search-form" method="post">
                                <label for="search-term">Search</label>&nbsp;&nbsp;
                                <input name="searchTerm" class="form-control" size="25" id="search-term" />
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </form>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="filter-item">
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col">
                                        <div class="field-name">Premium content:</div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="icon-wrapper" data-field="premium-filter">
                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="filter-item">
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col">
                                        <div class="field-name">Affiliates:</div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="icon-wrapper"  data-field = 'affiliate-filter'>
                                            <img class="active-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-active.png" />
                                            <img class="inactive-icon" src="<?php echo BASE_URL;?>/assets/images/global-icons/check-inactive.png" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <form class="form-inline sort-from" method="post">
                                <label for="sort-by">Sort By</label>&nbsp;&nbsp;
                                <select name="sortBy" class="form-control" id="sort-by">
                                    <option value="a-z">A&nbsp;&nbsp;-&nbsp;&nbsp;Z</option>
                                    <option value="z-a">Z&nbsp;&nbsp;-&nbsp;&nbsp;A</option>
                                </select>
                                <div class="new-series">
                                    <span>New</span> <a href="createseries?level=0"><svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="page-top-title">
                    Select solutions that will enrich your life. Content from these.<br>
                    solutions will be provided to you on a daily basis.
                </div>
                <div class="scroll-fix-rplc" hidden>
                    <div class="flex-row vertical-center space-between">
                        <div class="flex-col fix-col">
                            <a class="back-to-prev" href="javascript:;"><i class="glyphicon glyphicon-menu-left"></i></a>
                        </div>
                        <div class="flex-col">
                            <div class="rplc-content-wrp text-center">
                                <a href="createseries?level=1">Not finding what you want? Create the perfect experience for you. <svg aria-hidden="true" data-fa-processed="" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <div class="item-sample-wrapper" hidden>
                    <div>
                        <div class="item-inner">
                            <aside>
                                <div class="bottom-line"></div>
                                <svg class="star-item" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                    <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                        81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                                    </svg>
                            </aside>
                            <div class="img-wrapper">
                                <img src="" alt="" class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <div class="item-title"></div>
                            </div>
<!--                            <div class="buttons-wrapper">-->
<!--                                <a href="#" class="view-post btn btn-circle" target="_blank">View</a>-->
<!--                            </div>-->
                            <div class="buttons-wrapper">
                                <a href="#" class="preview-post btn btn-circle" target="_blank">Preview</a>
                            </div>
                            <div class="buttons-wrapper">
                                <div class="wrap">
                                    <button class="select-post">Join</button>
                                    <img src="../assets/images/check_arrow_2.svg" alt="">
                                    <svg width="42px" height="42px">
                                        <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                    </svg>
                                </div>
                            </div>
                            <div class="object_line"></div>
                        </div>
                    </div>
                </div>
                <section class="posts-block sample" hidden>
                    <div class="row">
                        <div class="col-md-10 col-sm-12"><h3 class="category-title"></h3></div>
                        <div class="col-md-2 col-sm-12"><div class="view-category-wrapper"><a class="view-category" href="javascript:;">View all</a></div></div>
                    </div>
                    <div class="tt-grid-wrapper after-clearfix">
                        <span class="pagging prev-pagging">
                            <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
                        </span>
                        <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                        </ul>
                        <span class="pagging next-pagging">
                            <i class="glyphicon glyphicon-play"></i>
                        </span>
                    </div>
                </section>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/explore-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initCategories = <?php echo json_encode($categories);?>;
    var isFirstVisit = <?php echo json_encode($isFirstVisit);?>;
    var stripeApi_pKey = <?php echo json_encode($stripeApi_pKey);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/explore-page.js?version=<?php echo time();?>"></script>

</body>
</html>
