<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 30.10.2018
 * Time: 01:13
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Browse - walden</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=1.0" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/login-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="page login-page">
    <div class="login-inner">
        <div class="auth-wrapper">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade <?php echo $type === 'signin' ? 'in active' : '';?>" id="login-tab">
                    <form action="" class="auth-form login-form">
                        <h4 class="form-name">
                            Log in
                        </h4>
                        <div class="alert alert-danger" role="alert" hidden>
                            username or password is wrong
                        </div>
                        <div class="form-group left-circle left-line">
                            <label for="login-username" class="control-label">E-mail</label>
                            <input type="text" id="login-username" class="form-control" placeholder="type your email or username" required/>
                        </div>
                        <div class="form-group left-circle mb-0">
                            <label for="login-password" class="control-label">Password</label>
                            <input type="password" id="login-password" class="form-control" placeholder="type password" required/>
                        </div>
                        <aside class="forgot-password">
                            <a href="#forgot-password" data-toggle="collapse">Forgot password?</a>
                            <div class="collapse" id="forgot-password">
                                <div class="form-group left-circle">
                                    <label for="login-reset-password">Username or email</label>
                                    <input type="text" placeholder="type your username or email" class="form-control" id="login-reset-password">
                                </div>
                                <div class="button-wrapper">
                                    <button type="button" class="auth-btn small reset-password">Send me</button>
                                </div>
                            </div>
                        </aside>
                        <div class="button-wrapper">
                            <button class="auth-btn">Log In</button>
                        </div>
                        <aside class="auth-helper">
                            <div class="auth-helper-text">
                                If you don't have account yet, please
                            </div>
                            <div class="helper-btn-wrapper">
                                <button class="helper-btn" data-toggle="tab" data-target="#signup-tab">
                                    Sign Up
                                </button>
                            </div>
                        </aside>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade <?php echo $type === 'register' ? 'in active' : '';?>" id="signup-tab">
                    <form action="" class="auth-form signup-form">
                        <h4 class="form-name">
                            Sign Up
                        </h4>
                        <div class="alert alert-danger" role="alert" hidden>
                            Something is wrong
                        </div>
                        <div class="form-group left-circle left-line">
                            <label for="signup-full-name" class="control-label">Full name</label>
                            <input type="text" id="signup-full-name" name="f_name" class="form-control" placeholder="type your full name" />
                        </div>
                        <div class="form-group left-circle left-line">
                            <label for="signup-email" class="control-label">E-mail</label>
                            <input type="email" id="signup-email" name="email" class="form-control" placeholder="type your email" required/>
                        </div>
                        <div class="form-group left-circle">
                            <label for="signup-password" class="control-label">Password</label>
                            <input type="password" id="signup-password" name="password" class="form-control" placeholder="type your password" required/>
                        </div>
                        <div class="button-wrapper">
                            <button class="auth-btn">Sign Up</button>
                        </div>
                        <aside class="auth-helper">
                            <div class="auth-helper-text">
                                If you have account already, please
                            </div>
                            <div class="helper-btn-wrapper">
                                <button class="helper-btn" data-toggle="tab" data-target="#login-tab">
                                    Log in
                                </button>
                            </div>
                        </aside>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade <?php echo $type === 'rest_pw' ? 'in active' : '';?>" id="reset-pwd-tab">
                    <form action="" class="auth-form reset-pwd-form">
                        <h4 class="form-name">
                            Reset Password
                        </h4>
                        <div class="alert alert-danger" role="alert" hidden>
                            username or password is wrong
                        </div>
                        <div class="form-group left-circle left-line">
                            <label for="reset-pwd" class="control-label">New Password</label>
                            <input type="text" id="reset-pwd" class="form-control" name="password" placeholder="type new password" required/>
                        </div>
                        <div class="form-group left-circle mb-0">
                            <label for="login-password" class="control-label">Repeat Password</label>
                            <input type="password" id="login-password"  name="re_password" class="form-control" placeholder="repeat password" required/>
                        </div>
                        <input name="temper" value="<?php echo $temper?>" hidden />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-loading" hidden>
    <div class="progress-container">
        <div class="progress-item sample" hidden>
            <div class="progress-txt">waiting...</div>
            <div class="progress-bar">
                <div class="loaded-bar"></div>
            </div>
        </div>
    </div>
    <div class="loading-lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>
<script>
    const CURRENT_PAGE = window.location.pathname.substr(1);
    const BASE_URL = window.location.protocol + "//" + window.location.host;
    const AFFILIATE_ID = <?php echo isset($_SESSION['affiliate_id']) ? json_encode($_SESSION['affiliate_id']) : -1;?>;
    const ACTION_URL = BASE_URL + window.location.pathname;
    var CLIENT_ID = -1;
    var prevPage = <?php echo json_encode($prevPage);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/login-page.js?version=<?php echo time();?>"></script>

</body>
</html>

