<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.02.2018
 * Time: 06:43
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="My favorite experiences on thegreyshirt">
    <meta name="Description" content="View all of the experiences that you liked on thegreyshirt">
    <title>My favorite experiences on thegreyshirt</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/favorites-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/favorites-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-favorites">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Favorites
                </div>
            </div>
            <header class="content-header">
                <div class="multi-select-series-wrapper">
                    <span>View Multiple</span>
                    <div class="dropdown">
                        <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select Series To View <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                        </ul>
                    </div>
                </div>
            </header>
            <main class="main-content">
                <div class="item-sample-wrapper" hidden>
                    <a>
                        <div class="item-inner">
                            <aside>
                                <div class="bottom-line"></div>
                            </aside>
                            <div class="img-wrapper">
                                <img class="item-img" />
                            </div>
                            <div class="title-wrapper">
                                <div class="item-title"></div>
                            </div>
                            <footer class="btns-container">
                                <div class="btn-wrapper view-btn">
                                    <img src="assets/images/global-icons/read.png"/>
                                </div>
                                <div class="btn-wrapper star-btn">
                                    <span class="favorite-wrapper favorited">
                                        <i class="fa fa-star fa-2x" aria-hidden="true"></i>
                                        <i class="fa fa-star-o fa-2x" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </footer>
                        </div>
                    </a>
                </div>
                <section class="posts-block sample" hidden>
                    <div class="row">
                        <div class="col-md-12 col-sm-12"><h3 class="sery-title"></h3></div>
                    </div>
                    <div class="tt-grid-wrapper after-clearfix">
                        <span class="pagging prev-pagging">
                            <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
                        </span>
                        <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                            <li class="tt-empty"></li>
                        </ul>
                        <span class="pagging next-pagging">
                            <i class="glyphicon glyphicon-play"></i>
                        </span>
                    </div>
                </section>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/favorites-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initial_purchasedSeries = <?php echo json_encode($purchasedSeries);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/favorites-page.js?version=<?php echo time();?>"></script>

</body>
</html>