<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 25.10.2018
 * Time: 11:01
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="<?php echo $series['strSeries_title'];?>" />
    <meta name="Description" content='<?php echo $series['strSeries_description'];?>. Contribute to this experience to help others or join it to help yourself.' />
    <meta property="og:image" content="<?php echo $shareData['image'];?>" />
    <meta property="og:title" content="<?php echo $series['strSeries_title'];?>" />
    <?php if (isset($shareData['image_width'])): ?>
        <meta property="og:image:width" content="<?php echo $shareData['image_width']?>" />
        <meta property="og:image:height" content="<?php echo $shareData['image_height']?>" />
    <?php endif; ?>
    <meta property="og:description" content="<?php echo $series['strSeries_description'];?>. Contribute to this experience to help others or join it to help yourself." />

    <title><?php echo $series['strSeries_title'];?> - Join this experience to grow and share.</title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/preview_series-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormField/FormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/preview_series-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-preview-series <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <section class="content-header">
            <div class="flex-row margin-between flex-wrap d-xs-block">
                <div class="flex-col fix-col">
                    <div class="category-wrapper">
                        <a href="browse?id=<?php echo $category['category_ID'];?>" class="category-name bottom-underline"><?php echo $category['strCategory_name'];?></a>
                        <aside><a href="browse?id=<?php echo $category['category_ID'];?>"><<<</a></aside>
                    </div>
                </div>
                <div class="flex-col center-col">
                    <div class="center-col-wrapper">
                        <div class="flex-row space-between">
                            <div class="flex-col fix-col">
                                <div class="series-title-wrapper">
                                    <div class="flex-row align-items-end margin-between">
                                        <div class="flex-col">
                                            <div class="series-title"><?php echo $series['strSeries_title'];?></div>
                                        </div>
                                        <div class="flex-col d-xs-none">
                                            <div class="posts-count common-radius-aside"><span class="count-value"><?php echo $postsCount;?></span> posts</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-xs-none">
                                    <?php require_once ASSETS_PATH . '/components/SeriesOwners/SeriesOwners.html';?>
                                </div>
                            </div>
                            <div class="flex-col fix-col d-xs-none">
                                <div class="series-actions">
                                    <div class="flex-row margin-between">
                                        <div class="flex-col fix-col">
                                            <?php require_once ASSETS_PATH . '/components/SeriesCopyEmbed/SeriesCopyEmbed.html';?>
                                        </div>
                                        <?php if ($permission): ?>
                                            <div class="flex-col fix-col">
                                                <a target="_blank" href="edit_series?id=<?php echo $series['series_ID'];?>" class="site-green-btn edit-btn">Edit</a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="flex-col fix-col">
                                            <button class="site-green-btn share-btn">Share</button>
                                            <div class="list-icon-wrapper">
                                                <a href="#list-posts" class="list-action" data-toggle="tab">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"></path>
                                                                <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"></path>
                                                                <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"></path>
                                                                <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <aside class="line-decoration-wrapper d-none d-md-block">
                            <div class="top-circle"></div>
                            <div class="bottom-circle"></div>
                        </aside>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <div class="join-wrapper d-xs-flex horizon-center">
                        <div class="btn-wrapper mr-xs-1">
                            <div class="wrap">
                                <button class="select-post">Join</button>
                                <img src="../assets/images/check_arrow_2.svg" alt="">
                                <svg width="42px" height="42px">
                                    <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                </svg>
                            </div>
                        </div>
                        <div class="price-wrapper">
                            <?php if ((int)$series['boolSeries_charge']): ?>
                                <span>$</span> <span class="series-price"><?php echo $series['intSeries_price'];?></span>
                                <?php if ($series['strSeries_charge_type'] === 'monthly'): ?>
                                    <aside>per month</aside>
                                <?php endif; ?>
                            <?php else: ?>
                                <span>Free</span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <main class="main-content">
            <div class="tab-content">
                <div class="preview-tab tab-pane fade in active" id="preview-posts">
                    <div class="item sample" hidden>
                        <header class="item-header current-day-show d-xs-block">
                            <div class="flex-row vertical-center margin-between horizon-center">
                                <div class="flex-col fix-col">
                                    <div class="day-wrapper">
                                        day <span class="day-value">2</span>
                                    </div>
                                </div>
                                <div class="flex-col fix-col">
                                    <aside class="media-icon-wrapper">
                                        <i class="fa fa-youtube-play video-icon"></i>
                                        <i class="fa fa-volume-up audio-icon"></i>
                                        <i class="fa fa-file-text text-icon"></i>
                                    </aside>
                                </div>
                                <div class="flex-col">
                                    <a href="javascript:;" class="item-title" target="_blank">Why gifted students need to be taught formal writing</a>
                                </div>
                                <?php if ($permission): ?>
                                    <div class="flex-col">
                                        <button class="site-green-btn edit-btn">Edit</button>
                                    </div>
                                <?php endif; ?>
                                <div class="flex-col">
                                    <button class="site-green-btn share-btn">Share</button>
                                </div>
                            </div>
                        </header>
                        <div class="item-body">
                            <div class="type-pay blog-type sample" hidden>
                                <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
                            </div>
                            <div class="type-0 blog-type sample" hidden>
                                <div class="audiogallery">
                                    <div class="items">
                                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="type-2 blog-type sample" hidden>
                                <div class="video-wrapper">
                                    <video controls class="video-js"></video>
                                </div>
                            </div>
                            <div class="type-8 blog-type sample" hidden>
                            </div>
                            <div class="type-menu blog-type sample" hidden>
                                <?php require_once ASSETS_PATH . '/components/MenuOptionsBlog/MenuOptionsBlog.html';?>
                            </div>
                            <div class="type-other blog-type sample" hidden></div>
                            <aside class="blog-duration current-day-show d-xs-block">
                                waiting...
                            </aside>
                        </div>
                        <div class="preview-pan other-day-show d-xs-none">
                            <header class="item-header pan-header">
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col fix-col">
                                        <div class="day-wrapper">
                                            day <span class="day-value">2</span>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col mr-auto">
                                        <aside class="media-icon-wrapper">
                                            <i class="fa fa-youtube-play video-icon"></i>
                                            <i class="fa fa-volume-up audio-icon"></i>
                                            <i class="fa fa-file-text text-icon"></i>
                                        </aside>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <button class="site-green-btn share-btn">Share</button>
                                    </div>
                                </div>
                            </header>
                            <div class="img-wrapper">
                                <img class="item-img" alt="" />
                                <aside class="blog-duration">
                                    waiting...
                                </aside>
                            </div>
                            <div class="item-title-wrapper">
                                <a href="javascript:;" class="item-title" target="_blank">
                                    Everyday Nut-Free Desserts Recipes
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="posts-container">
                    </div>
                    <div class="slider-nav flex-centering arrow-left d-xs-none">
                        <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-slider-arrow-left.png" />
                    </div>
                    <div class="slider-nav flex-centering arrow-right d-xs-none">
                        <img src="<?php echo BASE_URL;?>/assets/images/global-icons/home-slider-arrow-right.png" />
                    </div>
                    <aside class="main-content-aside d-xs-none">
                        <div class="aside-inner">
                            <div class="provide-intro">
                                When you join, you can contribute to this experience and will receive crowd-sourced content from this experience when you login.
                            </div>
                            <div class="all-list-wrapper">
                                <a href="#list-posts" class="all-list-action" data-toggle="tab">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"/>
                                                                <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"/>
                                                                <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"/>
                                                                <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                    <span>all series list</span>
                                </a>
                            </div>
                        </div>
                    </aside>
                </div>
                <div class="list-tab tab-pane fade" id="list-posts">
                    <div class="flex-row">
                        <div class="flex-col fix-col">
                            <div class="left-aside">
                                <div class="post-title">
                                    The content will be provided on daily basis
                                </div>
                                <div class="back-wrapper">
                                    <a href="#preview-posts" data-toggle="tab">
                                        << back to day <span class="day-value">1</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col">
                            <div class="posts-container flex-row flex-wrap">
                                <article class="flex-col fix-col item sample" hidden>
                                    <header class="item-header">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col fix-col">
                                                <div class="media-icon-wrapper">
                                                    <i class="fa fa-youtube-play video-icon"></i>
                                                    <i class="fa fa-volume-up audio-icon"></i>
                                                    <i class="fa fa-file-text text-icon"></i>
                                                </div>
                                            </div>
                                            <div class="flex-col">
                                                <div class="day-wrapper">
                                                    <span>day</span> <span class="day-value">11</span>
                                                </div>
                                            </div>
                                            <?php if ($permission): ?>
                                                <div class="flex-col fix-col">
                                                    <a class="item-action edit-action flex-centering" href="javascript:;" tabindex="0">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <div class="flex-col fix-col">
                                                <button class="site-green-btn share-btn">Share</button>
                                            </div>
                                        </div>
                                    </header>
                                    <div class="item-type-body">
                                        <div class="blog-type type-0 sample" hidden>
                                        </div>
                                        <div class="blog-type type-2 sample" hidden>
                                            <img alt="">
                                        </div>
                                        <div class="blog-type type-default sample" hidden>
                                            Lorem lpsum is simply dummy text of the printing and type
                                        </div>
                                        <aside class="blog-duration">
                                            waiting...
                                        </aside>
                                    </div>
                                    <div class="item-title-wrapper">
                                        <a href="javascript:;" target="_blank" class="item-title">Why gifted student needs to be taught formal writing</a>
                                    </div>
                                </article>
                            </div>
                            <div class="load-more-status flex-centering">
                                <span class="load-more">load more</span> <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="right-aside">
                                <div class="affiliate-wrapper" hidden>
                                    <a href="javascript:;" class="btn get-affiliate">Get affiliate link</a>
                                    <div class="earn-wrapper">
                                        <span>earn</span> $<span class="affiliate-value">5</span>
                                    </div>
                                    <div class="per-month">per month</div>
                                    <?php if ($series['strSeries_charge_type'] === 'monthly'): ?>
                                        <div class="per-month">per month</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <section class="content-footer">
            <div class="footer-inner">
                <div class="flex-row vertical-center space-between justify-content-xs-center">
                    <div class="flex-col fix-col">
                        <div class="take-first-step-wrapper">
                            <h3 class="take-first-step">
                                Take the first step
                            </h3>
                            <div class="actions-container">
                                <div class="flex-row margin-between justify-content-xs-center">
                                    <div class="flex-col fix-col">
                                        <div class="wrap">
                                            <button class="select-post">Join</button>
                                            <img src="../assets/images/check_arrow_2.svg" alt="">
                                            <svg width="42px" height="42px">
                                                <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col d-xs-none">
                                        <button class="site-green-btn share-btn">Share</button>
                                    </div>
                                    <div class="flex-col fix-col d-xs-none">
                                        <?php require ASSETS_PATH . '/components/SeriesCopyEmbed/SeriesCopyEmbed.html';?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none">
                        <div class="price-wrapper">
                            <?php if ((int)$series['boolSeries_charge']): ?>
                                <div class="series-price">
                                    <span>$</span> <span class="series-price"><?php echo $series['intSeries_price'];?></span>
                                </div>
                                <?php if ($series['strSeries_charge_type'] === 'monthly'): ?>
                                    <aside>per month</aside>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="series-price">
                                    <span>Free</span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <aside class="line-decoration-wrapper d-xs-none">
                <div class="line-decoration-inner">
                    <div class="flex-row vertical-center">
                        <div class="flex-col fix-col">
                            <div class="you-are-here">you are here</div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="you-are-here-line"></div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="on-your-path">
                                on your path to
                            </div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="on-your-path-line"></div>
                        </div>
                        <div class="flex-col fix-col">
                            <div class="self-development">
                                personal growth
                            </div>
                        </div>
                        <div class="flex-col fb-0">
                            <div class="self-development-line"></div>
                        </div>
                    </div>
                </div>
            </aside>
        </section>
        <?php require_once ASSETS_PATH . '/components/StripeCardModal/StripeCardModal.html';?>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
    <?php require ASSETS_PATH . '/components/ListingPost/ListingPost.php';?>
    <?php require ASSETS_PATH . '/components/PostEditor/PostEditor.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
  var series = <?php echo json_encode($series);?>;
  var posts = <?php echo json_encode($posts);?>;
  var stripeApi_pKey = <?php echo json_encode($stripeApi_pKey);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/preview_series-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesOwners/SeriesOwners.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/MenuOptionsBlog/MenuOptionsBlog.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesStructureTree/SeriesStructureTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopQuestion/FormLoopQuestion.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerParag/FormLoopAnswerParag.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopAnswerTable/FormLoopAnswerTable.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/ListingPost/ListingPost.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/preview_series-page.js?version=<?php echo time();?>"></script>

</body>
</html>
