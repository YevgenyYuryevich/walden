<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 06.08.2018
 * Time: 15:49
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $series['strSeries_title'];?></title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/seriesEmbedGallery-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/SeriesEmbedGallery-page.css?version=<?php echo time();?>">
    <?php
    if ( $externalStyle && $externalStyle !== 'false' ) {
        echo '<link rel="stylesheet" href="'. $externalStyle .'" />';
    }
    ?>
    <style type="text/css">
        <?php echo $inlineStyle && $inlineStyle !== 'false' ? $inlineStyle : '';?>
    </style>
</head>

<body>
<div class="site-wrapper <?php echo $isLoggedIn ? 'logged-in' : '';?>">
    <main class="main-content">
        <div class="content-inner">
            <header class="content-header">
                <div class="flex-row vertical-center space-between">
                    <div class="flex-col">
                        <h3 class="welcome">Welcome back, <span class="display-name"><?php echo $isLoggedIn ? $_SESSION['f_name'] : '';?></span>!</h3>
                        <div class="login-btn">
                            <a class="btn big-login" href="javascript:;">Log in</a>
                        </div>
                    </div>
                </div>
            </header>
            <section class="posts-section">
                <div class="item sample" hidden>
                    <div class="item-inner">
                        <header class="item-header">
                            <h2 class="day-wrapper">Day <span class="item-day"></span></h2>
                            <h3 class="post-title"></h3>
                        </header>
                        <div class="item-body">
                            <div class="type-pay blog-type sample" hidden>
                                <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
                            </div>
                            <div class="type-0 blog-type sample" hidden>
                                <div class="audiogallery">
                                    <div class="items">
                                        <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="type-2 blog-type sample" hidden>
                                <div class="video-wrapper">
                                    <video controls class="video-js"></video>
                                </div>
                            </div>
                            <div class="type-8 blog-type sample" hidden>
                            </div>
                            <div class="type-other blog-type sample" hidden></div>
                        </div>
                    </div>
                </div>
                <div class="items-container">
                </div>
                <div class="nav-portion left-portion flex-box vertical-center horizon-center">
                            <span class="pagging prev-pagging">
                                <i class="glyphicon glyphicon-play icon-flip icon-rotate"></i>
                            </span>
                </div>
                <div class="nav-portion right-portion flex-box vertical-center horizon-center">
                            <span class="pagging next-pagging">
                                <i class="glyphicon glyphicon-play"></i>
                            </span>
                </div>
            </section>
        </div>
    </main>
</div>
<div class="modal fade login" id="loginModal">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login with</h4>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="content">
                        <div class="social">
                            <a class="circle github" href="/auth/github">
                                <i class="fa fa-github fa-fw"></i>
                            </a>
                            <a id="google_login" class="circle google" href="/auth/google_oauth2">
                                <i class="fa fa-google-plus fa-fw"></i>
                            </a>
                            <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                                <i class="fa fa-facebook fa-fw"></i>
                            </a>
                        </div>
                        <div class="division">
                            <div class="line l"></div>
                            <span>or</span>
                            <div class="line r"></div>
                        </div>
                        <div class="error"></div>
                        <div class="form loginBox">
                            <form method="post">
                                <input class="form-control" type="text" placeholder="Email" name="email" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="btn btn-default btn-login" type="submit" value="Login">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content registerBox" style="display:none;">
                        <div class="form">
                            <form method="post">
                                <input class="form-control" type="email" placeholder="Email" name="email" required>
                                <input class="form-control" type="text" placeholder="First Name" name="first_name" required>
                                <input class="form-control" type="text" placeholder="Last Name" name="last_name" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation" required>
                                <input class="btn btn-default btn-register" type="submit" value="Create account" name="commit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="forgot login-footer">
                            <span>Looking to
                                 <a href="javascript:;" class="show-register">create an account</a>
                            ?</span>
                </div>
                <div class="forgot register-footer" style="display:none">
                    <span>Already have an account?</span>
                    <a class="show-login" href="javascript:;">Login</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
    const CURRENT_PAGE = window.location.pathname.substr(1);
    const BASE_URL = window.location.protocol + "//" + window.location.host;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/seriesEmbedGallery-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var series = <?php echo json_encode($series);?>;
    var initialPosts = <?php echo json_encode($posts);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var clientName = <?php echo $isLoggedIn ? json_encode($_SESSION['f_name']) : json_encode(false);?>;
    var clientSeriesView = <?php echo json_encode($clientSeriesView);?>;
    var seekPost = <?php echo json_encode($seekPost);?>;
    var formFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var welcomePost = <?php echo json_encode($welcomePost);?>;
    var embedId = <?php echo json_encode($embedId);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/services/EmbedPageGlobal.js?version=<?php echo time();?>"></script>
<?php
if ( $externalScript && $externalScript !== 'false' ) {
    echo '<script src="'. $externalScript .'"></script>';
}
?>
<script type="application/javascript">
    <?php echo $inlineScript && $inlineScript !== 'false' ? $inlineScript : '';?>
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/SeriesEmbedGallery-page.js?version=<?php echo time();?>"></script>
</body>
</html>
