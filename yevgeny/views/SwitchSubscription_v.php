<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 23.03.2018
 * Time: 07:45
 */
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Change your experience for today - thegreyshirt</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/switchSubscription-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/switchSubscription-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-switchSubscription">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <ul class="nav nav-tabs" hidden>
                <li class="active"><a data-toggle="tab" href="#options-tab"></a></li>
                <li class=""><a data-toggle="tab" href="#switch-from-existing"></a></li>
                <li class=""><a data-toggle="tab" href="#switch-from-scratch"></a></li>
            </ul>
            <div class="tab-content">
                <div id="options-tab" class="tab-pane fade in active">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Switch Options
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Change today's experience to one that is just right</h2>
                        <ul>
                            <li>
                                <a data-toggle="tab" href="#switch-from-existing">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-existing.png">
                                    </div>
                                    <div class="icon-label">
                                        Select an experience from existing ideas
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#switch-from-scratch">
                                    <div class="icon-wrapper">
                                        <img src="assets/images/global-icons/switch-from-scratch.png">
                                    </div>
                                    <div class="icon-label">
                                        Create an experience from your idea
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </main>
                </div>
                <div id="switch-from-existing" class="tab-pane fade">
                    <div class="search-results-wrapper">
                        <aside class="decoration-container">
                            <div class="decoration top-left-rectangular">
                                Switch From Existing
                            </div>
                        </aside>
                        <header class="search-header">
                            <div class="row">
                                <div class="col-md-5 col-sm-12">
                                    <div class="results-from">
                                        <span>Results from: </span>
                                        <div class="dropdown">
                                            <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select types To View <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:;"><input id="from-web" name="from" type="checkbox" value="youtube"/><label for="from-web">WEB</label></a></li>
                                                <li><a href="javascript:;"><input id="from-spreaker" name="from" type="checkbox" value="spreaker"/><label for="from-spreaker">Podcasts</label></a></li>
                                                <li><a href="javascript:;"><input id="from-twingly" name="from" type="checkbox" value="twingly"/><label for="from-twingly">Blogs</label></a></li>
                                                <li><a href="javascript:;"><input id="from-foodfork" name="from" type="checkbox" value="recipes"/><label for="from-foodfork">Recipes</label></a></li>
                                                <li><a href="javascript:;"><input id="from-posts" name="from" type="checkbox" value="posts" checked/><label for="from-posts">POSTS</label></a></li>
                                                <li><a href="javascript:;"><input id="from-ideabox" name="from" type="checkbox" value="ideabox" checked/><label for="from-ideabox">IDEABOX</label></a></li>
                                                <li><a href="javascript:;"><input id="from-rssbposts" name="from" type="checkbox" value="rssbposts"/><label for="from-rssbposts">RSSBlogPosts</label></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-12">
                                    <div class="search-term-wrapper">
                                        <form class="form-inline" method="post">
                                            <label for="search-term2">Search: </label>
                                            <input class="form-control" id="search-term2" size="30"/>
                                            <button class="btn btn-default" type="submit">Submit</button>
                                            <button class="btn btn-default clear-search-term" type="reset">Clear</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div class="search-results-content">
                            <div class="item-sample-wrapper" hidden>
                                <a>
                                    <div class="item-inner">
                                        <aside>
                                            <div class="bottom-line"></div>
                                        </aside>
                                        <div class="img-wrapper">
                                            <img class="item-img" />
                                        </div>
                                        <div class="title-wrapper">
                                            <div class="item-title"></div>
                                        </div>
                                        <div class="buttons-wrapper">
                                            <button href="#" class="select-post btn btn-circle">Select</button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <h2 class="text-center">Here are some ideas for your experience. Please select the one you like</h2>
                            <div class="tt-grid-wrapper youtube-grid after-clearfix">
                                <h3 class="from-where">Results from Youtube</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <div class="tt-grid-wrapper podcasts-grid after-clearfix">
                                <h3 class="from-where">Results from Podcasts</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <div class="tt-grid-wrapper blogs-grid after-clearfix">
                                <h3 class="from-where">Results from Blogs</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <div class="tt-grid-wrapper recipes-grid after-clearfix">
                                <h3 class="from-where">Results from Recipes</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <div class="tt-grid-wrapper posts-grid after-clearfix">
                                <h3 class="from-where">Results from Posts</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <div class="tt-grid-wrapper ideabox-grid after-clearfix">
                                <h3 class="from-where">Results from Ideabox</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <div class="tt-grid-wrapper rssb-posts-grid after-clearfix">
                                <h3 class="from-where">Results from RSSBlogPost</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                            <footer>
                                <button class="btn btn-circle show-more" type="button"><span class="glyphicon glyphicon-play"></span> Show more</button>
                            </footer>
                        </div>
                    </div>
                </div>
                <div id="switch-from-scratch" class="tab-pane fade">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Create a unique Idea
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Your Personal Idea</h2>
                        <form role="form" method="post" id="ideabox-form" enctype="multipart/form-data">
                            <ul>
                                <li>
                                    <div class="form-group">
                                        <label>Name your Ideabox:</label>
                                        <input class="form-control" name="title" required/>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Describe your Idea:</label>
                                        <textarea name="description" class="form-control" rows="3" required></textarea>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Do you want this private?</label>
                                        <div class="cd-switch">
                                            <div class="switch">
                                                <input type="radio" name="isPrivate" id="isPublic_yes" value="1" checked>
                                                <label for="isPublic_yes">Yes</label>
                                                <input type="radio" name="isPrivate" value="0" id="isPublic_no">
                                                <label for="isPublic_no">No</label>
                                                <span class="switchFilter"></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="reference-item">
                                    <label>Type Reference Information to remind (optional)</label>
                                    <div class="flex-row vertical-center margin-between space-between flex-wrap">
                                        <div class="flex-col fix-col">
                                            <div class="form-group">
                                                <label>Text</label>
                                                <input name="refText" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col">
                                            <div class="form-group">
                                                <label>Link</label>
                                                <input name="refLink" class="form-control" size="40"/>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Upload Cover Image for this Idea: (optional)</label>
                                        <div class="cover-image-wrapper">
                                            <div class="img-wrapper">
                                                <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                            </div>
                                            <div>{Drag Image here to upload}</div>
                                            <input class="form-control" type="file" name="cover_img"/>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <footer>
                                <button class="btn btn-circle" type="submit">All set! Now let's Create and Switch With!</button>
                            </footer>
                        </form>
                    </main>
                </div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/switchSubscription-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var postId = <?php echo $postId;?>;
    var subscriptionId = <?php echo $subscriptionId;?>;
    var owner = "<?php echo $owner;?>";
    var subscription = <?php echo json_encode($subscription);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/switchSubscription-page.js?version=<?php echo time();?>"></script>

</body>
</html>