<?php

$rootPath = realpath($_SERVER['DOCUMENT_ROOT']);
$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
$default_image = 'assets/images/beautifulideas.jpg';

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Create a new daily solution - thegreyshirt</title>
    <meta name="description" content="Create a new daily solution that matches your needs or interests." />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/css/yevgeny/createSeries-global.css?version=<?php echo time();?>">
    <link rel="stylesheet" href="<?php echo $base_url;?>/assets/css/yevgeny/createSeries-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-create-series">
    <?php require_once $rootPath . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <ul class="nav nav-tabs" hidden>
                <li class="active"><a data-toggle="tab" href="#for-series">series</a></li>
                <li class=""><a data-toggle="tab" href="#for-posts">posts</a></li>
            </ul>
            <div class="tab-content">
                <div id="for-series" class="for-series tab-pane fade in active">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Create
                        </div>
                    </div>
                    <main class="main-content">
                        <h2 class="text-center">Create your experience</h2>
                        <form role="form" method="post" id="series-form" enctype="multipart/form-data">
                            <ul>
                                <li>
                                    <div class="form-group">
                                        <label>Name your experience:</label>
                                        <input class="form-control" name="strSeries_title" required/>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Describe it:</label>
                                        <textarea name="strSeries_description" class="form-control" required></textarea>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Charge users to join series?</label>
                                        <div class="charge-helper">
                                                <span>
                                                    <?php if ($isStripeAvailable): ?>
                                                        To change your connected stripe account <br/>
                                                        Click here
                                                    <?php else: ?>
                                                        To charge for your series, you must setup stripe public API in your settings.<br/>
                                                        Click here
                                                    <?php endif; ?>
                                                </span>
                                            <a target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId;?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect');?>" class="stripe-connect"><span>Connect with Stripe</span></a>
                                        </div>
                                        <div class="flex-row vertical-center margin-between series-charge-wrapper">
                                            <div class="flex-col fix-col">
                                                <div class="charge-label">Charge: </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <label class="c-switch">
                                                    <input type="checkbox" name="boolSeries_charge" value="1">
                                                    <div class="slider round"></div>
                                                </label>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <div class="price-label">Price: </div>
                                            </div>
                                            <div class="flex-col fix-col">
                                                <input class="form-control charge-price" name="intSeries_price" type="number" value="5" disabled/>
                                            </div>
                                        </div>
                                        <div class="affiliate-settings">
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col fix-col">
                                                    <div class="field-label">Pay Affiliates for Referrals:</div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <label class="c-switch">
                                                        <input type="checkbox" name="boolSeries_affiliated" value="1" disabled>
                                                        <div class="slider round"></div>
                                                    </label>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="field-label percent-label">Percent:</div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <input class="field-value percent-value" name="intSeries_affiliate_percent" type="range" max="100" value="30" disabled/>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <span class="field-helper" style="font-style: italic;" data-toggle="popover" data-trigger="hover" contenteditable="false" data-content="affiliater will get this percent of total budget of this series" data-original-title="" title="">i</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="money-expects">
                                            <div class="flex-row vertical-center margin-between">
                                                <div class="flex-col fix-col">
                                                    <div class="field-label charge-field">You get:</div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="field-value charge-field"></div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="field-label affiliate-field">The Affiliate Gets:</div>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <div class="field-value affiliate-field"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Display On:</label>
                                        <ul class="available-days-list">
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'mon'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                M
                                            </li>
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'tus'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                T
                                            </li>
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'wed'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                W
                                            </li>
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'thi'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                Th
                                            </li>
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'fri'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                F
                                            </li>
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'sat'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                Sa
                                            </li>
                                            <li class="active-day">
                                                <div class="icon-wrapper" data-day = 'sun'>
                                                    <img class="active-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-active.png" />
                                                    <img class="inactive-icon" src="<?php echo $base_url?>/assets/images/global-icons/check-inactive.png" />
                                                </div>
                                                S
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Search Terms for it:(We use these to find your experience)</label>
                                        <input name="search_term" class="form-control" required/>
                                    </div>
                                </li>
                                <?php if (!$banToCreatePublic): ?>
                                    <li>
                                        <div class="form-group">
                                            <label>Do you want this to be available to others?</label>
                                            <div class="cd-switch">
                                                <div class="switch">
                                                    <input type="radio" name="boolSeries_isPublic" id="isPublic_yes" value="1" checked>
                                                    <label for="isPublic_yes">Yes</label>
                                                    <input type="radio" name="boolSeries_isPublic" value="0" id="isPublic_no">
                                                    <label for="isPublic_no">No</label>
                                                    <span class="switchFilter"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <div class="form-group">
                                        <label>What category is most appropriate for this experience?</label>
                                        <select name="intSeries_category" class="form-control">
                                            <?php foreach ($categories as $category): ?>
                                                <option value="<?php echo $category['category_ID'];?>"><?php echo $category['strCategory_name'];?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group">
                                        <label>Upload Cover Image for this Experience: (optional)</label>
                                        <div class="cover-image-wrapper">
                                            <div class="img-wrapper">
                                                <img src="<?php  echo $base_url;?>/assets/images/global-icons/cloud-uploading.png" />
                                            </div>
                                            <div>{Drag Image here to upload}</div>
                                            <input class="form-control" type="file" name="strSeries_image"/>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <footer>
                                <button class="btn btn-circle" type="submit">All set! Now let's create! >></button>
                            </footer>
                        </form>
                    </main>
                </div>
                <div id="for-posts" class="for-posts tab-pane fade">
                    <div class="decoration-container">
                        <div class="decoration top-left-rectangular">
                            Results
                        </div>
                    </div>
                    <header class="content-header">
                        <div class="row">
                            <div class="col-md-5 col-sm-12">
                                <div class="results-from">
                                    <span>Results from: </span>
                                    <div class="dropdown">
                                        <button class="btn btn-custom dropdown-toggle" type="button" data-toggle="dropdown">Select types To View <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:;"><input id="from-url" name="from" type="checkbox" value="url"/><label for="from-url">My Unique URLs</label></a></li>
                                            <li><a href="javascript:;"><input id="from-web" name="from" type="checkbox" value="youtube" checked/><label for="from-web">WEB</label></a></li>
                                            <li><a href="javascript:;"><input id="from-spreaker" name="from" type="checkbox" value="spreaker" checked/><label for="from-spreaker">Podcasts</label></a></li>
                                            <li><a href="javascript:;"><input id="from-twingly" name="from" type="checkbox" value="twingly" checked/><label for="from-twingly">Blogs</label></a></li>
                                            <li><a href="javascript:;"><input id="from-foodfork" name="from" type="checkbox" value="recipes"/><label for="from-foodfork">Recipes</label></a></li>
                                            <li><a href="javascript:;"><input id="from-ideabox" name="from" type="checkbox" value="ideabox"/><label for="from-ideabox">IDEABOX</label></a></li>
                                            <li><a href="javascript:;"><input id="from-posts" name="from" type="checkbox" value="posts"/><label for="from-posts">POSTS</label></a></li>
                                            <li><a href="javascript:;"><input id="from-rssbposts" name="from" type="checkbox" value="rssbposts"/><label for="from-rssbposts">RSSBlogPosts</label></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12">
                                <div class="search-term-wrapper">
                                    <form class="form-inline" method="post">
                                        <label for="search-term2">Search: </label>
                                        <input class="form-control" id="search-term2" size="30"/>
                                        <button class="btn btn-default" type="submit">Submit</button>
                                        <button class="btn btn-default clear-search-term" type="reset">Clear</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </header>
                    <main class="main-content">
                        <aside class="actions-container">
                            <a href="javascript:;" class="add-new-posts action">
                                Create your own idea
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                        <g>
                                            <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                        S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"></path>
                                            <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                        s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"></path></g>
                                    </svg>
                            </a>
                        </aside>
                        <h2 class="text-center">Here are some ideas for your experience. Please select the ones you like</h2>
                        <h3 class="text-center">(You can also drag a URL here to add it)</h3>
                        <div class="meta-posts-block-container">
                            <div class="item-sample" hidden>
                                <div class="item-inner">
                                    <aside>
                                        <img class="delete-item" src="assets/images/global-icons/close.png" />
                                    </aside>
                                    <div class="img-wrapper">
                                        <img class="item-img" />
                                        <div class="input-wrapper">
                                            <aside class="helper-text">Click or Drag</aside>
                                            <input name="item_image" type="file" title="Click or Drag">
                                        </div>
                                    </div>
                                    <div class="title-wrapper">
                                        <textarea name="item_title" rows="2" placeholder="Post Title"></textarea>
                                        <div class="item-title"></div>
                                        <aside class="actions-container">
                                            <div class="edit-wrapper">
                                                <span class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></span>
                                            </div>
                                            <div class="save-wrapper">
                                                <div class="flex-row margin-between space-between">
                                                    <div class="flex-col">
                                                        <button class="btn btn-primary btn-block btn-xs save-entry">save</button>
                                                    </div>
                                                    <div class="flex-col">
                                                        <button class="btn btn-block btn-default btn-xs back-to-origin">cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                    <div class="description-wrapper">
                                        <textarea name="item_body" rows="3" placeholder="Describe Here..."></textarea>
                                        <div class="item-description"></div>
                                        <aside class="actions-container">
                                            <div class="edit-wrapper">
                                                <span class="edit-entry"><img class="edit-icon" src="assets/images/mode_edit_grey_192x192.png"></span>
                                            </div>
                                            <div class="save-wrapper">
                                                <div class="flex-row margin-between space-between">
                                                    <div class="flex-col">
                                                        <button class="btn btn-primary btn-block btn-xs save-entry">save</button>
                                                    </div>
                                                    <div class="flex-col">
                                                        <button class="btn btn-block btn-default btn-xs back-to-origin">cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                    <div class="type-wrapper">
                                        <div class="flex-row margin-between vertical-center">
                                            <div class="flex-col fix-col">Post Type: </div>
                                            <div class="flex-col">
                                                <select name="type">
                                                    <option value="2">Video</option>
                                                    <option value="0">Audio</option>
                                                    <option value="8">Image</option>
                                                    <option value="7">Text</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="buttons-wrapper">
                                        <div class="wrap">
                                            <button class="select-post">Select</button>
                                            <img src="../assets/images/check_arrow_2.svg" alt="">
                                            <svg width="42px" height="42px">
                                                <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="bottom-line"></div>
                                </div>
                            </div>
                            <div class="tt-grid-wrapper posts-block sample" hidden>
                                <h3 class="from-where">My Unique URLs</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                </ul>
                            </div>
                        </div>
                        <div class="posts-block-container">
                            <div class="item-sample-wrapper" hidden>
                                <div>
                                    <div class="item-inner">
                                        <aside>
                                            <div class="bottom-line"></div>
                                        </aside>
                                        <div class="img-wrapper">
                                            <img src="" alt="" class="item-img">
                                        </div>
                                        <div class="title-wrapper">
                                            <div class="item-title"></div>
                                        </div>
                                        <div class="buttons-wrapper">
                                            <div class="wrap">
                                                <button class="select-post">Select</button>
                                                <img src="../assets/images/check_arrow_2.svg" alt="">
                                                <svg width="42px" height="42px">
                                                    <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tt-grid-wrapper posts-block sample" hidden>
                                <h3 class="from-where">Results from</h3>
                                <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                    <li class="tt-empty"></li>
                                </ul>
                            </div>
                        </div>
                        <footer>
                            <button class="btn btn-circle show-more" type="button"><span class="glyphicon glyphicon-play"></span> Show more</button>
                            <button class="btn btn-circle i-am-done" type="button">I am done!</button>
                        </footer>
                    </main>
                </div>
            </div>
        </div>
    </div>
    <?php require_once $rootPath . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php  echo $base_url;?>/assets/js/yevgeny/createSeries-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    const DEFAULT_IMAGE = '<?php echo $default_image; ?>';
    var level = <?php echo json_encode($level);?>;
    var banToCreatePublic = <?php echo json_encode($banToCreatePublic);?>;
    var isStripeAvailable = <?php echo json_encode($isStripeAvailable);?>;
</script>
<script src="<?php  echo $base_url;?>/assets/js/yevgeny/createSeries-page.js?version=<?php echo time();?>"></script>

</body>
</html>