<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.12.2018
 * Time: 22:43
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Walden.ly - View the notes that you have taken while viewing content.</title>
    <meta name="description" content="Review your notes from the content that you have viewed.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my_notes-global.css?version=<?php echo time();?>">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my_notes-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-favorite <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content <?php echo !count($posts) ? 'no-content' : '';?>">
        <div class="filter-wrapper">
            <div class="filter-content">
                <div class="flex-row vertical-center space-between flex-wrap">
                    <div class="flex-col">
                        <div class="my-notes">
                            <h3 class="filter-name">My Notes</h3>
                            <div class="flex-row vertical-center margin-between flex-wrap d-xs-none d-no-content-none">
                                <div class="flex-col fix-col">
                                    <a href="javascript:;" class="filter-category active" data-category="-1">All</a>
                                </div>
                                <?php foreach ($categories as $category): ?>
                                    <div class="flex-col fix-col">
                                        <a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none d-no-content-none">
                        <div class="sort-by">
                            <h4 class="filter-name">Sort by</h4>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                    <span>Popular</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                                <ul class="dropdown-menu">
                                    <li hidden><a href="#" data-value="-1">Popular</a></li>
                                    <li><a href="#" data-value="audio">Audio</a></li>
                                    <li><a href="#" data-value="video">Video</a></li>
                                    <li><a href="#" data-value="article">Article</a></li>
                                    <li><a href="#" data-value="A-z">A-z</a></li>
                                    <li><a href="#" data-value="Z-a">Z-a</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-none d-xs-block d-no-content-none">
                        <div class="categories-container-mobile">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                    <span>All</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                                <ul class="dropdown-menu">
                                    <li hidden><a href="javascript:;" class="filter-category" data-category="-1">All</a></li>
                                    <?php foreach ($categories as $category): ?>
                                        <li><a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <main class="main-content d-no-content-none">
            <div class="flex-row d-xs-block">
                <div class="flex-col fix-col series-col">
                    <ul class="series-list d-xs-flex align-items-xs-start">
                        <li class="item sample" hidden><a href="javascript:;"><i class="glyphicon glyphicon-menu-right d-xs-none"></i><span>Daily Recipe</span></a></li>
                    </ul>
                </div>
                <div class="d-none d-xs-block">
                    <div class="current-series">
                        All Series
                    </div>
                </div>
                <div class="flex-col posts-col">
                    <div class="posts-container">
                        <div class="post-item sample" hidden>
                            <div class="item-inner">
                                <div class="flex-row margin-between">
                                    <div class="flex-col fix-col mr-1">
                                        <img class="item-img" alt="" />
                                    </div>
                                    <div class="flex-col">
                                        <div class="item-inner-content">
                                            <div class="to-wrapper">
                                                <div class="flex-row vertical-center">
                                                    <div class="flex-col fix-col">
                                                        <div>to&nbsp;</div>
                                                    </div>
                                                    <div class="flex-col fix-col mr-1">
                                                        <div class="to-value">video</div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="media-icon-wrapper">
                                                            <i class="fa fa-youtube-play video-icon"></i>
                                                            <i class="fa fa-volume-up audio-icon"></i>
                                                            <i class="fa fa-file-text text-icon"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="title-wrapper">
                                                <a href="javascript:;" class="item-title" target="_blank">Great breakfasts</a><span class="divide-aside"> | </span><span class="blog-duration">10:09</span>
                                            </div>
                                            <div class="item-series-wrapper">
                                                <a href="javascript:;" class="item-series bottom-underline">Fresh food</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="notes-list">
                                <div class="note-item sample" hidden>
                                    <div class="flex-row margin-between">
                                        <div class="flex-col fix-col d-xs-none">
                                            <div class="drag-icon-wrapper">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 180 180" style="enable-background:new 0 0 180 180;" xml:space="preserve">
                                                    <path d="M38.937,128.936L0,89.999l38.935-38.936l0.001,29.982h42.11V38.937l-29.982,0L90.001,0l38.935,38.936l-29.982,0v42.109
                                                        h42.109l-0.001-29.982L180,90.001l-38.936,38.935l-0.001-29.982h-42.11v42.109l29.982,0L89.999,180l-38.936-38.936l29.982,0V98.954
                                                        H38.937L38.937,128.936z"/>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="flex-col">
                                            <div class="note-content-wrapper">
                                                <div class="note-content">
                                                </div>
                                                <div class="actions-container">
                                                    <div class="flex-row vertical-center margin-between">
                                                        <div class="flex-col fix-col d-xs-none">
                                                            <a class="item-action edit-action flex-centering" href="javascript:;" tabindex="0">
                                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                    <g>
                                                                        <g>
                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <div class="flex-col fix-col">
                                                            <a class="item-action delete-action" href="javascript:;" tabindex="0">
                                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                                                        <g>
                                                                            <g>
                                                                                <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
                                                                                    286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"></polygon>
                                                                                <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
                                                                                    C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
                                                                                    S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"></path>
                                                                            </g>
                                                                        </g>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade site-modal note-editor" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <a href="javascript:;" class="modal-close" data-dismiss="modal"></a>
                                <div class="modal-body">
                                    <div class="post-editor-content">
                                        <h2 class="modal-title">Edit Note</h2>
                                        <form action="" method="post">
                                            <div class="goal-input-wrapper multi-line-background">
                                                <textarea class="note-add-input" name="postNote_content" rows="4" required="" placeholder="Just start writing here some notes"></textarea>
                                                <div class="background-placeholder"></div>
                                            </div>
                                            <div class="button-wrapper">
                                                <button class="save-btn">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <div class="no-content-wrapper">
            <div class="flex-row vertical-center d-xs-block">
                <div class="flex-col min-width-0">
                    <div class="no-content-txt-wrapper">
                        <h2 class="you-not-txt">You have not taken any notes yet.</h2>
                        <div class="to-add-txt d-xs-none">To taken notes, click the "Notebook" icon, located to the right of your content on the "Today's Experiences" page.</div>
                    </div>
                </div>
                <div class="flex-col min-width-0">
                    <div class="no-content-img-wrapper">
                        <img class="background-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/cloud.png" alt="" />
                        <div class="to-add-icons-wrapper flex-centering">
                            <div class="note-icon flex-centering">
                                <svg height="436pt" viewBox="-6 0 436 436" width="436pt" xmlns="http://www.w3.org/2000/svg"><path d="m405.269531 95.183594c-15.378906-15.332032-38.886719-19.011719-58.214843-9.109375v-36.074219c-.03125-27.601562-22.398438-49.96875-50-50h-247.054688c-27.601562.03125-49.96875 22.398438-50 50v336c.03125 27.601562 22.398438 49.96875 50 50h247.054688c27.601562-.03125 49.96875-22.398438 50-50v-153.125l62.601562-62.601562c19.527344-19.527344 19.527344-51.183594 0-70.710938zm-220.289062 187.9375 36.425781 36.761718-49.578125 13.152344zm55.722656 27.820312-46.8125-47.242187 135.117187-134.730469 46.835938 46.832031zm86.351563 75.058594c-.019532 16.5625-13.4375 29.980469-30 30h-247.054688c-16.5625-.019531-29.980469-13.4375-30-30v-336c.019531-16.5625 13.4375-29.980469 30-30h247.054688c16.5625.019531 29.984374 13.4375 30 30v52.664062l-154.34375 153.90625c-1.257813 1.253907-2.160157 2.820313-2.609376 4.535157l-21.992187 83.457031c-.90625 3.441406.085937 7.105469 2.605469 9.625 2.519531 2.515625 6.183594 3.5 9.628906 2.585938l82.890625-21.992188c1.703125-.449219 3.257813-1.34375 4.507813-2.59375l79.3125-79.3125zm68.457031-229.867188-5.527344 5.527344-46.816406-46.816406 5.574219-5.558594c11.730468-11.65625 30.675781-11.636718 42.386718.039063l4.382813 4.382812c11.699219 11.722657 11.695312 30.703125 0 42.425781zm0 0"></path></svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="to-add-txt-mobile d-none d-xs-block">To taken notes, click the "Notebook" icon, located to the right of your content on the "Today's Experiences" page.</div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script>
    var categories = <?php echo json_encode($categories);?>;
    var series = <?php echo json_encode($series);?>;
    var posts = <?php echo json_encode($posts);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my_notes-page.js?version=<?php echo time();?>"></script>

</body>
</html>
