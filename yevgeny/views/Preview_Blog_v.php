<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.12.2018
 * Time: 20:50
 */

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Title</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/view_blog-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/preview_blog-page.css?version=<?php echo time();?>" />

</head>
<body>
<div class="site-wrapper page page-view-blog <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="preview-blog-header">
            <div class="blog-title"><?php echo $postData['title'];?></div>
        </header>
        <main class="main-content">
            <div class="blog-wrapper">
                    <div class="type-0 subscription-type sample" hidden>
                        <div class="audiogallery">
                            <div class="items">
                                <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="type-2 subscription-type sample" hidden>
                        <div class="video-wrapper">
                            <video class="video-js" controls width="960" height="540"></video>
                        </div>
                    </div>
                    <div class="type-menu subscription-type sample" hidden>
                        <header>
                            <h3 class="menu-title"></h3>
                            <p class="menu-description"></p>
                        </header>
                        <div class="paths-container">
                            <div class="flex-col path-col sample" hidden>
                                <div class="img-wrapper">
                                    <img />
                                </div>
                                <h3 class="path-title"></h3>
                                <p class="path-description"></p>
                            </div>
                            <div class="flex-row paths-list flex-wrap">
                            </div>
                        </div>
                    </div>
                    <div class="type-8 subscription-type sample" hidden></div>
                    <div class="type-10 subscription-type sample" hidden>
                        <div class="item-description"></div>
                    </div>
                    <div class="type-other subscription-type sample" hidden>
                        <div class="item-description"></div>
                    </div>
                    <aside class="blog-duration">
                        waiting...
                    </aside>
                </div>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
<script>
    var postData = <?php echo json_encode($postData);?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/view_blog-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/preview_blog-page.js?version=<?php echo time();?>"></script>
</body>
</html>