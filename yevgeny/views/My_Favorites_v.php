<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.10.2018
 * Time: 19:48
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Walden.ly - My Favorite Content</title>
    <meta name="description" content="View your favorite content from the content channels that you have subscribed to.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my_favorites-global.css?version=<?php echo time();?>">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my-favorites-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-favorite <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content <?php echo !count($categories) ? 'no-content' : '';?>">
        <div class="filter-wrapper">
            <div class="filter-content">
                <div class="flex-row vertical-center space-between flex-wrap-xs-wrap">
                    <div class="flex-col">
                        <div class="my-favorites">
                            <h3 class="filter-name">My Favorites</h3>
                            <div class="flex-row vertical-center margin-between flex-wrap d-xs-none d-no-content-none">
                                <div class="flex-col fix-col">
                                    <a href="javascript:;" class="filter-category active" data-category="-1">All</a>
                                </div>
                                <?php foreach ($categories as $category): ?>
                                    <div class="flex-col fix-col">
                                        <a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none d-no-content-none">
                        <div class="sort-by">
                            <h4 class="filter-name">Sort by</h4>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                    <span>Popular</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                                <ul class="dropdown-menu">
                                    <li hidden><a href="#" data-value="-1">Popular</a></li>
                                    <li><a href="#" data-value="audio">Audio</a></li>
                                    <li><a href="#" data-value="video">Video</a></li>
                                    <li><a href="#" data-value="article">Article</a></li>
                                    <li><a href="#" data-value="A-z">A-z</a></li>
                                    <li><a href="#" data-value="Z-a">Z-a</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-none d-xs-block d-no-content-none">
                        <div class="categories-container-mobile">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                    <span>All</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                                <ul class="dropdown-menu">
                                    <li hidden><a href="javascript:;" class="filter-category" data-category="-1">All</a></li>
                                    <?php foreach ($categories as $category): ?>
                                        <li><a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <main class="main-content d-no-content-none">
            <div class="flex-row d-xs-block">
                <div class="flex-col fix-col series-col">
                    <ul class="series-list d-xs-flex align-items-xs-start">
                        <li class="item sample" hidden><a href="javascript:;"><i class="glyphicon glyphicon-menu-right d-xs-none"></i><span>Daily Recipe</span></a></li>
                    </ul>
                </div>
                <div class="d-none d-xs-block">
                    <div class="current-series">
                        All Series
                    </div>
                </div>
                <div class="flex-col posts-col">
                    <div class="posts-container flex-row flex-wrap">
                        <article class="flex-col fix-col item sample" hidden>
                            <header class="item-header">
                                <div class="flex-row vertical-center margin-between">
                                    <div class="flex-col fix-col">
                                        <div class="media-icon-wrapper">
                                            <i class="fa fa-youtube-play video-icon"></i>
                                            <i class="fa fa-volume-up audio-icon"></i>
                                            <i class="fa fa-file-text text-icon"></i>
                                        </div>
                                    </div>
                                    <div class="flex-col">
                                        <div class="day-wrapper">
                                            <span>day</span> <span class="day-value">11</span>
                                        </div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <a class="fav-star" href="javascript:;">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="49.655px" height="49.654px" viewBox="0 0 49.655 49.654" style="enable-background:new 0 0 49.655 49.654;"
                                                 xml:space="preserve">
<g>
    <g>
        <path d="M24.827,0C11.138,0,0,11.137,0,24.826c0,13.688,11.137,24.828,24.826,24.828c13.688,0,24.827-11.14,24.827-24.828
			C49.653,11.137,38.516,0,24.827,0z M38.543,23.011L38.543,23.011l-7.188,5.201l2.769,8.46c0.044,0.133,0.067,0.273,0.067,0.417
			c0,0.739-0.6,1.339-1.339,1.339c-0.293,0-0.562-0.094-0.782-0.252l-7.244-5.24l-7.243,5.24c-0.22,0.158-0.49,0.252-0.783,0.252
			c-0.74,0-1.339-0.599-1.339-1.339c0-0.146,0.024-0.284,0.068-0.417l2.769-8.46l-7.187-5.2v-0.001
			c-0.336-0.242-0.554-0.637-0.554-1.083c0-0.739,0.598-1.337,1.338-1.337h8.897l2.755-8.421c0.168-0.547,0.677-0.945,1.28-0.945
			c0.603,0,1.112,0.398,1.28,0.945l2.754,8.421h8.896c0.739,0,1.338,0.598,1.338,1.337C39.096,22.374,38.878,22.769,38.543,23.011z"
        />
    </g>
</g>
</svg>
                                        </a>
                                    </div>
                                </div>
                            </header>
                            <div class="item-type-body">
                                <div class="blog-type type-0 sample" hidden>
                                </div>
                                <div class="blog-type type-2 sample" hidden>
                                    <img alt="">
                                </div>
                                <div class="blog-type type-default sample" hidden>
                                    Lorem lpsum is simply dummy text of the printing and type
                                </div>
                                <aside class="blog-duration">
                                    waiting...
                                </aside>
                            </div>
                            <a href="javascript:;" class="item-title" target="_blank">Why gifted student needs to be taught formal writing</a>
                            <div class="item-series-wrapper">
                                <a href="javascript:;" class="item-series">
                                    Fresh food
                                </a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </main>
        <div class="no-content-wrapper">
            <div class="flex-row vertical-center d-xs-block">
                <div class="flex-col min-width-0">
                    <div class="no-content-txt-wrapper">
                        <h2 class="you-not-txt">You do not have any favorites yet</h2>
                        <div class="to-add-txt d-xs-none">To add favorites, click the star icon next to any content.</div>
                    </div>
                </div>
                <div class="flex-col min-width-0">
                    <div class="no-content-img-wrapper">
                        <img class="background-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/cloud.png" alt="" />
                        <div class="to-add-icons-wrapper flex-centering">
                            <div class="flex-row vertical-center horizon-center margin-between">
                                <div class="flex-col fix-col mr-1">
                                    <a href="javascript:;" class="action-item flex-centering complete-action">
                                        <svg version="1.1" id="Layer_1"
                                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="49px" height="38px"
                                             viewBox="0.5 0 49 38" enable-background="new 0.5 0 49 38" xml:space="preserve">
                                                        <title>Rectangle 4</title>
                                            <desc>Created with Sketch.</desc>
                                            <g id="Page-1" sketch:type="MSPage">
                                                <g id="Check_1" transform="translate(115.000000, 102.000000)" sketch:type="MSLayerGroup">
                                                    <path id="Rectangle-4" sketch:type="MSShapeGroup" fill="#FFFFFF" d="M-99.8-72.6l28.3-28.3c1.4-1.4,3.6-1.4,4.9,0
                                                                    c1.4,1.4,1.4,3.6,0,4.9l-31.2,31.1c-0.6,0.6-1.4,0.9-2.1,0.9c-0.8,0-1.5-0.3-2.1-0.9l-11.6-11.6c-1.4-1.4-1.4-3.6,0-4.9
                                                                    c1.4-1.4,3.6-1.4,4.9,0L-99.8-72.6z"/>
                                                </g>
                                            </g>
                                                        </svg>
                                    </a>
                                </div>
                                <div class="flex-col fix-col mr-1">
                                    <a href="javascript:;" class="action-item flex-centering favorite-action">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                                                            <polygon points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                                                                81.485,416.226 106.667,269.41 0,165.436 147.409,144.017"/>
                                                        </svg>
                                    </a>
                                </div>
                                <div class="flex-col fix-col">
                                    <a href="javascript:;" class="action-item flex-centering share-post">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 525.152 525.152" style="enable-background:new 0 0 525.152 525.152;" xml:space="preserve">
                                                             <g>
                                                                 <path d="M420.735,371.217c-20.021,0-37.942,7.855-51.596,20.24L181.112,282.094c1.357-6.061,2.407-12.166,2.407-18.468
                                                                    c0-6.302-1.072-12.385-2.407-18.468l185.904-108.335c14.179,13.129,32.931,21.334,53.719,21.334
                                                                    c43.828,0,79.145-35.251,79.145-79.079C499.88,35.338,464.541,0,420.735,0c-43.741,0-79.079,35.338-79.079,79.057
                                                                    c0,6.389,1.072,12.385,2.407,18.468L158.158,205.947c-14.201-13.194-32.931-21.378-53.741-21.378
                                                                    c-43.828,0-79.145,35.317-79.145,79.057s35.317,79.079,79.145,79.079c20.787,0,39.54-8.206,53.719-21.334l187.698,109.604
                                                                    c-1.291,5.58-2.101,11.4-2.101,17.199c0,42.45,34.594,76.979,76.979,76.979c42.428,0,77.044-34.507,77.044-76.979
                                                                    S463.163,371.217,420.735,371.217z"/>
                                                             </g>
                                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="to-add-txt-mobile d-none d-xs-block">To add favorites, click the star icon next to any content.</div>
            </div>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script>
    var categories = <?php echo json_encode($categories);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my-favorites-page.js?version=<?php echo time();?>"></script>

</body>
</html>
