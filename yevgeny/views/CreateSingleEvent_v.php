<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 04:21
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Create a single event</title>
    <meta name="description" content="Create a single event.  This is a great idea for things like parties or social outings that only occur a single time." />

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/createSingleEvent-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/createSingleEvent-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-create-series">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Create Event
                </div>
            </div>
            <main class="main-content">
                <h2 class="text-center">Your Personal Event</h2>
                <form role="form" method="post" id="event-form" enctype="multipart/form-data">
                    <ul>
                        <li>
                            <div class="form-group">
                                <label>Name your event:</label>
                                <input class="form-control" name="title" required/>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Describe it:</label>
                                <textarea name="description" class="form-control" required></textarea>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Location:</label>
                                <input class="form-control" name="location" required/>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Start Date Time:</label>
                                <div class="flex-row margin-between">
                                    <div class="flex-col">
                                        <input class="form-control" name="start_date" required/>
                                    </div>
                                    <div class="flex-col">
                                        <input class="form-control" name="start_time" required/>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>Upload Cover Image for this Event: (optional)</label>
                                <div class="cover-image-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/cloud-uploading.png" />
                                    </div>
                                    <div>{Drag Image here to upload}</div>
                                    <input class="form-control" type="file" name="cover_img"/>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <footer>
                        <button class="btn btn-circle" type="submit">All set! Now let's create! >></button>
                    </footer>
                </form>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php  echo BASE_URL;?>/assets/js/yevgeny/createSingleEvent-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
</script>
<script src="<?php  echo BASE_URL;?>/assets/js/yevgeny/createSingleEvent-page.js?version=<?php echo time();?>"></script>

</body>
</html>