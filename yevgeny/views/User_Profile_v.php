<?php

?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title> - Join this experience to grow and share.</title>
    <meta name="description" content="Contribute to this experience to help others or join it to help yourself.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/user_profile-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/user_profile-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-user-profile <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content">
        <header class="content-header">
            <div class="header--inner">
                <div class="d-flex align-items-center flex-wrap">
                    <div class="mr-1">
                        <div class="meet-user">
                            <span class="user-name">Meet <?php echo $user['f_name'];?></span>
                        </div>
                    </div>
                    <div class="mr-auto">
                        <div class="share-with-list flex-row vertical-center horizon-center margin-between">
                            <div class="flex-col fix-col">
                                <a target="<?php echo $user['social_facebook'] ? '_blank' : '_self';?>" href="<?php echo $user['social_facebook'] ?: 'javascript:;';?>" class="share-item item-facebook">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                <g>
                                    <circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
                                    <path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                        c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                </g>
                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a target="<?php echo $user['social_twitter'] ? '_blank' : '_self';?>" href="<?php echo $user['social_twitter'] ?: 'javascript:;';?>" class="share-item item-twitter">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
                                                c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
                                                c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
                                                c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
                                                c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
                                                c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
                                                c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
                                                C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                                        </g>
                                    </g>
                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a target="<?php echo $user['social_linkedin'] ? '_blank' : '_self';?>" href="<?php echo $user['social_linkedin'] ?: 'javascript:;';?>" class="share-item item-linkedin">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
                                                c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
                                                c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
                                                C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
                                                c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
                                                 M27.865,83.739H41.27V43.409H27.865V83.739z"/>
                                        </g>
                                    </g>
                                </svg>
                                </a>
                            </div>
                            <div class="flex-col fix-col">
                                <a href="mailto:<?php echo $user['email'];?>" class="share-item item-email">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="512pt" version="1.1" viewBox="0 0 512 512" width="512pt">
                                        <g id="surface1">
                                            <path d="M 512 256 C 512 397.386719 397.386719 512 256 512 C 114.613281 512 0 397.386719 0 256 C 0 114.613281 114.613281 0 256 0 C 397.386719 0 512 114.613281 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294118%,32.941176%,87.843137%);fill-opacity:1;" />
                                            <path d="M 512 256 C 512 245.425781 511.347656 235.003906 510.101562 224.765625 L 379.667969 94.332031 L 100.5 409 L 196.542969 505.042969 C 215.625 509.582031 235.527344 512 256 512 C 397.386719 512 512 397.386719 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(22.745098%,25.490196%,80%);fill-opacity:1;" />
                                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 100.5 409 L 411.5 409 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(98.039216%,36.470588%,5.882353%);fill-opacity:1;" />
                                            <path d="M 411.5 188 L 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 409 L 411.5 409 Z M 411.5 188 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.705882%,29.803922%,5.882353%);fill-opacity:1;" />
                                            <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 197.046875 293.835938 C 228.695312 328.527344 283.304688 328.527344 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;" />
                                            <path d="M 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 319.847656 C 277.40625 319.910156 299.070312 311.246094 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(92.156863%,92.156863%,94.117647%);fill-opacity:1;" />
                                            <path d="M 100.5 409 L 197.046875 303.164062 C 228.695312 268.472656 283.304688 268.472656 314.953125 303.164062 L 411.5 409 Z M 100.5 409 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,58.431373%,0%);fill-opacity:1;" />
                                            <path d="M 314.953125 303.164062 C 299.070312 285.753906 277.40625 277.085938 255.761719 277.152344 L 255.761719 409 L 411.5 409 Z M 314.953125 303.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(91.764706%,50.980392%,2.352941%);fill-opacity:1;" />
                                            <path d="M 132.332031 94.332031 L 132.332031 222.894531 L 197.046875 293.835938 C 212.871094 311.179688 234.4375 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 132.332031 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(90.196078%,90.196078%,90.196078%);fill-opacity:1;" />
                                            <path d="M 255.761719 94.332031 L 255.761719 319.847656 C 255.839844 319.847656 255.921875 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 255.761719 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.313725%,84.313725%,84.313725%);fill-opacity:1;" />
                                            <path d="M 159 124.332031 L 231 124.332031 L 231 211 L 159 211 Z M 159 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                            <path d="M 247 124.332031 L 355.667969 124.332031 L 355.667969 139 L 247 139 Z M 247 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                            <path d="M 255.761719 124.332031 L 355.667969 124.332031 L 355.667969 139 L 255.761719 139 Z M 255.761719 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                            <path d="M 247 160.332031 L 355.667969 160.332031 L 355.667969 175 L 247 175 Z M 247 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                            <path d="M 255.761719 160.332031 L 355.667969 160.332031 L 355.667969 175 L 255.761719 175 Z M 255.761719 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                            <path d="M 247 196.332031 L 355.667969 196.332031 L 355.667969 211 L 247 211 Z M 247 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                            <path d="M 255.761719 196.332031 L 355.667969 196.332031 L 355.667969 211 L 255.761719 211 Z M 255.761719 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                            <path d="M 159 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 159 246.40625 Z M 159 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                            <path d="M 255.761719 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 255.761719 246.40625 Z M 255.761719 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <form class="search-input-wrapper d-xs-flex" style="color: grey;">
                        <input class="fg-xs-1" placeholder="Search for content created and curated by <?php echo $user['f_name']?>?" data-toggle="popover" data-content="Search for content that interests you.">
                        <button class="search-submit-button">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 76.652 76.652" style="enable-background:new 0 0 76.652 76.652;" xml:space="preserve">
                                                <g>
                                                    <g id="Zoom_2_">
                                                        <g>
                                                            <path d="M75.549,68.735L58.189,52.06c4.267-5.283,6.814-11.907,6.814-19.097c0-17.216-14.58-31.221-32.504-31.221     C14.58,1.741,0,15.746,0,32.963s14.58,31.223,32.5,31.223c7.73,0,14.835-2.609,20.42-6.954l17.301,16.618     c0.732,0.707,1.699,1.061,2.662,1.061c0.967,0,1.93-0.354,2.666-1.061C77.02,72.435,77.02,70.149,75.549,68.735z M7.536,32.963     c0-13.223,11.2-23.979,24.964-23.979c13.768,0,24.968,10.756,24.968,23.979S46.268,56.949,32.5,56.949     C18.736,56.949,7.536,46.185,7.536,32.963z" fill="#1fcd98"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </header>
        <main class="main-content">
            <section class="series-diamond">
                <div class="section--inner">
                    <div class="diamonds-wrapper">
                        <a href="javascript:;" class="item sample" target="_blank" hidden></a>
                        <div class="diamonds-container"></div>
                        <div class="diamond-box-wrap for-side sample" hidden>
                            <div class="diamond-box">
                                <div class="diamond-box-inner">
                                    <a class="item" href="javascript:;"></a>
                                </div>
                            </div>
                        </div>
                        <div class="series-content">
                            <div class="diamond-box-wrap diamond-main-wrap">
                                <div class="diamond-box">
                                    <div class="diamond-box-inner">
                                        <div class="item">
                                            <div class="user-item">
                                                <div class="item__image">
                                                    <img src="<?php echo $user['image'] ?>" alt="" />
                                                </div>
                                                <div class="item__name"><?php echo $user['f_name'];?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="diamond-box-wrap diamond-box--lg-top">
                                <div class="diamond-box">
                                    <div class="diamond-box-inner">
                                        <div class="item">
                                            <a href="preview_series?id=<?php echo count($user_series) ? $user_series[0]['series_ID'] : 0;?>" target="_blank" />
                                                <img />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="diamond-box-wrap diamond-box--lg-bottom">
                                <div class="diamond-box">
                                    <div class="diamond-box-inner">
                                        <div class="item">
                                            <div class="share-with-list flex-row vertical-center horizon-center margin-between">
                                                <div class="flex-col fix-col">
                                                    <a target="<?php echo $user['social_facebook'] ? '_blank' : '_self';?>" href="<?php echo $user['social_facebook'] ?: 'javascript:;';?>" class="share-item item-facebook">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                <g>
                                    <circle style="fill:#3B5998;" cx="56.098" cy="56.098" r="56.098"/>
                                    <path style="fill:#FFFFFF;" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                        c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                </g>
                                </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <a target="<?php echo $user['social_twitter'] ? '_blank' : '_self';?>" href="<?php echo $user['social_twitter'] ?: 'javascript:;';?>" class="share-item item-twitter">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
                                                c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
                                                c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
                                                c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
                                                c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
                                                c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
                                                c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
                                                C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
                                        </g>
                                    </g>
                                </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <a target="<?php echo $user['social_linkedin'] ? '_blank' : '_self';?>" href="<?php echo $user['social_linkedin'] ?: 'javascript:;';?>" class="share-item item-linkedin">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <circle style="fill:#007AB9;" cx="56.098" cy="56.097" r="56.098"/>
                                        <g>
                                            <path style="fill:#F1F2F2;" d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
                                                c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
                                                c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
                                                C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
                                                c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
                                                 M27.865,83.739H41.27V43.409H27.865V83.739z"/>
                                        </g>
                                    </g>
                                </svg>
                                                    </a>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <a href="mailto:<?php echo $user['email'];?>" class="share-item item-email">
                                                        <svg xmlns="http://www.w3.org/2000/svg" height="512pt" version="1.1" viewBox="0 0 512 512" width="512pt">
                                                            <g id="surface1">
                                                                <path d="M 512 256 C 512 397.386719 397.386719 512 256 512 C 114.613281 512 0 397.386719 0 256 C 0 114.613281 114.613281 0 256 0 C 397.386719 0 512 114.613281 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(35.294118%,32.941176%,87.843137%);fill-opacity:1;" />
                                                                <path d="M 512 256 C 512 245.425781 511.347656 235.003906 510.101562 224.765625 L 379.667969 94.332031 L 100.5 409 L 196.542969 505.042969 C 215.625 509.582031 235.527344 512 256 512 C 397.386719 512 512 397.386719 512 256 Z M 512 256 " style=" stroke:none;fill-rule:nonzero;fill:rgb(22.745098%,25.490196%,80%);fill-opacity:1;" />
                                                                <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 100.5 409 L 411.5 409 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(98.039216%,36.470588%,5.882353%);fill-opacity:1;" />
                                                                <path d="M 411.5 188 L 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 409 L 411.5 409 Z M 411.5 188 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.705882%,29.803922%,5.882353%);fill-opacity:1;" />
                                                                <path d="M 314.953125 82.164062 C 283.304688 47.472656 228.695312 47.472656 197.046875 82.164062 L 100.5 188 L 197.046875 293.835938 C 228.695312 328.527344 283.304688 328.527344 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;" />
                                                                <path d="M 314.953125 82.164062 C 299.070312 64.753906 277.40625 56.085938 255.761719 56.152344 L 255.761719 319.847656 C 277.40625 319.910156 299.070312 311.246094 314.953125 293.835938 L 411.5 188 Z M 314.953125 82.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(92.156863%,92.156863%,94.117647%);fill-opacity:1;" />
                                                                <path d="M 100.5 409 L 197.046875 303.164062 C 228.695312 268.472656 283.304688 268.472656 314.953125 303.164062 L 411.5 409 Z M 100.5 409 " style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,58.431373%,0%);fill-opacity:1;" />
                                                                <path d="M 314.953125 303.164062 C 299.070312 285.753906 277.40625 277.085938 255.761719 277.152344 L 255.761719 409 L 411.5 409 Z M 314.953125 303.164062 " style=" stroke:none;fill-rule:nonzero;fill:rgb(91.764706%,50.980392%,2.352941%);fill-opacity:1;" />
                                                                <path d="M 132.332031 94.332031 L 132.332031 222.894531 L 197.046875 293.835938 C 212.871094 311.179688 234.4375 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 132.332031 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(90.196078%,90.196078%,90.196078%);fill-opacity:1;" />
                                                                <path d="M 255.761719 94.332031 L 255.761719 319.847656 C 255.839844 319.847656 255.921875 319.855469 256 319.855469 C 277.5625 319.855469 299.128906 311.179688 314.953125 293.835938 L 379.667969 222.894531 L 379.667969 94.332031 Z M 255.761719 94.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(84.313725%,84.313725%,84.313725%);fill-opacity:1;" />
                                                                <path d="M 159 124.332031 L 231 124.332031 L 231 211 L 159 211 Z M 159 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                                <path d="M 247 124.332031 L 355.667969 124.332031 L 355.667969 139 L 247 139 Z M 247 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                                <path d="M 255.761719 124.332031 L 355.667969 124.332031 L 355.667969 139 L 255.761719 139 Z M 255.761719 124.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                                <path d="M 247 160.332031 L 355.667969 160.332031 L 355.667969 175 L 247 175 Z M 247 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                                <path d="M 255.761719 160.332031 L 355.667969 160.332031 L 355.667969 175 L 255.761719 175 Z M 255.761719 160.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                                <path d="M 247 196.332031 L 355.667969 196.332031 L 355.667969 211 L 247 211 Z M 247 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                                <path d="M 255.761719 196.332031 L 355.667969 196.332031 L 355.667969 211 L 255.761719 211 Z M 255.761719 196.332031 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                                <path d="M 159 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 159 246.40625 Z M 159 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(43.921569%,48.235294%,54.901961%);fill-opacity:1;" />
                                                                <path d="M 255.761719 231.738281 L 355.667969 231.738281 L 355.667969 246.40625 L 255.761719 246.40625 Z M 255.761719 231.738281 " style=" stroke:none;fill-rule:nonzero;fill:rgb(29.411765%,33.333333%,40.784314%);fill-opacity:1;" />
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="profile-description">
                <div class="section--inner">
                    <span class="section-mark">&ldquo;</span>
                    <div class="user_description">
                        <?php echo $user['about_me'];?>
                    </div>
                </div>
            </section>
            <section class="content-channel">
                <div class="section--inner">
                    <div class="list__item sample" hidden>
                        <article class="series-article">
                            <div class="item-title d-none d-xs-block"></div>
                            <div class="item-image-wrapper">
                                <div class="item-image"></div>
                                <div class="hover-content">
                                    <div class="footer-buttons">
                                        <div class="flex-row">
                                            <div class="flex-col">
                                                <a href="javascript:;" class="preview-btn" target="_blank">
                                                    <svg class="preview-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969
                                                                    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160
                                                                    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969
                                                                    S360.813,263.01,357.771,264.969z"/>
                                                            </g>
                                                        </g>
                                                    </svg> preview
                                                </a>
                                            </div>
                                            <div class="flex-col last-col">
                                                <a href="javascript:;" class="join-btn">
                                                    <svg class="join-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    <svg class="purchased-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#1fcd98"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                                    join
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="child-count-wrapper">
                                <div class="child-count"><span>2</span> posts</div>
                            </div>
                            <div class="item-title d-xs-none"></div>
                            <div class="item-category-wrapper">
                                <a href="javascript:;" class="item-category" target="_blank">category name</a>
                            </div>
                        </article>
                    </div>
                    <div class="series--list">
                        <div class="list__head">
                            <div class="head__inner">
                                <div class="head__title"><?php echo $user['f_name']?>'s Content Channels</div>
                                <div class="head__description">
                                    The amazing things that have been created and curated by <?php echo $user['f_name']?>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    let userSeries = <?php echo json_encode($user_series)?>;
    let user = <?php echo json_encode($user);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/user_profile-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/user_profile-page.js?version=<?php echo time();?>"></script>

</body>
</html>