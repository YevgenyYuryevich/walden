<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.11.2018
 * Time: 17:43
 */
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>My additions to the world</title>
    <meta name="description" content="Make the world a better place by creating great experiences and sharing them with the people you love.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my_series-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormField/FormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SkypeLoader/SkypeLoader.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/components/SeriesConversation/SeriesConversation.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/my_series-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-my-solutions <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <div class="site-content <?php echo !count($series) ? 'no-content' : '';?>">
        <div class="filter-wrapper">
            <div class="filter-content">
                <div class="flex-row vertical-center flex-wrap">
                    <div class="flex-col">
                        <div class="category-filter-wrapper">
                            <div class="my-series-title-wrapper">
                                <div class="flex-row vertical-center">
                                    <div class="flex-col fix-col">
                                        <h2 class="my-series-title">
                                            My contributions
                                        </h2>
                                    </div>
                                    <div class="flex-col fix-col d-xs-none">
                                        <a href="create_series" class="create-series" target="_blank">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="categories-container d-xs-none d-no-content-none">
                                <div class="flex-row vertical-center margin-between flex-wrap">
                                    <div class="flex-col fix-col">
                                        <a href="javascript:;" class="filter-category active" data-category="-1">All</a>
                                    </div>
                                    <?php foreach ($categories as $category): ?>
                                        <div class="flex-col fix-col">
                                            <a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-none d-xs-block d-no-content-none">
                        <div class="categories-container-mobile">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                    <span>All</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                                <ul class="dropdown-menu">
                                    <li hidden><a href="javascript:;" class="filter-category" data-category="-1">All</a></li>
                                    <?php foreach ($categories as $category): ?>
                                        <li><a href="javascript:;" class="filter-category" data-category="<?php echo $category['category_ID']?>"><?php echo $category['strCategory_name'];?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none d-no-content-none">
                        <div class="sort-by">
                            <h4 class="filter-name">Filter by</h4>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" data-hover="dropdown">
                                    <span>All</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M402.207,182.625    L217.75,367.083c-4.167,4.167-9.625,6.25-15.083,6.25c-5.458,0-10.917-2.083-15.083-6.25L88.46,267.958    c-4.167-4.165-4.167-10.919,0-15.085l15.081-15.082c4.167-4.165,10.919-4.165,15.086,0l84.04,84.042L372.04,152.458    c4.167-4.165,10.919-4.165,15.086,0l15.081,15.082C406.374,171.706,406.374,178.46,402.207,182.625z" fill="#737661"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                                <ul class="dropdown-menu">
                                    <li hidden><a href="#" data-value="-1">All</a></li>
                                    <li><a href="#" data-value="audio">Audio</a></li>
                                    <li><a href="#" data-value="video">Video</a></li>
                                    <li><a href="#" data-value="text">Text</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col d-xs-none d-no-content-none">
                        <div class="statistics-wrapper">
                            <h3 class="filter-name">Statistics</h3>
                            <div>
                                <a href="javascript:;" class="filter-date active" data-when="<?php echo date('Y-m-d');?>">November</a>
                                <a href="javascript:;" class="filter-date" data-when="<?php echo date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));?>">October</a>
                                <a href="javascript:;" class="filter-date" data-when="calendar">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="36.447px" height="36.447px" viewBox="0 0 36.447 36.447" style="enable-background:new 0 0 36.447 36.447;"
                                         xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M30.224,3.948h-1.098V2.75c0-1.517-1.197-2.75-2.67-2.75c-1.474,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75
                                                                c0-1.517-1.197-2.75-2.67-2.75c-1.473,0-2.67,1.233-2.67,2.75v1.197h-2.74V2.75c0-1.517-1.197-2.75-2.67-2.75
                                                                c-1.473,0-2.67,1.233-2.67,2.75v1.197H6.224c-2.343,0-4.25,1.907-4.25,4.25v24c0,2.343,1.907,4.25,4.25,4.25h24
                                                                c2.344,0,4.25-1.907,4.25-4.25v-24C34.474,5.855,32.567,3.948,30.224,3.948z M25.286,2.75c0-0.689,0.525-1.25,1.17-1.25
                                                                c0.646,0,1.17,0.561,1.17,1.25v4.896c0,0.689-0.524,1.25-1.17,1.25c-0.645,0-1.17-0.561-1.17-1.25V2.75z M17.206,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z M9.125,2.75
                                                                c0-0.689,0.525-1.25,1.17-1.25s1.17,0.561,1.17,1.25v4.896c0,0.689-0.525,1.25-1.17,1.25s-1.17-0.561-1.17-1.25V2.75z
                                                                 M31.974,32.198c0,0.965-0.785,1.75-1.75,1.75h-24c-0.965,0-1.75-0.785-1.75-1.75v-22h27.5V32.198z"/>
                                                            <rect x="6.724" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="12.857" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="18.995" y="14.626" width="4.595" height="4.089"/>
                                                            <rect x="25.128" y="14.626" width="4.596" height="4.089"/>
                                                            <rect x="6.724" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="20.084" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="20.084" width="4.596" height="4.086"/>
                                                            <rect x="6.724" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="12.857" y="25.54" width="4.596" height="4.086"/>
                                                            <rect x="18.995" y="25.54" width="4.595" height="4.086"/>
                                                            <rect x="25.128" y="25.54" width="4.596" height="4.086"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                    <input type="text" class="active-date" data-date-format="yyyy-mm-dd"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-xs-block d-no-content-none">
                    <div class="series-filter-wrapper">
                        <div class="flex-row vertical-center align-items-xs-start">
                            <div class="flex-col fix-col">
                                <a href="javascript:;" class="filter-item active" data-id="-1">All</a>
                            </div>
                            <?php foreach ($series as $item): ?>
                                <div class="flex-col fix-col">
                                    <a href="javascript:;" class="filter-item" data-id="<?php echo $item['series_ID'];?>"><?php echo $item['strSeries_title'];?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <main class="main-content d-no-content-none">
            <div class="series-container">
                <div class="series-item sample" hidden>
                    <div class="d-none d-xs-block">
                        <div class="series-item-mobile-header">
                            <div class="flex-row vertical-center margin-between flex-wrap">
                                <div class="flex-col fix-col max-width-100">
                                    <div class="series-title">I am series title</div>
                                </div>
                                <div class="flex-col fix-col max-width-100">
                                    <div class="series-viewed-times">(Viewed 1 time)</div>
                                </div>
                                <div class="flex-col">
                                    <a href="javascript:;" target="_blank" class="add-post-wrapper flex-centering">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-row flex-wrap">
                        <div class="flex-col fb-0 content-col">
                            <div class="series-content-wrapper">
                                <header class="series-content-header d-xs-none">
                                    <div class="flex-row vertical-center">
                                        <div class="flex-col fix-col">
                                            <h3 class="series-title">Experience name</h3>
                                        </div>
                                        <div class="flex-col">
                                            <div class="series-viewed-times">(Viewed <span class="viewed-times-val">306</span> times)</div>
                                        </div>
                                        <div class="flex-col fix-col overflow-visible">
                                            <div class="actions-container">
                                                <div class="flex-row vertical-center margin-between">
                                                    <div class="flex-col">
                                                        <a href="javascript:;" target="_blank" class="action-item edit-action">
                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                <g>
                                                                    <g>
                                                                        <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                        <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                        <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                            l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                    <div class="flex-col">
                                                        <a href="javascript:;" class="action-item edit-structure-action" target="_blank">
                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <path style="fill:#455A64;" d="M501.333,96H138.667C132.776,96,128,91.224,128,85.333c0-5.891,4.776-10.667,10.667-10.667h362.667
                                        c5.891,0,10.667,4.776,10.667,10.667C512,91.224,507.224,96,501.333,96z"></path>
                                    <path style="fill:#455A64;" d="M501.333,266.667H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,261.891,507.224,266.667,501.333,266.667z"></path>
                                    <path style="fill:#455A64;" d="M501.333,437.333H138.667c-5.891,0-10.667-4.776-10.667-10.667c0-5.891,4.776-10.667,10.667-10.667
                                        h362.667c5.891,0,10.667,4.776,10.667,10.667C512,432.558,507.224,437.333,501.333,437.333z"></path>
                                </g>
                                                                <g>
                                                                    <circle style="fill:#2196F3;" cx="53.333" cy="256" r="53.333"></circle>
                                                                    <circle style="fill:#2196F3;" cx="53.333" cy="426.667" r="53.333"></circle>
                                                                    <circle style="fill:#2196F3;" cx="53.333" cy="85.333" r="53.333"></circle>
                                                                </g>
                            </svg>
                                                        </a>
                                                    </div>
                                                    <div class="flex-col overflow-visible">
                                                        <?php require_once ASSETS_PATH . '/components/SeriesCopyEmbed/SeriesCopyEmbed.html';?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </header>
                                <div class="series-content-body">
                                    <div class="flex-row margin-between">
                                        <div class="flex-col fb-0">
                                            <div class="posts-list-wrapper">
                                                <div class="flex-row vertical-center margin-between">
                                                    <div class="flex-col fix-col overflow-visible d-xs-none">
                                                        <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-left">
                                                        </a>
                                                    </div>
                                                    <div class="flex-col fb-0">
                                                        <article class="post-item sample" hidden>
                                                            <header class="item-header">
                                                                <div class="flex-row vertical-center margin-between">
                                                                    <div class="flex-col fix-col">
                                                                        <div class="media-icon-wrapper">
                                                                            <i class="fa fa-youtube-play video-icon"></i>
                                                                            <i class="fa fa-volume-up audio-icon"></i>
                                                                            <i class="fa fa-file-text text-icon"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col">
                                                                        <div class="day-wrapper">
                                                                            <span>day</span> <span class="day-value">11</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-col fix-col">
                                                                        <a class="item-action delete-action" href="javascript:;">
                                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                 width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
    <g>
        <polygon points="424.032,443.7 443.7,424.032 325.667,306 443.7,187.967 424.032,168.3 306,286.333 187.967,168.3 168.3,187.967
			286.333,306 168.3,424.032 187.967,443.7 306,325.667 		"/>
        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306
			C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306
			S459.64,584.182,306,584.182S27.818,459.64,27.818,306z"/>
    </g>
</g>
</svg>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flex-col fix-col d-xs-none">
                                                                        <a class="item-action edit-action flex-centering" href="javascript:;">
                                                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="540.329px" height="540.329px" viewBox="0 0 540.329 540.329" style="enable-background:new 0 0 540.329 540.329;" xml:space="preserve">
                                                                                    <g>
                                                                                        <g>
                                                                                            <polygon points="0.002,540.329 58.797,532.66 7.664,481.528 		"></polygon>
                                                                                            <polygon points="16.685,412.341 10.657,458.56 81.765,529.668 127.983,523.64 442.637,208.992 331.338,97.688 		"></polygon>
                                                                                            <path d="M451.248,5.606C447.502,1.861,442.57,0,437.57,0c-5.264,0-10.6,2.062-14.701,6.157L346.92,82.106l111.299,111.298
                                                                                                l75.949-75.949c7.992-7.986,8.236-20.698,0.557-28.378L451.248,5.606z"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </header>
                                                            <div class="item-type-body">
                                                                <div class="blog-type type-0 sample" hidden>
                                                                </div>
                                                                <div class="blog-type type-2 sample" hidden>
                                                                    <img alt="">
                                                                </div>
                                                                <div class="blog-type type-default sample" hidden>
                                                                    Lorem lpsum is simply dummy text of the printing and type
                                                                </div>
                                                                <aside class="blog-duration">
                                                                    waiting...
                                                                </aside>
                                                            </div>
                                                            <div class="title-wrapper">
                                                                <a href="javascript:;" class="item-title" target="_blank">Why gifted student needs to be taught formal writing</a>
                                                            </div>
                                                        </article>
                                                        <div class="posts-list">
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col overflow-visible d-xs-none">
                                                        <a href="javascript:;" class="nav-action arrow-icon-wrapper arrow-right">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-col fix-col d-xs-none">
                                            <aside class="aside-wrapper">
                                                <a href="javascript:;" target="_blank" class="add-post-wrapper flex-centering">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <g>
                                                            <g>
                                                                <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <div class="full-list-wrapper">
                                                    <a href="javascript:;" class="full-list-action" target="_blank">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 19.132 19.132" style="enable-background:new 0 0 19.132 19.132;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path style="fill:#030104;" d="M9.172,9.179V0.146H0v9.033H9.172z"/>
                                                                    <path style="fill:#030104;" d="M19.132,9.179V0.146H9.959v9.033H19.132z"/>
                                                                    <path style="fill:#030104;" d="M19.132,18.986V9.955H9.959v9.032H19.132z"/>
                                                                    <path style="fill:#030104;" d="M9.172,18.986V9.955H0v9.032H9.172z"/>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        <span>preview</span>
                                                    </a>
                                                </div>
                                            </aside>
                                        </div>
                                    </div>
                                </div>
                                <footer class="series-content-footer">
                                    <form class="invite-friend-form" method="post">
                                        <aside class="plane-wrapper d-none d-xs-block">
                                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/plane.png" alt="">
                                        </aside>
                                        <label for="friend-email-input">Invite a friend</label>
                                        <div class="d-none d-xs-block">
                                            <div class="to-add-txt">to add content to the series together</div>
                                        </div>
                                        <div class="input-wrapper">
                                            <div class="flex-row margin-between">
                                                <div class="flex-col">
                                                    <input type="text" name="emails" placeholder="Type your friend's email to invite." required>
                                                </div>
                                                <div class="flex-col fix-col">
                                                    <button class="btn" type="submit">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <polygon style="fill:#5EBAE7;" points="490.452,21.547 16.92,235.764 179.068,330.053 179.068,330.053 "/>
                                                            <polygon style="fill:#36A9E1;" points="490.452,21.547 276.235,495.079 179.068,330.053 179.068,330.053 "/>
                                                            <rect x="257.137" y="223.122" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 277.6362 609.0793)" style="fill:#FFFFFF;" width="15.652" height="47.834"/>
                                                            <path style="fill:#1D1D1B;" d="M0,234.918l174.682,102.4L277.082,512L512,0L0,234.918z M275.389,478.161L190.21,332.858
                                                                l52.099-52.099l-11.068-11.068l-52.099,52.099L33.839,236.612L459.726,41.205L293.249,207.682l11.068,11.068L470.795,52.274
                                                                L275.389,478.161z"/>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </footer>
                            </div>
                        </div>
                        <div class="flex-col fix-col charge-col d-none">
                            <div class="series-income-wrapper">
                                <div class="earned-wrapper d-xs-none">
                                    <h3 class="value-wrapper">
                                        $<span class="total-earned-value">25</span> earned
                                    </h3>
                                    <div class="total-txt">in total this month</div>
                                </div>
                                <div class="states-list d-xs-flex space-between">
                                    <div class="state-item sign-up-state">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col fix-col d-xs-none">
                                                <div class="state-icon-wrapper">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 40.749 40.749" style="enable-background:new 0 0 40.749 40.749;" xml:space="preserve">
<g>
    <path d="M16.856,16.826c0.234,1.927,1.891,3.925,3.396,3.925c1.729,0,3.373-2.099,3.631-3.925c0.098-0.069,0.256-0.237,0.312-0.621
		c0,0,0.371-1.32-0.117-1.178c0.17-0.51,0.736-2.494-0.363-3.728c0,0-0.508-0.699-1.756-1.072c-0.043-0.038-0.09-0.073-0.141-0.107
		c0,0,0.027,0.031,0.07,0.086c-0.072-0.02-0.146-0.037-0.223-0.055c-0.068-0.07-0.143-0.144-0.23-0.222c0,0,0.076,0.078,0.166,0.208
		c-0.033-0.008-0.064-0.017-0.096-0.022c-0.059-0.087-0.129-0.178-0.215-0.266c0,0,0.037,0.069,0.084,0.179
		c-0.229-0.169-0.689-0.56-0.689-0.997c0,0-0.289,0.135-0.461,0.384c0.066-0.206,0.18-0.394,0.361-0.549c0,0-0.189,0.099-0.367,0.31
		c-0.135,0.075-0.445,0.289-0.551,0.667l-0.096-0.05c0.051-0.108,0.117-0.22,0.211-0.334c0,0-0.135,0.122-0.254,0.314l-0.201-0.102
		c0.059-0.111,0.139-0.225,0.244-0.338c0,0-0.107,0.083-0.221,0.223c0.031-0.131,0.025-0.279-0.379,0.163
		c0,0-1.82,0.791-2.348,2.427c0,0-0.311,0.738,0.1,2.912c-0.582-0.277-0.184,1.149-0.184,1.149
		C16.601,16.591,16.759,16.757,16.856,16.826z M20.165,9.249c-0.086,0.125-0.162,0.277-0.199,0.464l-0.065-0.025
		C19.95,9.532,20.034,9.381,20.165,9.249z"/>
    <path d="M29.122,29.857c0.369-3.202-1.213-6.805-1.213-6.805c-0.385-0.854-2.357-1.614-3.094-1.873
		c-0.178-0.063-0.281-0.097-0.281-0.097c-1.549-0.546-1.561-1.089-1.561-1.089c-3.041,5.996-5.352,0.016-5.352,0.016
		c-0.209,0.809-3.338,1.76-3.338,1.76c-0.916,0.352-1.303,0.878-1.303,0.878c-1.354,2.005-1.51,6.471-1.51,6.471
		c0.016,1.016,0.453,1.124,0.453,1.124c3.115,1.389,7.994,1.636,7.994,1.636c2.703,0.055,5.002-0.364,6.555-0.76
		c1.322-0.341,2.102-0.664,2.102-0.664C29.105,30.12,29.122,29.857,29.122,29.857z"/>
    <path d="M39.595,22.085c-0.432-0.96-3.072-1.792-3.072-1.792c-1.406-0.496-1.418-0.992-1.418-0.992
		c-2.768,5.455-4.869,0.016-4.869,0.016c-0.137,0.525-1.629,1.114-2.471,1.41c0.676,0.235,2.469,0.928,2.818,1.705
		c0,0,1.445,4.52,1.111,7.431c0,0-0.014,0.24-0.496,0.544c0,0-0.92,0.729-2.127,1.038c8.951-0.335,11.133-2.626,11.133-2.626
		c0.48-0.304,0.494-0.543,0.494-0.543C41.036,25.365,39.595,22.085,39.595,22.085z"/>
    <path d="M29.308,16.418c0.213,1.755,1.723,3.571,3.092,3.571c1.572,0,3.068-1.91,3.305-3.571c0.088-0.063,0.229-0.216,0.285-0.562
		c0,0,0.338-1.203-0.109-1.074c0.156-0.463,0.672-2.27-0.328-3.392c0,0-0.463-0.638-1.6-0.976c-0.039-0.033-0.082-0.066-0.127-0.099
		c0,0,0.023,0.03,0.064,0.079c-0.068-0.017-0.135-0.034-0.205-0.049c-0.059-0.064-0.129-0.132-0.207-0.203
		c0,0,0.068,0.071,0.152,0.187c-0.031-0.006-0.059-0.013-0.092-0.019c-0.051-0.079-0.113-0.16-0.191-0.242
		c0,0,0.031,0.063,0.076,0.163c-0.209-0.154-0.631-0.509-0.631-0.906c0,0-0.262,0.124-0.418,0.348
		c0.064-0.186,0.164-0.357,0.33-0.499c0,0-0.174,0.091-0.332,0.282c-0.123,0.069-0.408,0.264-0.502,0.608l-0.092-0.046
		c0.049-0.099,0.105-0.201,0.191-0.304c0,0-0.123,0.11-0.229,0.285l-0.184-0.092c0.053-0.1,0.125-0.205,0.221-0.308
		c0,0-0.096,0.074-0.203,0.203c0.031-0.12,0.023-0.254-0.34,0.147c0,0-1.656,0.72-2.137,2.208c0,0-0.285,0.672,0.09,2.649
		c-0.531-0.25-0.17,1.049-0.17,1.049C29.073,16.204,29.22,16.355,29.308,16.418z M32.319,9.523
		c-0.078,0.113-0.148,0.252-0.184,0.423l-0.054-0.023C32.124,9.78,32.202,9.642,32.319,9.523z"/>
    <path d="M9.056,29.863c-0.334-2.911,1.113-7.431,1.113-7.431c0.35-0.777,2.141-1.468,2.812-1.705
		c-0.84-0.294-2.33-0.885-2.469-1.41c0,0-2.1,5.44-4.869-0.016c0,0-0.008,0.496-1.416,0.992c0,0-2.639,0.832-3.072,1.792
		c0,0-1.439,3.28-1.104,6.191c0,0,0.016,0.24,0.496,0.545c0,0,2.18,2.289,11.129,2.625c-1.203-0.31-2.127-1.039-2.127-1.039
		C9.071,30.103,9.056,29.863,9.056,29.863z"/>
    <path d="M4.757,15.856c0.057,0.348,0.203,0.499,0.291,0.562c0.232,1.663,1.729,3.573,3.303,3.573c1.371,0,2.877-1.818,3.09-3.573
		c0.09-0.063,0.236-0.214,0.291-0.562c0,0,0.361-1.299-0.168-1.049c0.371-1.977,0.09-2.647,0.09-2.647
		c-0.48-1.489-2.137-2.209-2.137-2.209c-0.37-0.403-0.375-0.267-0.344-0.148C9.069,9.675,8.972,9.6,8.972,9.6
		c0.093,0.103,0.166,0.207,0.222,0.309l-0.183,0.09c-0.107-0.175-0.23-0.284-0.23-0.284c0.084,0.102,0.145,0.205,0.191,0.304
		l-0.09,0.045C8.788,9.721,8.503,9.528,8.38,9.458C8.222,9.267,8.046,9.177,8.046,9.177c0.166,0.142,0.268,0.312,0.328,0.497
		c-0.152-0.224-0.42-0.347-0.42-0.347c0,0.396-0.416,0.751-0.625,0.905c0.041-0.099,0.074-0.161,0.074-0.161
		c-0.076,0.081-0.14,0.161-0.195,0.241c-0.027,0.005-0.055,0.013-0.086,0.019c0.08-0.118,0.15-0.188,0.15-0.188
		c-0.082,0.071-0.146,0.138-0.209,0.203c-0.07,0.016-0.137,0.032-0.201,0.05c0.037-0.05,0.065-0.079,0.065-0.079
		c-0.047,0.031-0.088,0.065-0.125,0.097c-1.141,0.34-1.606,0.976-1.606,0.976c-0.996,1.124-0.482,2.93-0.328,3.392
		C4.425,14.654,4.757,15.856,4.757,15.856z M8.671,9.923L8.612,9.946C8.579,9.775,8.511,9.637,8.429,9.523
		C8.55,9.642,8.624,9.78,8.671,9.923z"/>
</g>
</svg>
                                                </div>
                                            </div>
                                            <div class="flex-col">
                                                <div class="state-content">
                                                    $<span class="sign-up-value">5</span> for sign up
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="state-item people-join-state">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col fix-col d-xs-none">
                                                <div class="state-icon-wrapper">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 40.749 40.749" style="enable-background:new 0 0 40.749 40.749;" xml:space="preserve">
<g>
    <path d="M16.856,16.826c0.234,1.927,1.891,3.925,3.396,3.925c1.729,0,3.373-2.099,3.631-3.925c0.098-0.069,0.256-0.237,0.312-0.621
		c0,0,0.371-1.32-0.117-1.178c0.17-0.51,0.736-2.494-0.363-3.728c0,0-0.508-0.699-1.756-1.072c-0.043-0.038-0.09-0.073-0.141-0.107
		c0,0,0.027,0.031,0.07,0.086c-0.072-0.02-0.146-0.037-0.223-0.055c-0.068-0.07-0.143-0.144-0.23-0.222c0,0,0.076,0.078,0.166,0.208
		c-0.033-0.008-0.064-0.017-0.096-0.022c-0.059-0.087-0.129-0.178-0.215-0.266c0,0,0.037,0.069,0.084,0.179
		c-0.229-0.169-0.689-0.56-0.689-0.997c0,0-0.289,0.135-0.461,0.384c0.066-0.206,0.18-0.394,0.361-0.549c0,0-0.189,0.099-0.367,0.31
		c-0.135,0.075-0.445,0.289-0.551,0.667l-0.096-0.05c0.051-0.108,0.117-0.22,0.211-0.334c0,0-0.135,0.122-0.254,0.314l-0.201-0.102
		c0.059-0.111,0.139-0.225,0.244-0.338c0,0-0.107,0.083-0.221,0.223c0.031-0.131,0.025-0.279-0.379,0.163
		c0,0-1.82,0.791-2.348,2.427c0,0-0.311,0.738,0.1,2.912c-0.582-0.277-0.184,1.149-0.184,1.149
		C16.601,16.591,16.759,16.757,16.856,16.826z M20.165,9.249c-0.086,0.125-0.162,0.277-0.199,0.464l-0.065-0.025
		C19.95,9.532,20.034,9.381,20.165,9.249z"/>
    <path d="M29.122,29.857c0.369-3.202-1.213-6.805-1.213-6.805c-0.385-0.854-2.357-1.614-3.094-1.873
		c-0.178-0.063-0.281-0.097-0.281-0.097c-1.549-0.546-1.561-1.089-1.561-1.089c-3.041,5.996-5.352,0.016-5.352,0.016
		c-0.209,0.809-3.338,1.76-3.338,1.76c-0.916,0.352-1.303,0.878-1.303,0.878c-1.354,2.005-1.51,6.471-1.51,6.471
		c0.016,1.016,0.453,1.124,0.453,1.124c3.115,1.389,7.994,1.636,7.994,1.636c2.703,0.055,5.002-0.364,6.555-0.76
		c1.322-0.341,2.102-0.664,2.102-0.664C29.105,30.12,29.122,29.857,29.122,29.857z"/>
    <path d="M39.595,22.085c-0.432-0.96-3.072-1.792-3.072-1.792c-1.406-0.496-1.418-0.992-1.418-0.992
		c-2.768,5.455-4.869,0.016-4.869,0.016c-0.137,0.525-1.629,1.114-2.471,1.41c0.676,0.235,2.469,0.928,2.818,1.705
		c0,0,1.445,4.52,1.111,7.431c0,0-0.014,0.24-0.496,0.544c0,0-0.92,0.729-2.127,1.038c8.951-0.335,11.133-2.626,11.133-2.626
		c0.48-0.304,0.494-0.543,0.494-0.543C41.036,25.365,39.595,22.085,39.595,22.085z"/>
    <path d="M29.308,16.418c0.213,1.755,1.723,3.571,3.092,3.571c1.572,0,3.068-1.91,3.305-3.571c0.088-0.063,0.229-0.216,0.285-0.562
		c0,0,0.338-1.203-0.109-1.074c0.156-0.463,0.672-2.27-0.328-3.392c0,0-0.463-0.638-1.6-0.976c-0.039-0.033-0.082-0.066-0.127-0.099
		c0,0,0.023,0.03,0.064,0.079c-0.068-0.017-0.135-0.034-0.205-0.049c-0.059-0.064-0.129-0.132-0.207-0.203
		c0,0,0.068,0.071,0.152,0.187c-0.031-0.006-0.059-0.013-0.092-0.019c-0.051-0.079-0.113-0.16-0.191-0.242
		c0,0,0.031,0.063,0.076,0.163c-0.209-0.154-0.631-0.509-0.631-0.906c0,0-0.262,0.124-0.418,0.348
		c0.064-0.186,0.164-0.357,0.33-0.499c0,0-0.174,0.091-0.332,0.282c-0.123,0.069-0.408,0.264-0.502,0.608l-0.092-0.046
		c0.049-0.099,0.105-0.201,0.191-0.304c0,0-0.123,0.11-0.229,0.285l-0.184-0.092c0.053-0.1,0.125-0.205,0.221-0.308
		c0,0-0.096,0.074-0.203,0.203c0.031-0.12,0.023-0.254-0.34,0.147c0,0-1.656,0.72-2.137,2.208c0,0-0.285,0.672,0.09,2.649
		c-0.531-0.25-0.17,1.049-0.17,1.049C29.073,16.204,29.22,16.355,29.308,16.418z M32.319,9.523
		c-0.078,0.113-0.148,0.252-0.184,0.423l-0.054-0.023C32.124,9.78,32.202,9.642,32.319,9.523z"/>
    <path d="M9.056,29.863c-0.334-2.911,1.113-7.431,1.113-7.431c0.35-0.777,2.141-1.468,2.812-1.705
		c-0.84-0.294-2.33-0.885-2.469-1.41c0,0-2.1,5.44-4.869-0.016c0,0-0.008,0.496-1.416,0.992c0,0-2.639,0.832-3.072,1.792
		c0,0-1.439,3.28-1.104,6.191c0,0,0.016,0.24,0.496,0.545c0,0,2.18,2.289,11.129,2.625c-1.203-0.31-2.127-1.039-2.127-1.039
		C9.071,30.103,9.056,29.863,9.056,29.863z"/>
    <path d="M4.757,15.856c0.057,0.348,0.203,0.499,0.291,0.562c0.232,1.663,1.729,3.573,3.303,3.573c1.371,0,2.877-1.818,3.09-3.573
		c0.09-0.063,0.236-0.214,0.291-0.562c0,0,0.361-1.299-0.168-1.049c0.371-1.977,0.09-2.647,0.09-2.647
		c-0.48-1.489-2.137-2.209-2.137-2.209c-0.37-0.403-0.375-0.267-0.344-0.148C9.069,9.675,8.972,9.6,8.972,9.6
		c0.093,0.103,0.166,0.207,0.222,0.309l-0.183,0.09c-0.107-0.175-0.23-0.284-0.23-0.284c0.084,0.102,0.145,0.205,0.191,0.304
		l-0.09,0.045C8.788,9.721,8.503,9.528,8.38,9.458C8.222,9.267,8.046,9.177,8.046,9.177c0.166,0.142,0.268,0.312,0.328,0.497
		c-0.152-0.224-0.42-0.347-0.42-0.347c0,0.396-0.416,0.751-0.625,0.905c0.041-0.099,0.074-0.161,0.074-0.161
		c-0.076,0.081-0.14,0.161-0.195,0.241c-0.027,0.005-0.055,0.013-0.086,0.019c0.08-0.118,0.15-0.188,0.15-0.188
		c-0.082,0.071-0.146,0.138-0.209,0.203c-0.07,0.016-0.137,0.032-0.201,0.05c0.037-0.05,0.065-0.079,0.065-0.079
		c-0.047,0.031-0.088,0.065-0.125,0.097c-1.141,0.34-1.606,0.976-1.606,0.976c-0.996,1.124-0.482,2.93-0.328,3.392
		C4.425,14.654,4.757,15.856,4.757,15.856z M8.671,9.923L8.612,9.946C8.579,9.775,8.511,9.637,8.429,9.523
		C8.55,9.642,8.624,9.78,8.671,9.923z"/>
</g>
</svg>
                                                </div>
                                            </div>
                                            <div class="flex-col">
                                                <div class="state-content">
                                                    <span class="people-join-value">5</span> people
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="state-item total-income-state">
                                        <div class="flex-row vertical-center margin-between">
                                            <div class="flex-col fix-col">
                                                <div class="state-icon-wrapper">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         width="404.09px" height="404.091px" viewBox="0 0 404.09 404.091" style="enable-background:new 0 0 404.09 404.091;"
                                                         xml:space="preserve">
<g>
    <g id="Layer_8_2_">
        <path d="M195.591,99.628v28.446c-0.011,14.726-43.794,26.667-97.795,26.667C43.784,154.742,0.012,142.8,0,128.075V99.628
			c0,14.73,43.784,26.67,97.795,26.67C151.808,126.299,195.591,114.359,195.591,99.628z M195.591,94.242
			c0,14.733-43.783,26.673-97.795,26.673C43.784,120.915,0,108.975,0,94.242c0-14.731,43.784-26.67,97.795-26.67
			C151.808,67.571,195.591,79.511,195.591,94.242z M145.003,92.725l-85.911-5.863l-2.92,5.25l3.185,0.443
			c4.365,0.615,9.907,1.961,7.335,6.55l-0.801,1.428l15.88,1.083l0.801-1.431c1.104-1.983,1.445-3.68,1.029-5.13l57.894,3.948
			L145.003,92.725z M97.795,162.009C43.784,162.009,0,150.069,0,135.336v28.449c0.012,14.726,43.784,26.668,97.795,26.668
			c54.001,0,97.784-11.942,97.795-26.668v-28.449C195.591,150.069,151.801,162.009,97.795,162.009z M97.795,197.922
			C43.784,197.922,0,185.979,0,171.25v28.451c0.012,14.719,43.784,26.658,97.795,26.658c54.001,0,97.784-11.939,97.795-26.658
			V171.25C195.591,185.979,151.801,197.922,97.795,197.922z M97.795,233.217C43.784,233.217,0,221.276,0,206.541v28.454
			c0.012,14.719,43.784,26.665,97.795,26.665c54.001,0,97.784-11.946,97.795-26.665v-28.454
			C195.591,221.293,151.801,233.217,97.795,233.217z M97.795,268.09C43.784,268.09,0,256.144,0,241.414v28.442
			c0.012,14.73,43.784,26.67,97.795,26.67c54.001,0,97.784-11.939,97.795-26.67v-28.442
			C195.591,256.144,151.801,268.09,97.795,268.09z M97.795,302.891C43.784,302.891,0,290.951,0,276.215v28.454
			c0.012,14.721,43.784,26.665,97.795,26.665c54.001,0,97.784-11.944,97.795-26.665v-28.454
			C195.591,290.951,151.801,302.891,97.795,302.891z M209.106,101.533h0.714v0.722c15.497,2.818,34.122,4.471,54.162,4.471
			c53.995,0,97.771-11.94,97.795-26.665V51.612c0,14.732-43.787,26.673-97.795,26.673c-54.009,0-97.797-11.946-97.797-26.679V62.31
			c23.467,5.642,43.635,15.74,43.635,33.833C209.82,98.036,209.536,99.807,209.106,101.533z M166.185,46.222
			c0-14.731,43.789-26.67,97.797-26.67c54.008,0,97.795,11.939,97.795,26.67c0,14.736-43.787,26.678-97.795,26.678
			C209.973,72.9,166.185,60.958,166.185,46.222z M216.757,47.74l85.909,5.861l2.928-5.245l-3.19-0.441
			c-4.378-0.618-9.898-1.961-7.349-6.549l0.816-1.437l-15.879-1.081l-0.789,1.434c-1.117,1.984-1.456,3.683-1.041,5.133
			l-57.893-3.951L216.757,47.74z M263.982,142.43c53.995,0,97.771-11.939,97.795-26.665V87.316c0,14.73-43.787,26.673-97.795,26.673
			c-20.04,0-38.665-1.652-54.162-4.479v20.466c-0.013,2.583-0.448,4.985-1.228,7.256h1.228v0.722
			C225.309,140.784,243.942,142.43,263.982,142.43z M209.82,173.153v0.726c15.497,2.818,34.122,4.47,54.162,4.47
			c53.995,0,97.771-11.94,97.795-26.667v-28.446c0,14.736-43.787,26.676-97.795,26.676c-20.04,0-38.665-1.647-54.162-4.47v20.243
			c-0.013,2.667-0.494,5.144-1.315,7.469H209.82z M361.777,184.277v-25.743c0,8.901-16.034,16.755-40.593,21.601
			C331.964,180.617,346.919,181.733,361.777,184.277z M209.82,198.245c9.497-6.465,23.682-10.673,38.335-13.409
			c-13.986-0.621-26.998-2.038-38.335-4.101V198.245z M306.294,249.179c-54.014,0-97.795-11.946-97.795-26.688v28.454
			c0.027,14.73,43.792,26.671,97.795,26.671c54.017,0,97.785-11.94,97.796-26.671v-28.454
			C404.098,237.232,360.311,249.179,306.294,249.179z M306.294,190.453c54.017,0,97.796,11.94,97.796,26.665
			c0,14.731-43.779,26.676-97.796,26.676c-54.014,0-97.795-11.944-97.795-26.676C208.5,202.393,252.292,190.453,306.294,190.453z
			 M320.497,216.302l-57.896-3.95l-3.502,6.282l85.91,5.865l2.924-5.258l-3.185-0.433c-4.361-0.623-9.894-1.963-7.335-6.561
			l0.799-1.424l-15.88-1.078l-0.785,1.424C320.418,213.156,320.073,214.857,320.497,216.302z M306.294,284.877
			c-54.014,0-97.795-11.945-97.795-26.676v28.46c0.027,14.73,43.792,26.673,97.795,26.673c54.017,0,97.785-11.942,97.796-26.673
			v-28.442C404.098,272.932,360.311,284.877,306.294,284.877z M306.294,320.79c-54.014,0-97.795-11.939-97.795-26.67v28.449
			c0.027,14.719,43.792,26.665,97.795,26.665c54.017,0,97.785-11.946,97.796-26.665V294.12
			C404.098,308.861,360.311,320.79,306.294,320.79z M306.294,356.103c-54.014,0-97.795-11.947-97.795-26.678v28.454
			c0.027,14.721,43.792,26.66,97.795,26.66c54.017,0,97.785-11.939,97.796-26.66v-28.454
			C404.098,344.155,360.311,356.103,306.294,356.103z"/>
    </g>
</g>
</svg>
                                                </div>
                                            </div>
                                            <div class="flex-col">
                                                <div class="state-content">
                                                    $<span class="total-earned-value">25</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="charge-wrapper d-xs-none">
                                    <h3 class="charge-title">Would you like to charge?</h3>
                                    <div class="flex-row margin-between vertical-center">
                                        <div class="flex-col fix-col">
                                            <label class="c-switch">
                                                <input type="checkbox" name="boolSeries_charge" value="1">
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <div class="flex-col">
                                            <span class="now-free-txt">now your series are free</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-col fix-col series-conversation-col">
                            <?php require ASSETS_PATH . '/components/SeriesConversation/SeriesConversation.php';?>
                        </div>
                    </div>
                    <div class="modal fade charge-modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <a class="modal-close" data-dismiss="modal"></a>
                                <div class="modal-body">
                                    <div class="charge-series-wrapper">
                                        <div class="content-header">
                                            <h3>charge users to join series</h3>
                                        </div>
                                        <div class="charge-content">
                                            <div class="connect-account-wrapper">
                                                <a target="_blank" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=<?php echo $stripeClientId?>&scope=read_write&redirect_uri=<?php echo urlencode(BASE_URL . '/StripeConnect');?>" class="stripe-connect"><span>Connect with Stripe</span></a> to charge for your series
                                            </div>
                                            <div class="group-row-wrapper charge-users-wrapper">
                                                <div class="flex-row margin-between">
                                                    <div class="flex-col fix-col">
                                                        <div class="would-charge-wrapper group-col toggle-col">
                                                            <div class="would-charge-txt col-name">Would you like to charge users?</div>
                                                            <div class="flex-row vertical-center margin-between">
                                                                <div class="flex-col fix-col">
                                                                    <label class="c-switch">
                                                                        <input type="checkbox" name="boolSeries_charge" value="1">
                                                                        <div class="slider round"></div>
                                                                    </label>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <div class="get-payment-txt">you will get payment</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fb-0">
                                                        <div class="charge-price-wrapper group-col">
                                                            <label class="price-label col-name">Price, $:</label>
                                                            <input type="text" name="intSeries_price" class="col-value" placeholder="0"/>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="you-will-get-wrapper group-col gets-col">
                                                            <label class="you-will-get-label col-name">You will get:</label>
                                                            <div class="you-will-get-value col-value"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pay-affiliates-wrapper group-row-wrapper">
                                                <div class="flex-row margin-between">
                                                    <div class="flex-col fix-col">
                                                        <div class="would-pay-affiliate-wrapper group-col toggle-col">
                                                            <div class="would-pay-txt col-name">Would you like to Pay Affiliates for Referrals?</div>
                                                            <div class="flex-row vertical-center margin-between">
                                                                <div class="flex-col fix-col">
                                                                    <label class="c-switch">
                                                                        <input type="checkbox" name="boolSeries_affiliated" value="1">
                                                                        <div class="slider round"></div>
                                                                    </label>
                                                                </div>
                                                                <div class="flex-col fix-col">
                                                                    <div class="get-payment-txt">Affiliate will get fee</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fb-0 overflow-visible">
                                                        <div class="affiliate-percent-wrapper group-col">
                                                            <label class="percent-label col-name">Percent, %:</label>
                                                            <input class="percent-value" name="intSeries_affiliate_percent" type="range" max="100" value="30"/>
                                                        </div>
                                                    </div>
                                                    <div class="flex-col fix-col">
                                                        <div class="affiliate-will-get-wrapper group-col gets-col">
                                                            <label class="affiliate-will-get-label col-name">Affiliate gets:</label>
                                                            <div class="affiliate-will-get-value col-value"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="button-wrapper">
                                            <p>All set!</p>
                                            <button class="save-btn">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="load-more-status" hidden>
                <span>loading</span> <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
        </main>
        <div class="no-content-wrapper">
            <div class="flex-row vertical-center d-xs-block">
                <div class="flex-col min-width-0">
                    <div class="no-content-txt-wrapper">
                        <h2 class="you-not-txt">You have not created an experience yet.</h2>
                        <div class="to-add-txt d-xs-none">To create experience, please click "Create experience" located at the top right of the page</div>
                    </div>
                </div>
                <div class="flex-col min-width-0">
                    <div class="no-content-img-wrapper">
                        <img class="background-img" src="<?php echo BASE_URL; ?>/assets/images/global-icons/cloud.png" alt="" />
                        <div class="to-add-icons-wrapper flex-centering">
                            <a class="create-link" href="create_series" target="_blank">
                                <div class="flex-row vertical-center horizon-center margin-between">
                                    <div class="flex-col fix-col">
                                        <div class="create-txt">Create experience</div>
                                    </div>
                                    <div class="flex-col fix-col">
                                        <div class="create-icon-wrapper">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.839,0,256s114.844,256,256,256s256-114.839,256-256S397.156,0,256,0z M389.594,272.699H272.699    v116.895c0,9.225-7.48,16.699-16.699,16.699c-9.219,0-16.699-7.475-16.699-16.699V272.699H122.406    c-9.219,0-16.699-7.475-16.699-16.699c0-9.225,7.48-16.699,16.699-16.699h116.895V122.406c0-9.225,7.48-16.699,16.699-16.699    c9.219,0,16.699,7.475,16.699,16.699v116.895h116.895c9.219,0,16.699,7.475,16.699,16.699    C406.294,265.225,398.813,272.699,389.594,272.699z" fill="#1fcd98"></path>
                                                </g>
                                            </g>
                                        </svg>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="to-add-txt-mobile d-none d-xs-block">To create experience, please click "Create experience" located at the top right of the page</div>
            </div>
        </div>
        <?php require_once ASSETS_PATH . '/components/PostEditor/PostEditor.php';?>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    var series = <?php echo json_encode($series);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my_series-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesPostsTree/SeriesPostsTree.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoopEditor/FormLoopEditor.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormsList/FormsList.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormField/FormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/FormLoop/FormLoop.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/AnsweredFormField/AnsweredFormField.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/PostEditor/PostEditor.js?version=<?php echo time();?>"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.1/firebase-database.js"></script>
<script src="<?php echo BASE_URL;?>/assets/config/js/firebase-config.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SkypeLoader/SkypeLoader.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/components/SeriesConversation/SeriesConversation.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/my_series-page.js?version=<?php echo time();?>"></script>

</body>
</html>