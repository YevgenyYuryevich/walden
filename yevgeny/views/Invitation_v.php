<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.02.2018
 * Time: 10:10
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Invite friends and family</title>
    <meta name="description" content="Invite your friends and family to enjoy thegreyshirt.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/invitation-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/invitation-page.css?version=<?php echo time();?>">

</head>

<body>
<div class="site-wrapper page page-invitation">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Invite
                </div>
            </div>
            <main class="main-content">
                <div class="content-inner <?php echo $invitationCnt ? 'can-invite' : '';?>">
                    <div class="invite-friend">
                        <h1>Invite friends and familiy to join thegreyshirt</h1>
                        <aside>You have <span class="remaining-invitations"><?php echo $invitationCnt;?> invitations</span> remaining</aside>
                        <form role="form" method="post">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" required/>
                            </div>
                            <footer>
                                <button class="btn btn-circle" type="submit">Submit</button>
                            </footer>
                        </form>
                    </div>
                    <div class="no-invitation">
                        <h1>Thank you for inviting your friends. You have no more invitations.</h1>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/invitation-global.js?version=<?php echo time();?>"></script>
<script>
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initialInvitationCnt = <?php echo $invitationCnt;?>;
</script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/invitation-page.js?version=<?php echo time();?>"></script>

</body>
</html>