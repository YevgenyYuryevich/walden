<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.05.2018
 * Time: 17:01
 */


?>

<!DOCTYPE html>
<html class="walden-site">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="strPost_title">
    <meta name="Description" content='$metaDes'>
    <meta property="og:title" content="strPost_title"/>
    <meta property="og:image" content="strPost_featuredimage"/>
    <meta property="og:description" content='$metaDes'/>

    <title>Title</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/viewEmbed-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/viewEmbed-page.css?version=<?php echo time();?>" />
    <?php if ($externalStyle): ?>
        <link rel="stylesheet" href="<?php echo $externalStyle;?>" />
    <?php endif; ?>
    <?php if ($inlineStyle): ?>
        <style type="text/css"><?php echo $inlineStyle;?></style>
    <?php endif; ?>
</head>
<body>
<div class="site-wrapper page page-view-embed <?php echo $isLoggedIn ? 'logged-in' : '';?>">
    <div class="blog-wrapper">
        <header class="content-header">
            <div class="flex-row space-between vertical-center">
                <div class="flex-col fix-col">
                    <h3 class="welcome">Welcome back, <span class="display-name"><?php echo $isLoggedIn ? $_SESSION['f_name'] : '';?></span>!</h3>
                    <div class="login-btn">
                        <a class="btn big-login" href="javascript:;">Log in</a>
                    </div>
                </div>
                <div class="flex-col fix-col">
                    <aside class="money-saved">
                        <div class="money-amount">$<span><?php echo $totalMoneySaved ? $totalMoneySaved : 0;?></span></div>
                        <div class="w-name">Money saved</div>
                    </aside>
                </div>
            </div>
        </header>
        <div class="blog-content-wrapper">
            <div class="post-image-wrapper">
                <img class="post-img" />
            </div>
            <div class="post-title"></div>
            <div class="post-body">
                <div class="type-0 blog-type sample" hidden>
                    <div class="audiogallery">
                        <div class="items">
                            <div class="audioplayer-tobe" data-thumb="" data-bgimage="assets/images/bg.jpg" data-scrubbg="assets/waves/scrubbg.png" data-scrubprog="assets/images/meditation.png" data-videoTitle="Audio Video" data-type="normal" data-source="" data-sourceogg="sounds/adg3.ogg"></div>
                        </div>
                    </div>
                </div>
                <div class="type-2 blog-type sample" hidden>
                    <div class="video-wrapper">
                        <video controls class="video-js" data-autoresize="fit" width="960" height="540"></video>
                    </div>
                </div>
                <div class="type-8 blog-type sample" hidden>
                </div>
                <div class="type-menu blog-type sample" hidden>
                    <header>
                        <h3 class="menu-title"></h3>
                        <p class="menu-description"></p>
                    </header>
                    <div class="paths-container">
                        <div class="flex-col path-col sample" hidden>
                            <div class="img-wrapper">
                                <img />
                            </div>
                            <h3 class="path-title"></h3>
                            <p class="path-description"></p>
                        </div>
                        <div class="flex-row paths-list flex-wrap">
                        </div>
                    </div>
                </div>
                <div class="type-10 blog-type sample" hidden></div>
                <div class="type-other blog-type sample" hidden></div>
            </div>
        </div>
        <div class="pay-blog-wrapper" hidden>
            <?php require_once ASSETS_PATH . '/components/PayBlog/PayBlog.html';?>
        </div>
        <footer class="right-panel-footer">
            <div class="nav-wrapper">
                <div class="flex-row space-between vertical-center">
                    <div class="flex-col fix-col">
                        <a class="step-back">
                            <span class="content-turbotax__back-arr">‹</span> Back
                        </a>
                    </div>
                    <div class="flex-col fix-col">
                        <a class="step-next">Continue</a>
                    </div>
                </div>
            </div>
            <div class="powered-by">Powered by Walden.ly</div>
        </footer>
    </div>
</div>
<div class="modal fade login" id="loginModal">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login with</h4>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="content">
                        <div class="social">
                            <a class="circle github" href="/auth/github">
                                <i class="fa fa-github fa-fw"></i>
                            </a>
                            <a id="google_login" class="circle google" href="/auth/google_oauth2">
                                <i class="fa fa-google-plus fa-fw"></i>
                            </a>
                            <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                                <i class="fa fa-facebook fa-fw"></i>
                            </a>
                        </div>
                        <div class="division">
                            <div class="line l"></div>
                            <span>or</span>
                            <div class="line r"></div>
                        </div>
                        <div class="error"></div>
                        <div class="form loginBox">
                            <form method="post">
                                <input class="form-control" type="text" placeholder="Email" name="email" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="btn btn-default btn-login" type="submit" value="Login">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content registerBox" style="display:none;">
                        <div class="form">
                            <form method="post">
                                <input class="form-control" type="email" placeholder="Email" name="email" required>
                                <input class="form-control" type="text" placeholder="First Name" name="first_name" required>
                                <input class="form-control" type="text" placeholder="Last Name" name="last_name" required>
                                <input class="form-control" type="password" placeholder="Password" name="password" required>
                                <input class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation" required>
                                <input class="btn btn-default btn-register" type="submit" value="Create account" name="commit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="forgot login-footer">
                            <span>Looking to
                                 <a href="javascript:;" class="show-register">create an account</a>
                            ?</span>
                </div>
                <div class="forgot register-footer" style="display:none">
                    <span>Already have an account?</span>
                    <a class="show-login" href="javascript:;">Login</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script type="text/javascript">
    const BASE_URL = <?php echo json_encode(BASE_URL);?>;
    var CLIENT_ID = <?php echo json_encode($_SESSION['client_ID']);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/viewEmbed-global.js?version=<?php echo time();?>"></script>

<script type="text/javascript">
    const ACTION_URL = BASE_URL + window.location.pathname;
    var initialSeries = <?php echo json_encode($series);?>;
    var from = <?php echo json_encode($from);?>;
    var series = <?php echo json_encode($series);?>;
    var purchased = <?php echo json_encode($purchased);?>;
    var initialPostData = <?php echo json_encode($postData);?>;
    var formFields = <?php echo json_encode($formFields);?>;
    var formFieldAnswers = <?php echo json_encode($formFieldAnswers);?>;
    var welcomePost = <?php echo json_encode($welcomePost);?>;
    var clientName = <?php echo $isLoggedIn ? json_encode($_SESSION['f_name']) : json_encode(false);?>;
    var clientSeriesView = <?php echo json_encode($clientSeriesView);?>;
    var seekPost = <?php echo json_encode($seekPost);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/view-embed-page.js?version=<?php echo time();?>"></script>
</body>
</html>