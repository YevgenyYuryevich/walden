<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 14.09.2018
 * Time: 13:06
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Affiliate - walden</title>
    <meta name="description" content="Change your experience today to better match your current needs.">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-common.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/home-common.css?version=<?php echo time();?>" />
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/yevgeny/affiliate-home-page.css?version=<?php echo time();?>">

</head>
<body>
<div class="site-wrapper page page-affiliate <?php echo isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1 ? 'logged-in' : '';?>">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_header.php';?>
    <main class="main-content">
        <section class="hero-section">
            <h1 class="hero-title title-font">Grow your affiliate business</h1>
            <h4 class="hero-sub-title">with series that people love</h4>
            <div class="hero-image-wrapper">
                <img src="<?php echo BASE_URL;?>/assets/images/global-icons/affiliate-home-hero.png" />
            </div>
        </section>
        <section class="passive-income">
            <h2 class="title-font section-title">
                Passive income
            </h2>
            <div class="section-description">
                by promoting many of the weekly series<br> that we feature
            </div>
            <div class="get-started-free">
                <a href="login?register" class="get-started-btn">Get Started</a>
            </div>
            <div class="terms-condition">
                Terms & Conditions
            </div>
        </section>
        <section class="high-commision">
            <div class="title-font section-title">
                High commision up to 25% each month
            </div>
            <div class="section-description">
                or each time someone signs up<br>
                through your service
            </div>
            <div class="icons-list">

            </div>
        </section>
        <section class="how-affiliate-works">
            <h2 class="section-title">
                How the Affiliate program works
            </h2>
            <article>
                <div class="flex-row vertical-center space-around flex-wrap">
                    <div class="flex-col fix-col">
                        <div class="article-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/article-img.png" />
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="article-explain">
                            <div class="article-title">
                                professionals can charge a fee<br> to access their series
                            </div>
                            <div class="article-description">
                                and set a commission rate<br> for affiliates
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-row vertical-center space-around flex-wrap">
                    <div class="flex-col fix-col">
                        <div class="article-explain">
                            <div class="article-title">
                                you will get that amounts
                            </div>
                            <div class="article-description">
                                each time someone signs up for the affiliate service through your efforts
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="article-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/article-img.png" />
                        </div>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-row vertical-center space-around flex-wrap">
                    <div class="flex-col fix-col">
                        <div class="article-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/article-img.png" />
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="article-explain">
                            <div class="article-title">
                                let's count how much you'll get when new user join
                            </div>
                            <div class="article-description">
                                if a series costs $20 to join and commision is set at 25% you will receive $5 each time someone signs up
                            </div>
                            <aside>commision when user subscribe</aside>
                        </div>
                    </div>
                </div>
            </article>
        </section>
        <section class="choose-how-learn">
            <div class="section-title">Choose how to learn:</div>
            <div class="section-description">
                get commission each time someone signs up <br> or <br>each month when users subscribe
            </div>
            <div class="get-started-free">
                <a href="login?register" class="get-started-btn">Get Started</a>
            </div>
        </section>
        <section class="how-setup-affiliate">
            <h2 class="section-title">
                How to set up affiliate program
            </h2>
            <article>
                <div class="flex-row vertical-center space-around flex-wrap">
                    <div class="flex-col fix-col">
                        <div class="article-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/article-img.png" />
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="article-explain">
                            <div class="article-title">
                                Search for services that interest you
                            </div>
                            <div class="article-description">
                                to find an existing service that fits with the message of your business
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-row vertical-center space-around flex-wrap">
                    <div class="flex-col fix-col">
                        <div class="article-explain">
                            <div class="article-title">
                                Filter by commission
                            </div>
                            <div class="article-description">
                                Filter for services by the amount that you will make from the sale<br>or<br>
                                for services that offer subscriptions versus one-time feese
                            </div>
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="article-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/article-img.png" />
                        </div>
                    </div>
                </div>
            </article>
            <article>
                <div class="flex-row vertical-center space-around flex-wrap">
                    <div class="flex-col fix-col">
                        <div class="article-image-wrapper">
                            <img src="<?php echo BASE_URL;?>/assets/images/global-icons/article-img.png" />
                        </div>
                    </div>
                    <div class="flex-col fix-col">
                        <div class="article-explain">
                            <div class="article-title">
                                Receive your affiliate link for that service
                            </div>
                            <div class="article-description">
                                click on the affiliate link to setup your account with stripe
                                <br>
                                (you will only have to do this once)
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <div class="get-started-free">
                <a href="login?register" class="get-started-btn">Get Started</a>
            </div>
            <div class="section-description">
                Affiliate marketing is all about spreading the word for great services that you love.<br>
                It's about passion and being rewarded helping other people find their passion.<br>
                We hope you join us and help people find the things that matter most of them.
            </div>
        </section>
    </main>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/site_footer.php';?>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-common.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/home-common.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/affiliate-home-page.js?version=<?php echo time();?>"></script>

</body>
</html>