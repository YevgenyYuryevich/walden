<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 28.07.2018
 * Time: 09:18
 */

?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Title" content="Create your experience" />
    <meta name="Description" content="Explore your ideas and create experiences that are perfect for you" />
    <title>Home</title>

    <link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/shared/primary/app-global.css?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL;?>/assets/css/yevgeny/PreviewSeriesEmbed-global.css?version=<?php echo time();?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL;?>/assets/css/yevgeny/PreviewSeriesEmbed-page.css?version=<?php echo time();?>" />
</head>

<body>
<div class="site-wrapper page page-createYourExperience">
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_header.php';?>
    <div class="site-content">
        <div class="content-wrapper">
            <div class="decoration-container">
                <div class="decoration top-left-rectangular">
                    Embed Preview
                </div>
            </div>
            <main class="main-content">
                <section class="content-item preview-embed">
                    <div class="content-item-inner">
                        <header class="item-header">
                            This is an Embedded Series
                        </header>
                        <div class="item-content">
                        </div>
                    </div>
                </section>
                <section class="content-item code-embed">
                    <div class="content-item-inner">
                        <header class="item-header">
                            This is an Embedded Series
                        </header>
                        <div class="item-content">
                            <?php require_once ASSETS_PATH . '/components/EmbedEditor/EmbedEditor.html';?>
                        </div>
                    </div>
                </section>
                <section class="content-item select-layout-type">
                    <div class="content-item-inner">
                        <header class="item-header">
                            View other styles
                        </header>
                        <div class="flex-row">
                            <div class="flex-col">
                                <div class="layout-item">
                                    <div class="item-icon-wrapper" data-layout="grid">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 341.333 341.333" style="enable-background:new 0 0 341.333 341.333;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <rect x="128" y="128" width="85.333" height="85.333"/>
                                                            <rect x="0" y="0" width="85.333" height="85.333"/>
                                                            <rect x="128" y="256" width="85.333" height="85.333"/>
                                                            <rect x="0" y="128" width="85.333" height="85.333"/>
                                                            <rect x="0" y="256" width="85.333" height="85.333"/>
                                                            <rect x="256" y="0" width="85.333" height="85.333"/>
                                                            <rect x="128" y="0" width="85.333" height="85.333"/>
                                                            <rect x="256" y="128" width="85.333" height="85.333"/>
                                                            <rect x="256" y="256" width="85.333" height="85.333"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                    </div>
                                    <h3>Grid</h3>
                                    <p>Great for displaying<br>
                                        all of your work.
                                    </p>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="layout-item">
                                    <div class="item-icon-wrapper" data-layout="list">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 294.842 294.842" style="enable-background:new 0 0 294.842 294.842;" xml:space="preserve">
                                                        <g>
                                                            <path d="M292.128,214.846c-2.342-2.344-6.143-2.344-8.484,0l-59.512,59.511V6c0-3.313-2.687-6-6-6s-6,2.687-6,6v268.356
                                                                l-59.513-59.512c-2.342-2.342-6.142-2.343-8.485,0.001c-2.343,2.343-2.343,6.142,0.001,8.485l69.755,69.754
                                                                c1.171,1.171,2.707,1.757,4.242,1.757s3.071-0.586,4.242-1.758l69.754-69.754C294.472,220.987,294.472,217.188,292.128,214.846z"/>
                                                            <path d="M6.956,12h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,12,6.956,12z"/>
                                                            <path d="M6.956,82.228h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,82.228,6.956,82.228z"/>
                                                            <path d="M6.956,152.456h180.137c3.313,0,6-2.687,6-6s-2.687-6-6-6H6.956c-3.313,0-6,2.687-6,6S3.643,152.456,6.956,152.456z"/>
                                                            <path d="M124.438,210.685H6.956c-3.313,0-6,2.687-6,6s2.687,6,6,6h117.482c3.313,0,6-2.687,6-6S127.752,210.685,124.438,210.685z"
                                                            />
                                                            <path d="M124.438,280.912H6.956c-3.313,0-6,2.687-6,6s2.687,6,6,6h117.482c3.313,0,6-2.687,6-6S127.752,280.912,124.438,280.912z"
                                                            />
                                                        </g>
                                                    </svg>
                                    </div>
                                    <h3>List</h3>
                                    <p>A great widget<br>
                                        to show off your work.
                                    </p>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="layout-item">
                                    <div class="item-icon-wrapper" data-layout="guided">
                                        <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/turbotax%20style%20icon.png" />
                                    </div>
                                    <h3>Guided</h3>
                                    <p>A great way to<br>
                                        let user's view your work.
                                    </p>
                                </div>
                            </div>
                            <div class="flex-col">
                                <div class="layout-item last-item">
                                    <div class="item-icon-wrapper" data-layout="gallery">
                                        <img src="<?php  echo BASE_URL;?>/assets/images/global-icons/gallery.png" />
                                    </div>
                                    <h3>Preview Gallery</h3>
                                    <p>A gallery to display<br>
                                        preview your work.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <aside class="go-to-preview">
                            <span>Preview</span> <i class="fa fa-arrow-circle-up"></i>
                        </aside>
                    </div>
                </section>
            </main>
        </div>
    </div>
    <?php require_once _ROOTPATH_ . '/yevgeny/views/_templates/_footer.php';?>
</div>

<div class="loading" style="display: none;">Loading&#8230;</div>

<script>
    var series = <?php echo json_encode($series);?>;
</script>

<script src="<?php echo BASE_URL;?>/assets/shared/primary/app-global.js?version=<?php echo APP_GLOBAL_ASSETS_VERSION;?>"></script>
<script src="<?php echo BASE_URL;?>/assets/plugins/ace/build/src/ace.js"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/PreviewSeriesEmbed-global.js?version=<?php echo time();?>"></script>
<script src="<?php echo BASE_URL;?>/assets/js/yevgeny/PreviewSeriesEmbed-page.js?version=<?php echo time();?>"></script>
</body>
</html>