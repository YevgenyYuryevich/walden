<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 15.03.2018
 * Time: 07:56
 */

global $unsplashOptions;

$unsplashOptions = [
    'applicationId'	=> '98ed5ecd389a5b19e3ae817fdcec9b184388937388ba185cd76fadb9d27eca87',
    'utmSource' => 'yevgeny photo web'
];