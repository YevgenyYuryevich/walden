<?php

global $base_url;
$base_url = sprintf(
    "%s://%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME']
);

require_once($_SERVER['DOCUMENT_ROOT'] . '/guardian/options/options.php');

session_save_path($guardian['session']);
session_name('guardian');
session_start();
if (!isset($_SESSION['client_ID'])){
    echo json_encode('no_access');
    die();
}

class databaseHandle{
    public function __construct()
    {
        include ('../config.php');
        include ('../dbconnect.php');
        $this->myconnection = $myconnection;
    }
    public function getCategories(){
        $query="SELECT `category_ID`, `strCategory_name`, `strCategory_description`, strCategory_image FROM `tblcategories` ORDER BY strCategory_name ASC";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getSeriesByCategoryId($client_id, $category_id){
        $query="SELECT series_ID, strSeries_title, strSeries_description, strSeries_image
            FROM tblseries
            WHERE intSeries_category = :category_id
            AND series_ID NOT IN (
            SELECT intTrash_series_ID AS series
            FROM tbltrash
            WHERE intTrash_client_ID = :client_id
            UNION
            SELECT intPurchased_series_ID AS series
            FROM tblpurchased
            WHERE intPurchased_client_ID = :client_id
            AND boolPurchased_active=1)";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->bindValue(':category_id', $category_id);
        $stmt->bindValue(':client_id', $client_id);
        $rlt = $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getSeriesById($id){
        $query="SELECT series_ID, strSeries_title, strSeries_description, strSeries_image
            FROM tblseries
            WHERE series_ID = :id";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->bindValue(':id', $id);
        $rlt = $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows[0];
    }
    public function getSeriesDataById($series_id){
        $query="SELECT post_ID, dtPost_due, strSeriesFrequency_name, strCategory_name, strSeries_description, strSeries_title, strSeries_writernotes, series_ID, strPost_title, strPost_body, strPost_summary, strPost_keywords, intPost_days, strPost_featuredimage   FROM `tblpost` INNER JOIN tblseries ON tblpost.intPost_series_ID=tblseries.series_ID
            INNER JOIN tblseriesfrequency ON tblseries.intSeries_frequency_ID=tblseriesfrequency.seriesfrequency_ID
            INNER JOIN tblcategories ON tblseries.intSeries_category=tblcategories.category_ID
            WHERE intPost_series_ID=:post
            ORDER BY post_ID ASC
            LIMIT 1";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute(array( ':post' => $series_id ));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return isset($rows[0]) ? $rows[0] : false;
    }
    public function joinSeries($series_id, $client_id){
        $query = "INSERT INTO tblpurchased (intPurchased_client_ID, intPurchased_series_ID, dtPurchased_startdate, boolPurchased_active, intPurchased_clientorder)
            VALUES(:client_id, :series_id, NOW(), 1, 1)";
        $this->myconnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $stmt = $this->myconnection->prepare($query);
        if($stmt->execute(array(':client_id' => $client_id, ':series_id' => $series_id))){
            return true;
        }
        else{
            return false;
        }
    }
    public function trashSeries($series_id, $client_id){
        $query = 'INSERT INTO tbltrash (intTrash_client_ID, intTrash_series_ID, dtTrash_datetrashed)
              VALUES (:client_id, :series_id, NOW())';
        $this->myconnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $stmt = $this->myconnection->prepare($query);
        if($stmt->execute(array(':client_id' => $client_id, ':series_id' => $series_id))){
            return true;
        }else{
            return false;
        }
    }
    public function getIdeaBox(){
        $query="SELECT `strIdeaBox_title`, `strIdeaBox_idea`, `strIdeaBox_image`, `ideabox_ID`, `intIdeaBox_subcategory`, dtIdeaBox_deadline, strIdeaBox_reference_link, strIdeaBox_reference_text, boolIdeaBox_done FROM `tblideabox` WHERE intIdeaBox_contractor_ID = :cid";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute([':cid' => $_SESSION['client_ID']]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function updateIdeaboxImage($id, $image_url){
        $query = 'UPDATE `tblideabox` SET `strIdeaBox_image` = :image_url WHERE `ideabox_ID` = :id';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->bindValue(':image_url', $image_url);
        $stmt->bindValue(':id', $id);
        $count = $stmt->execute();
        return $count;
    }
    public function createNewIdeabox($strIdeaBox_image, $strIdeaBox_title, $strIdeaBox_idea, $intIdeaBox_subcategory, $strIdeaBox_reference_link, $strIdeaBox_reference_text){
        $query = "INSERT INTO `tblideabox` (`ideabox_ID`, `intIdeaBox_contractor_ID`, `intIdeaBox_fromclient`, `strIdeaBox_idea`, `intIdeaBox_private`, `intIdeaBox_series_ID`, `boolIdeaBox_used`, `dtIdeaBox_dateadded`, `dtIdeaBox_dateused`, `strIdeaBox_title`, `intIdeaBox_visible`, `intIdeaBox_subcategory`, `intIdeaBox_order`, `dtIdeaBox_deadline`, `boolIdeaBox_done`, `strIdeaBox_image`, strIdeaBox_reference_link, strIdeaBox_reference_text) VALUES (NULL, :client_id, '0', :strIdeaBox_idea, '1', '0', '0', CURRENT_TIMESTAMP, NULL, :strIdeaBox_title, '0', :intIdeaBox_subcategory, 0, NULL, '0', :strIdeaBox_image, :strIdeaBox_reference_link, :strIdeaBox_reference_text)";
        $stmt = $this->myconnection->prepare( $query );
        $stmt->bindValue(':client_id', $_SESSION['client_ID']);
        $stmt->bindValue(':strIdeaBox_image', $strIdeaBox_image);
        $stmt->bindValue(':strIdeaBox_title', $strIdeaBox_title);
        $stmt->bindValue(':strIdeaBox_idea', $strIdeaBox_idea);
        $stmt->bindValue(':intIdeaBox_subcategory', $intIdeaBox_subcategory);
        $stmt->bindValue(':strIdeaBox_reference_link', $strIdeaBox_reference_link);
        $stmt->bindValue(':strIdeaBox_reference_text', $strIdeaBox_reference_text);
        $rlt = $stmt->execute();
        return $rlt ? $this->myconnection->lastInsertId() : 0;
    }
    public function updateIdeaBoxIdea($ideabox_ID, $strIdeaBox_idea, $strIdeaBox_reference_link = false, $strIdeaBox_reference_text = false){
        $query = 'UPDATE `tblideabox` SET `strIdeaBox_idea` = :strIdeaBox_idea';
        if ($strIdeaBox_reference_link !== false){
            $query = $query . ', strIdeaBox_reference_link = :strIdeaBox_reference_link';
        }
        if ($strIdeaBox_reference_text !== false){
            $query = $query . ', strIdeaBox_reference_text = :strIdeaBox_reference_text';
        }
        $query = $query . ' WHERE `ideabox_ID` = :ideabox_ID';

        $stmt = $this->myconnection->prepare( $query );
        $stmt->bindValue(':strIdeaBox_idea', $strIdeaBox_idea);
        $stmt->bindValue(':ideabox_ID', $ideabox_ID);
        if ($strIdeaBox_reference_link !== false){
            $stmt->bindValue(':strIdeaBox_reference_link', $strIdeaBox_reference_link);
        }
        if ($strIdeaBox_reference_text !== false) {
            $stmt->bindValue(':strIdeaBox_reference_text', $strIdeaBox_reference_text);
        }
        $rlt = $stmt->execute();
        return $rlt;
    }
    public function deleteCardAndSubCards($ideabox_ID){
        $query="SELECT `ideabox_ID` FROM `tblideabox` WHERE `tblideabox`.`intIdeaBox_subcategory` = :ideabox_ID";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->bindValue(':ideabox_ID', $ideabox_ID);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $row){
            $rlt_in_subs = $this->deleteCardAndSubCards($row['ideabox_ID']);
            if($rlt_in_subs == false){
                return false;
            }

        }

        $query = 'DELETE FROM `tblideabox` WHERE `tblideabox`.`ideabox_ID` = :ideabox_ID';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->bindValue(':ideabox_ID', $ideabox_ID);
        $rlt = $stmt->execute();
        return $rlt;
    }
}

class categoryHandle {
    public function __construct()
    {
        $this->db = new databaseHandle();
    }
    public function getCategories(){
        $rows = $this->db->getCategories();
        $rlts = array();
        foreach ($rows as $key => $row){
            $rowid=htmlspecialchars_decode($row['category_ID']);
            $rowimage=htmlspecialchars_decode($row['strCategory_image']);
            $rowtitle=htmlspecialchars_decode($row['strCategory_name']);
            $rowbody=htmlspecialchars_decode($row['strCategory_description']);
            $page_number = intval($key / 6) + 1;
            $rlts['page'.$page_number][] ='<a><div class="column" style=""><div class="column_object" title= "'. strip_tags($rowbody) . '"><img src="'. $rowimage .'" alt="" class="object_image"><div class="object_title">'. $rowtitle .'</div><div class="object_description"></div><div data-id = "'. $rowid .'" class="object_button">Explore</div><div class="object_line"></div></div></div></a>';
        }
        echo json_encode($rlts);
        die();
    }
}

class seriesHandle{
    public function __construct()
    {
        $this->db = new databaseHandle();
    }
    public function getSeriesByCategoryId(){
        //  avoid session_start 2 more times

        $client_id = $_SESSION['client_ID'];
        $category_id = $_POST['category_id'];
        $rows = $this->db->getSeriesByCategoryId($client_id, $category_id);
        $rlts = [];
        foreach ($rows as $key => $row){
            $rowid=htmlspecialchars_decode($row['series_ID']);
            $rowimage=htmlspecialchars_decode($row['strSeries_image']);
            $rowtitle=htmlspecialchars_decode($row['strSeries_title']);
            $rowbody=htmlspecialchars_decode($row['strSeries_description']);
            $page_number = intval($key / 6) + 1;
            $rlts['page'.$page_number][] =
                '<a data-category_id = "'. $category_id .'"><div class="column" style=""><div class="column_object" title= "'. strip_tags($rowbody) . '"><img src="'. $rowimage .'" alt="" class="object_image"><div class="object_title">'. $rowtitle .'</div><div class="object_description"></div>
                    <div class="action_button">
                    <div  data-link = "join" href="javascript:;" data-series_id = "'. $rowid .'">
                        <div class="wrap">
                            <button type="submit">Join</button>
                            <img src="../assets/images/check_arrow_2.svg" alt="">
                            <svg width="42px" height="42px">
                                <circle class="circle_2" stroke-position="outside" stroke-width="3" fill="none" cx="22" cy="20" r="18" stroke="#1ECD97"></circle>
                            </svg>
                        </div>
                    </div>
                    </div>
                    <div class="action_button"><span  data-link = "trash" href="javascript:;" data-series_id = "'. $rowid .'"> Trash </span></div>
                    <div class="action_button"><span  data-link = "preview" href="javascript:;" data-series_id = "'. $rowid .'"> Preview </span></div>
                    <div class="object_line"></div></div></div></a>';
        }
        echo json_encode($rlts);
        die();
    }
    public function previewSeries(){
        $series_id = $_POST['series_id'];
        $series_data = $this->db->getSeriesDataById($series_id);
        ?>
        <div class="content_container">
            <div class="header">
                <div class="line_container"> <img src="assets/images/line.png" alt="line"> </div>
                <div class="text_container">
                    <div class="title">
                        <?php
                        echo $series_data['strSeries_title'];
                        ?>
                    </div>
                    <div class="subtitle">
                        <?php
                        echo $series_data['strPost_title'];
                        ?>
                    </div>
                </div>
                <div class="line_container" style="margin-top:20px;"> <img src="assets/images/line.png" alt="line"> </div>
                <div id="header_1" class="animated subtext hided">
                    <article><?php echo htmlspecialchars_decode($series_data['strPost_body']); ?></article>
                </div>
            </div>
            <div class="footer"></div></div><?php
        die();
    }
    public function joinSeries(){
        $series_id = $_POST['series_id'];
        $client_id = $_SESSION['client_ID'];
        if ($client_id === -1){
            echo json_encode('no_access');
        }else{
            $rlt = $this->db->joinSeries($series_id, $client_id);
            echo json_encode($rlt);
        }
        die();
    }
    public function trashSeries(){
        $series_id = $_POST['series_id'];
        $client_id = $_SESSION['client_ID'];

        if ($client_id === -1){
            echo json_encode('no_access');
        }else{
            $rlt = $this->db->trashSeries($series_id, $client_id);
            if ($rlt){
                $this->getSeriesByCategoryId();
            }
            else{
                echo json_encode(false);
            }
        }
        die();
    }
}

class ideaBoxHandle{
    const DEFAULT_IMAGE_URL = '';

    public function __construct()
    {
        $this->db = new databaseHandle();
    }
    public function getIdeaBox(){
        $rows = $this->db->getIdeaBox();

        foreach ($rows as &$row){
            if($row['dtIdeaBox_deadline'] === null){
                $row['dtIdeaBox_deadline'] = false;
            }
            else{
                $row['dtIdeaBox_deadline'] = date("m/d/Y", strtotime($row['dtIdeaBox_deadline']));
            }
            $row['strIdeaBox_reference_link'] = $row['strIdeaBox_reference_link'] == null ? '' : $row['strIdeaBox_reference_link'];
            $row['strIdeaBox_reference_text'] = $row['strIdeaBox_reference_text'] == null ? '' : $row['strIdeaBox_reference_text'];
            $row['next_deadline'] = -1;
        }
        echo json_encode($rows);
        die();
    }
    public function updateIdeaboxImage(){

        $file = $_FILES['strIdeaBox_image'];
        $id = $_POST['ideabox_ID'];
        if ( 0 < $file['error'] ) {
            echo json_encode('Error: ' . $_FILES['file']['error']);
            die();
        }
        else {
            $unique_filename = uniqid('ideabox').'_' . $file['name'];
            $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
            $rlt = move_uploaded_file($file['tmp_name'], $destination);
            if ($rlt === true) {
                $save_url = 'assets/images/' . $unique_filename;
                $rlt = $this->db->updateIdeaboxImage($id, $save_url);
                echo json_encode(array('status' => $rlt, 'url' => $save_url));
            }
            else { echo json_encode(array('status' => false)); }
        }
        die();
    }
    public function copyIdeaBoxDoc(){
        $doc_file = $_FILES['ideabox_doc'];
        $unique_filename = uniqid('ideabox').'_' . $doc_file['name'];
        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/ideabox-doc-files/' . $unique_filename;
        $rlt = move_uploaded_file($doc_file['tmp_name'], $destination);
        if ($rlt === true) {
            global $base_url;
            $save_url = $base_url . '/assets/ideabox-doc-files/' . $unique_filename;
            echo json_encode(array('status' => $rlt, 'url' => $save_url));
        }
        else { echo json_encode(array('status' => false)); }
        die();
    }
    public function createNewIdeabox(){

        $strIdeaBox_image = $_FILES['ideaBox_image'];

        $image_url = '';
        if ($strIdeaBox_image['name'] !== ''){
            $unique_filename = uniqid('ideabox').'_' . $strIdeaBox_image['name'];
            $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
            $rlt = move_uploaded_file($strIdeaBox_image['tmp_name'], $destination);

            if ($rlt === true) {
                $image_url = 'assets/images/' . $unique_filename;
            }
            else { echo json_encode(array('status' => false)); die(); }
        }

        $strIdeaBox_title = $_POST['ideaBox_title'];
        $strIdeaBox_idea = $_POST['ideaBox_idea'];
        $intIdeaBox_subcategory = $_POST['intIdeaBox_subcategory'];
        $strIdeaBox_reference_link = $_POST['ideabox_reference_link'];
        $strIdeaBox_reference_text = $_POST['ideabox_reference_text'];

        $new_id = $this->db->createNewIdeabox($image_url, $strIdeaBox_title, $strIdeaBox_idea, $intIdeaBox_subcategory, $strIdeaBox_reference_link, $strIdeaBox_reference_text);

        if ($new_id > 0){
            $return_val = array();
            $return_val['status'] = true;
            $return_val['data'] = array(
                'ideabox_ID' => $new_id,
                'strIdeaBox_image' => $image_url,
                'strIdeaBox_title' => $strIdeaBox_title,
                'strIdeaBox_idea' => $strIdeaBox_idea,
                'intIdeaBox_subcategory' => $intIdeaBox_subcategory,
                'strIdeaBox_reference_link' => $strIdeaBox_reference_link,
                'strIdeaBox_reference_text' => $strIdeaBox_reference_text,
                'dtIdeaBox_deadline' => false
            );
            echo json_encode($return_val);
        }
        else{
            echo json_encode(array('status'=>false));
        }

        die();
    }
    public function createNewCardsByDoc(){

        $intIdeaBox_subcategory = $_POST['intIdeaBox_subcategory'];

        $doc_file = $_FILES['ideabox_doc'];
        $unique_filename = uniqid('ideabox').'_' . $doc_file['name'];

        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/ideabox-doc-files/' . $unique_filename;
        $rlt = move_uploaded_file($doc_file['tmp_name'], $destination);
        if ($rlt === true) {
            global $base_url;
            $save_url = $base_url . '/assets/ideabox-doc-files/' . $unique_filename;
            $image_url = self::DEFAULT_IMAGE_URL;
            $strIdeaBox_title = preg_replace('/(\.\w*)$/', '', $doc_file['name']);
            $strIdeaBox_idea = '';
            $dtIdeaBox_deadline = false;
            $strIdeaBox_reference_link = $save_url;
            $strIdeaBox_reference_text = 'view doc';
            $new_id = $this->db->createNewIdeabox($image_url, $strIdeaBox_title, $strIdeaBox_idea, $intIdeaBox_subcategory, $strIdeaBox_reference_link, $strIdeaBox_reference_text);

            $return_val = array();
            $return_val['status'] = true;
            $return_val['data'] = array(
                'ideabox_ID' => $new_id,
                'strIdeaBox_image' => $image_url,
                'strIdeaBox_title' => $strIdeaBox_title,
                'strIdeaBox_idea' => $strIdeaBox_idea,
                'intIdeaBox_subcategory' => $intIdeaBox_subcategory,
                'dtIdeaBox_deadline' => $dtIdeaBox_deadline,
                'strIdeaBox_reference_link' => $strIdeaBox_reference_link,
                'strIdeaBox_reference_text' => $strIdeaBox_reference_text
            );

            echo json_encode($return_val);
        }
        else { echo json_encode(array('status' => false)); }
        die();
    }
    public function createNewCardsByDocUrl(){

        $intIdeaBox_subcategory = $_POST['intIdeaBox_subcategory'];

        $save_url = $_POST['ideabox_doc_url'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $save_url
        ));
        $result = curl_exec($curl);
        $is_html_content = preg_match('/<title>(.*)<\/title>/', $result, $matches);

        if ($is_html_content){
            $strIdeaBox_title = $matches[1];
        }
        else{
            $strIdeaBox_title = 'title';
        }

        $is_html_content = preg_match('/<meta.*name\s*=\s*[\'\"]+description[\'\"]+.*>/', $result, $matches);
        if ($is_html_content){
            $meta_des = $matches[0];
            $is_content_in = preg_match('/.*content\s*=\s*[\"\']+([^\"\']*)[\"\']+.*/', $meta_des, $matches);
            if ($is_content_in){
                $des_content = $matches[1];
            }else{
                $des_content = '';
            }
        }else{
            $des_content = '';
        }

        $image_url = self::DEFAULT_IMAGE_URL;
        $strIdeaBox_idea = $des_content;
        $dtIdeaBox_deadline = false;

        $strIdeaBox_reference_link = $save_url;
        $strIdeaBox_reference_text = 'view doc';

        $new_id = $this->db->createNewIdeabox($image_url, $strIdeaBox_title, $strIdeaBox_idea, $intIdeaBox_subcategory, $strIdeaBox_reference_link, $strIdeaBox_reference_text);

        $return_val = array();
        $return_val['status'] = true;
        $return_val['data'] = array(
            'ideabox_ID' => $new_id,
            'strIdeaBox_image' => $image_url,
            'strIdeaBox_title' => $strIdeaBox_title,
            'strIdeaBox_idea' => $strIdeaBox_idea,
            'intIdeaBox_subcategory' => $intIdeaBox_subcategory,
            'dtIdeaBox_deadline' => $dtIdeaBox_deadline,
            'strIdeaBox_reference_link' => $strIdeaBox_reference_link,
            'strIdeaBox_reference_text' => $strIdeaBox_reference_text
        );

        echo json_encode($return_val);
        die();
    }
    public function createNewCardsByImage(){

        $intIdeaBox_subcategory = $_POST['intIdeaBox_subcategory'];

        $image_file = $_FILES['ideaBox_image'];
        $unique_filename = uniqid('ideabox').'_' . $image_file['name'];

        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;

        $rlt = move_uploaded_file($image_file['tmp_name'], $destination);

        if ($rlt === true) {
            global $base_url;
            $save_url = $base_url . '/assets/images/' . $unique_filename;
            $image_url = 'assets/images/' . $unique_filename;

            $strIdeaBox_title = preg_replace('/(\.\w*)$/', '', $image_file['name']);
            $strIdeaBox_idea = '';
            $dtIdeaBox_deadline = false;
            $strIdeaBox_reference_link = $save_url;
            $strIdeaBox_reference_text = 'view doc';

            $new_id = $this->db->createNewIdeabox($image_url, $strIdeaBox_title, $strIdeaBox_idea, $intIdeaBox_subcategory, $strIdeaBox_reference_link, $strIdeaBox_reference_text);
            if ($new_id > 0){

                $return_val = array();
                $return_val['status'] = true;
                $return_val['data'] = array(
                    'ideabox_ID' => $new_id,
                    'strIdeaBox_image' => $image_url,
                    'strIdeaBox_title' => $strIdeaBox_title,
                    'strIdeaBox_idea' => $strIdeaBox_idea,
                    'intIdeaBox_subcategory' => $intIdeaBox_subcategory,
                    'dtIdeaBox_deadline' => $dtIdeaBox_deadline,
                    'strIdeaBox_reference_link' => $strIdeaBox_reference_link,
                    'strIdeaBox_reference_text' => $strIdeaBox_reference_text
                );

                echo json_encode($return_val);
            }
            else{
                echo json_encode(array('status' => false));
            }
        }
        else { echo json_encode(array('status' => false)); }
        die();
    }
    public function updateIdeaBoxIdea(){
        $ideabox_ID = $_POST['ideabox_ID'];
        $doc_file = $_FILES['ideabox_doc'];
        $strIdeaBox_idea = $_POST['ideabox_idea'];
        $unique_filename = uniqid('ideabox').'_' . $doc_file['name'];

        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/ideabox-doc-files/' . $unique_filename;
        $rlt = move_uploaded_file($doc_file['tmp_name'], $destination);
        if ($rlt === true) {

            global $base_url;
            $save_url = $base_url . '/assets/ideabox-doc-files/' . $unique_filename;
            $strIdeaBox_reference_link = $save_url;
            $strIdeaBox_reference_text = isset($_POST['ideabox_doc_text']) ? $_POST['ideabox_doc_text'] : false;

             $db_rlt = $this->db->updateIdeaBoxIdea($ideabox_ID, $strIdeaBox_idea, $strIdeaBox_reference_link, $strIdeaBox_reference_text);
             if ($db_rlt == true){
                 echo json_encode(array(
                     'status' => true,
                     'data' => array(
                         'idea' => $strIdeaBox_idea,
                         'strIdeaBox_reference_link' => $strIdeaBox_reference_link,
                         'strIdeaBox_reference_text' => $strIdeaBox_reference_text)));
             }
             else{
                 echo json_encode(array('status' => false));
             }
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function updateIdeaBoxIdeaUrl(){

        $ideabox_ID = $_POST['ideabox_ID'];
        $strIdeaBox_idea = $_POST['ideabox_idea'];
        $strIdeaBox_reference_link = $_POST['ideabox_doc_url'];
        $strIdeaBox_reference_text = isset($_POST['ideabox_doc_text']) ? $_POST['ideabox_doc_text'] : false;

        $db_rlt = $this->db->updateIdeaBoxIdea($ideabox_ID, $strIdeaBox_idea, $strIdeaBox_reference_link, $strIdeaBox_reference_text);
        if ($db_rlt == true){
            echo json_encode(array(
                'status' => true,
                'data' => array(
                    'idea' => $strIdeaBox_idea,
                    'strIdeaBox_reference_link' => $strIdeaBox_reference_link,
                    'strIdeaBox_reference_text' => $strIdeaBox_reference_text)));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function deleteCardAndSubCards(){
        $ideabox_ID = $_POST['ideabox_ID'];
        $rlt_db = $this->db->deleteCardAndSubCards($ideabox_ID);
        if ($rlt_db == true){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
}

if (!isset($_POST['action'])){
    echo json_encode('no');
    die();
}

$category_handle = new categoryHandle();
$seriesHandle = new seriesHandle();
$ideaBoxHandle = new ideaBoxHandle();



switch ($_POST['action']){
    case 'getCategories':
        $category_handle->getCategories();
        break;
    case 'getSeries':
        $seriesHandle->getSeriesByCategoryId();
        break;
    case 'previewSeries':
        $seriesHandle->previewSeries();
        break;
    case 'joinSeries':
        $seriesHandle->joinSeries();
        break;
    case 'trashSeries':
        $seriesHandle->trashSeries();
        break;
    case 'getIdeaBox':
        $ideaBoxHandle->getIdeaBox();
        break;
    case 'update_ideabox_image':
        $ideaBoxHandle->updateIdeaboxImage();
        break;
    case 'copy_ideabox_doc':
        $ideaBoxHandle->copyIdeaBoxDoc();
        break;
    case 'create_new_ideabox':
        $ideaBoxHandle->createNewIdeabox();
        break;
    case 'create_new_cards_by_doc':
        $ideaBoxHandle->createNewCardsByDoc();
        break;
    case 'create_new_cards_by_doc_url':
        $ideaBoxHandle->createNewCardsByDocUrl();
        break;
    case 'create_new_cards_by_image':
        $ideaBoxHandle->createNewCardsByImage();
        break;
    case 'update_ideaBox_idea':
        $ideaBoxHandle->updateIdeaBoxIdea();
        break;
    case 'update_ideaBox_idea_url':
        $ideaBoxHandle->updateIdeaBoxIdeaUrl();
        break;
    case 'get_site_title':
        $ideaBoxHandle->getSiteTitle();
        break;
    case 'delete_card_and_sub_cards':
        $ideaBoxHandle->deleteCardAndSubCards();
        break;
    default:
        break;
}