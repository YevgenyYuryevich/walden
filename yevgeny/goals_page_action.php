<?php

$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',  $_SERVER['SERVER_NAME'] );
require_once("../guardian/access.php");

global $current_user;

$current_user = $_SESSION['client_ID'];

if (!isset($_POST['action'])){
    echo json_encode('no');
    die();
}


class databaseClass{

    const STEPS_FREQUENCY = array(0, 1, 7);

    public function __construct()
    {
        include ('../config.php');
        include ('../dbconnect.php');
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        global $current_user;
        $this->current_user = $current_user;
    }
    public function getEvents(){

        $query = "SELECT * FROM `events` WHERE created_by = :client_id";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':client_id' => $this->current_user) );
        $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $events;
    }
    public function getEventImagesByEventId($event_id){
        $query = 'SELECT * FROM event_images WHERE event_id = :event_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getFavoritesByEventId($event_id){
        $query = 'SELECT * FROM `event_favorites` WHERE event_id = :event_id AND client_id = :client_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->current_user) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getRescheduledEvents($event_id){
        $query = 'SELECT * FROM event_instance_exception WHERE event_id = :event_id AND is_cancelled = 0';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getCancelledEvents($event_id){
        $query = 'SELECT * FROM event_instance_exception WHERE event_id = :event_id AND is_cancelled = 1';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getRecurringPattern($event_id){
        $query = 'SELECT * FROM `recurring_pattern` WHERE event_id = :event_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        $recurring_pattern = $stmt->fetch(PDO::FETCH_ASSOC);
        return $recurring_pattern;
    }

    public function getIdeaBox(){
        $query="SELECT `strIdeaBox_title`, `strIdeaBox_idea`, `strIdeaBox_image`, `ideabox_ID`, `intIdeaBox_subcategory`, dtIdeaBox_deadline, strIdeaBox_reference_link, strIdeaBox_reference_text, boolIdeaBox_done FROM `tblideabox` WHERE dtIdeaBox_deadline IS NOT NULL";
        $stmt = $this->db->prepare ( $query );
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getGoals(){
        $query = 'SELECT * FROM tblGoals WHERE intGoal_client_ID = :client_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':client_id' => $this->current_user
        ]);
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
}
class goalsClass{
    const NUMS_DISPLAY = 5;
    const STEPS_FREQUENCY = array(0, 1, 7);
    const DEFAULT_IDEABOX_IMAGE = 'assets/images/beautifulideas.jpg';

    public function __construct()
    {
        $this->db = new databaseClass();
    }
    public function getGoalsData(){
        $events = $this->db->getEvents();
        foreach ($events as &$event) {
            $event['event_images'] = $this->db->getEventImagesByEventId($event['id']);
            $event['event_favorites'] = $this->db->getFavoritesByEventId($event['id']);
            $event['rescheduled_events'] = $this->db->getRescheduledEvents($event['id']);
            $event['cancelled_events'] = $this->db->getCancelledEvents($event['id']);
            if ($event['is_recurring']){
                $event['recurring_pattern'] = $this->db->getRecurringPattern($event['id']);
            }
        }

        $ideabox_rows = $this->db->getIdeaBox();
        foreach ($ideabox_rows as &$row){
            if($row['dtIdeaBox_deadline'] === null){
                $row['dtIdeaBox_deadline'] = date("Y-m-d");
            }
            else{
                $row['dtIdeaBox_deadline'] = date("Y-m-d", strtotime($row['dtIdeaBox_deadline']));
            }
            $row['strIdeaBox_reference_link'] = $row['strIdeaBox_reference_link'] == null ? '' : $row['strIdeaBox_reference_link'];
            $row['strIdeaBox_reference_text'] = $row['strIdeaBox_reference_text'] == null ? '' : $row['strIdeaBox_reference_text'];
        }

        $goals = $this->db->getGoals();
        $rlt = [
            'events' => $events,
            'ideabox' => $ideabox_rows,
            'goals' => $goals
        ];
        echo json_encode(['status' => true, 'data' => $rlt]);
        die();
    }
}

$goalsHandle = new goalsClass();

switch ($_POST['action']){
    case 'get_goals_data':
        $goalsHandle->getGoalsData();
        break;
    default:
        break;
}