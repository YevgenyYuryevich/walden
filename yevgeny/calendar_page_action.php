<?php

$base_url = sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',  $_SERVER['SERVER_NAME'] );
require_once("../guardian/access.php");

global $current_user;

$current_user = $_SESSION['client_ID'];

if (!isset($_POST['action'])){
    echo json_encode('no');
    die();
}


class databaseClass{

    const STEPS_FREQUENCY = array(0, 1, 7);

    public function __construct()
    {
        include ('../config.php');
        include ('../dbconnect.php');
        $this->db = $myconnection;
    }

    public function getPurchasedSeries(){

        global $current_user;
        $query = 'SELECT * FROM tblpurchased INNER JOIN tblseries ON tblseries.series_ID = tblpurchased.intPurchased_series_ID WHERE intPurchased_client_ID = :uid';
        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':uid' => $current_user));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getMaxSubscriptionByPurchaedId($purchased_id){
        $query = 'SELECT * FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id ORDER BY intClientSubscriptions_order DESC LIMIT 1';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':purchased_id' => $purchased_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function getFirstSubscriptionByPurchasedId($purchased_id){
        $query = 'SELECT * FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id AND intClientSubscriptions_finished = 0 ORDER BY intClientSubscriptions_order LIMIT 1';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':purchased_id' => $purchased_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function getSubscriptionsFrom($purchased_id, $from_order){
        $query = 'SELECT post_ID, strPost_title, strPost_featuredimage, intPost_type, clientsubscription_ID, strClientSubscription_title, dtClientSubscription_datetoshow, strClientSubscription_image, intClientSubscriptions_type, intClientSubscriptions_order FROM tblclientsubscription 
					INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
					INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
					WHERE intClientSubscription_purchased_ID = :purchased_id
					AND intClientSubscriptions_order >= :from_order
					AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :pid)
					ORDER BY intClientSubscriptions_order';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':purchased_id' => $purchased_id, ':from_order' => $from_order, ':pid' => $purchased_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getSubscriptionsByPurchaedId($purchased_id, $start_date_can_display = 0){
        $query = 'SELECT post_ID, strPost_title, strPost_featuredimage, intPost_type, clientsubscription_ID, strClientSubscription_title, dtClientSubscription_datetoshow, strClientSubscription_image, intClientSubscriptions_type, intClientSubscriptions_order FROM tblclientsubscription 
					INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
					INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
					WHERE intClientSubscription_purchased_ID = :purchased_id
					AND intClientSubscriptions_finished = 0
					AND (dtClientSubscription_datetoshow IS NULL OR dtClientSubscription_datetoshow >= :dateoshow)
					AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :pid)
					ORDER BY intClientSubscriptions_order';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':purchased_id' => $purchased_id, ':dateoshow' => $start_date_can_display, ':pid' => $purchased_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getUnsubscriptionPostsByPurchasedId($purchased_id, $series_id){
        $query = 'SELECT post_ID, strPost_title, strPost_featuredimage, intPost_type FROM tblpost WHERE intPost_series_ID = :sid AND post_ID NOT IN (SELECT intClientSubscription_post_ID FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :pid1) AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :pid2) ORDER BY post_ID';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':sid' => $series_id, ':pid1' => $purchased_id, ':pid2' => $purchased_id));
        $posts =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($posts as &$post){
            $post['strPost_title'] = utf8_encode($post['strPost_title']);
        }
        return $posts;
    }
    public function setNextSubscriptionsFinish($subscription_order, $purchased_id, $finished = 0){
        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_finished = :f WHERE intClientSubscription_purchased_ID = :pid AND intClientSubscriptions_order > :c_order';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute(array(':f' => $finished, ':pid' => $purchased_id, ':c_order' => $subscription_order));
    }
    public function getMaxOrderByPurchasedId($purchased_id){
        $query = 'SELECT MAX(intClientSubscriptions_order) AS max_order FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':purchased_id' => $purchased_id) );
        $max_order = $stmt->fetch(PDO::FETCH_ASSOC);
        $max_order = ($max_order && $max_order['max_order'] ? $max_order['max_order'] : 0);
        return $max_order;
    }
    public function insertSubscription($purchased_id, $post_id, $date_step){

        $max_order = $this->getMaxOrderByPurchasedId($purchased_id);

        $last_subscription = $this->getMaxSubscriptionByPurchaedId($purchased_id);
        $last_date = $last_subscription ? $last_subscription['dtClientSubscription_datetoshow'] : 0;
        if ($date_step){
            $date_oshow = ($last_date != null && $last_date) ? date('Y-m-d', strtotime($last_date . ' +' . $date_step . ' day')) : date('Y-m-d');
            $query = 'INSERT INTO `tblclientsubscription` (`intClientSubscription_purchased_ID`, `intClientSubscription_post_ID`, `strClientSubscription_body`, `strClientSubscription_notes`, `intClientSubscriptions_finished`, `strClientSubscription_title`, `dtClientSubscription_datetoshow`, `strClientSubscription_image`, `intClientSubscriptions_type`, intClientSubscriptions_order) VALUES (:purchased_id, :post_id, NULL, NULL, 0, NULL, :dateoshow, NULL, NULL, :plus_order)';
            $stmt = $this->db->prepare( $query );
            $rlt = $stmt->execute(array(':purchased_id' => $purchased_id, ':post_id' => $post_id, ':dateoshow' => $date_oshow, ':plus_order' => ++$max_order));
            return $rlt ? $this->db->lastInsertId() : 0;
        }
        else{
            $query = 'INSERT INTO `tblclientsubscription` (`intClientSubscription_purchased_ID`, `intClientSubscription_post_ID`, `strClientSubscription_body`, `strClientSubscription_notes`, `intClientSubscriptions_finished`, `strClientSubscription_title`, `dtClientSubscription_datetoshow`, `strClientSubscription_image`, `intClientSubscriptions_type`, intClientSubscriptions_order) VALUES (:purchased_id, :post_id, NULL, NULL, 0, NULL, NULL, NULL, NULL, :plus_order)';
            $stmt = $this->db->prepare( $query );
            $stmt->execute(array(':purchased_id' => $purchased_id, ':post_id' => $post_id, ':plus_order' => ++$max_order));
            return $this->db->lastInsertId();
        }
    }
    public function getIdeaBox(){
        $query="SELECT strIdeaBox_title, strIdeaBox_image, ideabox_ID, intIdeaBox_subcategory, strIdeaBox_idea FROM tblideabox";
        $stmt = $this->db->prepare( $query );
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getFavorites($series_id){
        $query = 'SELECT strPost_title, strPost_featuredimage, post_ID FROM tblpost
                    INNER JOIN tblfavorites ON intFavorite_post_ID = tblpost.post_ID 
                    WHERE intPost_series_ID = :sid
                    ORDER BY intFavorites_used DESC, dtFavorite_added DESC';
        $stmt = $this->db->prepare ( $query );
        $stmt->execute(array(':sid' => $series_id));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getNewPosts($series_id, $post_id, $purchased_id){
        $query = 'SELECT strPost_title, strPost_featuredimage, post_ID FROM tblpost WHERE intPost_series_ID = :sid AND post_ID > :pid AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :purchased_id)';
        $stmt = $this->db->prepare ( $query );
        $stmt->execute(array(':sid' => $series_id, ':pid' => $post_id, 'purchased_id' => $purchased_id));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function cleanSubscription($subscription_id){
        $query = 'UPDATE `tblclientsubscription` SET `strClientSubscription_body` = NULL, `strClientSubscription_title` = NULL, `strClientSubscription_image` = NULL, intClientSubscriptions_type = NULL WHERE `tblclientsubscription`.`clientsubscription_ID` = :cid';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute(array(':cid' => $subscription_id));
    }
    public function setPostAsSubscription($subscription_id, $post_id, $purchased_id){

        $query = 'UPDATE tblclientsubscription SET intClientSubscription_post_ID = :pid WHERE clientsubscription_ID = :cid';
        $stmt = $this->db->prepare ( $query );
        $rlt = $stmt->execute(array(':pid' => $post_id, ':cid' => $subscription_id));
        if ($rlt){
            $query = 'SELECT * FROM tblclientsubscription
                    INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
					INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
                    WHERE intClientSubscriptions_finished = 0
                    AND clientsubscription_ID = :cid
                    ORDER BY clientsubscription_ID LIMIT 1';
            $stmt = $this->db->prepare( $query );
            $stmt->execute(array(':cid' => $subscription_id));
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        else{
            return false;
        }
    }
    public function getIdeaboxRowById($id){
        $query = 'SELECT * FROM tblideabox WHERE ideabox_ID = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':id' => $id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function setSubscriptionContent($subscription_id, $strIdeaBox_image, $strIdeaBox_title, $strIdeaBox_idea, $type){
        $query = 'UPDATE tblclientsubscription SET strClientSubscription_title = :title, strClientSubscription_body = :body, strClientSubscription_image = :image, intClientSubscriptions_type = :type WHERE clientsubscription_ID = :cid';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute(array(':title' => $strIdeaBox_title, ':body' => $strIdeaBox_idea, ':image' => $strIdeaBox_image, ':type' => $type, ':cid' => $subscription_id));
    }
    public function getSubscriptionRowById($id){
        $query = 'SELECT * FROM tblclientsubscription WHERE clientsubscription_ID = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':id' => $id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function updateOrderBySubscriptionId($subscription_id, $new_order){
        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_order = :new_order WHERE clientsubscription_ID = :subscription_id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':new_order' => $new_order, ':subscription_id' => $subscription_id) );
    }
    public function trashPost($purchased_id, $post_id){
        $query = 'DELETE FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id AND intClientSubscription_post_ID = :post_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':purchased_id' => $purchased_id, ':post_id' => $post_id) );

        $query = 'INSERT INTO tblposttrash (posttrash_ID, intPosttrash_purchased_ID, intPosttrash_post_ID, dtPosttrash_datetrashed) VALUES (NULL, :purchased_id, :post_id, CURRENT_TIMESTAMP)';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':purchased_id' => $purchased_id, ':post_id' => $post_id) );
    }
    public function getPostById($id){
        $query = 'SELECT * FROM tblpost WHERE post_ID = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':id' => $id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}
class calendarClass{
    const NUMS_DISPLAY = 5;
    const STEPS_FREQUENCY = array(0, 1, 7);
    const DEFAULT_IDEABOX_IMAGE = 'assets/images/beautifulideas.jpg';

    public function __construct()
    {
        $this->db = new databaseClass();
    }
    public function getPostsOfAllSeries(){

        $purchased_series = $this->db->getPurchasedSeries();

        foreach ($purchased_series as &$purchased_sery){
            $start_date_can_display = $this->getStartDateCanDisplay($purchased_sery['intSeries_frequency_ID']);

            $last_subscription = $this->db->getMaxSubscriptionByPurchaedId($purchased_sery['purchased_ID']);
            $max_subscription_order = $last_subscription ? $last_subscription['intClientSubscriptions_order'] : 0;

            $this->db->setNextSubscriptionsFinish($max_subscription_order, $purchased_sery['purchased_ID'], 0);

            $unsubscription_posts = $this->db->getUnsubscriptionPostsByPurchasedId($purchased_sery['purchased_ID'], $purchased_sery['series_ID']);
            foreach ($unsubscription_posts as $unsubscription_post){
                $frequency = $purchased_sery['intSeries_frequency_ID'];
                $this->db->insertSubscription($purchased_sery['purchased_ID'], $unsubscription_post['post_ID'], self::STEPS_FREQUENCY[$frequency]);
            }
            $firstSubscription = $this->db->getFirstSubscriptionByPurchasedId($purchased_sery['purchased_ID']);
            $subscriptions = $this->db->getSubscriptionsFrom($purchased_sery['purchased_ID'], $firstSubscription['intClientSubscriptions_order']);
            if (count($subscriptions) == 0){
                $subscriptions = $this->db->getSubscriptionsFrom($purchased_sery['purchased_ID'], 0);
            }
            $purchased_sery['lightbox_data'] = $this->getSwitchLightboxData($purchased_sery['series_ID'], $purchased_sery['purchased_ID']);

            foreach ($subscriptions as &$subscription){
                $subscription['strPost_title'] = utf8_encode($subscription['strPost_title']);
                $subscription['strClientSubscription_title'] = utf8_encode($subscription['strClientSubscription_title']);
            }

            $purchased_sery['subscriptions'] = $subscriptions;
        }
        $res = array('status' => true, 'data' => $purchased_series);
        echo json_encode($res);
        die();
    }
    public function getSwitchLightboxData($series_id, $purchased_id){

        $ideaboxes = $this->db->getIdeaBox();
        $favorites = $this->db->getFavorites($series_id);
        $new_posts = $this->db->getNewPosts($series_id, 0, $purchased_id);

        foreach ($new_posts as &$new_post){
            $new_post['strPost_title'] = utf8_encode($new_post['strPost_title']);
        }

        $return_val = array(
            'ideaboxes' => $ideaboxes,
            'favorites' => $favorites,
            'new_posts' => $new_posts
        );
        return $return_val;
    }
    public function setPostAsSubscription(){
        $subscription_id = $_POST['subscription_ID'];
        $post_id = $_POST['post_ID'];
        $purchased_id = $_POST['purchased_ID'];

        $this->db->cleanSubscription($subscription_id);
        $subscription = $this->db->setPostAsSubscription($subscription_id, $post_id, $purchased_id);
        if ($subscription){
            $subscription['strClientSubscription_body'] = utf8_encode($subscription['strClientSubscription_body']);
            $subscription['strPost_body'] = utf8_encode($subscription['strPost_body']);
            echo json_encode(array('status' => true, 'data' => $subscription));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function setIdeaboxAsSubscription(){
        $subscription_id = $_POST['subscription_ID'];
        $ideabox_id = $_POST['ideabox_ID'];
        $type = $_POST['type'];
        $ideabox_row = $this->db->getIdeaboxRowById($ideabox_id);
        $rlt = $this->db->setSubscriptionContent($subscription_id, ($ideabox_row['strIdeaBox_image'] ? $ideabox_row['strIdeaBox_image'] : self::DEFAULT_IDEABOX_IMAGE), $ideabox_row['strIdeaBox_title'], $ideabox_row['strIdeaBox_idea'], $type);
        if ($rlt){
            $subscription_row = $this->db->getSubscriptionRowById($subscription_id);
            $res = array(
                'status' => true,
                'data' => array(
                    'strClientSubscription_title' => $subscription_row['strClientSubscription_title'],
                    'strClientSubscription_image' => $subscription_row['strClientSubscription_image']
                )
            );
            echo json_encode($res);
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function updateOrderOfSubscriptions(){
        $rows = $_POST['data'];
        foreach ($rows as $row){
            $this->db->updateOrderBySubscriptionId($row['subscription_id'], $row['order']);
        }
        echo json_encode(array('status' => true));
        die();
    }
    public function trashPost(){
        $post_id = $_POST['post_id'];
        $purchased_id = $_POST['purchased_id'];
        $rlt = $this->db->trashPost($purchased_id, $post_id);

        if ($rlt){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
    }
    public function getStartDateCanDisplay($frequency_id){
        switch ($frequency_id){
            case 0:
                $start_date_can_display = '0';
                break;
            case 1:
                $start_date_can_display = date('Y-m-d');
                break;
            case 2:
                $start_date_can_display = date('Y-m-d', strtotime( date('Y-m-d') . ' -6 day' ));
                break;
            default:
                $start_date_can_display = '0';
                break;
        }
        return $start_date_can_display;
    }
    public function getDescription(){
        $post_id = $_POST['post_id'];
        $subscription_id = $_POST['subscription_id'];
        $post = $this->db->getPostById($post_id);
        $subscription = $this->db->getSubscriptionRowById($subscription_id);
        $description = $subscription['strClientSubscription_body'] != null ? $subscription['strClientSubscription_body'] : $post['strPost_body'];
        $description = utf8_encode($description);
        echo json_encode(['status' => true, 'data' => $description]);
        die();
    }
}

$calendarHandle = new calendarClass();

switch ($_POST['action']){
    case 'get_posts_of_all_series':
        $calendarHandle->getPostsOfAllSeries();
        break;
    case 'set_post_as_subscription':
        $calendarHandle->setPostAsSubscription();
        break;
    case 'set_ideabox_as_subscription':
        $calendarHandle->setIdeaboxAsSubscription();
        break;
    case 'update_order_of_subscriptions':
        $calendarHandle->updateOrderOfSubscriptions();
        break;
    case 'trash_post':
        $calendarHandle->trashPost();
        break;
    case 'get_description':
        $calendarHandle->getDescription();
        break;
    default:
        break;
}