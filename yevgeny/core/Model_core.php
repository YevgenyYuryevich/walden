<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.05.2018
 * Time: 05:57
 */

namespace Core;

require_once __DIR__ . '/Database_core.php';

class Model_Core
{
    public $TNAME;
    public $PRIMARY_KEY;

    public function __construct($database = 'greyshirt')
    {
        $this->db = new Database_Core($database);
    }
    public function getRows($where = [], $selects = '*', $orderBy = [], $limit = false, $offset = 0){
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        foreach ($where as $key => $value) {
            if (is_array($value)) {
                $this->db->groupStart();
                foreach ($value as $v) {
                    $this->db->orWhere($key, $v);
                }
                $this->db->groupEnd();
            }
            else {
                $this->db->where($key, $value);
            }
        }
        $this->db->orderBy($orderBy);
        if ($limit){
            $this->db->limit($limit, $offset);
        }
        return $this->db->get();
    }
    public function get($where = [], $selects = '*', $orderBy = [], $limit = false){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->select($selects);
        $this->db->from($this->TNAME);
        foreach ($where as $key => $value) {
            if (is_array($value)) {
                $this->db->groupStart();
                foreach ($value as $v) {
                    $this->db->orWhere($key, $v);
                }
                $this->db->groupEnd();
            }
            else {
                $this->db->where($key, $value);
            }
        }
        $this->db->orderBy($orderBy);
        if ($limit){
            $this->db->limit($limit);
        }
        $row = $this->db->get(true);
        return !is_null($row) ? $row : false;
    }
    public function delete($where){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->where($where);
        return $this->db->delete();
    }
    public function insert($sets){
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        return $this->db->insert();
    }
    public function update($where = [], $sets = []){
        if (!is_array($where)){
            $where = [$this->PRIMARY_KEY => $where];
        }
        $this->db->from($this->TNAME);
        $this->db->set($sets);
        $this->db->where($where);
        return $this->db->update();
    }
    public function sum($select, $where = []) {
        $select = $select ? $select : $this->PRIMARY_KEY;
        $select = 'SUM(' . $select . ') AS sumV';
        $this->db->select($select);
        $this->db->where($where);
        $this->db->from($this->TNAME);
        $row = $this->db->get(true);
        return $row ? (int) $row['sumV'] : 0;
    }
    public function max($colName, $where = []) {
        $select = 'MAX('. $colName .') AS MaxV';
        $this->db->select($select);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $row = $this->db->get(true);
        return $row && $row['MaxV'] ? $row['MaxV'] : 0;
    }
    public function count($where = [], $select = false) {
        $select = $select ? $select : $this->PRIMARY_KEY;
        $select = 'COUNT(' . $select . ') AS cnt';
        $this->db->select($select);
        $this->db->where($where);
        $this->db->from($this->TNAME);
        $row = $this->db->get(true);
        return $row ? (int) $row['cnt'] : 0;
    }
    public function avg($where = [], $select) {
        $select = 'AVG('. $select .') AS avg';
        $this->db->select($select);
        $this->db->from($this->TNAME);
        $this->db->where($where);
        $row = $this->db->get(true);
        return $row ? $row['avg'] : false;
    }
    public function value($where = [], $column) {
        $row = $this->get($where);
        return $row ? $row[$column] : 'undefined';
    }
}