<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.02.2018
 * Time: 12:06
 */

namespace Core;

class Load_core
{

    public function __construct($parent = null)
    {
        $this->RootPath = realpath($_SERVER['DOCUMENT_ROOT']);
        $this->LoadPath = $this->RootPath . '/yevgeny';
        $this->ModelPath = $this->LoadPath . '/models';
        $this->ControllerPath = $this->LoadPath . '/controllers';
        $this->LibPath = $this->LoadPath . '/library';
        $this->ViewPath = $this->LoadPath . '/views';
        $this->TemplatesPath = $this->ViewPath . '/_templates';
        $this->OptionsPath = $this->LoadPath . '/options';
        $this->HelperPath = $this->LoadPath . '/helpers';
        $this->parent = $parent;
    }
    public function view($viewName, $params = []){
        $viewNamePath = $this->ViewPath . '/' . $viewName . '.php';

        foreach ($params as $key => $param){
            ${$key} = $param;
        }
        require_once $viewNamePath;
    }
    public function template($templateName, $params = []){
        $viewNamePath = $this->TemplatesPath . '/' . $templateName;

        foreach ($params as $key => $param){
            ${$key} = $param;
        }
        require_once $viewNamePath;
    }
    public function model($modelName, $asName = 'model'){
        $modelNamePath = $this->ModelPath . '/' . $modelName . '.php';
        require_once $modelNamePath;
        $className = 'Models\\' . $modelName;
        return $className;
//        $this->parent->$asName = new $className();
//        return $this->parent->$asName;
    }
    public function controller($controllerName, $asName = 'controller'){
        $controllerNamePath = $this->ControllerPath . '/' . $controllerName . '.php';
        require_once $controllerNamePath;
        $className = 'Controllers\\' . $controllerName;
        return $className;
//        $this->parent->$asName = new $className();
//        return $this->parent->$asName;
    }
    public function library($libName){
        $libNamePath = $this->LibPath . '/' . $libName . '.php';
        require_once $libNamePath;
    }
}