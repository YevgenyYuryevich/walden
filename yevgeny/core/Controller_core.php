<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.02.2018
 * Time: 12:00
 */

namespace Core;

require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/helpers/funcs.php';
require_once _ROOTPATH_ . '/yevgeny/options/devOptions.php';
use const _ROOTPATH_;

require_once _ROOTPATH_ . '/yevgeny/core/Load_core.php';

class Controller_core
{
    public $load;
    public function __construct()
    {
        $this->load = new Load_core($this);
    }
}