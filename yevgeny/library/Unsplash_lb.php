<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 17.04.2018
 * Time: 22:32
 */

namespace Library;

use Crew\Unsplash\HttpClient;

require_once __DIR__ . '/../helpers/funcs.php';
require_once __DIR__ . '/../options/Unsplash.php';

class Unsplash_lb
{
    public function __construct()
    {
        global $unsplashOptions;
        HttpClient::init($unsplashOptions);
        $this->unsplash = new \Crew\Unsplash\Photo();
        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';
    }
    public function random($query){
        $filter = ['query' => $query, 'w' => 300];
        $unsplashPhoto = $this->unsplash::random($filter);

        if ($unsplashPhoto->errors){
            return $this->defaultPhoto;
        }
        return $unsplashPhoto->urls['small'];
    }
    public function getExtensionFromUrl($url){
        preg_match('/fm=([^\&]*)/', $url, $matches);
        return $matches[1];
    }
}