<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 04.07.2018
 * Time: 08:09
 */

namespace Library;

require_once __DIR__ . '/../vendor/autoload.php';

class PhpWord_lb
{
    private $source;
    public function __construct($source)
    {
        $this->source = $source;
    }
    public function convertToHtml() {
        $fileArray = pathinfo($this->source);
        $file_ext  = $fileArray['extension'];
        switch ($file_ext) {
            case 'doc':
                return $this->docToHtml();
                break;
            case 'docx':
                return $this->docxToHtml();
                break;
            default:
                return false;
                break;
        }
    }
    public function docxToHtml() {
        $phpword = \PhpOffice\PhpWord\IOFactory::load($this->source);
        $sections = $phpword->getSections();
        $rtnHtml = '';
        foreach ($sections as $s) {
            $rtnHtml .= $this->sectionToHtml($s);
        }
        return $rtnHtml;
    }
    public function docToHtml() {
        $phpword = \PhpOffice\PhpWord\IOFactory::load($this->source, 'MsDoc');
        $sections = $phpword->getSections();
        $rtnHtml = '';
        foreach ($sections as $s) {
            $rtnHtml .= $this->sectionToHtml($s);
        }
        return $rtnHtml;
    }
    private function sectionToHtml($section) {
        $rtnHtml = '';
        $els = $section->getElements();
        foreach ($els as $e) {
            $eHtml = $this->elementToHtml($e);
            $rtnHtml .= $eHtml;
        }
        return $rtnHtml;
    }
    private function elementToHtml($e) {
        $rtnHtml = '';
        switch (get_class($e)){
            case 'PhpOffice\PhpWord\Element\TextRun':
                foreach($e->getElements() as $text) {
                    $rtnHtml .= $this->wTextToHtml($text);
                }
                break;
            case 'PhpOffice\PhpWord\Element\Text':
                $rtnHtml .= $this->wTextToHtml($e);
                break;
            case 'PhpOffice\PhpWord\Section\TextBreak':
            case 'PhpOffice\PhpWord\Element\TextBreak':
                $rtnHtml .= '<br>';
                break;
            default:
                break;
        }
        return '<div>' . $rtnHtml . '</div>';
    }
    private function wTextToHtml($text) {
        $font = $text->getFontStyle();
        $fontCss = $this->fontStyleCss($font);
        return '<span style="'. $fontCss .'">'. $text->getText() .'</span>';
    }
    private function fontStyleCss($font) {
        $fontSize = $font->getSize();
        $family = $font->getName();
        $lineHeight = $font->getLineHeight();
        $color = $font->getColor();

        $fontCss = '';

        if ($fontSize) {
            $fontCss .= 'font-size: ' . $fontSize / 10 . 'em;';
        }
        if ($color) {
            $fontCss .= 'color: #' . $color . ';';
        }
        if ($family) {
            $fontCss .= 'font-family: ' . $family . ';';
        }
        if ($lineHeight) {
            $fontCss .= 'line-height: ' . $lineHeight . ';';
        }
        return $fontCss;
    }
}