<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.09.2018
 * Time: 10:23
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';
require_once './../options/cloud_convert.php';

class FileToHtml extends \Core\Controller_core
{
    private $cloudApi;
    public function __construct()
    {
        global $cloudConvertApiKey;
        $this->cloudApi = new \CloudConvert\Api($cloudConvertApiKey);
    }
    public function index() {
        $file = $_FILES['file'];
        $sets = isset($_POST['sets']) ? \Helpers\parseStringToArray($_POST['sets']) : [];
        $tempSets = ['where' => 'assets/media/temp', 'prefix' => 'temp'];
        $savedUrl = \Helpers\uploadFile($file, $tempSets);
        $path = $savedUrl['path'];
        $fileInfo = pathinfo($path);
        $name = preg_replace('/[\?\/\#\s\.]/', '-', $file['name']);
        $outputName = uniqid(isset($sets['prefix']) ? $sets['prefix'] : 'cvt') . '_' . $name . '.html';
        $outputPath = _ROOTPATH_ . '/assets/media/document/' . $outputName;
        $outputUrl = BASE_URL . '/assets/media/document/' . $outputName;
        $converterOptions = [];
        switch ($fileInfo['extension']) {
            case 'pdf':
                break;
            case 'doc':
                $converterOptions['simple_html'] = true;
                break;
            default:
                $converterOptions['simple_html'] = true;
                break;
        }
        $this->cloudApi->convert([
            'inputformat' => $fileInfo['extension'],
            'outputformat' => 'html',
            'input' => 'upload',
            'file' => fopen($path, 'r'),
            'converteroptions' => $converterOptions,
        ])
            ->wait()
            ->download($outputPath);
        $xml = new DOMDocument();
        libxml_use_internal_errors(true);
        $xml->loadHTMLFile($outputUrl);
        $body = $xml->getElementsByTagName('body');
        $bodyHtml = $this->DomInnerHTML($body[0]);

        $rlt = [
            'status' => true,
            'data' => $bodyHtml,
            'url' => $outputUrl
        ];
        echo json_encode($rlt);
        die();
    }
    public function domInnerHTML($element)
    {
        $innerHTML = "";
        $children  = $element->childNodes;

        foreach ($children as $child)
        {
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        }
        return $innerHTML;
    }
}

$handle = new FileToHtml();
$handle->index();