<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.06.2018
 * Time: 09:00
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';

class ClientSeriesView extends \Core\Controller_core
{
    public $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/ClientSeriesView_m');
        $this->model = new \Models\api\ClientSeriesView_m();
    }
    public function index() {

    }
    public function ajax_insert(){
        $sets = $_POST['sets'];
        $id = $this->model->insert($sets);
        $row = $this->model->get($id);
        echo json_encode(['status' => true, 'data' => $row]);
        die();
    }
    public function ajax_get(){
        $where = $_POST['where'];
        $row = $this->model->get($where);
        echo json_encode(['status' => true, 'data' => $row]);
        die();
    }
    public function ajax_update() {
        $where = $_POST['where'];
        $sets = $_POST['sets'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_delete() {

    }
}

$handle = new ClientSeriesView();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $handle->ajax_get();
            break;
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}