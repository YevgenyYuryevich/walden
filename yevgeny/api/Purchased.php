<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.07.2018
 * Time: 11:24
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';

class Purchased extends \Core\Controller_core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Purchased_m');
        $this->model = new \Models\api\Purchased_m();
    }
    public function index() {

    }
    public function ajax_get() {
        $where = $_POST['where'];
        $item = $this->model->get($where);
        echo json_encode(['status' => true, 'data' => $item]);
        die();
    }
    public function ajax_insert() {

    }
    public function ajax_update() {

    }
}

$hdl = new Purchased();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $hdl->ajax_get();
            break;
        case 'insert':
            $hdl->ajax_insert();
            break;
        case 'update':
            $hdl->ajax_update();
            break;
        default:
            $hdl->index();
            break;
    }
}
else{
    $hdl->index();
}