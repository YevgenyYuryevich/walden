<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 11.07.2018
 * Time: 14:34
 */

require_once '../../guardian/class/userauth.class.php';
require_once __DIR__ . '/../core/Controller_core.php';

use function Helpers\{
    strongEncrypt,
};

class User extends \Core\Controller_core
{
    public $model;
    public $isLoggedIn;
    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $this->isLoggedIn = true;
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
        }
        $this->load->model('api_m/User_m');
        $this->model = new \Models\api\User_m();
    }
    public function index(){

    }
    public function ajax_get() {
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : 'id, username, email, f_name, l_name, phone, image';
        if (isset($where['hash'])) {
            $where['hash'] = strongEncrypt($where['hash']);
        }
        $row = $this->model->get($where, $select);

        if ($row) {
            if ((int)$row['id'] === (int)$_SESSION['client_ID']) {
                echo json_encode(['status' => true, 'data' => $row]);
            }
            else {
                echo json_encode(['status' => true, 'data' => true]);
            }
        }
        else {
            echo json_encode(['status' => true, 'data' => false]);
        }
        die();
    }
    public function ajax_getSecret() {
        $where = $_POST['where'];

        if (isset($where['hash'])) {
            $where['hash'] = strongEncrypt($where['hash']);
            $where['id'] = $_SESSION['client_ID'];
        }
        else {
            echo json_encode(['status' => false, 'data' => false]);
            die();
        }
        $row = $this->model->get($where);

        if ($row) {
            $row['stripeApi_pKey'] = \Helpers\jwtDecode($row['stripeApi_pKey']);
            $row['stripeApi_cUrl'] = \Helpers\jwtDecode($row['stripeApi_cUrl']);
            echo json_encode(['status' => true, 'data' => $row]);
        }
        else {
            echo json_encode(['status' => true, 'data' => false]);
        }
        die();
    }
    public function ajax_update() {
        $sets = $_POST['sets'];

        if (isset($sets['hash'])) {
            $sets['hash'] = strongEncrypt($sets['hash']);
        }
        if (isset($sets['stripeApi_pKey'])) {
            $sets['stripeApi_pKey'] = \Helpers\jwtEncode($sets['stripeApi_pKey']);
        }
        if (isset($sets['stripeApi_cUrl'])) {
            $sets['stripeApi_cUrl'] = \Helpers\jwtEncode($sets['stripeApi_cUrl']);
        }
        $this->model->update($_SESSION['client_ID'], $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_search() {
        $keyword = $_POST['q'];
        $where = isset($_POST['where']) ? $_POST['where'] : [];
        $rows = $this->model->search($keyword, 100, 0, $where);
        foreach ($rows as &$row) {
            unset($row['stripeApi_pKey']);
            unset($row['stripeApi_cUrl']);
        }
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
}

$handle = new User();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $handle->ajax_get();
            break;
        case 'get_secret':
            $handle->ajax_getSecret();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'search':
            $handle->ajax_search();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}
