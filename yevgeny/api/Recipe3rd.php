<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 24.07.2018
 * Time: 11:10
 */

require_once __DIR__ . '/../options/Recipe.php';

class Recipe3rd
{
    private $apiKey;
    public function __construct()
    {
        global $recipeApiKey;
        $this->apiKey = $recipeApiKey;
    }
    public function index(){

    }
    public function ajax_get() {
        $id = $_POST['id'];
        $item = $this->getFood2Fork($id);
        $item->body = $this->makePostBodyByRecipe($item);
        echo json_encode(['status' => true, 'data' => $item]);
    }
    private function makePostBodyByRecipe($recipe){
        $body = '<section class="recipe-body">';
        $body .= '<div><img src="'. $recipe->image_url .'"></div>';
        $body .= '<h4 style="margin-bottom: 0px;">Ingredients:</h4>';
        $body .= '<ul>';
        foreach ($recipe->ingredients as $ingredient){
            $body .= '<li>'. $ingredient .'</li>';
        }
        $body .= '</ul>';
        $body .= '<h5><lable>Publisher:</lable> <span class="publisher">'. $recipe->publisher .'</span></h5>';
        $body .= '<div><a href="'. $recipe->source_url .'" target="_blank">View Detail</a></div>';
        $body .= '</section>';
        return $body;
    }
    private function getFood2Fork($id){
        $curl = curl_init();
        $url = 'http://food2fork.com/api/get?key='. $this->apiKey .'&rId=' . $id;
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        return $result->recipe;
    }
}


$handle = new Recipe3rd();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $handle->ajax_get();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}