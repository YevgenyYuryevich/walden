<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.04.2018
 * Time: 09:12
 */

require_once '../helpers/funcs.php';

use function Helpers\{uploadFile, parseStringToArray};

class Uploads
{
    public function __construct()
    {
    }
    public function index(){
        $file = $_FILES['file'];
        $sets = isset($_POST['sets']) ? parseStringToArray($_POST['sets']) : [];
        $rlt = uploadFile($file, $sets);
        echo json_encode(['status' => $rlt ? true : false, 'data' => $rlt]);
        die();
    }
}

$handle = new Uploads();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}
