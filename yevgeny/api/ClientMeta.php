<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 03.04.2018
 * Time: 06:21
 */

require_once '../../guardian/access.php';
require_once './../models/api_m/ClientMeta_m.php';
require_once '../helpers/funcs.php';

class ClientMeta
{
    public function __construct()
    {
        $this->model = new \Models\api\ClientMeta_m();
    }
    public function index(){}
    public function ajax_insert(){

    }
    public function ajax_update(){

    }
    public function ajax_delete(){

    }
}

$handle = new ClientMeta();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}