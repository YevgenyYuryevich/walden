<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.03.2018
 * Time: 16:55
 */

require_once '../../guardian/access.php';
require_once '../helpers/funcs.php';
require_once __DIR__ . '/../controllers/Products_c.php';

use Controllers\Products_c;

class Products extends Products_c{
    public function ajax_getProducts(){
        $products = $this->getProductsData();
        echo json_encode(['status' => true, 'data' => $products]);
        die();
    }
    public function ajax_createNewProduct(){
        $res = $this->createNewProductByAsin();
        echo json_encode(array('status' => true, 'data' => $res));
        die();
    }
    public function ajax_closeProduct(){
        $rlt = $this->closeProduct();
        if ($rlt){
            echo json_encode(['status' => true]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
}


$handle = new Products();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get_products':
            $handle->ajax_getProducts();
            break;
        case 'create_new_product':
            $handle->ajax_createNewProduct();
            break;
        case 'close_product':
            $handle->ajax_closeProduct();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
//    $handle->test();
}