<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 29.09.2018
 * Time: 08:31
 */

require_once __DIR__ . '/../core/Controller_core.php';

class Category extends \Core\Controller_core
{
    private $model;
    private $seriesModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Category_m');
        $this->model = new \Models\api\Category_m();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
    }
    public function ajax_get(){
        $rows = $this->model->getRows();
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
    public function ajax_getMany() {
        $rows = $this->model->getRows();
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
    public function ajax_getRowsGroupByLevel() {
        $categories = $this->model->getRows();
        $sCategories = [];
        $eCategories = [];

        foreach ($categories as $category) {
            $sSeries_cnt = $this->seriesModel->count(['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 0]);
            if ($sSeries_cnt) {
                $sCategories[] = $category;
            }

            $eSeries_cnt = $this->seriesModel->count(['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 1]);
            if ($eSeries_cnt) {
                $eCategories[] = $category;
            }
        }
        echo json_encode(['status' => true, 'data' => ['simplify' => $sCategories, 'enrich' => $eCategories]]);
        die();
    }
}

$hdl = new Category();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $hdl->ajax_get();
            break;
        case 'get_many':
            $hdl->ajax_getMany();
            break;
        case 'insert':
            $hdl->ajax_insert();
            break;
        case 'update':
            $hdl->ajax_update();
            break;
        case 'get_group_by_level':
            $hdl->ajax_getRowsGroupByLevel();
            break;
        default:
            $hdl->ajax_get();
            break;
    }
}
else {
    $hdl->ajax_get();
}