<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 22:38
 */

require_once '../../guardian/class/userauth.class.php';
require_once '../helpers/funcs.php';
require_once '../models/api_m/Posts_m.php';
require_once '../models/api_m/Series_m.php';
require_once '../controllers/Posts_c.php';

use Models\api\Series_m;
use function Helpers\input;

class Posts extends \Controllers\Posts_c
{
    public function __construct()
    {

        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $this->isLoggedIn = true;
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
        }
        parent::__construct();
        $this->seriesModel = new Series_m();
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function index(){
    }
    public function isPostMine($where){
        $post = $this->model->get($where);
        $seriesId = $post['intPost_series_ID'];
        $series = $this->seriesModel->get($seriesId);
        $seriesClientId = $series['intSeries_client_ID'];
        return $this->currentUser == $seriesClientId;
    }

    public function ajax_add(){

//        will deprecate

        $sets = $_POST['sets'];
        $newId = $this->model->insert($sets);
        echo json_encode(['status' => true, 'data' => $newId]);
        die();
    }
    public function ajax_insert(){
        $input = input();
        $sets = $input['sets'];

        $newId = $this->insert($sets);
        $row = $this->model->get($newId);
        echo json_encode(['status' => true, 'data' => $row]);
        die();
    }
    public function ajax_insertMany() {
        $items = isset($_POST['items']) ? $_POST['items'] : [];
        $posts = [];
        foreach ($items as $item) {
            $id = $this->insert($item);
            $post = $this->model->get($id);
            $post['viewDay'] = $this->calcDay($post);
            $posts[] = $post;
        }
        echo json_encode(['status' => true, 'data' => $posts]);
        die();
    }
    public function ajax_deleteMany() {
        $deletes = $_POST['deletes'];
        $post = $this->model->get($deletes[0]['where']);
        $purchases = $this->purchasedModel->getRows(['intPurchased_series_ID' => $post['intPost_series_ID']]);
        foreach ($deletes as $delete) {
            $this->delete($delete['where']);
        }
        foreach ($purchases as $purchased) {
            $this->subsHandle->orderSubscriptions(['intClientSubscription_purchased_ID' => $purchased['purchased_ID'], 'intClientSubscriptions_parent' => 0]);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateMany() {
        $updates = $_POST['updates'];
        $parents = [];
        $seriesId = false;
        foreach ($updates as $update) {
            $orgPost = $this->model->get($update['where']);
            $this->update($update['sets'], $update['where']);
            $sets = $update['sets'];
            $seriesId = $orgPost['intPost_series_ID'];
            if (isset($sets['intPost_parent']) && $sets['intPost_parent'] != $orgPost['intPost_parent']) {
                $parents[$sets['intPost_parent']] = true;
                $parents[$orgPost['intPost_parent']] = true;
            }
            else if (isset($sets['intPost_order'])) {
                $parents[$orgPost['intPost_parent']] = true;
            }
        }
        $parents = array_keys($parents);
        foreach ($parents as $parentPostId) {
            $this->orderPosts([
                'intPost_series_ID' => $seriesId,
                'intPost_parent' => $parentPostId,
            ]);
            $this->orderSubscriptions($seriesId, $parentPostId);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_delete(){

        $where = $_POST['where'];
        if ($this->isPostMine($where)){
            $orgPost = $this->model->get($where);
            $this->delete($where);
            $this->orderPosts([
                'intPost_series_ID' => $orgPost['intPost_series_ID'],
                'intPost_parent' => $orgPost['intPost_parent'],
            ]);
            $this->orderSubscriptions($orgPost['intPost_series_ID'], $orgPost['intPost_parent']);
            echo json_encode(['status' => true]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
    public function ajax_update(){
        $input = input();
        $where = $input['where'];
        $sets = $input['sets'];
        if (isset($sets['intPost_savedMoney']) && $sets['intPost_savedMoney'] == '') {
            unset($sets['intPost_savedMoney']);
        }
        $orgPost = $this->model->get($where);
        $sets = $this->update($sets, $where);
        if (isset($sets['intPost_parent']) && $sets['intPost_parent'] != $orgPost['intPost_parent']) {
            $this->orderPosts([
                'intPost_series_ID' => $orgPost['intPost_series_ID'],
                'intPost_parent' => $orgPost['intPost_parent'],
            ]);
            $this->orderSubscriptions($orgPost['intPost_series_ID'], $orgPost['intPost_parent']);

            $this->orderPosts([
                'intPost_series_ID' => $orgPost['intPost_series_ID'],
                'intPost_parent' => $sets['intPost_parent'],
            ]);
            $this->orderSubscriptions($orgPost['intPost_series_ID'], $sets['intPost_parent']);
        }
        else if (isset($sets['intPost_order'])) {
            $this->orderPosts([
                'intPost_series_ID' => $orgPost['intPost_series_ID'],
                'intPost_parent' => $orgPost['intPost_parent'],
            ]);
            $this->orderSubscriptions($orgPost['intPost_series_ID'], $orgPost['intPost_parent']);
        }
        echo json_encode(['status' => true, 'data' => $sets]);
        die();
    }
    public function ajax_updatePosts(){
        $updateSets = $_POST['updateSets'];
        foreach ($updateSets as $updateSet){
            $where = $updateSet['where'];
            $sets = $updateSet['sets'];
            $this->update($sets, $where);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getMany(){
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : '*';
        $limit = isset($_POST['limit']) ? $_POST['limit'] : false;
        $items = $this->model->getPosts($where, $select, ['intPost_order', 'post_ID'], $limit);
        foreach ($items as &$item) {
            $this->format($item);
        }
        echo json_encode(['status' => true, 'data' => $items]);
        die();
    }
    public function ajax_getSerialItems(){
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : '*';
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 1000000;
        $orderBy = isset($_POST['orderBy']) ? $_POST['orderBy'] : ['intPost_order', 'post_ID'];
        $items = $this->getSerialItems($where, $select, $orderBy, $limit);
        foreach ($items as & $item) {
            $this->format($item);
        }
        echo json_encode(['status' => true, 'data' => $items]);
        die();
    }
    public function ajax_get(){
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : '*';
        $item = $this->model->get($where, $select);
//        $rItem = $this->filterRedirect($item);
        $fItem = $this->format($item);
        echo json_encode(['status' => true, 'data' => $fItem]);
        die();
    }
    public function ajax_getMax(){
        $where = $_POST['where'];
        $select = $_POST['select'];
        $item = $this->model->get($where, 'MAX('. $select .') as max_' . $select);
        $max = $item ? $item['max_' . $select] : 0;
        echo json_encode(['status' => true, 'data' => $max]);
        die();
    }
    public function ajax_clone() {
        $where = $_POST['where'];
        $rtn = $this->cloneItem($where);
        echo json_encode(['status' => true, 'data' => $rtn]);
        die();
    }
    public function ajax_afterClone() {
        $where = $_POST['where'];
        $rtn = $this->afterClone($where);
        echo json_encode(['status' => true, 'data' => $rtn]);
        die();
    }
    public function ajax_seekPost() {
        $CSViewId = $_POST['CSView'];
        $CSView = $this->CSViewModel->get($CSViewId);
        $seekPost = $this->seekPost($CSView);
        $fPost = $this->format($seekPost);
        echo json_encode(['status' => true, 'data' => $fPost]);
        die();
    }
    public function ajax_seekNext() {
        $CSViewId = $_POST['CSViewId'];
        $nextPost = $this->seekNext($CSViewId);
        $fPost = $this->format($nextPost);
        echo json_encode(['status' => true, 'data' => $fPost]);
        die();
    }
    public function ajax_seekPrev() {
        $CSViewId = $_POST['CSViewId'];
        $prevPost = $this->seekPrev($CSViewId);
        $fPost = $this->format($prevPost);
        echo json_encode(['status' => true, 'data' => $fPost]);
        die();
    }
    public function ajax_setSeek() {
        $where = $_POST['where'];
        $sets = $_POST['sets'];
        $sets['intCSView_seekOrder'] = isset($sets['intCSView_seekOrder']) ? $sets['intCSView_seekOrder'] : 0;
        $seekPost = $this->setSeek($where, $sets);
        $fPost = $this->format($seekPost);
        echo json_encode(['status' => true, 'data' => $fPost]);
        die();
    }
    public function ajax_selectPath() {
        $id = $_POST['id'];
        $path = $this->model->get($id);
        $post = $this->model->get(['intPost_series_ID' => $path['intPost_series_ID'], 'intPost_parent' => $id]);
        if (!$post) {
            echo json_encode(['status' => false]);
            die();
        }
        $rPost = $this->filterRedirect($post);
        $fPost = $this->format($rPost);
        echo json_encode(['status' => true, 'data' => $fPost]);
        die();

    }
    public function ajax_nextPost() {
        $id = $_POST['id'];
        $nextPost = $this->getNextPost($id);
        $fNextPost = $this->format($nextPost);
        echo json_encode(['status' => true, 'data' => $fNextPost]);
        die();
    }
    public function ajax_prevPost() {
        $id = $_POST['id'];
        $prevPost = $this->getPrevPost($id);
        $fPrevPost = $this->format($prevPost);
        echo json_encode(['status' => true, 'data' => $fPrevPost]);
        die();
    }
    public function ajax_search() {
        $kwd = $_POST['keyword'];
        $rows = $this->model->search($kwd, 100, 0);
        foreach ($rows as &$row) {
            $row['viewDay'] = $this->calcDay($row);
        }
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
    public function ajax_deleteUnread() {
        $where = $_POST['where'];
        $this->unReadPostsModel->delete($where);
        echo json_encode(['status' => true]);
        die();
    }
}

$postsHandle = new Posts();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'add':
            $postsHandle->ajax_add();
            break;
        case 'insert':
            $postsHandle->ajax_insert();
            break;
        case 'insert_many':
            $postsHandle->ajax_insertMany();
            break;
        case 'delete':
            $postsHandle->ajax_delete();
            break;
        case 'delete_many':
            $postsHandle->ajax_deleteMany();
            break;
        case 'update':
            $postsHandle->ajax_update();
            break;
        case 'update_many':
            $postsHandle->ajax_updateMany();
            break;
        case 'get':
            $postsHandle->ajax_get();
            break;
        case 'get_max':
            $postsHandle->ajax_getMax();
            break;
        case 'get_many':
        case 'get_items':
            $postsHandle->ajax_getMany();
            break;
        case 'get_serial_items':
            $postsHandle->ajax_getSerialItems();
            break;
        case 'update_posts':
            $postsHandle->ajax_updatePosts();
            break;
        case 'clone':
            $postsHandle->ajax_clone();
            break;
        case 'after_clone':
            $postsHandle->ajax_afterClone();
            break;
        case 'next_post':
            $postsHandle->ajax_nextPost();
            break;
        case 'prev_post':
            $postsHandle->ajax_prevPost();
            break;
        case 'seek_post':
            $postsHandle->ajax_seekPost();
            break;
        case 'seek_next':
            $postsHandle->ajax_seekNext();
            break;
        case 'seek_prev':
            $postsHandle->ajax_seekPrev();
            break;
        case 'set_seek':
            $postsHandle->ajax_setSeek();
            break;
        case 'select_path':
            $postsHandle->ajax_selectPath();
            break;
        case 'search':
            $postsHandle->ajax_search();
            break;
        case 'delete_unread':
            $postsHandle->ajax_deleteUnread();
            break;
        default:
            $postsHandle->index();
            break;
    }
}
else{
    $postsHandle->index();
}