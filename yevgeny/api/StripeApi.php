<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.07.2018
 * Time: 09:45
 */
require_once __DIR__ . '/../core/Controller_core.php';

class StripeApi extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->userModel = new \Models\api\User_m();
    }
    public function chargeCreate() {
        $sKey = 'sk_test_YYExn8Osz9AZUIoZFH0CWqa3';
        \Stripe\Stripe::setApiKey($sKey);
        $token = $_POST['stripeToken'];
        $amount = (int)$_POST['amount'];
        try {
            $charge = \Stripe\Charge::create([
                'amount' => $amount,
                'currency' => 'usd',
                'description' => 'Test charge',
                'source' => $token,
            ]);
            return $charge;
        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return $body;
        } catch (\Stripe\Error\RateLimit $e) {
            return false;
            // Too many requests made to the API too quickly
        } catch (\Stripe\Error\InvalidRequest $e) {
            return false;
            // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
            return false;
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        } catch (\Stripe\Error\ApiConnection $e) {
            return false;
            // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
            return false;
            // Display a very generic error to the user, and maybe send
            // yourself an email
        } catch (Exception $e) {
            return false;
            // Something else happened, completely unrelated to Stripe
        }
    }
}

$handle = new StripeApi();
$res = $handle->chargeCreate();
echo json_encode($res);
