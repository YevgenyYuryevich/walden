<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.02.2018
 * Time: 20:10
 */

//require_once '../../guardian/access.php';
require_once '../helpers/funcs.php';
require_once '../models/api_m/Search_m.php';
require_once '../vendor/autoload.php';
require_once '../options/azure_bing.php';

use \Crew\Unsplash\Photo;
use Crew\Unsplash\Search;
use Twingly\Client;
use Models\api\Search_m;

use function Helpers\aOrb;
use function Helpers\utf8Encode;

class SearchPosts
{
    public function __construct()
    {
        $this->model = new Search_m();
        $client = new Google_Client();
        $DEVELOPER_KEY = 'AIzaSyDYQGHEJkL-Kghnc_lO04bOML8_3pfG-s8';
        $client->setDeveloperKey($DEVELOPER_KEY);
        $this->youtubeApi = new Google_Service_YouTube($client);

        Crew\Unsplash\HttpClient::init([
            'applicationId'	=> '98ed5ecd389a5b19e3ae817fdcec9b184388937388ba185cd76fadb9d27eca87',
            'utmSource' => 'yevgeny photo web'
        ]);
        $this->unsplash = new Photo();
        $this->unsplashSearch = new Search();
        $twinglyClient = new Client('DAF536AB-B547-48F7-BCBC-CB2273052DF8');
        $this->twingly = $twinglyClient->query();

        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';
        global $azureBingKey;
        $this->azureKey = $azureBingKey;
    }

    public function test(){
    }

    public function index(){
        $keyword = utf8Encode($_POST['q']);
        $size = aOrb($_POST['size'], 10);
        $items = $_POST['items'];
        $data = [];
        foreach ($items as $item){
            $token = isset($item['token']) ? $item['token'] : false;
            $token = $token == 'false' ? false : $token;

            switch ($item['name']){
                case 'youtube':
                    $posts = $this->searchYoutube($keyword, $size, $token);
                    break;
                case 'spreaker':
                    $posts = $this->searchSpreaker($keyword, $size, $token);
                    break;
                case 'recipes':
                    $posts = $this->searchFood2Fork($keyword, $size, aOrb($token, 0));
                    break;
                case 'blogs':
//                    $posts = $this->searchTwingly($keyword, $size, aOrb($token, 0));
//                    $posts = ['items' => [], 'nextPageToken' => false];
                    $posts = $this->searchAzureBing($keyword, $size, aOrb($token, 0));
                    break;
                case 'ideabox':
                    $posts = $this->searchIdeaBox($keyword, $size, aOrb($token, 0));
                    break;
                case 'posts':
                    $posts = $this->searchPosts($keyword, $size, aOrb($token, 0));
                    break;
                case 'rssbPosts':
                    $posts = $this->searchRssbPosts($keyword, $size, aOrb($token, 0));
                    break;
            }
            $data[$item['name']] = $posts;
        }
        echo json_encode(['status' => true, 'data' => $data]);
    }

    public function searchSpreaker($keyword, $size = 10, $pageToken = false){
        $curl = curl_init();
        if ($pageToken){
            $url = $pageToken;
        }
        else{
            $keyword = urlencode($keyword);
            $url = 'https://api.spreaker.com/v2/search?type=episodes&q='. $keyword .'&limit=' . $size;
        }
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        if (isset($result->response->error)){
            return ['items' => [], 'nextPageToken' => false];
        }
        return ['items' => $result->response->items, 'nextPageToken' => $result->response->next_url];
    }
    public function searchTwingly($keyword, $size = 10, $pageToken = 0){
        $this->twingly->search_query = $keyword . ' page:'. $pageToken .' page-size:' . $size;
        $result = $this->twingly->execute();
        $imgs = $this->getPhotosFromUsp($keyword, $size, $pageToken);
        $img_ct = count($imgs);
        $posts = $result->posts;
        foreach ($posts as $key => &$post){
            $imgUrl = $img_ct ? $imgs[$key % $img_ct]['urls']['small'] : $this->defaultPhoto;
            $post->imgOrg = $imgUrl;
        }
        return ['items' => $result->posts, 'nextPageToken' => ++$pageToken];
    }
    public function searchAzureBing($keyword, $size = 10, $pageToken = 0) {
        $keyword = aOrb($keyword, 'rand');
        $headers = "Ocp-Apim-Subscription-Key: $this->azureKey\r\n";
        $options = array ('http' => array (
            'header' => $headers,
            'method' => 'GET'));

        // Perform the Web request and get the JSON response
        $context = stream_context_create($options);
        $result = file_get_contents("https://api.cognitive.microsoft.com/bing/v7.0/search?q=" . urlencode($keyword) . "&offset=" . $pageToken . "&count=" . $size, false, $context);
        $result = \Helpers\parseStringToArray($result);
        $items = isset($result) && isset($result['webPages']) ? $result['webPages']['value'] : [];

        $imgs = $items ? $this->getPhotosFromUsp($keyword, $size, $pageToken) : [];
        $img_ct = count($imgs);
        foreach ($items as $key => &$post){
            $imgUrl = $img_ct ? $imgs[$key % $img_ct]['urls']['small'] : $this->defaultPhoto;
            $post['image'] = $imgUrl;
            $post['body'] = $this->makeBodyFromAzureBing($post);
        }
        return ['items' => $items, 'nextPageToken' => count($items) + (int) $pageToken];
    }
    public function searchFood2Fork($keyword, $size = 10, $pageToken = 0){
        $curl = curl_init();
        $keyword = urlencode($keyword);
        $url = 'http://food2fork.com/api/search?key=4b57211bd951556bb6f35998d1586fff&q='. $keyword .'&page=' . floor($size * $pageToken / 30);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $curlResult = curl_exec($curl);
        $result = json_decode($curlResult);
        if (!$result) {
            return ['items' => [], $pageToken];
        }
        $recipes = $result->recipes;
        $offset = ($pageToken * $size) % 30;
        $page = array_slice($recipes, $offset, $size);
//        $nextPageToken = count($page) == $size ? ++$pageToken : $pageToken;
        $nextPageToken = count($page) ? ++$pageToken : $pageToken;

        return ['items' => $page, 'nextPageToken' => $nextPageToken];
    }
    public function searchYoutube($keyword, $size = 10, $pageToken = false){
        if (!!$pageToken){
            $searchResponse = $this->youtubeApi->search->listSearch('snippet', array(
                'type' => 'video',
                'q' => $keyword,
                'maxResults' => $size,
                'pageToken' => $pageToken
            ));
        }
        else{
            $searchResponse = $this->youtubeApi->search->listSearch('snippet', array(
                'type' => 'video',
                'q' => $keyword,
                'maxResults' => $size
            ));
        }
        $items = [];
        foreach ($searchResponse->items as $item){
            array_push($items, $item);
        }
        return ['items' => $items, 'nextPageToken' => $searchResponse->nextPageToken];
    }
    public function searchIdeaBox($keyword, $size = 10, $pageToken = 0){
        $searchRlts = $this->model->searchIdeaBox($keyword, $size, $pageToken);
        $nextPageToken = count($searchRlts) + $pageToken;
        return ['items' => $searchRlts, 'nextPageToken' => $nextPageToken];
    }
    public function searchPosts($keyword, $size = 10, $pageToken = 0){
        $searchRlts = $this->model->searchPosts($keyword, $size, $pageToken);
        $nextPageToken = count($searchRlts) + $pageToken;
        $searchRlts = utf8Encode($searchRlts);
        return ['items' => $searchRlts, 'nextPageToken' => $nextPageToken];
    }
    public function searchRssbPosts($keyword, $size = 10, $pageToken = 0){
        $searchRlts = $this->model->searchRssbPosts($keyword, $size, $pageToken);
        $nextPageToken = count($searchRlts) + $pageToken;
        $searchRlts = utf8Encode($searchRlts);
        return ['items' => $searchRlts, 'nextPageToken' => $nextPageToken];
    }
    public function getPhotosFromUsp($query, $size = 10, $pageToken = 0){
        $result = $this->unsplashSearch::photos($query, $pageToken, $size);
        return $result->getResults();
    }

    private function makeBodyFromAzureBing($item) {
        return $item['snippet'] . '<div><a href="'. $item['url'] .'">View Detail</a></div>';
    }
}

$searchPostsHandle = new SearchPosts();

$searchPostsHandle->index();
//$searchPostsHandle->test();