<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.04.2018
 * Time: 21:07
 */

require_once '../helpers/funcs.php';
require_once '../options/Youtube.php';
require_once '../options/Unsplash.php';

class Api
{
    private $youtubeApi;
    private $unsplash;
    private $defaultPhoto;
    public function __construct()
    {
        global $developerKey;
        $client = new Google_Client();
        $client->setDeveloperKey($developerKey);
        $this->youtubeApi = new Google_Service_YouTube($client);

        global $unsplashOptions;
        Crew\Unsplash\HttpClient::init($unsplashOptions);
        $this->unsplash = new \Crew\Unsplash\Photo();
        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';
    }
    public function index(){

    }
    public function ajax_get(){
        $what = $_POST['what'];
        switch ($what){
            case 'youtube':
                $where = $_POST['where'];
                $res = $this->getYoutube($where);
                break;
            case 'curl':
                $where = $_POST['where'];
                $res = $this->getCurl($where);
                break;
            default:
                break;
        }
        echo json_encode(['status' => true, 'data' => $res]);
    }
    public function ajax_curl(){
        $url = $_POST['url'];
        $data = [];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = curl_exec($curl);

        $isTitleIn = preg_match('/<title>(.*)<\/title>/', $result, $matches);
        if ($isTitleIn){
            $data['title'] = $matches[1];
        }

        $isDescriptionIn = preg_match('/<meta.*name\s*=\s*[\'\"]+description[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
        if ($isDescriptionIn){
            $data['body'] = $matches[1];
        }

        $data['image'] = $this->randUnsplash('rand');

        echo json_encode(['status' => true, 'data' => $data]);
        die();
    }
    private function getYoutube($params){
        $res = $this->youtubeApi->videos->listVideos(
            'snippet,contentDetails,statistics',
            $params
        );
        return $res->items[0];
    }
    private function randUnsplash($query){
        $filter = ['query' => $query, 'w' => 300];
        $unsplashPhoto = $this->unsplash::random($filter);
        if ($unsplashPhoto->errors){
            return $this->defaultPhoto;
        }
        return $unsplashPhoto->urls['small'];
    }
    private function getCurl($url) {
        $data = [];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = curl_exec($curl);

        $isTitleIn = preg_match('/<title>(.*)<\/title>/', $result, $matches);
        if ($isTitleIn){
            $data['title'] = $matches[1];
        }
        else {
            $isTitleIn = preg_match('/<meta.*property\s*=\s*[\'\"]+og:title[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
            if ($isTitleIn){
                $data['title'] = $matches[1];
            }
        }

        $isImageIn = preg_match('/<meta.*name\s*=\s*[\'\"]+image[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
        if ($isImageIn){
            $data['image'] = $matches[1];
        }
        else {
            $isImageIn = preg_match('/<meta.*property\s*=\s*[\'\"]+og:image[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
            if ($isImageIn){
                $data['image'] = $matches[1];
            }
        }

        $isDescriptionIn = preg_match('/<meta.*name\s*=\s*[\'\"]+description[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
        if ($isDescriptionIn){
            $data['body'] = $matches[1];
        }
        else {
            $isDescriptionIn = preg_match('/<meta.*property\s*=\s*[\'\"]+og:description[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
            if ($isDescriptionIn){
                $data['body'] = $matches[1];
            }
        }

        $isKeywordsIn = preg_match('/<meta.*name\s*=\s*[\'\"]+keywords[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
        if ($isKeywordsIn){
            $data['keywords'] = $matches[1];
        }

        $isTypeIn = preg_match('/<meta.*property\s*=\s*[\'\"]+og:type[\'\"]+.*content\s*=\s*[\"\']([^\"\']*)[\"\'].*>/i', $result, $matches);
        if ($isTypeIn){
            $data['str_type'] = $matches[1];
        }

        if (!$isImageIn) {
            $data['image'] = $this->randUnsplash('rand');
        }
        return $data;
    }
}

$handle = new Api();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $handle->ajax_get();
            break;
        case 'curl':
            $handle->ajax_curl();
            break;
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}