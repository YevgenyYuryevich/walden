<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

require_once '../../guardian/access.php';
require_once '../helpers/funcs.php';
require_once '../controllers/Helpers_c.php';

use Controllers\Helpers_c;

class Helpers extends Helpers_c
{
    public function ajax_rand(){
        $where = $_POST['where'];
        $rlt = $this->randWhere($where);
        echo json_encode(['status' => true, 'data' => $rlt]);
        die();
    }
}

$handle = new Helpers();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'rand':
            $handle->ajax_rand();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}