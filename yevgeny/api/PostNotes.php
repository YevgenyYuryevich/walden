<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 02.11.2018
 * Time: 07:54
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';

class PostNotes extends \Core\Controller_core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/PostNotes_m');
        $this->model = new \Models\api\PostNotes_m();
    }
    public function ajax_insert() {
        $sets = $_POST['sets'];
        $defaultSets = [
            'postNote_user_ID' => $_SESSION['client_ID'],
        ];
        $sets = array_merge($defaultSets, $sets);
        $id = $this->model->insert($sets);
        echo json_encode(['status' => true, 'data' => $id]);
        die();
    }
    public function ajax_update() {
        $sets = $_POST['sets'];
        $where = $_POST['where'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateMany() {
        $updates = $_POST['updates'];
        foreach ($updates as $update) {
            $this->model->update($update['where'], $update['sets']);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_delete() {
        $where = $_POST['where'];
        $this->model->delete($where);
        echo json_encode(['status' => true]);
        die();
    }
}

$hdl = new PostNotes();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $hdl->ajax_get();
            break;
        case 'insert':
            $hdl->ajax_insert();
            break;
        case 'update':
            $hdl->ajax_update();
            break;
        case 'update_many':
            $hdl->ajax_updateMany();
            break;
        case 'delete':
            $hdl->ajax_delete();
            break;
        default:
            $hdl->ajax_get();
            break;
    }
}
else {
    $hdl->ajax_get();
}