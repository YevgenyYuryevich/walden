<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.06.2018
 * Time: 15:52
 */
require_once '../../guardian/access.php';
require_once '../controllers/FormFields_c.php';

class FormFields extends \Controllers\FormFields_c
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index() {
        echo 'I am index';
        die();
    }
    public function ajax_insert() {
        $sets = $_POST['sets'];
        $id = $this->model->insert($sets);
        echo json_encode(['status' => true, 'data' => $id]);
        die();
    }
    public function ajax_insertMany() {
        $inserts = $_POST['inserts'];
        foreach ($inserts as & $sets) {
            $id = $this->model->insert($sets);
            $sets['formField_ID'] = $id;
        }
        echo json_encode(['status' => true, 'data' => $inserts]);
        die();
    }
    public function ajax_update() {
        $sets = $_POST['sets'];
        $where = $_POST['where'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_delete() {
        $where = $_POST['where'];
        $this->delete($where);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getMany() {
        $where = $_POST['where'];
        $rows = $this->model->getRows($where);
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
}

$handle = new FormFields();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'insert_many':
            $handle->ajax_insertMany();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'get_many':
            $handle->ajax_getMany();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}