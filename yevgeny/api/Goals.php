<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

require_once '../../guardian/access.php';
require_once '../helpers/funcs.php';
require_once '../controllers/Goals_c.php';

use Controllers\Goals_c;

class Goals extends Goals_c
{
    public function ajax_insert(){
        $sets = $_POST['sets'];
        $id = $this->model->insert($sets);
        $goal = $this->model->get($id);
        $goal = $this->formatGoal($goal);

        $this->filterFirstInsert();

        echo json_encode(['status' => true, 'data' => $goal]);
        die();
    }
    public function ajax_delete(){
        $where = $_POST['where'];
        $this->model->delete($where);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_update(){
        $where = $_POST['where'];
        $sets = $_POST['sets'];
        $this->model->update($sets, $where);
        if (isset($sets['boolGoal_complete'])){
            $this->filterFirstGoalsComplete();
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateGoals(){
        $updateSets = $_POST['updateSets'];
        foreach ($updateSets as $updateSet){
            $where = $updateSet['where'];
            $sets = $updateSet['sets'];
            $this->model->update($sets, $where);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getGoalsDataByDate(){
        $date = $_POST['date'];
        $today = date('Y-m-d');
        $goalsData = $this->getGoalsDataByDate($date);
        if ($today == $date){
//            $tomorrowDate = date('Y-m-d', strtotime($today . ' +1 day'));
//            $tomorrowData = $this->getGoalsDataByDate($tomorrowDate);
//            $goalsData['grateGoals'] = array_merge($goalsData['grateGoals'], $tomorrowData['grateGoals']);
//            $goalsData['normalGoals'] = array_merge($goalsData['normalGoals'], $tomorrowData['normalGoals']);
//            $goalsData['trapGoals'] = array_merge($goalsData['trapGoals'], $tomorrowData['trapGoals']);
        }
        echo json_encode(['status' => true, 'data' => $goalsData]);
        die();
    }
}

$handle = new Goals();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'update_goals':
            $handle->ajax_updateGoals();
            break;
        case 'get_goals_data_by_date':
            $handle->ajax_getGoalsDataByDate();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}