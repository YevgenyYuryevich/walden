<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 09.06.2018
 * Time: 05:59
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';

class FormFieldAnswers extends \Core\Controller_core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/FormFieldAnswer_m');
        $this->model = new \Models\api\FormFieldAnswer_m();
    }
    public function index() {
        echo 'I am index';
        die();
    }
    public function ajax_insert() {
        $sets = $_POST['sets'];
        $id = $this->model->insert($sets);
        echo json_encode(['status' => true, 'data' => $id]);
    }
    public function ajax_update() {
        $sets = $_POST['sets'];
        $where = $_POST['where'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true]);
    }
    public function ajax_delete() {
        $where = $_POST['where'];
        $this->model->delete($where);
        echo json_encode(['status' => true]);
    }
    public function ajax_getMany() {
        $where = $_POST['where'];
        $rows = $this->model->getRows($where);
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
}


$handle = new FormFieldAnswers();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'get_many':
            $handle->ajax_getMany();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}