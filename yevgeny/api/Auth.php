<?php

require_once __DIR__ . '/../controllers/Auth_c.php';

class Auth extends \Controllers\Auth_c
{
    public function __construct()
    {
        parent::__construct();
    }
    public function apiLogin() {
        $user = $_POST['username'];
        $password = $_POST['password'];
        $rlt = $this->login($user, $password);
        if ($rlt) {
            echo json_encode($rlt);
        } else {
            echo json_encode(['error' => 'invalid credential']);
        }
        die();
    }
}
$hdl = new Auth();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'login':
            $hdl->apiLogin();
            break;
        default:
            $hdl->apiLogin();
            break;
    }
}
else {
}