<?php
/**
 * Created by PhpStorm.
 * User: danny batl
 * Date: 04.04.2019
 * Time: 21:22
 */

require_once __DIR__ . '/../core/Controller_core.php';

class HtmlToPdf
{
    public function index() {
        $html = $_POST['html'];
        $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'fr');
        $html2pdf->writeHTML($html);
        $html2pdf->output('example00.pdf');
    }
}
$handle = new HtmlToPdf();
$handle->index();