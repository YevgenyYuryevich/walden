<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 03.07.2018
 * Time: 13:12
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';

use function Helpers\{
    uploadFile,
    parseStringToArray
};

class FileToText extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('FileToText_lb');
        $this->load->library('PhpWord_lb');
    }
    public function index(){
        $file = $_FILES['file'];
        $sets = isset($_POST['sets']) ? parseStringToArray($_POST['sets']) : [];
        $sets = array_merge(['where' => 'assets/media/document', 'prefix' => 'temp'], $sets);
        $savedUrl = uploadFile($file, $sets);
        $path = $savedUrl['path'];

        $phpWord = new \Library\PhpWord_lb($path);
        $html = $phpWord->convertToHtml();
        if ($html) {
            echo json_encode(['status' => true, 'data' => $html, 'url' => $savedUrl]);
        }
        else {
            $fileToTextHandle = new \Library\FileToText_lb($path);
            $text = $fileToTextHandle->convertToText();
            echo json_encode(['status' => true, 'data' => $text, 'url' => $savedUrl]);
        }
        die();
    }
    private function cleanString($str) {
        if (substr($str, 0, 4) === '<br>') {
            return $this->cleanString(substr($str, 4));
        }
        else if (substr($str, -4, 4) === '<br>') {
            return $this->cleanString(substr($str, 0, -4));
        }
        else {
            return $str;
        }
    }
}

$handle = new FileToText();
$handle->index();