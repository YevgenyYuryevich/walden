<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.02.2018
 * Time: 01:17
 */
require_once '../../guardian/access.php';
require_once '../controllers/Ideabox_c.php';
require_once '../helpers/funcs.php';
require_once '../models/api_m/Ideabox_m.php';

use Controllers\Ideabox_c;

class Ideabox extends Ideabox_c
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
    }
    public function ajax_delete(){
        $id = $_POST['id'];
        $this->model->delete($id);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_insert(){
        $sets = json_decode($_POST['sets']);
        $sets = json_decode(json_encode($sets), true);
        if (isset($_FILES['image'])){
            $sets['strIdeaBox_image'] = $_FILES['image'];
        }
        $id = $this->insert($sets);
        $data = $this->model->get($id);
        $data = $this->formatIdeabox($data);
        echo json_encode(['status' => true, 'data' => $data]);
        die();
    }
    public function ajax_update(){
        $sets = json_decode($_POST['sets']);
        $sets = json_decode(json_encode($sets), true);

        $where = json_decode($_POST['where']);
        $where = json_decode(json_encode($where), true);

        if (isset($_FILES['image'])){
            $sets['strIdeaBox_image'] = $_FILES['image'];
        }
        $this->update($sets, $where);
        $data = $this->model->get($where);
        $data = $this->formatIdeabox($data);
        echo json_encode(['status' => true, 'data' => $data]);
        die();
    }
    public function get(){

    }
}

$handle = new Ideabox();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}