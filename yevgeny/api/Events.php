<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 05.03.2018
 * Time: 17:07
 */

require_once '../../guardian/access.php';
require_once '../helpers/funcs.php';
require_once __DIR__ . '/../controllers/Events_c.php';

use Controllers\Events_c;

class Events extends Events_c
{
    public function ajax_getEvents(){
        $events = $this->getEvents();
        echo json_encode(['status' => true, 'data' => $events]);
    }
    public function ajax_insert(){
        $sets = json_decode($_POST['sets']);
        $sets = json_decode(json_encode($sets), true);
        if (isset($_FILES['event_image'])){
            $sets['event_image'] = $_FILES['event_image'];
        }
        $data = $this->insert($sets);
        echo json_encode(['status' => true, 'data' => $data]);
        die();
    }
    public function ajax_makeEventFavorite(){
        $rlt = $this->makeEventFavorite();
        echo json_encode(['status' => true, 'data' => $rlt]);
        die();
    }
    public function ajax_makeEventUnFavorite(){
        $this->makeEventUnFavorite();
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_deleteRescheduledEvent(){
        $this->deleteRescheduledEvent();
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_deleteEvent(){
        $this->deleteEvent();
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_cancelEventInstance(){
        $res = $this->cancelEventInstance();
        echo json_encode(array('status' => true, 'data' => $res));
        die();
    }
}
$handle = new Events();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get_events':
            $handle->ajax_getEvents();
            break;
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'make_event_favorite':
            $handle->ajax_makeEventFavorite();
            break;
        case 'make_event_unfavorite':
            $handle->ajax_makeEventUnFavorite();
            break;
        case 'delete_rescheduled_event':
            $handle->ajax_deleteRescheduledEvent();
            break;
        case 'delete_event':
            $handle->ajax_deleteEvent();
            break;
        case 'cancel_event_instance':
            $handle->ajax_cancelEventInstance();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
//    $handle->test();
}