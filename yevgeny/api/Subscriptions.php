<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 22:56
 */

require_once '../../guardian/access.php';
require_once '../controllers/Subscriptions_c.php';
require_once '../helpers/funcs.php';

use Models\api\Subscriptions_m;
use Models\api\Posts_m;
use Models\api\Series_m;
use function \Helpers\input;

class Subscriptions extends \Controllers\Subscriptions_c
{
    private $seriesModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new Series_m();
    }
    public function index(){

    }
    public function getSubscriptionsByPostId($postId){
        $post = $this->postsModel->get($postId);
        $purchased = $this->model->getPurchasedBySeriesId($post['intPost_series_ID']);
        if (!$purchased){
            return [];
        }
        return $this->model->getSubscriptionsByPurchasedIdPostId($purchased['purchased_ID'], $postId);
    }
    public function ajax_add(){
//     will deprecate
        $sets = $_POST['sets'];
        $newId = $this->model->insert($sets);
        echo json_encode(['status' => true, 'data' => $newId]);
        die();
    }
    public function ajax_insert(){
        $input = input();
        $sets = $input['sets'];
        $newId = $this->insert($sets);
        $row = $this->model->get($newId);
        echo json_encode(['status' => true, 'data' => $row]);
        die();
    }
    public function ajax_update(){
        $sets = $_POST['sets'];
        $where = $_POST['where'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_updateItems() {
        $updateSets = $_POST['updateSets'];

        $synchronizeWithPost = isset($_POST['synchronizeWithPost']) ? $_POST['synchronizeWithPost'] : false;

        foreach ($updateSets as $updateSet){
            $where = $updateSet['where'];
            $sets = $updateSet['sets'];
            $this->model->update($where, $sets);
            if ($synchronizeWithPost) {
                $this->synchronizeWithPost('update', ['where' => $where, 'sets' => $sets]);
            }
        }

        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_delete(){
        $where = $_POST['where'];
        $this->model->delete($where);
        $synchronizeWithPost = isset($_POST['synchronizeWithPost']) ? $_POST['synchronizeWithPost'] : false;
        if ($synchronizeWithPost) {
            $this->synchronizeWithPost('delete', $where);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_deepDelete() {
        $where = $_POST['where'];
        $this->deepDelete($where);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_trash(){
        $where = isset($_POST['where']) ? $_POST['where'] : $_POST['id'];
        $subs = $this->model->get($where);
        if (is_null($subs['intClientSubscription_post_ID'])){
            $this->model->delete($subs['clientsubscription_ID']);
        }
        else {
            $post = $this->postsModel->get($subs['intClientSubscription_post_ID']);
            $sets = [
                'intTrashPost_post_ID' => $subs['intClientSubscription_post_ID'],
                'intTrashPost_series_ID' => $post['intPost_series_ID']
            ];
            $this->model->insertTrashPost($sets);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_deleteByPostId(){
        $postId = $_POST['postId'];
        $post = $this->postsModel->get($postId);
        $subs = $this->getSubscriptionsByPostId($postId);
        foreach ($subs as $sub){
            $sets = [
                'intTrashPost_post_ID' => $postId,
                'intTrashPost_series_ID' => $post['intPost_series_ID'],
            ];
            $this->model->insertTrashPost($sets);
            $this->model->delete($sub['clientsubscription_ID']);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getItems() {
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : '*';
        $res = [
            'status' => true,
        ];
        if (isset($_POST['meta'])) {
            $meta = $_POST['meta'];
            $preAction = isset($meta['pre_action']) ? $meta['pre_action'] : false;
            if ($preAction == 'subscribe') {
                $metaWhere = $meta['where'];
                $purchasedId = $meta['intClientSubscription_purchased_ID'];
                $limit = isset($meta['limit']) ? $meta['limit'] : 100;
                $this->subscribeSeries($metaWhere, $purchasedId, $limit);
            }
            if (isset($meta['get_unread_posts'])) {
                $seriesId = $meta['get_unread_posts']['series_id'];
                $this->load->controller('Series_c');
                $this->seriesHandle = new \Controllers\Series_c();
                $unReadPosts = $this->seriesHandle->unReadPosts($seriesId);
                $res['unReadPosts'] = $unReadPosts;
            }
        }
        $items = $this->getSubscriptions($where, $select);
        $res['data'] = $items;
        echo json_encode($res);
        die();
    }
    public function ajax_get() {
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : '*';
        $item = $this->get($where, $select);
        echo json_encode(['status' => true, 'data' => $item]);
        die();
    }
    public function ajax_clone() {

        $where = $_POST['where'];
        $rtn = $this->cloneItem($where);
        echo json_encode(['status' => true, 'data' => $rtn]);
        die();
    }
    public function ajax_setComplete() {
        $sets = $_POST['sets'];
        $id = $this->subsCpltModel->insert($sets);
        echo json_encode(['status' => true, 'data' => $id]);
        die();
    }
    public function ajax_setFavorite() {
        $subscriptionId = $_POST['subscription_id'];
        $value = $_POST['value'];
        if ((int) $value) {
            $subscription = $this->model->get($subscriptionId);
            $sets = ['intFavorite_subscription_ID' => $subscriptionId];

            if ($subscription['intClientSubscription_post_ID']){
                $post = $this->postsModel->get($subscription['intClientSubscription_post_ID']);
                $sets['intFavorite_post_ID'] = $subscription['intClientSubscription_post_ID'];
                $sets['intFavorite_series_ID'] = $post['intPost_series_ID'];
            }
            $defaults = [
                'intFavorite_client_ID' => $_SESSION['client_ID'],
            ];
            $sets = array_merge($defaults, $sets);
            $id = $this->favoriteModel->insert($sets);
            $f = $this->favoriteModel->get($id);
            echo json_encode(['status' => true, 'data' => $f]);
        }
        else {
            $this->favoriteModel->delete(['intFavorite_client_ID' => $_SESSION['client_ID'], 'intFavorite_subscription_ID' => $subscriptionId]);
            echo json_encode(['status' => true]);
        }
        die();
    }
    public function ajax_synchronizeWithPost(){

    }
    public function ajax_deepSubscribe() {
        $endPoint = $_POST['end_point'];
        $sub = $this->deepSubscribe($endPoint, 'all');
        if ($sub) {
            $sub = $this->formatSubscription($sub);
        }
        echo json_encode(['status' => true, 'data' => $sub]);
        die();
    }
}

$subscriptionsHandle = new Subscriptions();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'add':
            $subscriptionsHandle->ajax_add();
            break;
        case 'insert':
            $subscriptionsHandle->ajax_insert();
            break;
        case 'get':
            $subscriptionsHandle->ajax_get();
            break;
        case 'update':
            $subscriptionsHandle->ajax_update();
            break;
        case 'update_items':
            $subscriptionsHandle->ajax_updateItems();
            break;
        case 'delete':
            $subscriptionsHandle->ajax_delete();
            break;
        case 'deep_delete':
            $subscriptionsHandle->ajax_deepDelete();
            break;
        case 'trash':
            $subscriptionsHandle->ajax_trash();
            break;
        case 'delete_by_postId':
            $subscriptionsHandle->ajax_deleteByPostId();
            break;
        case 'get_many':
        case 'get_items':
            $subscriptionsHandle->ajax_getItems();
            break;
        case 'clone':
            $subscriptionsHandle->ajax_clone();
            break;
        case 'set_complete':
            $subscriptionsHandle->ajax_setComplete();
            break;
        case 'set_favorite':
            $subscriptionsHandle->ajax_setFavorite();
            break;
        case 'deep_subscribe':
            $subscriptionsHandle->ajax_deepSubscribe();
            break;
        case 'synchronize_with_post':
            $subscriptionsHandle->ajax_synchronizeWithPost();
        default:
            $subscriptionsHandle->index();
            break;
    }
}
else{
    $subscriptionsHandle->index();
}