<?php

/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.04.2019
 * Time: 15:40
 */

require_once '../../guardian/access.php';
require_once __DIR__ . '/../core/Controller_core.php';

class SharedPosts extends \Core\Controller_core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/SharedPosts_m');
        $this->model = new \Models\api\SharedPosts_m();
    }

    public function ajax_shareMany() {
        $posts = $_POST['posts'];
        $maxId = $this->model->max('shared_id');
        $shared_id = (int) $maxId + 1;
        foreach ($posts as $post) {
            $this->model->insert([
                'user_id' => $_SESSION['client_ID'],
                'post_id' => $post,
                'shared_id' => $shared_id,
            ]);
        }
        echo json_encode(['status' => true, 'data' => $shared_id]);
        die();
    }
}
$hdl = new SharedPosts();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'share_many':
            $hdl->ajax_shareMany();
            break;
        default:
            $hdl->ajax_get();
            break;
    }
}