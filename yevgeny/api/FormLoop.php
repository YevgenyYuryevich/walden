<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 20.02.2019
 * Time: 19:29
 */
require_once '../../guardian/access.php';
require_once '../core/Controller_core.php';

class FormLoop extends \Core\Controller_core
{
    public $model;
    public $fieldModel;
    public $answerModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/FormLoop_m');
        $this->model = new \Models\api\FormLoop_m();

        $this->load->model('api_m/FormField_m');
        $this->fieldModel = new \Models\api\FormField_m();

        $this->load->model('api_m/FormFieldAnswer_m');
        $this->answerModel = new \Models\api\FormFieldAnswer_m();
    }
    public function ajax_insert() {
        $sets = $_POST['sets'];
        $id = $this->model->insert($sets);
        $sets[$this->model->PRIMARY_KEY] = $id;
        echo json_encode(['data' => $sets, 'status' => true]);
        die();
    }
    public function ajax_getMany() {
        $where = $_POST['where'];
        $rows = $this->model->getRows($where);
        echo json_encode(['data' => $rows, 'status' => true]);
        die();
    }
    public function ajax_getLoop() {
        $where = $_POST['where'];
        $loop = $this->model->get($where);
        if (!$loop) {
            echo json_encode(['status' => true, 'data' => false]);
            die();
        }
        $fields = $this->fieldModel->getRows(['formField_loop_ID' => $loop['formLoop_ID']]);
        foreach ($fields as & $field) {
            $field['answers'] = $this->answerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID'], 'intFormFieldAnswer_field_ID' => $field['formField_ID']]);
        }
        $loop['fields'] = $fields;
        echo json_encode(['status' => true, 'data' => $loop]);
        die();
    }
    public function ajax_delete() {
        $where = $_POST['where'];
        $this->model->delete($where);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_update() {
        $where = $_POST['where'];
        $sets = $_POST['sets'];
        $this->model->update($where, $sets);
        echo json_encode(['status' => true, 'data' => $sets]);
        die();
    }
}

$handle = new FormLoop();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        case 'update':
            $handle->ajax_update();
            break;
        case 'get_loop':
            $handle->ajax_getLoop();
            break;
        case 'get_many':
            $handle->ajax_getMany();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}