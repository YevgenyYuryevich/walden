<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.12.2018
 * Time: 23:54
 */

require_once __DIR__ . '/../core/Controller_core.php';

class Options extends \Core\Controller_core
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Options_m');
        $this->model = new \Models\api\Options_m();
    }
    public function ajax_get() {
        $name = $_POST['name'];
        $row = $this->model->get(['strOption_name' => $name]);
        $value = $row ? $row['strOption_value'] : false;
        echo json_encode(['status' => true, 'data' => $value]);
        die();
    }
}

$hdl = new Options();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'get':
            $hdl->ajax_get();
            break;
        case 'insert':
            $hdl->ajax_insert();
            break;
        case 'update':
            $hdl->ajax_update();
            break;
        case 'get_group_by_level':
            $hdl->ajax_getRowsGroupByLevel();
            break;
        default:
            $hdl->ajax_get();
            break;
    }
}
else {
    $hdl->ajax_get();
}