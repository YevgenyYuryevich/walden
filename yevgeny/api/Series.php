<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 21.02.2018
 * Time: 20:26
 */

require_once '../../guardian/class/userauth.class.php';
require_once '../controllers/Series_c.php';
require_once '../models/api_m/Series_m.php';
require_once '../models/api_m/Purchased_m.php';

use function Helpers\input;

class Series extends \Controllers\Series_c
{
    private $affiliateTracksModel;
    private $checkoutSessionSeriesModel;

    public function __construct()
    {
        $auth = new UserAuthentication();

        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->userModel = new \Models\api\User_m();
        $this->load->model('api_m/Affiliate_Tracks_m');
        $this->affiliateTracksModel = new \Models\api\Affiliate_Tracks_m();

        $this->load->model('api_m/CheckoutSession_Series_m');
        $this->checkoutSessionSeriesModel = new \Models\api\CheckoutSession_Series_m();
    }
    public function ajax_insert() {
        $input = input();
        $id = $this->insert($input['sets']);
        $row = $this->model->get($id);
        echo json_encode(['status' => true, 'data' => $row]);
        die();
    }
    public function ajax_insertAutoDraft() {
        $sets = [
            'strSeries_status' => 'auto-draft',
            'boolSeries_approved' => 0,
        ];
        $id = $this->model->insert($sets);
        echo json_encode(['status' => true, 'data' => $id]);
        die();
    }
    public function ajax_get() {
        $where = isset($_POST['where']) ? $_POST['where'] : [];
        $item = $this->model->get($where);
        $item['posts_count'] = $this->postsModel->countPosts(['intPost_series_ID' => $item['series_ID']]);
        echo json_encode(['status' => true, 'data' => $item]);
        die();
    }
    public function ajax_getItems() {
        $where = isset($_POST['where']) ? $_POST['where'] : [];
        $rows = $this->model->getRows($where);
        echo json_encode(['status' => true, 'data' => $rows]);
        die();
    }
    public function ajax_update() {
        $input = input();
        $sets = $input['sets'];
        $where = $input['where'];
        $this->update($where, $sets);
        $series = $this->model->get($where);
        echo json_encode(['status' => true, 'data' => $series]);
        die();
    }
    public function index() {
        $series = $this->model->getPurchasedSeries();
        echo json_encode(['status' => true, 'data' => $series]);
        die();
    }
    public function ajax_join_pre() {
        $id = $_POST['id'];
        $series = $this->model->get($id);
        if ((int)$series['boolSeries_charge']) {
            $token = isset($_POST['token']) ? $_POST['token'] : 0;
            if ($token === '0' || !$token) {
                echo json_encode(['status' => false]);
            }
            else {
                $res = $this->charge($series, $token['id'], $series['intSeries_price'] * 100);
                if ($res) {
                    $sets = ['intPurchased_series_ID' => $id];
                    $newId = $this->purchasedModel->insert($sets);
                    echo json_encode(['status' => true, 'data' => $newId, 'charge' => $res]);
                }
                else {
                    echo json_encode(['status' => false, 'data' => $res]);
                }
            }
        }
        else {
            $sets = ['intPurchased_series_ID' => $id];
            $newId = $this->purchasedModel->insert($sets);
            echo json_encode(['status' => true, 'data' => $newId]);
        }
        die();
    }
    public function ajax_join() {
        $id = $_POST['id'];
        $series = $this->model->get($id);
        if ((int)$series['boolSeries_charge']) {
            $source = isset($_POST['source']) ? $_POST['source'] : 0;
            $email = isset($_POST['email']) ? $_POST['email'] : 0;
            if ($source === '0' || !$source || $email === '0' || !$email) {
                echo json_encode(['status' => false]);
            }
            else {
                $subscription = $this->subscribeToPlan($_SESSION['client_ID'], $email, $source, $series);
                if ($subscription) {
                    $sets = ['intPurchased_series_ID' => $id];
                    $newId = $this->purchasedModel->insert($sets);
                    echo json_encode(['status' => true, 'data' => $newId, 'subscription' => $subscription]);
                }
                else {
                    echo json_encode(['status' => false, 'error' => 'subscription error']);
                }
            }
        }
        else {
            $sets = ['intPurchased_series_ID' => $id];
            $newId = $this->purchasedModel->insert($sets);
            echo json_encode(['status' => true, 'data' => $newId]);
        }
        die();
    }
    public function ajax_delete() {
        $where = $_POST['where'];
        $this->delete($where);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_unJoin() {
        $id = $_POST['id'];
        $this->purchasedModel->delete($id);
        echo json_encode(['status' => true]);
        die();
    }
    public function get() {}
    public function charge($series, $token, $amount) {
        if (!is_array($series)){
            $series = $this->model->get($series);
        }
        $owner = $this->userModel->get($series['intSeries_client_ID']);
        $stripeAccountId = $owner['stripe_account_id'];
        if (!$stripeAccountId) { return false; }
        $stripeApi_sKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_sKey']);
        $stripeApi_sKey = $stripeApi_sKeyRow ? $stripeApi_sKeyRow['strOption_value'] : false;
        $stripeApi_sKey = \Helpers\jwtDecode($stripeApi_sKey);

        if ($series['intSeries_application_fee']) {
            $series['intSeries_application_fee'] = (int) $series['intSeries_application_fee'];
            $fee = (int) ($series['intSeries_application_fee'] * $amount / 100);
        }
        else {
            $feeRow = $this->optionsModel->get(['strOption_name' => 'series_application_fee']);
            $series['intSeries_application_fee'] = $feeRow ? (int) $feeRow['strOption_value'] : 5;
            $fee = (int) ($series['intSeries_application_fee'] * $amount / 100);
        }
        if (!$stripeApi_sKey) {
            return false;
        }
        \Stripe\Stripe::setApiKey($stripeApi_sKey);
        try {
            if (isset($_SESSION['introduced_affiliate']) && $owner['id'] != $_SESSION['client_ID'] && $_SESSION['introduced_affiliate'] != $_SESSION['affiliate_id'] && $owner['affiliate_id'] != $_SESSION['introduced_affiliate'] && (int) $series['boolSeries_affiliated']) {
                $charge = \Stripe\Charge::create([
                    "amount" => $amount,
                    "currency" => "usd",
                    "source" => $token,
                    "transfer_group" => "ORDER_95",
                ]);
                $sets = ['affiliate_id' => $_SESSION['introduced_affiliate'], 'user_id' => $_SESSION['client_ID'], 'action' => 'join', 'data' => $series['series_ID']];
                $this->affiliateTracksModel->insert($sets);
                $remainAmount = $amount - $fee - (int) ($amount * 2.9 / 100 + 30);
                $affiliatedUser = $this->userModel->get(['affiliate_id' => $_SESSION['introduced_affiliate']]);
                if ($affiliatedUser['stripe_account_id']) {
                    $affiliateAmount = (int)( $amount * $series['intSeries_affiliate_percent'] / 100 );
                    \Stripe\Transfer::create([
                        "amount" => $affiliateAmount,
                        "currency" => "usd",
                        "destination" => $affiliatedUser['stripe_account_id'],
                        "transfer_group" => "ORDER_95",
                    ]);
                    $remainAmount -= $affiliateAmount;
                }

//                series owner transfer
                \Stripe\Transfer::create([
                    "amount" => $remainAmount,
                    "currency" => "usd",
                    "destination" => $stripeAccountId,
                    "transfer_group" => "ORDER_95",
                ]);
                return $charge;
            }
            else {
                $charge = \Stripe\Charge::create([
                    "amount" => $amount,
                    "currency" => "usd",
                    "source" => $token,
                    "application_fee" => $fee,
                ], ["stripe_account" => $stripeAccountId]);

                return $charge;
            }
        }
        catch (Exception $e) {
            return false;
        }
    }
    public function ajax_search() {
        $keyword = $_POST['keyword'];
        $offset = isset($_POST['pageToken']) ? $_POST['pageToken'] : 0;
        $size = isset($_POST['size']) ? $_POST['size'] : 10;
        $where = isset($_POST['where']) ? $_POST['where'] : [];
        $rows = $this->search($keyword, $size, $offset, $where);
        foreach ($rows as & $row) {
            $this->searchFormat($row);
        }
        echo json_encode(['status' => true, 'data' => $rows, 'pageToken' => (int) $offset + count($rows)]);
        die();
    }
    public function ajax_invite() {
        $id = $_POST['id'];
        $emails = $_POST['emails'];

        $res = true;
        $invitations = [];
        foreach ($emails as $email) {
            $rlt = $this->invite($email, $id);
            if (!$rlt) {
                $res = false;
            }
            else{
                array_push($invitations, $rlt);
            }
        }
        echo json_encode(['status' => true, 'data' => $res ? $invitations : []]);
        die();
    }
    public function ajax_deleteOwner() {
        $where = $_POST['where'];
        $owners = $this->seriesOwnersModel->getRows($where);
        foreach ($owners as $owner) {
            $this->deleteOwner($owner);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_deleteInvitation() {
        $where = $_POST['where'];
        $invitations = $this->seriesInvitationsModel->getRows($where);
        foreach ($invitations as $invitation) {
            $this->deleteInvitation($invitation);
        }
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getOwners() {
        $id = $_POST['id'];
        $owners = $this->getOwners($id);
        echo json_encode(['status' => true, 'data' => $owners]);
        die();
    }
    public function ajax_acceptInvitation() {
        $invitationId = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        $invitation = $this->seriesInvitationsModel->get($invitationId);
        $this->seriesInvitationsModel->update($invitationId, ['strSeriesInvitation_status' => 'accept']);
        $sets = [
            'intSeriesOwner_user_ID' => $_SESSION['client_ID'],
            'intSeriesOwner_series_ID' => $invitation['intSeries_ID']
        ];
        $this->seriesOwnersModel->insert($sets);
        $this->purchasedModel->insert(['intPurchased_series_ID' => $sets['intSeriesOwner_series_ID']]);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_getMany() {
        $where = isset($_POST['where']) ? $_POST['where'] : [];
        $offset = isset($_POST['pageToken']) ? (int) $_POST['pageToken'] : 0;
        $size = isset($_POST['size']) ? (int) $_POST['size'] : 20;

        $defWhere = [
            'boolSeries_approved' => 1,
//            'boolSeries_isPublic' => 1,
        ];

        $where = array_merge($defWhere, $where);

        $rows = $this->model->getRows($where, '*');
        $pRows = [];
        foreach ($rows as & $row) {
            if ((int) $row['boolSeries_isPublic'] == 0) {
                if (!$this->getRole($row)) {
                    continue;
                }
            }
            if ($offset) {
                $offset--;
            }
            else{
                if ($size) {
                    $this->searchFormat($row);
                    array_push($pRows, $row);
                }
                else{
                    break;
                }
                $size--;
            }
        }
        \Helpers\utf8Encode($pRows);
        echo json_encode(['status' => true, 'data' => $pRows]);
        die();
    }
    public function ajax_joinMany() {
        $items = $_POST['items'];
        $token = isset($_POST['token']) ? $_POST['token'] : false;
        $p_ids = [];
        foreach ($items as $item) {
            $p_id = $this->join($item, $token);
            if (!$p_id) {
                echo json_encode(['status' => false]);
                die();
            }
            $p_ids[] = $p_id;
        }
        echo json_encode(['status' => true, 'data' => $p_ids]);
        die();
    }
    public function ajax_emailAddPosts() {
        $series = $_POST['series'];
        $posts = $_POST['posts'];
        $cnt = $this->sendEmailAddPostsToRelations($series, $posts);
        echo json_encode(['status' => true, 'data' => $cnt]);
        die();
    }
    public function ajax_createPlan() {
        $seriesId = $_POST['series_id'];
        $series = $this->model->get($seriesId);
        if ((int) $series['boolSeries_charge'] == 0) {
            echo json_encode(['status' => false, 'error' => 'This series is uncharged']);
            die();
        }
        if (is_null($series['strSeries_stripe_product'])) {
            $product = $this->stripeController->createProduct([
                'name' => $series['strSeries_title'],
                'type' => 'service',
            ]);
            $this->model->update($seriesId, ['strSeries_stripe_product' => $product['id']]);
            $series['strSeries_stripe_product'] = $product['id'];
        }
        $plan = $this->stripeController->createPlan([
            'currency' => 'usd',
            'interval' => 'month',
            'product' => $series['strSeries_stripe_product'],
            'amount' => (int) $series['intSeries_price'] * 100,
        ]);
        $sets = [
            'plan_ID' => $plan['id'],
            'strPlan_product_ID' => $series['strSeries_stripe_product'],
            'intPlan_series_ID' => $series['series_ID'],
            'strPlan_data' => json_encode($plan),
        ];
        $this->stripePlansModel->insert($sets);
        echo json_encode(['status' => true, 'data' => $sets]);
        die();
    }
    public function ajax_createCheckoutSession() {
        $id = $_POST['id'];
        $url = $_POST['url'];
        $series = $this->model->get($id);

        $stripeApi_sKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_sKey']);
        $stripeApi_sKey = $stripeApi_sKeyRow ? $stripeApi_sKeyRow['strOption_value'] : false;
        if (!$stripeApi_sKey) {
            echo json_encode(['error' => 'no key']);
            die();
        }
        $stripeApi_sKey = \Helpers\jwtDecode($stripeApi_sKey);
        $session = false;
        \Stripe\Stripe::setApiKey($stripeApi_sKey);
        switch ($series['strSeries_charge_type']) {
            case 'onetime':
                $session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [[
                        'name' => $series['strSeries_title'],
                        'description' => $series['strSeries_description'],
                        'images' => [\Helpers\absUrl($series['strSeries_image'])],
                        'amount' => $series['intSeries_price'] * 100,
                        'currency' => 'usd',
                        'quantity' => 1,
                    ]],
                    'success_url' => $url,
                    'cancel_url' => $url
                ]);
                break;
            case 'monthly':
                $plan = $this->stripePlansModel->value(['strPlan_product_ID' => $series['strSeries_stripe_product']], 'plan_ID');
                $session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'subscription_data' => [
                        'items' => [[
                            'plan' => $plan,
                        ]],
                    ],
                    'success_url' => $url,
                    'cancel_url' => $url
                ]);
                break;
            default:
                break;
        }
        if ($session) {
            $this->checkoutSessionSeriesModel->insert([
                'user_id' => $_SESSION['client_ID'],
                'series' => (string) $series['series_ID'],
                'session_id' => $session['id'],
            ]);
            echo json_encode(['data' => $session, 'series' => $series]);
        } else {
            echo json_encode(['error' => 'there is no data needed']);
        }
        die();
    }
}

$hdl = new Series();

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'get':
            $hdl->ajax_get();
            break;
        case 'get_many':
            $hdl->ajax_getMany();
            break;
        case 'insert':
            $hdl->ajax_insert();
            break;
        case 'insert_auto_draft':
            $hdl->ajax_insertAutoDraft();
            break;
        case 'update':
            $hdl->ajax_update();
            break;
        case 'delete':
            $hdl->ajax_delete();
            break;
        case 'join':
            $hdl->ajax_join();
            break;
        case 'unJoin':
            $hdl->ajax_unJoin();
            break;
        case 'get_items':
            $hdl->ajax_getItems();
            break;
        case 'invite':
            $hdl->ajax_invite();
            break;
        case 'delete_owner':
            $hdl->ajax_deleteOwner();
            break;
        case 'delete_invitation':
            $hdl->ajax_deleteInvitation();
            break;
        case 'search':
            $hdl->ajax_search();
            break;
        case 'get_owners':
            $hdl->ajax_getOwners();
            break;
        case 'accept_invitation':
            $hdl->ajax_acceptInvitation();
            break;
        case 'join_many':
            $hdl->ajax_joinMany();
            break;
        case 'email_add_posts':
            $hdl->ajax_emailAddPosts();
            break;
        case 'create_plan':
            $hdl->ajax_createPlan();
            break;
        case 'create_checkout_session':
            $hdl->ajax_createCheckoutSession();
            break;
        default:
            $hdl->index();
            break;
    }
}
else{
    $hdl->index();
}
