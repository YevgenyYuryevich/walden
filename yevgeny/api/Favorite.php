<?php

require_once '../core/Controller_core.php';
require_once '../../guardian/class/userauth.class.php';

class Favorite extends \Core\Controller_core
{
    private $model;
    private $isLoggedIn;
    private $postsModel;
    private $seriesModel;
    private $purchasedModel;
    private $subscriptionModel;
    public function __construct()
    {
        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        parent::__construct();
        $this->load->model('api_m/Favorite_m');
        $this->model = new \Models\api\Favorite_m();

        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();

        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Subscriptions_m');
        $this->subscriptionModel = new \Models\api\Subscriptions_m();
    }
    public function ajax_insert() {
        $sets = $_POST['sets'];
        if ($_SESSION['client_ID'] === -1) {
            echo json_encode(['error' => 'access denied']);
            die(403);
        }
        $sets['intFavorite_client_ID'] = $_SESSION['client_ID'];
        if (!isset($sets['intFavorite_subscription_ID'])) {
            $post = $this->postsModel->get($sets['intFavorite_post_ID']);
            $sets['intFavorite_series_ID'] = $post['intPost_series_ID'];
            $series = $this->seriesModel->get($post['intPost_series_ID']);
            $purchased = $this->purchasedModel->get([
                'intPurchased_client_ID' => $_SESSION['client_ID'],
                'intPurchased_series_ID' => $series['series_ID']
            ]);
            if ($purchased) {
                $subscription = $this->subscriptionModel->get([
                    'intClientSubscription_post_ID' => $post['post_ID'],
                    'intClientSubscription_purchased_ID' => $purchased['purchased_ID'],
                ]);
                if ($subscription) {
                    $sets['intFavorite_subscription_ID'] = $subscription['clientsubscription_ID'];
                }
            }
        }
        if (!isset($sets['intFavorite_post_ID'])) {
            $subscription = $this->subscriptionModel->get($sets['intFavorite_subscription_ID']);
            if ($subscription['intClientSubscription_post_ID']) {
                $sets['intFavorite_post_ID'] = $subscription['intClientSubscription_post_ID'];
            }
        }
        $id = $this->model->insert($sets);
        $row = $this->model->get($id);
        echo json_encode($row);
        die();
    }
    public function ajax_delete() {
        if ($_SESSION['client_ID'] === -1) {
            echo json_encode(['error' => 'access denied']);
            die(403);
        }
        $where = $_POST['where'];
        $this->model->delete($where);
        echo json_encode($where);
        die();
    }
}

$handle = new Favorite();
if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'insert':
            $handle->ajax_insert();
            break;
        case 'delete':
            $handle->ajax_delete();
            break;
        default:
            break;
    }
} else {

}
