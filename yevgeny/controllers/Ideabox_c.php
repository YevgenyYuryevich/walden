<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 22.03.2018
 * Time: 22:32
 */

namespace Controllers;

require_once __DIR__ . '/../models/api_m/Ideabox_m.php';
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../options/Unsplash.php';
require_once __DIR__ . '/../models/api_m/ClientMeta_m.php';
require_once __DIR__ . '/../helpers/funcs.php';

use function Helpers\htmlMailHeader;
use Models\api\Ideabox_m;
use Crew\Unsplash\Photo;
use Crew\Unsplash\Search;
use Models\api\ClientMeta_m;
use function Helpers\aOrb;

class Ideabox_c
{

    public function __construct()
    {
        global $unsplashOptions;
        $this->model = new Ideabox_m();
        $this->clientMetaModel = new ClientMeta_m();
        \Crew\Unsplash\HttpClient::init($unsplashOptions);
        $this->unsplash = new Photo();
        $this->unsplashSearch = new Search();
        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';
    }
    public function insert($sets){
        if (isset($sets['strIdeaBox_image'])){
            $image_file = $sets['strIdeaBox_image'];
            $unique_filename = uniqid('ideabox_image').'_' . $image_file['name'];
            $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
            move_uploaded_file($image_file['tmp_name'], $destination);
            $sets['strIdeaBox_image'] = 'assets/images/' . $unique_filename;
        }
        else{
            $t = explode(' ', trim($sets['strIdeaBox_title']));
            $apiImg = $this->getPhotoFromUnsplash($t[0], 'ideabox');
            $sets['strIdeaBox_image'] = $apiImg['savedUrl'];
        }
        $id = $this->model->insert($sets);
        return $id;
    }
    public function update($sets, $where){
        if (isset($sets['strIdeaBox_image'])){
            $image_file = $sets['strIdeaBox_image'];
            $unique_filename = uniqid('ideabox_image').'_' . $image_file['name'];
            $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
            move_uploaded_file($image_file['tmp_name'], $destination);
            $sets['strIdeaBox_image'] = 'assets/images/' . $unique_filename;
        }
        if (isset($sets['dtIdeaBox_deadline']) && $sets['dtIdeaBox_deadline']){
            $sets['dtIdeaBox_deadline'] = date("Y-m-d 00:00:00", strtotime($sets['dtIdeaBox_deadline']));
        }
        return $this->model->update($sets, $where);
    }
    public function getPhotoFromUnsplash($query, $for){
        $filter = ['query' => $query, 'w' => 300];
        $unsplashPhoto = $this->unsplash::random($filter);
        if ($unsplashPhoto->errors){
            return ['savedUrl' => $this->defaultPhoto, 'originUrl' => $this->defaultPhoto];
        }
        $uPhotoUrl = $unsplashPhoto->urls['small'];
        preg_match('/fm=([^\&]*)/', $uPhotoUrl, $matches);

        $savedUrl = $this->putContents($uPhotoUrl, $for, $query, $matches[1]);
        return ['savedUrl' => $savedUrl, 'originUrl' => $uPhotoUrl];
    }
    public function putContents($orgUrl, $prefix, $name, $extends){
        $name = preg_replace('/[\?\/\#]/', '-', $name);
        $uniqueFilename = uniqid(str_replace(' ', '_', $prefix)).'_' . urlencode($name) . '.' . $extends;
        $savedUrl = 'assets/images/' . $uniqueFilename;
        file_put_contents(_ROOTPATH_ . '/' . $savedUrl, fopen($orgUrl, 'r'));
        return $savedUrl;
    }
    public function formatIdeabox($ideabox){
        if($ideabox['dtIdeaBox_deadline'] === null){
            $ideabox['dtIdeaBox_deadline'] = false;
        }
        else{
            $ideabox['dtIdeaBox_deadline'] = date("m/d/Y", strtotime($ideabox['dtIdeaBox_deadline']));
        }
        $ideabox['strIdeaBox_reference_link'] = $ideabox['strIdeaBox_reference_link'] == null ? '' : $ideabox['strIdeaBox_reference_link'];

        $ideabox['strIdeaBox_reference_text'] = $ideabox['strIdeaBox_reference_text'] == null ? '' : $ideabox['strIdeaBox_reference_text'];
        $ideabox['next_deadline'] = -1;
        $ideabox['strIdeaBox_image'] = aOrb($ideabox['strIdeaBox_image'], $this->defaultPhoto);
        return $ideabox;
    }
}