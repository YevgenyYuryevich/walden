<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 14.03.2018
 * Time: 02:09
 */

namespace Controllers;

use Core\Controller_core;
use function Helpers\htmlMailHeader;
use function Helpers\parseStringToArray;
use function Helpers\putContents;
use function Helpers\uploadFile;
use function Helpers\utf8Encode;
use Library\Unsplash_lb;
use Models\api\StripePlans_m;
use Models\api\StripeSubscriptions_m;
use Models\api\UnReadPosts_m;
use Models\api\Purchased_m;
use Models\api\Series_m;

require_once __DIR__ . '/../core/Controller_core.php';
require_once __DIR__ . '/../models/api_m/Series_m.php';
require_once __DIR__ . '/../controllers/Posts_c.php';
require_once __DIR__ . '/../models/api_m/Purchased_m.php';

class Series_c extends Controller_core
{
    public $model;
    public $purchasedModel;
    public $postsModel;
    public $userHandle;
    public $userModel;
    public $postsHandle;
    public $formFieldHandle;
    public $optionsModel;
    public $categoryModel;
    public $seriesInvitationsModel;
    public $seriesOwnersModel;
    public $unReadPosts_m;
    public $clientSeriesViewModel;
    public $stripePlansModel;
    public $stripeSubscriptionsModel;
    public $unsplashLib;
    public $stripeController;
    public $options;
    public function __construct()
    {
        parent::__construct();
        $this->model = new Series_m();
        $this->purchasedModel = new Purchased_m();
        $this->postsHandle = new Posts_c();
        $this->postsModel = $this->postsHandle->model;

        $this->load->library('Unsplash_lb');
        $this->unsplashLib = new Unsplash_lb();

        $this->load->controller('User_c');
        $this->userHandle = new User_c();
        $this->userModel = $this->userHandle->model;
        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();

        $this->load->model('api_m/Series_Invitations_m');
        $this->seriesInvitationsModel = new \Models\api\Series_Invitations_m();

        $this->load->model('api_m/Series_Owners_m');
        $this->seriesOwnersModel = new \Models\api\Series_Owners_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->controller('FormFields_c');
        $this->formFieldHandle = new FormFields_c();

        $this->load->model('api_m/UnReadPosts_m');
        $this->unReadPosts_m = new UnReadPosts_m();

        $this->load->model('api_m/ClientSeriesView_m');
        $this->clientSeriesViewModel = new \Models\api\ClientSeriesView_m();

        $this->load->model('api_m/StripePlans_m');
        $this->stripePlansModel = new StripePlans_m();

        $this->load->model('api_m/StripeSubscriptions_m');
        $this->stripeSubscriptionsModel = new StripeSubscriptions_m();

        $this->load->controller('Stripe_c');
        $this->stripeController = new Stripe_c();
        $this->getOptions();
    }
    public function delete($where){
        $series = $this->model->getSeries($where, 'series_ID, intSeries_client_ID');
        foreach ($series as $sery){
            if ((int) $sery['intSeries_client_ID'] !== (int) $_SESSION['client_ID']) {
                return false;
            }
            $this->postsHandle->delete(['intPost_series_ID' => $sery['series_ID']]);
            $this->purchasedModel->delete(['intPurchased_series_ID' => $sery['series_ID']]);
            $this->formFieldHandle->delete(['intFormField_series_ID' => $sery['series_ID']]);
        }
        return $this->model->delete($where);
    }
    public function invite($email, $seriesId, $owner = false) {
        if (!$owner) {
            $owner = $_SESSION['guardian'];
        }
        $series = $this->model->get($seriesId);
        $series['posts_count'] = $this->postsModel->countPosts(['intPost_series_ID' => $series['series_ID']]);
        $waitingRow = $this->seriesInvitationsModel->get(['intSeries_ID' => $seriesId, 'strInvitation_email' => $email, 'strSeriesInvitation_status' => 'waiting']);
        if ($waitingRow) {
            $id = $waitingRow['series_invitation_ID'];
        }
        else{
            $id = $this->seriesInvitationsModel->insert(['intSeries_ID' => $seriesId, 'strInvitation_email' => $email]);
        }
        $header = htmlMailHeader();
        $posts = $this->postsHandle->getSerialItems(['intPost_series_ID' => $seriesId, 'intPost_parent' => 0], 'post_ID, strPost_featuredimage, intPost_parent, intPost_order, strPost_title, strPost_duration, intPost_type', ['intPost_order', 'post_ID'], 3);
        $posts = utf8Encode($posts);
        $series = utf8Encode($series);
        $params = [
            'id' => $id,
            'series_id' => $seriesId,
            'mail' => $owner['email'],
            'series_title_short' => strlen($series['strSeries_title']) > 35 ? substr($series['strSeries_title'], 0, 35) . '...' : $series['strSeries_title'],
            'series_title' => $series['strSeries_title'],
            'name' => $owner['f_name'],
            'posts' => $posts,
            'posts_count' => $series['posts_count']
        ];
        $msg = file_get_contents(BASE_URL . '/assets/email-templates/experience_invited.php?' . http_build_query($params),"UTF-8");
        mail($email, $owner['f_name'] . ' has sent you an invitation on Walden.ly', $msg, $header);
        $row = $this->seriesInvitationsModel->get($id);
        $row['user'] = $this->userHandle->get(['email' => $email]);
        return $row;
    }
    public function deleteOwner($owner) {
        $this->seriesOwnersModel->delete($owner['seriesOwner_ID']);
        return true;
    }
    public function deleteInvitation($invitation) {
        $this->seriesInvitationsModel->delete($invitation['series_invitation_ID']);
        return true;
    }
    public function insert($sets){
        if (isset($sets['strSeries_image'])){}
        elseif (isset($_FILES['strSeries_image'])){
            $uploadRlt = uploadFile($_FILES['strSeries_image'], ['prefix' => 'series']);
            $sets['strSeries_image'] = $uploadRlt['url'];
        }
        else{
            $url = $this->unsplashLib->random('rand');
            $ext = $this->unsplashLib->getExtensionFromUrl($url);
            $savedUrl = putContents($url, 'series.' . $ext, 'assets/images', 'unsplash');
            $sets['strSeries_image'] = $savedUrl;
        }

        $option = $this->optionsModel->get(['strOption_name' => 'series_auto_approve']);
        $defaultSets = [];
        $defaultSets['boolSeries_approved'] = $option ? $option['strOption_value'] : 0;

        $sets = array_merge($defaultSets, $sets);
        $id = $this->model->insert($sets);
        $this->createPlan($id);
        return $id;
    }
    public function update($where, $sets) {
        $series = $this->model->get($where);
        $currentPrice = $series['intSeries_price'];
        if (isset($_FILES['strSeries_image'])){
            $uploadRlt = uploadFile($_FILES['strSeries_image'], ['prefix' => 'series']);
            $sets['strSeries_image'] = $uploadRlt['url'];
        }
        if (isset($sets['boolSeries_approved']) && $sets['boolSeries_approved'] == 'server_control'){
            $option = $this->optionsModel->get(['strOption_name' => 'series_auto_approve']);
            $sets['boolSeries_approved'] = $option ? $option['strOption_value'] : 0;
        }
        $rtn = $this->model->update($where, $sets);
        if (isset($sets['intSeries_price']) && ((int) $currentPrice !== (int) $sets['intSeries_price'])) {
            $series = array_merge($series, $sets);
            $this->updatePlan($series);
        }
        return $rtn;
    }
    public function search($keyword = '*', $size = 10, $offset = 0, $where = []) {
        $searchedSeries = [];
        $series = $this->getAvailableRows($where);
        foreach ($series as $sery) {
            if ($this->model->isSearched($keyword, ['series_ID' => $sery['series_ID']])) {
                array_push($searchedSeries, $sery);
            }
            elseif ($this->postsHandle->isSearched($keyword, ['intPost_series_ID' => $sery['series_ID']])) {
                array_push($searchedSeries, $sery);
            }
        }
        return array_slice($searchedSeries, $offset, $size);
    }
    public function searchFormat(& $row) {
        $row = \Helpers\utf8Encode($row);
        $row['posts_count'] = $this->postsModel->countPosts(['intPost_series_ID' => $row['series_ID']]);

        $owner = $this->userHandle->get($row['intSeries_client_ID']);
        $row['stripe_account_id'] = $owner['stripe_account_id'];

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $row['series_ID']]);
        $row['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;

        $row['category'] = $this->categoryModel->get($row['intSeries_category']);
        return $row;
    }
    public function getOwners($id) {
        $series = $this->model->get($id);
        $creator = $this->userHandle->get($series['intSeries_client_ID']);
        $owners = [$creator];
        $sOwners = $this->seriesOwnersModel->getRows(['intSeriesOwner_series_ID' => $id]);
        foreach ($sOwners as $sOwner) {
            $o = $this->userHandle->get($sOwner['intSeriesOwner_user_ID']);
            $owners[] = $o;
        }
        foreach ($owners as &$owner) {
            $ownerSeries = $this->model->getRows(['intSeries_client_ID' => $owner['id']]);
            foreach ($ownerSeries as &$ownerSery) {
                $ownerSery = $this->searchFormat($ownerSery);
            }
            $owner['series'] = $ownerSeries;
        }
        return $owners;
    }
    public function getAvailableRows($where = [], $selects = '*', $orderBy = [], $limit = false) {
        $rows = $this->model->getRows($where, $selects, $orderBy, $limit);
        $availableRows = [];
        foreach ($rows as $row) {
            if ($this->isAvailable($row)) {
                $availableRows[] = $row;
            }
        }
        return $availableRows;
    }
    public function getAccessibleRows($where = [], $selects = '*', $orderBy = [], $limit = false) {
        $rows = $this->model->getRows($where, $selects, $orderBy, $limit);
        $availableRows = [];
        foreach ($rows as $row) {
            if ($this->isAccessible($row)) {
                $availableRows[] = $row;
            }
        }
        return $availableRows;
    }
    public function isAccessible($row) {
        $row['boolSeries_isPublic'] = (int) $row['boolSeries_isPublic'];
        $row['boolSeries_approved'] = (int) $row['boolSeries_approved'];

        if ($row['boolSeries_isPublic'] && $row['boolSeries_approved']) {
            return true;
        }
        $role = $this->getRole($row);
        return $role ? true : false;
    }
    public function isAvailable( $row ) {
        $row['boolSeries_isPublic'] = (int) $row['boolSeries_isPublic'];
        $row['boolSeries_approved'] = (int) $row['boolSeries_approved'];

        if (!$this->options['allow_unApprovedUser_create_publicSeries']) {
            $creator = $this->userModel->get($row['intSeries_client_ID']);
            if (!((int) $creator['active'])) {
                return false;
            }
        }

        if ($row['boolSeries_isPublic'] && $row['boolSeries_approved']) {
            return true;
        }
        if (!$row['boolSeries_approved']){
            return false;
        }
        if (!$row['boolSeries_isPublic']) {
            $role = $this->getRole($row);
            return $role ? true : false;
        }
        return true;
    }
    public function getRole($row) {
        if ($row['intSeries_client_ID'] == $_SESSION['client_ID']) {
            return 1;
        }
        $ownerRow = $this->seriesOwnersModel->get(['intSeriesOwner_user_ID' => $_SESSION['client_ID'], 'intSeriesOwner_series_ID' => $row['series_ID']]);
        return $ownerRow ? (int) $ownerRow['intSeriesOwner_role'] : 0;
    }
    public function join($id, $token = false) {
        $series = $this->model->get($id);
        if ((int)$series['boolSeries_charge']) {
            if ($token === '0' || !$token) {
                return false;
            }
            else {
                $res = $this->charge($series, $token['id'], $series['intSeries_price'] * 100);
                if ($res) {
                    $sets = ['intPurchased_series_ID' => $id];
                    $newId = $this->purchasedModel->insert($sets);
                    return $newId;
                }
                else {
                    return false;
                }
            }
        }
        else {
            $sets = ['intPurchased_series_ID' => $id];
            $newId = $this->purchasedModel->insert($sets);
            return $newId;
        }
    }
    public function sendEmailAddPostsToRelations($series, $postIds) {
        if (!is_array($series)) {
            $series = $this->model->get($series);
        }
        $posts = [];
        foreach ($postIds as $key => $id) {
            if ($key > 2) {
                break;
            }
            $post = $this->postsModel->get($id);
            $post['viewDay'] = $this->postsHandle->calcDay($post);
            $posts[] = $post;
        }
        $posts = utf8Encode($posts);
        $series = utf8Encode($series);
        $owner = $_SESSION['guardian'];
        $params = [
            'series_id' => $series['series_ID'],
            'series_title_short' => strlen($series['strSeries_title']) > 35 ? substr($series['strSeries_title'], 0, 35) . '...' : $series['strSeries_title'],
            'series_title' => $series['strSeries_title'],
            'name' => $owner['f_name'],
            'posts' => $posts,
            'posts_count' => count($postIds),
        ];
        $msg = file_get_contents(BASE_URL . '/assets/email-templates/email_add_posts.php?' . http_build_query($params),"UTF-8");
        $owners = $this->getOwners($series['series_ID']);
        $header = htmlMailHeader();
        $userFlg = [$owner['id'] => true];
        foreach ($owners as $u) {
            if ($u['id'] == $owner['id']) {
                continue;
            }
            $userFlg[$u['id']] = true;
            mail($u['email'], $owner['f_name'] . ' has added new experiences', $msg, $header);
        }
        $purchases = $this->purchasedModel->getRows(['intPurchased_series_ID' => $series['series_ID']]);
        foreach ($purchases as $purchase) {
            if (isset($userFlg[$purchase['intPurchased_client_ID']])) {
                continue;
            }
            $userFlg[$purchase['intPurchased_client_ID']] = true;
            $u = $this->userHandle->get($purchase['intPurchased_client_ID']);
            mail($u['email'], $owner['f_name'] . ' has added new experiences', $msg, $header);
        }
        return (count($userFlg) - 1);
    }
    public function unReadPostsCount($series) {
        return $this->unReadPosts_m->count(['user_id' => $_SESSION['client_ID'], 'series_id' => $series]);
    }
    public function unReadPosts($series) {
        $news = $this->unReadPosts_m->getRows(['user_id' => $_SESSION['client_ID'], 'series_id' => $series]);
        $posts = [];
        foreach ($news as $n) {
            $posts[] = $this->postsModel->get($n['post_id']);
        }
        return $posts;
    }
    public function filterViewRecord($id) {
        if (!isset($_SESSION['series_logout_views'])) {
            $_SESSION['series_logout_views'] = [];
        }
        $myLogoutViews = $_SESSION['series_logout_views'];
        $series = $this->model->get($id);
        $seriesLogoutViews = (int) $series['intSeries_logout_views'];
        if ( (int) $_SESSION['client_ID'] > 0 ) {
            if (in_array($id, $myLogoutViews)) {
                $this->model->update($id, ['intSeries_logout_views' => --$seriesLogoutViews]);
                $key = array_search($id, $myLogoutViews);
                unset($_SESSION['series_logout_views'][$key]);
            }
            $seriesViewRow = $this->clientSeriesViewModel->get(['intCSView_client_ID' => $_SESSION['client_ID'], 'intCSView_series_ID' => $id]);
            if ( $seriesViewRow ) {

            }
            else {
                $this->clientSeriesViewModel->insert([
                   'intCSView_client_ID' => $_SESSION['client_ID'],
                    'intCSView_series_ID' => $id
                ]);
            }
        }
        else {
            if (in_array($id, $myLogoutViews)) {
            }
            else {
                $this->model->update($id, ['intSeries_logout_views' => ++$seriesLogoutViews]);
                $_SESSION['series_logout_views'][] = $id;
            }
        }
        $loginViews = $this->clientSeriesViewModel->count(['intCSView_series_ID' => $id]);
        return $loginViews + $seriesLogoutViews;
    }
    public function filterAnyViewRecord($id) {
        $series = $this->model->get($id);
        $av = (int) $series['intSeries_any_views'];
        $this->model->update($id, ['intSeries_any_views' => ++$av]);
        if ( (int) $_SESSION['client_ID'] > 0 ){
            $seriesViewRow = $this->clientSeriesViewModel->get(['intCSView_client_ID' => $_SESSION['client_ID'], 'intCSView_series_ID' => $id]);
            if ( $seriesViewRow ) {
                $cSView_views = (int) $seriesViewRow['intCSView_views'];
                $this->clientSeriesViewModel->update($seriesViewRow['clientSeriesView_ID'], ['intCSView_views' => ++$cSView_views]);
            }
            else {
                $this->clientSeriesViewModel->insert([
                    'intCSView_client_ID' => $_SESSION['client_ID'],
                    'intCSView_series_ID' => $id
                ]);
            }
        }
        return $av;
    }
    public function subscribeToPlan($who, $email, $source, $series) {
        $stripeProduct = $series['strSeries_stripe_product'];
        $plan = $this->stripePlansModel->value(['strPlan_product_ID' => $stripeProduct], 'plan_ID');
        $customer = $this->stripeController->createCustomer([
            'email' => $email,
            'source' => $source
        ]);
        $subscription = $this->stripeController->createSubscription([
            'customer' => $customer['id'],
            'items' => [
                ['plan' => $plan],
            ]
        ]);
        $sets = [
            'subscription_ID' => $subscription['id'],
            'strSubscription_plan_ID' => $plan,
            'intSubscription_series_ID' => $series['series_ID'],
            'intSubscription_user_ID' => $who,
        ];
        $owner = $this->userModel->get($series['intSeries_client_ID']);
        if ((int) $series['boolSeries_affiliated'] && isset($_SESSION['introduced_affiliate']) && isset($_SESSION['affiliate_id']) && $_SESSION['introduced_affiliate'] != $_SESSION['affiliate_id'] && $owner['affiliate_id'] != $_SESSION['introduced_affiliate']) {
            $sets['strSubscription_affiliate'] = $_SESSION['introduced_affiliate'];
        }
        $this->stripeSubscriptionsModel->insert($sets);
        return $subscription;
    }
    public function createPlan($series) {
        if (!is_array($series)) {
            $series = $this->model->get($series);
        }
        if ((int) $series['boolSeries_charge'] == 0 || !$series['intSeries_price']) {
            return false;
        }
        if (is_null($series['strSeries_stripe_product'])) {
            if (!$series['strSeries_title'] || is_null($series['strSeries_title'])) {
                return false;
            }
            $product = $this->stripeController->createProduct([
                'name' => $series['strSeries_title'],
                'type' => 'service',
            ]);
            $this->model->update($series['series_ID'], ['strSeries_stripe_product' => $product['id']]);
            $series['strSeries_stripe_product'] = $product['id'];
        }
        $row = $this->stripePlansModel->get(['strPlan_product_ID' => $series['strSeries_stripe_product'], 'intPlan_series_ID' => $series['series_ID']]);
        if ($row) {
//            return parseStringToArray($row['strPlan_data']);
            return 'already created';
        }
        $plan = $this->stripeController->createPlan([
            'currency' => 'usd',
            'interval' => 'month',
            'product' => $series['strSeries_stripe_product'],
            'amount' => (int) $series['intSeries_price'] * 100,
        ]);
        $sets = [
            'plan_ID' => $plan['id'],
            'strPlan_product_ID' => $series['strSeries_stripe_product'],
            'intPlan_series_ID' => $series['series_ID'],
            'strPlan_data' => json_encode($plan),
        ];
        $this->stripePlansModel->insert($sets);
        return $plan;
    }
    public function updatePlan($series) {
        if (!is_array($series)) {
            $series = $this->model->get($series);
        }
        if ((int) $series['boolSeries_charge'] == 0 || !$series['intSeries_price']) {
            return false;
        }
        if (is_null($series['strSeries_stripe_product'])) {
            if (!$series['strSeries_title'] || is_null($series['strSeries_title'])) {
                return false;
            }
            $product = $this->stripeController->createProduct([
                'name' => $series['strSeries_title'],
                'type' => 'service',
            ]);
            $this->model->update($series['series_ID'], ['strSeries_stripe_product' => $product['id']]);
            $series['strSeries_stripe_product'] = $product['id'];
        }
        $row = $this->stripePlansModel->get(['strPlan_product_ID' => $series['strSeries_stripe_product'], 'intPlan_series_ID' => $series['series_ID']]);
        if ($row) {
            $this->stripePlansModel->delete(['strPlan_product_ID' => $series['strSeries_stripe_product'], 'intPlan_series_ID' => $series['series_ID']]);
        }
        $plan = $this->stripeController->createPlan([
            'currency' => 'usd',
            'interval' => 'month',
            'product' => $series['strSeries_stripe_product'],
            'amount' => (int) $series['intSeries_price'] * 100,
        ]);
        $sets = [
            'plan_ID' => $plan['id'],
            'strPlan_product_ID' => $series['strSeries_stripe_product'],
            'intPlan_series_ID' => $series['series_ID'],
            'strPlan_data' => json_encode($plan),
        ];
        $this->stripePlansModel->insert($sets);
        return $plan;
    }
    public function getOptions() {
        $allow_unApprovedUser_create_publicSeries = $this->optionsModel->getOption('allow_unApprovedUser_create_publicSeries');
        $allow_unApprovedUser_create_publicSeries = $allow_unApprovedUser_create_publicSeries !== 'undefined' ? (int) $allow_unApprovedUser_create_publicSeries : 0;
        $this->options['allow_unApprovedUser_create_publicSeries'] = $allow_unApprovedUser_create_publicSeries;
        return $this->options;
    }
}
