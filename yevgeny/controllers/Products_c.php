<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.03.2018
 * Time: 16:25
 */

namespace Controllers;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../options/AmazonOptions.php';
require_once __DIR__ . '/../models/Products_m.php';

use MarcL\AmazonUrlBuilder;
use MarcL\AmazonAPI;
use Models\Products_m;

class Products_c
{
    public function __construct()
    {
        global $amazonOptions;
        $urlBuilder = new AmazonUrlBuilder(
            $amazonOptions['keyId'],
            $amazonOptions['secretKey'],
            $amazonOptions['associateId'],
            'us'
        );
        $this->amazonAPI = new AmazonAPI($urlBuilder, 'array');
        $this->model = new Products_m();
    }
    public function index(){}
    public function getProductInfoFromAmazonByAsin($asin, $limit_time = 10){
        $response_from_amazon = $this->amazonAPI->ItemLookUp($asin, ['EditorialReview', 'ItemAttributes', 'OfferSummary', 'Images'], true);

        if (isset($response_from_amazon->Items)){
            $data = $response_from_amazon->Items->Item;
            return [
                'product_title' => $data->ItemAttributes->Title,
                'product_description' => $data->EditorialReviews->EditorialReview->Content,
                'product_image' => $data->MediumImage->URL,
                'product_large_image' => $data->LargeImage->URL,
                'product_asin' => $asin,
                'new_quantity' => $data->OfferSummary->TotalNew,
                'new_price' => isset($data->OfferSummary->LowestNewPrice) ? $data->OfferSummary->LowestNewPrice->Amount : 0,
                'used_quantity' => $data->OfferSummary->TotalUsed,
                'used_price' => isset($data->OfferSummary->LowestUsedPrice) ? $data->OfferSummary->LowestUsedPrice->Amount : 0,
                'product_amazon_url' => $data->DetailPageURL
            ];
        }
        else{
            if ($limit_time == 0){
                return false;
            }
            return $this->getProductInfoFromAmazonByAsin($asin, $limit_time - 1);
        }
    }
    public function createNewProductByAsin($asin = false){
        if (!$asin){
            $asin = $_POST['asin'];
        }

        $product_from_db = $this->model->getProductByAsin($asin);
        $product_id = false;

        $response_from_amazon = $this->getProductInfoFromAmazonByAsin($asin);

        if ($response_from_amazon){
            $product_title = $response_from_amazon['product_title'];
            $product_description = $response_from_amazon['product_description'];
            $product_image = $response_from_amazon['product_image'];
            $product_large_image = $response_from_amazon['product_large_image'];
            $product_asin = $asin;
            $new_price = $response_from_amazon['new_price'];
            $new_quantity = $response_from_amazon['new_quantity'];
            $used_quantity = $response_from_amazon['used_quantity'];
            $used_price = $response_from_amazon['used_price'];
            $product_amazon_url = $response_from_amazon['product_amazon_url'];
        }
        else{
            echo json_encode(array('status' => false, 'data' => 'no item'));
            die();
        }

        if ($product_from_db !== false){
            $product_id = $product_from_db['product_ID'];
        }
        else{
            $product_id = $this->model->insertProduct($product_title, $product_description, $product_image, $product_large_image, $product_asin, $product_amazon_url);
        }
        if (!$product_id){
            echo json_encode(array('status' => false, 'not insert product'));
            die();
        }
        $product_user = $this->model->getProductUser($product_id);
        if (!$product_user){
            $this->model->insertProductUser($product_id);
        }
        $this->insertProductPriceWithValidate($product_id, $new_price, $new_quantity, $used_price, $used_quantity);

        $res_data = $this->getFormatDataByProductId($product_id);
        return $res_data;
    }
    public function insertProductPriceWithValidate($product_id, $new_price, $new_quantity, $used_price, $used_quantity){
        $latest_row = $this->model->getLatestProductPrice($product_id);
        if ($latest_row){
            $new_price = (int)$new_quantity > 0 ? $new_price : ((int)$new_price > 0 ? $new_price : $latest_row['intProductPrice_newprice']);
            $used_price = (int)$used_quantity > 0 ? $used_price : ((int)$used_price > 0 ? $used_price : $latest_row['intProductPrice_usedprice']);
        }
        else{
            $new_price = (int)$new_quantity > 0 ? $new_price : ((int)$new_price > 0 ? $new_price : $used_price);
            $used_price = (int)$used_quantity > 0 ? $used_price : ((int)$used_price > 0 ? $used_price : $new_price);
        }
        return $this->model->insertProductPrice($product_id, $new_price, $new_quantity, $used_price, $used_quantity);
    }
    public function getFormatDataByProductId($product_id){
        $product_row = $this->model->getProductRowById($product_id);
        if (!$product_row){ return false; }
        $product_row['price_rows'] = $this->model->getProductPricesByProductId($product_id);
        $product_row['recommendation'] = $this->getRecommendation($product_row['price_rows']);
        $product_row['strProduct_large_img'] = $product_row['strProduct_large_img'] != null ? $product_row['strProduct_large_img'] : $product_row['strProduct_image'];

        foreach ($product_row['price_rows'] as & $price_row){
            $price_row['intProductPrice_newprice'] = number_format((float)$price_row['intProductPrice_newprice'], 2, '.', '');
            $price_row['intProductPrice_newquantity'] = number_format((float)$price_row['intProductPrice_newquantity'], 2, '.', '');
            $price_row['intProductPrice_usedprice'] = number_format((float)$price_row['intProductPrice_usedprice'], 2, '.', '');
            $price_row['intProductPrice_usedquantity'] = number_format((float)$price_row['intProductPrice_usedquantity'], 2, '.', '');
        }

        return $product_row;
    }
    public function getProductsData(){
        $product_ids = $this->model->getProductIdsByClientId();
        $amazon_data = array();
        foreach ($product_ids as $product_id){
            $new_data = $this->getProductInfoFromAmazonByAsin($product_id['strProduct_ASIN']);
            if ($new_data){
                $this->insertProductPriceWithValidate($product_id['product_ID'], $new_data['new_price'], $new_data['new_quantity'], $new_data['used_price'], $new_data['used_quantity']);
            }
            $amazon_data[] = $this->getFormatDataByProductId($product_id['product_ID']);
        }
        return $amazon_data;
    }
    public function getRecommendation($price_rows){
        require_once __DIR__ . '/../options/ProductUtils.php';
        $vals = [];
        foreach ($price_rows as $price_row){
            $vals[] = $price_row['intProductPrice_newprice'] / 100;
        }
        if (count($vals) < 2){ return 'Buy Now'; }

        $std = stddev($vals);
        $stderr = $std / sqrt(count($vals));
        $CI = $stderr*ttable((count($vals)-1),0.025);
        $mean = array_sum($vals) / count($vals);
        $lowval = $mean - $CI;
        $highval = $mean + $CI;
        $latest_price = $vals[count($vals) - 1];

        if ($latest_price < $lowval){
            return 'Buy Now';
        }
        elseif ($lowval <= $latest_price && $latest_price <= $highval){
            return 'No recommendation';
        }
        else{
            return 'Wait';
        }
    }
    public function closeProduct(){
        $product_id = $_POST['product_id'];
        return $this->model->deleteProductUser($product_id);
    }
}