<?php


namespace Controllers;

use Core\Controller_core;
use Models\api\Options_m;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Product;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Transfer;

require_once __DIR__ . '/../core/Controller_core.php';

class Stripe_c extends Controller_core
{
    private $optionsModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Options_m');
        $this->optionsModel = new Options_m();
        $stripeApi_sKey = $this->optionsModel->getOption('stripeApi_sKey');
        $stripeApi_sKey = \Helpers\jwtDecode($stripeApi_sKey);
        Stripe::setApiKey($stripeApi_sKey);
    }
    public function createProduct($sets) {
        return Product::create($sets);
    }
    public function createPlan($sets) {
        return Plan::create($sets);
    }
    public function createCustomer($sets) {
        return Customer::create($sets);
    }
    public function createSubscription($sets) {
        return Subscription::create($sets);
    }
    public function createTransfer($sets) {
        return Transfer::create($sets);
    }
}
