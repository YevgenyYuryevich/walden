<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

namespace Controllers;

require_once __DIR__ . '/../core/Controller_core.php';
require_once __DIR__ . '/../models/api_m/Posts_m.php';
require_once __DIR__ . '/../models/api_m/Subscriptions_m.php';
require_once __DIR__ . '/../controllers/Subscriptions_c.php';

use Core\Controller_core;
use function Helpers\formatPostToSubs;
use function Helpers\putContents;
use function Helpers\uploadFile;
use function Helpers\utf8Encode;
use Library\Unsplash_lb;
use Models\api\Posts_m;
use Models\api\Purchased_m;
use Models\api\Subscriptions_m;
use models\api_m\SubscriptionCompletes_m;

class Posts_c extends Controller_core
{
    public $model;
    public $subsModel;
    public $subsHandle;
    public $purchasedModel;
    public $subsCpltModel;
    public $unReadPostsModel;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Posts_m();
        if (isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1) {
            $this->subsHandle = new Subscriptions_c();
            $this->subsModel = new Subscriptions_m();
            $this->load->model('api_m/Purchased_m');
            $this->purchasedModel = new Purchased_m();

            $this->load->model('api_m/SubscriptionCompletes_m');
            $this->subsCpltModel = new SubscriptionCompletes_m();
        }
        $this->load->library('Unsplash_lb');
        $this->unsplashLib = new Unsplash_lb();

        $this->load->model('api_m/ClientSeriesView_m');
        $this->CSViewModel = new \Models\api\ClientSeriesView_m();

        $this->load->model('api_m/UnReadPosts_m');
        $this->unReadPostsModel = new \Models\api\UnReadPosts_m();
    }
    public function insert($sets){
        $defaultSets = [
            'intPost_parent' => 0,
            'strPost_nodeType' => 'post',
            'intPost_type' => 7,
        ];
        $sets = array_merge($defaultSets, $sets);

        if ($sets['strPost_nodeType'] == 'menu' && !isset($sets['strPost_status'])) {
            $sets['strPost_status'] = 'draft';
        }

        if (isset($sets['strPost_featuredimage']) && $sets['strPost_featuredimage']){}
        elseif (isset($_FILES['strPost_featuredimage'])){
            $uploadRlt = uploadFile($_FILES['strPost_featuredimage'], ['prefix' => 'post']);
            $sets['strPost_featuredimage'] = $uploadRlt['url'];
        }
        else{
            if ($sets['strPost_nodeType'] != 'path' && $sets['strPost_nodeType'] != 'menu') {
                $url = $this->unsplashLib->random('rand');
                $ext = $this->unsplashLib->getExtensionFromUrl($url);
                $savedUrl = putContents($url, 'post.' . $ext, 'assets/images', 'unsplash');
                $sets['strPost_featuredimage'] = $savedUrl;
            }
        }

        if ((int)$sets['intPost_type'] === 9) {
            $sets['strPost_body'] = $this->getValidRedirect($sets['strPost_body']);
        }
        if (isset($sets['intPost_savedMoney']) && !$sets['intPost_savedMoney']) {
            unset($sets['intPost_savedMoney']);
        }
        if (isset($sets['strPost_title'])) {
            utf8Encode($sets['strPost_title']);
        }
        if (isset($sets['strPost_body'])) {
            utf8Encode($sets['strPost_body']);
        }
        $newId = $this->model->insert($sets);
        if (isset($sets['intPost_order'])) {
            $sameOrderPosts = $this->model->getPosts(['intPost_series_ID' => $sets['intPost_series_ID'], 'intPost_parent' => $sets['intPost_parent'], 'intPost_order' => $sets['intPost_order']]);
            if (count($sameOrderPosts) > 1) {
                $this->orderPosts(['intPost_series_ID' => $sets['intPost_series_ID'], 'intPost_parent' => $sets['intPost_parent']]);
            }
        }
        if (isset($_SESSION['client_ID']) && $_SESSION['client_ID'] !== -1) {
            if ($sets['strPost_nodeType'] != 'path' || true) {
                $this->insertUnreadToUsersRelated($newId);
            }
            $this->synchronizeToSubscription('insert', ['post' => $newId]);
        }
        return $newId;
    }
    public function insertUnreadToUsersRelated($row) {
        if (!is_array($row)) {
            $row = $this->model->get($row);
        }
        $purchases = $this->purchasedModel->getRows(['intPurchased_series_ID' => $row['intPost_series_ID']], 'intPurchased_client_ID');
        $flgs = [];
        foreach ($purchases as $purchase) {
            if (isset($flgs[$purchase['intPurchased_client_ID']])) {
                continue ;
            }
            $flgs[$purchase['intPurchased_client_ID']] = true;
            $this->unReadPostsModel->insert([
                'user_id' => $purchase['intPurchased_client_ID'],
                'post_id' => $row['post_ID'],
                'series_id' => $row['intPost_series_ID']
            ]);
        }
    }
    public function getValidRedirect($to) {
        $post = $this->model->get($to);
        if ($post['strPost_nodeType'] == 'path') {
            $child = $this->model->get(['intPost_parent' => $to, 'post_ID']);
            if ($child) {
                return $child['post_ID'];
            }
            else {
                $parentPost = $this->model->get($post['intPost_parent']);
                $nextPost = $this->getNextPost($parentPost);
                return $nextPost['post_ID'];
            }
        }
        return $to;
    }
    public function orderPosts($where){
        if (!is_array($where)) {
            $where = ['intPost_series_ID' => $where, 'intPost_parent' => 0];
        }
        $posts = $this->model->getPosts($where, ['post_ID', 'intPost_order']);
        $returns = [];
        foreach ($posts as $key => $post){
            $this->model->update(['intPost_order' => $key + 1], $post['post_ID']);
            $returns[$post['post_ID']] = $key + 1;
        }
        return $returns;
    }
    public function update($sets, $where) {
        $posts = $this->model->getPosts($where, 'post_ID, intPost_type');
        foreach ($posts as $post) {
            $type = isset($sets['intPost_type']) ? $sets['intPost_type'] : $post['intPost_type'];
            if ((int)$type === 9 && isset($sets['strPost_body'])) {
                $sets['strPost_body'] = $this->getValidRedirect($sets['strPost_body']);
            }
            if (isset($_FILES['strPost_featuredimage'])) {
                $uploadRlt = uploadFile($_FILES['strPost_featuredimage'], ['prefix' => 'post']);
                $sets['strPost_featuredimage'] = $uploadRlt['url'];
            }
            $this->model->update($sets, $post['post_ID']);
            $this->synchronizeToSubscription('update', ['where' => $post['post_ID'], 'sets' => $sets]);
        }
        return $sets;
    }
    public function delete($where){
        if (!is_array($where)){
            $where = ['post_ID' => $where];
        }
        $posts = $this->model->getPosts($where, ['post_ID', 'strPost_nodeType']);
        foreach ($posts as $post){
            if ($post['strPost_nodeType'] != 'post') {
                $this->delete(['intPost_parent' => $post['post_ID']]);
            }
            $this->subsHandle->delete(['intClientSubscription_post_ID' => $post['post_ID']]);
            $this->model->deleteFavorites(['intFavorite_post_ID' => $post['post_ID']]);
            $this->unReadPostsModel->delete(['post_id' => $post['post_ID']]);
            $this->synchronizeToSubscription('delete', $post['post_ID']);
        }
        $this->model->delete($where);
    }
    public function orderSubscriptions($seriesId, $parentPostId) {
        $purchases = $this->purchasedModel->getRows(['intPurchased_series_ID' => $seriesId]);
        foreach ($purchases as $purchase) {
            if ($parentPostId == 0) {
                $parentSubId = 0;
            }
            else {
                $parentSub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $purchase['purchased_ID'], 'intClientSubscription_post_ID' => $parentPostId]);
                $parentSubId = $parentSub ? $parentSub['clientsubscription_ID'] : false;
            }
            if ($parentSubId !== false) {
                $this->subsHandle->orderSubscriptions(['intClientSubscription_purchased_ID' => $purchase['purchased_ID'], 'intClientSubscriptions_parent' => $parentSubId]);
            }
        }
    }
    public function setComplete($postId, $completeValue) {
        $post = $this->model->get($postId);
        $purchase = $this->purchasedModel->get(['intPurchased_series_ID' => $post['intPost_series_ID']]);
        $sets = [

        ];
        if ($purchase) {
            $sub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $purchase['purchased_ID'], 'intClientSubscription_post_ID' => $postId]);
            if ($sub) {

            }
        }
    }
    public function test_ConnectItems($items){
        $len = count($items);
        for ($i = 0; $i < $len; $i++) {
            $b = $items[$i];
            if ($i) {
                $a = $items[$i - 1];
                $this->model->update(['intPost_nodeParent' => $a['post_ID']], $b['post_ID']);
            }
            if ($i + 1 < $len) {
                $c = $items[$i + 1];
                $this->model->update(['intPost_nodeChild' => $c['post_ID']], $b['post_ID']);
            }
        }
    }
    public function cloneItem($where) {
        $rtn = [];
        $items = $this->model->getPosts($where, 'intPost_series_ID, strPost_body, strPost_title, strPost_featuredimage, intPost_type, intPost_order, strPost_nodeType, intPost_parent, strPost_status');
        foreach ($items as $item) {
            if ($item['strPost_nodeType'] == 'menu') {
                $item['strPost_status'] = 'draft';
            }
            $id = $this->model->insert($item);
            array_push($rtn, $this->model->get($id));
        }

        if (count($rtn) === 1) {
            $rtn = $rtn[0];
        }
        return $rtn;
    }
    public function afterClone($where) {

        $item = $this->model->get($where, 'intPost_series_ID, strPost_body, strPost_title, strPost_featuredimage, intPost_type, intPost_order, strPost_nodeType, intPost_parent, strPost_status');
        if ($item['strPost_nodeType'] == 'menu') {
            $item['strPost_status'] = 'draft';
        }
        $id = $this->model->insert($item);
        $rtn = $this->model->get($id);
        return $rtn;
    }
    public function getPurchasedByPostId($postId) {
        $allSubs = $this->subsModel->getSubscriptions(['intClientSubscription_post_ID' => $postId]);
        foreach ($allSubs as $sub) {
            $purchased = $this->purchasedModel->get($sub['intClientSubscription_purchased_ID']);
            if ($purchased['intPurchased_client_ID'] == $_SESSION['client_ID']) {
                return $purchased;
            }
        }
        return false;
    }
    public function getNextPost($post, $circle = true) {
        if (!is_array($post)) {
            $post = $this->model->get($post);
        }
        $nextPost = $this->model->get(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['intPost_parent'], 'intPost_order >' => $post['intPost_order']], '*', ['intPost_order', 'post_ID']);

        if (!$nextPost){
            if ((int) $post['intPost_parent'] == 0) {
                if ($circle) {
                    $nextPost = $this->model->get(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['intPost_parent'], 'intPost_order >' => 0], '*', ['intPost_order', 'post_ID']);
                }
                else {
                    return false;
                }
            }
            else {
                $parentPost = $this->model->get($post['intPost_parent']);
                $parentPost = $this->model->get($parentPost['intPost_parent']);
                $nextPost = $this->getNextPost($parentPost);
            }
        }
        return $this->filterRedirect($nextPost);
    }
    public function getNextPosts($post, $limit = 100) {
        $nextSiblings = $this->model->getPosts(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['intPost_parent'], 'intPost_order >' => $post['intPost_order']], '*', ['intPost_order', 'post_ID'], $limit);
        $validItems = [];
        foreach ($nextSiblings as $sibling) {
            $rSibling = $this->filterRedirect($sibling);
            if ($rSibling['post_ID'] !== $sibling['post_ID']) {
                $rNextSiblings = $this->getNextPosts($rSibling);
                return array_merge($validItems, [$rSibling], $rNextSiblings);
            }
            $validItems[] = $sibling;
        }
        if ((int)$post['intPost_parent'] === 0) {
            return $validItems;
        }
        $parentPost = $this->model->get($post['intPost_parent']);
        $parentPost = $this->model->get($parentPost['intPost_parent']);
        $parentNextSiblings = $this->getNextPosts($parentPost);
        return array_merge($validItems, $parentNextSiblings);
    }
    public function filterRedirect($post) {
        if ((int)$post['intPost_type'] === 9) {
            if (!isset($post['strPost_body'])){
                $tp = $this->model->get($post['post_ID']);
                $post['strPost_body'] = $tp['strPost_body'];
            }
            $rPost = $this->model->get($post['strPost_body']);
            return $this->filterRedirect($rPost);
        }
        else {
            return $post;
        }
    }
    public function filterRedirected($post) {
        $rPost = $this->model->get(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_type' => 9, 'strPost_body' => $post['post_ID']]);
        if ($rPost) {
            return $this->filterRedirected($rPost);
        }
        else {
            return $post;
        }
    }
    public function getPrevPost($post) {
        if (!is_array($post)) {
            $post = $this->model->get($post);
        }
        $post = $this->filterRedirected($post);
        $prevPost = $this->model->get(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['intPost_parent'], 'intPost_order <' => $post['intPost_order']], '*', ['intPost_order DESC', 'post_ID DESC']);

        if ($prevPost){
            return $this->filterRedirect($prevPost);
        }
        else{
            if ((int) $post['intPost_parent'] == 0) {
                $prevPost = $this->model->get(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $post['intPost_parent'], 'intPost_order <' => 9999999], '*', ['intPost_order DESC', 'post_ID DESC']);
                return $this->filterRedirect($prevPost);
            }
            else {
                $parentPost = $this->model->get($post['intPost_parent']);
                $parentPost = $this->model->get($parentPost['intPost_parent']);
                return $this->filterRedirect($parentPost);
            }
        }
    }
    public function filterFormField($des) {
    }
    public function format(& $postData) {
        if ($postData['strPost_nodeType'] == 'menu') {
            if (!isset($postData['intPost_series_ID'])) {
                $row = $this->model->get($postData['post_ID'], 'intPost_series_ID');
                $postData['intPost_series_ID'] = $row['intPost_series_ID'];
            }
            $where = [
                'intPost_series_ID' => $postData['intPost_series_ID'],
                'intPost_parent' => $postData['post_ID']
            ];
            $paths = $this->model->getPosts($where);
            $postData['paths'] = $paths;
        }
        $postData['viewDay'] = $this->calcDay($postData);
        return $postData;
    }
    public function setSeek($where, $sets){
        $sets['intCSView_seekOrder'] = isset($sets['intCSView_seekOrder']) ? $sets['intCSView_seekOrder'] : 0;
        $this->CSViewModel->update($where, $sets);
        $CSView = $this->CSViewModel->get($where);
        return $this->seekPost($CSView);
    }
    public function seekPost($clientSeriesView) {
        if (!is_array($clientSeriesView)) {
            $clientSeriesView = $this->CSViewModel->get($clientSeriesView);
        }
        $where = [
            'intPost_series_ID' => $clientSeriesView['intCSView_series_ID'],
            'intPost_parent' => $clientSeriesView['intCSView_seekParent'],
            'intPost_order >=' => $clientSeriesView['intCSView_seekOrder']
        ];
        $row = $this->model->get($where);
        if (!$row) {
            unset($where['intPost_order >=']);
            $where['intPost_order <'] = $clientSeriesView['intCSView_seekOrder'];
            $row = $this->model->get($where, '*', ['intPost_order DESC']);
        }
        if (!$row){
            return false;
        }
        $row = $this->filterRedirect($row);
        $this->CSViewModel->update($clientSeriesView['clientSeriesView_ID'], ['intCSView_seekOrder' => $row['intPost_order']]);
        return $row;
    }
    public function seekNext($clientSeriesView) {
        if (!is_array($clientSeriesView)) {
            $clientSeriesView = $this->CSViewModel->get($clientSeriesView);
        }
        $seekPost = $this->seekPost($clientSeriesView);
        if (!$seekPost) {
            $parent = $this->model->get($clientSeriesView['intCSView_seekParent']);
            $seekPost = $this->model->get($parent['intPost_parent']);
        }
        $nextPost = $this->getNextPost($seekPost);
        $this->CSViewModel->update($clientSeriesView['clientSeriesView_ID'], ['intCSView_seekParent' => $nextPost['intPost_parent'], 'intCSView_seekOrder' => $nextPost['intPost_order']]);
        return $nextPost;
    }
    public function seekPrev($clientSeriesView) {
        if (!is_array($clientSeriesView)) {
            $clientSeriesView = $this->CSViewModel->get($clientSeriesView);
        }
        $seekPost = $this->seekPost($clientSeriesView);
        $prevPost = $this->getPrevPost($seekPost);
        $this->CSViewModel->update($clientSeriesView['clientSeriesView_ID'], ['intCSView_seekParent' => $prevPost['intPost_parent'], 'intCSView_seekOrder' => $prevPost['intPost_order']]);
        return $prevPost;
    }
    public function synchronizeToSubscription($action, $data, $scope = 'global') {
        switch ($action) {
            case 'insert':
                $post = $data['post'];
                if (!is_array($post)) {
                    $post = $this->model->get($post);
                }
                $seriesId = $post['intPost_series_ID'];
                $purchases = $this->purchasedModel->getRows(['intPurchased_series_ID' => $seriesId]);
                foreach ($purchases as $purchased) {
                    $distance = $this->distanceFromPurchased($post, $purchased['purchased_ID']);
                    if ($distance === false || $distance === 'is_in' || $distance > 0) { continue; }
                    $this->subsHandle->subscribePostWithOrder($post, $purchased['purchased_ID']);
                }
                break;
            case 'update':
                $postId = $data['where'];
                $sets = $data['sets'];
                $post = $this->model->get($postId);

                if ($scope == 'global') {
                    $subs = $this->subsModel->getSubscriptions(['intClientSubscription_post_ID' => $postId]);
                    if (isset($sets['intPost_parent'])) {
                        foreach ($subs as $sub) {
                            if ((int)$sets['intPost_parent'] == 0) {
                                $this->subsModel->update($sub['clientsubscription_ID'], ['intClientSubscriptions_parent' => 0]);
                            }
                            else {
                                $parSub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID' => $sets['intPost_parent']]);
                                if ($parSub) {
                                    $this->subsModel->update($sub['clientsubscription_ID'], ['intClientSubscriptions_parent' => $parSub['clientsubscription_ID']]);
                                }
                                else{
                                    $this->subsHandle->deepDelete($sub['clientsubscription_ID']);
                                }
                            }
                        }
                    }
                    if (isset($sets['strPost_body'])) {
                        if ((isset($sets['intPost_type']) && (int) $sets['intPost_type'] == 9) || (int) $post['intPost_type'] === 9) {
                            $this->subsModel->update(['intClientSubscription_post_ID' => $postId], ['intClientSubscriptions_type' => 9]);
                            foreach ($subs as $sub) {
                                $this->subsModel->update($sub['clientsubscription_ID'], ['strClientSubscription_body' => '']);
                            }
                        }
                    }
                    $subSets = formatPostToSubs($sets);
                    unset($subSets['strClientSubscription_title']);
                    unset($subSets['strClientSubscription_body']);
                    unset($subSets['strClientSubscription_image']);
                    unset($subSets['intClientSubscriptions_type']);
                    unset($subSets['intClientSubscriptions_parent']);
                    if ($subSets) {
                        $this->subsModel->update(['intClientSubscription_post_ID' => $postId], $subSets);
                    }
                }
                else {
                    $purchased = $this->getPurchasedByPostId($postId);
                    if (!$purchased) { return false; }
                    $sub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $purchased['purchased_ID'], 'intClientSubscription_post_ID' => $postId]);
                    if (!$sub) { return false; }

                    $subSets = formatPostToSubs($sets);
                    unset($subSets['strClientSubscription_title']);
                    unset($subSets['strClientSubscription_body']);
                    unset($subSets['strClientSubscription_image']);
                    unset($subSets['intClientSubscriptions_type']);
                    unset($subSets['intClientSubscriptions_parent']);

                    if (isset($sets['intPost_parent'])) {
                        if ((int)$sets['intPost_parent'] == 0) {
                            $subSets['intClientSubscriptions_parent'] = 0;
                        }
                        else {
                            $parSub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID' => $sets['intPost_parent']]);
                            if ($parSub) {
                                $subSets['intClientSubscriptions_parent'] = $parSub['clientsubscription_ID'];
                            }
                        }
                    }
                    if ($subSets) {
                        $this->subsModel->update($sub['clientsubscription_ID'], $subSets);
                    }
                }
                $this->synchronizeToSubscription('insert', ['post' => $post]);
                break;
            case 'delete':
                $postId = $data;
                if ($scope == 'global') {
                    $this->subsHandle->deepDelete(['intClientSubscription_post_ID' => $postId]);
                }
                else {
                    $purchased = $this->getPurchasedByPostId($postId);
                    if ($purchased) {
                        $this->subsHandle->delete(['intClientSubscription_post_ID' => $postId, 'intClientSubscription_purchased_ID' => $purchased['purchased_ID']]);
                    }
                }
                break;
        }
        return true;
    }
    public function getSerialItems($where, $select = '*', $orderBy = ['intPost_order', 'post_ID'], $limit = 1000000) {
        $defSelect = ['intPost_series_ID'];
        $select = is_array($select) ? $select : [$select];
        $select = array_merge($select, $defSelect);
        $items = $this->model->getPosts($where, $select, $orderBy, $limit);
        $rtn = [];
        foreach ($items as $item) {
            if ($limit == 0) {
                return $rtn;
            }
            $rItem = $this->filterRedirect($item);
            if ($rItem['post_ID'] !== $item['post_ID']) {
                $rWhere = [
                    'intPost_series_ID' => $rItem['intPost_series_ID'],
                    'intPost_parent' => $rItem['intPost_parent'],
                    'intPost_order >=' => $rItem['intPost_order'],
                ];
                $rSerialItems = $this->getSerialItems($rWhere, $select, $orderBy, $limit);
                return array_merge($rtn, $rSerialItems);
            }
            $limit--;
            $rtn[] = $item;
            if ($rItem['strPost_nodeType'] === 'menu') {
                return $rtn;
            }
        }
        if ($limit > 0) {
            if ($rtn) {
                $lastItem = $rtn[count($rtn) - 1];
                $nextPost = $this->getNextPost($lastItem, false);
                if ($nextPost) {
                    $nWhere = [
                        'intPost_series_ID' => $nextPost['intPost_series_ID'],
                        'intPost_parent' => $nextPost['intPost_parent'],
                        'intPost_order >=' => $nextPost['intPost_order'],
                    ];
                    $nSerialItems = $this->getSerialItems($nWhere, $select, $orderBy, $limit);
                    return array_merge($rtn, $nSerialItems);
                }
            }
            else {
                if (isset($where['intPost_parent']) && (int) $where['intPost_parent']) {
                    $option = $this->model->get($where['intPost_parent']);
                    $menu = $this->model->get($option['intPost_parent']);
                    $nextPost = $this->getNextPost($menu, false);
                    if ($nextPost) {
                        $nWhere = [
                            'intPost_series_ID' => $nextPost['intPost_series_ID'],
                            'intPost_parent' => $nextPost['intPost_parent'],
                            'intPost_order >=' => $nextPost['intPost_order'],
                        ];
                        $nSerialItems = $this->getSerialItems($nWhere, $select, $orderBy, $limit);
                        return array_merge($rtn, $nSerialItems);
                    }
                }
            }
        }
        return $rtn;
    }
    public function getHistoryItems($where) {
        $post = $this->model->get($where);
        $beforePosts = $this->model->getPosts(['intPost_parent' => $post['intPost_parent'], 'intPost_series_ID' => $post['intPost_series_ID'], 'intPost_order <' => $post['intPost_order']]);
        if ((int) $post['intPost_parent'] === 0) {
            return $beforePosts;
        }
        $parent = $this->model->get($post['intPost_parent']);
        $parent = $this->model->get($parent['intPost_parent']);
        $pBeforePosts = $this->getHistoryItems($parent['post_ID']);
        return array_merge($pBeforePosts, [$parent], $beforePosts);
    }
    public function isSearched($keyword, $where = []) {
        return $this->model->isSearched($keyword, $where);
    }
    public function calcDay($post) {
        if ((int) $post['intPost_parent'] === 0) {
            return (int) $post['intPost_order'];
        }
        else {
            $parent = $this->model->get($post['intPost_parent']);
            $parent = $this->model->get($parent['intPost_parent']);
            return (int) $post['intPost_order'] + $this->calcDay($parent);
        }
    }
    public function distanceFromPurchased($post, $purchasedId) {
        if (!is_array($post)) {
            $post = $this->model->get($post);
        }
        $sub = $this->subsModel->get(['intClientSubscription_purchased_ID' => $purchasedId, 'intClientSubscription_post_ID' => $post['post_ID']]);
        if ($sub) {
            return 'is_in';
        }
        $parentPost = (int) $post['intPost_parent'];

        $maxOrderPost = $this->subsHandle->getMaxSubscribePost(['intPost_parent' => $parentPost], $purchasedId);
        if ($maxOrderPost) {
            return (int) $post['intPost_order'] - (int) $maxOrderPost['intPost_order'];
        }
        else {
            return false;
        }
    }
}