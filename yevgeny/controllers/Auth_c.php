<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.06.2018
 * Time: 06:29
 */

namespace Controllers;

use Core\Controller_core;
use Firebase\JWT\JWT;
use Models\api\User_m;

require_once __DIR__ . '/../core/Controller_core.php';

class Auth_c extends Controller_core
{
    public $model;
    private $jwtKey = 'ihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQAB
AoGAb/MXV46XxCFRxNuB8LyAtmLDgi/xRnTAlMHjSACddwkyKem8//8eZtw9fzxz
bWZ/1/doQOuHBGYZU8aDzzj59FZ78dyzNFoF91hbvZKkg+6wGyd/LrGVEB+Xre0J
Nil0GReM2AHDNZUYRv+HYJPIOrB0CRczLQsgFJ8K6aAD6F0CQQDzbpjYdx10qgK1
cP59UHiHjPZYC0loEsk7s+hUmT3QHerAQJMZWC11Qrn2N+ybwwNblDKv+s5qgMQ5
5tNoQ9IfAkEAxkyffU6ythpg/H0Ixe1I2rd0GbF05biIzO/i7';
    public $user;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->model = new User_m();
    }
    public function login($username, $password) {
        $password = $this->strongEncrypt($password);
        $user = $this->model->get(['username' => $username, 'hash' => $password], 'id, username, email, f_name, l_name, phone, image, about_me');
        if (!$user) {
            $user = $this->model->get(['email' => $username, 'hash' => $password], 'id, username, email, f_name, l_name, phone, image, about_me');
        }
        if (!$user) { return false; }
        $user['auth_token'] = time();
        $token = JWT::encode($user, $this->jwtKey);
        $this->model->update($user['id'], ['auth_token' => $token]);
        $user['auth_token'] = $token;
        return $user;
    }
    public function filterValidToken($token) {
        $user = $this->model->get(['auth_token' => $token]);
        if (!$user) {
            echo json_encode(['error' => 'invalid token']);
            die();
        }
        $this->user = $user;
        $_SESSION['client_ID'] = $user['id'];
        return $user;
    }
    public function strongEncrypt($password){
        $strong1 = hash_pbkdf2('haval192,4', $password, 'nbNB12_+' . substr($password, 0, 3), 50);
        $strong2 = hash_pbkdf2('gost-crypto', $strong1, 'yvgYVG1@<[~' . substr($password, 3), 100);
        $strong3 = substr($strong2, 1) . $strong2[0];
        return $strong3;
    }
}