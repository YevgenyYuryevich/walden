<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.03.2018
 * Time: 17:47
 */

namespace Controllers;

require_once __DIR__ . '/../models/Events_m.php';
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../options/Unsplash.php';
require_once __DIR__ . '/../models/api_m/ClientMeta_m.php';
require_once __DIR__ . '/../helpers/funcs.php';

use function Helpers\htmlMailHeader;
use Models\Events_m;
use Crew\Unsplash\Photo;
use Crew\Unsplash\Search;
use Models\api\ClientMeta_m;

class Events_c
{

    public function __construct()
    {
        global $unsplashOptions;
        $this->model = new Events_m();
        $this->clientMetaModel = new ClientMeta_m();
        \Crew\Unsplash\HttpClient::init($unsplashOptions);
        $this->unsplash = new Photo();
        $this->unsplashSearch = new Search();
        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';
    }
    public function createEvent(){
        $data = $_POST['data'];
        $id = $this->model->createEvent($data);

        if ($data['is_recurring'] === 'true'){
            $this->model->createRecurringPattern($id, $data['recurring_pattern_data'], $data);
        }
        $event_data = $this->getEventById($id);
        echo json_encode(array('status' => true, 'data' => $event_data));
        die();
    }
    public function getEvents(){
        $events = $this->model->getEvents();

        foreach ($events as &$event) {
            $event['event_images'] = $this->model->getEventImagesByEventId($event['id']);
            $event['event_favorites'] = $this->model->getFavoritesByEventId($event['id']);
            $event['rescheduled_events'] = $this->model->getRescheduledEvents($event['id']);
            $event['cancelled_events'] = $this->model->getCancelledEvents($event['id']);
            if ($event['is_recurring']){
                $event['recurring_pattern'] = $this->model->getRecurringPattern($event['id']);
            }
        }
        return $events;
    }
    public function insert($sets){
        if (isset($sets['recurring_pattern'])){
            $recurringPattern = $sets['recurring_pattern'];
            unset($sets['recurring_pattern']);
        }
        if (isset($sets['event_image'])){
            $eventImage = $sets['event_image'];
            unset($sets['event_image']);
        }
        $eventId = $this->model->insert($sets);
        if (isset($recurringPattern)){
            $recurringPattern['event_id'] = $eventId;
            $this->insertRecurringPattern($recurringPattern);
        }
        if (isset($eventImage)){
            $imgSets['event_id'] = $eventId;
            $imgSets['file'] = $eventImage;
            $this->insertEventImage($imgSets);
        }
        else{
            $t = explode(' ', trim($sets['event_title']));
            $apiImg = $this->getPhotoFromUnsplash($t[0], 'event');
            $this->model->insertEventImage(['event_id' => $eventId, 'media_url' => $apiImg['savedUrl']]);
        }
        $this->filterFirstCreate();
        return $eventId;
    }
    public function getPhotoFromUnsplash($query, $for){
        $filter = ['query' => $query, 'w' => 300];
        $unsplashPhoto = $this->unsplash::random($filter);
        if ($unsplashPhoto->errors){
            return ['savedUrl' => $this->defaultPhoto, 'originUrl' => $this->defaultPhoto];
        }
        $uPhotoUrl = $unsplashPhoto->urls['small'];
        preg_match('/fm=([^\&]*)/', $uPhotoUrl, $matches);

        $savedUrl = $this->putContents($uPhotoUrl, $for, $query, $matches[1]);
        return ['savedUrl' => $savedUrl, 'originUrl' => $uPhotoUrl];
    }
    public function putContents($orgUrl, $prefix, $name, $extends){
        $name = preg_replace('/[\?\/\#]/', '-', $name);
        $uniqueFilename = uniqid(str_replace(' ', '_', $prefix)).'_' . urlencode($name) . '.' . $extends;
        $savedUrl = 'assets/images/' . $uniqueFilename;
        file_put_contents(_ROOTPATH_ . '/' . $savedUrl, fopen($orgUrl, 'r'));
        return $savedUrl;
    }
    public function insertRecurringPattern($sets){
        if (isset($sets['event_instance_exceptions'])){
            $eventInstanceExceptions = $sets['event_instance_exceptions'];
            foreach ($eventInstanceExceptions as $eventInstanceException){
                $eventInstanceException['event_id'] = $sets['event_id'];
                $this->model->insertInstanceException($eventInstanceException);
            }
            unset($sets['event_instance_exceptions']);
        }
        $newId = $this->model->insertRecurringPattern($sets);
        return $newId;
    }
    public function insertEventImage($sets){
        $event_id = $sets['event_id'];
        $image_file = $sets['file'];
        $unique_filename = uniqid('event_image').'_' . $image_file['name'];
        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
        $rlt = move_uploaded_file($image_file['tmp_name'], $destination);

        if ($rlt == true){
            $image_url = 'assets/images/' . $unique_filename;
            return $this->model->insertEventImage(['event_id' => $event_id, 'media_url' => $image_url]);
        }
        else{
            return false;
        }
    }
    public function getEventById($id){
        $event = $this->model->getEventById($id);
        $event['event_images'] = $this->model->getEventImagesByEventId($event['id']);
        $event['event_favorites'] = $this->model->getFavoritesByEventId($event['id']);
        if ($event['is_recurring']){
            $event['recurring_pattern'] = $this->model->getRecurringPattern($event['id']);
            $event['rescheduled_events'] = $this->model->getRescheduledEvents($event['id']);
            $event['cancelled_events'] = $this->model->getCancelledEvents($event['id']);
        }
        return $event;
    }
    public function deleteRescheduledEvent(){
        $id = $_POST['id'];
        $this->model->deleteRescheduledEvent($id);
        return true;
    }
    public function deleteEvent(){
        $id = $_POST['id'];
        $this->model->deleteEvent($id);
        return true;
    }
    public function cancelEventInstance(){
        $data = $_POST['data'];
        $new_id = $this->model->createCancelledEvent($data['event_id'], $data['start_date']);
        $res = $this->model->getCancelledEventById($new_id);
        return $res;
    }
    public function deleteEventImage(){
        $id = $_POST['id'];
        $rlt = $this->model->deleteEventImage($id);
        if ($rlt){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function rescheduleInstance(){
        $event_id = $_POST['data']['event_id'];
        $data = $_POST['data'];
        $origin_start_date = $_POST['origin_start_date'];

        $cancelled_id = $this->model->createCancelledEvent($event_id, $origin_start_date);
        $cancelled_row = $this->model->getCancelledEventById($cancelled_id);

        $rescheduled_id = $this->model->createRescheduledEvent($event_id, $data);
        $rescheduled_row = $this->model->getRescheduledEventById($rescheduled_id);

        echo json_encode(array('status' => true, 'data' => array('rescheduled_row' => $rescheduled_row, 'cancelled_row' => $cancelled_row)));
        die();
    }
    public function updateEvent(){
        $event_id = $_POST['data']['event_id'];
        $data = $_POST['data'];

        $this->model->updateEvent($event_id, $data);
        echo json_encode(array('status' => true));
        die();
    }
    public function updateReschduledInstance(){
        $id = $_POST['id'];
        $data = $_POST['data'];
        $this->model->updateReschduledInstance($id, $data);
        echo json_encode(array('status' => true));
        die();
    }
    public function updateAllInstancesOfEvent(){
        $do_reschedule = $_POST['doReschedule'];
        $data = $_POST['data'];
        $start_date = $data['start_date'];
        $data['start_date'] = null;
        $this->model->updateEvent($data['event_id'], $data);
        $this->model->updateRescheduledInstancesByEventId($data['event_id'], $data);
        switch ($do_reschedule){
            case '0':
                echo json_encode(array('status' => true));
                break;
            case '1':
                $origin_start_date = $_POST['origin_start_date'];
                $new_id = $this->model->createCancelledEvent($data['event_id'], $origin_start_date);
                $cancelled_row = $this->model->getCancelledEventById($new_id);

                $data['start_date'] = $start_date;
                $new_id = $this->model->createRescheduledEvent($data['event_id'], $data);
                $rescheduled_row = $this->model->getRescheduledEventById($new_id);

                echo json_encode(array('status' => true, 'data' => array('cancelled_row' => $cancelled_row, 'rescheduled_row' => $rescheduled_row)));
                break;
            case '2':
                $data['start_date'] = $start_date;
                $this->model->updateReschduledInstance($_POST['id'], $data);
                echo json_encode(array('status' => true));
                break;
        }
        die();
    }
    public function makeEventFavorite(){
        $event_id = $_POST['event_id'];
        $is_rescheduled = $_POST['is_rescheduled'];
        $start_date = $_POST['start_date'];
        $instance_id = null;
        if ($is_rescheduled == 1){
            $instance_id = $_POST['instance_id'];
        }
        $new_id = $this->model->insertEventFavorite($event_id, $is_rescheduled, $start_date, $instance_id);
        $new_row = $this->model->getFavoriteById($new_id);
        return $new_row;
    }
    public function makeEventUnFavorite(){
        $id = $_POST['id'];
        $this->model->deleteEventFavorite($id);
        return true;
    }
    public function filterFirstCreate(){
        $flgFirstCreate = $this->clientMetaModel->get(['meta_key' => 'first_event_create']);
        if (!$flgFirstCreate){
            $this->clientMetaModel->insert(['meta_key' => 'first_event_create']);
            $headers = htmlMailHeader();
            $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/congrats_creating_event.html');
            mail($_SESSION['guardian']['email'], 'Congratulations on creating your first event!', $msg, $headers);
        }
    }
}