<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 12.07.2018
 * Time: 22:46
 */

namespace Controllers;

use Core\Controller_core;
use function Helpers\jwtDecode;
use Models\api\User_m;

require_once __DIR__ . '/../core/Controller_core.php';

class User_c extends Controller_core
{
    public $model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/User_m');
        $this->model = new User_m();
    }
    public function getMe() {
        return $this->get($_SESSION['client_ID']);
    }
    public function get($where, $select = '*'){
        $row = $this->model->get($where, $select);
        if ($row) {
            if (isset($row['stripeApi_pKey']) && $row['stripeApi_pKey']) {
                $row['stripeApi_pKey'] = jwtDecode($row['stripeApi_pKey']);
            }
            if (isset($row['stripeApi_cUrl']) && $row['stripeApi_cUrl']) {
                $row['stripeApi_cUrl'] = jwtDecode($row['stripeApi_cUrl']);
            }
        }
        return $row;
    }
}
