<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 07.06.2018
 * Time: 16:15
 */

namespace Controllers;

use Core\Controller_core;
use Models\api\FormField_m;
use Models\api\FormFieldAnswer_m;

require_once __DIR__ . '/../core/Controller_core.php';

class FormFields_c extends Controller_core
{
    public $model;
    public $answerModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/FormField_m');
        $this->model = new FormField_m();
        $this->load->model('api_m/FormFieldAnswer_m');
        $this->answerModel = new FormFieldAnswer_m();
    }
    public function delete($where = []) {
        if (!is_array($where)) {
            $where = [
                $this->model->PRIMARY_KEY => $where
            ];
        }
        $rows = $this->model->getRows($where);
        foreach ($rows as $row) {
            $this->answerModel->delete(['intFormFieldAnswer_field_ID' => $row['formField_ID']]);
        }
        return $this->model->delete($where);
    }
}