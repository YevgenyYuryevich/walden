<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 01.03.2018
 * Time: 17:40
 */

namespace Controllers;

require_once __DIR__ . '/../models/api_m/Goals_m.php';
require_once __DIR__ . '/../models/api_m/ClientMeta_m.php';
require_once __DIR__ . '/../helpers/funcs.php';

use Models\api\{Goals_m, ClientMeta_m};
use function Helpers\htmlMailHeader;
use function mail;

class Goals_c
{
    public function __construct()
    {
        $this->model = new Goals_m();
        $this->clientMetaModel = new ClientMeta_m();
        $this->currentUser = isset($_SESSION['client_ID']) ? $_SESSION['client_ID'] : -1;
    }
    public function getGoalsDataByDate($date){
        $grateGoals = $this->model->getGoals(['dtGoal_date' => $date, 'intGoal_type' => 1]);
        $normalGoals = $this->model->getRows(['dtGoal_date >=' => $date, 'intGoal_type' => 2]);
        $trapGoals = $this->model->getGoals(['dtGoal_date' => $date, 'intGoal_type' => 3]);
        $morningGoals = $this->model->getGoals(['dtGoal_date' => $date, 'intGoal_type' => 4]);
        $eveningGoals = $this->model->getGoals(['dtGoal_date' => $date, 'intGoal_type' => 5]);
        
        foreach ($grateGoals as & $goal){
            $goal = $this->formatGoal($goal);
        }

        foreach ($normalGoals as & $goal){
            $goal = $this->formatGoal($goal);
        }
        foreach ($morningGoals as & $goal){
            $goal = $this->formatGoal($goal);
        }
        foreach ($eveningGoals as & $goal){
            $goal = $this->formatGoal($goal);
        }
        foreach ($trapGoals as & $goal){
            $goal = $this->formatGoal($goal);
        }

        return ['grateGoals' => $grateGoals, 'normalGoals' => $normalGoals, 'trapGoals' => $trapGoals, 'morningGoals' => $morningGoals, 'eveningGoals' => $eveningGoals];
    }
    public function formatGoal($goal){
        $goal['boolGoal_complete'] = (int) $goal['boolGoal_complete'];
        return $goal;
    }
    public function getRandom($where){
        $row = $this->model->randWhere($where);
        return $row;
    }
    public function filterFirstInsert(){
        $flgFirstInsert = $this->clientMetaModel->get(['meta_key' => 'first_goal_create']);
        if (!$flgFirstInsert){
            $this->clientMetaModel->insert(['meta_key' => 'first_goal_create']);
            $headers = htmlMailHeader();
            $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/congrats_on_creating_daily_goals.html');
            mail($_SESSION['guardian']['email'], 'Congratulations on creating your first set of daily goals!', $msg, $headers);
        }
    }
    public function filterFirstGoalsComplete() {
        $flgFirstComplete = $this->clientMetaModel->get(['meta_key' => 'first_goals_complete']);
        if (!$flgFirstComplete){
            $totCnt = $this->model->countMainGoals();
            $cpltCnt = $this->model->countMainGoals(['boolGoal_complete' => 1]);
            if ($totCnt > 0 && $totCnt == $cpltCnt){
                $this->clientMetaModel->insert(['meta_key' => 'first_goals_complete']);
                $headers = htmlMailHeader();
                $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/congrats_on_completing_everything_today.html');
                mail($_SESSION['guardian']['email'], 'Congratulations on completing everything you set out to do today', $msg, $headers);
            }
        }
    }
    public function completePercent($where = []){
        $totCnt = $this->model->countMainGoals($where);
        $where['boolGoal_complete'] = 1;
        $cpltCnt = $this->model->countMainGoals($where);
        return $totCnt == 0 ? 0 : $cpltCnt / $totCnt * 100;
    }
}