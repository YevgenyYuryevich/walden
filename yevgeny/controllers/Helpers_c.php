<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

namespace Controllers;

use Models\api\Helpers_m;

require_once __DIR__ . '/../models/api_m/Helpers_m.php';

class Helpers_c
{
    public function __construct()
    {
        $this->model = new Helpers_m();
    }
    public function randWhere($where){
        return $this->model->randWhere($where);
    }
}