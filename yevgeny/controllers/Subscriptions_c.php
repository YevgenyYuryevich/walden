<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 10.03.2018
 * Time: 12:39
 */

namespace Controllers;

require_once __DIR__ . '/../core/Controller_core.php';
require_once __DIR__ . '/../models/api_m/Subscriptions_m.php';
require_once __DIR__ . '/../models/api_m/Posts_m.php';
require_once __DIR__ . '/../models/api_m/TrashPosts_m.php';
require_once __DIR__ . '/../models/api_m/Purchased_m.php';
require_once __DIR__ . '/../helpers/funcs.php';

use Core\Controller_core;
use Library\Unsplash_lb;
use Models\api\Favorite_m;
use Models\api\Subscriptions_m;
use Models\api\Posts_m;
use Models\api\TrashPosts_m;

use function Helpers\{
    binarySearch, arrayValuesByKey, formatSubToPost, putContents, uploadFile, aOrb
};
use Models\api\Purchased_m;
use models\api_m\SubscriptionCompletes_m;

class Subscriptions_c extends Controller_core {

    public $model;
    public $postsModel;
    public $purchasedModel;
    public $trashPostsModel;
    public $subsCpltModel;
    public $unsplashLib;
    public $favoriteModel;
    public $seriesHandle;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Subscriptions_m();
        $this->postsModel = new Posts_m();
        $this->purchasedModel = new Purchased_m();
        $this->trashPostsModel = new TrashPosts_m();
        $this->load->library('Unsplash_lb');
        $this->load->model('api_m/SubscriptionCompletes_m');
        $this->subsCpltModel = new SubscriptionCompletes_m();
        $this->load->model('api_m/Favorite_m');
        $this->favoriteModel = new Favorite_m();
        $this->unsplashLib = new Unsplash_lb();
        $this->currentUser = $_SESSION['client_ID'];
    }
    public function insert($sets){
        if (isset($sets['strClientSubscription_image']) && $sets['strClientSubscription_image']){}
        elseif (isset($_FILES['strClientSubscription_image'])){
            $uploadRlt = uploadFile($_FILES['strClientSubscription_image'], ['prefix' => 'post']);
            $sets['strPost_featuredimage'] = $uploadRlt['url'];
        }
        else{
            $url = $this->unsplashLib->random('rand');
            $ext = $this->unsplashLib->getExtensionFromUrl($url);
            $savedUrl = putContents($url, 'subscription.' . $ext, 'assets/images', 'unsplash');
            $sets['strClientSubscription_image'] = $savedUrl;
        }
        return $this->model->insert($sets);
    }
    public function getSubscriptions($where, $select = '*'){
        $subscriptions = $this->model->getSubscriptions($where, $select, ['intClientSubscriptions_order', 'clientsubscription_ID']);
        $validItems = [];
        foreach ($subscriptions as $subscription) {
            $subscription = $this->formatSubscription($subscription);
            if ((int)$subscription['intClientSubscriptions_type'] === 9) {
                continue;
//                break;
            }
            $validItems[] = $subscription;
        }
        return $validItems;
    }
    public function get($where, $select = '*', $orderBy = ['intClientSubscriptions_order', 'clientsubscription_ID']) {
        $subscription = $this->model->get($where, $select, $orderBy);
        $subscription = $this->formatSubscription($subscription);
        return $subscription;
    }
    public function deepSubscribe( $endPoint, $range = 'only_routing' ) {
        $sub = $this->model->get( $endPoint );
        if ( $sub ) {
            return $sub;
        }
        else if ((int) $endPoint['intClientSubscription_post_ID'] == 0) {
            return false;
        }
        else {
            $post = $this->postsModel->get($endPoint['intClientSubscription_post_ID']);
            $postParent = $post['intPost_parent'];
            if ((int) $postParent) {
                $parSub = $this->deepSubscribe(['intClientSubscription_purchased_ID' => $endPoint['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID' => $postParent], $range);
                if (!$parSub) {
                    return false;
                }
            }
        }
        if ($range === 'only_routing') {
            $this->subscribeSeries(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $postParent, 'intPost_order' => $post['intPost_order'], 'intPost_order >' => 0], $endPoint['intClientSubscription_purchased_ID'], 1);
        }
        else {
            $this->subscribeSeries(['intPost_series_ID' => $post['intPost_series_ID'], 'intPost_parent' => $postParent, 'intPost_order <=' => $post['intPost_order'], 'intPost_order >' => 0], $endPoint['intClientSubscription_purchased_ID'], 100000);
        }
        return $this->model->get( $endPoint );
    }
    public function subscribeSeries($where, $purchasedId, $limit = 100) {
        $maxOrderPost = $this->getMaxSubscribePost($where, $purchasedId);
        $defaultWhere = [
            'strPost_status' => 'publish',
            'intPost_order >' => $maxOrderPost ? $maxOrderPost['intPost_order'] : 0,
        ];
        $where = array_merge($defaultWhere, $where);
        $unSubsPosts = $this->getUnSubsPosts($where, $purchasedId, $limit);
        foreach ($unSubsPosts as $unSubsPost) {
            $this->subscribePost($unSubsPost['post_ID'], $purchasedId);
            if ($unSubsPost['intPost_type'] == 9) { break; }
        }
        return $unSubsPosts;
    }
    public function getMaxSubscribePost($where, $purchasedId) {
        if ($where['intPost_parent'] == 0) {
            $parentSubId = 0;
        }
        else {
            $parentSub = $this->model->get(['intClientSubscription_post_ID' => $where['intPost_parent'], 'intClientSubscription_purchased_ID' => $purchasedId]);
            $parentSubId = $parentSub ? $parentSub['clientsubscription_ID'] : false;
        }
        if ($parentSubId === false) {
            return false;
        }
        $subs = $this->model->getSubscriptions(['intClientSubscription_purchased_ID' => $purchasedId, 'intClientSubscriptions_parent' => $parentSubId], 'intClientSubscription_post_ID');
        $postMaxOrder = 0;
        $maxOrderPost = false;
        foreach ($subs as $sub) {
            if (is_null($sub['intClientSubscription_post_ID'])) {
                continue;
            }
            $post = $this->postsModel->get($sub['intClientSubscription_post_ID']);
            if ((int)$post['intPost_order'] > $postMaxOrder) {
                $postMaxOrder = $post['intPost_order'];
                $maxOrderPost = $post;
            }
        }
        return $maxOrderPost;
    }
    public function subscribePost($postId, $purchasedId) {
        $sets = [
            'intClientSubscription_purchased_ID' => $purchasedId,
            'intClientSubscription_post_ID' => $postId,
        ];
        $post = $this->postsModel->get($postId, 'intPost_parent');
        $parentPost = (int)$post['intPost_parent'];
        if ($parentPost) {
            $parentSubscription = $this->model->get(['intClientSubscription_purchased_ID' => $purchasedId, 'intClientSubscription_post_ID' => $parentPost]);
            $sets['intClientSubscriptions_parent'] = $parentSubscription['clientsubscription_ID'];
        }

        $this->model->insert($sets);
    }
    public function subscribePostWithOrder($post, $purchasedId) {
        $sets = [
            'intClientSubscription_purchased_ID' => $purchasedId,
            'intClientSubscription_post_ID' => $post['post_ID'],
            'intClientSubscriptions_order' => $post['intPost_order'],
        ];
        $parentPost = (int) $post['intPost_parent'];
        if ($parentPost) {
            $parentSubscription = $this->model->get(['intClientSubscription_purchased_ID' => $purchasedId, 'intClientSubscription_post_ID' => $parentPost]);
            $sets['intClientSubscriptions_parent'] = $parentSubscription['clientsubscription_ID'];
        }
        $this->model->insert($sets);
    }
    public function getUnSubsPosts($where, $purchasedId, $limit = 100) {
        $posts = $this->postsModel->getPosts($where, ['post_ID', 'intPost_order', 'intPost_type']);
        $subscriptions = $this->model->getSubscriptions(['intClientSubscription_purchased_ID' => $purchasedId], 'intClientSubscription_post_ID');
        $trashPosts = $this->trashPostsModel->getWhere(['intTrashPost_series_ID' => $where['intPost_series_ID']], 'intTrashPost_post_ID');

        $unsubsPosts = [];

        $subsIds = arrayValuesByKey($subscriptions, 'intClientSubscription_post_ID');
        $trashPostIds = arrayValuesByKey($trashPosts, 'intTrashPost_post_ID');
        sort($subsIds);
        sort($trashPostIds);
        $len = count($posts);

        for ($i = 0; $i < $len && $limit; $i++){
            if (binarySearch($posts[$i]['post_ID'], $subsIds) || binarySearch($posts[$i]['post_ID'], $trashPostIds)){
                continue;
            }
            $unsubsPosts[] = $posts[$i];
            $limit--;
        }
        return $unsubsPosts;
    }
    public function stepNext($purchased) {
        $nextSubs = $this->seekNextSubs($purchased);
        if (!$nextSubs) {
            return false;
        }
        $this->purchasedModel->update($purchased['purchased_ID'], ['intPurchased_seekOrder' => $nextSubs['intClientSubscriptions_order'], 'intPurchased_seekParent' => $nextSubs['intClientSubscriptions_parent']]);
        return $nextSubs;
    }
    public function stepPrev($purchased) {
        $prevSubs = $this->seekPrevSubs($purchased);
        if (!$prevSubs) { return false; }
        $this->purchasedModel->update($purchased['purchased_ID'], ['intPurchased_seekOrder' => $prevSubs['intClientSubscriptions_order'], 'intPurchased_seekParent' => $prevSubs['intClientSubscriptions_parent']]);
        return $prevSubs;
    }
    public function seekNextSubs($purchased) {
        if (!is_array($purchased)) {
            $purchased = $this->purchasedModel->get($purchased);
        }
        $row = $this->seekSubscription($purchased);
        $nextSub = $this->getNextSub($row);
        return $nextSub;
    }
    public function seekPrevSubs($purchased) {

        if (!is_array($purchased)) {
            $purchased = $this->purchasedModel->get($purchased);
        }
        $row = $this->seekSubscription($purchased);
        return $this->getPrevSub($row);
    }
    public function seekSubscription($purchased) {
        if (!is_array($purchased)) {
            $purchased = $this->purchasedModel->get($purchased);
        }
        $where = [
            'intClientSubscription_purchased_ID' => $purchased['purchased_ID'],
            'intClientSubscriptions_parent' => $purchased['intPurchased_seekParent'],
            'intClientSubscriptions_order >=' => $purchased['intPurchased_seekOrder'],
        ];
        $row = $this->model->get($where);
        if (!$row){
            unset($where['intClientSubscriptions_order >=']);
            $where['intClientSubscriptions_order <='] = $purchased['intPurchased_seekOrder'];
            $row = $this->model->get($where,'*', 'intClientSubscriptions_order DESC');
        }
        if (!$row){
            return false;
        }
        if (is_null($row['dtClientSubscription_datetoshow'])){
            $sets = ['dtClientSubscription_datetoshow' => date('Y-m-d')];
            $this->model->update($row['clientsubscription_ID'], $sets);
            $row = array_merge($row, $sets);
        }
        $row = $this->filterRedirect($row);
        $this->purchasedModel->update($purchased['purchased_ID'], ['intPurchased_seekOrder' => $row['intClientSubscriptions_order']]);
        return $row;
    }
    public function seekUpstairs($purchased) {
        if (!is_array($purchased)) {
            $purchased = $this->purchasedModel->get($purchased);
        }
        $seekParent = $purchased['intPurchased_seekParent'];
        if ((int)$seekParent == 0) {
            return false;
        }
        $subscriptionParent = $this->model->get($seekParent);
        $sets = [
            'intPurchased_seekParent' => $subscriptionParent['intClientSubscriptions_parent'],
            'intPurchased_seekOrder' => $subscriptionParent['intClientSubscriptions_order']
        ];
        $this->purchasedModel->update($purchased['purchased_ID'], $sets);
        return array_merge($purchased, $sets);
    }
    public function setSeek($purchased, $sets) {
        if (!is_array($purchased)) {
            $purchased = $this->purchasedModel->get($purchased);
        }
        if (isset($sets['intPurchased_seekParent'])) {
            $parentSubscription = $this->model->get($sets['intPurchased_seekParent']);
            if ($parentSubscription) {
                if (!is_null($parentSubscription['intClientSubscription_post_ID'])) {
                    $this->subscribeSeries(['intPost_series_ID' => $purchased['intPurchased_series_ID'], 'intPost_parent' => $parentSubscription['intClientSubscription_post_ID']], $purchased['purchased_ID']);
                }
            }
            else {
                $this->subscribeSeries(['intPost_series_ID' => $purchased['intPurchased_series_ID'], 'intPost_parent' => 0], $purchased['purchased_ID']);
            }
        }
        $this->purchasedModel->update($purchased['purchased_ID'], $sets);
        return array_merge($purchased, $sets);
    }
    public function filterRedirect($sub, & $trackFlg = []) {
        $type = $sub['intClientSubscriptions_type'];
        if (is_null($sub['intClientSubscription_post_ID'])) { return $sub; }

        if (isset($trackFlg[$sub['clientsubscription_ID']])) { return false; }
        $trackFlg[$sub['clientsubscription_ID']] = 1;

        $post = $this->postsModel->get($sub['intClientSubscription_post_ID']);
        if (is_null($type)) {
            $type = $post['intPost_type'];
        }
        if ( (int) $type === 9 ) {
            if (!$sub['strClientSubscription_body']) {
                $postBody = $post['strPost_body'];
                $rSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID' => $postBody]);
                if (!$rSub) {
                    $rSub = $this->deepSubscribe(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID' => $postBody], 'all');
                    if (! $rSub) {
                        return false;
                    }
                }
            }
            else {
                $rSub = $this->model->get($sub['strClientSubscription_body']);
            }
            return $this->filterRedirect($rSub, $trackFlg);
        }
        else {
            return $sub;
        }
    }
    public function filterRedirected($sub) {
        if (!$sub['intClientSubscription_post_ID']) { return $sub; }

        $post = $this->postsModel->get($sub['intClientSubscription_post_ID']);
        $rPost = $this->postsModel->get(['intPost_type' => 9, 'intPost_series_ID' => $post['intPost_series_ID'], 'strPost_body' => $post['post_ID']]);
        if ($rPost) {
            $rSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID' => $rPost['post_ID']]);
            if ($rSub) { return $rSub; }
            return $this->deepSubscribe(['intClientSubscription_post_ID' => $rPost['post_ID'], 'intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID']], 'all');
        }
        else {
            return $sub;
        }
    }
    public function maxSubscribedPost($sub) {
        $maxPostSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscription_post_ID >' => 0, 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent']], '*');
        return $this->postsModel->get($maxPostSub['intClientSubscription_post_ID']);
    }
    public function getNextSub($sub) {
        if (!is_array($sub)) {
            $sub = $this->model->get($sub);
        }
        $nextSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent'], 'intClientSubscriptions_order >' => $sub['intClientSubscriptions_order']], '*', ['intClientSubscriptions_order', 'clientsubscription_ID']);
        if (!$nextSub) {
            $maxPost = $this->maxSubscribedPost($sub);
            $this->subscribeSeries(['intPost_series_ID' => $maxPost['intPost_series_ID'], 'intPost_parent' => $maxPost['intPost_parent'], 'intPost_order >' => $maxPost['intPost_order']], $sub['intClientSubscription_purchased_ID']);
            $nextSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent'], 'intClientSubscriptions_order >' => $sub['intClientSubscriptions_order']], '*', ['intClientSubscriptions_order', 'clientsubscription_ID']);
        }
        if (!$nextSub){
            if ((int) $sub['intClientSubscriptions_parent'] == 0) {
                $nextSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent'], 'intClientSubscriptions_order >' => 0], '*', ['intClientSubscriptions_order', 'clientsubscription_ID']);
            }
            else {
                $parentSub = $this->model->get($sub['intClientSubscriptions_parent']);
                $parentSub = $this->model->get($parentSub['intClientSubscriptions_parent']);
                $nextSub = $this->getNextSub($parentSub);
            }
        }
        $rNextSub = $this->filterRedirect($nextSub);
        if (!$rNextSub) { return $sub; }
        if ($rNextSub['clientsubscription_ID'] !== $nextSub['clientsubscription_ID']) {
            $rNextPost = $this->postsModel->get($rNextSub['intClientSubscription_post_ID']);
            $this->subscribeSeries(['intPost_series_ID' => $rNextPost['intPost_series_ID'], 'intPost_parent' => $rNextPost['intPost_parent'], 'intPost_order >=' => $rNextPost['intPost_order']], $sub['intClientSubscription_purchased_ID'], 100);
        }
        return $rNextSub;
    }
    public function getPrevSub($sub) {
        if (!is_array($sub)) {
            $sub = $this->model->get($sub);
        }
        $sub = $this->filterRedirected($sub);
        $prevSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent'], 'intClientSubscriptions_order <' => $sub['intClientSubscriptions_order']], '*', ['intClientSubscriptions_order DESC', 'clientsubscription_ID DESC']);
        if (!$prevSub){
            if ((int) $sub['intClientSubscriptions_parent'] == 0) {
                $prevSub = $this->model->get(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent'], 'intClientSubscriptions_order >' => 9999999], '*', ['intClientSubscriptions_order DESC', 'clientsubscription_ID DESC']);
            }
            else {
                $parentSub = $this->model->get($sub['intClientSubscriptions_parent']);
                $parentSub = $this->model->get($parentSub['intClientSubscriptions_parent']);
                $prevSub = $parentSub;
            }
        }
        return $this->filterRedirect($prevSub);
    }
    public function getNextSubs($sub, $select = '*', & $trackFlg = []) {
        if (!is_array($sub)) {
            $sub = $this->model->get($sub);
        }
        $nextSiblings = $this->model->getSubscriptions(['intClientSubscription_purchased_ID' => $sub['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $sub['intClientSubscriptions_parent'], 'intClientSubscriptions_order >' => $sub['intClientSubscriptions_order']], $select);
        $validSiblings = [];
        if (isset($trackFlg[$sub['clientsubscription_ID']])) { return []; }
        $trackFlg[$sub['clientsubscription_ID']] = 1;
        foreach ($nextSiblings as $nextSibling) {
            if ( isset($trackFlg[$nextSibling['clientsubscription_ID']]) ) { return $validSiblings; }
            $rSibling = $this->filterRedirect($nextSibling, $trackFlg);
            if (!$rSibling) { return $validSiblings; }
            if ($nextSibling['clientsubscription_ID'] !== $rSibling['clientsubscription_ID']) {
                $rNextSiblings = $this->getNextSubs($rSibling, '*', $trackFlg);
                return array_merge($validSiblings, [$rSibling], $rNextSiblings);
            }
            $validSiblings[] = $nextSibling;
        }
        if ($sub['intClientSubscriptions_parent'] === '0') {
            return $nextSiblings;
        }
        $parentSub = $this->model->get($sub['intClientSubscriptions_parent']);
        $parentSub = $this->model->get($parentSub['intClientSubscriptions_parent']);

        $parentNextSiblings = $this->getNextSubs($parentSub, '*', $trackFlg);
        return array_merge($nextSiblings, $parentNextSiblings);
    }
    public function delete($where = []){
        if (!is_array($where)){
            $where = ['clientsubscription_ID' => $where];
        }
        $subscriptions = $this->model->getSubscriptions($where, 'clientsubscription_ID');
        foreach ($subscriptions as $subscription){
            $this->model->deleteSubsCompletes(['intSubscription_ID' => $subscription['clientsubscription_ID']]);
            $this->model->deleteFavorites(['intFavorite_subscription_ID' => $subscription['clientsubscription_ID']]);
        }
        $this->model->delete($where);

    }
    public function deepDelete($where = []) {
        if (!is_array($where)){
            $where = ['clientsubscription_ID' => $where];
        }
        $subscriptions = $this->model->getSubscriptions($where, 'clientsubscription_ID');

        foreach ($subscriptions as $subscription){
            $childs = $this->model->getSubscriptions(['intClientSubscriptions_parent' => $subscription['clientsubscription_ID']]);
            foreach ($childs as $child) {
                $this->deepDelete($child['clientsubscription_ID']);
            }
        }
        $this->delete($where);
    }
    public function trash($where){
        $subscription = $this->model->get($where);
        if ($subscription) {
            if (!is_null($subscription['intClientSubscription_post_ID'])){
                $post = $this->postsModel->get($subscription['intClientSubscription_post_ID']);
                $this->model->insertTrashPost(['intTrashPost_post_ID' => $post['post_ID'], 'intTrashPost_series_ID' => $post['intPost_series_ID']]);
            }
            $this->model->delete($where);
        }
        else {
            return false;
        }
    }
    public function orderSubscriptions($where){
        $purchasedId = $where['intClientSubscription_purchased_ID'];
        $subscriptions = $this->model->getSubscriptions($where, ['clientsubscription_ID', 'intClientSubscriptions_order']);
        $purchased = $this->purchasedModel->get($where['intClientSubscription_purchased_ID']);
        $returns = [];
        foreach ($subscriptions as $key => $subscription){
            $this->model->update($subscription['clientsubscription_ID'], ['intClientSubscriptions_order' => $key + 1]);
            if ($subscription['intClientSubscriptions_order'] == $purchased['intPurchased_seekOrder']){
                $this->purchasedModel->update($purchasedId, ['intPurchased_seekOrder' => $key + 1]);
            }
            $returns[$subscription['clientsubscription_ID']] = $key + 1;
        }
        return $returns;
    }
    public function formatSubscription($subscription) {
        $subscription['viewDay'] = $this->calcDay($subscription);
        if (!isset($subscription['intClientSubscription_post_ID']) || is_null($subscription['intClientSubscription_post_ID'])){
            return $subscription;
        }
        $post = $this->postsModel->get($subscription['intClientSubscription_post_ID']);

        $keyPair = [
            'strClientSubscription_title' => 'strPost_title',
            'strClientSubscription_body' => 'strPost_body',
            'strClientSubscription_image' => 'strPost_featuredimage',
            'intClientSubscriptions_type' => 'intPost_type',
            'strClientSubscription_nodeType' => 'strPost_nodeType',
            'strSubscription_duration' => 'strPost_duration',
        ];

        foreach ($subscription as $key => $value){
            if ( isset($keyPair[$key]) ) {
                $subscription[$key] = aOrb($value, $post[$keyPair[$key]]);
            }
        }

        $subsCplt = $this->subsCpltModel->get(['intSubscription_ID' => $subscription['clientsubscription_ID']], '*', 'created_datetime DESC');

        if ($subsCplt) {
            $subscription['boolCompleted'] = (int) $subsCplt['boolCompleted'];
        }
        else {
            $subscription['boolCompleted'] = 0;
        }
        return $subscription;
    }
    public function cloneItem($where) {
        $rtn = [];
        $items = $this->getSubscriptions($where);
        foreach ($items as $item) {
            unset($item['clientsubscription_ID']);
            unset($item['intClientSubscription_post_ID']);
            unset($item['boolCompleted']);
            $id = $this->model->insert($item);
            array_push($rtn, $this->model->get($id));
        }

        if (count($rtn) === 1) {
            $rtn = $rtn[0];
        }
        return $rtn;
    }
    public function synchronizeWithPost($action, $data) {
        $this->load->controller('Posts_c');
        $postsHandle = new Posts_c();
        $postSets = [];
        switch ($action) {
            case 'update':
                $sets = $data['sets'];
                $where = $data['where'];
                $sub = $this->model->get($where);
                if (is_null($sub['intClientSubscription_post_ID'])) {
                    return false;
                }
                $postSets = formatSubToPost($sets);
                unset($postSets['intPost_parent']);
                if (isset($sets['intClientSubscriptions_parent'])) {
                    if ((int)$sets['intClientSubscriptions_parent'] === 0) {
                        $postSets['intPost_parent'] = 0;
                    }
                    else {
                        $parentSub = $this->model->get($sets['intClientSubscriptions_parent']);
                        if (!is_null($parentSub['intClientSubscription_post_ID'])) {
                            $postSets['intPost_parent'] = $parentSub['intClientSubscription_post_ID'];
                        }
                    }
                }
                $postsHandle->update($postSets, $sub['intClientSubscription_post_ID']);
                break;
            case 'delete':
                $where = $data;
                $sub = $this->model->get($where);
                if (is_null($sub['intClientSubscription_post_ID'])) {
                    return false;
                }
                $postsHandle->delete($sub['intClientSubscription_post_ID']);
                break;
        }
        return $postSets;
    }
    public function calcDay($subscription) {
        if ((int) $subscription['intClientSubscriptions_parent'] === 0) {
            return (int) $subscription['intClientSubscriptions_order'];
        }
        else {
            $parent = $this->model->get($subscription['intClientSubscriptions_parent']);
            $parent = $this->model->get($parent['intClientSubscriptions_parent']);
            return (int) $subscription['intClientSubscriptions_order'] + $this->calcDay($parent);
        }
    }
}