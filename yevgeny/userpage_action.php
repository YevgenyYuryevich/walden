<?php

global $base_url;
$base_url = sprintf(
    "%s://%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME']
);

require_once("../guardian/access.php");

global $current_user;

$current_user = $_SESSION['client_ID'];

require_once 'helpers/funcs.php';
require('vendor/autoload.php');
require_once 'models/Database_m.php';

use MarcL\AmazonAPI;
use MarcL\AmazonUrlBuilder;
use Models\Database_m;
use function Helpers\address;
use function Helpers\isAbsUrl;
use function Helpers\htmlPurify;
use function Helpers\baseUrl;

class databaseHandle{
    public function __construct()
    {
        $this->db = new Database_m();

        $this->myconnection = $this->db->db;
        $this->myconnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    public function getLastSubscribe($purchased_ID){
        $query = 'SELECT * FROM tblclientsubscription 
                      INNER JOIN tblpurchased on intClientSubscription_purchased_ID = purchased_ID
                      INNER JOIN tblseries on intPurchased_series_ID = series_ID
                      INNER JOIN tblpost on intClientSubscription_post_ID = post_ID
                      WHERE intClientSubscription_purchased_ID = :purchased_ID
                      AND intClientSubscriptions_finished = 0
                      ORDER BY intClientSubscriptions_order DESC
                      LIMIT 1';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':purchased_ID' => $purchased_ID));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function getNextPost($series_id, $subscriptions_order, $purchased_ID){
        $query = 'SELECT intClientSubscription_post_ID AS post_ID FROM tblclientsubscription
                    WHERE intClientSubscription_purchased_ID = :purchased_ID
                    AND intClientSubscription_post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :purchased_id2)
                    AND intClientSubscriptions_order > :subscriptions_order
                    AND intClientSubscriptions_finished = 1
                    ORDER BY intClientSubscriptions_order LIMIT 1';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':purchased_ID' => $purchased_ID, ':subscriptions_order' => $subscriptions_order, ':purchased_id2' => $purchased_ID));
        $next_post = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$next_post){
            $query = 'SELECT post_ID FROM tblpost WHERE intPost_series_ID = :series_id AND post_ID NOT IN (SELECT intClientSubscription_post_ID FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_ID) AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :purchased_id2) ORDER BY post_ID LIMIT 1';
            $stmt = $this->myconnection->prepare( $query );
            $stmt->execute(array(':series_id' => $series_id, ':purchased_ID' => $purchased_ID, ':purchased_id2' => $purchased_ID));
            $next_post = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        return $next_post;
    }
    public function setSubscription($purchased_id, $post_id, $date_oshow, $finished = 0){
        $query = 'SELECT * FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_ID AND intClientSubscription_post_ID = :post_ID';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':purchased_ID' => $purchased_id, ':post_ID' => $post_id));
        $is_in = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($is_in){
            return $this->setFinished($is_in['clientsubscription_ID'], $finished);
        }

        $query = 'SELECT MAX(intClientSubscriptions_order) AS max_order FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute( array(':purchased_id' => $purchased_id) );
        $max_order = $stmt->fetch(PDO::FETCH_ASSOC);
        $max_order = ($max_order && $max_order['max_order'] ? $max_order['max_order'] : 0);

        if ($date_oshow == null){
            $query = 'INSERT INTO `tblclientsubscription` (`intClientSubscription_purchased_ID`, `intClientSubscription_post_ID`, `strClientSubscription_body`, `strClientSubscription_notes`, `clientsubscription_ID`, `intClientSubscriptions_finished`, `strClientSubscription_title`, `dtClientSubscription_datetoshow`, intClientSubscriptions_order) VALUES (:purchased_ID, :post_ID, NULL, NULL, NULL, :finished, NULL, NULL, :plus_order)';
            $stmt = $this->myconnection->prepare( $query );
            $rlt = $stmt->execute(array(':purchased_ID' => $purchased_id, ':post_ID' => $post_id, ':finished' => $finished, ':plus_order' => ++$max_order));
        }
        else{
            $query = 'INSERT INTO `tblclientsubscription` (`intClientSubscription_purchased_ID`, `intClientSubscription_post_ID`, `strClientSubscription_body`, `strClientSubscription_notes`, `clientsubscription_ID`, `intClientSubscriptions_finished`, `strClientSubscription_title`, `dtClientSubscription_datetoshow`, intClientSubscriptions_order) VALUES (:purchased_ID, :post_ID, NULL, NULL, NULL, :finished, NULL, :next_date, :plus_order)';
            $stmt = $this->myconnection->prepare( $query );
            $rlt = $stmt->execute(array(':purchased_ID' => $purchased_id, ':post_ID' => $post_id, ':next_date' => $date_oshow, ':finished' => $finished, ':plus_order' => ++$max_order));
        }
        return $rlt;
    }
    public function setFinished($subscription_id, $finished){
        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_finished = :finished WHERE clientsubscription_ID = :subscription_id';
        $stmt = $this->myconnection->prepare( $query );
        $rlt = $stmt->execute(array(':subscription_id' => $subscription_id, ':finished' => $finished));
        return $rlt;
    }
    public function setBeforeSubscriptionsFinished($subscription_id){

        $query = 'SELECT * FROM tblclientsubscription WHERE clientsubscription_ID = :subscription_id';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute( array(':subscription_id' => $subscription_id) );
        $subscription = $stmt->fetch(PDO::FETCH_ASSOC);

        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_finished = 1 WHERE intClientSubscription_purchased_ID = :pid AND intClientSubscriptions_order <= :from_order';
        $stmt = $this->myconnection->prepare( $query );
        $rlt = $stmt->execute(array(':pid' => $subscription['intClientSubscription_purchased_ID'], ':from_order' => $subscription['intClientSubscriptions_order']));
        return $rlt;
    }
    public function getNextSubscription($purchased_ID){
        $query = 'SELECT * FROM tblclientsubscription
                    INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
					INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
                    WHERE intClientSubscriptions_finished = 0
                    AND intClientSubscription_purchased_ID = :purchased_ID
                    AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :purchased_id2)
                    ORDER BY intClientSubscriptions_order LIMIT 1';

        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':purchased_ID' => $purchased_ID, ':purchased_id2' => $purchased_ID));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function resetSubscriptionsFromNow($purchased_id){
        $query = 'SELECT * FROM tblclientsubscription
                    INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
                    INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
					WHERE intClientSubscription_purchased_ID = :pid
					AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :pid2) ORDER BY intClientSubscriptions_order';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute( array(':pid' => $purchased_id, ':pid2' => $purchased_id) );
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $frequency = count($rows) > 0 ? $rows[0]['intSeries_frequency_ID'] : false;
        $last_date = $frequency == 0 ? null : date('Y-m-d');

        foreach ($rows as $row){
            $query = 'UPDATE tblclientsubscription SET dtClientSubscription_datetoshow = :o_date WHERE clientsubscription_ID = :cid';
            $stmt = $this->myconnection->prepare( $query );
            $stmt->execute( array(':cid' => $row['clientsubscription_ID'], ':o_date' => $last_date) );
            switch ($frequency){
                case 0:
                    $last_date = null;
                    break;
                case 1:
                    $last_date = date('Y-m-d', strtotime($last_date . ' +1 day'));
                    break;
                case 2:
                    $last_date = date('Y-m-d', strtotime($last_date . ' +7 day'));
                    break;
                default:
                    $last_date = null;
                    break;
            }
        }
    }
    public function setFinishedFirst5($purchased_ID, $start_date){
        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_finished = 0 
                    WHERE intClientSubscription_purchased_ID = :purchased_ID 
                    AND (dtClientSubscription_datetoshow IS NULL OR dtClientSubscription_datetoshow >= :start_date)
                    ORDER BY clientsubscription_ID LIMIT 5';
        $stmt = $this->myconnection->prepare( $query );
        return $stmt->execute(array(':purchased_ID' => $purchased_ID, ':start_date' => $start_date));
    }
    public function getBeforeSubscription($clientsubscription_ID, $purchased_ID, $start_date_can_display){
        $query = 'SELECT * FROM tblclientsubscription
                    INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
					INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
                    AND intClientSubscription_purchased_ID = :purchased_ID
                    AND post_ID NOT IN (SELECT intPosttrash_post_ID FROM tblposttrash WHERE intPosttrash_purchased_ID = :purchased_id2)
                    AND intClientSubscriptions_order < (SELECT intClientSubscriptions_order FROM tblclientsubscription WHERE clientsubscription_ID = :clientsubscription_ID)
                    AND (dtClientSubscription_datetoshow IS NULL OR dtClientSubscription_datetoshow >= :start_date_can_display)
                    ORDER BY intClientSubscriptions_order DESC LIMIT 1';

        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':purchased_ID' => $purchased_ID, ':clientsubscription_ID' => $clientsubscription_ID, ':start_date_can_display' => $start_date_can_display, ':purchased_id2' => $purchased_ID));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function getPostsBySeriesId($series_id){
        $query = 'SELECT * FROM tblpost WHERE intPost_series_ID = :series_id ORDER BY post_ID';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':series_id' => $series_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getSubscription($purchased_ID, $post_ID){
        $query = 'SELECT * FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_ID AND intClientSubscription_post_ID :post_ID';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':post_ID' => $post_ID, ':purchased_ID' => $purchased_ID));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function setSubscriptionsTo1($purchased_ID){
        $query = 'UPDATE tblclientsubscription SET intClientSubscriptions_finished = 1 
                    WHERE intClientSubscription_purchased_ID = :purchased_ID';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute( array( ':purchased_ID' => $purchased_ID ) );
    }
    public function getLastSubscriptionRow($purchased_ID){
        $query = 'SELECT * FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_ID ORDER BY intClientSubscriptions_order DESC LIMIT 1';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':purchased_ID' => $purchased_ID));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function makeFavorite($current_user, $subscriptionId, $series_id, $post_id){
        $query = 'INSERT INTO tblfavorites 
                    (intFavorite_subscription_ID, intFavorite_post_ID, intFavorite_series_ID, intFavorite_client_ID, dtFavorite_added)
                    VALUES (:subs, :post, :series, :client, NOW())';
        $stmt = $this->myconnection->prepare( $query );
        $rlt = $stmt->execute(array(':subs' => $subscriptionId, ':post' => $post_id, ':series' => $series_id, ':client' => $current_user));
        return $rlt ? $this->myconnection->lastInsertId() : 0;
    }
    public function getIdeaBox(){
        $query="SELECT strIdeaBox_title, strIdeaBox_image, ideabox_ID, intIdeaBox_subcategory, strIdeaBox_idea FROM tblideabox";
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getFavorites($series_id){
        $query = 'SELECT strPost_title, strPost_featuredimage, post_ID FROM tblpost
                    INNER JOIN tblfavorites ON intFavorite_post_ID = tblpost.post_ID 
                    WHERE intPost_series_ID = :series_id
                    ORDER BY intFavorites_used DESC, dtFavorite_added DESC';
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute(array(':series_id' => $series_id));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function getNewPosts($series_id, $post_id){
        $query = 'SELECT strPost_title, strPost_featuredimage, post_ID FROM tblpost WHERE intPost_series_ID = :series_id AND post_ID > :post_id';
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute(array(':series_id' => $series_id, ':post_id' => $post_id));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    public function setPostAsSubscription($subscription_id, $post_id, $purchased_id){
        $query = 'DELETE FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id AND intClientSubscription_post_ID = :post_id AND clientsubscription_ID != :subscription_id';
        $stmt = $this->myconnection->prepare ( $query );
        $stmt->execute(array(':purchased_id' => $purchased_id, ':post_id' => $post_id, ':subscription_id' => $subscription_id));

        $query = 'UPDATE tblclientsubscription SET intClientSubscription_post_ID = :post_id WHERE clientsubscription_ID = :clientsubscription_id';
        $stmt = $this->myconnection->prepare ( $query );
        $rlt = $stmt->execute(array(':clientsubscription_id' => $subscription_id, ':post_id' => $post_id));
        if ($rlt){
            $query = 'SELECT * FROM tblclientsubscription
                    INNER JOIN tblpost ON tblclientsubscription.intClientSubscription_post_ID = tblpost.post_ID
					INNER JOIN tblpurchased ON tblclientsubscription.intClientSubscription_purchased_ID = tblpurchased.purchased_ID
					INNER JOIN tblseries ON tblpurchased.intPurchased_series_ID = tblseries.series_ID
                    WHERE intClientSubscriptions_finished = 0
                    AND clientsubscription_ID = :clientsubscription_id
                    ORDER BY clientsubscription_ID LIMIT 1';
            $stmt = $this->myconnection->prepare( $query );
            $stmt->execute( array(':clientsubscription_id' => $subscription_id) );
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        else{
            return false;
        }

    }
    public function updateSubscriptionContent($subscription_id, $title, $body, $image, $type){

        $query = 'UPDATE tblclientsubscription SET strClientSubscription_title = :title, strClientSubscription_body = :body, strClientSubscription_image = :image, intClientSubscriptions_type = :cType WHERE clientsubscription_ID = :cid';
        $stmt = $this->myconnection->prepare( $query );
        return $stmt->execute( array( ':title' => $title, ':body' => $body, ':cid' => $subscription_id, ':image' => $image, ':cType' => $type) );
    }
    public function cleanSubscription($subscription_id){
        $query = 'UPDATE `tblclientsubscription` SET `strClientSubscription_body` = NULL, `strClientSubscription_title` = NULL, `strClientSubscription_image` = NULL, intClientSubscriptions_type = NULL WHERE `tblclientsubscription`.`clientsubscription_ID` = :cid';
        $stmt = $this->myconnection->prepare( $query );
        return $stmt->execute(array(':cid' => $subscription_id));
    }
    public function trashPost($purchased_id, $post_id){
        $query = 'DELETE FROM tblclientsubscription WHERE intClientSubscription_purchased_ID = :purchased_id AND intClientSubscription_post_ID = :post_id';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute( array(':purchased_id' => $purchased_id, ':post_id' => $post_id) );

        $query = 'INSERT INTO tblposttrash (posttrash_ID, intPosttrash_purchased_ID, intPosttrash_post_ID, dtPosttrash_datetrashed) VALUES (NULL, :purchased_id, :post_id, CURRENT_TIMESTAMP)';
        $stmt = $this->myconnection->prepare( $query );
        return $stmt->execute( array(':purchased_id' => $purchased_id, ':post_id' => $post_id) );
    }
    public function makeUnfavoriteByFavoriteId($favorite_id){
        $query = 'DELETE FROM tblfavorites WHERE favorite_ID = :fid';
        $stmt = $this->myconnection->prepare( $query );
        return $stmt->execute(array(':fid' => $favorite_id));
    }
    public function getFavoriteId($client_id, $series_id, $post_id){
        $query = 'SELECT favorite_ID FROM tblfavorites WHERE intFavorite_client_ID = :cid AND intFavorite_series_ID = :sid AND intFavorite_post_ID = :pid';
        $stmt = $this->myconnection->prepare( $query );
        $stmt->execute(array(':cid' => $client_id, ':sid' => $series_id, ':pid' => $post_id));
        $favorite_row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $favorite_row ? $favorite_row['favorite_ID'] : 0;
    }
    public function setPurchasedActive($purchasedId, $activeV){
        $query = 'UPDATE tblpurchased SET boolPurchased_active = :activeV WHERE purchased_ID = :purchasedId';
        $stmt = $this->myconnection->prepare( $query );
        $rlt = $stmt->execute( [':activeV' => $activeV, ':purchasedId' => $purchasedId] );
        return $rlt ? $rlt : false;
    }
    public function setSubscriptionRow($id, $sets){
        $this->db->from($this->db::SUBSCRIPTION_TNAME);
        $this->db->set($sets);
        $this->db->where('clientsubscription_ID', $id);
        return $this->db->update();
    }
    public function insertSubsComplete($subscriptionId, $completed = 0, $dateTime = false){
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->set('intSubscription_ID', $subscriptionId);
        $this->db->set('boolCompleted', $completed);
        if ($dateTime){ $this->db->set('created_datetime', $dateTime); }
        return $this->db->insert();
    }
    public function getSubsCompletesBySubsId($subscriptionId){
        $this->db->select('*');
        $this->db->from($this->db::SUBSCPLT_TNAME);
        $this->db->where('intSubscription_ID', $subscriptionId);
        $this->db->orderBy('created_datetime');
        return $this->db->get();
    }
}

class userSubscriptionHandle{
    public function __construct()
    {
        $this->db = new databaseHandle();
    }

    public function formatPost($post){

        $type = & address($post['intClientSubscriptions_type'], $post['intPost_type']);
        $body = & address($post['strClientSubscription_body'], $post['strPost_body']);

        switch ($type){
            case 0:
                $body = isAbsUrl($body) ? $body : baseUrl() . $body;
                break;
            case 2:
                break;
            case 8:
                break;
            default:
                $subBody = trim(substr($body, 0, 250));
                $fltBody = stripslashes($subBody . '...');
                $body = htmlPurify($fltBody);
                break;
        }

        $subsCompletes = $this->db->getSubsCompletesBySubsId($post['clientsubscription_ID']);
        $subsCompletes = array_reverse($subsCompletes);
        $post['isSubsCompleted'] = $subsCompletes ? $subsCompletes[0]['boolCompleted'] : 0;

        return $post;
    }

    public function stepNext(){
        $purchased_ID = $_POST['purchased_ID'];
        $post_ID = $_POST['post_ID'];
        $subscription_ID = $_POST['subscription_ID'];

        $last_subscribe = $this->db->getLastSubscribe($purchased_ID);
        $series_id = $last_subscribe['series_ID'];
        $next_to_insert = $this->db->getNextPost($series_id, $last_subscribe['intClientSubscriptions_order'], $purchased_ID);

        if ($next_to_insert){
            $last_date = $last_subscribe['dtClientSubscription_datetoshow'];
            switch ($last_subscribe['intSeries_frequency_ID']){
                case 0:
                    $datet_oshow = null;
                    break;
                case 1:
                    $datet_oshow = $last_date != null ? date('Y-m-d', strtotime($last_date . ' +1 day')) : date('Y-m-d');
                    break;
                case 2:
                    $datet_oshow = $last_date != null ? date('Y-m-d', strtotime($last_date . ' +7 day')) : date('Y-m-d');
                    break;
                default:
                    $datet_oshow = null;
                    break;
            }
            $this->db->setSubscription($purchased_ID, $next_to_insert['post_ID'], $datet_oshow);
        }
        $this->db->setBeforeSubscriptionsFinished($subscription_ID);
        $next_subscription = $this->db->getNextSubscription($purchased_ID);

        if ($next_subscription){
            $next_subscription['strPost_body'] = utf8_encode($next_subscription['strPost_body']);
            $next_subscription['strClientSubscription_body'] = utf8_encode($next_subscription['strClientSubscription_body']);
        }
        else{
            $this->db->resetSubscriptionsFromNow($purchased_ID);
            $start_date_can_display = $this->getStartDateCanDisplay($last_subscribe['intSeries_frequency_ID']);
            $this->db->setFinishedFirst5($purchased_ID, $start_date_can_display);
            $next_subscription = $this->db->getNextSubscription($purchased_ID);
            if ($next_subscription){
                $next_subscription['strPost_body'] = utf8_encode($next_subscription['strPost_body']);
                $next_subscription['strClientSubscription_body'] = utf8_encode($next_subscription['strClientSubscription_body']);
            }
        }
        global $current_user;

        $favorite_id = $this->db->getFavoriteId($current_user, $series_id, $next_subscription['post_ID']);
        $next_subscription['favorite_id'] = $favorite_id;
        $next_subscription = $this->formatPost($next_subscription);
        $return_val = array('status' => true, 'data' => $next_subscription);
        echo json_encode($return_val);

        die();
    }
    public function stepBefore(){
        $purchased_ID = $_POST['purchased_ID'];
        $series_ID = $_POST['series_ID'];
        $frequency_ID = $_POST['frequency_ID'];
        $clientsubscription_ID = $_POST['clientsubscription_ID'];


        $start_date_can_display = $this->getStartDateCanDisplay($frequency_ID);
        $before_post = $this->db->getBeforeSubscription($clientsubscription_ID, $purchased_ID, $start_date_can_display);
        if ($before_post){
            $this->db->setFinished($before_post['clientsubscription_ID'], 0);
            $before_post['strPost_body'] = utf8_encode($before_post['strPost_body']);
            $before_post['strClientSubscription_body'] = utf8_encode($before_post['strClientSubscription_body']);
        }
        else{
            $this->db->setSubscriptionsTo1($purchased_ID);
            $lastRow = $this->db->getLastSubscriptionRow($purchased_ID);
            $this->db->setFinished($lastRow['clientsubscription_ID'], 0);
            $before_post = $this->db->getNextSubscription($purchased_ID);
            $before_post['strPost_body'] = utf8_encode($before_post['strPost_body']);
            $before_post['strClientSubscription_body'] = utf8_encode($before_post['strClientSubscription_body']);
        }

        global $current_user;

        $favorite_id = $this->db->getFavoriteId($current_user, $series_ID, $before_post['post_ID']);
        $before_post['favorite_id'] = $favorite_id;
        $before_post = $this->formatPost($before_post);
        $return_val = array('status' => true, 'data' => $before_post);
        echo json_encode($return_val);

        die();
    }
    public function getStartDateCanDisplay($frequency){
        $start_date_can_display = '0';
        switch ($frequency){
            case 0:
                $start_date_can_display = '0';
                break;
            case 1:
                $start_date_can_display = date('Y-m-d');
                break;
            case 2:
                $start_date_can_display = date('Y-m-d', strtotime( date('Y-m-d') . ' -6 day' ));
                break;
            default:
                $start_date_can_display = '0';
                break;
        }
        return $start_date_can_display;
    }
    public function makeFavorite(){
        $post_id = $_POST['post_id'];
        $series_id = $_POST['series_id'];
        $subscriptionId = $_POST['subscriptionId'];
        global $current_user;

        $new_id = $this->db->makeFavorite($current_user, $subscriptionId, $series_id, $post_id);
        if ($new_id > 0){
            echo json_encode(array('status' => true, 'data' => $new_id));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function getSwitchLightboxData(){
        $post_id = $_POST['post_id'];
        $series_id = $_POST['series_id'];
        global $current_user;

        $ideaboxes = $this->getIdeaBox();
        $favorites = $this->db->getFavorites($series_id);
        $new_posts = $this->db->getNewPosts($series_id, $post_id);

        foreach ($new_posts as &$new_post){
            $new_post['strPost_title'] = utf8_encode($new_post['strPost_title']);
        }

        $return_val = array(
            'ideaboxes' => $ideaboxes,
            'favorites' => $favorites,
            'new_posts' => $new_posts
        );
        echo json_encode(array('status' => true, 'data' => $return_val));
        die();
    }
    public function getIdeaBox(){
        $rows = $this->db->getIdeaBox();
        return $rows;
    }
    public function setPostAsSubscription(){
        $subscription_id = $_POST['subscription_ID'];
        $post_id = $_POST['post_ID'];
        $purchased_id = $_POST['purchased_ID'];

        $this->db->cleanSubscription($subscription_id);
        $subscription = $this->db->setPostAsSubscription($subscription_id, $post_id, $purchased_id);
        if ($subscription){
            $subscription['strClientSubscription_body'] = utf8_encode($subscription['strClientSubscription_body']);
            $subscription['strPost_body'] = utf8_encode($subscription['strPost_body']);
            echo json_encode(array('status' => true, 'data' => $subscription));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function updateSubscriptionContent(){

        $subscription_id = $_POST['subscription_ID'];
        $title = $_POST['title'];
        $body = $_POST['body'];
        $image = $_POST['image'];
        $type = $_POST['type'];

        $rlt = $this->db->updateSubscriptionContent($subscription_id, $title, $body, $image, $type);
        if ($rlt){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function trashPost(){
        $post_id = $_POST['post_id'];
        $purchased_id = $_POST['purchased_id'];
        $rlt = $this->db->trashPost($purchased_id, $post_id);

        if ($rlt){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
    }
    public function makeUnFavorite(){
        $favorite_id = $_POST['favorite_id'];
        $rlt = $this->db->makeUnfavoriteByFavoriteId($favorite_id);
        if ($rlt){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
    }
    public function ajax_removeSeries(){
        $purchasedId = $_POST['purchasedId'];
        $this->db->setPurchasedActive($purchasedId, 0);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_completeSubscription(){
        $id = $_POST['id'];
        $this->db->insertSubsComplete($id, 1);
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_unCompleteSubscription(){
        $id = $_POST['id'];
        $this->db->insertSubsComplete($id, 0);
        echo json_encode(['status' => true]);
        die();
    }
}

class eventDatabaseClass{
    public function __construct()
    {
        include ('../config.php');
        include ('../dbconnect.php');
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        global $current_user;
        $this->current_user = $current_user;
    }

    public function createCancelledEvent($event_id, $date){
        $query = "INSERT INTO `event_instance_exception` (`event_id`, `start_date`, `is_rescheduled`, `is_cancelled`, `created_by`) VALUES (:event_id, :start_date, 0, 1, :created_by)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':start_date' => $date, ':created_by' => $this->current_user) );
        return $this->db->lastInsertId();
    }

    public function createRescheduledEvent($event_id, $data){
        $query = "INSERT INTO `event_instance_exception` (`event_id`, `event_title`, `event_description`, `event_location`, `start_date`, `start_time`, `created_by`) VALUES (:event_id, :event_title, :event_description, :event_location, :start_date, :start_time, :created_by)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':event_title' => $data['event_title'], ':event_description' => $data['event_description'], ':event_location' => $data['event_location'], ':start_date' => $data['start_date'], ':start_time' => $data['start_time'], ':created_by' => $this->current_user) );
        return $this->db->lastInsertId();
    }

    public function getRescheduledEventById($id){
        $query = 'SELECT * FROM event_instance_exception WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function createRecurringPattern($event_id, $recurring_pattern_data, $origin_event_data){

        switch ($recurring_pattern_data['recurring_type']){
            case 'd':
                $query = "INSERT INTO `recurring_pattern` (`id`, `event_id`, `recurring_type`, `separation_count`, `days_of_week`) VALUES (NULL, :event_id, :recurring_type, :separation_count, :days_of_week)";
                $stmt = $this->db->prepare(  $query );
                $stmt->execute( array(':event_id' => $event_id, ':recurring_type' => $recurring_pattern_data['recurring_type'], ':separation_count' => $recurring_pattern_data['separation_count'], ':days_of_week' => $recurring_pattern_data['days_of_week']) );
                $new_recurring_id = $this->db->lastInsertId();
                break;
            case 'w':
                $query = "INSERT INTO `recurring_pattern` (`id`, `event_id`, `recurring_type`, `separation_count`) VALUES (NULL, :event_id, :recurring_type, :separation_count)";
                $stmt = $this->db->prepare(  $query );
                $stmt->execute( array(':event_id' => $event_id, ':recurring_type' => $recurring_pattern_data['recurring_type'], ':separation_count' => $recurring_pattern_data['separation_count']) );
                $new_recurring_id = $this->db->lastInsertId();
                break;
        }


        if (isset($recurring_pattern_data['additional_event_dates'])){
            foreach ($recurring_pattern_data['additional_event_dates'] as $date){
                $rescheduled_data = array(
                    'event_title' => $origin_event_data['event_title'],
                    'event_description' => $origin_event_data['event_description'],
                    'event_location' => $origin_event_data['event_location'],
                    'start_date' => $date,
                    'start_time' => $origin_event_data['start_time']);
                $new_rescheduled_id = $this->createRescheduledEvent($event_id, $rescheduled_data);
            }
        }

        if (isset($recurring_pattern_data['omit_dates'])){
            foreach ($recurring_pattern_data['omit_dates'] as $date){
                $new_cancelled_id = $this->createCancelledEvent($event_id, $date);
            }
        }

        return $new_recurring_id;
    }

    public function updateRecurringPattern($event_id, $recurring_pattern_data){

    }

    public function createEvent($data){
        global $current_user;
        $query = "INSERT INTO `events` (`id`, `event_title`, `event_description`, `event_location`, `start_date`, `start_time`, `is_recurring`, `created_by`, `created_datetime`) VALUES (NULL, :event_title, :event_description, :event_location, :start_date, :start_time, :is_recurring, :client, CURRENT_TIMESTAMP)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute(array(':event_title' => $data['event_title'], ':event_description' => $data['event_description'], ':event_location' => $data['event_location'], ':start_date' => $data['start_date'], ':start_time' => $data['start_time'], ':is_recurring' => $data['is_recurring'] === 'true' ? 1 : 0, ':client' => $current_user));
        $new_id = $this->db->lastInsertId();
        return $new_id;
    }
    public function getEvents(){
//        $date_now = date('Y-m-d');
//        $time_now = date("H:i:s");
        $date_now = '1990-01-01';
        $time_now = date("H:i:s");

        $query = "SELECT * FROM `events` WHERE created_by = :client_id AND (start_date >= :date_now OR is_recurring = 1)";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':date_now' => $date_now, ':client_id' => $this->current_user) );
        $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $events;
    }
    public function getRecurringPattern($event_id){
        $query = 'SELECT * FROM `recurring_pattern` WHERE event_id = :event_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        $recurring_pattern = $stmt->fetch(PDO::FETCH_ASSOC);
        return $recurring_pattern;
    }
    public function getRescheduledEvents($event_id){
        $query = 'SELECT * FROM event_instance_exception WHERE event_id = :event_id AND is_cancelled = 0';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getCancelledEvents($event_id){
        $query = 'SELECT * FROM event_instance_exception WHERE event_id = :event_id AND is_cancelled = 1';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getCancelledEventById($id){
        $query = 'SELECT * FROM event_instance_exception WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function getEventById($id){
        $query = "SELECT * FROM `events` WHERE id = :id";
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function deleteRescheduledEvent($id){
        $query = 'DELETE FROM `event_instance_exception` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
    public function deleteEvent($id){
        $query = 'DELETE FROM `events` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
    public function getEventImagesByEventId($event_id){
        $query = 'SELECT * FROM event_images WHERE event_id = :event_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function insertEventImage($event_id, $image_url){
        $query = 'INSERT INTO `event_images`(`event_id`, `media_url`) VALUES (:event_id, :image_url)';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':image_url' => $image_url) );
        return $this->db->lastInsertId();
    }
    public function deleteEventImage($id){
        $query = 'DELETE FROM event_images WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
    public function updateEvent($event_id, $data){
        if ($data['start_date'] == null){
            $query = 'UPDATE `events` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_time`=:start_time,`created_by`=:client_id WHERE id = :event_id';
            $stmt = $this->db->prepare( $query );
            return $stmt->execute( array(
                ':event_id' => $event_id,
                ':event_title' => $data['event_title'],
                ':event_description' => $data['event_description'],
                ':event_location' => $data['event_location'],
                ':start_time' => $data['start_time'],
                ':client_id' => $this->current_user
            ) );
        }
        else{
            $query = 'UPDATE `events` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_date`=:start_date,`start_time`=:start_time,`created_by`=:client_id WHERE id = :event_id';
            $stmt = $this->db->prepare( $query );
            return $stmt->execute( array(
                ':event_id' => $event_id,
                ':event_title' => $data['event_title'],
                ':event_description' => $data['event_description'],
                ':event_location' => $data['event_location'],
                ':start_date' => $data['start_date'],
                ':start_time' => $data['start_time'],
                ':client_id' => $this->current_user
            ) );
        }
    }
    public function updateReschduledInstance($id, $data){
        $query = 'UPDATE `event_instance_exception` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_date`=:start_date,`start_time`=:start_time,`created_by`=:client_id WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(
            ':id' => $id,
            ':event_title' => $data['event_title'],
            ':event_description' => $data['event_description'],
            ':event_location' => $data['event_location'],
            ':start_date' => $data['start_date'],
            ':start_time' => $data['start_time'],
            ':client_id' => $this->current_user
        ) );
    }
    public function updateRescheduledInstancesByEventId($event_id, $data){
        $query = 'UPDATE `event_instance_exception` SET `event_title`=:event_title,`event_description`=:event_description,`event_location`=:event_location,`start_time`=:start_time,`created_by`=:client_id WHERE event_id = :event_id AND is_cancelled = 0';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(
            ':event_id' => $event_id,
            ':event_title' => $data['event_title'],
            ':event_description' => $data['event_description'],
            ':event_location' => $data['event_location'],
            ':start_time' => $data['start_time'],
            ':client_id' => $this->current_user
        ) );
    }
    public function insertEventFavorite($event_id, $is_rescheduled, $start_date, $instance_id){
        if ($is_rescheduled == '1'){
            $query = 'INSERT INTO `event_favorites`(`event_id`, `client_id`, `is_rescheduled`, `instance_id`) VALUES (:event_id, :client_id, :is_rescheduled, :instance_id)';
            $stmt = $this->db->prepare( $query );
            $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->current_user, ':is_rescheduled' => $is_rescheduled, ':instance_id' => $instance_id) );
            return $this->db->lastInsertId();
        }
        else{
            $query = 'INSERT INTO `event_favorites`(`event_id`, `client_id`, `is_rescheduled`, `start_date`) VALUES (:event_id, :client_id, :is_rescheduled, :start_date)';
            $stmt = $this->db->prepare( $query );
            $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->current_user, ':is_rescheduled' => $is_rescheduled, ':start_date' => $start_date) );
            return $this->db->lastInsertId();
        }
    }
    public function getFavoritesByEventId($event_id){
        $query = 'SELECT * FROM `event_favorites` WHERE event_id = :event_id AND client_id = :client_id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':event_id' => $event_id, ':client_id' => $this->current_user) );
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getFavoriteById($id){
        $query = 'SELECT * FROM `event_favorites` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        $stmt->execute( array(':id' => $id) );
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function deleteEventFavorite($id){
        $query = 'DELETE FROM `event_favorites` WHERE id = :id';
        $stmt = $this->db->prepare( $query );
        return $stmt->execute( array(':id' => $id) );
    }
}

class eventsClass{
    public function __construct()
    {
        $this->db = new eventDatabaseClass();
    }
    public function createEvent(){
        $data = $_POST['data'];
        $id = $this->db->createEvent($data);

        if ($data['is_recurring'] === 'true'){
            $this->db->createRecurringPattern($id, $data['recurring_pattern_data'], $data);
        }
        $event_data = $this->getEventById($id);
        echo json_encode(array('status' => true, 'data' => $event_data));
        die();
    }
    public function getEvents(){
        $events = $this->db->getEvents();

        foreach ($events as &$event) {
            $event['event_images'] = $this->db->getEventImagesByEventId($event['id']);
            $event['event_favorites'] = $this->db->getFavoritesByEventId($event['id']);
            $event['rescheduled_events'] = $this->db->getRescheduledEvents($event['id']);
            $event['cancelled_events'] = $this->db->getCancelledEvents($event['id']);
            if ($event['is_recurring']){
                $event['recurring_pattern'] = $this->db->getRecurringPattern($event['id']);
            }
        }
        echo json_encode(array('status' => true, 'data' => $events));
        die();
    }
    public function getEventById($id){
        $event = $this->db->getEventById($id);
        $event['event_images'] = $this->db->getEventImagesByEventId($event['id']);
        $event['event_favorites'] = $this->db->getFavoritesByEventId($event['id']);
        if ($event['is_recurring']){
            $event['recurring_pattern'] = $this->db->getRecurringPattern($event['id']);
            $event['rescheduled_events'] = $this->db->getRescheduledEvents($event['id']);
            $event['cancelled_events'] = $this->db->getCancelledEvents($event['id']);
        }
        return $event;
    }
    public function deleteRescheduledEvent(){
        $id = $_POST['id'];
        $this->db->deleteRescheduledEvent($id);
        echo json_encode(array('status' => true));
        die();
    }
    public function deleteEvent(){
        $id = $_POST['id'];
        $this->db->deleteEvent($id);
        echo json_encode(array('status' => true));
        die();
    }
    public function cancelEventInstance(){
        $data = $_POST['data'];
        $new_id = $this->db->createCancelledEvent($data['event_id'], $data['start_date']);
        $res = $this->db->getCancelledEventById($new_id);
        echo json_encode(array('status' => true, 'data' => $res));
        die();
    }
    public function addImageToEvent(){

        $event_id = $_POST['event_id'];
        $image_file = $_FILES['event_image'];
        $unique_filename = uniqid('event_image').'_' . $image_file['name'];
        $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
        $rlt = move_uploaded_file($image_file['tmp_name'], $destination);

        if ($rlt == true){
            $image_url = 'assets/images/' . $unique_filename;
            $new_id = $this->db->insertEventImage($event_id, $image_url);
            $res = array('id' => $new_id, 'event_id' => $event_id, 'media_url' => $image_url);
            echo json_encode(array('status' => true, 'data' => $res));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function deleteEventImage(){
        $id = $_POST['id'];
        $rlt = $this->db->deleteEventImage($id);
        if ($rlt){
            echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));
        }
        die();
    }
    public function rescheduleInstance(){
        $event_id = $_POST['data']['event_id'];
        $data = $_POST['data'];
        $origin_start_date = $_POST['origin_start_date'];

        $cancelled_id = $this->db->createCancelledEvent($event_id, $origin_start_date);
        $cancelled_row = $this->db->getCancelledEventById($cancelled_id);

        $rescheduled_id = $this->db->createRescheduledEvent($event_id, $data);
        $rescheduled_row = $this->db->getRescheduledEventById($rescheduled_id);

        echo json_encode(array('status' => true, 'data' => array('rescheduled_row' => $rescheduled_row, 'cancelled_row' => $cancelled_row)));
        die();
    }
    public function updateEvent(){
        $event_id = $_POST['data']['event_id'];
        $data = $_POST['data'];

        $this->db->updateEvent($event_id, $data);
        echo json_encode(array('status' => true));
        die();
    }
    public function updateReschduledInstance(){
        $id = $_POST['id'];
        $data = $_POST['data'];
        $this->db->updateReschduledInstance($id, $data);
        echo json_encode(array('status' => true));
        die();
    }
    public function updateAllInstancesOfEvent(){
        $do_reschedule = $_POST['doReschedule'];
        $data = $_POST['data'];
        $start_date = $data['start_date'];
        $data['start_date'] = null;
        $this->db->updateEvent($data['event_id'], $data);
        $this->db->updateRescheduledInstancesByEventId($data['event_id'], $data);
        switch ($do_reschedule){
            case '0':
                echo json_encode(array('status' => true));
                break;
            case '1':
                $origin_start_date = $_POST['origin_start_date'];
                $new_id = $this->db->createCancelledEvent($data['event_id'], $origin_start_date);
                $cancelled_row = $this->db->getCancelledEventById($new_id);

                $data['start_date'] = $start_date;
                $new_id = $this->db->createRescheduledEvent($data['event_id'], $data);
                $rescheduled_row = $this->db->getRescheduledEventById($new_id);

                echo json_encode(array('status' => true, 'data' => array('cancelled_row' => $cancelled_row, 'rescheduled_row' => $rescheduled_row)));
                break;
            case '2':
                $data['start_date'] = $start_date;
                $this->db->updateReschduledInstance($_POST['id'], $data);
                echo json_encode(array('status' => true));
                break;
        }
        die();
    }
    public function makeEventFavorite(){
        $event_id = $_POST['event_id'];
        $is_rescheduled = $_POST['is_rescheduled'];
        $start_date = $_POST['start_date'];
        $instance_id = null;
        if ($is_rescheduled == 1){
            $instance_id = $_POST['instance_id'];
        }
        $new_id = $this->db->insertEventFavorite($event_id, $is_rescheduled, $start_date, $instance_id);
        $new_row = $this->db->getFavoriteById($new_id);
        echo json_encode(array('status' => true, 'data' => $new_row));
        die();
    }
    public function makeEventUnFavorite(){
        $id = $_POST['id'];
        $this->db->deleteEventFavorite($id);
        echo json_encode(array('status' => true));
        die();
    }
}

class amazonDatabaseClass{
    public function __construct()
    {
        include ('../config.php');
        include ('../dbconnect.php');
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        global $current_user;
        $this->current_user = $current_user;
    }
    public function insertProduct($product_title, $product_description, $product_image, $product_large_image, $product_asin, $product_amazon_url){
        $query = 'INSERT INTO `tblProduct` (`strProduct_name`, `strProduct_description`, `strProduct_image`, `strProduct_large_img`, `strProduct_ASIN`, `strProduct_amazonURL`, `dtProduct_dateadded`) VALUES (:product_title, :product_description, :product_image, :product_large_img, :product_asin, :product_amazon_url, CURRENT_TIMESTAMP)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(
            ':product_title' => $product_title,
            ':product_description' => $product_description,
            ':product_image' => $product_image,
            ':product_large_img' => $product_large_image,
            ':product_asin' => $product_asin,
            ':product_amazon_url' => $product_amazon_url
        ) );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getProductByAsin($product_asin){
        $query = 'SELECT * FROM tblProduct WHERE strProduct_ASIN = :product_asin';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_asin' => $product_asin) );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getProductUser($product_id, $user_id = false){
        $user_id = $user_id ? $user_id : $this->current_user;
        $query = 'SELECT * FROM tblProductUsers WHERE intProductUsers_product_ID = :product_id AND intProductUsers_client_ID = :user_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_id' => $product_id, ':user_id' => $user_id) );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function insertProductUser($product_id, $user_id = false){
        $user_id = $user_id ? $user_id : $this->current_user;
        $query = 'INSERT INTO tblProductUsers (`intProductUsers_client_ID`, `intProductUsers_product_ID`) VALUES (:user_id, :product_id)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':user_id' => $user_id, ':product_id' => $product_id) );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getLatestProductPrice($product_id){
        $query = 'SELECT * FROM tblProductPrice WHERE intProductPrice_product_ID = :product_id ORDER BY dtProductPrice_date DESC, productPrice_ID DESC LIMIT 1';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':product_id' => $product_id] );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function insertProductPrice($product_id, $new_price, $new_quantity, $used_price, $used_quantity){

        $now = date('Y-m-d');
        $query = 'INSERT INTO `tblProductPrice`(`dtProductPrice_date`, `intProductPrice_newprice`, `intProductPrice_newquantity`, `intProductPrice_usedprice`, `intProductPrice_usedquantity`, `intProductPrice_product_ID`) VALUES (:now_date, :new_price, :new_quantity, :used_price, :used_quantity, :product_id)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(
            ':now_date' => $now,
            ':new_price' => $new_price,
            ':new_quantity' => $new_quantity,
            ':used_price' => $used_price,
            ':used_quantity' => $used_quantity,
            ':product_id' => $product_id
        ) );
        return $rlt ? $this->db->lastInsertId() : false;
    }
    public function getProductRowById($product_id){
        $query = 'SELECT * FROM tblProduct WHERE product_ID = :product_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_id' => $product_id) );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function getProductPricesByProductId($product_id){
        $query = 'SELECT * FROM tblProductPrice WHERE intProductPrice_product_ID = :product_id ORDER BY dtProductPrice_date, productPrice_ID';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':product_id' => $product_id) );
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function getProductIdsByClientId($client_id = false){
        $client_id = $client_id ? $client_id : $this->current_user;
        $query = 'SELECT tblProduct.product_ID AS product_ID, tblProduct.strProduct_ASIN AS strProduct_ASIN FROM tblProduct INNER JOIN tblProductUsers ON tblProductUsers.intProductUsers_product_ID = tblProduct.product_ID WHERE tblProductUsers.intProductUsers_client_ID = :client_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( array(':client_id' => $client_id) );
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function deleteProductUser($product_id, $user_id = false){
        $user_id = $user_id ? $user_id : $this->current_user;
        $query = 'DELETE From tblProductUsers WHERE intProductUsers_client_ID = :client_id AND intProductUsers_product_ID = :product_id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':client_id' => $user_id, ':product_id' => $product_id] );
        return $rlt;
    }
}

class amazonClass{
    public function __construct()
    {
        include_once('./amazonSecretKeys.php');
        $urlBuilder = new AmazonUrlBuilder(
            $keyId,
            $secretKey,
            $associateId,
            'us'
        );
        $this->amazonAPI = new AmazonAPI($urlBuilder, 'array');
        $this->db = new amazonDatabaseClass();
    }
    public function getProductInfoFromAmazonByAsin($asin, $limit_time = 10){
        $response_from_amazon = $this->amazonAPI->ItemLookUp($asin, ['EditorialReview', 'ItemAttributes', 'OfferSummary', 'Images'], true);

        if (isset($response_from_amazon->Items)){
            $data = $response_from_amazon->Items->Item;
            return [
                'product_title' => $data->ItemAttributes->Title,
                'product_description' => $data->EditorialReviews->EditorialReview->Content,
                'product_image' => $data->MediumImage->URL,
                'product_large_image' => $data->LargeImage->URL,
                'product_asin' => $asin,
                'new_quantity' => $data->OfferSummary->TotalNew,
                'new_price' => isset($data->OfferSummary->LowestNewPrice) ? $data->OfferSummary->LowestNewPrice->Amount : 0,
                'used_quantity' => $data->OfferSummary->TotalUsed,
                'used_price' => isset($data->OfferSummary->LowestUsedPrice) ? $data->OfferSummary->LowestUsedPrice->Amount : 0,
                'product_amazon_url' => $data->DetailPageURL
            ];
        }
        else{
            if ($limit_time == 0){
                return false;
            }
            return $this->getProductInfoFromAmazonByAsin($asin, $limit_time - 1);
        }
    }
    public function createNewProductByAsin($asin = false){
        if (!$asin){
            $asin = $_POST['asin'];
        }

        $product_from_db = $this->db->getProductByAsin($asin);
        $product_id = false;

        $response_from_amazon = $this->getProductInfoFromAmazonByAsin($asin);

        if ($response_from_amazon){
            $product_title = $response_from_amazon['product_title'];
            $product_description = $response_from_amazon['product_description'];
            $product_image = $response_from_amazon['product_image'];
            $product_large_image = $response_from_amazon['product_large_image'];
            $product_asin = $asin;
            $new_price = $response_from_amazon['new_price'];
            $new_quantity = $response_from_amazon['new_quantity'];
            $used_quantity = $response_from_amazon['used_quantity'];
            $used_price = $response_from_amazon['used_price'];
            $product_amazon_url = $response_from_amazon['product_amazon_url'];
        }
        else{
            echo json_encode(array('status' => false, 'data' => 'no item'));
            die();
        }

        if ($product_from_db !== false){
            $product_id = $product_from_db['product_ID'];
        }
        else{
            $product_id = $this->db->insertProduct($product_title, $product_description, $product_image, $product_large_image, $product_asin, $product_amazon_url);
        }
        if (!$product_id){
            echo json_encode(array('status' => false, 'not insert product'));
            die();
        }
        $product_user = $this->db->getProductUser($product_id);
        if (!$product_user){
            $this->db->insertProductUser($product_id);
        }
        $this->insertProductPriceWithValidate($product_id, $new_price, $new_quantity, $used_price, $used_quantity);

        $res_data = $this->getFormatDataByProductId($product_id);
        echo json_encode(array('status' => true, 'data' => $res_data));
        die();
    }

    public function insertProductPriceWithValidate($product_id, $new_price, $new_quantity, $used_price, $used_quantity){
        $latest_row = $this->db->getLatestProductPrice($product_id);
        if ($latest_row){
            $new_price = (int)$new_quantity > 0 ? $new_price : ((int)$new_price > 0 ? $new_price : $latest_row['intProductPrice_newprice']);
            $used_price = (int)$used_quantity > 0 ? $used_price : ((int)$used_price > 0 ? $used_price : $latest_row['intProductPrice_usedprice']);
        }
        else{
            $new_price = (int)$new_quantity > 0 ? $new_price : ((int)$new_price > 0 ? $new_price : $used_price);
            $used_price = (int)$used_quantity > 0 ? $used_price : ((int)$used_price > 0 ? $used_price : $new_price);
        }
        return $this->db->insertProductPrice($product_id, $new_price, $new_quantity, $used_price, $used_quantity);
    }

    public function getFormatDataByProductId($product_id){
        $product_row = $this->db->getProductRowById($product_id);
        if (!$product_row){ return false; }
        $product_row['price_rows'] = $this->db->getProductPricesByProductId($product_id);
        $product_row['recommendation'] = $this->getRecommendation($product_row['price_rows']);
        $product_row['strProduct_large_img'] = $product_row['strProduct_large_img'] != null ? $product_row['strProduct_large_img'] : $product_row['strProduct_image'];
        return $product_row;
    }
    public function getAmazonData(){
        $product_ids = $this->db->getProductIdsByClientId();
        $amazon_data = array();
        foreach ($product_ids as $product_id){
            $new_data = $this->getProductInfoFromAmazonByAsin($product_id['strProduct_ASIN']);
            if ($new_data){
                $this->insertProductPriceWithValidate($product_id['product_ID'], $new_data['new_price'], $new_data['new_quantity'], $new_data['used_price'], $new_data['used_quantity']);
            }
            $amazon_data[] = $this->getFormatDataByProductId($product_id['product_ID']);
        }
        echo json_encode(['status' => true, 'data' => $amazon_data]);
        die();
    }
    public function getRecommendation($price_rows){
        include_once("utils.php");
        $vals = [];
        foreach ($price_rows as $price_row){
            $vals[] = $price_row['intProductPrice_newprice'] / 100;
        }
        if (count($vals) < 2){ return 'Buy Now'; }

        $std = stddev($vals);
        $stderr = $std / sqrt(count($vals));
        $CI = $stderr*ttable((count($vals)-1),0.025);
        $mean = array_sum($vals) / count($vals);
        $lowval = $mean - $CI;
        $highval = $mean + $CI;
        $latest_price = $vals[count($vals) - 1];

        if ($latest_price < $lowval){
            return 'Buy Now';
        }
        elseif ($lowval <= $latest_price && $latest_price <= $highval){
            return 'No recommendation';
        }
        else{
            return 'Wait';
        }
    }
    public function closeProduct(){
        $product_id = $_POST['product_id'];
        $rlt = $this->db->deleteProductUser($product_id);
        if ($rlt){
            echo json_encode(['status' => true]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
}

class goalDataBaseClass{
    public function __construct()
    {
        include ('../config.php');
        include ('../dbconnect.php');
        $this->db = $myconnection;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        global $current_user;
        $this->current_user = $current_user;
    }
    public function insertGoal($goal_text, $client_id = false){
        if (!$client_id){ $client_id = $this->current_user; }
        $query = 'INSERT INTO `tblGoals`(`strGoal_text`, `dtGoal_datecreated`, `intGoal_client_ID`) VALUES (:goal_text, :now_date, :client_id)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [
            ':goal_text' => $goal_text,
            ':now_date' => date('Y-m-d'),
            ':client_id' => $client_id
        ] );
        return $rlt ? $this->db->lastInsertId() : 0;
    }
    public function insertClientMeta($meta_key, $meta_value, $client_id = false){
        if (!$client_id){ $client_id = $this->current_user; }
        $query = 'INSERT INTO `tblclientmeta`(`client_id`, `meta_key`, `meta_value`) VALUES (:client_id, :meta_key, :meta_value)';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':client_id' => $client_id,
            ':meta_key' => $meta_key,
            ':meta_value' => $meta_value
        ]);
        return $rlt ? $this->db->lastInsertId() : 0;
    }
    public function getGoals(){
        $query = 'SELECT * FROM tblGoals WHERE intGoal_client_ID = :client_id AND dtGoal_datecreated = :now_date';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':client_id' => $this->current_user,
            ':now_date' => date('Y-m-d')
        ]);
        return $rlt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;
    }
    public function getGoalById($id){
        $query = 'SELECT * FROM tblGoals WHERE goal_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':id' => $id
        ]);
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    public function updateGoal($id, $goal_text, $goal_complete){
        $query = 'UPDATE `tblGoals` SET `strGoal_text` = :goal_text, `boolGoal_complete` = :goal_complete WHERE `goal_ID` = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':id' => $id,
            ':goal_text' => $goal_text,
            ':goal_complete' => $goal_complete
        ]);
        return $rlt;
    }
    public function deleteGoal($id){
        $query = 'DELETE FROM `tblGoals` WHERE `goal_ID` = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':id' => $id
        ]);
        return $rlt;
    }
}

class goalClass{
    public function __construct()
    {
        $this->db = new goalDataBaseClass();
    }
    public function createGoal(){
        $goal_text = $_POST['goal_text'];
        $new_id = $this->db->insertGoal($goal_text);
        $goal = $this->db->getGoalById($new_id);
        echo json_encode(['status' => true, 'data' => $goal]);
        die();
    }
    public function notNewGoalAgain(){
        $new_id = $this->db->insertClientMeta('show_goal_popup_everyday', 'false');
        if ($new_id){
            echo json_encode(['status' => true, 'data' => $new_id]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
    public function getGoals(){
        $goals = $this->db->getGoals();
        echo json_encode([
            'status' => true,
            'data' => $goals
        ]);
        die();
    }
    public function updateGoal(){
        $goal_id = $_POST['id'];
        $goal_text = $_POST['text'];
        $goal_complete = $_POST['complete'];
        $rlt = $this->db->updateGoal($goal_id, $goal_text, $goal_complete);
        if ($rlt){
            echo json_encode(['status' => true]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
    public function deleteGoal(){
        $id = $_POST['id'];
        $rlt = $this->db->deleteGoal($id);
        if ($rlt){
            echo json_encode(['status' => true]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
}

if (!isset($_POST['action'])){
    echo json_encode('no');
    die();
}

$userSubscriptionHandle = new userSubscriptionHandle();
$eventsHanlde = new eventsClass();
$amazonHandle = new amazonClass();
$goalHandle = new goalClass();

switch ($_POST['action']){
    case 'step_next':
        $userSubscriptionHandle->stepNext();
        break;
    case 'step_before':
        $userSubscriptionHandle->stepBefore();
        break;
    case 'make_favorite':
        $userSubscriptionHandle->makeFavorite();
        break;
    case 'make_unfavorite':
        $userSubscriptionHandle->makeUnFavorite();
        break;
    case 'remove_series':
        $userSubscriptionHandle->ajax_removeSeries();
        break;
    case 'get_switch_lightbox_data':
        $userSubscriptionHandle->getSwitchLightboxData();
        break;
    case 'set_post_as_subscription':
        $userSubscriptionHandle->setPostAsSubscription();
        break;
    case 'update_subscription_content':
        $userSubscriptionHandle->updateSubscriptionContent();
        break;
    case 'trash_post':
        $userSubscriptionHandle->trashPost();
        break;
    case 'complete_subscription':
        $userSubscriptionHandle->ajax_CompleteSubscription();
        break;
    case 'unComplete_subscription':
        $userSubscriptionHandle->ajax_unCompleteSubscription();
        break;
    case 'create_event':
        $eventsHanlde->createEvent();
        break;
    case 'getEvents':
        $eventsHanlde->getEvents();
        break;
    case 'delete_rescheduled_event':
        $eventsHanlde->deleteRescheduledEvent();
        break;
    case 'delete_event':
        $eventsHanlde->deleteEvent();
        break;
    case 'cancel_event_instance':
        $eventsHanlde->cancelEventInstance();
        break;
    case 'add_image_to_event':
        $eventsHanlde->addImageToEvent();
        break;
    case 'delete_event_image':
        $eventsHanlde->deleteEventImage();
        break;
    case 'reschedule_instance':
        $eventsHanlde->rescheduleInstance();
        break;
    case 'update_event':
        $eventsHanlde->updateEvent();
        break;
    case 'update_reschduled_instance':
        $eventsHanlde->updateReschduledInstance();
        break;
    case 'update_all_instances_of_event':
        $eventsHanlde->updateAllInstancesOfEvent();
        break;
    case 'make_event_favorite':
        $eventsHanlde->makeEventFavorite();
        break;
    case 'make_event_unfavorite':
        $eventsHanlde->makeEventUnFavorite();
        break;
    case 'create_new_product':
        $amazonHandle->createNewProductByAsin();
        break;
    case 'get_amazon_data':
        $amazonHandle->getAmazonData();
        break;
    case 'close_product':
        $amazonHandle->closeProduct();
        break;
    case 'get_goals':
        $goalHandle->getGoals();
        break;
    case 'create_goal':
        $goalHandle->createGoal();
        break;
    case 'not_new_goal_again':
        $goalHandle->notNewGoalAgain();
        break;
    case 'update_goal':
        $goalHandle->updateGoal();
        break;
    case 'delete_goal':
        $goalHandle->deleteGoal();
    default:
        break;
}