<?php

require_once __DIR__ . '/../core/Controller_core.php';

class Posts extends \Core\Controller_core
{
    private $authController;
    private $user;
    private $model;
    private $postsController;

    public function __construct()
    {
        parent::__construct();
        $this->load->controller('Auth_c');
        $this->authController = new \Controllers\Auth_c();
        $token = isset($_GET['auth_token']) ? $_GET['auth_token'] : (isset($_POST['auth_token']) ? $_POST['auth_token'] : false);
        $this->user = $this->authController->filterValidToken($token);
        $this->load->model('api_m/Posts_m');
        $this->model = new \Models\api\Posts_m();
        $this->load->controller('Posts_c');
        $this->postsController = new \Controllers\Posts_c();
    }
    public function getRows() {
        $where = $_POST['where'];
        $select = isset($_POST['select']) ? $_POST['select'] : '*';
        $rows = $this->model->getPosts($where, $select);
        echo json_encode($rows);
        die();
    }
    public function insert() {
        $sets = $_POST['sets'];
        $id = $this->postsController->insert($sets);
        $row = $this->model->get($id);
        echo json_encode($row);
        die();
    }
    public function update() {
        $where = $_POST['where'];
        $sets = $_POST['sets'];
        $this->postsController->update($sets, $where);
        $row = $this->model->get($where);
        echo json_encode($row);
        die();
    }
}

$handle = new Posts();

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'get_rows':
            $handle->getRows();
            break;
        case 'insert':
            $handle->insert();
            break;
        case 'update':
            $handle->update();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}