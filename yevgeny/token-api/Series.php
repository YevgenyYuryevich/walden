<?php

require_once __DIR__ . '/../core/Controller_core.php';

class Series extends \Core\Controller_core
{
    private $controller;
    private $authController;
    private $model;
    private $purchasedModel;
    private $user;
    public function __construct()
    {
        parent::__construct();
        $this->load->controller('Auth_c');
        $this->authController = new \Controllers\Auth_c();
        $token = isset($_GET['auth_token']) ? $_GET['auth_token'] : (isset($_POST['auth_token']) ? $_POST['auth_token'] : false);
        $this->user = $this->authController->filterValidToken($token);

        $this->load->controller('Series_c');
        $this->controller = new \Controllers\Series_c();

        $this->load->model('api_m/Series_m');
        $this->model = new \Models\api\Series_m();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();
    }
    public function get() {
    }
    public function getSeriesJoined() {
        $purchased = $this->purchasedModel->getRows(['intPurchased_client_ID' => $this->user['id']]);
        $series = [];
        foreach ($purchased as $value) {
            $row = $this->model->get(['series_ID' => $value['intPurchased_series_ID']]);
            if ($row) {
                $row['purchased_id'] = $value['purchased_ID'];
                $series[] = $row;
            }
        }
        echo json_encode($series);
        die();
    }
    public function getSeriesCreated() {
        $rows = $this->model->getRows(['intSeries_client_ID' => $this->user['id']]);
        echo json_encode($rows);
        die();
    }
    public function insertNew() {
        $sets = $_POST['sets'];
        $sets['intSeries_client_ID'] = $this->authController->user['id'];
        $id = $this->controller->insert($sets);
        $row = $this->model->get($id);
        echo json_encode($row);
        die();
    }
}
$handle = new Series();

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'get_rows_joined':
            $handle->getSeriesJoined();
            break;
        case 'get_rows_created':
            $handle->getSeriesCreated();
            break;
        case 'insert':
            $handle->insertNew();
            break;
        default:
            $handle->index();
            break;
    }
}
else{
    $handle->index();
}