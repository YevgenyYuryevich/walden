<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 16.03.2018
 * Time: 21:04
 */

require_once __DIR__ . '/../helpers/funcs.php';
require_once __DIR__ . '/../models/api_m/Goals_m.php';
require_once __DIR__ . '/../models/api_m/Purchased_m.php';
require_once __DIR__ . '/../models/api_m/User_m.php';
require_once __DIR__ . '/../controllers/Goals_c.php';

use Controllers\Goals_c;
use Models\api\{Goals_m, User_m};
use function Helpers\htmlMailHeader;

class WeeklyCron
{
    public function __construct()
    {
        $this->goalsHandle = new Goals_c();
        $this->userModel = new User_m();
        $this->goalsModel = new Goals_m();
    }
    public function index(){
        $this->filterGoalsComplte();
    }
    public function filterGoalsComplte(){
        $users = $this->userModel->getUsers();
        foreach ($users as $user){
            $this->filterUserGoalsComplte($user);
        }
    }
    public function filterUserGoalsComplte($user){
        $lastWeekMonday = date('Y-m-d', strtotime('last week monday'));
        $lastSunday = date('Y-m-d', strtotime('last sunday'));
        $where = [
            'dtGoal_date' => -1,
            'dtGoal_date >=' => $lastWeekMonday,
            'dtGoal_date <=' => $lastSunday,
            'intGoal_client_ID' => $user['id'],
        ];
        $totCnt = $this->goalsModel->countMainGoals($where);
        $where['boolGoal_complete'] = 1;
        $cpltCnt = $this->goalsModel->countMainGoals($where);
        if ($cpltCnt >= 5 && $cpltCnt / $totCnt * 100 >= 80){
            $headers = htmlMailHeader();
            $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/congrats_80_week.html');
            mail($user['email'], 'Greate Week', $msg, $headers);
        }
    }
}

$handle = new WeeklyCron();
$handle->index();