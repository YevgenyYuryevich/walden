<?php

require_once __DIR__ . '/../core/Controller_core.php';

class StripeWebHookCron extends \Core\Controller_core
{
    private $stripeSubscriptionModel;
    private $userModel;
    private $seriesModel;
    private $purchasedModel;
    private $optionsModel;
    private $stripeController;
    private $checkoutSessionSeriesModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/StripeSubscriptions_m');
        $this->stripeSubscriptionModel = new \Models\api\StripeSubscriptions_m();

        $this->load->model('api_m/User_m');
        $this->userModel = new \Models\api\User_m();

        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->controller('Stripe_c');
        $this->stripeController = new \Controllers\Stripe_c();

        $this->load->model('api_m/CheckoutSession_Series_m');
        $this->checkoutSessionSeriesModel = new \Models\api\CheckoutSession_Series_m();
    }
    public function index() {
        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }
        switch ($event->type) {
            case 'invoice.payment_succeeded':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $this->transferSeriesFunds($data);
                break;
            case 'checkout.session.completed':
                $data = $event->data->object;
                $this->fulFillSeriesJoin($data);
                break;
            default:
                // Unexpected event type
                http_response_code(400);
                exit();
        }
    }
    public function transferSeriesFunds($data) {
        $amount = (int) $data->amount_paid;
        $subscription = $data->subscription;
        $subscription = $this->stripeSubscriptionModel->get($subscription);
        $series = $this->seriesModel->get($subscription['intSubscription_series_ID']);
        $ownerId = $series['intSeries_client_ID'];
        $seriesOwner = $this->userModel->get($ownerId);

        if ($series['intSeries_application_fee']) {
            $series['intSeries_application_fee'] = (int) $series['intSeries_application_fee'];
            $fee = (int) ($series['intSeries_application_fee'] * $amount / 100);
        }
        else {
            $feeRow = $this->optionsModel->get(['strOption_name' => 'series_application_fee']);
            $series['intSeries_application_fee'] = $feeRow ? (int) $feeRow['strOption_value'] : 5;
            $fee = (int) ($series['intSeries_application_fee'] * $amount / 100);
        }
        $amount -= $fee;
        if ($seriesOwner['stripe_account_id']) {
            $res = [];
            if (((int) $series['boolSeries_affiliated']) && !is_null($subscription['strSubscription_affiliate']) && $subscription['strSubscription_affiliate']) {
                $affiliate = $subscription['strSubscription_affiliate'];
                $affiliateUser = $this->userModel->get(['affiliate_id' => $affiliate]);
                if ($affiliateUser) {
                    if (!is_null($affiliateUser['stripe_account_id']) && $affiliateUser['stripe_account_id']) {
                        $affiliateTransfer = $this->stripeController->createTransfer([
                            'amount' => (int) $series['intSeries_affiliate_price'] * 100,
                            'currency' => 'usd',
                            'destination' => $affiliateUser['stripe_account_id']
                        ]);
                        $amount -= (int) $series['intSeries_affiliate_price'] * 100;
                        $res['affiliate_transfer'] = $affiliateTransfer;
                    }
                }
            }
            $res['series_owner_transfer'] = $this->stripeController->createTransfer([
                'amount' => $amount,
                'currency' => 'usd',
                'destination' => $seriesOwner['stripe_account_id']
            ]);
            echo json_encode(['status' => true, 'data' => $res]);
        } else {
            echo json_encode(['status' => false, 'error' => 'No Stripe Account for the owner of series']);
        }
        die();
    }
    public function fulFillSeriesJoin($data) {
        $sessionID = $data['id'];
        $checkoutSessionSeries = $this->checkoutSessionSeriesModel->get($sessionID);
        $user = $checkoutSessionSeries['user_id'];
        $seriesItems = explode(',', $checkoutSessionSeries['series']);
        $purchasedRows = [];
        foreach ($seriesItems as $series) {
            $sets = [
                'intPurchased_client_ID' => $user,
                'intPurchased_series_ID' => $series,
                'boolPurchased_active' => 1,
            ];
            $row = $this->purchasedModel->insert($sets);
            array_push($purchasedRows, $row);
            $this->transferFundsToSeriesOwners($series);
        }
        $this->checkoutSessionSeriesModel->update($sessionID, ['status' => 'achieve']);
        echo json_encode(['data' => $purchasedRows]);
        die();
    }
    public function transferFundsToSeriesOwners($series) {
        $amount = (int) $series['intSeries_price'];
        if ($series['intSeries_application_fee']) {
            $series['intSeries_application_fee'] = (int) $series['intSeries_application_fee'];
            $fee = (int) ($series['intSeries_application_fee'] * $amount / 100);
        }
        else {
            $feeRow = $this->optionsModel->get(['strOption_name' => 'series_application_fee']);
            $series['intSeries_application_fee'] = $feeRow ? (int) $feeRow['strOption_value'] : 5;
            $fee = (int) ($series['intSeries_application_fee'] * $amount / 100);
        }
        $amount = (int) ($amount - $fee);

        $seriesOwner = $this->userModel->get( $series['intSeries_client_ID']);
        if ($seriesOwner && $seriesOwner['stripe_account_id']) {
            $this->stripeController->createTransfer([
                'amount' => $amount,
                'currency' => 'usd',
                'destination' => $seriesOwner['stripe_account_id']
            ]);
        }
    }
}
$handle = new StripeWebHookCron();
$handle->index();
