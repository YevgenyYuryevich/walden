<?php

namespace Helpers;

require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/vendor/autoload.php';

function rootPath(){
    return realpath($_SERVER['DOCUMENT_ROOT']);
}
function baseUrl(){
    return sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
}
function currentPage(){
    $rUri = $_SERVER['REQUEST_URI'];
    $rUri = str_replace('/', '', $rUri);
    $rUri = str_replace('.php', '', $rUri);
    return $rUri;
}

define('_ROOTPATH_', rootPath());
define('ASSETS_PATH', _ROOTPATH_ . '/assets');
define('BASE_URL', baseUrl());
define('CURRENT_PAGE', currentPage());
define('PRIMARY_EMAIL', 'nbaca@mtnlegal.com');

function &address(&$a, &$b, $allowEmpty = false){
    if ($allowEmpty){
        if (isset($a)){
            return $a;
        }
        return $b;
    }
    else{
        if (isset($a) && $a){
            return $a;
        }
        return $b;
    }
}

function aOrb($a, $b, $allowEmpty = false){
    if ($allowEmpty){
        return isset($a) ? $a : $b;
    }
    return isset($a) && $a ? $a : $b;
}

function isAbsUrl($url){
    return preg_match('/^https?\:\/\//', $url, $matches);
}
function absUrl($url) {
    return isAbsUrl($url) ? $url : BASE_URL . '/' . $url;
}
function utf8Encode(& $arr){
    if (!is_array($arr)){
        $arr = utf8_encode($arr);
        return $arr;
    }
    foreach ($arr as &$value){
        $value = utf8Encode($value);
    }
    return $arr;
}

function isImage($url, & $fileName, & $extension) {
    $imgExtens = ['jpg', 'png', 'gif', 'tiff'];
    foreach ($imgExtens as $imgExten){
        $pattern = sprintf('@((?P<filename>[^\/]+)\.(?P<extension>%s))$@', $imgExten);
        $f = preg_match($pattern, $url, $matches);
        if ($f){
            $extension = $matches['extension'];
            $fileName = $matches['filename'];
            return true;
        }
    }
    return false;
}

function htmlPurify($dirtyHtml){
    $config = \HTMLPurifier_Config::createDefault();
    $defConfig = $config->getHTMLDefinition(true);

    $defConfig->addElement('section', 'Block', 'Flow', 'Common');
    $defConfig->addElement('header', 'Block', 'Flow', 'Common');
    $defConfig->addElement('footer', 'Block', 'Flow', 'Common');
    $defConfig->addElement('article', 'Block', 'Flow', 'Common');
    $defConfig->addElement('aside', 'Block', 'Flow', 'Common');
    $defConfig->addElement('event-source', 'Block', 'Flow', 'Common');
    $defConfig->addElement('datagrid', 'Block', 'Flow', 'Common');
    $defConfig->addElement('video', 'Block', 'Flow', 'Common');
    $defConfig->addElement('label', 'Block', 'Flow', 'Common');

    $defConfig->addAttribute('div', 'id', 'CDATA#');

    $defConfig->addAttribute('video', 'id', 'CDATA#');
    $defConfig->addAttribute('video', 'data-autoresize', 'CDATA#');
    $defConfig->addAttribute('video', 'width', 'CDATA#');
    $defConfig->addAttribute('video', 'height', 'CDATA#');
    $defConfig->addAttribute('video', 'data-youtube-id', 'CDATA#');

//    img lightbox start
    $defConfig->addAttribute('a', 'data-title', 'CDATA#');
    $defConfig->addAttribute('a', 'data-lightbox', 'CDATA#');
//    img lightbox end

    $defConfig->addAttribute('span', 'id', 'CDATA#');
    $defConfig->addAttribute('section', 'id', 'CDATA#');
    $defConfig->addAttribute('img', 'id', 'CDATA#');
//    form-fields start
    $defConfig->addAttribute('div', 'contenteditable', 'CDATA#');
    $defConfig->addAttribute('div', 'data-form-field', 'CDATA#');
//    form-field end
    $purifier = new \HTMLPurifier($config);
    return $purifier->purify($dirtyHtml);
}

function strongEncrypt($password){
    $strong1 = hash_pbkdf2('haval192,4', $password, 'nbNB12_+' . substr($password, 0, 3), 50);
    $strong2 = hash_pbkdf2('gost-crypto', $strong1, 'yvgYVG1@<[~' . substr($password, 3), 100);
    $strong3 = substr($strong2, 1) . $strong2[0];
    return $strong3;
}

function binarySearch($elem, $array)
{
    $top = sizeof($array) -1;
    $bot = 0;
    while($top >= $bot)
    {
        $p = floor(($top + $bot) / 2);
        if ($array[$p] < $elem) $bot = $p + 1;
        elseif ($array[$p] > $elem) $top = $p - 1;
        else return TRUE;
    }
    return FALSE;
}
function arrayValuesByKey($arr, $key){
    $vals = [];
    foreach ($arr as $item){
        $vals[] = $item[$key];
    }
    return $vals;
}
function filterUnsetArr(& $arr, $unsetVal = -1){
    if (!is_array($arr)){
        return $arr;
    }
    foreach ($arr as $key => $val){
        if (is_array($val)){
            $arr[$key] = $val = filterUnsetArr($val, $unsetVal);
        }
        if ($val == $unsetVal){
            unset($arr[$key]);
        }
    }
    return $arr;
}
function getRightType($body, $type){
//        is video
    $isVideo = preg_match('@^(https://www.youtube.com/embed/)@', $body, $matches);
    if ($isVideo){
        return 2;
    }
//        is audio
    $isMyAudio = preg_match('@^([\w\/\.\-\:]+)((\.mp3)|(play))$@', $body, $matches);
    if ($isMyAudio){
        return 0;
    }

//          is image
    $isImage = preg_match('@^(\s*\<section\s+id=[\'\"]photos[\'\"]\>)@', $body);
    if ($isImage){
        return 8;
    }
    return $type;
}
function htmlMailHeader($fromName = 'Walden.ly', $fromEmail = 'nbaca@mtnlegal.com', $replyEmail = 'nbaca@mtnlegal.com'){
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";
    $headers .= "From: " . $fromName . " <" . $fromEmail . ">\r\n";
    $headers .= "Reply-To: <" . strip_tags($replyEmail) . ">\r\n";
    return $headers;
}

function putContents($orgUrl, $name = 'rand.jpg', $where = 'assets/images', $prefix = 'global'){
    $name = preg_replace('/[\?\/\#\s]/', '-', $name);
    $uniqueFilename = uniqid(str_replace(' ', '_', $prefix)).'_' . urlencode($name);
    $savedUrl = _ROOTPATH_ . '/' . $where . '/' . urlencode($uniqueFilename);
    file_put_contents($savedUrl, fopen($orgUrl, 'r'));
    return $where . '/' . urlencode($uniqueFilename);
}

function uploadFile($file, $sets = []){
    $defSets = [
        'where' => 'assets/images',
        'prefix' => 'global',
        'unique' => true
    ];
    $sets = array_merge($defSets, $sets);
    $unique_filename = ($sets['unique'] ? uniqid($sets['prefix']) : $sets['prefix']) . '_' . $file['name'];
    $destination = _ROOTPATH_ . '/' . $sets['where'] . '/' . $unique_filename;
    $rlt = move_uploaded_file($file['tmp_name'], $destination);
    if ($rlt) {
        $unique_filename = rawurlencode($unique_filename);
        return [
            'path' => $destination,
            'abs_url' => BASE_URL . '/' . $sets['where'] . '/' . $unique_filename,
            'url' => $sets['where'] . '/' . $unique_filename
        ];
    }
    else {
        return false;
    }
}
function parseStringToArray($data){
    $data = json_decode($data);
    return json_decode(json_encode($data), true);
}
function getApiTypeFromUrl($url){
    $isYoutube = preg_match('@^(https://www.youtube.com)@', $url);
    if ($isYoutube){
        return 'youtube';
    }

    $isLocal = preg_match('@^('. BASE_URL .')@', $url);
    if ($isLocal){
        return 'local';
    }
    return 'other';
}
function parseYoutubeUrl($url) {
    $rlt = preg_match('@(embed/(?P<video_id>[^/]+))$@', $url, $matches);
    if ($rlt){
        return [
            'id' => $matches['video_id'],
            'type' => 'embed'
        ];
    }
    $rlt = preg_match('@(watch\?v\=(?P<video_id>[^/]+))$@', $url, $matches);
    if ($rlt){
        return [
            'id' => $matches['video_id'],
            'type' => 'watch'
        ];
    }
}

function input() {
    $rOptions = getRequestOptions();
    $input = $_POST;
    if ($rOptions['contentType'] == 'multipart/form-data'){
        foreach ($input as & $filed) {
            if (json_decode($filed)){
                $filed = json_decode(json_encode(json_decode($filed)), true);
            }
        }
    }
    return $input;
}
function getRequestOptions() {
    $headers = getallheaders();
    $contentType = $headers['Content-Type'];
    $rtn = [];
    if (preg_match('@^(multipart/form-data;)@', $contentType)){
        $rtn['contentType'] = 'multipart/form-data';
    }
    elseif (preg_match('@^(text/html;)@', $contentType)){
        $rtn['contentType'] = 'text/html';
    }
    else {
        $rtn['contentType'] = 'text/html';
    }
    return $rtn;
}

define('POST_SUBS_PAIR', [
    'strPost_title' => 'strClientSubscription_title',
    'strPost_body' => 'strClientSubscription_body',
    'strPost_featuredimage' => 'strClientSubscription_image',
    'intPost_type' => 'intClientSubscriptions_type',
    'intPost_order' => 'intClientSubscriptions_order',
    'strPost_nodeType' => 'strClientSubscription_nodeType',
    'intPost_parent' => 'intClientSubscriptions_parent'
]);

function formatPostToSubs($sets) {
    $subsSets = [];
    foreach (POST_SUBS_PAIR as $key => $subsKey) {
        if (isset($sets[$key])) {
            $subsSets[$subsKey] = $sets[$key];
        }
    }
    return $subsSets;
}

function formatSubToPost($sets) {
    $postSets = [];
    foreach (POST_SUBS_PAIR as $key => $subsKey) {
        if (isset($sets[$subsKey])) {
            $postSets[$key] = $sets[$subsKey];
        }
    }
    return $postSets;
}
function strToAscii($str) {
    $arr = [];
    for ( $pos = 0; $pos < strlen($str); $pos ++ ) {
        $byte = substr($str, $pos);
        $arr[] = ord($byte);
    }
    return $arr;
}
function asciiToStr($arr) {
    $str = '';
    foreach ($arr as $v) {
        $str .= chr((int)$v);
    }
    return $str;
}
function asciiEncode($a) {
    return ($a * 1 + 133) % 255;
}
function asciiDecode($a) {
    return (255 + $a * 1 - 133) % 255;
}
function pushEncode($str) {
    $arr = strToAscii($str);
    $encodeArr = [];
    foreach ($arr as $v) {
        $encodeArr[] = asciiEncode($v);
    }
    $encodeArr = array_reverse($encodeArr);
    return asciiToStr($encodeArr);
}
function pushDecode($str) {
    $arr = strToAscii($str);
    $decodeArr = [];
    foreach ($arr as $v) {
        $decodeArr[] = asciiDecode($v);
    }
    $decodeArr = array_reverse($decodeArr);
    return asciiToStr($decodeArr);
}

function jwtEncode($plainText) {
    return \Firebase\JWT\JWT::encode($plainText, 'wa!den=p@rf$ct');
}

function jwtDecode($hashedText) {
    return \Firebase\JWT\JWT::decode($hashedText, 'wa!den=p@rf$ct', ['HS256']);
}

function postCurl($url, $args) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);
    return parseStringToArray($server_output);
}
function customSort(& $array, $key) {
    return usort($array, function ($a, $b) use ($key) {
        return strcmp(strtolower($a[$key]), strtolower($b[$key]));
    });
}
