<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 08.10.2018
 * Time: 18:31
 */

require_once 'guardian/class/userauth.class.php';
require_once 'yevgeny/core/Controller_core.php';

class Browse extends \Core\Controller_core
{
    public $isLoggedIn;
    private $model;
    private $seriesHandle;
    private $seriesModel;
    private $postsModel;
    private $optionsModel;
    private $userHandle;
    private $purchasedModel;
    private $categoryModel;

    public function __construct()
    {
        parent::__construct();
        $auth = new UserAuthentication();
        if (!$auth->isLoggedIn()){
            $this->isLoggedIn = false;
            $_SESSION['client_ID'] = -1;
        }
        else{
            $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
            $this->isLoggedIn = true;
        }
        $this->load->model('api_m/Category_m');
        $this->model = new \Models\api\Category_m();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();

        $this->seriesModel = $this->seriesHandle->model;

        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();

        $this->load->model('api_m/Options_m');
        $this->optionsModel = new \Models\api\Options_m();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();

        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();
    }
    public function index() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if ($id === 'bootcamp') {
            $category = [
                'category_ID' => 'Bootcamp',
                'strCategory_name' => 'All solutions for Bootcamp',
                'strCategory_description' => 'The content from these category will be provided to you on daily basis.',
            ];
            $series = $this->seriesHandle->getAvailableRows(['strSeries_level' => 'Bootcamp']);
        }
        else if ($id === 'on-demand') {
            $category = [
                'category_ID' => 'On Demand',
                'strCategory_name' => 'All solutions for On Demand',
                'strCategory_description' => 'The content from these category will be provided to you on daily basis.',
            ];
            $series = $this->seriesHandle->getAvailableRows(['strSeries_level' => 'On Demand']);
        }
        else if ($id) {
            $category = $this->model->get($id);
            $category['strCategory_description'] = strip_tags(htmlspecialchars_decode($category['strCategory_description']));
            $series = $this->seriesHandle->getAvailableRows(['intSeries_category' => $id]);
        }
        else {
            $category = false;
            $series = $this->seriesHandle->getAvailableRows([]);
        }
        foreach ($series as & $sery) {
            $sery = $this->format($sery);
        }
        $categories = $this->getCategoriesGroupByLevel();

        $stripeApi_pKeyRow = $this->optionsModel->get(['strOption_name' => 'stripeApi_pKey']);
        $stripeApi_pKey = $stripeApi_pKeyRow ? $stripeApi_pKeyRow['strOption_value'] : 'noKey';
        $this->load->view('Browse_v', ['series' => $series, 'inputCategory' => $category, 'categories' => $categories, 'stripeApi_pKey' => $stripeApi_pKey]);
    }
    private function format($row) {
        $row = \Helpers\utf8Encode($row);
        $row['posts_count'] = $this->postsModel->countPosts(['intPost_series_ID' => $row['series_ID']]);

        $owner = $this->userHandle->get($row['intSeries_client_ID']);
        $row['stripe_account_id'] = $owner['stripe_account_id'];

        $purchased = $this->purchasedModel->get(['intPurchased_series_ID' => $row['series_ID']]);
        $row['purchased'] = $purchased ? $purchased['purchased_ID'] : 0;

        $row['category'] = $this->categoryModel->get($row['intSeries_category']);
        return $row;
    }
    public function getCategoriesGroupByLevel() {

        $categories = $this->model->getRows();
        $sCategories = [];
        $eCategories = [];

        foreach ($categories as $category) {
            $sSeries_cnt = $this->seriesModel->count(['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 0]);
            if ($sSeries_cnt) {
                $sCategories[] = $category;
            }

            $eSeries_cnt = $this->seriesModel->count(['intSeries_category' => $category['category_ID'], 'boolSeries_level' => 1]);
            if ($eSeries_cnt) {
                $eCategories[] = $category;
            }
        }

        return [
            'simplify' => $sCategories,
            'enrich' => $eCategories
        ];
    }
}
$handle = new Browse();
$handle->index();
