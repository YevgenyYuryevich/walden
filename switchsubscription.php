<?php
/**
 * Client: Nathaniel Baca
 * Developer: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 23.03.2018
 * Time: 07:44
 */
require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';
require_once 'yevgeny/models/api_m/Posts_m.php';
require_once 'yevgeny/models/api_m/Subscriptions_m.php';

use Core\Controller_core;
use Models\api\{Posts_m, Subscriptions_m};

class SwitchSubscription extends Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->postsModel = new Posts_m();
        $this->subsModel = new Subscriptions_m();
    }
    public function index(){
        $postId = isset($_POST['postId']) ? $_POST['postId'] : (isset($_GET['postId']) ? $_GET['postId'] : 0);
        $subscriptionId = isset($_POST['subscriptionId']) ? $_POST['subscriptionId'] : (isset($_GET['subscriptionId']) ? $_GET['subscriptionId'] : 0);
        $owner = isset($_POST['owner']) ? $_POST['owner'] : (isset($_GET['owner']) ? $_GET['owner'] : 'other');
        if ($subscriptionId == 0){
            echo 'Please come from switch button. Thanks!';
            die();
        }
        $subscription = $this->subsModel->get($subscriptionId);
        $subscription = $this->subsModel->formatSubscription($subscription);
        $this->load->view('SwitchSubscription_v', ['postId' => $postId, 'subscriptionId' => $subscriptionId, 'owner' => $owner, 'subscription' => $subscription]);
    }
}

$handle = new switchsubscription();

$handle->index();