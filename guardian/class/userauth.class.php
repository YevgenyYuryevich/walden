<?php
require_once __DIR__ . './../options/options.php';
require_once __DIR__ . './../options/connect.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/config.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/dbconnect.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/helpers/funcs.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/models/Invitation_m.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/models/api_m/Options_m.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/models/api_m/User_m.php';
require_once realpath($_SERVER['DOCUMENT_ROOT']) . '/yevgeny/models/api_m/Affiliate_Tracks_m.php';

use Models\Invitation_m;
use function Helpers\htmlMailHeader;

class UserAuthentication {

    private $model;
    private $affiliateTracksModel;

    function __construct(){
        global $guardian, $guard, $myconnection;
        $this->db = $myconnection;
        $this->invitationModel = new Invitation_m();
        $this->optionsModel = new \Models\api\Options_m();
        $this->model = new \Models\api\User_m();
        $this->affiliateTracksModel = new \Models\api\Affiliate_Tracks_m();
        if ($guard->connect_error) throw new Exception("Database connection failed (" . $guard->connect_error . ")");
        $sessionId = false;
        $this->startSession($sessionId);
    }
    public function startSession($sessionId=false){
        global $guardian;
        $this->mkdir_chmod($guardian['session']);
        session_name('guardian');
        session_set_cookie_params(3600*24*7,"/");
        session_save_path($guardian['session']);
        if (!@session_start()) throw new Exception("Unable to start a session");
        $_SESSION['guardian']['selfie'] = session_id();
        if ($sessionId) $_SESSION['guardian']['selfie'] = $sessionId;
        else if (isset($_COOKIE[$this->hostName()])) $_SESSION['guardian']['selfie'] = $_COOKIE[$this->hostName()];
    }
    public function isLoggedIn(){

        global $guardian,$guard;
        if (isset($_SESSION['guardian']['username']) || isset($_COOKIE[$this->hostName()])){
            if (isset($_SESSION['guardian']['username'])){
                $usernameCheck = $_SESSION['guardian']['username'];
            } else if (isset($_COOKIE[$this->hostName()])){
                $user_q = $guard->query("SELECT * FROM guard_users WHERE session_id = '".$guard->escape_string($_COOKIE[$this->hostName()])."'");
                $user = mysqli_fetch_assoc($user_q);
                $usernameCheck = $user['username'];
            }

            $user_q = $guard->query("SELECT * FROM guard_users WHERE username = '".$guard->escape_string($usernameCheck)."'");
            if (mysqli_num_rows($user_q) > 0){
                $user = mysqli_fetch_assoc($user_q);
                if ($user['session_id'] == $_SESSION['guardian']['selfie']){
                    $this->setSession($user['username']);
                    $_SESSION['client_ID'] = $_SESSION['guardian']['id'];
                    $_SESSION['affiliate_id'] = $_SESSION['guardian']['affiliate_id'];
                    return true;
                }
            }
        }
        $_SESSION['client_ID'] = -1;
        $_SESSION['affiliate_id'] = -1;
        return false;
    }
    public function logIn($login,$password) {
        global $guardian,$guard;
        if (isset($_SESSION['guardian']['error'])) unset($_SESSION['guardian']['error']);
        $user_q = $guard->query("SELECT * FROM guard_users WHERE username='".$guard->escape_string($login)."' or email='".$guard->escape_string($login)."'");
        if (mysqli_num_rows($user_q) > 0) {
            $user = mysqli_fetch_assoc($user_q);
            $pass = $this->strongEncrypt($password);
            if ($pass == $user['hash']) {
                $_SESSION['guardian']['username'] = $user['username'];
                $guard->query("UPDATE guard_users SET session_id = '".session_id()."' WHERE id='".$user['id']."'");
                $this->setSession($user['username']);
                $this->writeLog($user['id'],$user['username'],$pass);
                return true;
            } else {
                $_SESSION['guardian']['error'] = "Password is wrong";
                $this->writeLog($user['id'],$user['username'],$pass);
            }
        } else {
            $_SESSION['guardian']['error'] = "Username or email does not exist";
            //$this->writeLog($user['id'],$login,$password);
        }
        return false;
    }
    function setSession($login) {
        global $guardian,$guard;
        $user_q = $guard->query("SELECT * FROM guard_users WHERE username = '".$guard->escape_string($login)."'");
        $user = mysqli_fetch_assoc($user_q);
        $_SESSION['guardian']['logged'] = true;
        $_SESSION['guardian']['username'] = $user['username'];
        $_SESSION['guardian']['email'] = $user['email'];
        $_SESSION['guardian']['f_name'] = $user['f_name'];
        $_SESSION['guardian']['l_name'] = $user['l_name'];
        $_SESSION['guardian']['id'] = $user['id'];
        $_SESSION['guardian']['affiliate_id'] = $user['affiliate_id'];
        $_SESSION['guardian']['level'] = $user['level'];
        $_SESSION['guardian']['trial'] = $user['trial'];
        $_SESSION['guardian']['active'] = $user['active'];
        $_SESSION['client_ID'] = $user['id'];
        $_SESSION['affiliate_id'] = $user['affiliate_id'];
        setcookie($this->hostName(), session_id(), time()+365*24*3600, "/");
    }
    function writeLog($id,$login,$password) {
        global $guardian,$guard;
        if (isset($_SESSION['guardian']['error'])) {
            $errorColumn = ",success,error";
            $errorValues = ",'0','".$_SESSION['guardian']['error']."'";
        } else {
            $errorColumn = "";
            $errorValues = "";
        }
        $guard->query("INSERT INTO guard_log (member,ip,browser,credentials".$errorColumn.") VALUES ('".$id."','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."','".$guard->escape_string($login)."|".$guard->escape_string($password)."'".$errorValues.")");
        //$guard->query("INSERT INTO guard_log (member,ip,browser,country,credentials".$errorColumn.") VALUES ('".$id."','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."','".geoip_country_code_by_name($_SERVER['REMOTE_ADDR'])."','".$guard->escape_string($login)."|".$guard->escape_string($password)."'".$errorValues.")");
    }
    public function log_out() {
        if($this->isLoggedIn()) {
            unset($_SESSION['guardian']);
            setcookie($this->hostName(), '', time()-3600, "/");
            return true;
        }
        return false;
    }
    public function check(){
        global $guard;
        $user_q = $guard->query("SELECT * FROM guard_users WHERE ".$_POST['check']."='".$guard->escape_string($_POST['value'])."'");
        if ($user_q->num_rows>0) return 0;
        return 1;
    }
    public function signin(){
        $info = [];
        $info['success'] = $this->logIn($_POST['username'],$_POST['password']);
        if (isset($_SESSION['guardian']['error'])) $info['error'] = $_SESSION['guardian']['error'];
        else {
            $info['data'] = $_SESSION['guardian'];
        }
        return $info;
    }
    public function register(){
        $info = [];
        global $guard;
        $columns = array(); $values = array();

        $allowToRegisterRow = $this->optionsModel->get(['strOption_name' => 'users_can_register']);
        $allowToRegister = $allowToRegisterRow ? (int)$allowToRegisterRow['strOption_value'] : 0;

        if ($allowToRegister) {
            unset($_POST['token']);
        }
        else {
            if (!isset($_POST['token'])){ return false; }
            $invitation = $this->getInvitationByToken($_POST['token']);

            if ($invitation['strInvitedEmail'] != $_POST['email']){
                return false;
            }
            unset($_POST['token']);
        }

        $_POST['username'] = isset($_POST['username']) ? $_POST['username'] : $_POST['email'];
        foreach($_POST as $key => $value){
            if ($key!='submit' && $key!='function'){
                if ($key=='password'){
                    $columns[] = 'hash';
                    $values[] = "'" . $this->strongEncrypt($value) . "'";
                } else {
                    $columns[] = $guard->escape_string($key);
                    $values[] = "'".$guard->escape_string($value)."'";
                }
            }
        }
        $maxId = $this->model->max('id');
        $columns[] = 'affiliate_id';
        $values[] = "'affiliate_" . ($maxId + 1) . "'";
        $q = "INSERT INTO guard_users (level,".implode(',',$columns).") VALUES ('1',".implode(',',$values).")";
        if ($guard->query($q)) if (isset($_POST['password'])) $this->logIn($_POST['username'], $_POST['password']);
        if (isset($_SESSION['introduced_affiliate'])) {
            $sets = ['affiliate_id' => $_SESSION['introduced_affiliate'], 'user_id' => $_SESSION['client_ID'], 'action' => 'register'];
            $this->affiliateTracksModel->insert($sets);
        }
        $info['data'] = $_SESSION['guardian'];
        $header = htmlMailHeader();
        $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/thanks_for_joining.html');
        mail($_POST['email'], 'Thanks for joining', $msg, $header);
        $info['registered'] = true;
        return $info;
    }
    public function reset_send(){
        global $guardian,$guard;
        $info = array();
        $user_q = $guard->query("SELECT * FROM guard_users WHERE username='".$guard->escape_string($_POST['username'])."' or email='".$guard->escape_string($_POST['username'])."'");
        if (mysqli_num_rows($user_q) > 0){
            $user = mysqli_fetch_assoc($user_q);
            $temper = substr($this->strongEncrypt($user['email'].time()),0,30);
            $guard->query("UPDATE guard_users SET temper='".$temper."' WHERE id='".$user['id']."'");
            $message = "Hello, ".$user['email']."\r\n\r\n
You requested password reset at ".$this->hostName().".\r\n
Please follow this link or ignore this message.\r\n
http://".$_SERVER['HTTP_HOST']."/login.php?reset_pw&temper=".$temper."\r\n\r\n
Yours, ".$this->hostName();
            mail($user['email'],$this->hostName().' password reset',$message);
            $info['success'] = true;
            $info['message'] = $_SESSION['guardian']['message'] = 'E-mail sent. Please check your mailbox';
        } else {
            $info['error'] = $_SESSION['guardian']['error'] = 'Not found in database';
        }
        return $info;
    }
    public function reset_pw(){
        $info = [];
        global $guardian,$guard;
        $user_q = $guard->query("SELECT * FROM guard_users WHERE temper='".$guard->escape_string($_POST['temper'])."'");
        if (mysqli_num_rows($user_q) > 0){
            $user = mysqli_fetch_assoc($user_q);
            $guard->query("UPDATE guard_users SET temper='0',hash='".$this->strongEncrypt($_POST['password'])."' WHERE id='".$user['id']."'");
            $this->logIn($user['username'],$_POST['password']);
            $info['success'] = true;
        } else {
            $info['error'] = $_SESSION['guardian']['error'] = 'Not found in database';
        }
        return $info;
    }
    public function register_ramdom(){
        global $guardian,$guard;
        $data = array();
        $ip = explode('.',$_SERVER['REMOTE_ADDR']);
        $data['username'] = 'Guest '.implode('',$ip);
        $data['session_id'] = $_SESSION['guardian']['selfie'];
        $q = "SELECT * FROM guard_users WHERE username='".$data['username']."'";
        $result = $guard->query($q);
        if ($result->num_rows>0) $info = $this->logIn($data['username'],'','',true);
        else $info = $this->user_register($data);
        return $info;
    }
    function edit(){
        global $guardian,$guard;
        foreach ($_POST['edit'] as $key => $val){
            if ($key=='password') $update[] = "hash='".$this->strongEncrypt($val)."'";
            else $update[] = $guard->escape_string($key)."='".$guard->escape_string($val)."'";
        }
        $q = "UPDATE guard_users SET ".implode(',',$update)." WHERE username='".$_SESSION['guardian']['username']."'";
        $guard->query($q);
    }
    function user_data(){
        global $guardian,$guard;
        $info = array();
        $user = $guard->query("SELECT * FROM guard_users WHERE username='".$guard->escape_string($_SESSION['guardian']['username'])."'");
        if (($user->num_rows) > 0){
            $user = $user->fetch_assoc();
            $info['username'] = $user['username'];
            $info['email'] = $user['email'];
            $info['f_name'] = $user['f_name'];
            $info['l_name'] = $user['l_name'];
            $info['phone'] = $user['phone'];
        }
        return $info;
    }
    function getInvitationByToken($token){
        $query = 'SELECT * FROM `tblClientInvitation` WHERE strToken = :token';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute( [':token' => $token] );
        return $rlt ? $stmt->fetch(PDO::FETCH_ASSOC) : false;
    }
    function view_tmpl($tpl='login'){
        $vars = array();

        if (isset($_GET['token'])){
            $invitationRow = $this->getInvitationByToken($_GET['token']);
            if ($invitationRow){
                $vars['INVITED_CLASS'] = 'invited-person';
                $vars['INVITED_EMAIL'] = $invitationRow['strInvitedEmail'];
                $vars['TOKEN'] = $_GET['token'];
            }
        }
        else {
            $vars['INVITED_CLASS'] = '';
            $vars['INVITED_EMAIL'] = '';
            $vars['TOKEN'] = '';
        }

        $allowToRegisterRow = $this->optionsModel->get(['strOption_name' => 'users_can_register']);
        $allowToRegister = $allowToRegisterRow ? (int)$allowToRegisterRow['strOption_value'] : 0;
        $vars['ALLOW_USER_REGISTER'] = $allowToRegister ? 'allow-any-register' : '';
        global $guardian;
        $html = file_get_contents($_SERVER['DOCUMENT_ROOT'].$guardian['folder'].'/tpl/'.$tpl.'.tpl');
        $vars['REF'] = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : (isset($_SESSION['uri']) ? $_SESSION['uri'] : '/userpage'));
        $vars['REF'] = $vars['REF'] == '/index' || $vars['REF'] == '/index.php' ? '/userpage' : $vars['REF'];
        if ($tpl=='profile') $vars = array_merge($vars,$this->user_data());
        $template = $this->prepare_tmpl($html,$vars);
        echo $template;
    }
    public function ajax_sendEmail(){
        $email = $_POST['email'];
        $id = $this->invitationModel->insert(['intClient_ID' => -1, 'strInvitedEmail' => $email, 'strToken' => -1, 'intInvitation_status' => 1]);
        $headers = htmlMailHeader();
        $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/Thanks_for_your_interest.html');
        mail($email, 'Thanks for your interest', $msg, $headers);
        $msg = '<h4>There is someone('. $email .') to have interest for theGreyShirt</h4><a href="'. BASE_URL .'/admin/invitations.php?id='. $id .'">Send Invitation</a>';
        mail(PRIMARY_EMAIL, 'Invitation Request From Walden.ly!', $msg, $headers);
        return ['data' => $id];
    }
    function prepare_tmpl($html,$vars){
        foreach ($vars as $var => $val){
            $html = str_replace('%'.$var.'%',$val,$html);
        }
        return $html;
    }
    function hostName() {
        $host = array_reverse(explode('.',$_SERVER['HTTP_HOST']));
        return $host[1];
    }
    function mkdir_chmod($folder){
        if (!file_exists($folder)){
            @mkdir($folder,0777,true);
            @chmod($folder,0777);
        }
    }
    public function controlTable() {
        global $guardian,$guard;
        $query = "DESCRIBE guard_users";
        return $guard->query($query);
    }
    public function strongEncrypt($password){
        $strong1 = hash_pbkdf2('haval192,4', $password, 'nbNB12_+' . substr($password, 0, 3), 50);
        $strong2 = hash_pbkdf2('gost-crypto', $strong1, 'yvgYVG1@<[~' . substr($password, 3), 100);
        $strong3 = substr($strong2, 1) . $strong2[0];
        return $strong3;
    }
}

?>
