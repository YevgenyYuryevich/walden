<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/guardian/js/main.js"></script>
<div class="edit_profile center-block align-center" data-user="edit">
    <h2>Edit your account</h2>
    <form method="post">
        <div class="field"><div class="gd-username"><i class="fa fa-user-circle-o" aria-hidden="true"></i> %username%</div></div>
        <div class="field"><span>E-mail</span><input type="email" name="email" value="%email%" data-check="true"></div>
        <div class="field"><span>First name</span><input type="text" name="f_name" value="%f_name%"></div>
        <div class="field"><span>Last name</span><input type="text" name="l_name" value="%l_name%"></div>
        <!--<div class="field"><span>Phone</span><input type="text" name="phone" value="%phone%"></div>-->
        <div class="field_separator"></div>
        <div class="field"><span>Password</span><input type="password" name="password"></div>
        <div class="field"><span>Repeat password</span><input type="password" name="repeat" data-skip="true"></div>
        <button class="buttoner">Edit Account</button>
    </form>
</div>