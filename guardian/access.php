<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

$access = true;
require_once $_SERVER['DOCUMENT_ROOT'].'/guardian/class/userauth.class.php';
$auth = new UserAuthentication();

if (!$auth->isLoggedIn()){
    if (substr($_SERVER['REQUEST_URI'], 1, 7) != 'yevgeny') {
        $_SESSION['uri'] = $_SERVER['REQUEST_URI'];
    }
	header('Location:/login');
}

$_SESSION['client_ID'] = $_SESSION['guardian']['id'];
$_SESSION['affiliate_id'] = $_SESSION['guardian']['affiliate_id'];
$myuser = $_SESSION['myuser'] = $_SESSION['guardian']['username'];
$logged = $_SESSION['guardian']['logged'];

?>