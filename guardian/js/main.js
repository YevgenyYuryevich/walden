// JavaScript Document
is_loading_controlled_in_local = false;
$(function(){

    var registerHandle;
    var RegisterClass = function () {

        var domRoot = $('.register');

        var domSendEmailForm = domRoot.find('form.send-email-form');

        var sendEmail = function (email) {
            var ajaxData = {
                function: 'send_email',
                email: email
            };
            $.ajax({
                data: ajaxData,
                dataType: 'json',
                success: function(data){
                    swal("SUCCESS!", 'Thanks!. We will send invitation soon', "success", {
                        button: "OK"
                    });
                }
            });
        }

        var bindEvents = function () {
            domSendEmailForm.submit(function () {
                if (domSendEmailForm.find('[name="email"]').hasClass('error')){
                    return false;
                }
                sendEmail(domSendEmailForm.find('[name="email"]').val());
                return false;
            });
        }

        this.init = function () {
            if (domRoot.hasClass('invited-person') || domRoot.hasClass('allow-any-register')){
                domRoot.find('.send-email-form').remove();
                validField(domRoot.find('[name="email"]'));
            }
            else {
                domRoot.find('.register-form').remove();
            }
            bindEvents();
        }
    }

	$.ajaxSetup({
		url:'/login.php',
		type:'POST',
		dataType: 'json',
		beforeSend: function(){
			// if ($('.hover.load').length==0) $('body').append('<div class="hover load"><div class="sk-wave"><div class="sk-rect sk-rect1"></div><div class="sk-rect sk-rect2"></div><div class="sk-rect sk-rect3"></div><div class="sk-rect sk-rect4"></div><div class="sk-rect sk-rect5"></div></div></div>');
			// $('.load').fadeIn(100);
		},
		complete: function(){
			// $('.load').fadeOut(100);
		},
		success: function(){
			// $('.load').fadeOut(100);
		},
		cache: false,
		error: function(request, status, error){
			//alert(error);
		}
	});
	
	// edit account
	$(document).on('click', '.edit_profile form button', function(e){
		e.preventDefault();
		var $this = $(this);
		var process = $this.closest('[data-user]').attr('data-user');
		var data = {};
		data['function'] = process;
		var fields = $this.closest('form').find('input');
		data['edit'] = {};
		fields.each(function(){
            if (!$(this).hasAttr('data-skip') && $(this).val()!='' && $(this).val()!=$(this).attr('data-old')){
				data['edit'][$(this).attr('name')] = $(this).val();
			}			
        });
		console.log(data)
		$.ajax({
			data:data,
			dataType: 'json',
			success: function(data){
				if ($('.auth_notice').length==0) $this.before('<div class="auth_notice"></div>');
				$('.auth_notice').html('Changes saved');
			}
		});
		
	});
	
	// users
	$(document).on('click', '.auth form button', function(e){
		if (!$('body').hasClass('login-page')) return false;
		e.preventDefault();
		var proceedAjax = false;
		var $this = $(this);
		var process = $this.closest('[data-user]').attr('data-user');
		var page = ($('[data-ref]').length>0)?$('[data-ref]').attr('data-ref'):'http://'+document.location.host;
		var data = {};
		data['function'] = process;
		$('[data-user="'+process+'"] input').each(function(){
		    console.log($(this).attr('name'));
			if ($(this).attr("name")!='repeat') data[$(this).attr("name")] = $(this).val();
		});
		if (process=='register'){
			var form = $this.closest('form');
			fields = form.find("input");
			valid_fields = form.find("input.valid");
			$this.attr('disabled','disabled').addClass('disabled');	
			if (fields.length==valid_fields.length){
				proceedAjax = true;
				$this.removeAttr('disabled').removeClass('disabled');
			}
		} else if (process=='reset_pw'){
			proceedAjax = true;
			data['temper'] = $.urlParam('temper');
		} else if (process=='signin' || process=='reset_send') proceedAjax = true;
		if (proceedAjax){
			$.ajax({
				data:data,
				dataType: 'json',
				success: function(data){
					if (process=='signin'){
						if (data.result.success===true) window.location.href = page;
						else {
							if ($('[data-user="'+process+'"] .error_msg').length==0) $this.before('<div class="error_msg"><div>');
							$('[data-user="'+process+'"] .error_msg').html(data.result.error);
						}
					} else if (process=='reset_send'){
						$this.attr('disabled','disabled').addClass('disabled');
						if ($('[data-user="'+process+'"] .error_msg').length==0) $this.before('<div class="error_msg"><div>');
						if (data.result.success===true){
							$('[data-user="'+process+'"] .error_msg').html(data.result.message);
						} else {
							$('[data-user="'+process+'"] .error_msg').html(data.result.error);
						}
					} else if (process=='reset_pw'){
						if (data.result.success===true) window.location.href = page;					
						else {
							if ($('[data-user="'+process+'"] .error_msg').length==0) $this.before('<div class="error_msg"><div>');
							$('[data-user="'+process+'"] .error_msg').html(data.result.error);
						}
					} else {
                        window.location.href = 'simplify?first_visit=1';
                    }
                }
			});
		}
	});
	$('.auth .field input').each(function(){
		if (!$(this).hasAttr('required')) $(this).addClass('valid');
		$(this).attr('placeholder',$(this).closest('div').find('span').html());
		if ($('body').hasClass('profile')) $(this).attr('data-old',$(this).val());
	});
	$(document).on('focusout','[data-user="register"] .field input, [data-user="reset_pw"] .field input, [data-user="edit"] .field input',function(){
		validField($(this));
    });
	
	if ($('body').hasClass('login-page')){
		var params = {};
		params['signin'] = 'Please login';
		params['register'] = 'Register your account';
		params['reset_send'] = 'Request password reset';
		params['reset_pw'] = 'Reset your password';
		var param = param_find();
		if (param === undefined) var param = 'signin';
		$('title').html(params[param]);
		
		var separ = ['reset_pw'];
		$.each(separ,function(k,value){
			if (param!=value){
				$('[data-user="'+value+'"]').remove();
			} else {
				$.each(params,function(key){
					if (key!=value) $('[data-user="'+key+'"]').remove();
				});
			}
		});
	
		var h2 = $('[data-user="'+param+'"]').find('h2');
		show_form(h2);
		$(document).on('click','.auth h2',function(){
			show_form($(this));
		});
		function show_form($this){
			$('.auth form').hide(0);
			$('.auth h2').each(function(){
				var fa = $(this).find('.fa');
				if (fa.length==0) $(this).append(' <i class="fa fa-angle-double-down" aria-hidden="true"></i>');
			});
			$this.closest('div').find('form').removeClass('hidden').show(0);
			$this.closest('div').find('.fa').remove();
		}
		function param_find(){
			var param;
			$.each(params,function(key){
				if ($.hasUrlParam(key)){
					param = key;
					return false;
				}
			});
			return param;
		}
	}

    registerHandle = new RegisterClass();
    registerHandle.init();
});

function validField(field){
	var name = field.attr("name");
	if (field.val()==field.attr('data-old') && name!='password' && name!='repeat'){
		removeError(field);
		return true;
	}
	var param = field.closest('[data-user]').attr('data-user');
	if (name=="username" || name=="password"){
		var regex = name=='username'?/^([a-zA-Z0-9.]{4,16})$/:/^.{6,16}$/;
		var error_msg = name=='username'?'4-16 characters. Letters and numbers only':'6-16 characters';
		if (!field.val().match(regex) || field.val().length==0){
			postError(field,error_msg);
		} else {
			if (name=="username"){
				checkVal(field,name,'This username is taken. Please choose another');
			} else {
				removeError(field);
				validField($('[name="repeat"]'));
			}
		}
	} else if (name=="email"){
		if(!validEmail(field.val()) || field.val().length==0){
			postError(field,'Invalid E-mail address');
		} else {
			if (param!='myprofile') checkVal(field,name,'This E-mail is taken. Please choose another');
			else if (param=='myprofile' && field.attr('data-original')!=field.val()) checkVal(field,name,'This E-mail is taken. Please choose another');
			else if (param=='myprofile' && field.attr('data-original')==field.val()) removeError(field);
		}
	} else if (name=="repeat"){
		if (field.val()==field.closest('form').find('[name="password"]').val()) removeError(field);
		else postError(field,'Passwords do not match');
	}
}
function validEmail(email){
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if( !emailReg.test(email)){
		return false;
	} else {
		return true;
	}
}
function checkVal(field,name,error_msg){
	var form = field.closest('form');
	postError(field,'Checking availability');
	form.find('button').attr('disabled','disabled');
	var data = {};
	data['check'] = name;
	data['value'] = field.val();
	response = $.ajax({
		data:data,
		dataType: 'json',
		success: function(data){
			if (data.result==1) removeError(field);
			else postError(field,error_msg);
		}
	});
}
function postError(field,error_msg){
	var form = field.closest('form');
	var error_div = field.closest('div').find('.error_msg');
	field.addClass('error').removeClass('valid');
	form.find('button').attr('disabled','disabled');
	if (error_div.length<1) field.after('<div class="error_msg">'+error_msg+'</div>');
	else error_div.html(error_msg);
}
function removeError(field){
	var form = field.closest('form');
	var error_div = field.closest('div').find('.error_msg');
	field.removeClass("error").addClass('valid');
	fields = form.find("input");
	valid_fields = form.find("input.valid");
	if (valid_fields.length==fields.length) form.find('button').removeAttr('disabled');	
	error_div.remove();
	return true;
}

$.fn.hasAttr = function(name){
   return this.attr(name) !== undefined;
};
$.hasUrlParam = function(name){
    if (window.location.href.indexOf(name) > -1) return true;
	else return false;
};
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    } else {
       return decodeURI(results[1]) || 0;
    }
}

$(document).ajaxStart(function () {
    if(!is_loading_controlled_in_local) {
        $('.loading').css('display', 'block');
    }
});
$(document).ajaxComplete(function () {
    if(!is_loading_controlled_in_local){
        $('.loading').css('display', 'none');
    }
});