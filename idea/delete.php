<?php
require_once "../guardian/access.php";
require_once __DIR__."/"."db.php";
header("Content-Type: application/json");
$status = "error";
$query = "";
$connection = mysqli_connect($db_host, $db_user, $db_pass);
if ($connection) {
	mysqli_select_db($connection, $db_name);
	mysqli_set_charset($connection, "utf8");
	if (isset($_POST["id"])) {
		$query = "delete from tblideabox where ideabox_ID=".intval($_POST["id"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
		mysqli_query($connection, $query);
		$result = mysqli_affected_rows($connection);
		if ($result === 1) {
			$status = "success";
		} else {
			$status = "error";
		}
	}
	mysqli_close($connection);
	$connection = null;
} else {
	error_log("Error : mysqli_connect");
}
echo json_encode(Array("status" => $status));
