<?php
require_once "../guardian/access.php";

$BASE_URL = sprintf(
    "%s://%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME']
);

$request_url = $_SERVER['REQUEST_URI'];
$request_dir_url = $BASE_URL . preg_replace('/\/(grid.php)$/', '', $request_url);

?>
<!DOCTYPE html>
<?php
/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/
require_once __DIR__."/"."db.php";
function displaySelect($argConnection)
{
	$postId=isset($_POST["id"])?intval($_POST["id"]):0;
	$query = "select * from tblideabox where intIdeaBox_subcategory=0 and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
	$result = mysqli_query($argConnection, $query);
	if ($result) {
		?>
		<select name="id" id="ideaSelect">
			<?php
			while ($record = mysqli_fetch_array($result)) {
				?>
				<option value="<?php echo intval($record["ideabox_ID"]); ?>" <?php if ($postId==intval($record["ideabox_ID"])) {echo "selected";} ?>>
					<?php
					echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8");
					?>
				</option>
				<?php
			}
			?>
		</select>
		<?php
		mysqli_free_result($result);
		$result = null;
	} else {
		error_log("Error : mysqli_query");
	}
	?>
	<input type="submit" value="Show"><br>
	<?php
}
function displaySubDeadline($argConnexion, $argId, $argAll)
{
	$query = "select * from tblideabox where intIdeaBox_subcategory=".intval($argId)." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
	$result = mysqli_query($argConnexion, $query);
	if ($result) {
		while ($record = mysqli_fetch_array($result)) {
			$deadline = "";
			if ($record["dtIdeaBox_deadline"] != null) {
				if ($argAll || $record["boolIdeaBox_done"] == 0) {
					$deadlineDate = explode(" ", $record["dtIdeaBox_deadline"]);
					$deadlineTab = explode("-", $deadlineDate[0]);
					$deadline = "".$deadlineTab[1]."/".$deadlineTab[2]."/".$deadlineTab[0]."";
					?>
					<tr>
						<td><input type="checkbox" class="check" data-idea-id="<?php echo $record["ideabox_ID"]; ?>" <?php echo $record["boolIdeaBox_done"]?"checked":""; ?>></td>
						<td><?php echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8"); ?></td>
						<td><?php echo $deadline; ?></td>
					</tr>
					<?php
				}
			}
			displaySubDeadline($argConnexion, $record["ideabox_ID"], $argAll);
		}
		mysqli_free_result($result);
		$result = null;
	} else {
		error_log("Error : mysqli_query");
	}
}
function displayIdeaDeadline($argConnexion, $argId, $argAll)
{
	$query = "select * from tblideabox where ideabox_ID=".intval($argId)." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
	$result = mysqli_query($argConnexion, $query);
	if ($result) {
		while ($record = mysqli_fetch_array($result)) {
			$deadline = "";
			if ($record["dtIdeaBox_deadline"] != null) {
				if ($argAll || $record["boolIdeaBox_done"] == 0) {
					$deadlineDate = explode(" ", $record["dtIdeaBox_deadline"]);
					$deadlineTab = explode("-", $deadlineDate[0]);
					$deadline = "".$deadlineTab[1]."/".$deadlineTab[2]."/".$deadlineTab[0]."";
					?>
					<tr>
						<td><input type="checkbox" class="check" data-idea-id="<?php echo $record["ideabox_ID"]; ?>" <?php echo $record["boolIdeaBox_done"]?"checked":""; ?>></td>
						<td><?php echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8"); ?></td>
						<td><?php echo $deadline; ?></td>
					</tr>
					<?php
				}
			}
			displaySubDeadline($argConnexion, $record["ideabox_ID"], $argAll);
		}
		mysqli_free_result($result);
		$result = null;
	} else {
		error_log("Error : mysqli_query");
	}
}
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Home</title>
		<!--Css contection!-->
		<link href="https://fonts.googleapis.com/css?family=Reem+Kufi|EB+Garamond" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" >
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $request_dir_url;?>/css/main_blog.css">
		<link rel="stylesheet" href="<?php echo $request_dir_url;?>/css/adaptive_blog.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
		<style>
			a button {
				color: black;
			}
		</style>
	</head>
	<body>
		<div class="content_container">
			<div class="header">
				<div class="line_container"> <img src="<?php echo $request_dir_url;?>/img/line.png" alt="line"> </div>
				<div class="text_container">
					<div class="title"> DESIGN DOCUMENT </div>
					<div class="subtitle"> PROJECT IDEAS </div>
				</div>
				<div class="line_container" style="margin-top:20px;"> <img src="<?php echo $request_dir_url;?>/img/line.png" alt="line"> </div>
				<div id="header_1" class="animated subtext hided">
					<a href="index.php"><button type="button">Write</button></a>
					<a href="reorder.php"><button type="button">Reorder</button></a>
					<form id="ideaForm" action="" method="post" accept-charset="utf-8">
						<?php
						$connection = mysqli_connect($db_host, $db_user, $db_pass);
						if ($connection) {
							mysqli_select_db($connection, $db_name);
							mysqli_set_charset($connection, "utf8");
							displaySelect($connection);
							if (isset($_POST["id"])) {
								?>
								<button name="filter" value="all">Show all</button>
								<button name="filter" value="notDone">Show not done</button>
								<table class="table" id="gridtable">
									<thead>
										<tr>
											<td>Done</td>
											<td>Title</td>
											<td>Deadline</td>
										</tr>
									</thead>
									<tbody>
										<?php
										displayIdeaDeadline($connection, $_POST["id"], !isset($_POST["filter"]) || $_POST["filter"] == "all");
										?>
									</tbody>
								</table>
								<?php
							}
							mysqli_close($connection);
							$connection = null;
						} else {
							error_log("Error : mysqli_connect");
						}
						?>
					</form>
				</div>
			</div>
		</div>
		<!--Script contection!-->
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
		<script src="<?php echo $request_dir_url;?>/js/animation.js"></script>
		<script src="<?php echo $request_dir_url;?>/js/readingTime.min.js"></script>
		<script src="<?php echo $request_dir_url;?>/js/tinymce/jquery.tinymce.min.js"></script>
		<script src="<?php echo $request_dir_url;?>/js/tinymce/tinymce.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
		<script>
			var $j = jQuery.noConflict(true);
			$j(function() {
				$j(".datepicker").datepicker();
				$j('#gridtable').DataTable();
			});
			function GetXhr() {
				var xhr = null;
				if (window.XMLHttpRequest) {
					xhr = new XMLHttpRequest();
				} else if (window.ActiveXObject) {
					try {
						xhr = new ActiveXObject('Msxml2.XMLHTTP');
					}
					catch (e) {
						xhr = new ActiveXObject('Microsoft.XMLHTTP');
					}
				}
				return xhr;
			}
			function checkIdea() {
				me = this;
				this.style.color = 'orange';
				var xhr=GetXhr();
				if (xhr) {
					var formData = new FormData();
					formData.append('id', this.getAttribute('data-idea-id'));
					xhr.onreadystatechange =
						function () {
							if ((this.readyState == 4) && (this.status == 200)) {
								result = JSON.parse(this.responseText);
								if (result.status == 'success') {
									me.style.color = null;
								} else {
									me.style.color = 'red';
								}
							}
						};
					formData.append('check', this.checked);
					page = 'done.php';
					xhr.open('POST', page, true);
					xhr.send(formData);
					delete xhr;
					xhr = null;
				}
			}
			elements = document.querySelectorAll('.check');
			for (i = 0 ; i < elements.length ; i++) {
				elements[i].addEventListener('click', checkIdea);
			}
		</script>
	</body>
</html>
