<?php
require_once "../guardian/access.php";
require_once __DIR__."/"."db.php";
header("Content-Type: application/json");
$connection = mysqli_connect($db_host, $db_user, $db_pass);
if ($connection) {
	mysqli_select_db($connection, $db_name);
	mysqli_set_charset($connection, "utf8");
	if (isset($_POST["source"]) && isset($_POST["dest"])) {
		$query = "select intIdeaBox_order from tblideabox where intIdeaBox_subcategory=".intval($_POST["dest"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"])." order by intIdeaBox_order desc;";
		$result = mysqli_query($connection, $query);
		$row = mysqli_fetch_array($result);
		$order = intval($row["intIdeaBox_order"])+1;
		mysqli_free_result($result);
		$result = null;
		$query = "update tblideabox set intIdeaBox_subcategory=".intval($_POST["dest"]).", intIdeaBox_order=".$order." where ideabox_ID=".intval($_POST["source"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
		mysqli_query($connection, $query);
		$result = mysqli_affected_rows($connection);
		if ($result !== false) {
			$status = "success";
		} else {
			$status = "error";
		}
	}
	mysqli_close($connection);
	$connection = null;
} else {
	error_log("Error : mysqli_connect");
}
echo json_encode(Array("status" => $status));
