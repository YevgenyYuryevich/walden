<?php

require_once "../guardian/access.php";

global $current_user;

$current_user = $_SESSION['client_ID'];

if (!isset($_POST['action'])){
    echo json_encode('no');
    die();
}

class databaseClass{

    public function __construct()
    {
        include ('db.php');
        $this->db = new PDO ('mysql:host='. $db_host .';dbname='. $db_name, $db_user, $db_pass);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        global $current_user;
        $this->current_user = $current_user;
    }
    public function setDone($id, $done){
        $query = 'UPDATE `tblideabox` SET boolIdeaBox_done = :done WHERE ideabox_ID = :id';
        $stmt = $this->db->prepare( $query );
        $rlt = $stmt->execute([
            ':id' => $id,
            ':done' => $done
        ]);
        return $rlt;
    }
}
class ideaClass{
    public function __construct()
    {
        $this->db = new databaseClass();
    }
    public function setDone(){
        $id = $_POST['id'];
        $done = $_POST['done'];
        $rlt = $this->db->setDone($id, $done);
        if ($rlt){
            echo json_encode(['status' => true]);
        }
        else{
            echo json_encode(['status' => false]);
        }
        die();
    }
}

$ideaHandle = new ideaClass();

switch ($_POST['action']){
    case 'set_done':
        $ideaHandle->setDone();
        break;
    default:
        break;
}