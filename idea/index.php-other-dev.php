<!DOCTYPE html>
<?php
/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/

$BASE_URL = sprintf(
    "%s://%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME']
);

$request_url = $_SERVER['REQUEST_URI'];
$request_dir_url = $BASE_URL . preg_replace('/\/(index.php)$/', '', $request_url);

require_once __DIR__."/"."db.php";
function displayIdeaQuery($argConnection, $argQuery, $argDepth = 0, $argParent)
{
	$result = mysqli_query($argConnection, $argQuery);
	if ($result) {
		?>
		<ul>
			<?php
			while ($record = mysqli_fetch_array($result)) {
				$deadline = "";
				if ($record["dtIdeaBox_deadline"] != null) {
					$deadlineDate = explode(" ", $record["dtIdeaBox_deadline"]);
					$deadlineTab = explode("-", $deadlineDate[0]);
					$deadline = "".$deadlineTab[1]."/".$deadlineTab[2]."/".$deadlineTab[0]."";
				}
				$hTag=intval($argDepth+2);
				if ($hTag > 6) {
					$hTag = 6;
				}
				?>
				<li>
					<article>
						<h<?php echo $hTag; ?> data-idea-type="title" data-idea-id="<?php echo intval($record["ideabox_ID"]); ?>" class="editable"><?php echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8"); ?></h<?php echo $hTag; ?>>
                        <?php
                        if ($_POST["visual"]=="Edit" || !empty($deadline)) {
                            ?>
						    Deadline : <input data-idea-type="deadline" data-idea-id="<?php echo intval($record["ideabox_ID"]); ?>" class="datepicker" value="<?php echo $deadline; ?>" placeholder="no deadline" style="border:0">
                            <?php
                        }
                        ?>
						<div data-idea-type="text" data-idea-id="<?php echo intval($record["ideabox_ID"]); ?>" class="editable"><?php echo $record["strIdeaBox_idea"]; ?></div>
					</article>
					<?php
					$query = "select * from tblideabox where intIdeaBox_subcategory=".intval($record["ideabox_ID"])." order by intIdeaBox_order;";
					displayIdeaQuery($argConnection, $query, $argDepth+1, $record["ideabox_ID"]);
					?>
				</li>
				<?php
			}
            if ($_POST["visual"]=="Edit") {
			?>
			<li class="newIdea" data-idea-parent="<?php echo intval($argParent); ?>" data-idea-depth="<?php echo intval($argDepth); ?>">New idea</li>
                <?php
            }
            ?>
		</ul>
		<?php
		mysqli_free_result($result);
		$result = null;
	} else {
		error_log("Error : mysqli_query");
	}
}
function displayOneIdea($argConnection, $argId)
{
	$query = "select * from tblideabox where ideabox_ID=".intval($argId)." order by intIdeaBox_order;";
	displayIdeaQuery($argConnection, $query, 0, 0);
}
function displaySelect($argConnection)
{

	$postId=isset($_POST["id"])?intval($_POST["id"]):0;

	$query = "select * from tblideabox where intIdeaBox_subcategory=0;";
	$result = mysqli_query($argConnection, $query);
	if ($result) {
		?>
		<select name="id" id="ideaSelect">
			<?php
			while ($record = mysqli_fetch_array($result)) {
				?>
				<option value="<?php echo intval($record["ideabox_ID"]); ?>" <?php if ($postId==intval($record["ideabox_ID"])) {echo "selected";} ?>>
					<?php
					echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8");
					?>
				</option>
				<?php
			}
			?>
		</select>
		<?php
		mysqli_free_result($result);
		$result = null;
	} else {
		error_log("Error : mysqli_query");
	}
	?>
    <input name="visual" type="submit" value="Edit">
    <input name="visual" type="submit" value="View"><br>
	<?php
}
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Home</title>
		<!--Css contection!-->
		<link href="https://fonts.googleapis.com/css?family=Reem+Kufi|EB+Garamond" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" >
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $request_dir_url?>/css/main_blog.css">
		<link rel="stylesheet" href="<?php echo $request_dir_url?>/css/adaptive_blog.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<style>
			.newIdea {
				color: #cccccc;
			}
			a button {
				color: black;
			}
		</style>
	</head>
	<body>
		<div class="content_container">
			<div class="header">
				<div class="line_container"> <img src="<?php echo $request_dir_url?>/img/line.png" alt="line"> </div>
				<div class="text_container">
					<div class="title"> DESIGN DOCUMENT </div>
					<div class="subtitle"> PROJECT IDEAS </div>
				</div>
				<div class="line_container" style="margin-top:20px;"> <img src="<?php echo $request_dir_url?>/img/line.png" alt="line"> </div>
				<div id="header_1" class="animated subtext hided">
					<a href="reorder.php"><button type="button">Reorder</button></a>
					<a href="grid.php"><button type="button">Deadlines</button></a>
					<form id="ideaForm" action="<?php echo $request_dir_url.'/index.php'?>" method="post" accept-charset="utf-8">
						<?php
						$connection = mysqli_connect($db_host, $db_user, $db_pass);
						if ($connection) {
							mysqli_select_db($connection, $db_name);
							mysqli_set_charset($connection, "utf8");
							displaySelect($connection);
							if (isset($_POST["id"])) {
								displayOneIdea($connection, $_POST["id"]);
							}
							mysqli_close($connection);
							$connection = null;
						} else {
							error_log("Error : mysqli_connect");
						}
						?>
					</form>
				</div>
			</div>
		</div>
		<!--Script contection!-->
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
		<script src="<?php echo $request_dir_url?>/js/animation.js"></script>
		<script src="<?php echo $request_dir_url?>/js/readingTime.min.js"></script>
		<script src="<?php echo $request_dir_url?>/js/tinymce/jquery.tinymce.min.js"></script>
		<script src="<?php echo $request_dir_url?>/js/tinymce/tinymce.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">

            tinymce.init({
                selector: '.editable',
                inline: true,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            });
        </script>
        <script type="text/javascript">

			function updateIdea2(argThis) {
				me = argThis;

				argThis.style.color = 'orange';
				var xhr=GetXhr();
				if (xhr) {
					var formData = new FormData();
					formData.append('id', argThis.getAttribute('data-idea-id'));
					xhr.onreadystatechange =
						function () {
							if ((this.readyState == 4) && (this.status == 200)) {
								result = JSON.parse(this.responseText);
								if (result.status == 'success') {
									me.style.color = null;
                                    $j(function () {
                                        var p = window.parent.$(window.parent.document).find('.tt-grid-wrapper');
                                        if (me.getAttribute('data-idea-type') === 'deadline'){
                                            p.trigger('update_idea', [{id: me.getAttribute('data-idea-id'), content: me.value, what: me.getAttribute('data-idea-type')}]);
                                        }
                                        else {

                                            p.trigger('update_idea', [{id: me.getAttribute('data-idea-id'), content: me.innerHTML, what: me.getAttribute('data-idea-type')}]);
                                        }
                                    });
								} else {
									me.style.color = 'red';
								}
							}
						};
					switch (argThis.getAttribute('data-idea-type')) {
						case 'title':
							formData.append('value', argThis.innerHTML);
							page = '<?php echo $request_dir_url;?>/title.php';
							break;
						case 'text':
							formData.append('value', argThis.innerHTML);
							page = '<?php echo $request_dir_url;?>/text.php';
							break;
						case 'deadline':
							formData.append('value', argThis.value);
							page = '<?php echo $request_dir_url;?>/deadline.php';
							break;
						default:
							page = 'error.php';
							break;
					}
					xhr.open('POST', page, true);
					xhr.send(formData);
					delete xhr;
					xhr = null;
				}
			}
			var $j = jQuery.noConflict(true);

			$j(function() {
				$j('.datepicker').datepicker({
					onSelect: function() {
						updateIdea2(this);
					}
				});
			});
			function GetXhr() {
				var xhr = null;
				if (window.XMLHttpRequest) {
					xhr = new XMLHttpRequest();
				} else if (window.ActiveXObject) {
					try {
						xhr = new ActiveXObject('Msxml2.XMLHTTP');
					}
					catch (e) {
						xhr = new ActiveXObject('Microsoft.XMLHTTP');
					}
				}
				return xhr;
			}
			function updateIdea() {

				updateIdea2(this);
			}
			elements = document.querySelectorAll('.editable');
			for (i = 0 ; i < elements.length ; i++) {
				elements[i].addEventListener('blur', updateIdea);
			}
			var gloIdea = 0;
			function insertIdea() {
				me = this.parentElement;
				me.style.color = 'orange';
				var xhr=GetXhr();
				if (xhr) {
					var formData = new FormData();
					formData.append('parent', me.getAttribute('data-idea-parent'));
					formData.append('title', me.children[0].innerHTML);
					formData.append('text', me.children[2].children[0].innerHTML);
					xhr.onreadystatechange =
						function () {
							if ((this.readyState == 4) && (this.status == 200)) {
								result = JSON.parse(this.responseText);
								if (result.status == 'success') {
									me.style.color = null;
									me.children[0].setAttribute('data-idea-type', 'title');
									me.children[0].setAttribute('data-idea-id', result.id);
									me.children[0].addEventListener('blur', updateIdea);
									me.children[2].children[0].setAttribute('data-idea-type', 'text');
									me.children[2].children[0].setAttribute('data-idea-id', result.id);
									me.children[2].children[0].addEventListener('blur', updateIdea);
									me.removeChild(me.children[3]);
									me.removeChild(me.children[3]);
									// new child idea
									element = document.createElement('ul');
										element2 = document.createElement('li');
										element2.setAttribute('class', 'newIdea');
										element2.setAttribute('data-idea-parent', result.id);
										element2.setAttribute('data-idea-depth', parseInt(me.getAttribute('data-idea-depth'))+1);
										element2.innerHTML='New idea';
										element2.addEventListener('click', newIdea);
										element.appendChild(element2);
									me.appendChild(element);
									// new sibling idea
									element = document.createElement('li');
									element.setAttribute('class', 'newIdea');
									element.setAttribute('data-idea-parent', me.getAttribute('data-idea-parent'));
									element.setAttribute('data-idea-depth', me.getAttribute('data-idea-depth'));
									element.innerHTML='New idea';
									element.addEventListener('click', newIdea);
									me.parentElement.appendChild(element);
									// reload if idea with no parent
									if (me.getAttribute('data-idea-parent') == '0') {var x = document.getElementById("mySelect");
										option = document.createElement('option');
										option.text = '';
										option.value = result.id;
										select = document.getElementById('ideaSelect');
										select.add(option);
										select.value = result.id;
										document.getElementById('ideaForm').submit();
									}
								} else {
									me.style.color = 'red';
								}
							}
						};
					xhr.open('POST', 'insert.php', true);
					xhr.send(formData);
					delete xhr;
					xhr = null;
				}
			}
			function cancelIdea(event) {
				element=this.parentElement;
				element.style.color = null;
				// empty list item
				element.classList.add('newIdea');
				while (element.firstChild) {
					element.removeChild(element.firstChild);
				}
				element.innerHTML='New idea';
				element.addEventListener('click', newIdea);
				event.stopPropagation();
			}
			function newIdea() {
				// empty list item
				this.removeEventListener('click', newIdea);
				this.classList.remove('newIdea');
				while (this.firstChild) {
					this.removeChild(this.firstChild);
				}
				// add htag
				depth = parseInt(this.getAttribute('data-idea-depth'));
				if (parseInt(depth)+2 > 6) {
					depth = 6;
				} else {
					depth = parseInt(depth)+2;
				}
				element = document.createElement('h'+depth);
				element.setAttribute('id', 'newTitle'+gloIdea);
				element.setAttribute('class', 'editable'+gloIdea);
				element.innerHTML='Title';
				this.appendChild(element);
				// add article
				element = document.createElement('article');
					// add div
					element2 = document.createElement('div');
					element2.setAttribute('class', 'editable'+gloIdea);
					element2.innerHTML='Text';
					element.appendChild(element2);
				this.appendChild(element);
				// add ok button
				element = document.createElement('button');
				element.setAttribute('type', 'button');
				element.innerHTML='Validate';
				element.addEventListener('click', insertIdea);
				this.appendChild(element);
				// add cancel button
				element = document.createElement('button');
				element.setAttribute('type', 'button');
				element.innerHTML='Cancel';
				element.addEventListener('click', cancelIdea);
				this.appendChild(element);
				// activate tinymce
				tinymce.init({
					selector: '.editable'+gloIdea,
					inline: true,
					menubar: false,
					plugins: [
						'advlist autolink lists link image charmap print preview anchor',
						'searchreplace visualblocks code fullscreen',
						'insertdatetime media table contextmenu paste code'
					],
					toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
				});
				// give focus
				document.getElementById('newTitle'+gloIdea).focus();
				gloIdea++;
			}
			elements = document.getElementsByClassName('newIdea');
			for (i = 0 ; i < elements.length ; i++) {

				elements[i].addEventListener('click', newIdea);
			}
		</script>
	</body>
</html>