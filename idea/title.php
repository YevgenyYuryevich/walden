<?php
require_once "../guardian/access.php";
require_once __DIR__."/"."db.php";
header("Content-Type: application/json");
$connection = mysqli_connect($db_host, $db_user, $db_pass);
if ($connection) {
	mysqli_select_db($connection, $db_name);
	mysqli_set_charset($connection, "utf8");
	if (isset($_POST["id"]) && isset($_POST["value"])) {
		$query = "update tblideabox set strIdeaBox_title='".mysqli_real_escape_string($connection, $_POST["value"])."' where ideabox_ID=".intval($_POST["id"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
		$result = mysqli_query($connection, $query);
		if ($result !== false) {
			$status = "success";
		} else {
			$status = "error";
		}
	}
	mysqli_close($connection);
	$connection = null;
} else {
	error_log("Error : mysqli_connect");
}
echo json_encode(Array("status" => $status));
