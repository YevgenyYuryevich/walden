<?php
require_once "../guardian/access.php";
?>
<!DOCTYPE html>
<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
require_once __DIR__."/"."db.php";
function displayIdea($argConnection, $argId) {
	$query = "select * from tblideabox where intIdeaBox_subcategory=".intval($argId)." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
	$result = mysqli_query($argConnection, $query);
	if ($result) {
		?>
		<ul>
			<?php
			while ($record = mysqli_fetch_array($result)) {
				?>
				<li id="li<?php echo intval($record["ideabox_ID"]); ?>">
					<?php
					echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8");
					?>
					<button class="deletable" data-idea-id="<?php echo intval($record["ideabox_ID"]); ?>">Delete</button>
					<?php
					displayIdea($argConnection, $record["ideabox_ID"]);
					?>
				</li>
				<?php
			}
			?>
		</ul>
		<?php
		mysqli_free_result($result);
		$result = null;
	} else {
		error_log("Error : mysqli_query");
	}
}
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Home</title>
		<!--Css contection!-->
		<link href="https://fonts.googleapis.com/css?family=Reem+Kufi|EB+Garamond" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" >
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main_blog.css">
		<link rel="stylesheet" href="css/adaptive_blog.css">
		<style>
			a button {
				color: black;
			}
		</style>
	</head>
	<body>
		<div class="content_container">
			<div class="header">
				<div class="line_container"> <img src="img/line.png" alt="line"> </div>
				<div class="text_container">
					<div class="title"> DESIGN DOCUMENT </div>
					<div class="subtitle"> PROJECT IDEAS </div>
				</div>
				<div class="line_container" style="margin-top:20px;"> <img src="img/line.png" alt="line"> </div>
				<div id="header_1" class="animated subtext hided">
					<a href="index.php"><button type="button">Write</button></a>
					<a href="reorder.php"><button type="button">Reorder</button></a>
					<a href="grid.php"><button type="button">Deadlines</button></a>
					<?php
					$connection = mysqli_connect($db_host, $db_user, $db_pass);
					if ($connection) {
						mysqli_select_db($connection, $db_name);
						mysqli_set_charset($connection, "utf8");
						$query = "select * from tblideabox where intIdeaBox_subcategory=0;";
						$result = mysqli_query($connection, $query);
						if ($result) {
							?>
							<ul>
								<?php
								while ($record = mysqli_fetch_array($result)) {
									?>
									<li id="li<?php echo intval($record["ideabox_ID"]); ?>">
										<?php
										echo htmlspecialchars($record["strIdeaBox_title"], ENT_COMPAT | ENT_HTML5, "UTF-8");
										?>
										<button class="deletable" data-idea-id="<?php echo intval($record["ideabox_ID"]); ?>">Delete</button>
										<?php
										displayIdea($connection, $record["ideabox_ID"]);
										?>
									</li>
									<?php
								}
								?>
							</ul>
							<?php
							mysqli_free_result($result);
							$result = null;
						} else {
							error_log("Error : mysqli_query");
						}
						mysqli_close($connection);
						$connection = null;
					} else {
						error_log("Error : mysqli_connect");
					}
					?>
				</div>
			</div>
		</div>
		<!--Script contection!-->
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
		<script src="js/animation.js"></script>
		<script src="js/readingTime.min.js"></script>
		<script>
			function GetXhr() {
				var xhr = null;
				if (window.XMLHttpRequest) {
					xhr = new XMLHttpRequest();
				} else if (window.ActiveXObject) {
					try {
						xhr = new ActiveXObject('Msxml2.XMLHTTP');
					}
					catch (e) {
						xhr = new ActiveXObject('Microsoft.XMLHTTP');
					}
				}
				return xhr;
			}
			function delete1(ev) {
				me = this;
				this.style.color = 'orange';
				var xhr=GetXhr();
				if (xhr) {
					var formData = new FormData();
					formData.append('id', this.getAttribute('data-idea-id'));
					xhr.onreadystatechange =
						function () {
							if ((this.readyState == 4) && (this.status == 200)) {
								result = JSON.parse(this.responseText);
								if (result.status == 'success') {
									elementLi=me.parentElement;
									elementUl=elementLi.parentElement;
									me.style.color = null;
									elementLi.remove();
									if (elementUl.children.length == 0) {
										elementUl.remove();
									}
								} else {
									me.style.color = 'red';
								}
							}
						};
					formData.append('check', this.checked);
					page = 'delete.php';
					xhr.open('POST', page, true);
					xhr.send(formData);
					delete xhr;
					xhr = null;
				}
			}
			elements = document.querySelectorAll('.deletable');
			for (i = 0 ; i < elements.length ; i++) {
				elements[i].addEventListener('click', delete1);
			}
		</script>
	</body>
</html>
