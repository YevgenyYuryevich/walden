<?php
session_start();
if (!isset($_SESSION["client_ID"]) || $_SERVER["PHP_SELF"]=="/guardian/access.php") {
	?>
	<!DOCTYPE html>
	<html>
		<head>
			<title>access</title>
		</head>
		<body>
			<?php
			if (isset($_POST["access_ok"])) {
				$client_ID=intval($_POST["client_ID"]);
				if ($client_ID) {
					$_SESSION["client_ID"]=$client_ID;
					?>
					Session id is set to <?php echo $client_ID; ?>.
					Please refresh page to continue.
					<?php
				} else {
					$_SESSION["client_ID"]=null;
					?>
					Session id is erased !
					<?php
				}
			} else {
				?>
				<form method="post">
					Choose id to fake (0 resets session): <input type="number" name="client_ID"><br>
					<input type="submit" name="access_ok" value="OK">
				</form>
				<?php
			}
			?>
		</body>
	</html>
	<?php
	exit(0);
}
