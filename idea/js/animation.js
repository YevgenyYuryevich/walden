
$(function () {
  initVars();
  listeScroll();
  checkVisibility();
  sliderControlls();
  autoSlider();
});

function listeScroll() {
    $(window).scroll(function(){
        checkVisibility();
    });
}

function autoSlider(){
  var current_number = 1;
  auto();

  function auto(){
    if(current_number == 1){
      current_number = 2;
      setTimeout(function(){
        if(changed)  autoChangeImage("img/slide_2.jpg",$("#slide2"))
        auto();
      },8000)
    }else  if(current_number == 2){
      current_number = 3;
      setTimeout(function(){
       if(changed)   autoChangeImage("img/slide_3.jpg",$("#slide3"))
        auto();
      },8000)
    }else  if(current_number == 3){
      current_number = 4;
      setTimeout(function(){
        if(changed)  autoChangeImage("img/slide_4.jpg",$("#slide4"))
        auto();
      },8000)
    }else  if(current_number == 4){
      current_number = 1;
      setTimeout(function(){
        if(changed)  autoChangeImage("img/slide_1.jpg",$("#slide1"))
        auto();
      },8000)
    }


  }


}



function autoChangeImage(src, current_point){
  changeImage(src);
  current.removeClass("active");
  current_point.addClass("active");
  current = current_point;

}

function checkVisibility() {
  header_1.isOnScreen(function () {
    header_1.removeClass('hided');
  });

  header_2.isOnScreen(function () {
    header_2.removeClass('hided');
  });

  header_3.isOnScreen(function () {
    header_3.removeClass('hided');
  });

  summary_1.isOnScreen(function () {
    summary_1.removeClass('hided');
  });

  summary_2.isOnScreen(function () {
    summary_2.removeClass('hided');
  });

  summary_3.isOnScreen(function () {
    summary_3.removeClass('hided');
  });

  thumb_1.isOnScreen(function () {
    thumb_1.removeClass('hided');

    setTimeout(function () {
      thumb_2.removeClass('hided');
    },150);

    setTimeout(function () {
      thumb_3.removeClass('hided');
    },300);

  });

  texted_1.isOnScreen(function () {
    texted_1.removeClass('hided');
  });

  texted_2.isOnScreen(function () {
    texted_2.removeClass('hided');
  });

  gallery.isOnScreen(function () {
    gallery.removeClass('hided');
  });

  inputed_1.isOnScreen(function () {
    inputed_1.removeClass('hided');
  });
}

function sliderControlls(){
   var slide1= $("#slide1"),slide2= $("#slide2"),slide3= $("#slide3"),slide4= $("#slide4");
   current = slide1;

   slide1.click(function() {
     changeImage("/img/slide_1.jpg");

     changed = false;
     clearTimeout(timeout);
     timeout = setTimeout(function () {
       changed = true;
     }, 8000);

     current.removeClass("active");
     current = slide1;
     current.addClass("active");

   });
   slide2.click(function() {
     changeImage("/img/slide_2.jpg");

     changed = false;
     clearTimeout(timeout);
     timeout = setTimeout(function () {
       changed = true;
     }, 8000);

     current.removeClass("active");
     current = slide2;
     current.addClass("active");
   });
   slide3.click(function() {
     changeImage("/img/slide_3.jpg");

     changed = false;
     clearTimeout(timeout);
     timeout = setTimeout(function () {
       changed = true;
     }, 8000);

     current.removeClass("active");
     current = slide3;
     current.addClass("active");
   });
   slide4.click(function() {
     changeImage("/img/slide_4.jpg");

     changed = false;
     clearTimeout(timeout);
     timeout = setTimeout(function () {
       changed = true;
     }, 8000);

     current.removeClass("active");
     current = slide4;
     current.addClass("active");
   });
}

function changeImage(new_src) {
  $("#itemed").fadeOut(500,function () {
     $("#itemed").attr('src',new_src);
     $("#itemed").fadeIn(900);
  });

}

function initVars(){
  summary_1 = $("#summary_1");
  summary_2 = $("#summary_2");
  summary_3 = $("#summary_3");

  header_1 = $("#header_1");
  header_2 = $("#header_2");
  header_3 = $("#header_3");

  thumb_1 = $("#thumb_1");
  thumb_2 = $("#thumb_2");
  thumb_3 = $("#thumb_3");

  texted_1 = $("#texted_1");
  texted_2 = $("#texted_2");

  gallery = $("#gallery");
  inputed_1 = $("#inputed_1");
}

$.fn.isOnScreen = function (cbTrue, cbFalse) {

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    if (bounds !== undefined) {
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();
    } else {
        bounds = {left: 0, top: 0, right: 0, bottom: 0};
    }

    var isOnScreen = (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    return isOnScreen
      ? cbTrue && cbTrue(viewport, bounds)
      : cbFalse && cbFalse(viewport, bounds);
};

var summary_1,summary_2,summary_3;
var header_1, header_2, header_3;
var thumb_1,thumb_2, thumb_3;
var texted_1,texted_2;
var gallery ,inputed_1;

var changed = true;
var current;
var timeout;
