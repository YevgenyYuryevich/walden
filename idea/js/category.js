$(function () {
  rowsControll();
  checkBox();
  popopControll();
});


function rowsControll() {
  var
   $first_column  = $("#first_column"),
   $second_column = $("#second_column"),
   $third_column  = $("#third_column");

   var
    $first_column_b  = $("#first_column_b"),
    $second_column_b = $("#second_column_b"),
    $third_column_b  = $("#third_column_b");

    $first_column.hover(function() {
      $first_column_b.addClass( "active" );
    }, function() {
      $first_column_b.removeClass( "active" );
    });

    $second_column.hover(function() {
      $second_column_b.addClass( "active" );
    }, function() {
      $second_column_b.removeClass( "active" );
    });

    $third_column.hover(function() {
      $third_column_b.addClass( "active" );
    }, function() {
      $third_column_b.removeClass( "active" );
    });


}

function checkBox(){
  $(".checkbox").click(function () {
    if($(this).hasClass("checked"))
        $(this).removeClass("checked");
    else
        $(this).addClass("checked");
  });
}

function popopControll() {
  $("#create_new").click(function () {
    openPopup($("#p1"), $("#p1_b"));
  });
  $("#create_new_1").click(function () {
    openPopup($("#p1"), $("#p1_b"));
  });



  $("#p1").click(function () {closePopup($("#p1"), $("#p1_b"));});
  $("#p1_b").click(function (e) {e.stopPropagation();});
}

function openPopup(pop,pop_b){
  pop.removeClass("hided");
  pop_b.removeClass("hided_b");
}

function closePopup(pop,pop_b){
  pop.addClass("hided");
  pop_b.addClass("hided_b");
}
