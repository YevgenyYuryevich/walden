jQuery(document).ready(function ($) {
    $('.toogle-complete').click(function () {
        var id = $(this).data('id');
        $(this).toggleClass('btn-success');
        var ajax_data = {
            action: 'set_done',
            id: id,
            done: $(this).hasClass('btn-success') ? '1' : '0'
        };
        console.log(ACTION_URL);
        $.ajax({
            url: ACTION_URL,
            data: ajax_data,
            success: function (res) {
                if (res.status == true){

                }
                else {
                    alert('something went wrong');
                }
            },
            type: 'post',
            dataType: 'json'
        })
    })
})