<?php
require_once "../guardian/access.php";
require_once __DIR__."/"."db.php";
header("Content-Type: application/json");
$connection = mysqli_connect($db_host, $db_user, $db_pass);
if ($connection) {
	mysqli_select_db($connection, $db_name);
	mysqli_set_charset($connection, "utf8");
	if (isset($_POST["parent"]) && isset($_POST["title"]) && isset($_POST["text"])) {
		$query = "select intIdeaBox_order from tblideabox where intIdeaBox_subcategory=".intval($_POST["parent"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"])." order by intIdeaBox_order desc;";
		$result = mysqli_query($connection, $query);
		$row = mysqli_fetch_array($result);
		$order = intval($row["intIdeaBox_order"])+1;
		mysqli_free_result($result);
		$result = null;
		$query = "insert into tblideabox (strIdeaBox_title, strIdeaBox_idea, intIdeaBox_subcategory, intIdeaBox_contractor_ID, intIdeaBox_fromclient, intIdeaBox_series_ID, intIdeaBox_order) values ('".mysqli_real_escape_string($connection, $_POST["title"])."', '".mysqli_real_escape_string($connection, $_POST["text"])."', ".intval($_POST["parent"]).", ".intval($_SESSION["client_ID"]).", 0, 0, ".$order.");";
		$result = mysqli_query($connection, $query);
		if ($result !== false) {
			$status = "success";
			$id = mysqli_insert_id($connection);
		} else {
			$status = "error";
			$id = 0;
		}
	}
	mysqli_close($connection);
	$connection = null;
} else {
	error_log("Error : mysqli_connect");
}
echo json_encode(Array("status" => $status, "id" => $id));
