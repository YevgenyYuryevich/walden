<?php
require_once "../guardian/access.php";
require_once __DIR__."/"."db.php";
header("Content-Type: application/json");
$query = "";
$connection = mysqli_connect($db_host, $db_user, $db_pass);
if ($connection) {
	mysqli_select_db($connection, $db_name);
	mysqli_set_charset($connection, "utf8");
	if (isset($_POST["id"])) {
		if (!isset($_POST["check"])) {
			$status = "error";
		} elseif ($_POST["check"] === "true") {
			$query = "update tblideabox set boolIdeaBox_done=1 where ideabox_ID=".intval($_POST["id"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
		} else {
			$query = "update tblideabox set boolIdeaBox_done=0 where ideabox_ID=".intval($_POST["id"])." and intIdeaBox_contractor_ID=".intval($_SESSION["client_ID"]).";";
		}
		if ($query != "") {
			mysqli_query($connection, $query);
			$result = mysqli_affected_rows($connection);
			if ($result !== false) {
				$status = "success";
			} else {
				$status = "error";
			}
		}
	}
	mysqli_close($connection);
	$connection = null;
} else {
	error_log("Error : mysqli_connect");
}
echo json_encode(Array("status" => $status, "check" => $_POST["check"]));
