<?php

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

use Core\Controller_core;
use Models\EditSeries_m;
use Controllers\{Subscriptions_c, Posts_c};
use Models\api\ClientMeta_m;
use function Helpers\htmlMailHeader;

class EditSeries extends Controller_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('api_m/ClientMeta_m');
        $this->load->controller('Subscriptions_c');
        $this->load->controller('Posts_c');
        $this->load->model('EditSeries_m');
        $this->load->model('api_m/Posts_m');
        $this->load->model('api_m/Subscriptions_m');
        $this->load->model('api_m/Options_m');
        $this->load->model('api_m/FormField_m');
        $this->load->model('api_m/FormFieldAnswer_m');

        $this->optionModel = new \Models\api\Options_m();
        $this->model = new EditSeries_m();
        $this->clientMetaModel = new ClientMeta_m();
        $this->postsModel = new \Models\api\Posts_m();
        $this->subsModel = new \Models\api\Subscriptions_m();
        $this->subsHandle = new Subscriptions_c();
        $this->postsHandle = new Posts_c();
        $this->currentUser = $_SESSION['client_ID'];
        $this->defaultPhoto = 'assets/images/beautifulideas.jpg';

        $this->formFieldModel = new \Models\api\FormField_m();
        $this->formFieldAnswerModel = new \Models\api\FormFieldAnswer_m();

        $this->load->controller('User_c');
        $this->userHandle = new \Controllers\User_c();
    }

    public function index()
    {
        $usersCanEditSeries = $this->optionModel->get(['strOption_name' => 'users_can_edit_series']);
        $usersCanEditSeries = $usersCanEditSeries ? (int) $usersCanEditSeries['strOption_value'] : false;
        if ($usersCanEditSeries) {
            $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
            $join = isset($_POST['join']) ? $_POST['join'] : (isset($_GET['join']) ? $_GET['join'] : false);
            $panel = isset($_POST['panel']) ? $_POST['panel'] : (isset($_GET['panel']) ? $_GET['panel'] : 'main');

            $default_image = 'assets/images/beautifulideas.jpg';
            if ($id){
                $series = $this->model->getSeriesById($id);
            }
            else{
                $series = $this->model->getRandomSeries();
            }
            if (!$series){
                echo '<h1>There is nothing to display</h1>';
                echo '<h3><a href="createseries">Create One</a></h3>';
                die();
            }

            $purchase = $this->model->getPurchaseBySeriesId($series['series_ID']);
            if (!$purchase){
                if ($join){
                    $this->model->joinSeries($series['series_ID']);
                }
                else if ($this->isSeriesMine($series['series_ID'])) {
                    $this->model->joinSeries($series['series_ID']);
                }
                else{
                    echo '<h1>You have to join to this series Or Not Active Now</h1>';
                    echo '<h3><a href="viewexperience?id='. $series['series_ID'] .'">Go To</a></h3>';
                    die();
                }
                $purchase = $this->model->getPurchaseBySeriesId($series['series_ID']);
            }

            if ($series['intSeries_client_ID'] == $this->currentUser){
                $posts = $this->postsModel->getPosts(['intPost_series_ID' => $series['series_ID'], 'intPost_parent' => 0], 'post_ID, strPost_title, strPost_featuredimage, intPost_order, strPost_nodeType, intPost_parent');

                $isSeriesMine = true;
                $availableDays = $series['available_days'];
            }
            else{
                $subscriptions = $this->subsHandle->getSubscriptions(['intClientSubscription_purchased_ID' => $purchase['purchased_ID']]);
                $isSeriesMine = false;
                $availableDays = $purchase['available_days'];
                $posts = $subscriptions;
            }

            $owner = $this->userHandle->get($series['intSeries_client_ID']);
            $stripeAccountId = $owner['stripe_account_id'];
            $isStripeAvailable = true && $stripeAccountId;
            $stripeClientRow = $this->optionModel->get(['strOption_name' => 'stripe_client_id']);
            $stripeClientId = $stripeClientRow ? $stripeClientRow['strOption_value'] : '';

            $formFields = $this->formFieldModel->getRows(['intFormField_series_ID' => $id]);
            $formFieldAnswers = $this->formFieldAnswerModel->getRows(['intFormFieldAnswer_client_ID' => $_SESSION['client_ID']]);
            $vParams = [
                'isSeriesMine' => $isSeriesMine,
                'series' => $series,
                'posts' => $posts,
                'purchase' => $purchase,
                'default_image' => $default_image,
                'panel' => $panel,
                'availableDays' => $availableDays,
                'isStripeAvailable' => $isStripeAvailable,
                'stripeClientId' => $stripeClientId,
                'formFields' => $formFields,
                'formFieldAnswers' => $formFieldAnswers];

            $this->load->view('editSeries_v', $vParams);
        }
        else {
            $this->load->template('403.php');
        }
    }

    public function getFood2Fork($id){
        $curl = curl_init();
        $url = 'http://food2fork.com/api/get?key=4b57211bd951556bb6f35998d1586fff&rId=' . $id;
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        return $result->recipe;
    }
    public function isSeriesMine($seriesId){
        $clientId = $this->model->getClientIdBySeriesID($seriesId);
        return $clientId == $this->currentUser;
    }
    public function orderSubscriptionsByPurchasedId($purchasedId){
        $subscriptions = $this->model->getSubscriptionsByPurchasedId($purchasedId);
        $orders = [];
        foreach ($subscriptions as $key => $subscription){
            $this->model->setSubscriptionOrder($subscription['clientsubscription_ID'], $key + 1);
            $orders[$subscription['clientsubscription_ID']] = $key + 1;
        }
        return $orders;
    }

    public function test(){
        $this->orderSubscriptionsByPurchasedId(2);
        echo 'yes';
        die();
    }

    public function ajax_setAvailableDays(){
        $availableDays = $_POST['days'];
        $purchasedId = $_POST['purchasedId'];
        $seriesId = $_POST['seriesId'];

        $rlt1 = true;
        if ($this->isSeriesMine($seriesId)){
            $rlt1 = $this->model->setSeriesAvailableDays($seriesId, $availableDays);
        }
        $rlt2 = $this->model->setPurchaseAvailableDays($purchasedId, $availableDays);
        $this->filterFirstCustomize();
        echo json_encode(['status' => $rlt1 && $rlt2 ? true : false]);
        die();
    }
    public function ajax_setPostsOrder(){
        $seriesId = $_POST['seriesId'];
        $sets = $_POST['updateSets'];

        if ($this->isSeriesMine($seriesId)){
            $purchased = $this->model->getPurchaseBySeriesId($seriesId);
            foreach ($sets as $set){
                $this->model->setPostOrder($set['id'], $set['order']);
                $subscription = $this->model->getSubscriptionByPurchasedIdPostId($purchased['purchased_ID'], $set['id']);
                $this->model->setSubscriptionOrder($subscription['clientsubscription_ID'], $set['order']);
            }
        }
        else{
            foreach ($sets as $set){
                $this->model->setSubscriptionOrder($set['id'], $set['order']);
            }
        }
        $this->filterFirstCustomize();
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_deletePost(){
        $where = $_POST['where'];
        $seriesId = $_POST['meta'];

        if ($this->isSeriesMine($seriesId)){
            $this->postsHandle->delete($where);
        }
        else{
            $this->subsHandle->trash($where);
        }

        $this->filterFirstCustomize();
        echo json_encode(['status' => true]);
        die();
    }
    public function ajax_clonePost(){
        $where = $_POST['where'];
        $cloneItem = $this->postsHandle->cloneItem($where);
        $this->postsHandle->orderPosts(['intPost_series_ID' => $cloneItem['intPost_series_ID'], 'intPost_parent' => $cloneItem['intPost_parent']]);
        $this->filterFirstCustomize();
        echo json_encode(['status' => true, 'data' => $cloneItem]);
        die();
    }
    public function ajax_cloneSubscription(){
        $where = $_POST['where'];
        $cloneItem = $this->subsHandle->cloneItem($where);
        $newOrders = $this->subsHandle->orderSubscriptions(['intClientSubscription_purchased_ID' => $cloneItem['intClientSubscription_purchased_ID'], 'intClientSubscriptions_parent' => $cloneItem['intClientSubscriptions_parent']]);
        $this->filterFirstCustomize();
        echo json_encode(['status' => true, 'data' => $cloneItem, 'orders' => $newOrders]);
        die();
    }
    public function formatYoutubeById($id){
        return 'https://www.youtube.com/embed/' . $id;
    }
    public function makePostBodyByRecipe($recipe){
        $body = '<section class="recipe-body">';
        $body .= '<div><img src="'. $recipe->image_url .'"></div>';
        $body .= '<h4 style="margin-bottom: 0px;">Ingredients:</h4>';
        $body .= '<ul>';
        foreach ($recipe->ingredients as $ingredient){
            $body .= '<li>'. $ingredient .'</li>';
        }
        $body .= '</ul>';
        $body .= '<h5><lable>Publisher:</lable> <span class="publisher">'. $recipe->publisher .'</span></h5>';
        $body .= '<div><a href="'. $recipe->source_url .'" target="_blank">View Detail</a></div>';
        $body .= '</section>';
        return $body;
    }
    public function makePostBodyFromIdeabox($postData){
        $refLink = $postData['reference_link'];
        $refText = $postData['reference_text'];

        $isYoutube = preg_match('@^(https://www.youtube.com/[^/=]+[/=])(?<youtubeId>\w+)@', $refLink, $matches);
        if ($isYoutube){
            $youtubeId = $matches['youtubeId'];
            $refHtml = sprintf('<video class="afterglow" data-autoresize="fit" id="video_%s_%s" width="960" height="540" data-youtube-id="%s"></video>', uniqid(), $youtubeId, $youtubeId);
        }
        else{
            $refHtml = $refLink ? sprintf('<a href="%s">%s</a>', $refLink, $refText) : '';
        }
        $postBody = sprintf('<p><img src="%s"></p><div>%s</div><div>%s</div>', $postData['image'], $postData['body'], $refHtml);
        return $postBody;
    }
    public function putContents($orgUrl, $prefix, $name, $extends){
        $name = preg_replace('/[\?\/\#]/', '-', $name);
        $uniqueFilename = uniqid(str_replace(' ', '_', $prefix)).'_' . urlencode($name) . '.' . $extends;
        $savedUrl = 'assets/images/' . $uniqueFilename;
        file_put_contents($savedUrl, fopen($orgUrl, 'r'));

        return $savedUrl;
    }
    public function ajax_addEntriesToPost(){
        $seriesId = $_POST['seriesId'];
        $entries = isset($_POST['entries']) ? $_POST['entries'] : [];
        $metaPosts = isset($entries['metaPostItems']) ? $entries['metaPostItems'] : [];

        $youtubePosts = isset($entries['youtubeItems']) ? $entries['youtubeItems'] : [];
        $podcastsPosts = isset($entries['podcastsItems']) ? $entries['podcastsItems'] : [];
        $blogsPosts = isset($entries['blogsItems']) ? $entries['blogsItems'] : [];
        $recipesPosts = isset($entries['recipesItems']) ? $entries['recipesItems'] : [];
        $ideaboxPosts = isset($entries['ideaboxItems']) ? $entries['ideaboxItems'] : [];
        $postPosts = isset($entries['postItems']) ? $entries['postItems'] : [];
        $rssbPostPosts = isset($entries['rssbPostItems']) ? $entries['rssbPostItems'] : [];

        $purchase = $this->model->getPurchaseBySeriesId($seriesId);

        $posts = [];
        foreach ($youtubePosts as $youtubePost){
            $id = $this->model->insertPost($seriesId, $youtubePost['title'], $this->formatYoutubeById($youtubePost['body']), $youtubePost['summary'], $youtubePost['type'], $youtubePost['image'], $youtubePost['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($podcastsPosts as $post){
            $id = $this->model->insertPost($seriesId, $post['title'], $post['body'], $post['summary'], $post['type'], $post['image'], $post['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($blogsPosts as $post){
            if (\Helpers\isImage($post['image'], $fName, $exs)){
                $savedUrl = $post['image'];
            }
            else{
                preg_match('/fm=([^\&]*)/', $post['image'], $matches);
                $savedUrl = $this->putContents($post['image'], 'blogs', '', $matches[1]);
            }
            $id = $this->model->insertPost($seriesId, $post['title'], $post['body'], $post['summary'], $post['type'], $savedUrl, $post['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($recipesPosts as $post){

            $recipe = $this->getFood2Fork($post['id']);
            $body = $this->makePostBodyByRecipe($recipe);

            $id = $this->model->insertPost($seriesId, $post['title'], $body, $post['summary'], $post['type'], $post['image'], $post['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($ideaboxPosts as $ideaboxPost){
            $postBody = $this->makePostBodyFromIdeabox($ideaboxPost);
            $id = $this->model->insertPost($seriesId, $ideaboxPost['title'], $postBody, $ideaboxPost['summary'], $ideaboxPost['type'], $ideaboxPost['image'], $post['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($postPosts as $postPost){
            $id = $this->model->insertPost($seriesId, $postPost['title'], $postPost['body'], $postPost['summary'], $postPost['type'], $postPost['image'], $postPost['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($rssbPostPosts as $rssbPostPost){
            $id = $this->model->insertPost($seriesId, $rssbPostPost['title'], $rssbPostPost['body'], $rssbPostPost['summary'], $rssbPostPost['type'], $rssbPostPost['image'], $rssbPostPost['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }
        foreach ($metaPosts as $postPost){
            $id = $this->model->insertPost($seriesId, $postPost['title'], $postPost['body'], $postPost['summary'], $postPost['type'], $postPost['image'], $postPost['keyword']);
            $post = $this->model->getPostById($id);
            array_push($posts, $post);
        }

        foreach ($posts as $post){
            $this->model->copyPostToSubscription($post['post_ID'], $purchase['purchased_ID']);
        }
        $this->filterFirstCustomize();
        echo json_encode(['status' => true, 'data' => $posts]);
        die();
    }
    public function ajax_addEntriesToSubscription(){
        $seriesId = $_POST['seriesId'];
        $entries = isset($_POST['entries']) ? $_POST['entries'] : [];

        $metaPosts = isset($entries['metaPostItems']) ? $entries['metaPostItems'] : [];
        $youtubePosts = isset($entries['youtubeItems']) ? $entries['youtubeItems'] : [];
        $podcastsPosts = isset($entries['podcastsItems']) ? $entries['podcastsItems'] : [];
        $blogsPosts = isset($entries['blogsItems']) ? $entries['blogsItems'] : [];
        $recipesPosts = isset($entries['recipesItems']) ? $entries['recipesItems'] : [];
        $ideaboxPosts = isset($entries['ideaboxItems']) ? $entries['ideaboxItems'] : [];
        $postPosts = isset($entries['postItems']) ? $entries['postItems'] : [];
        $rssbPostPosts = isset($entries['rssbPostItems']) ? $entries['rssbPostItems'] : [];

        $purchase = $this->model->getPurchaseBySeriesId($seriesId);

        $posts = [];
        foreach ($youtubePosts as $youtubePost){
            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $youtubePost['title'],
                'body' => $this->formatYoutubeById($youtubePost['body']),
                'image' => $youtubePost['image'],
                'type' => $youtubePost['type'],
                'order' => false,
                'finished' => 0
            ];
            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        foreach ($podcastsPosts as $post){
            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $post['title'],
                'body' => $post['body'],
                'image' => $post['image'],
                'type' => $post['type'],
                'order' => false,
                'finished' => 0
            ];
            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        foreach ($blogsPosts as $post){
            if (\Helpers\isImage($post['image'], $fName, $exs)){
                $savedUrl = $post['image'];
            }
            else{
                preg_match('/fm=([^\&]*)/', $post['image'], $matches);
                $savedUrl = $this->putContents($post['image'], 'blogs', '', $matches[1]);
            }
            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $post['title'],
                'body' => $post['body'],
                'image' => $savedUrl,
                'type' => $post['type'],
                'order' => false,
                'finished' => 0
            ];
            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        foreach ($recipesPosts as $post){

            $recipe = $this->getFood2Fork($post['id']);
            $body = $this->makePostBodyByRecipe($recipe);
            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $post['title'],
                'body' => $body,
                'image' => $savedUrl,
                'type' => $post['type'],
                'order' => false,
                'finished' => 0
            ];
            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        foreach ($ideaboxPosts as $ideaboxPost){
            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $ideaboxPost['title'],
                'body' => $this->makePostBodyFromIdeabox($ideaboxPost),
                'image' => $ideaboxPost['image'],
                'type' => $ideaboxPost['type'],
                'order' => false,
                'finished' => 0
            ];

            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        foreach ($postPosts as $postPost){

            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $postPost['title'],
                'body' => $postPost['body'],
                'image' => $postPost['image'],
                'type' => $postPost['type'],
                'order' => false,
                'finished' => 0
            ];

            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }

        foreach ($metaPosts as $postPost){

            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $postPost['title'],
                'body' => $postPost['body'],
                'image' => $postPost['image'],
                'type' => $postPost['type'],
                'order' => false,
                'finished' => 0
            ];

            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        foreach ($rssbPostPosts as $rssbPostPost){
            $data = [
                'purchasedId' => $purchase['purchased_ID'],
                'postId' => null,
                'title' => $rssbPostPost['title'],
                'body' => $rssbPostPost['body'],
                'image' => $rssbPostPost['image'],
                'type' => $rssbPostPost['type'],
                'order' => false,
                'finished' => 0
            ];

            $id = $this->model->insertSubscription($data);
            $post = $this->model->getSubscription($id);
            array_push($posts, $post);
        }
        $this->filterFirstCustomize();
        echo json_encode(['status' => true, 'data' => $posts]);
        die();
    }
    public function ajax_updateCoverImage(){
        $seriesId = $_POST['seriesId'];
        $file = $_FILES['src'];

        if ($file['tmp_name'] !== ''){
            $unique_filename = uniqid('series').'_' . $file['name'];
            $destination = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/' . $unique_filename;
            move_uploaded_file($file['tmp_name'], $destination);
            $savedUrl = 'assets/images/' . $unique_filename;
        }
        else{
            $savedUrl = $this->defaultPhoto;
        }
        $this->model->setSeriesImage($seriesId, $savedUrl);
        $this->filterFirstCustomize();
        echo json_encode(['status' => true]);
        die();
    }
    public function filterFirstCustomize(){
        $flgCustomize = $this->clientMetaModel->get(['meta_key' => 'first_series_customize']);
        if (!$flgCustomize){
            $this->clientMetaModel->insert(['meta_key' => 'first_series_customize']);
            $headers = htmlMailHeader();
            $msg = file_get_contents(_ROOTPATH_ . '/assets/email-templates/editseries.html');
            mail($_SESSION['guardian']['email'], 'Congratulations on customizing your experience!', $msg, $headers);
        }
    }
}

$editSeriesHandle = new EditSeries();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        case 'set_available_days':
            $editSeriesHandle->ajax_setAvailableDays();
            break;
        case 'set_posts_order':
            $editSeriesHandle->ajax_setPostsOrder();
            break;
        case 'delete_post':
            $editSeriesHandle->ajax_deletePost();
            break;
        case 'clone_post':
            $editSeriesHandle->ajax_clonePost();
            break;
        case 'clone_subscription':
            $editSeriesHandle->ajax_cloneSubscription();
            break;
        case 'add_entries_to_post':
            $editSeriesHandle->ajax_addEntriesToPost();
            break;
        case 'add_entries_to_subscription':
            $editSeriesHandle->ajax_addEntriesToSubscription();
            break;
        case 'update_cover_image':
            $editSeriesHandle->ajax_updateCoverImage();
            break;
        default:
            $editSeriesHandle->index();
            break;
    }
}
else{
    $editSeriesHandle->index();
//    $editSeriesHandle->test();
}