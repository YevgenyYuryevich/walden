<?php

require_once("guardian/access.php");

include_once("config.php");
include_once("dbconnect.php");

$query="SELECT `category_ID`, `strCategory_name`, `strCategory_description`, strCategory_image FROM `tblcategories` ORDER BY strCategory_name ASC";
$stmt = $myconnection->prepare ( $query );
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Daily Opportunities to simplify your life and grow. - the grey shirt</title>

    <!--Css contection!-->
    <link rel="stylesheet" href="assets/css/materialize.css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi|EB+Garamond|Montserrat" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/adaptive.css">
    <link rel="stylesheet" href="assets/css/category.css">
    <link rel="stylesheet" href="assets/css/category_adaptive.css">
<!--    <link rel="stylesheet" href="assets/css/yevgeny_custom.css">-->
    <link rel="stylesheet" href="assets/css/yevgeny_custom.css?version=<?php echo time();?>">
    <link rel="stylesheet" href="assets/css/yevgeny/component.css" />
    <link rel="stylesheet" href="assets/css/yevgeny/demo.css" />
    <link rel="stylesheet" href="assets/css/yevgeny/normalize.css" />
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Reem+Kufi" rel="stylesheet">
    <script src="assets/js/yevgeny/modernizr.custom.js"></script>
    <link rel="stylesheet" href="assets/css/main_blog.css">
    <link rel="stylesheet" href="assets/css/adaptive_blog.css">
    <link rel="stylesheet" href="assets/css/yevgeny/sweetalert2.min.css" />
<!--    <link rel="stylesheet" href="assets/css/yevgeny/join-series.css?version=--><?php //echo time();?><!--" />-->
    <link rel="stylesheet" href="assets/css/yevgeny/join-series.css" />

</head>

<body>
    <div class="category_content category_content_1">
		<? include('assets/includes/header.php')?>
        <div class="events">
            Today's Events
           <i class="material-icons">arrow_drop_down_circle</i>
        </div>
        <div class="insider insider_1" >
            <div class="top_line"> </div>
            <div class="bottom_line"> </div>
            <div class="right_line"> </div>
            <div class="left_line"> </div>
            <div class="rectangle" style="top:-10px; right:-10px;"></div>
            <div class="rectangle" style="bottom:-10px; right:-10px;"></div>
            <div class="row main_row " style="height:100%; border-top:2px solid black;">
                <div class="col-xs-0 col-sm-6 col-md-4 col-lg-3 category_rows">
                    <div class="big_rectangle"><i class="fa fa-bicycle" aria-hidden="true"></i>&nbsp; Free Yourself<i class="fa fa-caret-down drop_down" aria-hidden="true"></i> </div>
                    <div class="first_column">
                        <div class="category_column">
                            <img src="assets\images\free3.jpg" alt="" class="first_column_image">
                            <div class="first_column_data">
                                <div class="header_first_column"></div>
                                <div class="header_first_quotes">&rdquo;</div>
                                <div class="header_first_text">
                                    "Instead of trying to make your life perfect, give yourself the freedom to make it an adventure, and go ever upward."
                                    <br>
                                    <div class="author">-Drew Houston</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-12 col-sm-6 col-md-8 col-lg-9" style="padding-top: 30px;">
                    <section class="tt-grid-wrapper">
                        <nav style="background-color: white; color:black; font-family:'EB Garamond', sans-serif;">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                    <button class="back-to-categories"><img src="assets/images/global-icons/back.png" /><span>Back</span></button>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12">
                                    <div class="pagination">
                                        <button class="prev-page"><img src="assets/images/global-icons/previous.png"><span>Previous</span></button>
                                        <div class="page-items-wrapper"></div>
                                        <button class="next-page"><span>Next</span><img src="assets/images/global-icons/next.png" /></button>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
<!--                                    <button class="create-ideabox"><span>New</span><img src="assets/images/global-icons/plus-icon.png" /></button>-->
                                </div>
                            </div>
                        </nav>
                        <ul class="tt-grid tt-effect-3dflip tt-effect-delay">
                            <?php foreach ($rows as $key => $row){
                                if ($key == 6){ break; }
                                $rowid=htmlspecialchars_decode($row['category_ID']);
                                $rowimage=htmlspecialchars_decode($row['strCategory_image']);
                                $rowtitle=htmlspecialchars_decode($row['strCategory_name']);
                                $rowbody=htmlspecialchars_decode($row['strCategory_description']);
                                ?>
                                <li><a>
                                        <div class="column" style="">
                                            <div class="column_object" title= '<?php  echo strip_tags($rowbody);  ?>'>
                                                <img src="<?php echo $rowimage; ?>" alt="" class="object_image">
                                                <div class="object_title"><?php echo $rowtitle; ?></div>
                                                <div class="object_description">
                                                </div>
                                                <div data-id = '<?php echo $rowid; ?>' class="object_button" data-link = ''> Explore </div>
                                                <div class="object_line"></div>
                                            </div>
                                        </div>
                                    </a></li>
                            <?php } ?>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
	</div>
	<? include('assets/includes/footer.php')?>
    <div class="preview-rightbox">
        <div class="preview-lightbox-wrapper">
            <section class="preview-lightbox-content">
            </section>
            <button class="close">X</button>
        </div>
    </div>
    <!--Script contection!-->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
    <script src="assets/js/yevgeny_custom.js?version=<?php echo time();?>"></script>
    <script src="assets/js/category.js"></script>
    <script src="assets/js/yevgeny/classie.js"></script>
    <script src="assets/js/yevgeny/sweetalert2.min.js"></script>
    <script src="assets/js/yevgeny/thumbnailGridEffects.js?version=<?php echo time();?>"></script>
<!--    <script src="assets/js/yevgeny/thumbnailGridEffects.js"></script>-->
    <script src="assets/js/animation.js?version=<?php echo time(); ?>"></script>
    <script src="assets/js/animation.js"></script>
    <script src="assets/js/readingTime.min.js"></script>
<script src="assets/js/custom.js"></script>
</body>
</html>