<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 28.07.2018
 * Time: 08:43
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class Preview_Series_Embed extends \Core\Controller_core
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
    }
    public function index() {
        $id = isset($_POST['id']) ? $_POST['id'] : (isset($_GET['id']) ? $_GET['id'] : false);
        if (!$id){ echo 'no id'; die(); }
        $series = $this->seriesModel->get($id);
        $this->load->view('PreviewSeriesEmbed_v', ['series' => $series]);
    }
}

$handle = new Preview_Series_Embed();

$handle->index();