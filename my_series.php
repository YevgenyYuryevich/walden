<?php
/**
 * Client: Nathaniel Baca
 * User: Yevgeny Yuryevich
 * Created by PhpStorm.
 * Date: 13.11.2018
 * Time: 17:39
 */

require_once 'guardian/access.php';
require_once 'yevgeny/core/Controller_core.php';

class My_Series extends \Core\Controller_core
{
    private $purchasedModel;
    private $postsHandle;
    private $seriesModel;
    private $postsModel;
    private $categoryModel;
    private $optionModel;
    private $seriesOwnersModel;
    private $seriesHandle;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_m/Series_m');
        $this->seriesModel = new \Models\api\Series_m();
        $this->load->model('api_m/Posts_m');
        $this->postsModel = new \Models\api\Posts_m();
        $this->load->model('api_m/Category_m');
        $this->categoryModel = new \Models\api\Category_m();
        $this->load->controller('Posts_c');
        $this->postsHandle = new \Controllers\Posts_c();
        $this->load->model('api_m/Purchased_m');
        $this->purchasedModel = new \Models\api\Purchased_m();

        $this->load->model('api_m/Options_m');
        $this->optionModel = new \Models\api\Options_m();

        $this->load->model('api_m/Series_Owners_m');
        $this->seriesOwnersModel = new \Models\api\Series_Owners_m();

        $this->load->controller('Series_c');
        $this->seriesHandle = new \Controllers\Series_c();
    }
    public function index() {
        $allSeries = $this->seriesModel->getRows();
        $series = [];
        foreach ($allSeries as $seriesRow) {
            if ($seriesRow['intSeries_client_ID'] == $_SESSION['client_ID']) {
                $series[] = $seriesRow;
            }
            else {
                $owner = $this->seriesOwnersModel->get(['intSeriesOwner_series_ID' => $seriesRow['series_ID'], 'intSeriesOwner_user_ID' => $_SESSION['client_ID']]);
                if ($owner) {
                    $series[] = $seriesRow;
                }
            }
        }
        $categories = [];
        $cateFlgs = [];

        foreach ($series as & $sery) {
            $sery['posts'] = $this->postsHandle->getSerialItems(['intPost_series_ID' => $sery['series_ID'], 'intPost_parent' => 0], 'post_ID, intPost_series_ID, strPost_title, strPost_body, strPost_featuredimage, intPost_type, intPost_order, intPost_parent, strPost_nodeType, strPost_duration, strPost_keywords', ['intPost_order', 'post_ID'], 30);
            foreach ($sery['posts'] as &$post) {
                $post['viewDay'] = $this->postsHandle->calcDay($post);
                $body = $post['strPost_body'];
                switch ((int) $post['intPost_type']){
                    case 0:
                        $body = \Helpers\isAbsUrl($body) ? $body : BASE_URL . '/' . $body;
                        break;
                    case 2:
                        break;
                    case 8:
                        $imgSection = substr($body, 0, strpos($body, "</section>"));
                        $body = \Helpers\htmlPurify($imgSection);
                        break;
                    case 10:
                        break;
                    default:
                        $subBody = trim(substr($body, 0, 480));
                        $fltBody = stripslashes($subBody . '...');
                        $body = \Helpers\htmlPurify($fltBody);
                        break;
                }
                $post['strPost_body'] = $body;
            }
            if (!isset($cateFlgs[$sery['intSeries_category']])) {
                $cateFlgs[$sery['intSeries_category']] = true;
                $categories[] = $this->categoryModel->get($sery['intSeries_category']);
            }
            $this->seriesHandle->filterViewRecord($sery['series_ID']);
            $sery['totalViews'] = $this->seriesHandle->filterAnyViewRecord($sery['series_ID']);
            $joinCnt = $this->purchasedModel->count(['intPurchased_series_ID' => $sery['series_ID'], 'intPurchased_client_ID <>' => $_SESSION['client_ID']]);
            $sery['joinCnt'] = $joinCnt;
        }
        $series = \Helpers\utf8Encode($series);

        $stripeClientRow = $this->optionModel->get(['strOption_name' => 'stripe_client_id']);
        $stripeClientId = $stripeClientRow ? $stripeClientRow['strOption_value'] : '';
        $this->load->view('My_Series_v', ['categories' => $categories, 'series' => $series, 'stripeClientId' => $stripeClientId]);
    }
}

$handle = new My_Series();

if (isset($_POST['action'])){
    switch ($_POST['action']){
        default:
            $handle->index();
            break;
    }
}
else {
    $handle->index();
}